<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:r="http://ws.cmctos.com.my/ctosnet/response"
                xmlns:exslt="http://exslt.org/common"
                exclude-result-prefixes="xsl r">

    <xsl:template name="section_e_template">
    
            
        <p class="title">
            E1: ETR PLUS (NON-BANK PAYMENT HISTORY)
        </p>
        <p class="info" style="background-color: #DDDDDD;">ETR Plus is a non-bank payment information shared by third party sources. It serves as additional information
         	(other than banking payment history) to help in your credit assessment.
            <br/><br/>
            Note: The following information is provided by the third party sources for your reference. Searched Subject will also include transaction(s) that are
            under Guarantor capacity. CTOS does not gurantee the accuracy of the information as listed debts may have been settled since recorded.
            </p>
            <xsl:if test="(//r:summary/r:enq_sum/r:include_etr_plus = '2')">
            	<p class="info">
                   This section is for eTR Plus contributor only. To find out how you can contribute to get access, please contact us at
                   <a href = "mailto: etrplus@ctos.com.my">etrplus@ctos.com.my</a>.
                </p>
                <br/>
        	</xsl:if>
        
        <xsl:if test="(//r:summary/r:enq_sum/r:include_etr_plus = '1') and (//r:enquiry/r:section_etr_plus[@data = 'true'])"> 
           	<xsl:call-template name="etr_plus_account_profile_template"/>
            <br/>
            <xsl:call-template name="etr_plus_payment_history_template"/>
            <br/>
            <xsl:call-template name="non_blank_lender_credit_information_template"/>
            <br/>
        </xsl:if>
        <xsl:if test="((//r:enquiry/r:section_etr_plus[@data = 'false']) and (//r:summary/r:enq_sum/r:include_etr_plus != '2')) or not(//r:summary/r:enq_sum/r:include_etr_plus)">
            
            <p class="info">No Information Available</p>
            
            <br/>
            
        </xsl:if>
        
        <p class="title">
            E2: TRADE REFERENCE
        </p>
            		
        <xsl:if test="//r:tr_report/r:enquiry"> 
            <xsl:for-each select="//r:tr_report/r:header">
                <xsl:variable name="ref_com_name">
                    <xsl:value-of select="r:ref_com_name"/>
                </xsl:variable>
        
                <xsl:variable name="ref_com_bus">
                    <xsl:value-of select="r:ref_com_bus"/>
                </xsl:variable>

                <xsl:for-each select="../r:enquiry/r:section">
                    <xsl:call-template name="trade_reference_relationship_template"/>
                    <xsl:call-template name="trade_reference_sponsor_template"/>
                    <xsl:call-template name="trade_reference_account_status_template"/>
                    <xsl:call-template name="trade_reference_return_cheque_template"/>
                    <xsl:call-template name="trade_reference_legal_action_template"/>
                    <xsl:call-template name="trade_reference_cra_comment_template"/>
                    <xsl:call-template name="trade_reference_subject_comment_template"/>
                    <xsl:call-template name="trade_reference_contact_template">
                        <xsl:with-param name="referee" select="$ref_com_name"/>
                        <xsl:with-param name="nature_of_business" select="$ref_com_bus"/>
                    </xsl:call-template>
                </xsl:for-each>
            </xsl:for-each>
            <br/>
        </xsl:if>
        <xsl:if test="not(//r:tr_report/r:enquiry)">
            
            <p class="info">No Information Available</p>
            
            <br/>
            
        </xsl:if>
    </xsl:template>

    <!-- start trade_reference_relationship -->
    <xsl:template name="trade_reference_relationship_template">
        
        <xsl:if test="@id = 'relationship'">                       
            
            <p class="header">The following information are in relation to Account No: <xsl:value-of select="r:data[@name = 'account_no']"/></p>
        
            <table class="table">
                <tr>
                    <td class="header caption text-left">
                        <xsl:value-of select="position()"/>. Relationship</td>
                    <td class="short-content">
                        Subject is <xsl:value-of select="r:data[@name = 'rel_type']"/>
                    </td>
                        
                    <td class="header caption text-left">Relationship Start Date</td>
                    <td class="short-content">
                        <xsl:if test="r:data[@name = 'rel_sday'] != '' and r:data[@name = 'rel_smonth'] != '' and r:data[@name = 'rel_syear'] != ''">
                            <xsl:call-template name="check_empty_string">
                                <xsl:with-param name="value" select="r:data[@name = 'rel_sday']"/>
                            </xsl:call-template>
                                &#160;
                            <xsl:call-template name="month_name_template">
                                <xsl:with-param name="month" select="r:data[@name = 'rel_smonth']"/>
                            </xsl:call-template>
                                &#160;
                            <xsl:call-template name="check_empty_string">
                                <xsl:with-param name="value" select="r:data[@name = 'rel_syear']"/>
                            </xsl:call-template>
                        </xsl:if>
                        <xsl:if test="r:data[@name = 'rel_sday'] = '' and r:data[@name = 'rel_smonth'] = '' and r:data[@name = 'rel_syear'] = ''">
                            Not Provided
                        </xsl:if>
                    </td>
                </tr>
            </table>                        
            
        </xsl:if>
        
    </xsl:template>
    <!-- finish trade_reference_relationship -->

    <!-- start trade_reference_sponsor -->
    <xsl:template name="trade_reference_sponsor_template">
        
        <xsl:if test="@id = 'sponsor' and r:record">
            
            <p class="notice">To the best of our knowledge, we have the following info:</p>
            
            <table class="table">
                <tr>
                    <th class="caption" width="20px">No.</th>
                    <th class="caption">Name</th>
                    <th class="caption">New ID / Old ID</th>
                    <th class="caption">Type</th>
                    <th class="caption">Source</th>
                    <th class="caption">Date</th>
                </tr>
                
                <xsl:for-each select="r:record">
                    <tr>
                        <td width="20px">
                            <xsl:value-of select="@seq"/>.
                        </td>
                        <td>
                            <xsl:value-of select="r:data[@name = 'name']"/>
                        </td>
                        <td>
                            <xsl:if test="r:data[@name = 'nic_brno'] != '' and r:data[@name = 'ic_lcno'] = ''">
                                <xsl:call-template name="check_empty_string">
                                    <xsl:with-param name="value" select="r:data[@name = 'nic_brno']"/>
                                </xsl:call-template>

                            </xsl:if>
                            <xsl:if test="r:data[@name = 'nic_brno'] = '' and r:data[@name = 'ic_lcno'] != ''">
                                <xsl:call-template name="check_empty_string">
                                    <xsl:with-param name="value" select="r:data[@name = 'ic_lcno']"/>
                                </xsl:call-template>
                            </xsl:if>
                            <xsl:if test="r:data[@name = 'nic_brno'] != '' and r:data[@name = 'ic_lcno'] != ''">
                                <xsl:value-of select="r:data[@name = 'nic_brno']"/> / <xsl:value-of select="r:data[@name = 'ic_lcno']"/>
                            </xsl:if>
                        </td>
                        <td>
                            <xsl:value-of select="r:data[@name = 'type']"/>
                            <xsl:if test="r:data[@name = 'guarantor'] = '1'">
                                (GUARANTOR)
                            </xsl:if>
                        </td>
                        <td>
                            <xsl:call-template name="check_empty_string">
                                <xsl:with-param name="value" select="r:data[@name = 'source']"/>
                            </xsl:call-template>
                        </td>
                        <td>
                            <xsl:call-template name="check_empty_string">
                                <xsl:with-param name="value" select="r:data[@name = 'date']"/>
                            </xsl:call-template>
                        </td>
                    </tr>
                </xsl:for-each>
                
            </table>

        </xsl:if>
        
    </xsl:template>
    <!-- finish trade_reference_sponsor -->

    <!-- start trade_reference_account_status -->
    <xsl:template name="trade_reference_account_status_template">
        
        <xsl:if test="@id = 'account_status'">            
            
            <br/>
            
            <table class="table">
                <tr>
                    <td class="header caption text-left">
                        <xsl:value-of select="position() - 1"/>. Aging Information</td>
                    <td class="long-content" colspan="3">
                        <xsl:if test="r:data[@name = 'statement_date'] != ''">
                            As At Statement Date: <xsl:value-of select="r:data[@name = 'statement_date']"/>
                        </xsl:if>
                        <xsl:if test="r:data[@name = 'statement_date'] = ''">
                            Not Provided
                        </xsl:if>
                    </td>
                </tr>
                <tr>
                    <th class="caption">Name of Debtor</th>
                    <td class="long-content" colspan="3">
                        <xsl:value-of select="r:data[@name = 'debtor_name']"/>
                        <xsl:if test="r:data[@name = 'debtor_nic_brno'] != '' and r:data[@name = 'debtor_ic_lcno'] != ''">
                            (<xsl:value-of select="r:data[@name = 'debtor_nic_brno']"/> / <xsl:value-of select="r:data[@name = 'debtor_ic_lcno']"/>)
                        </xsl:if>
                        <xsl:if test="r:data[@name = 'debtor_nic_brno'] != '' and r:data[@name = 'debtor_ic_lcno'] = ''">
                            (<xsl:value-of select="r:data[@name = 'debtor_nic_brno']"/>)
                        </xsl:if>
                        <xsl:if test="r:data[@name = 'debtor_nic_brno'] = '' and r:data[@name = 'debtor_ic_lcno'] != ''">
                            (<xsl:value-of select="r:data[@name = 'debtor_ic_lcno']"/>)
                        </xsl:if>
                    </td>
                </tr>
                <tr>
                    <th class="caption">Credit Terms</th>
                    <td class="short-content">
                        <xsl:if test="r:data[@name = 'account_term'] &gt; '0'">
                            <xsl:value-of select="r:data[@name = 'account_term']"/> Days
                        </xsl:if>
                        <xsl:if test="r:data[@name = 'account_term'] = '0'">
                            CASH
                        </xsl:if>
                    </td>
                    <th class="caption">Credit Limit</th>
                    <td class="short-content">
                        <xsl:call-template name="check_empty_string">
                            <xsl:with-param name="value" select="r:data[@name = 'account_limit']"/>
                        </xsl:call-template>
                    </td>
                </tr>
                <tr>
                    <th class="caption">Account Status</th>
                    <td class="long-content" colspan="3">
                        <xsl:call-template name="check_empty_string">
                            <xsl:with-param name="value" select="r:data[@name = 'account_status']"/>
                        </xsl:call-template>
                    </td>
                </tr>
                <tr>
                    <th class="caption">Address</th>
                    <td class="long-content" colspan="3">
                        <xsl:call-template name="check_empty_string">
                            <xsl:with-param name="value" select="r:data[@name = 'address']"/>
                        </xsl:call-template>
                    </td>
                </tr>
                <tr>
                    <th class="caption">Nature of Debt</th>
                    <td class="long-content" colspan="3">
                        <xsl:value-of select="r:data[@name = 'debt_type']"/>
                    </td>
                </tr>
            </table>
            
            <br/>    
                        
            <table class="table-no-background-color">
                <tr class="table-no-background-color-text-center">
                    <th colspan="2" style="background-color: #C5E0B3;">Outstanding</th>
                    <th colspan="5" style="background-color: #FFCCFF;">Default</th>
                </tr>
                <tr class="table-no-background-color-text-center">
                    <th style="background-color: #C5E0B3;">0-30 Days</th>
                    <th style="background-color: #C5E0B3;">31-60 Days</th>
                    <th style="background-color: #FFCCFF;">61-90 Days</th>
                    <th style="background-color: #FFCCFF;">91-120 Days</th>
                    <th style="background-color: #FFCCFF;">121-150 Days</th>
                    <th style="background-color: #FFCCFF;">151-180 Days</th>
                    <th style="background-color: #FFCCFF;">> 180 Days</th>
                </tr>
                <tr class="table-text-right">
                    <xsl:for-each select="r:data[@name = 'age']/r:item">
                        <td>
                            <xsl:call-template name="check_empty_number">
                                <xsl:with-param name="number" select="."/>
                            </xsl:call-template>
                        </td>
                    </xsl:for-each>
                </tr>
            </table>
                
            <p class="notice">The above was accurate as at the statement date indicate above.</p>
            
            <br/>    
                        
            <table class="table">
                <tr>
                    <td class="header caption text-left">Account Conduct</td>
                </tr>
            </table>
            <table class="table-text-center">                
                <tr>
                    <th>Not Provided</th>
                    <th>Excellent</th>
                    <th>Good</th>
                    <th>Satisfactory</th>
                    <th>Unsatisfactory</th>
                </tr>
                <tr>
                    <td>
                        <xsl:if test="r:data[@name = 'account_rating'] = '0'">
                            X
                        </xsl:if>
                    </td>
                    <td>
                        <xsl:if test="r:data[@name = 'account_rating'] = '1'">
                            X
                        </xsl:if>
                    </td>
                    <td>
                        <xsl:if test="r:data[@name = 'account_rating'] = '2'">
                            X
                        </xsl:if>
                    </td>
                    <td>
                        <xsl:if test="r:data[@name = 'account_rating'] = '3'">
                            X
                        </xsl:if>
                    </td>
                    <td>
                        <xsl:if test="r:data[@name = 'account_rating'] = '4'">
                            X
                        </xsl:if>
                    </td>
                </tr>
            </table>                                   
            
        </xsl:if>
        
    </xsl:template>
    <!-- finish trade_reference_account_status -->

    <!-- start trade_reference_return_cheque -->
    <xsl:template name="trade_reference_return_cheque_template">
        
        <xsl:if test="@id = 'return_cheque'">
        
            <br/>
            
            <table class="table">
                <tr>
                    <td class="header caption text-left" colspan="7">
                        <xsl:value-of select="position() - 1"/>. Returned Cheque History</td>
                </tr>
            </table>
        
        </xsl:if>
        
        <xsl:if test="@id = 'return_cheque' and r:record and @status != 'Not Provided'">
                
            <table class="table-text-center">
                <tr>
                    <th>Cheque No.</th>
                    <th>Account No.</th>
                    <th>Bank</th>
                    <th>Amount (RM)</th>
                    <th>Date Returned</th>
                    <th>Reason</th>
                    <th>Cheque Issuer</th>
                </tr>
                <xsl:for-each select="r:record">
                    <tr>
                        <td>
                            <xsl:call-template name="check_empty_string">
                                <xsl:with-param name="value" select="r:data[@name = 'cheque_no']"/>
                            </xsl:call-template>
                        </td>
                        <td>
                            <xsl:call-template name="check_empty_string">
                                <xsl:with-param name="value" select="r:data[@name = 'cheque_acc_no']"/>
                            </xsl:call-template>
                        </td>
                        <td>
                            <xsl:call-template name="check_empty_string">
                                <xsl:with-param name="value" select="r:data[@name = 'cheque_bank']"/>
                            </xsl:call-template>
                        </td>
                        <td>
                            <xsl:call-template name="check_empty_number">
                                <xsl:with-param name="number" select="r:data[@name = 'cheque_amount']"/>
                            </xsl:call-template>
                        </td>
                        <td>
                            <xsl:call-template name="check_empty_string">
                                <xsl:with-param name="value" select="r:data[@name = 'cheque_date']"/>
                            </xsl:call-template>
                        </td>
                        <td>
                            <xsl:call-template name="check_empty_string">
                                <xsl:with-param name="value" select="r:data[@name = 'cheque_reason']"/>
                            </xsl:call-template>
                        </td>
                        <td>
                            <xsl:call-template name="check_empty_string">
                                <xsl:with-param name="value" select="r:data[@name = 'cheque_issuer']"/>
                            </xsl:call-template>
                        </td>
                    </tr>
                </xsl:for-each>
            </table>
            
            <p class="notice">The above cheques were received for payment of amount owed but were returned unpaid for reasons indicated.</p>                       
            
        </xsl:if>
        
        <xsl:if test="@id = 'return_cheque' and @status = 'Not Provided'">
            <p class="info">No Information Available</p>
        </xsl:if>
        
    </xsl:template>
    <!-- finish trade_reference_return_cheque -->

    <!-- start trade_reference_legal_action -->
    <xsl:template name="trade_reference_legal_action_template">
        
        <xsl:if test="@id = 'legal_action'">
        
            <br/>
            
            <table class="table">
                <tr>
                    <td class="header caption text-left" colspan="2">
                        <xsl:value-of select="position() - 1"/>. Reminders / Letter of Demand for Payment / General Remarks</td>
                </tr>
            </table>
        
        </xsl:if>
        
        <xsl:if test="@id = 'legal_action' and r:record and @status != 'Not Provided'">
                
            <table class="table">
                <xsl:for-each select="r:record">
                    <tr>
                        <td class="caption text-left">
                            <xsl:value-of select="r:data[@name = 'title']"/>
                        </td>
                        <td>
                            <xsl:value-of select="r:data[@name = 'date']"/>
                            &#160;
                            <xsl:value-of select="r:data[@name = 'comment']"/>
                        </td>
                    </tr>
                </xsl:for-each>
            </table>
            
        </xsl:if>        
        
        <xsl:if test="@id = 'legal_action' and @status = 'Not Provided'">
            <p class="info">No Information Available</p>
        </xsl:if>
        
    </xsl:template>
    <!-- finish trade_reference_legal_action -->

    <!-- start trade_reference_cra_comment -->
    <xsl:template name="trade_reference_cra_comment_template">
		
        <xsl:if test="@id = 'cra_comment'">            
            
            <br/>
            
            <table class="table">
                <tr>
                    <td class="header caption text-left">
                        <xsl:value-of select="position() - 1"/>. CRA Comment</td>
                    <td class="long-content">
                        <xsl:if test="r:data[@name = 'cra_status_desc'] != ''">
                            <xsl:call-template name="check_empty_string">
                                <xsl:with-param name="value" select="r:data[@name = 'cra_status_desc']"/>
                            </xsl:call-template>
                        </xsl:if>
                        <xsl:if test="r:data[@name = 'cra_status_desc'] = ''">
                            No Information Available
                        </xsl:if>
                    </td>
                </tr>                    
            </table>            
            
        </xsl:if>
        
    </xsl:template>
    <!-- finish trade_reference_cra_comment -->

    <!-- start trade_reference_subject_comment -->
    <xsl:template name="trade_reference_subject_comment_template">
		
        <xsl:if test="@id = 'subject_comment'">            
            
            <br/>
            
            <table class="table">
                <tr>
                    <td class="header caption text-left">
                        <xsl:value-of select="position() - 1"/>. Subject's Comment</td>
                    <td class="long-content">
                        <xsl:if test="r:data[@name = 'subject_comment'] != ''">
                            <pre>
                                <xsl:call-template name="replace_string">
                                    <xsl:with-param name="value" select="r:data[@name = 'subject_comment']"/>
                                    <xsl:with-param name="replace" select="'               '"/>
                                    <xsl:with-param name="with" select="''"/>
                                </xsl:call-template>
                            </pre>
                        </xsl:if>
                        <xsl:if test="r:data[@name = 'subject_comment'] = ''">
                            No Information Available
                        </xsl:if>
                    </td>
                </tr>                    
            </table>                                   
            
        </xsl:if>
        
    </xsl:template>
    <!-- finish trade_reference_subject_comment -->

    <!-- start trade_reference_contact -->
    <xsl:template name="trade_reference_contact_template">
        <xsl:param name="referee"/>
        <xsl:param name="nature_of_business"/>
		
        <xsl:if test="@id = 'contact'">                    
                        
            <br/>
            
            <table class="table">
                <tr>
                    <td class="header caption text-left" colspan="2">
                        <xsl:value-of select="position() - 1"/>. Referee's Information</td>
                </tr>
                <tr>
                    <th class="caption">Referee</th>
                    <td>
                        <xsl:call-template name="check_empty_string">
                            <xsl:with-param name="value" select="$referee"/>
                        </xsl:call-template>
                    </td>
                </tr>
                <tr>
                    <th class="caption">Contact Name</th>
                    <td class="long-content">
                        <xsl:call-template name="check_empty_string">
                            <xsl:with-param name="value" select="r:data[@name = 'name']"/>
                        </xsl:call-template>
                    </td>
                </tr>
                <tr>
                    <th class="caption">Address</th>
                    <td class="long-content">
                        <xsl:call-template name="check_empty_string">
                            <xsl:with-param name="value" select="r:data[@name = 'address']"/>
                        </xsl:call-template>
                    </td>
                </tr>
                <tr>

                    <th class="caption">
                        <xsl:if test="r:data[@name = 'type_code'] = '1' or r:data[@name = 'type_code'] >= '4'">
                            Telephone
                        </xsl:if>
                        <xsl:if test="r:data[@name = 'type_code'] = '2'">
                            Fax
                        </xsl:if>
                        <xsl:if test="r:data[@name = 'type_code'] = '3'">
                            Email
                        </xsl:if>
                    </th>
                    <td class="long-content">
                        <xsl:if test="r:data[@name = 'type_code'] = '1' or r:data[@name = 'type_code'] >= '4'">
                            <xsl:call-template name="check_empty_string">
                                <xsl:with-param name="value" select="r:data[@name = 'tel_no']"/>
                            </xsl:call-template>
                        </xsl:if>
                        <xsl:if test="r:data[@name = 'type_code'] = '2'">
                            <xsl:call-template name="check_empty_string">
                                <xsl:with-param name="value" select="r:data[@name = 'fax_no']"/>
                            </xsl:call-template>
                        </xsl:if>
                        <xsl:if test="r:data[@name = 'type_code'] = '3'">
                            <xsl:call-template name="check_empty_string">
                                <xsl:with-param name="value" select="r:data[@name = 'email']"/>
                            </xsl:call-template>
                        </xsl:if>
                    </td>
                </tr>
                <tr>
                    <th class="caption">Nature of Business</th>
                    <td>
                        <xsl:call-template name="check_empty_string">
                            <xsl:with-param name="value" select="$nature_of_business"/>
                        </xsl:call-template>
                    </td>
                </tr>
                <tr>
                    <th class="caption">Remarks</th>
                    <td></td>
                </tr>
            </table>                        
            
        </xsl:if>
        
    </xsl:template>
    <!-- finish trade_reference_contact -->
    
    <!-- start etr_plus_account_profile_template -->
    <xsl:template name="etr_plus_account_profile_template">
    	<br/>
    	<table class="table">
    		<tr>
	    		<td colspan="11" style="background-color: #666666; color:#FFFFFF"><strong>1a. Account Payment Profile</strong> - Varying payments (Static snapshot)</td>
	    	</tr>
	    	<xsl:choose>
	    	<xsl:when test="//r:enquiry/r:section_etr_plus/r:etrPlusAccountProfileList/r:etrPlusAccountProfile">
	    	<tr>
	    		<td class="header text-center" rowspan="2" style="font-size:7px">Industry Segment</td>
	    		<td class="header text-center" rowspan="2" style="font-size:7px">Account No</td>
	    		<td class="header text-center" rowspan="2" style="font-size:7px">Payment Term</td>
	    		<td class="header text-center" rowspan="2" nowrap="As at Due" style="font-size:7px">As at Due</td>
	    		<td class="header text-center" rowspan="2" nowrap="Amount Due" style="font-size:7px">Amount Due</td>
	    		<td class="header text-center" rowspan="2" nowrap="Within Term" style="font-size:7px">Within Term</td>
	    		<td class="header text-center" colspan="5" style="font-size:7px">Payment Overdue</td>
	        </tr>
	        <tr>
	        	<td class="header text-center" nowrap="1-30 days" style="font-size:7px">1-30 days</td>
	        	<td class="header text-center" nowrap="31-60 days" style="font-size:7px">31-60 days</td>
	        	<td class="header text-center" nowrap="61-90 days" style="font-size:7px">61-90 days</td>
	        	<td class="header text-center" nowrap="91-120 days" style="font-size:7px">91-120 days</td>
	        	<td class="header text-center" nowrap=">120 days" style="font-size:7px">>120 days</td>
	        </tr>
	        
	        
		        <xsl:for-each select="//r:enquiry/r:section_etr_plus/r:etrPlusAccountProfileList/r:etrPlusAccountProfile">
		        	<tr>
		        		<td class="text-center" style="font-size:7px">
                        	<xsl:value-of select="r:industry"/>
		        		</td>
		        		<td class="text-center" style="font-size:7px">
                        	<xsl:value-of select="r:account"/>
		        		</td>
		        		<td class="text-center" style="font-size:7px">
                        	<xsl:value-of select="r:paymentTerm"/>
                        	<xsl:text>days</xsl:text>
		        		</td>
		        		<td class="text-center" style="font-size:7px">
		        			<xsl:if test="r:asAtDate != ''">
		        				<xsl:variable name="dt" select="r:asAtDate"/>
							  		<xsl:value-of select="concat(
									    substring($dt,9,2),'/',
									    substring($dt,6,2),'/',
									    substring($dt,1,4))" />
		        			</xsl:if>
		        		</td>
		        		<td class="text-center" style="font-size:7px">
		        			<xsl:if test="r:amountDue != ''">
                        		<xsl:value-of select="format-number(r:amountDue, '#,##0')"/>
                        	</xsl:if>
		        		</td>
		        		<xsl:choose>
				        		<xsl:when test="r:withinTerm = 'S' or r:withinTerm = 's'">
									<td class="text-center" style="background-color: #A9D18E; font-size:7px">
				        				<xsl:text>S</xsl:text>
				        			</td>
								</xsl:when>
								<xsl:when test="r:withinTerm = 'W' or r:withinTerm = 'w'">
									<td class="text-center" style="background-color: #FE7676; font-size:7px">
				        				<xsl:text>W</xsl:text>
				        			</td>
								</xsl:when>
								<xsl:otherwise>
		                        	<td class="text-center" style="font-size:7px">
			                        	<xsl:value-of select="r:withinTerm"/>
			                        	<xsl:text>%</xsl:text>
					        		</td>
		                        </xsl:otherwise>
	                    </xsl:choose>
		        		<td class="text-center" style="font-size:7px">
                        	<xsl:value-of select="r:age1to30"/>
                        	<xsl:text>%</xsl:text>
		        		</td>
		        		<td class="text-center" style="font-size:7px">
                        	<xsl:value-of select="r:age31to60"/>
                        	<xsl:text>%</xsl:text>
		        		</td>
		        		<td class="text-center" style="font-size:7px">
                        	<xsl:value-of select="r:age61to90"/>
                        	<xsl:text>%</xsl:text>
		        		</td>
		        		<td class="text-center" style="font-size:7px">
                        	<xsl:value-of select="r:age91to120"/>
                        	<xsl:text>%</xsl:text>
		        		</td>
		        		<td class="text-center" style="font-size:7px">
                        	<xsl:value-of select="r:ageOver120"/>
                        	<xsl:text>%</xsl:text>
		        		</td>
		        	</tr>
		        </xsl:for-each>
	        </xsl:when>
            <xsl:otherwise>
            	<td class="vertical-center">
                		No Information Available
                </td>
            </xsl:otherwise>
            </xsl:choose>
        </table>      
    </xsl:template>
    <!-- finish etr_plus_account_profile_template -->
    
    <!-- start etr_plus_payment_history_template -->
    <xsl:template name="etr_plus_payment_history_template">
    	<table class="table">
    		<tr>
	    		<td colspan="16" style="background-color: #666666; color:#FFFFFF"><strong>1b. Payment Trend</strong> - Varying payments (Last 12 months)</td>
	    	</tr>
	    	<xsl:choose>
	    	<xsl:when test="//r:enquiry/r:section_etr_plus/r:etrPlusPaymentHistoryList/r:etrPlusPaymentHistory">
	        <tr>
	        	<td class="header text-center" style="font-size:7px">Industry Segment</td>
	        	<td class="header text-center" style="font-size:7px">Account No</td>
	        	<td class="header text-center" style="font-size:7px">Payment Term</td>
	        	<td class="header text-center" style="font-size:7px">Capacity</td>
				<td class="header text-center" style="font-size:7px">
                    <xsl:call-template name="getPastMonthAndYearOnly_section_e">
                        <xsl:with-param name="datestr" select="ancestor::r:report/r:enq_report/r:header/r:enq_date"/>
                        <xsl:with-param name="past">1</xsl:with-param>
                    </xsl:call-template>
                </td>
                <td class="header text-center" style="font-size:7px">
                    <xsl:call-template name="getPastMonthAndYearOnly_section_e">
                        <xsl:with-param name="datestr" select="ancestor::r:report/r:enq_report/r:header/r:enq_date"/>
                        <xsl:with-param name="past">2</xsl:with-param>
                    </xsl:call-template>
                </td>
                <td class="header text-center" style="font-size:7px">
                    <xsl:call-template name="getPastMonthAndYearOnly_section_e">
                        <xsl:with-param name="datestr" select="ancestor::r:report/r:enq_report/r:header/r:enq_date"/>
                        <xsl:with-param name="past">3</xsl:with-param>
                    </xsl:call-template>
                </td>
                <td class="header text-center" style="font-size:7px">
                    <xsl:call-template name="getPastMonthAndYearOnly_section_e">
                        <xsl:with-param name="datestr" select="ancestor::r:report/r:enq_report/r:header/r:enq_date"/>
                        <xsl:with-param name="past">4</xsl:with-param>
                    </xsl:call-template>
                </td>
                <td class="header text-center" style="font-size:7px">
                    <xsl:call-template name="getPastMonthAndYearOnly_section_e">
                        <xsl:with-param name="datestr" select="ancestor::r:report/r:enq_report/r:header/r:enq_date"/>
                        <xsl:with-param name="past">5</xsl:with-param>
                    </xsl:call-template>
                </td>
                <td class="header text-center" style="font-size:7px">
                    <xsl:call-template name="getPastMonthAndYearOnly_section_e">
                        <xsl:with-param name="datestr" select="ancestor::r:report/r:enq_report/r:header/r:enq_date"/>
                        <xsl:with-param name="past">6</xsl:with-param>
                    </xsl:call-template>
                </td>
                <td class="header text-center" style="font-size:7px">
                    <xsl:call-template name="getPastMonthAndYearOnly_section_e">
                        <xsl:with-param name="datestr" select="ancestor::r:report/r:enq_report/r:header/r:enq_date"/>
                        <xsl:with-param name="past">7</xsl:with-param>
                    </xsl:call-template>
                </td>
                <td class="header text-center" style="font-size:7px">
                    <xsl:call-template name="getPastMonthAndYearOnly_section_e">
                        <xsl:with-param name="datestr" select="ancestor::r:report/r:enq_report/r:header/r:enq_date"/>
                        <xsl:with-param name="past">8</xsl:with-param>
                    </xsl:call-template>
                </td>
                <td class="header text-center" style="font-size:7px">
                    <xsl:call-template name="getPastMonthAndYearOnly_section_e">
                        <xsl:with-param name="datestr" select="ancestor::r:report/r:enq_report/r:header/r:enq_date"/>
                        <xsl:with-param name="past">9</xsl:with-param>
                    </xsl:call-template>
                </td>
                <td class="header text-center" style="font-size:7px">
                    <xsl:call-template name="getPastMonthAndYearOnly_section_e">
                        <xsl:with-param name="datestr" select="ancestor::r:report/r:enq_report/r:header/r:enq_date"/>
                        <xsl:with-param name="past">10</xsl:with-param>
                    </xsl:call-template>
                </td>
                <td class="header text-center" style="font-size:7px">
                    <xsl:call-template name="getPastMonthAndYearOnly_section_e">
                        <xsl:with-param name="datestr" select="ancestor::r:report/r:enq_report/r:header/r:enq_date"/>
                        <xsl:with-param name="past">11</xsl:with-param>
                    </xsl:call-template>
                </td>
                <td class="header text-center" style="font-size:7px">
                    <xsl:call-template name="getPastMonthAndYearOnly_section_e">
                        <xsl:with-param name="datestr" select="ancestor::r:report/r:enq_report/r:header/r:enq_date"/>
                        <xsl:with-param name="past">12</xsl:with-param>
                    </xsl:call-template>
                </td>
			</tr>
	        <xsl:for-each select="//r:enquiry/r:section_etr_plus/r:etrPlusPaymentHistoryList/r:etrPlusPaymentHistory">
	        	<tr>
	        		<td class="text-center" style="font-size:7px">
                        <xsl:value-of select="r:industry"/>
		        	</td>
		        	<td class="text-center" style="font-size:7px">
                        <xsl:value-of select="r:account"/>
		        	</td>
		        	<td class="text-center" style="font-size:7px">
                        <xsl:value-of select="r:paymentTerm"/>
                        <xsl:text>days</xsl:text>
		        	</td>
		        	<td class="text-center" nowrap="r:capacity" style="font-size:7px">
                        <xsl:value-of select="r:capacity"/>
		        	</td>
		        		<xsl:variable name="countMonth">
					    	<xsl:value-of select="count(r:monthDetailsList/r:monthDetails)"/>
					    </xsl:variable>
					    <xsl:variable name="remainingMonth">
			                <xsl:value-of select="12 - $countMonth"/>
			            </xsl:variable>
		        		<xsl:for-each select="r:monthDetailsList/r:monthDetails">
		        				<xsl:if test="$countMonth = '12'">
						        	<xsl:choose>
						        		<xsl:when test="(substring(r:payment,2) = '+') or r:payment &gt; '0' ">
											<td class="text-center" style="font-size:7px; background-color: #FE7676;">
												<xsl:value-of select="r:payment"/>
											</td>
										</xsl:when>
										<xsl:when test="r:payment = 'W' or r:payment = 'w' ">
											<td class="text-center" style="font-size:7px; background-color: #FE7676;">
												<xsl:text>W</xsl:text>
											</td>
										</xsl:when>
										<xsl:when test="r:payment = 'S' or r:payment = 's'">
											<td class="text-center" style="font-size:7px; background-color: #A9D18E;">
												<xsl:text>S</xsl:text>
											</td>
										</xsl:when>
										<xsl:when test="r:payment = '0' or r:payment = '-'">
											<td class="text-center" style="font-size:7px;">
												<xsl:value-of select="r:payment"/>
											</td>
										</xsl:when>
										<xsl:otherwise>
					                        <td style="background-color: #DDDDDD;">
											</td>
					                    </xsl:otherwise>
				                    </xsl:choose>
			                    </xsl:if>
			                    <xsl:if test="$countMonth &lt; '12'">
			                    	<xsl:variable name="i" select="position()">
					    			</xsl:variable>
			                    	<xsl:choose>
						        		<xsl:when test="(substring(r:payment,2) = '+') or r:payment &gt; '0' ">
											<td class="text-center" style="font-size:7px; background-color: #FE7676;">
												<xsl:value-of select="r:payment"/>
											</td>
										</xsl:when>
										<xsl:when test="r:payment = 'W' or r:payment = 'w'">
											<td class="text-center" style="font-size:7px; background-color: #FE7676;">
												<xsl:text>W</xsl:text>
											</td>
										</xsl:when>
										<xsl:when test="r:payment = 'S' or r:payment = 's'">
											<td class="text-center" style="font-size:7px; background-color: #A9D18E;">
												<xsl:text>S</xsl:text>
											</td>
										</xsl:when>
										<xsl:when test="r:payment = '0' or r:payment = '-'">
											<td class="text-center" style="font-size:7px;">
												<xsl:value-of select="r:payment"/>
											</td>
										</xsl:when>
										<xsl:otherwise>
					                        <td style="background-color: #DDDDDD;">
											</td>
					                    </xsl:otherwise>
				                    </xsl:choose>
				                    <xsl:if test="12 - $remainingMonth = $i">
				                        <xsl:choose>
				                        	<xsl:when test="$remainingMonth = '1'">
				                        		<td style="background-color: #DDDDDD;">
												</td>
				                        	</xsl:when>
				                        	<xsl:when test="$remainingMonth = '2'">
				                        		<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
				                        	</xsl:when>
				                        	<xsl:when test="$remainingMonth = '3'">
				                        		<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
				                        	</xsl:when>
				                        	<xsl:when test="$remainingMonth = '4'">
				                        		<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
				                        	</xsl:when>
				                        	<xsl:when test="$remainingMonth = '5'">
				                        		<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
				                        	</xsl:when>
				                        	<xsl:when test="$remainingMonth = '6'">
				                        		<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
				                        	</xsl:when>
				                        	<xsl:when test="$remainingMonth = '7'">
				                        		<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
				                        	</xsl:when>
				                        	<xsl:when test="$remainingMonth = '8'">
				                        		<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
				                        	</xsl:when>
				                        	<xsl:when test="$remainingMonth = '9'">
				                        		<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
				                        	</xsl:when>
				                        	<xsl:when test="$remainingMonth = '10'">
				                        		<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
				                        	</xsl:when>
				                        	<xsl:when test="$remainingMonth = '11'">
				                        		<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
				                        	</xsl:when>
				                        </xsl:choose>
				                	</xsl:if>
			                    </xsl:if>
						</xsl:for-each>
	        	</tr>
	        </xsl:for-each>
	        <tr>
	    		<td class="caption" style="font-size:7px;" nowrap="caption" colspan="16">
	    		<br/>
		    		 <strong>Remark Legend:</strong>
		    		 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
		    		 3 61-90 Days Overdue
		    		 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
		    		 Personal - Transaction incurred by Subject
		    		 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
		    		 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
		    		 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
		    		 <strong>Status</strong>
		    		 <br/>
		    		 - No Data
		    		 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
		    		 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
		    		 4 91-120 Days Overdue
		    		 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
		    		 Non-Personal - Transaction where the Subject is a Guarantor / Joint Owner
		    		 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
		    		 S - Fully Settled
		    		 <br/>
		    		 0 No Overdue
		    		 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
		    		 &#160;&#160;
		    		 5 121-150 Days Overdue
		    		 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
		    		 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
		    		 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
		    		 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
		    		 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
		    		 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
		    		 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
		    		 W - Written Off Bad Debt
		    		 <br/>
		    		 1 1-30 Days Overdue
		    		 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
		    		 6 151-180 Days Overdue
		    		 <br/>
		    		 2 31-60 Days Overdue
		    		 &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
		    		 6+ >180 Days Overdue
		    		 <br/><br/>
	    		</td>
	    	</tr>
	    	</xsl:when>
            <xsl:otherwise>
            	<td class="vertical-center">
                		No Information Available
                </td>
            </xsl:otherwise>
            </xsl:choose>
        </table>      
    </xsl:template>
    <!-- finish etr_plus_payment_history_template -->
    
    <!-- start non_blank_lender_credit_information_template -->
    <xsl:template name="non_blank_lender_credit_information_template">
    
    	<xsl:variable name="currYear">
            <xsl:call-template name="getYearOnly_e">
                <xsl:with-param name="datestr" select="ancestor::r:report/r:enq_report/r:header/r:enq_date"/>
            </xsl:call-template>
        </xsl:variable>
        
        <table class="table-ccris">
            <tr>
                <td colspan="27" style="background-color: #666666; color:#FFFFFF; text-align: left; font-size:10.6px; padding-left: 2%; padding-top: 0.5%; padding-bottom: 0.5%">
                <strong>2. Non-Bank Lender Credit Information</strong> - Fixed Instalments (Last 12 months)</td>		
            </tr>
	    	<xsl:if test="//r:enquiry/r:section_etr_plus/r:etrPlusNBLHistory/r:etrPlusNBLCreditHistoryList/r:etrPlusNBLCreditHistory">
            <tr>
                <th width="2%" style="background-color: #666666; color:#FFFFFF">No.</th>
                <th width="8%" style="background-color: #666666; color:#FFFFFF">Aprv date</th>
                <th width="2%" style="background-color: #666666; color:#FFFFFF">Capacity</th>
                <th width="6%" style="text-align: left; padding-left: 1%; background-color: #666666; color:#FFFFFF">Lender Type</th>
                <th width="5%" style="background-color: #666666; color:#FFFFFF">Facility</th>
                <th width="8%" style="background-color: #666666; color:#FFFFFF">Limit<br/>(RM)</th>
                <th width="8%" style="text-align: left; padding-left: 1%; background-color: #666666; color:#FFFFFF">Instalment Amount <br/> (RM) </th>
                <th width="8%" style="text-align: left; padding-left: 1%; background-color: #666666; color:#FFFFFF">Intalment Tenor<br/>(MTH)</th>
                <th width="8%" style="text-align: left; padding-left: 1%; background-color: #666666; color:#FFFFFF">Date<br/>Balance<br/>Updated</th>
                <th width="4%" style="text-align: left; background-color: #666666; color:#FFFFFF">Total Outstanding <br/>Balance<br/>(RM)</th>
                <th width="4%" style="text-align: left; background-color: #666666; color:#FFFFFF">Prin Repymt Term</th>
                <th width="4%" style="text-align: center; background-color: #666666; color:#FFFFFF">Col Type</th>
                <th width="24%" colspan="12" style="text-align: left; padding-left: 1%; background-color: #666666; color:#FFFFFF">Conduct Of Account For Last 12 Months</th>
                <th width="5%" style="background-color: #666666; color:#FFFFFF">Status</th>
                <th width="7%" style="text-align: left; background-color: #666666; color:#FFFFFF">Date<br/>Status<br/>Updated</th>
            </tr>
            <tr>
            	<th colspan="12" style="text-align: left; padding-left: 1%; padding-top: 2%; background-color: #666666; color:#FFFFFF" rowspan="2">OUTSTANDING CREDIT</th>
                <th width="12%" colspan="6" style="text-align: left; padding-left: 1%; border-right-width: 0; background-color: #666666; color:#FFFFFF">
                    <xsl:copy-of select="$currYear"/>
                </th>
                <th width="12%" colspan="6" style="text-align: right; padding-right: 1%; border-left-width: 0; background-color: #666666; color:#FFFFFF">
                    <xsl:copy-of select="$currYear - 1"/>
                </th>
                <th style="background-color: #666666; color:#FFFFFF" rowspan="2"></th>
                <th style="background-color: #666666; color:#FFFFFF" rowspan="2"></th>
            </tr>
            <tr>
                <th width="2%" style="background-color: #666666; color:#FFFFFF">
                    <xsl:call-template name="getPastMonthFirstLetter">
                        <xsl:with-param name="datestr" select="ancestor::r:report/r:enq_report/r:header/r:enq_date"/>
                        <xsl:with-param name="past">1</xsl:with-param>
                    </xsl:call-template>
                </th>
                <th width="2%" style="background-color: #666666; color:#FFFFFF">
                    <xsl:call-template name="getPastMonthFirstLetter">
                        <xsl:with-param name="datestr" select="ancestor::r:report/r:enq_report/r:header/r:enq_date"/>
                        <xsl:with-param name="past">2</xsl:with-param>
                    </xsl:call-template>
                </th>
                <th width="2%" style="background-color: #666666; color:#FFFFFF">
                    <xsl:call-template name="getPastMonthFirstLetter">
                        <xsl:with-param name="datestr" select="ancestor::r:report/r:enq_report/r:header/r:enq_date"/>
                        <xsl:with-param name="past">3</xsl:with-param>
                    </xsl:call-template>
                </th>
                <th width="2%" style="background-color: #666666; color:#FFFFFF">
                    <xsl:call-template name="getPastMonthFirstLetter">
                        <xsl:with-param name="datestr" select="ancestor::r:report/r:enq_report/r:header/r:enq_date"/>
                        <xsl:with-param name="past">4</xsl:with-param>
                    </xsl:call-template>
                </th>
                <th width="2%" style="background-color: #666666; color:#FFFFFF">
                    <xsl:call-template name="getPastMonthFirstLetter">
                        <xsl:with-param name="datestr" select="ancestor::r:report/r:enq_report/r:header/r:enq_date"/>
                        <xsl:with-param name="past">5</xsl:with-param>
                    </xsl:call-template>
                </th>
                <th width="2%" style="background-color: #666666; color:#FFFFFF">
                    <xsl:call-template name="getPastMonthFirstLetter">
                        <xsl:with-param name="datestr" select="ancestor::r:report/r:enq_report/r:header/r:enq_date"/>
                        <xsl:with-param name="past">6</xsl:with-param>
                    </xsl:call-template>
                </th>
                <th width="2%" style="background-color: #666666; color:#FFFFFF">
                    <xsl:call-template name="getPastMonthFirstLetter">
                        <xsl:with-param name="datestr" select="ancestor::r:report/r:enq_report/r:header/r:enq_date"/>
                        <xsl:with-param name="past">7</xsl:with-param>
                    </xsl:call-template>
                </th>
                <th width="2%" style="background-color: #666666; color:#FFFFFF">
                    <xsl:call-template name="getPastMonthFirstLetter">
                        <xsl:with-param name="datestr" select="ancestor::r:report/r:enq_report/r:header/r:enq_date"/>
                        <xsl:with-param name="past">8</xsl:with-param>
                    </xsl:call-template>
                </th>
                <th width="2%" style="background-color: #666666; color:#FFFFFF">
                    <xsl:call-template name="getPastMonthFirstLetter">
                        <xsl:with-param name="datestr" select="ancestor::r:report/r:enq_report/r:header/r:enq_date"/>
                        <xsl:with-param name="past">9</xsl:with-param>
                    </xsl:call-template>
                </th>
                <th width="2%" style="background-color: #666666; color:#FFFFFF">
                    <xsl:call-template name="getPastMonthFirstLetter">
                        <xsl:with-param name="datestr" select="ancestor::r:report/r:enq_report/r:header/r:enq_date"/>
                        <xsl:with-param name="past">10</xsl:with-param>
                    </xsl:call-template>
                </th>
                <th width="2%" style="background-color: #666666; color:#FFFFFF">
                    <xsl:call-template name="getPastMonthFirstLetter">
                        <xsl:with-param name="datestr" select="ancestor::r:report/r:enq_report/r:header/r:enq_date"/>
                        <xsl:with-param name="past">11</xsl:with-param>
                    </xsl:call-template>
                </th>
                <th width="2%" style="background-color: #666666; color:#FFFFFF">
                    <xsl:call-template name="getPastMonthFirstLetter">
                        <xsl:with-param name="datestr" select="ancestor::r:report/r:enq_report/r:header/r:enq_date"/>
                        <xsl:with-param name="past">12</xsl:with-param>
                    </xsl:call-template>
                </th>
            </tr>
			<tr>
				<xsl:for-each select="//r:enquiry/r:section_etr_plus/r:etrPlusNBLHistory/r:etrPlusNBLCreditHistoryList/r:etrPlusNBLCreditHistory">
		        	<tr>
		        		<td>
		        			<xsl:value-of select="r:number"/>
		        		</td>
		        		<td>
		        			<xsl:if test="r:approvalDate != ''">
		        				<xsl:variable name="dt" select="r:approvalDate"/>
									  <xsl:value-of select="concat(
									    substring($dt,9,2),'/',
									    substring($dt,6,2),'/',
									    substring($dt,1,4))" />
		        			</xsl:if>
		        		</td>
		        		<xsl:choose>
				        	<xsl:when test="r:capacity = 'P'">
								<td>
				        			P
				        		</td>
							</xsl:when>
							<xsl:when test="r:capacity = 'NP'">
								<td>
				        			NP
				        		</td>
							</xsl:when>
	                    </xsl:choose>
		        		<td>
		        			<xsl:value-of select="r:lenderType"/>
		        		</td>
		        		<td>
		        			<xsl:value-of select="r:facility"/>
		        		</td>
		        		<td>
		        			<xsl:if test="r:limit != ''">
		        				<xsl:value-of select="format-number(r:limit, '#,##0')"/>
		        			</xsl:if>
		        		</td>
		        		<td>
		        			<xsl:if test="r:installmentAmount != ''">
		        				<xsl:value-of select="format-number(r:installmentAmount, '#,##0')"/>
		        			</xsl:if>
		        		</td>
		        		<td>
		        			<xsl:value-of select="r:tenure"/>
		        		</td>
		        		<td>
		        			<xsl:if test="r:dateBalanceUpdated != ''">
		        				<xsl:variable name="dt" select="r:dateBalanceUpdated"/>
								 <xsl:value-of select="concat(
								    substring($dt,9,2),'/',
								    substring($dt,6,2),'/',
								    substring($dt,1,4))" />
		        			</xsl:if>
		        		</td>
		        		<td>
		        			<xsl:if test="r:totalOutStandingBalance != ''">
		        				<xsl:value-of select="format-number(r:totalOutStandingBalance, '#,##0')"/>
		        			</xsl:if>
		        		</td>
		        		<td>
		        			<xsl:value-of select="r:principalRepaymentTerm"/>
		        		</td>
		        		<td>
		        			<xsl:value-of select="r:collateralType"/>
		        		</td>
		        		<xsl:variable name="countMonth">
							<xsl:value-of select="count(r:monthDetailsList/r:monthDetails)"/>
						</xsl:variable>
						<xsl:variable name="remainingMonth">
					        <xsl:value-of select="12 - $countMonth"/>
					    </xsl:variable>
		        		<xsl:for-each select="r:monthDetailsList/r:monthDetails">
	                    	<xsl:if test="$countMonth = '12'">
						    	<xsl:choose>
						        	<xsl:when test="(substring(r:payment,2) = '+') or r:payment &gt; '0' ">
										<td class="text-center" style="font-size:7px; background-color: #FE7676;">
											<xsl:value-of select="r:payment"/>
										</td>
									</xsl:when>
									<xsl:when test="r:payment = 'W' or r:payment = 'w'">
										<td class="text-center" style="font-size:7px; background-color: #FE7676;">
											<xsl:text>W</xsl:text>
										</td>
									</xsl:when>
									<xsl:when test="r:payment = 'S' or r:payment = 's'">
										<td class="text-center" style="font-size:7px; background-color: #A9D18E;">
											<xsl:text>S</xsl:text>
										</td>
									</xsl:when>
									<xsl:when test="contains(r:payment,'-') or r:payment = '0'">
										<td class="text-center" style="font-size:7px;">
											<xsl:value-of select="r:payment"/>
										</td>
									</xsl:when>
									<xsl:otherwise>
				                        <td style="background-color: #DDDDDD;">
										</td>
				                    </xsl:otherwise>
				            	</xsl:choose>
			            	</xsl:if>
			            	<xsl:if test="$countMonth &lt; '12'">
			                    	<xsl:variable name="i" select="position()">
					    			</xsl:variable>
			                    	<xsl:choose>
						        		<xsl:when test="(substring(r:payment,2) = '+') or r:payment &gt; '0' ">
											<td class="text-center" style="font-size:7px; background-color: #FE7676;">
												<xsl:value-of select="r:payment"/>
											</td>
										</xsl:when>
										<xsl:when test="r:payment = 'W' or r:payment = 'w'">
											<td class="text-center" style="font-size:7px; background-color: #FE7676;">
												<xsl:text>W</xsl:text>
											</td>
										</xsl:when>
										<xsl:when test="r:payment = 'S' or r:payment = 's'">
											<td class="text-center" style="font-size:7px; background-color: #A9D18E;">
												<xsl:text>S</xsl:text>
											</td>
										</xsl:when>
										<xsl:when test="contains(r:payment,'-') or r:payment = '0'">
											<td class="text-center" style="font-size:7px;">
												<xsl:value-of select="r:payment"/>
											</td>
										</xsl:when>
										<xsl:otherwise>
					                        <td style="background-color: #DDDDDD;">
											</td>
					                    </xsl:otherwise>
				                    </xsl:choose>
				                    <xsl:if test="12 - $remainingMonth = $i">
				                        <xsl:choose>
				                        	<xsl:when test="$remainingMonth = '1'">
				                        		<td style="background-color: #DDDDDD;">
												</td>
				                        	</xsl:when>
				                        	<xsl:when test="$remainingMonth = '2'">
				                        		<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
				                        	</xsl:when>
				                        	<xsl:when test="$remainingMonth = '3'">
				                        		<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
				                        	</xsl:when>
				                        	<xsl:when test="$remainingMonth = '4'">
				                        		<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
				                        	</xsl:when>
				                        	<xsl:when test="$remainingMonth = '5'">
				                        		<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
				                        	</xsl:when>
				                        	<xsl:when test="$remainingMonth = '6'">
				                        		<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
				                        	</xsl:when>
				                        	<xsl:when test="$remainingMonth = '7'">
				                        		<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
				                        	</xsl:when>
				                        	<xsl:when test="$remainingMonth = '8'">
				                        		<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
				                        	</xsl:when>
				                        	<xsl:when test="$remainingMonth = '9'">
				                        		<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
				                        	</xsl:when>
				                        	<xsl:when test="$remainingMonth = '10'">
				                        		<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
				                        	</xsl:when>
				                        	<xsl:when test="$remainingMonth = '11'">
				                        		<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
												<td style="background-color: #DDDDDD;">
												</td>
				                        	</xsl:when>
				                        </xsl:choose>
				                	</xsl:if>
			                    </xsl:if>
		        		</xsl:for-each>
		        		<td>
		        			<xsl:value-of select="r:status"/>
		        		</td>
		        		<td>
                            <xsl:if test="r:dateStatusUpdated != ''">
		        				<xsl:variable name="dt" select="r:dateStatusUpdated"/>
									  <xsl:value-of select="concat(
									    substring($dt,9,2),'/',
									    substring($dt,6,2),'/',
									    substring($dt,1,4))" />
		        			</xsl:if>
		        		</td>
		        	</tr>	
		        </xsl:for-each>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td>&#160;</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td>Total</td>
				<td><xsl:if test="//r:enquiry/r:section_etr_plus/r:etrPlusNBLHistory/r:totalLimit != ''"><xsl:value-of select="format-number(//r:enquiry/r:section_etr_plus/r:etrPlusNBLHistory/r:totalLimit, '#,##0')"/></xsl:if></td>
				<td></td>
				<td></td>
				<td>Total</td>
				<td><xsl:if test="//r:enquiry/r:section_etr_plus/r:etrPlusNBLHistory/r:overallTotalOutstandingBlance != ''"><xsl:value-of select="format-number(//r:enquiry/r:section_etr_plus/r:etrPlusNBLHistory/r:overallTotalOutstandingBlance, '#,##0')"/></xsl:if></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
			</tr>
			</xsl:if>
        </table>
        <xsl:choose>
	    <xsl:when test="//r:enquiry/r:section_etr_plus/r:etrPlusNBLHistory/r:etrPlusNBLCreditHistoryList/r:etrPlusNBLCreditHistory">
        <table class="table-ccris">
        	<tr>
                <th width="2%" nowrap="Remark Legend:" style="text-align: left; padding-left: 1%; padding-top: 1%; border-bottom: none;" colspan="12"><strong>Remark Legend:</strong></th>
            </tr>
            <tr>
            	<th width="2%" nowrap="CAPACITY" colspan="2" style="text-align: left; padding-left: 1%; padding-top: 1%; border-top: none; border-right: none; border-bottom: none;"><strong>CAPACITY</strong></th>
            	<th width="2%" nowrap="CAPACITY" colspan="2" style="text-align: left; padding-left: 1%; padding-top: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none;"><strong>LENDER TYPE</strong></th>
            	<th width="2%" nowrap="CAPACITY" colspan="2" style="text-align: left; padding-left: 1%; padding-top: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none;"><strong>FACILITY</strong></th>
            	<th width="2%" nowrap="CAPACITY" colspan="2" style="text-align: left; padding-left: 1%; padding-top: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none;"><strong>REPAYMENT TERM</strong></th>
            	<th width="2%" nowrap="CAPACITY" colspan="2" style="text-align: left; padding-left: 1%; padding-top: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none;"><strong>COLLATERAL TYPE</strong></th>
            	<th width="2%" nowrap="CAPACITY" colspan="2" style="text-align: left; padding-left: 1%; padding-top: 1%; border-top: none; border-left: none; border-bottom: none;"><strong>STATUS</strong></th>
            </tr>
            <tr>
            	<td width="2%" style="background-color: #DDDDDD; text-align: left; padding-left: 1%; padding-top: 1%; border-top: none; border-right: none; border-bottom: none; font-size:6px">P</td>
            	<td width="2%" style="background-color: #DDDDDD; text-align: left; padding-left: 1%; padding-top: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none; font-size:6px">- PERSONAL</td>
            	<td width="2%" style="background-color: #DDDDDD; text-align: left; padding-left: 1%; padding-top: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none; font-size:6px">P2P</td>
            	<td width="2%" style="background-color: #DDDDDD; text-align: left; padding-left: 1%; padding-top: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none; font-size:6px">- P2P LENDER</td>
            	<td width="2%" style="background-color: #DDDDDD; text-align: left; padding-left: 1%; padding-top: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none; font-size:6px">TF</td>
            	<td width="2%" style="background-color: #DDDDDD; text-align: left; padding-left: 1%; padding-top: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none; font-size:6px" nowrap="TRADE">- TRADE FACILITIES</td>
            	<td width="2%" style="background-color: #DDDDDD; text-align: left; padding-left: 1%; padding-top: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none; font-size:6px">MTH</td>
            	<td width="2%" style="background-color: #DDDDDD; text-align: left; padding-left: 1%; padding-top: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none; font-size:6px">- MONTHLY</td>
            	<td width="2%" style="background-color: #DDDDDD; text-align: left; padding-left: 1%; padding-top: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none; font-size:6px">CLE</td>
            	<td width="2%" style="background-color: #DDDDDD; text-align: left; padding-left: 1%; padding-top: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none; font-size:6px">- CLEAN</td>
            	<td width="2%" style="background-color: #DDDDDD; text-align: left; padding-left: 1%; padding-top: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none; font-size:6px">LEGAL</td>
            	<td width="2%" style="background-color: #DDDDDD; text-align: left; padding-left: 1%; padding-top: 1%; border-top: none; border-left: none; border-bottom: none; font-size:6px">- LEGAL LETTER SENT</td>
            </tr>
            <tr>
            	<td width="2%" style="background-color: #DDDDDD; text-align: left; padding-left: 1%; border-top: none; border-right: none; border-bottom: none; font-size:6px">NP</td>
            	<td width="2%" style="background-color: #DDDDDD; text-align: left; padding-left: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none; font-size:6px" nowrap="- NON-PERSONAL">- NON-PERSONAL</td>
            	<td width="2%" style="background-color: #DDDDDD; text-align: left; padding-left: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none; font-size:6px">CRLS</td>
            	<td width="2%" style="background-color: #DDDDDD; text-align: left; padding-left: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none; font-size:6px" nowrap="CREDIT">- CREDIT &amp; LEASING</td>
            	<td width="2%" style="background-color: #DDDDDD; text-align: left; padding-left: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none; font-size:6px">GS</td>
            	<td width="2%" style="background-color: #DDDDDD; text-align: left; padding-left: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none; font-size:6px">- GOODS &amp; SERVICES</td>
            	<td width="2%" style="background-color: #DDDDDD; text-align: left; padding-left: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none; font-size:6px"></td>
            	<td width="2%" style="background-color: #DDDDDD; text-align: left; padding-left: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none; font-size:6px"></td>
            	<td width="2%" style="background-color: #DDDDDD; text-align: left; padding-left: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none; font-size:6px">OAS</td>
            	<td width="2%" style="background-color: #DDDDDD; text-align: left; padding-left: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none; font-size:6px" nowrap="OTHER">- OTHER ASSETS</td>
            	<td width="2%" style="background-color: #DDDDDD; text-align: left; padding-left: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none; font-size:6px">NPL</td>
            	<td width="2%" style="background-color: #DDDDDD; text-align: left; padding-left: 1%; border-top: none; border-left: none; border-bottom: none; font-size:6px;" nowrap="NON">- NON-PERFORMING LOAN</td>
            </tr>
            <tr>
            	<td width="2%" style="background-color: #DDDDDD; text-align: left; padding-left: 1%; border-top: none; border-right: none; border-bottom: none;"></td>
            	<td width="2%" style="background-color: #DDDDDD; text-align: left; padding-left: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none;">(Guarantor or</td>
            	<td width="2%" style="background-color: #DDDDDD; text-align: left; padding-left: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none; font-size:6px">COOP</td>
            	<td width="2%" style="background-color: #DDDDDD; text-align: left; padding-left: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none; font-size:6px" nowrap="KOPERASI">- KOPERASI / CO-</td>
            	<td width="2%" style="background-color: #DDDDDD; text-align: left; padding-left: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none;"></td>
            	<td width="2%" style="background-color: #DDDDDD; text-align: left; padding-left: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none; font-size:6px">&#160;&#160;&#160;RENDERED</td>
            	<td style="background-color: #DDDDDD; border-top: none; border-right: none; border-left: none; border-bottom: none;"></td>
            	<td style="background-color: #DDDDDD; border-top: none; border-right: none; border-left: none; border-bottom: none;"></td>
            	<td style="background-color: #DDDDDD; border-top: none; border-right: none; border-left: none; border-bottom: none;"></td>
            	<td style="background-color: #DDDDDD; border-top: none; border-right: none; border-left: none; border-bottom: none;"></td>
            	<td style="background-color: #DDDDDD; text-align: left; padding-left: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none; font-size:6px">S</td>
            	<td style="background-color: #DDDDDD; text-align: left; padding-left: 1%; border-top: none; border-left: none; border-bottom: none; font-size:6px;">- FULLY SETTLED</td>	
            </tr>
            <tr>
            	<td width="2%" style="background-color: #DDDDDD; border-top: none; border-right: none; border-bottom: none;"></td>
            	<td width="2%" style="background-color: #DDDDDD; padding-left: 1%; text-align: left; border-top: none; border-right: none; border-left: none; border-bottom: none;">Joint Owner)</td>
            	<td width="2%" style="background-color: #DDDDDD; border-top: none; border-right: none; border-left: none; border-bottom: none;"></td>
            	<td width="2%" style="background-color: #DDDDDD;text-align: left; padding-left: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none; font-size:6px">&#160;&#160;&#160;OPERATIVE</td>
            	<td width="2%" style="background-color: #DDDDDD;text-align: left; padding-left: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none; font-size:6px">TL</td>
            	<td width="2%" style="background-color: #DDDDDD;text-align: left; padding-left: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none; font-size:6px" nowrap="TERM">- TERM LOAN</td>
            	<td style="background-color: #DDDDDD; border-top: none; border-right: none; border-left: none; border-bottom: none;"></td>
            	<td style="background-color: #DDDDDD; border-top: none; border-right: none; border-left: none; border-bottom: none;"></td>
            	<td style="background-color: #DDDDDD; border-top: none; border-right: none; border-left: none; border-bottom: none;"></td>
            	<td style="background-color: #DDDDDD; border-top: none; border-right: none; border-left: none; border-bottom: none;"></td>
            	<td style="background-color: #DDDDDD; text-align: left; padding-left: 1%; border-top: none; border-right: none; border-left: none; border-bottom: none; font-size:6px">W</td>
            	<td style="background-color: #DDDDDD; text-align: left; padding-left: 1%; border-top: none; border-left: none; border-bottom: none; font-size:6px;">- WRITTEN OFF BAD DEBT</td>
            </tr>
            <tr>
            	<th style="border-top: none; border-right: none; padding-bottom: 1%;"></th>
            	<th style="border-top: none; border-right: none; border-left: none; padding-bottom: 1%;"></th>
            	<th style="border-top: none; border-right: none; border-left: none; padding-bottom: 1%;"></th>
            	<th style="border-top: none; border-right: none; border-left: none; padding-bottom: 1%;"></th>
            	<th style="border-top: none; border-right: none; border-left: none; padding-bottom: 1%;"></th>
            	<td style="background-color: #DDDDDD; text-align: left; padding-left: 1%; border-top: none; border-right: none; border-left: none; font-size:6px; padding-bottom: 1%;">&#160;&#160;&#160;OPERATIVE</td>
            	<th style="border-top: none; border-right: none; border-left: none; padding-bottom: 1%;"></th>
            	<th style="border-top: none; border-right: none; border-left: none; padding-bottom: 1%;"></th>
            	<th style="border-top: none; border-right: none; border-left: none; padding-bottom: 1%;"></th>
            	<th style="border-top: none; border-right: none; border-left: none; padding-bottom: 1%;"></th>
            	<th style="border-top: none; border-right: none; border-left: none; padding-bottom: 1%;"></th>
            	<th style="border-top: none; border-left: none;"></th>
            </tr>
		</table>
		</xsl:when>
		<xsl:otherwise>
             <p class="info">No Information Available</p>
        </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <!-- finish non_blank_lender_credit_information_template -->
    
    <!-- start getYearOnly_e -->
    <xsl:template name="getYearOnly_e">
        <xsl:param name="datestr"/>
        <xsl:variable name="yyyy">
            <xsl:value-of select="substring($datestr,1,4)"/>
        </xsl:variable>
        <xsl:value-of select="$yyyy"/>
    </xsl:template>
    <!-- fnish getYearOnly_e -->
    
    <!-- start getPastMonthAndYearOnly_section_e -->
    <xsl:template name="getPastMonthAndYearOnly_section_e">
        <xsl:param name="datestr"/>
        <xsl:param name="past"/>		
        <xsl:variable name="currMth">
            <xsl:value-of select="substring($datestr,6,2)"/>
        </xsl:variable>		
        <xsl:variable name="month">
            <xsl:choose> 
                <xsl:when test="($currMth - $past) &lt;= 0">
                    <xsl:value-of select="$currMth - $past + 12"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$currMth - $past"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
                <xsl:variable name="yyyy">
            <xsl:value-of select="substring($datestr,1,2)"/>
        </xsl:variable>
        <xsl:variable name="mthyyyy">
            <xsl:choose> 
                <xsl:when test="($currMth - $past) &lt;= 0">
                    <xsl:value-of select="$yyyy - 1"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$yyyy"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:if test="$month = 1">
            <xsl:text>Jan&#xa;</xsl:text>
            <xsl:value-of select="$mthyyyy"/>
        </xsl:if>
        <xsl:if test="$month = 2">
            <xsl:text>Feb&#xa;</xsl:text>
            <xsl:value-of select="$mthyyyy"/>
        </xsl:if>
        <xsl:if test="$month = 3">
            <xsl:text>Mar&#xa;</xsl:text>
            <xsl:value-of select="$mthyyyy"/>
        </xsl:if>
        <xsl:if test="$month = 4">
            <xsl:text>Apr&#xa;</xsl:text>
            <xsl:value-of select="$mthyyyy"/>
        </xsl:if>
        <xsl:if test="$month = 5">
            <xsl:text>May&#xa;</xsl:text>
            <xsl:value-of select="$mthyyyy"/>
        </xsl:if>
        <xsl:if test="$month = 6">
            <xsl:text>Jun&#xa;</xsl:text>
            <xsl:value-of select="$mthyyyy"/>
        </xsl:if>
        <xsl:if test="$month = 7">
            <xsl:text>Jul&#xa;</xsl:text>
            <xsl:value-of select="$mthyyyy"/>
        </xsl:if>
        <xsl:if test="$month = 8">
            <xsl:text>Aug&#xa;</xsl:text>
            <xsl:value-of select="$mthyyyy"/>
        </xsl:if>
        <xsl:if test="$month = 9">
            <xsl:text>Sep&#xa;</xsl:text>
            <xsl:value-of select="$mthyyyy"/>
        </xsl:if>
        <xsl:if test="$month = 10">
            <xsl:text>Oct&#xa;</xsl:text>
            <xsl:value-of select="$mthyyyy"/>
        </xsl:if>
        <xsl:if test="$month = 11">
            <xsl:text>Nov&#xa;</xsl:text>
            <xsl:value-of select="$mthyyyy"/>
        </xsl:if>
        <xsl:if test="$month = 12">
            <xsl:text>Dec&#xa;</xsl:text>
            <xsl:value-of select="$mthyyyy"/>
        </xsl:if>	
    </xsl:template>
    <!-- finish getPastMonthAndYearOnly_section_e -->
    
</xsl:stylesheet>
