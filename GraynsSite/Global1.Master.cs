﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Helper;
using Synergy.Model;
using Synergy.Util;


namespace WMElegance
{
    public partial class Glober1 : System.Web.UI.MasterPage
    {
        String Return_Page = "/Form/login.aspx";
        //String Return_Page = "/Form/Ecommerce/SalesProductDisplay.aspx";
        //String PasswordReset_Link = "/Form/PasswordReset.aspx";
        //String Logout_Link = "/Form/Logout.aspx";

        protected LoginUserModel login_user;

        protected void Page_Init(object sender, EventArgs e)
        {
            String Current_RawURL = Request.RawUrl;

            if (Session[KeyVal.loginsecure] != null)
            {
                this.Add_PostBackTrigger();

                login_user = (LoginUserModel)Session[KeyVal.loginsecure];
                
                String Current_URL = HttpContext.Current.Request.RawUrl;
                String Current_Path = HttpContext.Current.Request.Url.AbsolutePath;
                //String Current_Role = login_user.Role;
                List<string> Current_Role = login_user.UserRole;
                
                if (login_user.HaveShorcutMenu())
                    showShorcut(Current_Role, Current_Path);

                //Perform Checking see if have the privillege to access this page -----------
                if (this.Verify_Access(Current_Role, Current_Path))
                {
                    //Update Last Action ----------------------------------------------------
                    LogUtil.UpdateCheckInUser(login_user.UserId);
                    //-----------------------------------------------------------------------
                }
                //When no Privillege To Access This Page ------------------------------------
                else { this.Logout_System(Current_RawURL); }
                //---------------------------------------------------------------------------
            }
            //When no session Detected ------------------------------------------------------
            else { this.Logout_System(Current_RawURL); }
            //-------------------------------------------------------------------------------
        }

        private void showShorcut(List<string> roleid, string currentURL)
        {
            wwdb db = new wwdb();
            currentURL = currentURL.ToLower();
            string remove = "";

            //if (!login_user.isStaffAdmin())
            //{
                remove = " AND rowID != 1";
            //}
            DataTable dt = db.getDataTable(string.Format(@"select b.ShowText_EN as 'name' , LinkUrl as 'url' from (select DISTINCT MenuID from tbl_RoleMenu where RoleID IN ({0}))a inner join tbl_menu b on a.MenuID = b.rowid and b.IsDeleted = 0 and b.haveShorcut = 1 and b.type = 1 {1}", string.Join(",", roleid), string.Join(",", remove)));
            //remove requestlist
            if (dt != null && dt.Rows.Count > 1)
            {
                //only url in shortcut url can view the shortcut
                //if (dt.AsEnumerable().Where(x => x.Field<string>("url").ToLower().Contains(currentURL)).Count() > 0) //remove so all activity can view shortcut
                //{
                    rptShortCut.Visible = true;
                    rptShortCut.DataSource = dt;
                    rptShortCut.DataBind();
                //}
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                login_user = (LoginUserModel)Session[KeyVal.loginsecure];
                ddlCulture.Visible = false;
                
                getMenu();

                setPersonalDetails();
                
                //Bind On Language ------------------------
                this.Bind_Language();
                //------------------------------------------
            }



            ScriptManager.RegisterStartupScript(A
                        ,
                        this.GetType(),
                        "UpdateLeftRightMenu",
                        "checkLeftRightMenu();",
                        true);
        }

        public void setPersonalDetails()
        {
            string strSql = string.Format(@"
                select Fullname, IC, UploadDocument
                from tbl_MemberInfo with (nolock) 
                where MemberId = N'{0}'
                ", secure.RC(login_user.UserId.ToString()));
            wwdb db = new wwdb();

            try
            {
                db.OpenTable(strSql.ToString());
                if (!db.HasError && db.RecordCount() > 0)
                {
                    string name = db.Item("Fullname");
                    string balance = string.Empty;

                    imgUserImage.Src = string.IsNullOrEmpty(db.Item("UploadDocument")) ? "/Images/About-us.png" : db.Item("UploadDocument");

                    imgUserImage.Attributes.Add("onclick", "editProfile()");
                    lblimguser.Src = "/Images/About-us.png";

                    db.OpenTable(
                        string.Format(@"
                            SELECT TOP (1) ucb.[CreditUsed], ucb.[FreeRequestCount]
                            FROM [tbl_UserCreditBalance] ucb with (nolock)
                            where ucb.MemberId = '{0}'
                        ", login_user.UserId)
                    );
                    if (db.RecordCount() > 0)
                        balance = db.Item("CreditUsed");
                    lblWelcomeUser.Text = Resources.resource.WelcomeNote + " " + name;

                    bool displayCredit = false;
                    if (!string.IsNullOrEmpty(balance))
                    {
                        if (ConvertHelper.ConvertToDecimal(balance, 0) > 0)
                        {
                            displayCredit = true;
                            lblUserCreditBalance.ForeColor = System.Drawing.Color.Lime;
                        }
                        else if (ConvertHelper.ConvertToDecimal(balance, 0) < 0)
                        {
                            displayCredit = true;
                            lblUserCreditBalance.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                            displayCredit = false;

                        if (displayCredit)
                            lblUserCreditBalance.Text = Environment.NewLine
                                    + Resources.resource.Balance + ": " + ConvertHelper.ConvertToDecimal(balance, 0).ToString("N0");
                    }
                    lblnavbarName.Text = name;
                        ddusrname.Text = name;
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                db.Close();
            }
        }

        private void Logout_System(String RedirectPath)
        {
            if (!String.IsNullOrEmpty(RedirectPath))
            {
                String Encry_Path = secure.EURC(secure.Encrypt(RedirectPath, true));
                Response.Redirect(Return_Page + "?path=" + Encry_Path);
            }
            else { Response.Redirect(Return_Page); }
        }

        protected void Add_PostBackTrigger()
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];

            ContentPlaceHolder cphPage = FindControl("ContentPlaceHolder1") as ContentPlaceHolder;
            Button btn = (Button)cphPage.FindControl("btnExport");
            Button btnCSV = (Button)cphPage.FindControl("btnExportCSV");
            Button btnUpload = (Button)cphPage.FindControl("btnUpload");
            Button btnPreview = (Button)cphPage.FindControl("btnPreview");
            Button btnUpdate = (Button)cphPage.FindControl("btnUpdate");
            //Button btnConfirm5 = (Button)cphPage.FindControl("btnConfirm5");
            Button btnSubmit = (Button)cphPage.FindControl("btnSubmit");

            if (btn != null)
            {
                if (!login_user.UserType.Contains(KeyVal.Admin) && !login_user.UserType.Contains("STAFF"))
                {
                    btn.Visible = false;
                }
                else
                {
                    btn.Visible = true;
                }

                PostBackTrigger trigger = new PostBackTrigger();
                trigger.ControlID = btn.UniqueID;
                A.Triggers.Add(trigger);
            }

            if (btnCSV != null)
            {
                PostBackTrigger trigger2 = new PostBackTrigger();
                trigger2.ControlID = btnCSV.UniqueID;
                A.Triggers.Add(trigger2);
            }

            if (btnUpload != null)
            {
                PostBackTrigger trigger2 = new PostBackTrigger();
                trigger2.ControlID = btnUpload.UniqueID;
                A.Triggers.Add(trigger2);
            }

            if (btnSubmit != null)
            {
                PostBackTrigger trigger2 = new PostBackTrigger();
                trigger2.ControlID = btnSubmit.UniqueID;
                A.Triggers.Add(trigger2);
            }

            if (btnPreview != null)
            {
                PostBackTrigger trigger2 = new PostBackTrigger();
                trigger2.ControlID = btnPreview.UniqueID;
                A.Triggers.Add(trigger2);
            }
            if (btnUpdate != null)
            {
                PostBackTrigger trigger3 = new PostBackTrigger();
                trigger3.ControlID = btnUpdate.UniqueID;
                A.Triggers.Add(trigger3);
            }
            //if (btnConfirm5 != null)
            //{
            //    PostBackTrigger trigger4 = new PostBackTrigger();
            //    trigger4.ControlID = btnConfirm5.UniqueID;
            //    A.Triggers.Add(trigger4);

            //}
        }

        #region Menu Control

        private void getMenu()
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];

            wwdb dbMenu = new wwdb();
            wwdb dbSubMenu = new wwdb();
            wwdb dbSubMenulevel2 = new wwdb();
            StringBuilder strmenu = new StringBuilder();
            string sql = "";
            string sql2 = "";
            DataTable dt = new DataTable();
            DataTable dtSubMenu = new DataTable();
            DataTable dtSubMenulevel2 = new DataTable();
            Boolean isUpgradeRankShow = true;

            StringBuilder checkMemberUpgrade = new StringBuilder();
            checkMemberUpgrade.AppendFormat(@" DECLARE @Date DATETIME = DATEADD(MILLISECOND, -3, DATEADD(MONTH, 0, GETDATE()));
                                            DECLARE @InsertDate DATETIME = GETDATE();
                                            DECLARE @Month INT = MONTH(DATEADD(MONTH, 0, @Date)) DECLARE @Year INT = YEAR(DATEADD(MONTH, 0, @Date)) DECLARE @StartDate DATETIME = CONVERT(DATETIME, (CONVERT(VARCHAR, @Year) + '-' + CONVERT(VARCHAR, @Month) + '-01'));
                                            DECLARE @EndDate DATETIME = @Date;
                                            DECLARE @MEMID NVARCHAR(50) = '{0}';
                                            SELECT * FROM tbl_MemberInfo
                                            WHERE MemberId = @MEMID AND (CreatedAt BETWEEN @StartDate AND @EndDate)
                                            AND Ranking<(select top 1Ranking from tbl_Rank order by Ranking desc )
                                            ", secure.RC(login_user.UserId));

            if (!login_user.UserType.Contains(KeyVal.Admin))
            {
                //check avaliability of login user to upgrade rank 
                dbMenu.OpenTable(checkMemberUpgrade.ToString());
                if (!dbMenu.HasError && dbMenu.RecordCount() < 1)
                {
                    isUpgradeRankShow = false;
                }
                dbMenu.Close();
                dbMenu = new wwdb();
            }

            try
            {
                //level 0
                sql = string.Format(@" SELECT a.ShowText_{0} AS 'Text',  a.RowID AS 'RowID', a.ParentID AS 'ParentID', ISNULL(a.LinkURL,'#') as [LinkURL],isnull(a.iconPath,'') as 'icon'
		                                    FROM tbl_menu a WITH (NOLOCK) 
		                                    LEFT JOIN  tbl_roleMenu b WITH (NOLOCK) ON a.rowID = b.menuID 
		                                    WHERE a.type = 1 AND a.isDeleted = 'false' AND b.roleID IN ({1}) ####
                                            GROUP BY a.ShowText_en, a.RowID, a.ParentID,a.LinkURL,a.iconPath,a.Sort
		                                    ORDER BY a.Sort ASC;", secure.RC(Thread.CurrentThread.CurrentCulture.Name), string.Join(",", login_user.UserRole));
                sql2 = sql.Replace("####", "AND a.menuLevel = 0 ");

                strmenu.Append("<ul id=\"MenuPart\" class=\"sidebar-menu\">");
                dt = dbMenu.getDataTable(sql2);
                foreach (DataRow row in dt.Rows)
                {

                    if (row["LinkURL"].ToString() == "#")
                    {
                        //level 1
                        string[] splitStr;
                        splitStr = row["Text"].ToString().Split(' ');

                        strmenu.AppendFormat("<li class=\"treeview\" style=\"text-transform:capitalize;\"> " +
                                            "<a href=\"{1}\"> <i class=\"fa fa-angle-left pull-right\"></i> " +
                                            "<span>{0} {2}<p style=\"font-size:10px;margin: 0 0 0 22px;\">{3}</p></span></a>",
                                          row["icon"].ToString(),
                                          row["LinkURL"].ToString(),
                                          splitStr[0].ToString(),
                                          splitStr.Length > 1 ? splitStr[1].ToString() : "");

                        //if user
                        //if (!login_user.isStaffAdmin())
                        //{
                        sql2 = sql.Replace("####", " AND a.menuLevel = 1 AND parentID = N'" + row["RowID"] + "' AND rowID != 1 ");
                        //}
                        //else
                            //if admin
                            //sql2 = sql.Replace("####", " AND a.menuLevel = 1 AND parentID = N'" + row["RowID"] + "'");
                        //remove requestlist

                        dtSubMenu = dbSubMenu.getDataTable(sql2);

                        strmenu.Append("<ul class=\"treeview-menu\" style=\"text-transform:none;\">");
                        foreach (DataRow submenurows in dtSubMenu.Rows)
                        {

                            if (submenurows["LinkURL"].ToString() == "#")
                            {
                                strmenu.AppendFormat("<li class=\"treeview\" > <a href=\"{1}\"> <i class=\"fa fa-angle-left pull-right\"></i> <span> {0}</span></a>",
                                               submenurows["Text"].ToString(),
                                               submenurows["LinkURL"].ToString());
                                sql2 = sql.Replace("####", " AND a.menuLevel = 2 AND parentID = N'" + submenurows["RowID"] + "'");

                                dtSubMenulevel2 = dbSubMenulevel2.getDataTable(sql2);
                                strmenu.Append("<ul class=\"treeview-menu\">");
                                foreach (DataRow submenurowsLevel2 in dtSubMenulevel2.Rows)
                                {
                                    if (submenurowsLevel2["Text"].ToString() != "Upgrade Ranking")
                                    {
                                        strmenu.AppendFormat("<li> <a href=\"{1}\"><span> {0}</span></a></li>",
                                                 submenurowsLevel2["Text"].ToString(),
                                                 submenurowsLevel2["LinkURL"].ToString());
                                    }
                                    else if (submenurowsLevel2["Text"].ToString() == "Upgrade Ranking" && isUpgradeRankShow)
                                    {
                                        strmenu.AppendFormat("<li> <a href=\"{1}\"><span> {0}</span></a></li>",
                                                 submenurowsLevel2["Text"].ToString(),
                                                 submenurowsLevel2["LinkURL"].ToString());
                                    }
                                }
                                strmenu.Append("</ul></li>");
                            }
                            else
                            {
                                strmenu.AppendFormat("<li> <a href=\"{1}\"> <i class=\"fa fa-fw fa-circle\"></i>  <span> {0}</span></a></li>",
                                               submenurows["Text"].ToString(),
                                               submenurows["LinkURL"].ToString());
                            }
                        }
                        strmenu.Append("</ul></li>");


                    }
                    else
                    {
                        //strmenu.AppendFormat("<li class=\"active\" style=\"text-transform:uppercase\"> <a href=\"{1}\"><span> {0}</span></a></li>",
                        //                  row["Text"].ToString(),
                        //                  row["LinkURL"].ToString());
                        strmenu.AppendFormat("<li style=\"text-transform:capitalize\"> <a href=\"{1}\"><span> {0}</span></a></li>",
                                          row["Text"].ToString(),
                                          row["LinkURL"].ToString());
                    }


                }

                strmenu.Append("</ul>");
                ltrMenu.Text = strmenu.ToString();
            }
            catch (Exception ex)
            {
                LoggerUtil.TextFileLogger.LogToFile("Error Retrieving Menu", ex.ToString(), true);
            }
            finally
            {
                dbMenu.Close();
                dbSubMenu.Close();
                dbSubMenulevel2.Close();
            }

        }

        #endregion

        #region LANGUAGE

        protected void ddlCulture_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (KeyVal.MultiLanguage)
            {

                if (Request.Cookies[KeyVal.cookie_language] != null)
                {
                    HttpCookie MyCookie = Request.Cookies[KeyVal.cookie_language];
                    MyCookie.Value = secure.Encrypt(ddlCulture.SelectedValue, true);
                    HttpContext.Current.Response.SetCookie(MyCookie);
                    HttpContext.Current.Response.Redirect(this.Request.RawUrl, true);
                }
                else
                {
                    DateTime exdate = DateTime.Now.AddYears(10);
                    HttpCookie MyCookie = new HttpCookie(KeyVal.cookie_language);
                    MyCookie.Value = secure.Encrypt(ddlCulture.SelectedValue, true);
                    MyCookie.Expires = exdate;
                    HttpContext.Current.Response.Cookies.Add(MyCookie);
                    HttpContext.Current.Response.Redirect(this.Request.RawUrl, true);
                }
            }
        }

        protected void Bind_Language()
        {
            DataTable dtCulture = LanguageUtil.LoadLanguageResources("language.resx");
            ddlCulture.DataTextField = "Key";
            ddlCulture.DataValueField = "Value";
            foreach (DataRow dr in dtCulture.Rows)
            {
                String value = LanguageUtil.LoadSingleResourceValue("LanguageLibrary.resx", dr["Key"].ToString());
                dr["Value"] = value;
            }
            ddlCulture.DataSource = dtCulture;
            ddlCulture.DataBind();

            ListItem liCulture = ddlCulture.Items.FindByValue(Thread.CurrentThread.CurrentCulture.Name);
            if (liCulture != null)
            {
                liCulture.Selected = true;
            }
            ////select ddl according to culture
            //try
            //{
            //    if (Request.Cookies[KeyVal.cookie_language] != null)
            //    {
            //        string value = secure.Decrypt(Request.Cookies[KeyVal.cookie_language].Value, true);
            //        ListItem liCulture = ddlCulture.Items.FindByValue(value);
            //        if (liCulture != null)
            //        {
            //            liCulture.Selected = true;
            //        }
            //    }
            //    else
            //    {
            //        ListItem liCulture = ddlCulture.Items.FindByValue(Thread.CurrentThread.CurrentCulture.Name);
            //        if (liCulture != null)
            //        {
            //            liCulture.Selected = true;
            //        }
            //    }
            //}
            //catch
            //{
            //    ListItem liCulture = ddlCulture.Items.FindByValue(Thread.CurrentThread.CurrentCulture.Name);
            //    if (liCulture != null)
            //    {
            //        liCulture.Selected = true;
            //    }
            //}
        }

        #endregion

        #region UAC

        private Boolean Verify_Access(List<string> Current_Role, String Current_URL)
        {
            //return true;
            wwdb db = new wwdb();
            String SQL = string.Format("SELECT TOP 1 1 FROM tbl_RoleMenu TRM " +
                        " WHERE 1 = 1 " +
                        " AND TRM.MenuID IN (SELECT RowId FROM tbl_Menu TM WHERE TM.Type = 1 AND TM.LinkURL = N'" + secure.RC(Current_URL) + "') " +
                        " AND TRM.RoleId IN ({0})", string.Join(",", Current_Role));

            db.OpenTable(SQL);
            db.Close();

            if (db.RecordCount() > 0)
            { return true; }
            else
            { return false; }
        }

        #endregion

        #region SYSTEM SETTING

        //Comment because not used in any place
        //private void Validate_IsPasswordExpired(LoginUserModel login_user)
        //{
        //    login_user = (LoginUserModel)Session[KeyVal.loginsecure];

        //    wwdb db = new wwdb();
        //    String check = ""; Boolean result = false;
        //    String SQL = String.Empty;
        //    int record = 0;
        //    try
        //    {
        //        SQL = " SELECT 1 from tbl_passwordExpired WITH (NOLOCK) WHERE memberid = '" +
        //                           secure.RC(login_user.UserId) + "' AND isDeleted = 'false' " +
        //                           " AND (DATEDIFF(DAY, changeDate, current_timestamp)) > " +
        //                           " (SELECT parameterValue FROM tbl_parameter WITH (NOLOCK) WHERE parameterName = 'PasswordExpired') ";
        //        db.OpenTable(SQL);
        //        record = db.RecordCount();

        //        SQL = " SELECT CONVERT(NVARCHAR, firstLogin) FROM tbl_login WITH (NOLOCK) WHERE " +
        //            " login_ID = N'" + secure.RC(login_user.UserId) + "'; ";
        //        check = db.executeScalar(SQL).ToString();

        //        if (check == "1")
        //        {
        //            result = false;
        //        }
        //        else if (record > 0)
        //        {
        //            result = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LogUtil.logError(ex.ToString(), SQL);
        //    }
        //    finally
        //    {
        //        db.Close();
        //    }

        //    //Request URL Must not PasswordReset / Logout page ------------------------------------------
        //    String Req_URL = Request.Url.ToString();
        //    if (!Req_URL.Contains(Logout_Link) && !Req_URL.Contains(PasswordReset_Link))
        //    {
        //        if (check == "1" && !result)
        //            Response.Redirect("/Form/PasswordReset.aspx?type=" + secure.RC(sqlString.encryptURL("FIRSTTIMELOGIN")) + "&mid=" + sqlString.encryptURL(login_user.UserId) + "&type2=" + sqlString.encryptURL(login_user.UserType) + "&from=" + secure.RC(sqlString.encryptURL("FIRSTTIMELOGIN")));
        //        else if (record > 0 && !result)
        //            Response.Redirect("/Form/PasswordReset.aspx?type=" + secure.RC(sqlString.encryptURL("FIRSTTIMELOGIN")) + "&mid=" + sqlString.encryptURL(login_user.UserId) + "&type2=" + sqlString.encryptURL(login_user.UserType) + "&from=" + secure.RC(sqlString.encryptURL("FIRSTTIMELOGIN")) + "&exp=" + secure.RC(sqlString.encryptURL("TRUE")));
        //    }
        //    //--------------------------------------------------------------------------------------------

        //}


        #endregion

        protected void btnWhatsapp_Click(object sender, ImageClickEventArgs e)
        {
            wwdb db = new wwdb();
            string url = "https://api.whatsapp.com/send?phone=";
            string message = string.Empty, branchName = string.Empty, phoneNumber = string.Empty;

            db.OpenTable("SELECT ParameterValue as 'PhoneNo' from tbl_parameter with (nolock) where " +
                "Category = 'Whatsapp' and ParameterName = 'AccountNumber'");
            if (db.RecordCount() > 0)
            {
                phoneNumber = db.Item("PhoneNo");

                //get branchName
                db.OpenTable("SELECT b.BranchName FROM tbl_Branch b with (nolock) WHERE b.isDeleted=0 and b.BranchID = '" + login_user.BranchID + "'");
                if (db.RecordCount() > 0)
                    branchName = db.Item("BranchName");

                //constructing message
                message = "Hi, I'm " + login_user.Name + (string.IsNullOrEmpty(branchName) ? "" : (" from " + branchName)) + ".";
                //encrypting message
                message = message.Replace("%", "%25").Replace("&", "%26");
                //appending url with phoneNumber and text message
                url += phoneNumber + (string.IsNullOrEmpty(message) ? "" : "&text=" + message);
                //encrypting url
                url = url.Replace("'", "%27").Replace(" ", "%20")
                         .Replace("@", "%40").Replace("#", "%23").Replace("$", "%24")//.Replace("%", "%25")
                         .Replace("^", "%5E")//.Replace("&", "%26")
                         .Replace(";", "%3B").Replace(",", "%2C")//.Replace(":", "%3A").Replace("/", "%2F").Replace("?", "%3F")
                         ;
                //open new tab
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWhatsappTab", "window.open('" + url + "');location=location;", true);
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWhatsappTab", "window.open('" + url + "','_blank');", true);
                //Response.Redirect(url);
            }
        }
    }
}