﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Util;
using Synergy.Model;
using Synergy.UI;
using System.Text;
using System.Web.UI.HtmlControls;
using Synergy.Controller;
using System.Drawing;
using System.Globalization;

namespace WMElegance.Form
{
    public partial class Login : System.Web.UI.Page
    {
        #region ErrorMsg Class
        error errormessage = new error();

        #endregion

        #region Captchar Variable

        public Captcha Captchar_Class;

        //private String Captchar_Text = "qpweirutyghfjdksamxncbzvZABCDZABCD#EFGHJK%MNPQR&STUVWX$YZ23456789";
        private String Captchar_Text = "0123456789";
        private Int16 Captchar_Length = 4;
        private Double CaptChar_FontSize = 15;
        private String CaptChar_FontFamily = "Verdana";
        private String CaptChar_BackgroundImgPath = "/images/CaptCharBG.png";
        private String CaptChar_TextColor = "Black";

        #endregion

        #region PAGE EVENT

        protected void Page_Load(object sender, EventArgs e)
        {
            //Validate If Server Is Online (Show Maintenance Page If Offline) --
            this.Validate_ServerIsOnline();
            //------------------------------------------------------------------
           
            //Validate [User] / [IP] is Blocked --------------------------------
            this.Validate_IPIsBlocked();
            //------------------------------------------------------------------
            //lblTipVerificationCode.Text = "Enter";
            if (!IsPostBack)
            {
                if (Request.Url.ToString().ToUpper().Contains("://LOCALHOST"))
                { actual.Visible = true; maintenance.Visible = false; }
                else
                { actual.Visible = true; maintenance.Visible = false; }

                //ddlCulture.Visible = KeyVal.MultiLanguage;

                if (Request["id"] != null)
                    TxtUserId.Text = sqlString.decryptURL(Request["id"].ToString());

                if (Request["pa"] != null)
                    TxtPassword.Attributes.Add("value", sqlString.decryptURL(Request["pa"].ToString()));

                //Clear All Session On Page Load -------------------------------
                Session.Remove("CaptchaClassLogin");
                Session.Remove(KeyVal.session_member);
                Session.Remove(KeyVal.session_clogin);
                //--------------------------------------------------------------

                //Load CaptChar() ----------------------------------------------
                while (Session["CaptchaClassLogin"] == null)
                { this.LoadCaptcha(); }
                //--------------------------------------------------------------

                bindAnnouncement();
                this.TxtUserId.Focus();
                this.Bind_Language();
            }

            Session.Remove("CaptchaClass");
            Session.Remove("captcha");
           // this.Bind_BackgroundImage();
        }

        private void bindAnnouncement()
        {
            wwdb db = new wwdb();
            const string Announcement_Header = "AnnouncementHeader";
            const string Announcement_Message = "AnnouncementMessage";
            const string Announcement_Image = "AnnouncementImage";
            const string Announcement_TextColor = "AnnouncementTextColor";
            bool display = false;

            db.OpenTable(string.Format(@"
                SELECT ParameterValue FROM tbl_Parameter with (nolock) 
                WHERE Category = 'Announcement' and ParameterName = '{0}'
            ", Announcement_Header));
            if (db.RecordCount() > 0)
            {
                string parameterValue = db.Item("ParameterValue");
                if (!string.IsNullOrEmpty(parameterValue))
                {
                    litAnnouncementHeader.Text = parameterValue;
                    display = true;
                }
            }

            db.OpenTable(string.Format(@"
                SELECT ParameterValue FROM tbl_Parameter with (nolock) 
                WHERE Category = 'Announcement' and ParameterName = '{0}'
            ", Announcement_Message));
            if (db.RecordCount() > 0)
            {
                string parameterValue = db.Item("ParameterValue");
                if (!string.IsNullOrEmpty(parameterValue))
                {
                    litAnnouncementMessage.Text = parameterValue;
                    display = true;
                }
            }

            db.OpenTable(string.Format(@"
                SELECT ParameterValue FROM tbl_Parameter with (nolock) 
                WHERE Category = 'Announcement' and ParameterName = '{0}'
            ", Announcement_Image));
            if (db.RecordCount() > 0)
            {
                string parameterValue = db.Item("ParameterValue");
                if (!string.IsNullOrEmpty(parameterValue))
                {
                    imgAnnouncement.ImageUrl = parameterValue;
                    display = true;
                }
            }

            db.OpenTable(string.Format(@"
                SELECT ParameterValue FROM tbl_Parameter with (nolock) 
                WHERE Category = 'Announcement' and ParameterName = '{0}'
            ", Announcement_TextColor));
            if (db.RecordCount() > 0)
            {
                string parameterValue = db.Item("ParameterValue");
                if (!string.IsNullOrEmpty(parameterValue))
                {
                    //Color clr;
                    //string colorcode = "#FFFFFF";
                    //if (!string.IsNullOrEmpty(parameterValue) && !string.IsNullOrWhiteSpace(parameterValue))
                    //    colorcode = parameterValue;
                    //int argb = Int32.Parse(colorcode.Replace("#", ""), NumberStyles.HexNumber);
                    //clr = Color.FromArgb(argb);

                    litAnnouncementText.Text =
                        string.Format("<span style='color:{0}'>{1}</span>"
                        , parameterValue//clr.Name
                        , GetGlobalResourceObject("resource", "Announcement"));
                }
            }

            if (!display)
            {
                hfAnnouncementWindow.Value = "hide";
                divAnnouncementContent.Visible = false;
                divAnnouncementEmpty.Visible = true;
            }
            else
            {
                hfAnnouncementWindow.Value = "show";
                divAnnouncementContent.Visible = true;
            }
        }

        private void Validate_ServerIsOnline()
        {
            Boolean Server_IsOnline = false;
            String SQL = "SELECT [ParameterValue] AS 'Status' FROM [tbl_Parameter] WHERE 1 = 1";
            SQL += " AND [ParameterName] = 'OnlineStatus' ";
            SQL += " AND [Category] ='OnlineStatus'";

            wwdb db = new wwdb();
            db.OpenTable(SQL);
            db.Close();
            if (!db.HasError && db.RecordCount() == 1)
            {
                String Status_Val = db.Item("Status");
                if (Status_Val == "1")
                {
                    Server_IsOnline = true;
                }
            }

            //Server Is Offline, Show Maintenance page ---------------
            if (!Server_IsOnline)
            {
                Response.Redirect("/Form/Maintenance.html");
            }
            //--------------------------------------------------------
        }

        #endregion

        #region LANGUAGE

        //protected void ddlCulture_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (KeyVal.MultiLanguage)
        //    {
        //        HttpCookie MyCookie = new HttpCookie(KeyVal.cookie_language);
        //        MyCookie.Value = secure.Encrypt(ddlCulture.SelectedValue, true);
        //        MyCookie.Expires = DateTime.Now.AddYears(500);
        //        Response.Cookies.Add(MyCookie);
        //        Response.Redirect(this.Request.RawUrl, true);
        //    }
        //}

        protected void Bind_Language()
        {
            DataTable dtCulture = LanguageUtil.LoadLanguageResources("language.resx");
            //ddlCulture.DataTextField = "Key";
            //ddlCulture.DataValueField = "Value";
            foreach (DataRow dr in dtCulture.Rows)
            {
                String value = LanguageUtil.LoadSingleResourceValue("LanguageLibrary.resx", dr["Key"].ToString());
                dr["Value"] = value;
            }
            //ddlCulture.DataSource = dtCulture;
            //ddlCulture.DataBind();

            //select ddl according to cultureF
            //ListItem liCulture = ddlCulture.Items.FindByValue(Thread.CurrentThread.CurrentCulture.Name);
            //if (liCulture != null)
            //{
            //    liCulture.Selected = true;
            //}
        }

        #endregion

        #region CAPTCHAR

        public void LoadCaptcha()
        {
            String text = this.Generate_RandomText();
            Session["cctextLogin"] = text;
            ViewState.Add("captchaLogin", text);
            Session.Add("CaptchaClassLogin", this.GET_CaptCharClass());//add captcha object to Session
            Session.Add("captchaLogin", text);//add captcha text to session             
            Im1.ImageUrl = "/captcha/CaptchaHandler.ashx";

            if (KeyVal.testingPeriod)
            {
                //TxtVerificationCode.Text = Session["cctext"].ToString();

            }

        }

        private Captcha GET_CaptCharClass()
        {
            Session.Remove("CaptchaClassLogin");
            Captchar_Class = new Captcha();

            Captchar_Class.FontSize = this.CaptChar_FontSize;
            Captchar_Class.FontFamily = this.CaptChar_FontFamily;
            Captchar_Class.BackgroundImagePath = this.CaptChar_BackgroundImgPath;
            Captchar_Class.TextColor = this.CaptChar_TextColor;
            return Captchar_Class;
        }

        private string Generate_RandomText()
        {

            char[] letters = Captchar_Text.ToCharArray();

            string text = string.Empty;
            Random r = new Random();
            int num = -1;

            for (int i = 0; i < this.Captchar_Length; i++)
            {
                num = (int)(r.NextDouble() * (letters.Length - 1));
                text += letters[num].ToString();
            }
            return text;
        }

        #endregion

        #region VALIDATION FUNCTION

        private Boolean Login_Validation()
        {
            this.LblError.Text = String.Empty;
            Boolean Validate_Success = false;
            if (this.RequireField_Validation())
            {
                //Verify If the UserId & IP has been blocked ----------
                if (!this.Validate_IPIsBlocked())
                {
                    if (hidd_Count.Value == "")
                    {
                        trVerification.Visible = false;
                        divImg.Visible = false;
                        Validate_Success = true;
                    }
                    else
                    {
                        trVerification.Visible = true;
                        divImg.Visible = true;
                        if (this.CaptChar_Validation())
                        {
                            Validate_Success = true;
                        }
                    }


                }
                //-----------------------------------------------------
            }
            return Validate_Success;
        }

        private Boolean RequireField_Validation()
        {
            String NewLine = " <br/>";
            String ErrMsg = Resources.resource.InputRequiredField;
            String ErrField = String.Empty;


            String UserName = this.TxtUserId.Text.Trim();
            String Password = this.TxtPassword.Text.Trim();
            String CaptChar = this.TxtVerificationCode.Text.Trim();

            if (String.IsNullOrEmpty(UserName))
            {
                ErrField += "- " + Resources.resource.Username_Input + "." + NewLine;

            }

            if (String.IsNullOrEmpty(Password))
            {
                ErrField += "- " + Resources.resource.Password_Input + "." + NewLine;

            }

            if (hidd_Count.Value != "")
            {
                if (String.IsNullOrEmpty(CaptChar))
                {
                    ErrField += "- " + Resources.resource.VerificationCode_Input + NewLine;


                }
            }


            if (String.IsNullOrEmpty(ErrField))
            { return true; }
            else
            {

                //Show error Message ---------------------------                      
                this.LblError.Text = errormessage.AlertText(ErrMsg + ErrField);
                //----------------------------------------------
                string strMsg = (ErrMsg + ErrField).Replace("<br/>", "\\n");

                sqlString.displayAlert(this, strMsg);
                return false;
            }
        }

        private Boolean Validate_IPIsBlocked()
        {
            String Member_Id = this.TxtUserId.Text.Trim();
            String IPAddr = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
            String SQL = "";
            SQL = " SELECT TOP 1 1 FROM tbl_blockedIP " +
            " WHERE 1 = 1 " +
            " AND IP = N'" + secure.RC(IPAddr) + "'" +
            //" AND memberID = N'" + secure.RC(Member_Id) + "'" +
            " AND isDeleted = 'false' ";

            wwdb db = new wwdb();
            db.OpenTable(SQL); db.Close();
            if (!db.HasError && db.RecordCount() >= 1)
            {
                //Ip + member Id Has Blocked ---------------------
                this.LblError.Text = errormessage.AlertText(Resources.resource.IPBlockError);
                sqlString.displayAlert(this, Resources.resource.IPBlockError);
                //------------------------------------------------
                return true;
            }
            { return false; }
        }

        private Boolean CaptChar_Validation()
        {
            //Perform Checking If Verification Code is visible;
            Boolean CaptChar_IsValidate = false;
            if (this.TxtVerificationCode.Visible == true)
            {
                String CaptChar_Val = this.TxtVerificationCode.Text.Trim();
                if (Session["captchaLogin"] != null)
                {
                    if (CaptChar_Val.ToLower() == Session["captchaLogin"].ToString().ToLower())
                    { 
                        CaptChar_IsValidate = true; 
                    }
                    else
                    //{ this.LblError.Text = sqlString.changeLanguage("* Incorrect Verification Code"); }
                    {
                        this.LblError.Text = errormessage.AlertText(Resources.resource.InvalidVerificationCodeError);

                        sqlString.displayAlert(this, Resources.resource.InvalidVerificationCodeError);

                    }
                }
                else
                {
                    this.LblError.Text = errormessage.AlertText(Resources.resource.VerificationCode_Expired);
                    sqlString.displayAlert(this, Resources.resource.VerificationCode_Expired);
                    this.IBtnRefresh_Click(null, null);
                }
            }
            else { CaptChar_IsValidate = true; }

            return CaptChar_IsValidate;
        }

        #endregion

        #region FUNCTION

        // return ip of user request
        protected string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }

        private void Login_System()
        {
            //Variable ------------------------------------------
            LoginUserModel login_model = new LoginUserModel();
            String UserId = this.TxtUserId.Text.Trim().Replace("'", "");
            String Password = this.TxtPassword.Text.Trim().Replace("'", "");
            String Encry_Password = secure.RC(secure.Encrypt(Password, true));
            string ip = GetIPAddress();
            bool access = false;
            //----------------------------------------------------

            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            //sql.Append(" SELECT * FROM tbl_BlockedIP WITH (NOLOCK) WHERE memberID=N'" + UserId + "' AND IP=N'" + ip + "';");
            //sql.Append(" SELECT * FROM tbl_BlockedIP WITH (NOLOCK) WHERE IP=N'" + ip + "' AND IsDeleted = 0;");
            //db.OpenTable(sql.ToString());

            //if (db.RecordCount() > 0 && !db.HasError)
            //{
           //     LogUtil.logLogin(UserId, Encry_Password, "Failed");
            //    this.LblError.Text = "The following IP (" + ip + ") has been blocked.";
             //   this.LblError.Visible = true;
            //    hidd_Count.Value = "1";
            //    trVerification.Visible = true;
            //    divImg.Visible = true;

                //InsertLoginAttempt(UserId, ip);
           // }
            //else
            //{
            if (login_model.VerifyLogin(UserId, Password))
            {
                LogUtil.logLogin(UserId, Encry_Password, "Success");
                LogUtil.CheckInUser(UserId);

                Session[KeyVal.loginsecure] = login_model;  
                Session[KeyVal.session_clogin] = login_model;
                
                if (login_model.isStaffAdmin())
                    Session.Timeout = 60;
                else
                    Session.Timeout = 20;

                access = true;

                //ClearLoginAttempt(UserId);
            }
            else
            {
                //InsertLoginAttempt(UserId, ip);
                String strsql = "Select (ISNULL((Select Convert(int,ParameterValue) from tbl_Parameter where ParameterName = 'MaximumLogin'),5) " +
                    " - ISNULL((Select COUNT(*) from tbl_LoginAttempt where log_User = '" + UserId + "' AND log_ip = '" + ip + "' AND isDeleted = 0), 0)) as AttemptsRemaining ";
                Object obj = db.executeScalar(strsql).ToString();
                String Attempts = "0";
                if (obj != null)
                {
                    Attempts = obj.ToString();
                }
                LogUtil.logLogin(UserId, Encry_Password, "Failed");
                this.LblError.Text = login_model.LoginMessage.ToString() + errormessage.AlertText(" * " + Attempts + " attempts remaining.");
                this.LblError.Visible = true;
                hidd_Count.Value = "1";
                trVerification.Visible = true;
                divImg.Visible = true;
            }
            //}
            db.Close();

            if (access)
            {
                if (Request.QueryString["path"] != null)
                {
                    //Check User Change the Password Or Not
                    String strsql = "Select IsChangedPassword from tbl_Login where login_id = '" + login_model.UserId + "'";
                    Object obj = db.executeScalar(strsql).ToString();
                    String IsChangedPassword = "0";
                    if (obj != null)
                    {
                        IsChangedPassword = obj.ToString();
                    }

                    if (IsChangedPassword == "0")
                        Response.Redirect("/Form/ChangePassword.aspx");


                    //Verify If Login User have to privillege to access the [Path] stated in URL -------------------------
                    string Redirect_Path;
                    try
                    {
                        Redirect_Path = secure.Decrypt(secure.DURC(Request.QueryString["path"].ToString()), true);
                    }
                    catch (Exception e)
                    {
                        Redirect_Path = "/Form/Report/ClientList.aspx";
                        LogUtil.logAction(e.Message + Environment.NewLine + ". Path = " + Request.QueryString["path"].ToString(), 
                            "Resolved URL path decryption error.");
                    }
                    if (this.Verify_Access(login_model.UserRole, Redirect_Path))
                    {
                        Response.Redirect(Redirect_Path);
                    }
                    else //Redirect To Default Page (If No Privllege To Access the [Path] in URL --------------------------
                    {
                        //Response.Redirect("/Form/Home.aspx");
                        //Response.Redirect("/Form/Loan/RequestList.aspx");
                        if (login_model.isStaffAdmin())
                            Response.Redirect("/Form/Report/ClientList.aspx");
                        else if (login_model.UserRole[0].Contains(("Easy Mode").ToString().ToUpper()))
                            Response.Redirect("/Form/Loan/EasyMode.aspx");
                        else
                        {
                            if (!Redirect_Path.Equals("/Form/Loan/NewRequest.aspx"))
                                Response.Redirect("/Form/Loan/NewRequest.aspx");
                            else
                                Response.Redirect("/Form/Report/ClientList.aspx");
                        }
                    }
                    //-----------------------------------------------------------------------------------------------------
                }
                else
                {
                    //Check User Change the Password Or Not
                    String strsql = "Select IsChangedPassword from tbl_Login where login_id = '" + login_model.UserId + "'";
                    Object obj = db.executeScalar(strsql).ToString();
                    String IsChangedPassword = "0";
                    if (obj != null)
                    {
                        IsChangedPassword = obj.ToString();
                    }

                    if (IsChangedPassword == "False")
                        Response.Redirect("/Form/ChangePassword.aspx");

                    //Response.Redirect("/Form/Home.aspx");
                    //Response.Redirect("/Form/Loan/RequestList.aspx");
                    if (login_model.isStaffAdmin())
                        Response.Redirect("/Form/Report/ClientList.aspx");
                    else if (login_model.UserRole[0].Contains(("Easy Mode").ToString().ToUpper()))
                        Response.Redirect("/Form/Loan/EasyMode.aspx");
                    else
                    {
                        if (this.Verify_Access(login_model.UserRole, "/Form/Loan/NewRequest.aspx"))
                            Response.Redirect("/Form/Loan/NewRequest.aspx");
                        else
                            Response.Redirect("/Form/Report/ClientList.aspx");
                    }
                }
            }
        }

        public void ClearLoginAttempt(string memId)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            try
            {
                sql.Append(" UPDATE tbl_LoginAttempt SET isDeleted=1 WHERE log_user=N'" + memId + "';");
                db.Execute(sql.ToString());

                // user login successfully.
                LogUtil.logAction(sql.ToString(), "Clear Login Attempt");
            }
            catch(Exception ex)
            {
                LogUtil.logError(ex.ToString(), sql.ToString());
            }
            finally
            {
                db.Close();
            }
        }

        public void InsertLoginAttempt(string memId, string ip)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            try
            {
                sql.AppendFormat(@" 
                    IF NOT EXISTS(SELECT * FROM tbl_BlockedIP WITH (NOLOCK) WHERE IP = N'{0}' AND IsDeleted = 0)
                    BEGIN DECLARE @COUNT AS INT = ISNULL((SELECT ParameterValue FROM TBL_PARAMETER WITH (NOLOCK)
                    WHERE Category = 'SECURITY' AND ParameterName = 'MaximumLogin'),0);
                    IF ((SELECT COUNT(id) FROM tbl_LoginAttempt WITH (NOLOCK) WHERE log_user = N'{2}' AND log_ip = N'{0}' AND isDeleted = 0) >= @COUNT)
                    BEGIN INSERT INTO tbl_BlockedIP (memberID, IP, createdAt, isDeleted) VALUES (N'{1}', N'{0}', GETDATE(), '0'); END END
                    
                    INSERT INTO tbl_LoginAttempt (log_user,log_ip,log_datetime,isDeleted) VALUES
                    (N'{1}', N'{0}', GETDATE(), '0');
                ", ip, memId);

                db.Execute(sql.ToString());

                LogUtil.logAction(sql.ToString(), "Insert Login Attempt");
            }
            catch (Exception ex) { LogUtil.logError(ex.ToString(), sql.ToString()); }
            finally { db.Close(); }
        }

        private Boolean Verify_Access(List<string> Current_Role, String Current_URL)
        {
            //Verify If The Role Have privillege to access the page -------------------------------
            wwdb db = new wwdb();
            String SQL = string.Format(@"
                            SELECT TOP 1 1 FROM tbl_RoleMenu TRM 
                            WHERE 1 = 1 
                            AND TRM.MenuID IN (SELECT RowId FROM tbl_Menu TM WHERE TM.Type = 1 AND TM.LinkURL = N'{1}') 
                            AND TRM.RoleId IN ({0})
                        ", string.Join(",", Current_Role), secure.RC(Current_URL));
            //-------------------------------------------------------------------------------------

            db.OpenTable(SQL);
            db.Close();

            if (db.RecordCount() > 0)
            { return true; }
            else
            { return false; }
        }

        #endregion

        #region BUTTON EVENT

        protected void BtnLogin_Click(object sender, EventArgs e)
        {
            if (this.Login_Validation())
            {
                this.Login_System();
            }

        }

        protected void IBtnRefresh_Click(object sender, ImageClickEventArgs e)
        {
            this.LoadCaptcha();
        }

        #endregion

        protected void Unnamed_ServerClick(object sender, EventArgs e)
        {
            //string a = HttpContext.Current.Request.Url.AbsolutePath;

            //a += "?a=" + sqlString.encryptURL("asdfNSIDOF");

            //if (Request.QueryString["path"] != null)
            //    a += "&path=" + sqlString.encryptURL(sqlString.decryptURL(Request.QueryString["path"].ToString()));

            //if (TxtUserId.Text.Trim() != string.Empty)
            //    a += "&id=" + sqlString.encryptURL(TxtUserId.Text);

            //if (TxtPassword.Text.Trim() != string.Empty)
            //    a += "&pa=" + sqlString.encryptURL(TxtPassword.Text);

            //Response.Redirect(a);

            LoadCaptcha();
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
       
            sqlString.OpenNewWindow_Center("/Upload/Consent/Disclaimer.pdf", "Consent Form", 600, 600, this);
        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Form/MemberRegister.aspx");
        }

        private void Bind_BackgroundImage()
        {
            String SQL = String.Empty;
            wwdb db = new wwdb();

            SQL = "SELECT TOP 1 FullPath, ImageURl, ToolTip, URL FROM tbl_ImagePath TI WHERE TI.[IsDeleted] = 0 " +
                  " ORDER BY RowId DESC ";
            db.OpenTable(SQL); db.Close();
            if (!db.HasError && db.RecordCount() == 1)
            {
                String ImgPath = db.Item("ImageUrl");
                Uri myUri;
                if (Uri.TryCreate(ImgPath, UriKind.RelativeOrAbsolute, out myUri))
                {                   
                    HtmlGenericControl css = new HtmlGenericControl();
                    css.TagName = "style";
                    css.Attributes.Add("type", "text/css");
                    css.InnerHtml = @"body{background-image: url('" + ImgPath + "');  position: relative;width: 100%;" +
                        "overflow: hidden;background-position: center center;z-index: 1;background-repeat:no-repeat;background-size:100% 100%;";

                    Page.Header.Controls.Add(css);
                }              
            }

        }

        protected void btnOpenAnnouncement_Click(object sender, EventArgs e)
        {
            hfAnnouncementWindow.Value = "show";
        }
    }
}