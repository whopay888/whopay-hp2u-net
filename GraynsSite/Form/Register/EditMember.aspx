﻿<%@ Register Src="~/Form/Register/MemberRoleRegistration.ascx" TagName="MemberRoleRegistration" TagPrefix="mrr1" %>

<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Global1.Master" CodeBehind="EditMember.aspx.cs" Inherits="HJT.Form.Register.EditMember" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function confirmSubmit() {
            return confirm('<%= GetGlobalResourceObject("resource" , "ConfirmToUpdate") %>');
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <aside class="right-side">
        <section class="content-header">
            <h1>
                <span class="icon fa fa-fw fa-book" aria-hidden="true"></span>
                <asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><span runat="server" id="spHeader"><%= GetGlobalResourceObject("resource", "Member_Registration")%></span></asp:Label>
            </h1>
            <asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
        </section>
        <section class="content">
            <div class="col-md-12">
                <div runat="server">
                    <div class="box">
                        <h4>
                            <asp:Label ID="panel" runat="server" Text="<%$ Resources:Resource, Edit_Member%>"></asp:Label>

                        </h4>

                        <div id="divErrorMessage" runat="server" visible="false" class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <b><%= GetGlobalResourceObject("resource", "Alert")%></b>
                            <br />
                            <asp:Label runat="server" ID="lblErr"></asp:Label>
                        </div>
                        <div class="box-body">



                            <table class="table table-bordered">
                                <tr class="form-group">
                                    <td><%= GetGlobalResourceObject("resource", "Username")%><span style="color: Red;">&nbsp;*</span></td>
                                    <td class="controls">
                                        <asp:TextBox runat="server" ID="txtUserName" CssClass="form-control" AutoPostBack="true" Enabled="false" OnTextChanged="txtUserName_TextChanged"> </asp:TextBox>

                                    </td>

                                </tr>

                                <tr class="form-group">
                                    <td><%= GetGlobalResourceObject("resource", "Password")%><span style="color: Red;">&nbsp;*</span></td>
                                    <td class="controls">
                                        <asp:TextBox runat="server" ID="txtPassword" CssClass="form-control"> </asp:TextBox>

                                    </td>

                                </tr>


                                <tr class="form-group">
                                    <td><%= GetGlobalResourceObject("resource", "FullName")%><span style="color: Red;">&nbsp;*</span></td>
                                    <td class="controls">
                                        <asp:TextBox runat="server" ID="txtName" CssClass="form-control"> </asp:TextBox>

                                    </td>

                                </tr>



                                <tr class="form-group">
                                    <td><%= GetGlobalResourceObject("resource", "Role")%><span style="color: Red;">&nbsp;*</span></td>
                                    <td class="controls">
                                        <table style="width: 100%; padding: 0px; border-spacing: 0px">
                                            <tr>
                                                <td style="width: 90%; vertical-align: top">
                                                    <asp:DropDownList runat="server" ID="ddlRole" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlRole_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 10%; vertical-align: top">
                                                    <asp:Button ID="btnAddRole" Text="<%$ Resources:Resource, Add%>" runat="server" CssClass="btn btn-primary" />
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:PlaceHolder ID="ph1" runat="server" />
                                    </td>

                                </tr>

                                <tr class="form-group">
                                    <td><%= GetGlobalResourceObject("resource", "Branch")%></td>
                                    <td class="controls">
                                        <asp:DropDownList runat="server" ID="ddlBranch" CssClass="form-control">
                                        </asp:DropDownList>

                                    </td>

                                </tr>


                                <tr class="form-group">
                                    <td><%= GetGlobalResourceObject("resource", "ContactNumber")%><span style="color: Red;">&nbsp;*</span></td>
                                    <td class="controls">
                                        <asp:TextBox runat="server" ID="txtMobile" CssClass="form-control"> </asp:TextBox>

                                    </td>

                                </tr>



                                <tr class="form-group">
                                    <td><%= GetGlobalResourceObject("resource", "Email")%><span style="color: Red;">&nbsp;*</span></td>
                                    <td class="controls">
                                        <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control"> </asp:TextBox>

                                    </td>

                                </tr>


                            </table>

                        </div>

                        <div class="box-footer">

                            <asp:Button ID="btnSubmit" Text="<%$ Resources:Resource, Update%>" runat="server" CssClass="btn btn-primary" OnClientClick="return confirmSubmit(); " OnClick="btnSubmit_Click" />

                        </div>
                    </div>
                </div>
                <asp:Literal ID="ltlCount" runat="server" Text="0" Visible="false" />
                <asp:Literal ID="ltlRemoved" runat="server" Visible="false" />
            </div>

        </section>
    </aside>


</asp:Content>
