﻿using CTOS.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Xsl;

namespace HJT.Form
{
    public partial class APITest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
 
        }


        private void method2()
        {
            var xmlText = @"<?xml version=""1.0"" encoding=""UTF-8""?>
            <soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
                <soapenv:Body>
                    <tns:request xmlns:tns=""http://ws.proxy.xml.ctos.com.my/"">
                        <input>{0}</input>
                    </tns:request>
                </soapenv:Body>
            </soapenv:Envelope>";

            var payload = @"<batch output=""{0}"" no=""0009"" xmlns=""http://ws.cmctos.com.my/ctosnet/request"">
                <company_code>synergyuat</company_code>
                <account_no>synergyuat</account_no>
                <user_id>sm_xml</user_id>
                <record_total>1</record_total>
                <records>
                <type>{1}</type>
                <ic_lc>{2}</ic_lc>
                <nic_br>{3}</nic_br>
                <name>{4}</name>
                <mphone_nos/>
                <ref_no></ref_no>
                <dist_code>PENANG BRANCH</dist_code>
                <purpose code=""200"">Credit evaluation/account opening on subject/directors/shareholder with consent /due diligence on AMLA compliance</purpose>
                <include_consent>0</include_consent>
                <include_ctos>1</include_ctos>
                <include_trex>1</include_trex>
                <include_ccris sum=""1"">1</include_ccris>
                <include_dcheq>0</include_dcheq>
                <include_ccris_supp>1</include_ccris_supp>
                <include_fico>0</include_fico>
                <include_sfi>0</include_sfi>
                <include_ssm>0</include_ssm>
                <confirm_entity>90007</confirm_entity>
                </records>
            </batch>";

            string xmlRequestBody = string.Format(xmlText, System.Net.WebUtility.HtmlEncode(string.Format(payload, "0", txtTYpe.Text, txtOldIC.Text, txtIC.Text, txtName.Text)));
            string htmlRequestBody = string.Format(xmlText, System.Net.WebUtility.HtmlEncode(string.Format(payload, "1", txtTYpe.Text, txtOldIC.Text, txtIC.Text, txtName.Text)));
            string _result;

            #region xml
            var xmlRequest = (HttpWebRequest)WebRequest.Create("https://uat.cmctos.com.my:8443/ctos/Proxy");
            xmlRequest.Method = "POST";
            xmlRequest.ContentType = "text/xml";
            xmlRequest.Headers["username"] = "sm_xml";
            xmlRequest.Headers["password"] = "a1234567";

            var xmlWriter = new StreamWriter(xmlRequest.GetRequestStream());
            xmlWriter.Write(xmlRequestBody);
            xmlWriter.Close();

            var xmlResponse = (HttpWebResponse)xmlRequest.GetResponse();

            var xmlStreamResponse = xmlResponse.GetResponseStream();
            var xmlStreamRead = new StreamReader(xmlStreamResponse);
            _result = xmlStreamRead.ReadToEnd().Trim();

            int startIndex = _result.IndexOf("<return>") + 8;
            int endIndex = _result.IndexOf("</return>");

            var result2 = _result.Substring(startIndex, endIndex - startIndex);
            byte[] data = Convert.FromBase64String(result2);
            string decodedString = Encoding.UTF8.GetString(data);

            xmlStreamRead.Close();
            xmlStreamResponse.Close();
            xmlResponse.Close();

            #endregion

            #region xml to object
            XmlSerializer serializer = new XmlSerializer(typeof(report));
            StringReader stringReader = new StringReader(decodedString);
            report report = (report)serializer.Deserialize(stringReader);
            var enquiry = report.enq_report.enquiry.FirstOrDefault();
            if (enquiry != null)
            {
                var accountList = enquiry.section_ccris.accounts.account.ToList();
            }

            #endregion

            #region xml to html using style sheet file
            XmlReader xmlReader = XmlReader.Create(new StringReader(decodedString));
            var myXslTrans = new XslCompiledTransform();
            myXslTrans.Load(Server.MapPath(@"..\CTOS\ctos_report.xsl"));

            StringBuilder builder = new StringBuilder();
            using (StringWriter stringWriter = new StringWriter(builder))
            {
                using (XmlTextWriter htmlTextWriter = new XmlTextWriter(stringWriter))
                {
                    myXslTrans.Transform(xmlReader, htmlTextWriter);
                }
            }
            string transformedHtml = builder.ToString();

            #endregion
            #region html
            var htmlRequest = (HttpWebRequest)WebRequest.Create("https://uat.cmctos.com.my:8443/ctos/Proxy");
            htmlRequest.Method = "POST";
            htmlRequest.ContentType = "text/xml";
            htmlRequest.Headers["username"] = "sm_xml";
            htmlRequest.Headers["password"] = "a1234567";

            var htmlWriter = new StreamWriter(htmlRequest.GetRequestStream());
            htmlWriter.Write(htmlRequestBody);
            htmlWriter.Close();

            var htmlResponse = (HttpWebResponse)htmlRequest.GetResponse();

            var htmlStreamResponse = htmlResponse.GetResponseStream();
            var htmlStreamRead = new StreamReader(htmlStreamResponse);
            _result = htmlStreamRead.ReadToEnd().Trim();

            startIndex = _result.IndexOf("<return>") + 8;
            endIndex = _result.IndexOf("</return>");

            result2 = _result.Substring(startIndex, endIndex-startIndex);
            data = Convert.FromBase64String(result2);
            decodedString = Encoding.UTF8.GetString(data);

            htmlStreamRead.Close();
            htmlStreamResponse.Close();
            htmlResponse.Close();
            Response.Write(decodedString);

            #endregion
        }

        public string ReturnXmlDocument(HttpRequestMessage request)
        {
            var doc = new XmlDocument();
            doc.Load(request.Content.ReadAsStreamAsync().Result);
            return doc.DocumentElement.OuterXml;
        }

        protected void CHeck_Click(object sender, EventArgs e)
        {
            method2();

        }
    }
}