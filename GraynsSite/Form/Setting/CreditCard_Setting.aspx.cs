﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Model;
using Synergy.Util;
using Synergy.Helper;

namespace WMElegance.Form.Setting
{
    public partial class CreditCard_Setting : System.Web.UI.Page
    {
        LoginUserModel login_user = null;

        protected void Page_Load(object sender, EventArgs e)
        {
           

            login_user = (LoginUserModel)Session[KeyVal.loginsecure];

            if (!IsPostBack)
            {
                sqlString.bindControl(gv, getExData(null, null, true));
            }

            //if (gv.Rows.Count > 0)
            //{
            //    ApplyPaging(-1);
            //}
        }


        #region GetData

        // Action[0], MemberID[1], Name[2], Ranking[3]*, Date[4], Status[5]
        protected string getExData(string columnName, string sortOrder, bool hasOrder)
        {
            StringBuilder sql = new StringBuilder();

            sql.Clear();

            sql.Append(" SELECT b.BankID AS 'BankID', b.BankName AS 'BankName', ISNULL(c.Minimum, '0.00') AS 'Minimum' FROM  ");

            sql.Append(" (SELECT CAST(BankID AS INT) AS BankID, BankName from tbl_BankName WITH (NOLOCK) WHERE  IsDeleted = 'False' AND Status = 'A') AS b ");
            sql.Append(" LEFT JOIN tbl_CreditCard c WITH (NOLOCK) ON b.BankID = c.BankID ");
            sql.Append("ORDER BY b.[BankID] ASC");

            LogUtil.logSQL(sql.ToString());

            return sql.ToString();
        }

        #endregion

        #region GridView - GV

        private SortDirection GridViewSortDirection1
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;

                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }
        }

        protected void gv_DataBound(object sender, EventArgs e)
        {
            if (gv.Rows.Count > 0)
            {
               
            }
        }




        protected void gv_Sorting(object sender, GridViewSortEventArgs e)
        {
            string direction = "ASC";

            if (GridViewSortDirection1 == SortDirection.Ascending)
            {
                GridViewSortDirection1 = SortDirection.Descending;
                direction = "DESC";
            }
            else
            {
                GridViewSortDirection1 = SortDirection.Ascending;
                direction = "ASC";
            }

            ViewState["Field1"] = e.SortExpression; ViewState["Direction1"] = direction;

            sqlString.bindControl(gv, getExData(e.SortExpression, direction, true));
        }

        #endregion

        #region Control Event

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            sqlString.bindControl(gv, getExData(null, null, true));
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            sqlString.exportFunc("City List", getExData(null, null, false));
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            //txtSearchBankIDName.Text = "";
            sqlString.bindControl(gv, getExData(null, null, true));
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            if (ValidateForm())
            {
                try
                {

                    sql.Clear();
                    sql.Append(" INSERT INTO tbl_CreditCard_History(BankID, Minimum, CreatedBy, CreatedAt)");
                    sql.Append(" SELECT BankID, Minimum, '" + secure.RC(login_user.UserId) + "' AS 'CreatedBy' , GetDate() AS 'CreatedAt'");
                    sql.Append(" FROM tbl_CreditCard WITH(NOLOCK)");
                    db.Execute(sql.ToString());

                    for (int i = 0; i < gv.Rows.Count; i++)
                    {
                        TextBox txtmin = (TextBox)gv.Rows[i].Cells[2].FindControl("txtMinimum");
                        string id = gv.Rows[i].Cells[0].Text.ToString().Trim();
                        sql.Clear();
                        sql.Append(" EXEC SP_CreditCard_DeleteInsert ");
                        sql.Append(" @ID = N'" + secure.RC(id.ToString()) + "' , ");
                        sql.Append(" @Minimum = N'" + secure.RC(txtmin.Text) + "' , ");
                        sql.Append(" @CreatedBy = N'" + secure.RC(login_user.UserId) + "'; ");

                        db.Execute(sql.ToString());
                        string strLog = "Update Credit Card - (BankID : " + id.ToString() + " , Minimum : " + txtmin.Text.ToString() + " )";
                        LogUtil.logAction(sql.ToString(), strLog.ToString());

                    }

                    sqlString.displayAlert(this, "Update Successful");
                    sqlString.bindControl(gv, getExData(null, null, true));
                }

                catch (Exception ex) { LogUtil.logError(ex.ToString(), sql.ToString()); sqlString.displayAlert(this, KeyVal.UnknownError); }
                finally { db.Close(); }
            }
        }

        #region GridView Control

        protected void btnView_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            //int row = Convert.ToInt32(btn.CommandArgument);
            Response.Redirect("/Form/Setting/AddCity.aspx?mid=" + sqlString.encryptURL(((GridViewRow)btn.NamingContainer).Cells[0].Text));
        }

        protected void txtMinimum_TextChanged(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            StringBuilder errMsg = new StringBuilder();

                if (String.IsNullOrEmpty(txt.Text.Trim()))
                {
                    errMsg.Append(Resources.resource.MinimumPaymentCannotEmpty + "\\n");
                }
                else 
                {
                    if (Validation.isDecimal(txt.Text.ToString()))
                    {
                        if (ConvertHelper.ConvertToDecimal(txt.Text.ToString(), 2) < 0)
                        {
                            errMsg.Append(Resources.resource.MinimumPaymentCannotLessThanZero + "\\n");
                        }
                    }
                    else
                    {
                        errMsg.Append(Resources.resource.MinimumPaymentCannotString + "\\n");
                    }
                }
           
                if (errMsg.ToString().Trim() != string.Empty)
                {
                    sqlString.displayAlert2(this, errMsg.ToString());
                    lblErr.Text = errMsg.Replace("\\n", "<br />").ToString();
                    lblErr.Visible = true;
                    divErrorMessage.Visible = true;
                    lblErr.Focus();
                }
                else
                {
                    lblErr.Text = "";
                    lblErr.Visible = false;
                    divErrorMessage.Visible = false;
                }


        }

        #endregion


        protected bool ValidateForm()
        {
            bool result = false;
            StringBuilder errMsg = new StringBuilder();

            for (int i = 0; i < gv.Rows.Count; i++)
            {
                TextBox txt = (TextBox)gv.Rows[i].Cells[2].FindControl("txtMinimum");
                if (String.IsNullOrEmpty(txt.Text.Trim()))
                {
                    errMsg.Append(Resources.resource.MinimumPaymentCannotEmpty + "\\n");
                }
                else
                {
                    if (Validation.isDecimal(txt.Text.ToString()))
                    {
                        if (ConvertHelper.ConvertToDecimal(txt.Text.ToString(), 2) < 0)
                        {
                            errMsg.Append(Resources.resource.MinimumPaymentCannotLessThanZero + "\\n");
                        }
                    }
                    else
                    {
                        errMsg.Append(Resources.resource.MinimumPaymentCannotString + "\\n");
                    }
                }
            }

            if (errMsg.ToString().Trim() != string.Empty)
            {
                sqlString.displayAlert2(this, errMsg.ToString());
                lblErr.Text = errMsg.Replace("\\n", "<br />").ToString();
                lblErr.Visible = true;
                divErrorMessage.Visible = true;
                lblErr.Focus();
            }
            else
            {
                lblErr.Text = "";
                lblErr.Visible = false;
                divErrorMessage.Visible = false;
                result = true;
            }

            return result;
        }

        #endregion

    }
}