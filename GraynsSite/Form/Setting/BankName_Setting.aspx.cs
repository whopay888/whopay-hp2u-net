﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Model;
using Synergy.Util;
using Synergy.Helper;
using HJT.Cls.Controller;

namespace WMElegance.Form.Setting
{
    public partial class BankName_Setting : System.Web.UI.Page
    {
        LoginUserModel login_user = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];

            if (!IsPostBack)
            {
                sqlString.bindControl(gv, getExData(null, null, true));
                sqlString.bindControl(gvPolicy, getPolicyList());
                binddata();
            }
        }

        #region Tab1
        #region GetData

        // Action[0], MemberID[1], Name[2], Ranking[3]*, Date[4], Status[5]
        protected string getExData(string columnName, string sortOrder, bool hasOrder)
        {
            StringBuilder sql = new StringBuilder();

            sql.Clear();
            sql.AppendFormat(@"
                SELECT CAST(a.BankID AS INT) AS 'BankID',  a.BankName AS 'Bank_Name', 
	            ISNULL(a.[IsSelectedDefault], 'false') as 'IsSelectedDefault'
	            FROM tbl_BankName a WITH (NOLOCK)  
	            WHERE status = 'A' AND isDeleted = 'False'
            ");

            if (txtSearchBankIDName.Text != String.Empty)
            {
                sql.Append(" AND (a.BankName LIKE '%"+ txtSearchBankIDName.Text +"%' OR a.BankID LIKE '%"+txtSearchBankIDName.Text+"%')");
            }

            sql.Append("ORDER BY cast(A.[BankID] as int) ASC");

            LogUtil.logSQL(sql.ToString());

            return sql.ToString();
        }

        #endregion

        #region GridView - GV

        private SortDirection GridViewSortDirection1
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;

                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }
        }

        protected void gv_DataBound(object sender, EventArgs e)
        {
            if (gv.Rows.Count > 0)
            {
                gv.HeaderRow.Cells[1].Visible = false;
                gv.FooterRow.Cells[1].Visible = false;

                for (int a = 0; a < gv.Rows.Count; a++)
                {
                    gv.Rows[a].Cells[1].Visible = false;

                    Button btn = (Button)gv.Rows[a].FindControl("btnActive");
                    Button btn2 = (Button)gv.Rows[a].FindControl("btnDeactive");

                    if (gv.Rows[a].Cells[4].Text == "Active")
                    {
                        btn.Visible = false;
                        btn2.Visible = true;
                    }
                    else
                    {
                        btn.Visible = true;
                        btn2.Visible = false;
                    }

                    TableCell cell = gv.Rows[a].Cells[0];
                    gv.Rows[a].Cells.Remove(cell);
                    gv.Rows[a].Cells.Add(cell);
                }

                TableCell headerCell = gv.HeaderRow.Cells[0];
                gv.HeaderRow.Cells.Remove(headerCell);
                gv.HeaderRow.Cells.Add(headerCell);

                TableCell footerCell = gv.FooterRow.Cells[0];
                gv.FooterRow.Cells.Remove(footerCell);
                gv.FooterRow.Cells.Add(footerCell);
            }
        }
        protected void gv_Sorting(object sender, GridViewSortEventArgs e)
        {
            string direction = "ASC";

            if (GridViewSortDirection1 == SortDirection.Ascending)
            {
                GridViewSortDirection1 = SortDirection.Descending;
                direction = "DESC";
            }
            else
            {
                GridViewSortDirection1 = SortDirection.Ascending;
                direction = "ASC";
            }

            ViewState["Field1"] = e.SortExpression; ViewState["Direction1"] = direction;

            sqlString.bindControl(gv, getExData(e.SortExpression, direction, true));
        }
        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chkShowInReport = (CheckBox)e.Row.FindControl("chkShowInReport");
                bool isShowing = (bool)DataBinder.Eval(e.Row.DataItem, "IsSelectedDefault");

                chkShowInReport.Checked = isShowing;
            }
        }
        #endregion

        #region Control Event
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            sqlString.bindControl(gv, getExData(null, null, true));
        }
        protected void btnExport_Click(object sender, EventArgs e)
        {
            sqlString.exportFunc("City List", getExData(null, null, false));
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            txtSearchBankIDName.Text = "";
            sqlString.bindControl(gv, getExData(null, null, true));
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            TextBox txt = (TextBox)((GridViewRow)btn.NamingContainer).Cells[1].FindControl("txtBankName");
            string id = ((GridViewRow)btn.NamingContainer).Cells[0].Text.ToString().Trim();
            wwdb db = new wwdb();
            string sql = "";

            try
            {
                if (txt.Text.Trim() != String.Empty)
                {
                    sql = " UPDATE tbl_BankName SET BankName=N'" + secure.RC(txt.Text.ToString()) + "',updatedBy = N'" + secure.RC(login_user.UserId) + "' " +
                        " , UpdatedAt = GETDATE() WHERE BankID = N'" + secure.RC(id) + "'; ";
                    db.Execute(sql);

                    LogUtil.logAction(sql, "Update Bank");

                    sqlString.displayAlert(this, "Action_Successful");
                    sqlString.bindControl(gv, getExData(null, null, true));
                }
                else
                {
                    sqlString.displayAlert(this, "Textbox cannot be empty");
                    sqlString.bindControl(gv, getExData(null, null, true));
                }
            }

            catch (Exception ex) { LogUtil.logError(ex.ToString(), sql); sqlString.displayAlert(this, KeyVal.UnknownError); }
            finally { db.Close(); }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (validateForm())
            {
                wwdb db = new wwdb();
                StringBuilder sql = new StringBuilder();
                int newID;

                try
                {
                    sql.Clear();
                    sql.Append("Select TOP 1 CAST(BankID AS INT) AS BankID from tbl_BankName WITH (NOLOCK) ORDER BY BankID desc");
                    db.OpenTable(sql.ToString());
                    if (db.RecordCount() > 0)
                    {
                        newID = ConvertHelper.ConvertToInt(db.Item("BankID").ToString(), 0) + 1;
                    }
                    else
                    {
                        newID = 1;
                    }

                    sql.Clear();
                    sql.AppendFormat(@" 
                        Insert INTO tbl_BankName (BankID, BankName, Status, IsDeleted, CreatedBy, CreatedAt, UpdatedAt, UpdatedBy) 
                        VALUES ('{0}', '{1}', 'A', 'false', '{2}', GETDate(), GetDate(), '{3}');
                    ", secure.RC(newID.ToString()), secure.RC(txtNewBank.Text), secure.RC(login_user.UserId), secure.RC(login_user.UserId));

                    //sql.Append(" INSERT INTO tbl_Area (Area_Name, State_RowID, isActive, CreatedBy,UpdatedBy,UpdatedAt) VALUES " +
                    //    " (N'" + secure.RCL(txtNewBank.Text) + "', N'" + secure.RC(ddlState.SelectedValue) + "', 1, N'" + secure.RC(login_user.UserId) + "', N'" + secure.RC(login_user.UserId) + "',GetDate());");

                    db.Execute(sql.ToString());

                    sqlString.displayAlert(this, "Action_Successful");

                    txtNewBank.Text = "";

                }
                catch (Exception ex)
                { LogUtil.logError(ex.ToString(), sql.ToString()); sqlString.displayAlert(this, KeyVal.UnknownError); }
                finally
                { 
                    db.Close();
                    sqlString.bindControl(gv, getExData(null, null, true));
                }
            }
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            string id = ((GridViewRow)btn.NamingContainer).Cells[0].Text.ToString().Trim();
            wwdb db = new wwdb();
            string sql = "";

            try
            {

                //terminal
                //sql = " UPDATE tbl_login SET login_status = '0' WHERE login_id = (SELECT memberId FROM tbl_memberInfo WITH (NOLOCK) WHERE memberid = N'" + secure.RC(id) + "'); " +
                //    " UPDATE tbl_memberInfo SET status = 'D', updatedBy = N'" + secure.RC(login_user.UserId) + "' " +
                //    " , UpdatedAt = GETDATE() WHERE memberid = N'" + secure.RC(id) + "'; ";

                sql = string.Format(@" 
                    UPDATE tbl_BankName SET status = 'T', IsDeleted = 'True', 
                        updatedBy = N'{0}', UpdatedAt = GETDATE() 
                    WHERE BankID = N'{1}'; 
                ", secure.RC(login_user.UserId), secure.RC(id));
                db.Execute(sql);
                LogUtil.logAction(sql, "Delete Bank");

                sqlString.displayAlert(this, "Action_Successful");
                sqlString.bindControl(gv, getExData(null, null, true));
            }

            catch (Exception ex) { LogUtil.logError(ex.ToString(), sql); sqlString.displayAlert(this, KeyVal.UnknownError); }
            finally { db.Close(); }
        }
        protected bool validateForm()
        {
            bool result = false;
            StringBuilder errMsg = new StringBuilder();
            StringBuilder sql = new StringBuilder();
            wwdb db = new wwdb();

            try
            {
                if (string.IsNullOrEmpty(txtNewBank.Text.Trim()))
                {
                    errMsg.Append(Resources.resource.Bank_Note + "\\n");
                }
            }
            catch (Exception ex) { LogUtil.logError(ex.ToString(), sql.ToString()); errMsg.Append(sqlString.changeLanguage(KeyVal.UnknownError) + "\\n"); }
            finally { db.Close(); }

            if (errMsg.ToString().Trim() != string.Empty)
            {
                sqlString.displayAlert2(this, errMsg.ToString());
                lblErr.Text = errMsg.Replace("\\n", "<br />").ToString();
                lblErr.Visible = true;
                divErrorMessage.Visible = true;
                lblErr.Focus();
            }
            else
            {
                lblErr.Text = "";
                lblErr.Visible = false;
                divErrorMessage.Visible = false;
                result = true;
            }

            return result;
        }
        #region GridView Control
        protected void btnView_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            //int row = Convert.ToInt32(btn.CommandArgument);
            Response.Redirect("/Form/Setting/AddCity.aspx?mid=" + sqlString.encryptURL(((GridViewRow)btn.NamingContainer).Cells[0].Text));
        }
        #endregion
        #endregion

        protected void btnSetBank_Click(object sender, EventArgs e)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            try
            {
                sql.Clear();
                foreach (GridViewRow row in gv.Rows)
                {
                    //to get the dropdown of each line
                    CheckBox chkshowinreport = (CheckBox)row.FindControl("chkShowInReport");

                    sql.Append("UPDATE tbl_BankName SET IsSelectedDefault = " + (chkshowinreport.Checked ? "1" : "0") + " WHERE BankID = '" + row.Cells[0].Text + "';");
                }
                db.Execute(sql.ToString());

                if (db.HasError)
                    sqlString.displayAlert(this, KeyVal.UnknownError);
                else
                {
                    sqlString.displayAlert(this, KeyVal.ActionSuccess);
                    sqlString.bindControl(gv, getExData(null, null, true));
                }
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.ToString(), sql.ToString());
                sqlString.displayAlert(this, KeyVal.UnknownError);
            }
            finally { db.Close(); }
        }
        protected void btnActive_Click(object sender, EventArgs e)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            try
            {
                Button btn = (Button)sender;

                sql.Clear();
                sql.Append(" UPDATE tbl_Area SET IsActive = 1, isDeleted = 0 WHERE id = '" + secure.RC(((GridViewRow)btn.NamingContainer).Cells[0].Text) + "'; ");
                db.Execute(sql.ToString());

                sqlString.displayAlert(this, KeyVal.ActionSuccess);
                sqlString.bindControl(gv, getExData(null, null, true));

                LogUtil.logAction(sql.ToString(), "Activate Country");

                sqlString.bindControl(gv, getExData(null, null, true));
            }
            catch (Exception ex) { LogUtil.logError(ex.ToString(), sql.ToString()); sqlString.displayAlert(this, KeyVal.UnknownError); }
            finally { db.Close(); }
        }

        protected void btnDeactive_Click(object sender, EventArgs e)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            try
            {
                Button btn = (Button)sender;

                sql.Clear();
                sql.Append(" UPDATE tbl_Area SET IsActive = 0 WHERE id = '" + secure.RC(((GridViewRow)btn.NamingContainer).Cells[0].Text) + "'; ");
                db.Execute(sql.ToString());

                sqlString.displayAlert(this, KeyVal.ActionSuccess);
                sqlString.bindControl(gv, getExData(null, null, true));

                LogUtil.logAction(sql.ToString(), "Deactivate Country");

                //sqlString.bindControl(gv,getExData(null,null,true));
            }
            catch (Exception ex) { LogUtil.logError(ex.ToString(), sql.ToString()); sqlString.displayAlert(this, KeyVal.UnknownError); }
            finally { db.Close(); }
        }

        private void ApplyPaging(int pageindex)
        {
            if (pageindex > 0)
            {
                gv.PageIndex = pageindex;
            }
            GridViewRow row = gv.BottomPagerRow;
            PlaceHolder phPaging;
            LinkButton lnkPaging;

            LinkButton lnkPrevPage;
            LinkButton lnkNextPage;

            phPaging = (PlaceHolder)row.FindControl("phPaging");
            //lnkPrevPage = new LinkButton();
            //lnkPrevPage.Text = Server.HtmlEncode("← Previous");
            //lnkPrevPage.CommandName = "Page";
            //lnkPrevPage.CommandArgument = "prev";
            //lnkPrevPage.CssClass = "prev";

            lnkPrevPage = new LinkButton();
            lnkPrevPage.Text = Server.HtmlEncode(Resources.resource.First);
            lnkPrevPage.CommandName = "Page";
            lnkPrevPage.CommandArgument = "First";
            lnkPrevPage.CssClass = "prev";

            // ltrPaging.Controls.Add(lnkFirstPage);
            if (gv.PageIndex == 0)
            {
                lnkPrevPage.Enabled = false;


            }
            phPaging.Controls.Add(lnkPrevPage);
            int startpage = 1;
            int endpage = gv.PageCount;
            if (gv.PageIndex - 3 < 0 && startpage + 3 < gv.PageCount)
            {
                startpage = 1;
                endpage = startpage + 4;
            }
            else if (gv.PageIndex - 2 > 0 && gv.PageIndex + 2 < gv.PageCount)
            {
                startpage = gv.PageIndex - 2;
                endpage = gv.PageIndex + 2;
                startpage += 1;
                endpage += 1;
            }
            else if (gv.PageIndex - 2 > 0 && gv.PageIndex + 2 >= gv.PageCount)
            {
                startpage = gv.PageCount - 4;
                endpage = gv.PageCount;
            }



            for (int i = startpage; i <= endpage; i++)
            {
                if (i > 0)
                {
                    lnkPaging = new LinkButton();

                    lnkPaging.Text = i.ToString();
                    lnkPaging.CommandName = "Page";
                    lnkPaging.CommandArgument = i.ToString();
                    if (i == gv.PageIndex + 1)
                    {
                        lnkPaging.BackColor = System.Drawing.Color.FromArgb(66, 139, 202);
                        lnkPaging.Enabled = false; lnkPaging.Attributes.Add("style", "font-weight: bold;color: black;");
                    }
                    phPaging = (PlaceHolder)row.FindControl("phPaging");
                    phPaging.Controls.Add(lnkPaging);
                }
            }

            //lnkNextPage = new LinkButton();
            //lnkNextPage.Text = Server.HtmlEncode("Next → ");
            //lnkNextPage.CommandName = "Page";
            //lnkNextPage.CommandArgument = "next";
            //lnkNextPage.CssClass = "next";

            lnkNextPage = new LinkButton();
            lnkNextPage.Text = Server.HtmlEncode(Resources.resource.Last);
            lnkNextPage.CommandName = "Page";
            lnkNextPage.CommandArgument = "Last";
            lnkNextPage.CssClass = "next";

            phPaging = (PlaceHolder)row.FindControl("phPaging");
            phPaging.Controls.Add(lnkNextPage);
            if (gv.PageIndex == gv.PageCount - 1)
            {
                lnkNextPage.Enabled = false;
            }

        }
        #endregion
        #region Tab2
        #region GV Policy
        private string getPolicyList()
        {
            StringBuilder sql = new StringBuilder();

            sql.AppendFormat(@"
                SELECT bp.ID, bp.[MinIncome], bp.[MaxIncome], bp.[Policy] as 'DSR'
                        ,bp.[IsMain], bn.BankName
                FROM [tbl_BankPolicy] bp with (nolock) inner join
                tbl_BankName bn with (nolock) on bp.BankID = bn.BankID
                WHERE 1 = 1
                order by cast(bn.BankID as int), cast(Policy as decimal)
            ");

            return sql.ToString();
        }

        protected void btnSavePolicy_Click(object sender, EventArgs e)
        {
            if (validatePolicy())
            {
                StringBuilder sql = new StringBuilder();
                wwdb db = new wwdb();

                sql.Clear();
                TextBox txtMinIncome, txtMaxIncome, txtDSR;
                string ID, minIncome, maxIncome, dsr;
                for (int i = 0; i < gvPolicy.Rows.Count; i++)
                {
                    ID = gvPolicy.Rows[i].Cells[0].Text;
                    txtMinIncome = (TextBox)gvPolicy.Rows[i].Cells[2].FindControl("txtMinIncome");
                    txtMaxIncome = (TextBox)gvPolicy.Rows[i].Cells[3].FindControl("txtMaxIncome");
                    txtDSR = (TextBox)gvPolicy.Rows[i].Cells[4].FindControl("txtDSR");
                    minIncome = secure.RC(txtMinIncome.Text);
                    maxIncome = secure.RC(txtMaxIncome.Text);
                    dsr = secure.RC(txtDSR.Text);

                    sql.AppendFormat(@"
                        UPDATE tbl_BankPolicy SET MinIncome = '{0}', MaxIncome = '{1}', Policy = '{2}' WHERE ID = '{3}';
                    ", string.IsNullOrEmpty(minIncome) ? "" : minIncome
                    , string.IsNullOrEmpty(maxIncome) ? "" : maxIncome
                    , string.IsNullOrEmpty(dsr) ? "" : dsr
                    , ID);
                }
                db.Execute(sql.ToString());
                LogUtil.logAction(sql.ToString(), "Updating BankPolicy Table");
                if (db.HasError)
                {
                    LogUtil.logError(db.ErrorMessage, sql.ToString());
                    sqlString.displayAlert(this, Resources.resource.unknownError);
                }
                else
                    sqlString.displayAlert(this, Resources.resource.ActionSuccessfulMsg, Request.Url.ToString());
            }
        }
        private bool validatePolicy()
        {
            bool result = false;
            TextBox txtMinIncome, txtMaxIncome, txtDSR;
            string ID, minIncome, maxIncome, dsr;
            StringBuilder errMsg = new StringBuilder();
            ReportController reportController = new ReportController();
            for (int i = 0; i < gvPolicy.Rows.Count; i++)
            {
                if (true)//errMsg.ToString().Trim().Equals(string.Empty))
                {
                    ID = gvPolicy.Rows[i].Cells[0].Text;
                    txtMinIncome = (TextBox)gvPolicy.Rows[i].Cells[2].FindControl("txtMinIncome");
                    txtMaxIncome = (TextBox)gvPolicy.Rows[i].Cells[3].FindControl("txtMaxIncome");
                    txtDSR = (TextBox)gvPolicy.Rows[i].Cells[4].FindControl("txtDSR");

                    txtMinIncome.Text = secure.RC(reportController.removeComma(txtMinIncome));
                    txtMaxIncome.Text = secure.RC(reportController.removeComma(txtMaxIncome));
                    txtDSR.Text = secure.RC(reportController.removeComma(txtDSR));
                    minIncome = txtMinIncome.Text;
                    maxIncome = txtMaxIncome.Text;
                    dsr = txtDSR.Text;
                    if (!long.TryParse(minIncome, out _))
                        if (!decimal.TryParse(minIncome, out _))
                            errMsg.Append("ID: " + ID + ".Min " + Resources.resource.Net_Income_Invalid_Format + "\\n");
                    if (!long.TryParse(maxIncome, out _))
                        if (!decimal.TryParse(maxIncome, out _))
                            errMsg.Append("ID: " + ID + ".Max " + Resources.resource.Net_Income_Invalid_Format + "\\n");
                    if (!int.TryParse(dsr, out _))
                        if (!decimal.TryParse(dsr, out _))
                            errMsg.Append("ID: " + ID + ".DSR " + Resources.resource.DSR_Invalid_Format + "\\n");
                }
                //else
                //    break;
            }

            if (errMsg.ToString().Trim() != string.Empty)
            {
                sqlString.displayAlert2(this, errMsg.ToString());
                lblErr.Text = errMsg.Replace("\\n", "<br />").ToString();
                lblErr.Visible = true;
                divErrorMessage.Visible = true;
                lblErr.Focus();
            }
            else
            {
                lblErr.Text = "";
                lblErr.Visible = false;
                divErrorMessage.Visible = false;
                result = true;
            }

            return result;
        }
        protected void btnRefreshPolicy_Click(object sender, EventArgs e)
        {
            sqlString.bindControl(gvPolicy, getPolicyList());
        }

        protected void btnSavePolicyRow_Click(object sender, EventArgs e)
        {
            if (validPolicyRow(sender))
            {
                Button btnSavePolicy = (Button)sender;
                string ID = ((GridViewRow)btnSavePolicy.NamingContainer).Cells[0].Text;
                TextBox txtMinIncome = (TextBox)((GridViewRow)btnSavePolicy.NamingContainer).Cells[2].FindControl("txtMinIncome");
                TextBox txtMaxIncome = (TextBox)((GridViewRow)btnSavePolicy.NamingContainer).Cells[3].FindControl("txtMaxIncome");
                TextBox txtDSR = (TextBox)((GridViewRow)btnSavePolicy.NamingContainer).Cells[4].FindControl("txtDSR");
                string minIncome = secure.RC(txtMinIncome.Text);
                string maxIncome = secure.RC(txtMaxIncome.Text);
                string dsr = secure.RC(txtDSR.Text);

                StringBuilder sql = new StringBuilder();
                wwdb db = new wwdb();

                sql.Clear();
                sql.AppendFormat(@"
                    UPDATE tbl_BankPolicy SET MinIncome = '{0}', MaxIncome = '{1}', Policy = '{2}' WHERE ID = '{3}';
                ", string.IsNullOrEmpty(minIncome) ? "" : minIncome
                , string.IsNullOrEmpty(maxIncome) ? "" : maxIncome
                , string.IsNullOrEmpty(dsr) ? "" : dsr
                , string.IsNullOrEmpty(ID) ? "" : ID);
                db.Execute(sql.ToString());
                LogUtil.logAction(sql.ToString(), "Updating Bank Policy. ID: " + ID);
                if (db.HasError)
                {
                    LogUtil.logError(db.ErrorMessage, sql.ToString());
                    sqlString.displayAlert(this, Resources.resource.unknownError);
                }
                else
                    sqlString.displayAlert(this, Resources.resource.ActionSuccessfulMsg, Request.Url.ToString());
            }
        }
        private bool validPolicyRow(object sender)
        {
            bool result = false;
            TextBox txtMinIncome, txtMaxIncome, txtDSR;
            string ID, minIncome, maxIncome, dsr;
            StringBuilder errMsg = new StringBuilder();
            GridViewRow row = (GridViewRow)((Button)sender).NamingContainer;
            ReportController reportController = new ReportController();

            ID = row.Cells[0].Text;
            txtMinIncome = (TextBox)row.Cells[2].FindControl("txtMinIncome");
            txtMaxIncome = (TextBox)row.Cells[3].FindControl("txtMaxIncome");
            txtDSR = (TextBox)row.Cells[4].FindControl("txtDSR");

            txtMinIncome.Text = secure.RC(reportController.removeComma(txtMinIncome));
            txtMaxIncome.Text = secure.RC(reportController.removeComma(txtMaxIncome));
            txtDSR.Text = secure.RC(reportController.removeComma(txtDSR));
            minIncome = txtMinIncome.Text;
            maxIncome = txtMaxIncome.Text;
            dsr = txtDSR.Text;
            if (!long.TryParse(minIncome, out _))
                if (!decimal.TryParse(minIncome, out _))
                    errMsg.Append("ID: " + ID + ".Min " + Resources.resource.Net_Income_Invalid_Format + "\\n");
            if (!long.TryParse(maxIncome, out _))
                if (!decimal.TryParse(maxIncome, out _))
                    errMsg.Append("ID: " + ID + ".Max " + Resources.resource.Net_Income_Invalid_Format + "\\n");
            if (!int.TryParse(dsr, out _))
                if (!decimal.TryParse(dsr, out _))
                    errMsg.Append("ID: " + ID + ".DSR " + Resources.resource.DSR_Invalid_Format + "\\n");

            if (errMsg.ToString().Trim() != string.Empty)
            {
                sqlString.displayAlert2(this, errMsg.ToString());
                lblErr.Text = errMsg.Replace("\\n", "<br />").ToString();
                lblErr.Visible = true;
                divErrorMessage.Visible = true;
                lblErr.Focus();
            }
            else
            {
                lblErr.Text = "";
                lblErr.Visible = false;
                divErrorMessage.Visible = false;
                result = true;
            }

            return result;
        }
        protected void btnDeletePolicyRow_Click(object sender, EventArgs e)
        {
            string ID = ((GridViewRow)((Button)sender).NamingContainer).Cells[0].Text;
            StringBuilder sql = new StringBuilder();
            wwdb db = new wwdb();

            sql.Clear();
            sql.AppendFormat(@"
                DELETE FROM tbl_BankPolicy WHERE ID = '{0}';
            ", ID);
            db.Execute(sql.ToString());
            LogUtil.logAction(sql.ToString(), "Deleting Bank Policy. ID: " + ID);
            if (db.HasError)
                LogUtil.logError(db.ErrorMessage, sql.ToString());
            else
                sqlString.displayAlert(this, Resources.resource.ActionSuccessfulMsg, Request.Url.ToString());
        }

        protected void gvPolicy_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //Bind Minimum and Maximum
                TextBox txtMinIncome = (TextBox)e.Row.Cells[2].FindControl("txtMinIncome");
                TextBox txtMaxIncome = (TextBox)e.Row.Cells[3].FindControl("txtMaxIncome");
                string minIncome = DataBinder.Eval(e.Row.DataItem, "MinIncome").ToString();
                string maxIncome = DataBinder.Eval(e.Row.DataItem, "MaxIncome").ToString();

                txtMinIncome.Text = ConvertHelper.ConvertToDecimal(minIncome, 0).ToString("N0");
                txtMaxIncome.Text = ConvertHelper.ConvertToDecimal(maxIncome, 0).ToString("N0");

                //Bind DSR
                TextBox txtDSR = (TextBox)e.Row.Cells[4].FindControl("txtDSR");
                string dsr = DataBinder.Eval(e.Row.DataItem, "DSR").ToString();

                txtDSR.Text = ConvertHelper.ConvertToDecimal(dsr, 0).ToString("N0");

                //Disable to delete 0 and unlimited data row
                Button btnDeletePolicyRow = (Button)e.Row.Cells[5].FindControl("btnDeletePolicyRow");

                if (ConvertHelper.ConvertToDecimal(minIncome, 0) <= 0
                    || ConvertHelper.ConvertToDecimal(maxIncome, 0) >= 9999999)
                    btnDeletePolicyRow.Visible = false;
            }
        }
        #endregion

        private void binddata()
        {
            StringBuilder sql = new StringBuilder();

            //ddlBankList
            sql.Clear();
            sql.AppendFormat(@"
                with cte as (
	                SELECT distinct bp.BankID as '1', bn.BankName as '0'
	                FROM [tbl_BankPolicy] bp with (nolock) inner join
	                tbl_BankName bn with (nolock) on bp.BankID = bn.BankID
                )
                select * from cte
                where 1 = 1
                order by cast(cte.[1] as int)
            ");
            sqlString.bindControl(ddlBankList, sql.ToString(), "0", "1", true);
        }
        protected void btnAddPolicy_Click(object sender, EventArgs e)
        {
            if (validateNewPolicy())
            {
                StringBuilder sql = new StringBuilder();
                wwdb db = new wwdb();

                sql.Clear();
                sql.AppendFormat(@"
                    INSERT INTO tbl_BankPolicy (BankID, MinIncome, MaxIncome, Policy, isMain, [Desc])
                    VALUES ('{0}', '{1}', '{2}', '{3}', 0, (select top 1 bp.[Desc] from tbl_BankPolicy bp with (nolock) where BankID = '{0}'));
                ", ddlBankList.SelectedItem.Value
                , string.IsNullOrEmpty(txtMinIncome.Text) ? "0" : txtMinIncome.Text
                , string.IsNullOrEmpty(txtMaxIncome.Text) ? "99999999" : txtMaxIncome.Text
                , string.IsNullOrEmpty(txtDSR.Text) ? "70" : txtDSR.Text);
                db.Execute(sql.ToString());
                if (db.HasError)
                {
                    LogUtil.logError(db.ErrorMessage, sql.ToString());
                    sqlString.displayAlert(this, Resources.resource.unknownError);
                }
                else
                    sqlString.displayAlert(this, Resources.resource.ActionSuccessfulMsg, Request.Url.ToString());
            }
        }
        private bool validateNewPolicy()
        {
            bool result = false;
            string minIncome, maxIncome, dsr;
            StringBuilder errMsg = new StringBuilder();
            ReportController reportController = new ReportController();

            txtMinIncome.Text = secure.RC(reportController.removeComma(txtMinIncome));
            txtMaxIncome.Text = secure.RC(reportController.removeComma(txtMaxIncome));
            txtDSR.Text = secure.RC(reportController.removeComma(txtDSR));
            minIncome = txtMinIncome.Text;
            maxIncome = txtMaxIncome.Text;
            dsr = txtDSR.Text;

            if (ddlBankList.SelectedIndex <= 0)
                errMsg.Append(Resources.resource.Please_Select_Bank + "\\n");
            if (!long.TryParse(minIncome, out _))
                if (!decimal.TryParse(minIncome, out _))
                    errMsg.Append(Resources.resource.Net_Income_Invalid_Format + "\\n");
            if (!long.TryParse(maxIncome, out _))
                if (!decimal.TryParse(maxIncome, out _))
                    errMsg.Append(Resources.resource.Net_Income_Invalid_Format + "\\n");
            if (!int.TryParse(dsr, out _))
                if (!decimal.TryParse(dsr, out _))
                    errMsg.Append(Resources.resource.DSR_Invalid_Format + "\\n");

            if (errMsg.ToString().Trim() != string.Empty)
            {
                sqlString.displayAlert2(this, errMsg.ToString());
                lblErr.Text = errMsg.Replace("\\n", "<br />").ToString();
                lblErr.Visible = true;
                divErrorMessage.Visible = true;
                lblErr.Focus();
            }
            else
            {
                lblErr.Text = "";
                lblErr.Visible = false;
                divErrorMessage.Visible = false;
                result = true;
            }

            return result;
        }
        #endregion

    }
}