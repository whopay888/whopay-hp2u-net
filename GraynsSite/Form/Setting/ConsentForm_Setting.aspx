﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Global1.Master" AutoEventWireup="true" CodeBehind="ConsentForm_Setting.aspx.cs" Inherits="HJT.Form.Setting.ConsentForm_Setting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .tblgv3 tr td:first-child {
            font-weight: bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <aside class="right-side">

        <section class="content-header">
            <h1>
                <span class="icon fa fa-fw fa-book" aria-hidden="true"></span>
                <asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><span runat="server" id="spHeader"></span></asp:Label>
            </h1>

            <asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div id="divErrorMessage" runat="server" visible="false" class="alert alert-danger alert-dismissable">
                        <i class="fa fa-ban"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <b>Alert!</b>
                        <br />
                        <asp:Label runat="server" ID="lblErr"></asp:Label>
                    </div>

                    <div id="dailysearchreport" runat="server" class="col-md-12">
                        <div class="box">
                            <h4>
                                <span style="color: black;"><%= GetGlobalResourceObject("resource", "ConsentFormList")%>&nbsp;:&nbsp;</span>
                            </h4>

                            <div class="box-body table-responsive">
                                <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow" ID="gv" runat="server" AutoGenerateColumns="false" GridLines="None" ShowFooter="false"
                                    EmptyDataText="<%$ Resources:Resource, No_Record_Found%>" CssClass="table table-bordered table-striped" Width="100%" OnDataBound="gv_DataBound" OnRowDataBound="gv_RowDataBound">
                                    <Columns>
                                        <asp:BoundField DataField="ID" HeaderText="<%$ Resources:Resource, Id%>" />
                                        <asp:BoundField DataField="Document Name" HeaderText="<%$ Resources:Resource, DocumentName%>" />
                                        <asp:BoundField DataField="Date" HeaderText="<%$ Resources:Resource, Dates%>" />
                                        <asp:BoundField DataField="Remarks" HeaderText="<%$ Resources:Resource, Remark%>" />
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, Action%>">
                                            <ItemTemplate>
                                                <asp:Button runat="server" ID="btnView" Text="<%$ Resources:Resource, View%>" OnClick="btnView_Click" CommandArgument='<%#Eval("ID") %>' CssClass="btn btn-view" />
                                                <%--<asp:Button ID="btnReplace" CssClass="btn btn-edit" runat="server" Text="<%$ Resources:Resource, Replace%>"
                                            CommandArgument='<%# Container.DataItemIndex  %>' OnClick="btnReplace_Click" OnClientClick="return confirm('Are you sure want to replace this document?')" />--%>
                                                <asp:Button ID="btnEdit" CssClass="btn btn-edit" runat="server" Text="<%$ Resources:Resource, Edit%> "
                                                    OnClick="btnEdit_Click" CommandArgument='<%#Eval("ID") %>'/>
                                                <asp:Button ID="btnDelete" CssClass="btn btn-danger" runat="server" Text="<%$ Resources:Resource, Delete%> "
                                                    CommandArgument='<%# Container.DataItemIndex  %>' OnClick="btnDelete_Click" OnClientClick="return confirm('Are you sure want to remove this document?')" />
                                                <asp:Button ID="btnActive" CssClass="btn btn-edit" runat="server" Text="<%$ Resources:Resource, Set_As_Active%> "
                                                    OnClick="btnActive_Click" CommandArgument='<%#Eval("ID") %>' OnClientClick="return confirm('Are you sure want to set this document active?')" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div class="box-footer">
                                <asp:LinkButton runat="server" ID="btnAddRow" CssClass="btn btn-primary" OnClick="btnAddRow_Click">
                            <i class="fa fa-plus fa-plus-circle"></i> Add New Document
                                </asp:LinkButton>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </aside>

</asp:Content>
