﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Global1.Master" CodeBehind="AddBilling_Setting.aspx.cs" Inherits="WMElegance.Form.Setting.AddBilling_Setting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function confirmSubmit() {
            return confirm('<%= GetGlobalResourceObject("resource" , "ConfirmToRegister") %>');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                <span class="icon fa fa-fw fa-book" aria-hidden="true"></span>
                <asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><span runat="server" id="spHeader"><%= GetGlobalResourceObject("resource", "Billing_Setting")%></span></asp:Label>
            </h1>
            <asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
        </section>
        <section class="content">
            <asp:Panel runat="server" DefaultButton="btnSubmit">
                <div class="col-md-12">
                    <div id="divPriceLimit" runat="server">
                        <div class="box-body">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr class="form-group" runat="server" id="trTenure">
                                        <td style="width: 20%; border-left: 0px!important;">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "Branch")%>&nbsp;<span style="color: Red;">*</span></label>
                                        </td>
                                        <td style="width: 80%;" class="controls">
                                            <asp:DropDownList runat="server" ID="ddlBranch" CssClass="form-control">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr class="form-group" runat="server" id="trInterest">
                                        <td style="width: 20%; border-left: 0px!important;">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "Per_Request_Price")%>&nbsp;<span style="color: Red;">*</span></label>
                                        </td>
                                        <td style="width: 80%;" class="controls">
                                            <asp:TextBox ID="txtPerRequestPrice" runat="server" CssClass="form-control"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="form-group" runat="server" id="trLoanAmount">
                                        <td style="width: 20%; border-left: 0px!important;">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "Per_Successful_Case_Price")%>&nbsp;<span style="color: Red;">*</span></label>
                                        </td>
                                        <td style="width: 80%;" class="controls">
                                            <asp:TextBox ID="txtPerSuccessfulCasePrice" runat="server" CssClass="form-control"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="form-group" runat="server" id="trPurchasePrice">
                                        <td style="width: 20%; border-left: 0px!important;">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "Credit_Limit")%>&nbsp;<span style="color: Red;">*</span></label>
                                        </td>
                                        <td style="width: 80%;" class="controls">
                                            <asp:TextBox ID="txtCreditLimit" runat="server" CssClass="form-control"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="form-group" runat="server" id="trProjectName">
                                        <td style="width: 20%; border-left: 0px!important;">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "From")%>&nbsp;<span style="color: Red;">*</span></label>
                                        </td>
                                        <td style="width: 80%;" class="controls">
                                            <asp:TextBox ID="txtFrom" runat="server" CssClass="form-control" TextMode="Date"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="form-group" runat="server" id="trUnit">
                                        <td style="width: 20%; border-left: 0px!important;">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "To")%>&nbsp;<span style="color: Red;">*</span></label>
                                        </td>
                                        <td style="width: 80%;" class="controls">
                                            <asp:TextBox ID="txtTo" runat="server" CssClass="form-control" TextMode="Date"></asp:TextBox>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <asp:Literal ID="ltlCount" runat="server" Text="0" Visible="false" />
                        <asp:Literal ID="ltlRemoved" runat="server" Visible="false" />
                    </div>
                    <div id="divPayment" class="box-body" runat="server">
                        <table class="table table-bordered">
                            <tbody>
                                <tr class="form-group" runat="server">
                                    <td style="width: 20%; border-left: 0px!important;">
                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Text")%>&nbsp;<span style="color: Red;">*</span></label>
                                    </td>
                                    <td style="width: 80%;" class="controls">
                                        <asp:TextBox ID="txtText" runat="server" CssClass="form-control"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="form-group" runat="server">
                                    <td style="width: 20%; border-left: 0px!important;">
                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Topup_Amount")%>&nbsp;<span style="color: Red;">*</span></label>
                                    </td>
                                    <td style="width: 80%;" class="controls">
                                        <asp:TextBox ID="txtTopupAmount" runat="server" CssClass="form-control"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="form-group" runat="server">
                                    <td style="width: 20%; border-left: 0px!important;">
                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Free_Credit")%>&nbsp;<span style="color: Red;">*</span></label>
                                    </td>
                                    <td style="width: 80%;" class="controls">
                                        <asp:TextBox ID="txtFreeCredit" runat="server" CssClass="form-control"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="form-group" runat="server">
                                    <td style="width: 20%; border-left: 0px!important;">
                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "FreeRequest")%>&nbsp;<span style="color: Red;">*</span></label>
                                    </td>
                                    <td style="width: 80%;" class="controls">
                                        <asp:TextBox ID="txtFreeRequest" runat="server" CssClass="form-control"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="form-group" runat="server">
                                    <td style="width: 20%; border-left: 0px!important;">
                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Sort_Order")%>&nbsp;<span style="color: Red;">*</span></label>
                                    </td>
                                    <td style="width: 80%;" class="controls">
                                        <asp:TextBox ID="txtSort" runat="server" CssClass="form-control"></asp:TextBox>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="box-footer">
				    <asp:Button runat="server" ID="btnSubmit" Visible="true" CssClass="btn btn-submit" OnClick="btnSubmit_Click" Text="<%$ Resources:Resource, Save%>" OnClientClick="return confirm('Comfirm to add new record ?')" />
                    <asp:Button runat="server" ID="btnBack" Visible="true" CssClass="btn btn-searh" OnClick="btnBack_Click" Text="<%$ Resources:Resource, Back%>"/>
			    </div>
            </asp:Panel>
        </section>
    </aside>
</asp:Content>
