﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Global1.Master" AutoEventWireup="true" CodeBehind="UACList.aspx.cs" Inherits="WMElegance.Form.Setting.UACList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function confirmSave() {
            return confirm('<%= GetGlobalResourceObject("resource","Confirm_Save_Changes")%>');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <aside class="right-side">
        <div class="content-header">
            <h1>
                <span class="icon fa fa-fw fa-cogs" aria-hidden="true"></span>
                <span runat="server" id="spHeader"><%= GetGlobalResourceObject("resource", "RoleManagement")%></span>
            </h1>
            <asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
        </div>
        <div class="content">
            <div class="col-md-4">
                <div class="box box-info">
                    <h4><asp:Label ID="panel" runat="server" Text="<%$ Resources:Resource, Selection_Option%>"></asp:Label></h4>
                    <div class="box-content">
                        <div class="control-group">
                            <label class="control-label"><%= GetGlobalResourceObject("resource", "Search")%></label>
                            <div class="controls">
                                <asp:TextBox ID="TxtSearch_RoleCode" placeholder="<%$ Resources:Resource, Search_By_RoleCode%>" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>                            
                                <asp:TextBox ID="TxtSearch_RoleName" placeholder="<%$ Resources:Resource, Search_By_RoleName%>" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                            </div>
                        </div>
                        <br /><br />
                    </div>
                    <div class="box-footer">
                        <asp:Button ID="btnSearch" Text="<%$ Resources:Resource, Search%>" runat="server" CssClass="btn btn-searh" OnClick="btnSearch_Click" />
                        <asp:Button ID="btnExport" Text="<%$ Resources:Resource, Export%>" runat="server" CssClass="btn btn-export" OnClick="btnExport_Click" />
                        <asp:Button ID="btnClear" Text="<%$ Resources:Resource, Clear%>" runat="server" CssClass="btn btn-clear" OnClick="btnClear_Click" />
                        <asp:Button ID="btnAddRole" Text="<%$ Resources:Resource, AddRole%>" runat="server" CssClass="btn btn-next" OnClick="btnAddRole_Click" />
                    </div>
                    <br /><br />
                    <div class="form-group">
                        <table style="width:100%;">
                            <tr style="width:100%;">
                                <td>
                                    <asp:Label runat="server" Text='<%$ Resources:resource, SpeedMode_ReportType %>' Font-Size="Large" Font-Bold="true"/>
                                </td>
                                <td>
                                    <asp:RadioButtonList runat="server" ID="rblReportType" RepeatDirection="Horizontal" Height="16px"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" Text='<%$ Resources:resource, AdviseVisibility %>' Font-Size="Large" Font-Bold="true"/>
                                </td>
                                <td>
                                    <asp:RadioButtonList runat="server" ID="rblAdviseVisibility" RepeatDirection="Horizontal" Height="16px">
                                        <asp:ListItem>Yes</asp:ListItem>
                                        <asp:ListItem>No</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" Text='<%$ Resources:resource, ReuseReport_Active %>' Font-Size="Large" Font-Bold="true"/>
                                </td>
                                <td>
                                    <asp:RadioButtonList runat="server" ID="rblReportUse" RepeatDirection="Horizontal" Height="16px">
                                        <asp:ListItem>Yes</asp:ListItem>
                                        <asp:ListItem>No</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" Text='<%$ Resources:resource, ReportExpirationDay %>' Font-Size="Large" Font-Bold="true"/>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtReportExpiration"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" Text='<%$ Resources:resource, WhatsAppNumber %>' Font-Size="Large" Font-Bold="true"/>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtWhatsAppNumber"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" Text='<%$ Resources:resource, NumberOfDataClientList %>' Font-Size="Large" Font-Bold="true"/>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtNoOfRows"/>
                                </td>
                            </tr>
                        </table>
                        <asp:Button Text="<%$ Resources:Resource, Save%>" runat="server" CssClass="btn btn-next" ID="btnSave" OnClick="btnSave_Click"
                            OnClientClick="return confirmSave();" />
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="box box-color box-bordered">
                    <div class="box-title">
                         <h4><asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, Role_List%>"></asp:Label>
                            <span style="float: right; font-size: 14px; margin-top: -6px">
                                <span style="color: white; font-weight: bold;"><%= GetGlobalResourceObject("resource", "Show")%>&nbsp;:&nbsp;</span>
                                <asp:DropDownList ID="ddlPageSize" CssClass="input-small" runat="server" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged"
                                    AutoPostBack="true">
                                    <asp:ListItem>10</asp:ListItem>
                                    <asp:ListItem>25</asp:ListItem>
                                    <asp:ListItem>50</asp:ListItem>
                                    <asp:ListItem>100</asp:ListItem>
                                </asp:DropDownList>
                                <span style="color: white; font-weight: bold;"><%= GetGlobalResourceObject("resource", "Records")%>&nbsp;</span>
                            </span>
                        </h4>
                    </div>
                    <div class="box-content">
                        <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow" ID="gv" runat="server" AutoGenerateColumns="true"
                            GridLines="None" AllowPaging="True" DataKeyNames="<%$ Resources:Resource, RoleCode%>"
                            EmptyDataText="<%$ Resources:Resource, No_Record_Found%>" CssClass="table"
                            Width="100%" AllowSorting="true"
                            OnSorting="gv_Sorting" OnPageIndexChanging="gv_PageIndexChanging"
                            OnRowDataBound="gv_RowDataBound">
                            <PagerSettings Mode="NumericFirstLast" />
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, Action%>">
                                    <ItemTemplate>
                                        <asp:Button ID="BtnEdit" CssClass="btn btn-terminated" runat="server" Text="<%$ Resources:Resource, Edit%>" CommandArgument='<%# Container.DataItemIndex  %>' OnClick="BtnEdit_Click" />
                                        <asp:Button ID="BtnDelete" CssClass="btn btn-terminated" runat="server" Text="<%$ Resources:Resource, Delete%>" CommandArgument='<%# Container.DataItemIndex  %>' OnClick="BtnDelete_Click" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </aside>
</asp:Content>