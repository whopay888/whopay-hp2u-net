﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Util;

namespace HJT.Form.Setting
{
    public partial class ConsentForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string cId = sqlString.decryptURL(Validation.GetUrlParameter(this.Page, "CID", ""));
                if (!string.IsNullOrWhiteSpace(cId))
                {
                    wwdb db = new wwdb();
                    StringBuilder sql = new StringBuilder();
                    string content = string.Empty;
                    sql.Clear();
                    sql.Append("SELECT top (1) Content FROM tbl_ConsentDocs WHERE Id = " + cId); //ToDo: add active column and enable to set by admin
                    db.OpenTable(sql.ToString());

                    if (db.RecordCount() > 0)
                        content = db.Item("Content");

                    string transformedHtml = content;

                    Response.Write(transformedHtml);
                }
            }
        }
    }
}