﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Global1.Master" CodeBehind="BillingList_Setting.aspx.cs" Inherits="WMElegance.Form.Setting.BillingList_Setting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function confirmSubmit() {
            return confirm('<%= GetGlobalResourceObject("resource" , "ConfirmToRegister") %>');
        }

        $(window).load(function () {
            var x = document.getElementById("filtercontent");
            var lblfilter = document.getElementById("lblfilter");
            if (x.style.display === "none") {
                x.style.display = "block";
                lblfilter.innerHTML = "Hide Filters"
            } else {
                x.style.display = "none";
                lblfilter.innerHTML = "Show Filters"
            }
        });

        function myFunction() {
            var x = document.getElementById("filtercontent");
            var lblfilter = document.getElementById("lblfilter");
            if (x.style.display === "none") {
                x.style.display = "block";
                lblfilter.innerHTML = "Hide Filters"
            } else {
                x.style.display = "none";
                lblfilter.innerHTML = "Show Filters"
            }
        }

        function NewFunction() {
            var lblfilter = document.getElementById("lblfilter");
            lblfilter.innerHTML = "Hide Filters"
        }

        function confirmDelete() {
            return confirm('<%= GetGlobalResourceObject("resource" , "Confirm_Delete") %>');
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                <span class="icon fa fa-fw fa-book" aria-hidden="true"></span>
                <asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><span runat="server" id="spHeader"><%= GetGlobalResourceObject("resource", "Billing_Setting")%></span></asp:Label>
            </h1>
            <asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
        </section>
        <section class="content" style="margin-top: 10px">
            <div class="col-md-12">
                <div runat="server">
                    <div class="box">
                        <asp:Label runat="server" ID="lblSection1Title" Font-Bold="true" Font-Size="Large" Font-Underline="true">
                            <span runat="server"><%= GetGlobalResourceObject("resource", "PaymentSetting")%></span>
                        </asp:Label>
                        <div style="text-align-last:right;">
                            <asp:Button ID="Button1" Text="<%$ Resources:Resource, Add%>" runat="server" CssClass="btn btn-edit" OnClick="btnAdd_Click1" />
                        </div>
                        <div class="box-body table-responsive">
                            <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow" ID="gvPayment" runat="server" AutoGenerateColumns="false" 
                                GridLines="None" AllowPaging="True" ShowFooter="true" EmptyDataText="<%$ Resources:Resource, No_Record_Found%>" 
                                CssClass="table table-bordered table-striped" Width="100%" OnPageIndexChanging="gvPayment_PageIndexChanging"
                                OnRowDataBound="gvPayment_RowDataBound" AllowSorting="true" OnSorting="gvPayment_Sorting">
                                <PagerSettings Mode="NumericFirstLast" />
                                <Columns>
                                    <asp:BoundField HeaderText="<%$ Resources:Resource, Text%>" DataField="Text" />
                                    <asp:BoundField HeaderText="<%$ Resources:Resource, Topup_Amount%>" DataField="TopupAmount" SortExpression="TopupAmount" />
                                    <asp:BoundField HeaderText="<%$ Resources:Resource, Free_Credit%>" DataField="AdditionalCredit" SortExpression="AdditionalCredit" />
                                    <asp:BoundField HeaderText="<%$ Resources:Resource, FreeRequest%>" DataField="FreeRequestCount" SortExpression="FreeRequestCount" />
                                    <asp:BoundField HeaderText="<%$ Resources:Resource, Sort_Order%>" DataField="Sort" SortExpression="Sort" />
                                    <asp:BoundField HeaderText="<%$ Resources:Resource, CreatedDate%>" DataField="CreatedAt" SortExpression="CreatedAt" Visible="false" />
                                    <asp:BoundField HeaderText="<%$ Resources:Resource, CreatedBy%>" DataField="CreatedBy" Visible="false" />
                                    <asp:BoundField HeaderText="<%$ Resources:Resource, UpdatedDate%>" DataField="UpdatedAt" SortExpression="UpdatedAt" Visible="false" />
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, UpdatedBy%>">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblUpdatedBy" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Action%>">
                                        <ItemTemplate>
                                            <asp:Button runat="server" ID="btnEdit" Text="<%$ Resources:Resource, Edit%>" CssClass="btn btn-edit" 
                                                CommandArgument='<%#Eval("ID") %>' OnClick="btnEdit_Click1" />
                                            <br />
                                            <asp:Button runat="server" ID="btnDelete" Text="<%$ Resources:Resource, Delete%>" CommandArgument='<%#Eval("ID") %>' 
                                                OnClick="btnDelete_Click1" OnClientClick="return confirmDelete();" CssClass="btn btn-danger" 
                                                Style="margin-bottom: 0px !important;" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerTemplate>
                                    <div>
                                        <div class="dataTables_paginate paging_bootstrap">
                                            <ul class="pagination">
                                                <li>
                                                    <asp:PlaceHolder runat="server" ID="phPreviousPaging"></asp:PlaceHolder>
                                                </li>

                                                <li>
                                                    <asp:PlaceHolder runat="server" ID="phPaging"></asp:PlaceHolder>
                                                </li>

                                                <li>
                                                    <asp:PlaceHolder runat="server" ID="phNextPaging"></asp:PlaceHolder>
                                                </li>
                                            </ul>

                                        </div>
                                    </div>
                                </PagerTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                    <div id="filter">
                        <a href="#0" id="showfilter" onclick="myFunction()">
                            <label id="lblfilter" class="btn btn-searh" style="text-transform:capitalize !important;">Show Filters</label></a>
                        <div id="filtercontent">
                            <h4>
                                <asp:Label ID="panel" runat="server" Text="<%$ Resources:Resource, Selection_Option%>"></asp:Label></h4>
                            <div class="box-body">
                                <div class="form-group">

                                    <label class="control-label"><%= GetGlobalResourceObject("resource", "From")%></label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtFrom" runat="server" CssClass="form-control" TextMode="Date"></asp:TextBox>

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="control-label"><%= GetGlobalResourceObject("resource", "To")%></label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtTo" runat="server" CssClass="form-control" TextMode="Date"></asp:TextBox>

                                    </div>

                                </div>

                                <div class="form-group" runat="server" id="divBranch">

                                    <label class="control-label"><%= GetGlobalResourceObject("resource", "Branch")%></label>
                                    <div class="controls">
                                        <asp:DropDownList runat="server" CssClass="form-control" ID="ddlBranch">
                                        </asp:DropDownList>
                                    </div>

                                </div>

                            </div>

                            <div class="box-footer">

                                <asp:Button ID="btnSearch" Text="<%$ Resources:Resource, Search%>" OnClientClick="myFunction()" runat="server" CssClass="btn btn-searh" OnClick="btnSearch_Click" />

                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div>
                            <asp:Label runat="server" ID="lblSection2Title" Font-Bold="true" Font-Size="Large" Font-Underline="true">
                                <span runat="server"><%= GetGlobalResourceObject("resource", "ReportPriceSetting")%> & <%= GetGlobalResourceObject("resource", "CreditLimitSetting")%></span>
                            </asp:Label>
                        </div>
                        <div>
                            <table style="width:100%">
                                <tr>
                                    <td style="text-align-last:right; width:100%">
                                        <asp:Button ID="btnAdd" Text="<%$ Resources:Resource, Add%>" runat="server" CssClass="btn btn-edit" OnClick="btnAdd_Click" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="box-body">
                            <div class="box-body table-responsive">
                                <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow" ID="gv" runat="server" AutoGenerateColumns="false" GridLines="None" 
                                    AllowPaging="True" ShowFooter="true" EmptyDataText="<%$ Resources:Resource, No_Record_Found%>" 
                                    CssClass="table table-bordered table-striped" Width="100%" OnPageIndexChanging="gv_PageIndexChanging" OnRowDataBound="gv_RowDataBound" 
                                    OnDataBound="gv_DataBound">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <Columns>
                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Branch_Name%>" DataField="BranchName" />
                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Id%>" Visible="false" DataField="Id" />
                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Branch_Name%>" Visible="false" DataField="BranchId" />
                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Per_Request_Price%>" DataField="PerRequestPrice" />
                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Per_Successful_Case_Price%>" DataField="PerSuccessfulCasePrice" />
                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Credit_Limit%>" DataField="CreditLimit" />
                                        <asp:BoundField HeaderText="<%$ Resources:Resource, From%>" DataField="DateStart" />
                                        <asp:BoundField HeaderText="<%$ Resources:Resource, To%>" DataField="DateEnd" />
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, Action%>">
                                            <ItemTemplate>
                                                <asp:Button runat="server" ID="btnEdit" Text="<%$ Resources:Resource, Edit%>" CssClass="btn btn-edit" 
                                                    CommandArgument='<%#Eval("Id") %>' OnClick="btnEdit_Click" />
                                                <br />
                                                <asp:Button runat="server" ID="btnDelete" Text="<%$ Resources:Resource, Delete%>" CommandArgument='<%#Eval("Id") %>' 
                                                    OnClick="btnDelete_Click" OnClientClick="return confirmDelete();" CssClass="btn btn-danger" 
                                                    Style="margin-bottom: 0px !important;" />
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                    </Columns>
                                    <PagerTemplate>
                                        <div>
                                            <div class="dataTables_paginate paging_bootstrap">
                                                <ul class="pagination">
                                                    <li>
                                                        <asp:PlaceHolder runat="server" ID="phPreviousPaging"></asp:PlaceHolder>
                                                    </li>

                                                    <li>
                                                        <asp:PlaceHolder runat="server" ID="phPaging"></asp:PlaceHolder>
                                                    </li>

                                                    <li>
                                                        <asp:PlaceHolder runat="server" ID="phNextPaging"></asp:PlaceHolder>
                                                    </li>
                                                </ul>

                                            </div>
                                        </div>
                                    </PagerTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </aside>
</asp:Content>
