﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Global1.Master" AutoEventWireup="true" CodeBehind="OverDraft_Setting.aspx.cs" Inherits="WMElegance.Form.Setting.OverDraft_Setting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                <asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><%= GetGlobalResourceObject("resource", "OverDraft")%></asp:Label>
            </h1>
            <asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div id="divErrorMessage" runat="server" visible="false" class="alert alert-danger alert-dismissable">
                        <i class="fa fa-ban"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <b>Alert!</b>
                        <br />
                        <asp:Label runat="server" ID="lblErr"></asp:Label>
                    </div>
                    <div class="box box-primary" runat="server">
                        <div class="box-header" style="background: #dec18c;" >
                            <h4 ><strong><i class="icon-th-list"></i>
                                <%= GetGlobalResourceObject("resource", "OverDraft")%></strong></h4>
                        </div>
                        <asp:Panel runat="server" DefaultButton="btnUpdate">
                            <div class="box-body table-responsive">
                                <div class="box">
                                    <div class="alert alert-block alert-success alert-nomargin">
                                        <%= GetGlobalResourceObject("resource", "OverDraftList")%>
                                    </div>
                                    <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow" ID="gv" runat="server" AutoGenerateColumns="false"
                                        CssClass="table table-bordered table-striped" GridLines="None"
                                        EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                        Width="100%" ShowFooter="true">
                                        <PagerSettings Mode="NumericFirstLast" />
                                        <Columns>
                                            <asp:BoundField DataField="BankID" HeaderText="<%$ Resources:Resource, ID%>" />
                                            <asp:BoundField DataField="BankName" HeaderText="<%$ Resources:Resource, Bank_Name%>" />
                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, OverDraftYear%>" ItemStyle-Width="35%">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" Style="width: 100%;" Text='<%# Bind("HYear") %>' CssClass="input-small" ID="txtYear" AutoPostBack="true" OnTextChanged="txtYear_TextChanged"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, OverDraftInterest%>" ItemStyle-Width="35%">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" Style="width: 100%;" Text='<%# Bind("Interest") %>' CssClass="input-small" ID="txtInterest" AutoPostBack="true" OnTextChanged="txtInterest_TextChanged"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <div class="box-footer">
                                <asp:Button runat="server" ID="btnUpdate" CssClass="btn btn-primary" Text="<%$ Resources:Resource, Update%>"
                                    OnClick="btnUpdate_Click" OnClientClick="return confirm('Comfirm to update all record ?')"/>
                            </div>
                        </asp:Panel>
                    </div>

                </div>
            </div>
        </section>
    </aside>

</asp:Content>
