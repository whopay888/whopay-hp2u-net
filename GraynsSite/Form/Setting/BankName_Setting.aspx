﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Global1.Master" AutoEventWireup="true" CodeBehind="BankName_Setting.aspx.cs" Inherits="WMElegance.Form.Setting.BankName_Setting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>

        function setTab() {
            if ($("#HidTab").val() == '' || $("#HidTab").val() === undefined) {
                $("#HidTab").val("#tab_1");
            }
            $("a[data-toggle='tab'] ");

            $("a[href='" + $("#HidTab").val() + "' ]").click();
        }

        function pageLoad() {
            $("a[data-toggle='tab']").each(
                function (index) {
                    $(this).on("click", function () {
                        $("#HidTab").val($(this).attr("href"));

                    });
                }
            )
            setTab();
        }

        function confirmSave() {
            return confirm('<%= GetGlobalResourceObject("Resource", "Confirm_Save_Changes") %>');
        }
        function confirmDelete() {
            return confirm('<%= GetGlobalResourceObject("Resource", "Confirm_Changes") %>');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                <asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><%= GetGlobalResourceObject("resource", "BankList")%></asp:Label>
            </h1>
            <asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div id="divErrorMessage" runat="server" visible="false" class="alert alert-danger alert-dismissable">
                        <i class="fa fa-ban"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <b>Alert!</b>
                        <br />
                        <asp:Label runat="server" ID="lblErr"></asp:Label>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class=""><a href="#tab_1" id="aTab1" data-toggle="tab" aria-expanded="false"><%= GetGlobalResourceObject("resource", "Bank_Information") %></a></li>
                        <li class=""><a href="#tab_2" id="aTab2" data-toggle="tab" aria-expanded="false"><%= GetGlobalResourceObject("resource", "DSR_Setting") %></a></li>
                        <asp:HiddenField runat="server" ID="HidTab" ClientIDMode="Static" />
                    </ul>
                    <div class="tab-content box-body" runat="server" id="divBody">
                        <div class="tab-pane" id="tab_1">
                            <div class="box">
                                <asp:Panel runat="server" DefaultButton="btnSubmit">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr style="background: #dec18c;">
                                                <td colspan="2">
                                                    <h4><strong><%= GetGlobalResourceObject("resource", "Add_New_Bank")%></strong></h4>
                                                </td>
                                            </tr>
                                            <tr class="form-group" runat="server" id="trRegisterCountry">
                                                <td style="width: 35%; border-left: 0px!important;">
                                                    <label class="control-label"><%= GetGlobalResourceObject("resource", "New_Bank_Name")%></label>
                                                </td>
                                                <td style="width: 50%;" class="controls">
                                                    <asp:TextBox ID="txtNewBank" CssClass="form-control" runat="server"  placeholder="<%$ Resources:Resource, New_Bank_Name%>" ></asp:TextBox>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="box-footer">
                                        <asp:Button runat="server" ID="btnSubmit" Visible="true" CssClass="btn btn-edit" OnClick="btnSubmit_Click" Text="<%$ Resources:Resource, Add%>"  OnClientClick="return confirm('Comfirm to add new record ?')"/>
                                    </div>
                                </asp:Panel>
                            </div>
                            <div class="box box-primary" runat="server">
                                <div class="box-header" style="background: #dec18c;" >
                                    <h4 ><strong><i class="icon-th-list"></i>
                                        <%= GetGlobalResourceObject("resource", "BankList")%></strong></h4>
                                </div>
                                <div class="box-body table-responsive">
                                    <div class="box">
                                        <div class="form-group">
                                            <asp:Panel runat="server" DefaultButton="btnSearch">
                                                <div class="form-group">
                                                    <label class="control-label"><%= GetGlobalResourceObject("resource", "SearchByBankIDName")%></label>
                                                    <div class="controls">
                                                        <asp:TextBox ID="txtSearchBankIDName" runat="server" CssClass="form-control" placeholder="<%$ Resources:Resource, SearchByBankIDName%>"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="box-footer">
                                                    <asp:Button ID="btnSearch" Text="<%$ Resources:Resource, Search%>" runat="server" CssClass="btn btn-searh"
                                                        OnClick="btnSearch_Click" />
                                                    <asp:Button ID="btnReset" Text="<%$ Resources:Resource, Reset%>" runat="server" CssClass="btn btn-export"
                                                        OnClick="btnReset_Click" />
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                    <div class="box">
                                        <div class="alert alert-block alert-success alert-nomargin">
                                            <%= GetGlobalResourceObject("resource", "BankList")%>
                                        </div>
                                        <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow" ID="gv" runat="server" AutoGenerateColumns="false"
                                            CssClass="table table-bordered table-striped" GridLines="None" EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                            Width="100%" ShowFooter="true" OnRowDataBound="gv_RowDataBound">
                                            <PagerSettings Mode="NumericFirstLast" />
                                            <Columns>
                                                <asp:BoundField DataField="BankID" HeaderText="<%$ Resources:Resource, ID%>" />
                                                <asp:TemplateField HeaderText="<%$ Resources:Resource, Bank_Name%>" ItemStyle-Width="70%">
                                                    <ItemTemplate>
                                                        <asp:TextBox runat="server" Style="width: 100%;" Text='<%# Bind("Bank_Name") %>' CssClass="input-small" ID="txtBankName" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<%$ Resources:Resource, Action%>">
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnUpdate" CssClass="btn btn-update" runat="server" Text="<%$ Resources:Resource, Update%>"
                                                            CommandArgument='<%# Container.DataItemIndex  %>' OnClick="btnUpdate_Click" 
                                                            OnClientClick="return confirm('Are you sure want to update current information?')"/>
                                                        <asp:Button ID="btnDelete" CssClass="btn btn-primary" runat="server" Text="<%$ Resources:Resource, Delete%> "
                                                            CommandArgument='<%# Container.DataItemIndex  %>' OnClick="btnDelete_Click" 
                                                            OnClientClick="return confirm('Are you sure want to remove current information?')"/>
                                                        <asp:CheckBox runat="server" ID="chkShowInReport" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                        <br /><br />
                                        <asp:Button runat="server" OnClick="btnSetBank_Click" CssClass="btn btn-edit"
                                            OnClientClick="confirmSave();" Text="<%$ Resources:Resource, Save %>" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <div class="box">
                                <h4><b><asp:Literal runat="server" Text='<%$ Resources:resource, Add_New_Policy %>' /></b></h4>
                                <asp:Panel runat="server" DefaultButton="btnAddPolicy">
                                    <table style="width: 100%;" class="table table-bordered table-striped">
                                        <tr style="width: 100%;">
                                            <td style="width: 20%">
                                                <asp:Label runat="server" Text='<%$ Resources:resource, BankList %>' />
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label runat="server" Text='<%$ Resources:resource, MinimumIncome %>' />
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label runat="server" Text='<%$ Resources:resource, MaximumIncome %>' />
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label runat="server" Text='<%$ Resources:resource, DSR %>' />
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label runat="server" Text='<%$ Resources:resource, Action %>' />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%;">
                                            <td style="width: 20%">
                                                <asp:DropDownList runat="server" ID="ddlBankList" />
                                            </td>
                                            <td style="width: 20%">
                                                <asp:TextBox runat="server" ID="txtMinIncome" />
                                            </td>
                                            <td style="width: 20%">
                                                <asp:TextBox runat="server" ID="txtMaxIncome" />
                                            </td>
                                            <td style="width: 20%">
                                                <asp:TextBox runat="server" ID="txtDSR" />
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Button runat="server" ID="btnAddPolicy" Text='<%$ Resources:resource, Add %>' 
                                                    OnClick="btnAddPolicy_Click" OnClientClick="return confirmSave();"
                                                    CssClass="btn btn-searh"/>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </div>
                            <div class="box">
                                <h4><b><asp:Literal runat="server" Text='<%$ Resources:resource, Bank_Policy_List %>' /></b></h4>
                                <asp:Panel runat="server" DefaultButton="btnSavePolicy">
                                    <asp:GridView runat="server" AutoGenerateColumns="false" ID="gvPolicy" GridLines="None" Width="100%" ShowFooter="true"
                                        AlternatingRowStyle-CssClass="table-alternateRow" EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                        CssClass="table table-bordered table-striped" OnRowDataBound="gvPolicy_RowDataBound">
                                        <Columns>
                                            <asp:BoundField DataField="ID" HeaderText='<%$ Resources:resource, ID %>' />
                                            <asp:BoundField DataField="BankName" HeaderText='<%$ Resources:resource, Bank_Name %>'/>
                                            <asp:TemplateField HeaderText='<%$ Resources:resource, MinimumIncome %>'>
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtMinIncome" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText='<%$ Resources:resource, MaximumIncome %>'>
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtMaxIncome" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText='<%$ Resources:resource, DSR %>'>
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtDSR" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText='<%$ Resources:resource, Action %>'>
                                                <ItemTemplate>
                                                    <asp:Button runat="server" ID="btnSavePolicyRow" OnClick="btnSavePolicyRow_Click" 
                                                        OnClientClick="return confirmSave();" CommandArgument='<%# Container.DataItemIndex %>'
                                                        Text='<%$ Resources:resource, Save %>' CssClass="btn btn-submit"/>
                                                    <asp:Button runat="server" ID="btnDeletePolicyRow" OnClick="btnDeletePolicyRow_Click"
                                                        OnClientClick="return confirmDelete();" CommandArgument='<%# Container.DataItemIndex %>'
                                                        Text='<%$ Resources:resource, Delete %>' CssClass="btn btn-danger" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:Button runat="server" Text='<%$ Resources:resource, Save_All %>' ID="btnSavePolicy" OnClick="btnSavePolicy_Click" 
                                        OnClientClick="return confirmSave();" CssClass="btn btn-submit" />
                                    <asp:Button runat="server" Text='<%$ Resources:resource, Refresh %>' ID="btnRefreshPolicy" OnClick="btnRefreshPolicy_Click"
                                        CssClass="btn btn-searh"/>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </aside>
</asp:Content>
