﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web.UI.WebControls;
using Synergy.Helper;
using Synergy.Model;
using Synergy.Util;

namespace HJT.Form.Setting
{
    public partial class  EditConsentForm_Setting : System.Web.UI.Page
    {
        List<string> uploadDocs = new List<string>();
        LoginUserModel login_user = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];
            if (!IsPostBack)
            {
                txtUpdatedBy.Text = login_user.UserId.ToString();
                txtUpdatedBy.Attributes.Add("readonly", "readonly");
                int cId = 0;
                if (Request["cid"] != null)
                {
                    cId = ConvertHelper.ConvertToInt32(sqlString.decryptURL(Request["CID"].ToString()).Trim(), 0);
                    lblcid.Text = cId.ToString();
                    lblTitle.Text = Resources.resource.EditProject;
                    GetContent(cId);
                }
            }
        }

        protected void GetContent(int Id)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            string conent = string.Empty;
            sql.Clear();
            sql.Append("SELECT * FROM tbl_ConsentDocs WHERE Id = " + Id); //ToDo: add active column and enable to set by admin
            db.OpenTable(sql.ToString());

            if (db.RecordCount() > 0)
            {
                txtContent.Text  = db.Item("Content").ToString();
                txtDocumentName.Text = db.Item("DocumentName").ToString();
                txtRemarks.Text = db.Item("Remarks").ToString();
            }


        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Form/Setting/ConsentForm_Setting.aspx");
        }

        protected bool ValidateForm()
        {
            bool result = false;
            StringBuilder errMsg = new StringBuilder();

            if (String.IsNullOrEmpty(txtDocumentName.Text))
                errMsg.Append(Resources.resource.DocumentNameCannotEmpty + "\\n");
            if (uploadDocs.Count <= 0)
                errMsg.Append(Resources.resource.DocumentEmptyErrMsg + "\\n");
            if (errMsg.ToString().Trim() != string.Empty)
            {
                sqlString.displayAlert2(this, errMsg.ToString());
                lblErr.Text = errMsg.Replace("\\n", "<br />").ToString();
                lblErr.Visible = true;
                divErrorMessage.Visible = true;
                lblErr.Focus();
            }
            else
            {
                lblErr.Text = "";
                lblErr.Visible = false;
                divErrorMessage.Visible = false;
                result = true;
            }

            return result;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            wwdb db = new wwdb();
            string strLog;
            StringBuilder sql = new StringBuilder();
            try
            {
                int newID;

                sql.Clear();
                sql.Append("SELECT TOP 1 CAST(Id AS INT) AS DocumentID FROM tbl_ConsentDocs WITH (NOLOCK) ORDER BY ID desc");
                db.OpenTable(sql.ToString());
                if (db.RecordCount() > 0)
                {
                    newID = ConvertHelper.ConvertToInt(db.Item("DocumentID").ToString(), 0) + 1;
                }
                else
                {
                    newID = 1;
                }

                sql.Clear();


                sql.Append("UPDATE tbl_ConsentDocs SET DocumentName = '"+ secure.RC(txtDocumentName.Text.Trim()) +"',Content ='" + secure.RC(txtContent.Text.Trim()) +"',Remarks='" + secure.RC(txtRemarks.Text.Trim()) + "' WHERE Id =" + lblcid.Text.Trim());
                db.Execute(sql.ToString());

                strLog = "Update Document - (DocumentID : " + newID.ToString() + " , Document Name : " + txtDocumentName.Text.ToString() + " )";
                LogUtil.logAction(sql.ToString(), strLog.ToString());

                sqlString.displayAlert2(this, Resources.resource.Action_Successful, "/Form/Setting/ConsentForm_Setting.aspx");
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.ToString(), sql.ToString()); sqlString.displayAlert(this, KeyVal.UnknownError);
            }
            finally
            {
                db.Close();
            }
        }

    }
}