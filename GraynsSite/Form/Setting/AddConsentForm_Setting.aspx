﻿<%@ Page Language="C#" Title="" MasterPageFile="~/Global1.Master" AutoEventWireup="true" CodeBehind="AddConsentForm_Setting.aspx.cs" Inherits="HJT.Form.Setting.AddConsentForm_Setting" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="https://tinymce.cachefly.net/4.0/tinymce.min.js"></script>
    <script type="text/javascript">
        tinymce.init({ selector: '.form-control-content', width: 900 });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <aside class="right-side">
        <section class="content-header">
            <h1>
                <asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><%= GetGlobalResourceObject("resource", "AddConsentForm")%></asp:Label>
            </h1>

            <asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div id="divErrorMessage" runat="server" visible="false" class="alert alert-danger alert-dismissable">
                        <i class="fa fa-ban"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <b>Alert!</b>
                        <br />
                        <asp:Label runat="server" ID="lblErr"></asp:Label>
                    </div>
                    <div class="box box-primary">
                        <div class="box-header" style="display: none">
                            <h4>
                                <asp:Label ID="lblTitle" runat="server">
                                </asp:Label>

                            </h4>
                        </div>

                        <div class="box-body">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr style="background: #dec18c;">
                                        <td colspan="2">
                                            <h4><strong><%= GetGlobalResourceObject("resource", "NewConsentForm")%></strong></h4>
                                        </td>
                                    </tr>
                                    <tr class="form-group" runat="server" id="trDocumentName">
                                        <td style="width: 20%; border-left: 0px!important;">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "DocumentName")%>&nbsp;<span style="color: Red;">*</span></label>
                                        </td>
                                        <td style="width: 80%;" class="controls">
                                            <asp:TextBox ID="txtDocumentName" runat="server" CssClass="form-control"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="form-group" runat="server" id="trCreatedBy">
                                        <td style="width: 20%; border-left: 0px!important;">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "CreatedBy")%>&nbsp;<span style="color: Red;"></span></label>
                                        </td>
                                        <td style="width: 80%;" class="controls">
                                            <asp:TextBox ID="txtCreatedBy" runat="server" CssClass="form-control"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="form-group" runat="server" id="trContent">
                                        <td style="border-left: 0px!important;">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "Content")%>&nbsp;<span style="color: Red;"></span></label>
                                        </td>
                                        <td style="width: 80%;" class="controls">
                                            <asp:TextBox ID="txtContent" runat="server" CssClass="form-control-content" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="form-group" runat="server" id="trRemarks">
                                        <td style="border-left: 0px!important;">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "Remarks")%>&nbsp;<span style="color: Red;"></span></label>
                                        </td>
                                        <td style="" class="controls">
                                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                        <div class="box-footer">
                            <asp:Button ID="btnBack" Text="<%$ Resources:Resource, Cancel%>" CssClass="btn" runat="server"
                                OnClick="btnBack_Click" OnClientClick="return confirm('The information is unsaved. Are you sure want to quit?')" />
                            <asp:Button ID="btnSubmit" Text="<%$ Resources:Resource, Submit%>" CssClass="btn btn-primary" runat="server"
                                OnClick="btnSubmit_Click" OnClientClick="return confirm('Are you sure want to add new information?')" />
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </aside>
</asp:Content>
