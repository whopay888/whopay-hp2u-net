﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Model;
using Synergy.Util;
using Synergy.Helper;

namespace WMElegance.Form.Setting
{
    public partial class CreditAdvisor_Setting : System.Web.UI.Page
    {
        LoginUserModel login_user = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];

            if (!IsPostBack)
            {
                sqlString.bindControl(gv, getExData(null, null, true));
                sqlString.bindControl(gvCreditType, getAdviseType());
                sqlString.bindControl(gvAdviceDetail, getAdviceDetail());
            }
        }

        #region GetData

        // Action[0], MemberID[1], Name[2], Ranking[3]*, Date[4], Status[5]
        protected string getExData(string columnName, string sortOrder, bool hasOrder)
        {
            StringBuilder sql = new StringBuilder();

            sql.Clear();
            sql.AppendFormat(@" 
                SELECT * FROM ( 
                    SELECT CAST(a.AdvisorID AS INT) AS 'AdvisorID',
                        a.Description AS 'Description',
                        a.Category, a.Remark
                    FROM tbl_CreditAdvisor a WITH (NOLOCK)
                    WHERE status = 'T' AND isDeleted = 0
                    {0}
                ) AS A
                ORDER BY A.[AdvisorID] ASC
            ", (txtSearchAdvisor.Text != string.Empty ? 
                    string.Format("AND (a.Description LIKE '%{0}%' OR a.Category LIKE '%{0}%') ", txtSearchAdvisor.Text) : "")
            );

            LogUtil.logSQL(sql.ToString());
            return sql.ToString();
        }

        #endregion

        #region GridView - GV

        private SortDirection GridViewSortDirection1
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;

                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }
        }

        protected void gv_Sorting(object sender, GridViewSortEventArgs e)
        {
            string direction = "ASC";

            if (GridViewSortDirection1 == SortDirection.Ascending)
            {
                GridViewSortDirection1 = SortDirection.Descending;
                direction = "DESC";
            }
            else
            {
                GridViewSortDirection1 = SortDirection.Ascending;
                direction = "ASC";
            }

            ViewState["Field1"] = e.SortExpression; ViewState["Direction1"] = direction;

            sqlString.bindControl(gv, getExData(e.SortExpression, direction, true));
        }

        #endregion

        #region Control Event

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            sqlString.bindControl(gv, getExData(null, null, true));
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            sqlString.exportFunc("City List", getExData(null, null, false));
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            txtSearchAdvisor.Text = "";
            sqlString.bindControl(gv, getExData(null, null, true));
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            TextBox txt = (TextBox)((GridViewRow)btn.NamingContainer).Cells[1].FindControl("txtCreditAdvisor");
            string id = ((GridViewRow)btn.NamingContainer).Cells[0].Text.ToString().Trim();
            wwdb db = new wwdb();
            string sql = "";

            try
            {
                if (txt.Text.Trim() != String.Empty)
                {
                    sql = string.Format(@" 
                            UPDATE tbl_CreditAdvisor SET Description = N'{0}', UpdatedBy = N'{1}', 
                                UpdatedAt = GETDATE() 
                            WHERE AdvisorID = N'{2}';
                        ", secure.RC(txt.Text.ToString())
                        , secure.RC(login_user.UserId)
                        , secure.RC(id));
                    db.Execute(sql);

                    LogUtil.logAction(sql, "Update Credit Advisor. ID: " + id);

                    sqlString.displayAlert(this, "Action_Successful", Request.Url.ToString());
                    sqlString.bindControl(gv, getExData(null, null, true));
                }
                else
                {
                    sqlString.displayAlert(this, "Textbox cannot be empty");
                    sqlString.bindControl(gv, getExData(null, null, true));
                }
            }
            catch (Exception ex) { LogUtil.logError(ex.ToString(), sql); sqlString.displayAlert(this, KeyVal.UnknownError); }
            finally { db.Close(); }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (validateForm())
            {
                wwdb db = new wwdb();
                StringBuilder sql = new StringBuilder();
                int newID;

                try
                {
                    sql.Clear();
                    sql.Append("Select TOP 1 CAST(AdvisorID AS INT) AS AdvisorID from tbl_CreditAdvisor WITH (NOLOCK) ORDER BY AdvisorID desc");
                    db.OpenTable(sql.ToString());
                    if (db.RecordCount() > 0)
                    {
                        newID = ConvertHelper.ConvertToInt(db.Item("AdvisorID").ToString(), 0) + 1;
                    }
                    else
                    {
                        newID = 1;
                    }

                    sql.Clear();
                    sql.Append(" Insert INTO tbl_CreditAdvisor (AdvisorID, Description, Status, IsDeleted, CreatedBy, CreatedAt, UpdatedAt, UpdatedBy) " +
                                " VALUES ('" + secure.RC(newID.ToString()) + "','" + secure.RC(txtNewAdvisor.Text) + "','A','false','" + secure.RC(login_user.UserId) + "', GETDate(), GetDate(), '" + secure.RC(login_user.UserId) + "');");
                    //sql.Append(" INSERT INTO tbl_Area (Area_Name, State_RowID, isActive, CreatedBy,UpdatedBy,UpdatedAt) VALUES " +
                    //    " (N'" + secure.RCL(txtNewBank.Text) + "', N'" + secure.RC(ddlState.SelectedValue) + "', 1, N'" + secure.RC(login_user.UserId) + "', N'" + secure.RC(login_user.UserId) + "',GetDate());");

                    db.Execute(sql.ToString());

                    sqlString.displayAlert(this, "Action_Successful");

                    txtNewAdvisor.Text = "";

                }
                catch (Exception ex)
                { LogUtil.logError(ex.ToString(), sql.ToString()); sqlString.displayAlert(this, KeyVal.UnknownError); }
                finally
                { 
                    db.Close();
                    sqlString.bindControl(gv, getExData(null, null, true));
                }
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            string id = ((GridViewRow)btn.NamingContainer).Cells[0].Text.ToString().Trim();
            wwdb db = new wwdb();
            string sql = "";

            try
            {

                //terminal
                //sql = " UPDATE tbl_login SET login_status = '0' WHERE login_id = (SELECT memberId FROM tbl_memberInfo WITH (NOLOCK) WHERE memberid = N'" + secure.RC(id) + "'); " +
                //    " UPDATE tbl_memberInfo SET status = 'D', updatedBy = N'" + secure.RC(login_user.UserId) + "' " +
                //    " , UpdatedAt = GETDATE() WHERE memberid = N'" + secure.RC(id) + "'; ";
                sql = string.Format(@"
                            UPDATE tbl_CreditAdvisor SET IsDeleted = 1, UpdatedBy = N'{0}'
                            , UpdatedAt = GETDATE() WHERE AdvisorID = N'{1}'; 
                        ", secure.RC(login_user.UserId)
                        , secure.RC(id));
                db.Execute(sql);
                LogUtil.logAction(sql, "Delete Credit Advisor. ID: " + id);
                sqlString.displayAlert(this, "Action_Successful");
                sqlString.bindControl(gv, getExData(null, null, true));
            }

            catch (Exception ex) { LogUtil.logError(ex.ToString(), sql); sqlString.displayAlert(this, KeyVal.UnknownError); }
            finally { db.Close(); }
        }
        protected bool validateForm()
        {
            bool result = false;
            StringBuilder errMsg = new StringBuilder();
            StringBuilder sql = new StringBuilder();
            wwdb db = new wwdb();

            try
            {
                if (String.IsNullOrEmpty(txtNewAdvisor.Text.Trim()))
                {
                    errMsg.Append(Resources.resource.Credit_Advisor_Note + "\\n");
                }
            }
            catch (Exception ex) { LogUtil.logError(ex.ToString(), sql.ToString()); errMsg.Append(sqlString.changeLanguage(KeyVal.UnknownError) + "\\n"); }
            finally { db.Close(); }

            if (errMsg.ToString().Trim() != string.Empty)
            {
                sqlString.displayAlert2(this, errMsg.ToString());
                lblErr.Text = errMsg.Replace("\\n", "<br />").ToString();
                lblErr.Visible = true;
                divErrorMessage.Visible = true;
                lblErr.Focus();
            }
            else
            {
                lblErr.Text = "";
                lblErr.Visible = false;
                divErrorMessage.Visible = false;
                result = true;
            }

            return result;
        }

        #region GridView Control
        #endregion
        #endregion

        #region GridView - gvCreditType
        private string getAdviseType()
        {
            StringBuilder sql = new StringBuilder();

            /*
            Category	ParameterName	ParameterValue	            Remarks	                            Sort
            -------------------------------------------------------------------------------------------------
            AdviseCode	CF	            Count Facilities	        Count Facilities advise	            1
            AdviseCode	AKPK	        AKPK	                    AKPK advise	                        2
            AdviseCode	SAA	            Special Attention Account	Special Attention Account advise	3
            AdviseCode	CLEAR	        CCRIS Clear	                CCRIS Clear advise	                4
            AdviseCode	CC	            Credit Card	                Credit Card advise	                5
            AdviseCode	TR	            Trade Reference	            Trade Reference advise	            6
            AdviseCode	LS	            Legal Status	            Legal Status advise	                7
            */
            sql.Clear();
            sql.AppendFormat(@"
                SELECT p.ParameterName as 'AdviseCode', 
                    p.ParameterValue as 'AdviseName', 
                    p.Remarks, p.Sort
                FROM tbl_Parameter p with (nolock)
                WHERE p.Category = 'AdviseCode'
                ORDER BY cast(Sort as int)
            ");

            return sql.ToString();
        }

        protected void gvCreditType_btnUpdate_Click(object sender, EventArgs e)
        {
            wwdb db = new wwdb();
            string sql = string.Empty;

            try
            {
                for (int count = 0; count < gvCreditType.Rows.Count; count++)
                {
                    TextBox txtSorting = (TextBox)gvCreditType.Rows[count].Cells[2].FindControl("txtSorting");
                    string adviseCode = gvCreditType.Rows[count].Cells[0].Text.ToString().Trim();
                    if (txtSorting.Text.Trim() != string.Empty)
                    {
                        sql += string.Format(@" 
                            UPDATE tbl_Parameter SET Sort = '{0}'
                            WHERE Category = 'AdviseCode' AND ParameterName = '{1}';
                        ", secure.RC(txtSorting.Text)
                        , secure.RC(adviseCode));
                    }
                    else
                    {
                        sqlString.displayAlert(this, "Textbox cannot be empty");
                        sqlString.bindControl(gvCreditType, getAdviseType());
                        break;
                    }
                }

                db.Execute(sql);
                LogUtil.logAction(sql, "Update Automation Advise Sorting");

                if (db.HasError)
                {
                    LogUtil.logError(db.ErrorMessage, sql);
                    sqlString.displayAlert(this, Resources.resource.unknownError);
                }
                else
                {
                    sqlString.displayAlert(this, Resources.resource.ActionSuccessfulMsg, Request.Url.ToString());
                    sqlString.bindControl(gvCreditType, getAdviseType());
                }
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.ToString(), sql);
                sqlString.displayAlert(this, KeyVal.UnknownError);
            }
            finally { db.Close(); }
        }
        #endregion

        #region GridView - gvAdviceDetail
        private string getAdviceDetail()
        {
            StringBuilder sql = new StringBuilder();

            sql.Clear();
            sql.AppendFormat(@"
                SELECT p.Category as 'MainCategory', 
                    p.ParameterName as 'Category', 
                    p.ParameterValue as 'Value'
                FROM tbl_Parameter p WITH (nolock)
                WHERE p.Category = 'AdviceDetail'
                ORDER BY p.ParameterName
            ");

            return sql.ToString();
        }
        protected void gvAdviceDetail_btnUpdate_Click(object sender, EventArgs e)
        {
            StringBuilder sql = new StringBuilder();
            wwdb db = new wwdb();

            GridViewRow gvRow = (GridViewRow)((Button)sender).NamingContainer;
            string category = gvRow.Cells[0].Text;
            string value = ((TextBox)gvRow.Cells[1].FindControl("txtValue")).Text;

            sql.Clear();
            sql.AppendFormat(@"
                UPDATE tbl_Parameter SET ParameterValue = '{0}'
                WHERE ParameterName = '{1}' AND Category = 'AdviceDetail';
            ", value, category);
            db.Execute(sql.ToString());
            if (db.HasError)
            {
                LogUtil.logError(db.ErrorMessage, sql.ToString());
                sqlString.displayAlert(this, Resources.resource.unknownError);
            }
            else
                sqlString.displayAlert(this, Resources.resource.ActionSuccessfulMsg, Request.Url.ToString());
        }
        #endregion
    }
}