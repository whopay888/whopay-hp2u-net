﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Global1.Master" AutoEventWireup="true" CodeBehind="UACDetails.aspx.cs" Inherits="WMElegance.Form.Setting.UACDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="/js/plugins/dynatree/jquery.dynatree.js"></script>
    <link rel="stylesheet" href="/css/plugins/dynatree/ui.dynatree.css" />
    <style>
        .functionalitycheckbox label {
            text-transform: none;
        }
        .functionalitycheckbox {
            display: block;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <aside class="right-side">
        <section class="content-header">
            <h1><span runat="server" id="spHeader"><%= GetGlobalResourceObject("resource", "RoleManagement")%></span></h1>
            <asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
        </section>
        <div class="content">

            <div id="divErrorMessage" runat="server" visible="false" class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <b>Alert!</b>
                <br />
                <asp:Label runat="server" ID="lblErr"></asp:Label>
            </div>

            <div class="box box-success">
                <div class="box-content">
                    <div class="col-md-12">
                        <div class="control-group">
                            <asp:HiddenField ID="HFAction" runat="server" />
                            <label class="control-label"><%= GetGlobalResourceObject("resource", "RoleCode")%></label>
                            <div class="controls">
                                <asp:TextBox ID="TxtRoleCode" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"><%= GetGlobalResourceObject("resource", "RoleName")%></label>
                            <div class="controls">
                                <asp:TextBox ID="TxtRoleName" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="controls">
                                <asp:CheckBox ID="ChkAll" runat="server" Visible="false" />
                                <!--  OnCheckedChanged="ChkAll_CheckedChanged" -->

                                <label class="control-label">
                                    <asp:Label ID="lblSelectAction" runat="server" Visible="false" Text="<%$ Resources:Resource, SelectAll%>"></asp:Label></label>
                                <asp:Button ID="btnSelectAll" CssClass="btn btn-danger" runat="server" OnClick="btnSelectAll_Click" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"><%= GetGlobalResourceObject("resource", "FunctionalityAccess")%></label>
                            <div class="controls" style="margin-left: 25px;">
                                <asp:Repeater runat="server" ID="rptFunctionalityAccessList" OnItemDataBound="rptFunctionalityAccessList_ItemDataBound">
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" ID="chkFunctionalityAccess" CssClass="functionalitycheckbox" />
                                    </ItemTemplate>
                                </asp:Repeater>


                                <asp:CheckBoxList runat="server" ID="chkFunctionalityAccessList">
                                </asp:CheckBoxList>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"><%= GetGlobalResourceObject("resource", "PageAccess")%></label>
                            <div class="controls">
                                <asp:TreeView ID="trAccessMenu" runat="server" PopulateNodesFromClient="True" CssClass="filetree checkbox"
                                    ShowCheckBoxes="All" ShowExpandCollapse="False" OnTreeNodePopulate="trAccessMenu_TreeNodePopulate" OnTreeNodeCheckChanged="trAccessMenu_TreeNodeCheckChanged" OnSelectedNodeChanged="trAccessMenu_SelectedNodeChanged"
                                    ShowLines="True">
                                    <NodeStyle CssClass="dynatree-checkbox" />
                                </asp:TreeView>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="box-footer">
                    <asp:Button ID="btnBack" Text="<%$ Resources:Resource, Back%>" runat="server" CssClass="btn btn-success" OnClick="btnBack_Click" />
                    <asp:Button ID="btnSubmit" Text="<%$ Resources:Resource, Submit%>" runat="server" CssClass="btn btn-submit" OnClick="btnSubmit_Click" />
                </div>
            </div>
        </div>
    </aside>
</asp:Content>
