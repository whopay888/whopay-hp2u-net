﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web.UI.WebControls;
using Synergy.Helper;
using Synergy.Model;
using Synergy.Util;

namespace HJT.Form.Setting
{
    public partial class AddConsentForm_Setting : System.Web.UI.Page
    {
        List<string> uploadDocs = new List<string>();
        LoginUserModel login_user = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];
            if (!IsPostBack)
            {
                txtCreatedBy.Text = login_user.UserId.ToString();
                txtCreatedBy.Attributes.Add("readonly", "readonly");
                Session["uploadDocs"] = uploadDocs;
            }
            uploadDocs = (List<string>)Session["uploadDocs"];
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Form/Setting/ConsentForm_Setting.aspx");
        }

        protected bool ValidateForm()
        {
            bool result = false;
            StringBuilder errMsg = new StringBuilder();

            if (String.IsNullOrEmpty(txtDocumentName.Text))
                errMsg.Append(Resources.resource.DocumentNameCannotEmpty + "\\n");
            if (uploadDocs.Count <= 0)
                errMsg.Append(Resources.resource.DocumentEmptyErrMsg + "\\n");
            if (errMsg.ToString().Trim() != string.Empty)
            {
                sqlString.displayAlert2(this, errMsg.ToString());
                lblErr.Text = errMsg.Replace("\\n", "<br />").ToString();
                lblErr.Visible = true;
                divErrorMessage.Visible = true;
                lblErr.Focus();
            }
            else
            {
                lblErr.Text = "";
                lblErr.Visible = false;
                divErrorMessage.Visible = false;
                result = true;
            }

            return result;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            wwdb db = new wwdb();
            string strLog;
            StringBuilder sql = new StringBuilder();
            try
            {
                int newID;

                sql.Clear();
                sql.Append("SELECT TOP 1 CAST(Id AS INT) AS DocumentID FROM tbl_ConsentDocs WITH (NOLOCK) ORDER BY ID desc");
                db.OpenTable(sql.ToString());
                if (db.RecordCount() > 0)
                {
                    newID = ConvertHelper.ConvertToInt(db.Item("DocumentID").ToString(), 0) + 1;
                }
                else
                {
                    newID = 1;
                }

                sql.Clear();

                sql.Append("INSERT INTO tbl_ConsentDocs(Id, DocsURL,IsDeleted, DocumentName, CreatedBy, CreatedAt, UpdatedBy, UpdatedAt, Remarks,Content,IsActive) ");
                sql.Append("VALUES(" + newID + ", N'',0, N'" + secure.RC(txtDocumentName.Text) + "', N'" + secure.RC(login_user.UserId) + "', ");
                sql.Append("GETDATE(), N'" + secure.RC(login_user.UserId) + "', GETDATE(), N'" + secure.RC(txtRemarks.Text) + "','" + secure.RC(txtContent.Text.Trim()) + "',0);");
                db.Execute(sql.ToString());

                strLog = "Add Document - (DocumentID : " + newID.ToString() + " , Document Name : " + txtDocumentName.Text.ToString() + " )";
                LogUtil.logAction(sql.ToString(), strLog.ToString());

                sqlString.displayAlert2(this, Resources.resource.Action_Successful, "/Form/Setting/ConsentForm_Setting.aspx");
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.ToString(), sql.ToString()); sqlString.displayAlert(this, KeyVal.UnknownError);
            }
            finally
            {
                db.Close();
            }
        }
    }
}