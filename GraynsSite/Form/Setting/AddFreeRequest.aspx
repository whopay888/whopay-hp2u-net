﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Global1.Master" CodeBehind="AddFreeRequest.aspx.cs" Inherits="WMElegance.Form.Setting.AddFreeRequest" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function confirmSubmit() {
            return confirm('<%= GetGlobalResourceObject("resource" , "ConfirmToRegister") %>');
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                <span class="icon fa fa-fw fa-book" aria-hidden="true"></span>
                <asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><span runat="server" id="spHeader"><%= GetGlobalResourceObject("resource", "AddFreeRequest")%></span></asp:Label>
            </h1>
            <asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
        </section>
        <section class="content">
            <div class="col-md-12">
                <div runat="server">
                    <div class="box-body">
                        <asp:Panel runat="server" DefaultButton="btnSubmit">
                            <table class="table table-bordered">
                                <tbody>
                                <tr class="form-group" runat="server">
                                    <td style="width: 20%; border-left: 0px!important;">
                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "SearchByFullName")%>&nbsp;<span style="color: Red;">*</span></label>
                                    </td>
                                    <td style="width: 80%;">
                                        <asp:Panel runat="server" DefaultButton="btnSearchName" Width="100%">
                                            <table style="width: 100%;">
                                                <tr>
                                                    <td style="width: 80%;" class="controls">
                                                        <asp:TextBox runat="server" ID="txtSearchName" Width="100%" CssClass="form-control" />
                                                    </td>
                                                    <td style="width: 20%;" class="controls">
                                                        <asp:Button runat="server" ID="btnSearchName" OnClick="btnSearchName_Click" CssClass="btn btn-searh" 
                                                            Text='<%$ Resources:resource, Search %>' />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr class="form-group" runat="server">
                                    <td style="width: 20%; border-left: 0px!important;">
                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "FullName")%>&nbsp;<span style="color: Red;">*</span></label>
                                    </td>
                                    <td style="width: 80%;" class="controls">
                                        <asp:DropDownList runat="server" ID="ddlFullname" CssClass="form-control" AutoPostBack="true" 
                                            OnSelectedIndexChanged="ddlFullName_SelectedIndexChanged" />
                                    </td>
                                </tr>
                                <tr class="form-group" runat="server">
                                    <td style="width: 20%; border-left: 0px!important;">
                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Branch")%></label>
                                    </td>
                                    <td style="width: 80%;" class="controls">
                                        <asp:TextBox ID="txtBranchName" runat="server" ReadOnly="true" CssClass="form-control" />
                                    </td>
                                </tr>
                                <tr class="form-group" runat="server">
                                    <td style="width: 20%; border-left: 0px!important;">
                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "ContactNumber")%></label>
                                    </td>
                                    <td style="width: 80%;" class="controls">
                                        <asp:TextBox ID="txtContactNumber" runat="server" ReadOnly="true" CssClass="form-control" />
                                    </td>
                                </tr>
                                <tr class="form-group" runat="server">
                                    <td style="width: 20%; border-left: 0px!important;">
                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "FreeRequest")%>&nbsp;<span style="color: Red;">*</span></label>
                                    </td>
                                    <td style="width: 80%;" class="controls">
                                        <asp:TextBox ID="txtFreeCount" runat="server" CssClass="form-control" />
                                    </td>
                                </tr>
                                <tr class="form-group" runat="server">
                                    <td style="width: 20%; border-left: 0px!important;">
                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Remark")%></label>
                                    </td>
                                    <td style="width: 80%;" class="controls">
                                        <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" CssClass="form-control" />
                                    </td>
                                </tr>
                            </tbody>
                            </table>
                            <div class="box-footer">
                                <asp:Button runat="server" ID="btnSubmit" Visible="true" CssClass="btn btn-edit" 
                                    OnClick="btnSubmit_Click" Text="<%$ Resources:Resource, Save%>" 
                                    OnClientClick="return confirm('Comfirm to add new record ?')" />
                            </div>
                        </asp:Panel>
                        <div class="box-body table-responsive">
                            <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow" ID="gv" runat="server" AutoGenerateColumns="false" 
                                GridLines="None" ShowFooter="false" EmptyDataText="<%$ Resources:Resource, No_Record_Found%>" 
                                CssClass="table table-bordered table-striped" Width="100%">
                                <Columns>
                                    <asp:BoundField DataField="Name" HeaderText="<%$ Resources:Resource, Name%>" />
                                    <asp:BoundField DataField="FreeRequest" HeaderText="<%$ Resources:Resource, FreeRequest%>" />
                                    <asp:BoundField DataField="CreatedBy" HeaderText="<%$ Resources:Resource, CreatedBy%>" />
                                    <asp:BoundField DataField="CreatedAt" HeaderText="<%$ Resources:Resource, Dates%>" />
                                    <asp:BoundField DataField="Remark" HeaderText="<%$ Resources:Resource, Remark%>" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </aside>


</asp:Content>
