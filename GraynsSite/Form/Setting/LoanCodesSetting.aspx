﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoanCodesSetting.aspx.cs" Inherits="HJT.Form.Setting.LoanCodesSetting" MasterPageFile="~/Global1.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function confirmSave() {
            return confirm('<%= GetGlobalResourceObject("Resource", "Confirm_Save_Changes") %>');
        }
        function confirmDelete() {
            return confirm('<%= GetGlobalResourceObject("Resource", "Confirm_Changes") %>');
        }
    </script>
    <style>

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                <asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><%= GetGlobalResourceObject("resource", "LoanCodeList")%></asp:Label>
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div id="divErrorMessage" runat="server" visible="false" class="alert alert-danger alert-dismissable">
                        <i class="fa fa-ban"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <b>Alert!</b>
                        <br />
                        <asp:Label runat="server" ID="lblErr"></asp:Label>
                    </div>
                    <div class="box">
                        <h4><b><asp:Literal runat="server" Text='<%$ Resources:resource, AddNew_LoanCode %>' /></b></h4>
                        <asp:Panel runat="server" DefaultButton="btnAddLoanCode">
                            <table style="width: 100%;" class="table table-bordered table-striped">
                                <tr style="width: 100%;">
                                    <td style="width: 20%">
                                        <asp:Label runat="server" Text='<%$ Resources:resource, LoanType %>' />
                                    </td>
                                    <td style="width: 20%">
                                        <asp:Label runat="server" Text='<%$ Resources:resource, LoanCode %>' />
                                    </td>
                                    <td style="width: 20%">
                                        <asp:Label runat="server" Text='<%$ Resources:resource, Action %>' />
                                    </td>
                                </tr>
                                <tr style="width: 100%;">
                                    <td style="width: 20%">
                                        <asp:DropDownList runat="server" ID="ddlLoanType" />
                                    </td>
                                    <td style="width: 20%">
                                        <asp:TextBox runat="server" ID="txtLoanCode" />
                                    </td>
                                    <td style="width: 20%">
                                        <asp:Button runat="server" ID="btnAddLoanCode" Text='<%$ Resources:resource, Add %>' 
                                            OnClick="btnAddLoanCode_Click" OnClientClick="return confirmSave();"
                                            CssClass="btn btn-searh"/>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <h4><b><asp:Literal runat="server" Text='<%$ Resources:resource, LoanCodeList %>' /></b></h4>
                        <asp:Panel runat="server" DefaultButton="btnSave">
                            <asp:GridView runat="server" AutoGenerateColumns="false" ID="gvLoanCode" GridLines="None" Width="100%" ShowFooter="true"
                                AlternatingRowStyle-CssClass="table-alternateRow" EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                CssClass="table table-bordered table-striped" OnRowDataBound="gvLoanCode_RowDataBound">
                                <Columns>
                                    <asp:BoundField DataField="ID" HeaderText='<%$ Resources:resource, ID %>' />
                                    <asp:TemplateField HeaderText='<%$ Resources:resource, LoanType %>'>
                                        <ItemTemplate>
                                            <asp:DropDownList runat="server" ID="ddlLoanType"/>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText='<%$ Resources:resource, LoanCode %>'>
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" ID="txtLoanCode"/>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText='<%$ Resources:resource, Action %>'>
                                        <ItemTemplate>
                                            <asp:Button runat="server" ID="btnSaveRow" OnClick="btnSaveRow_Click"
                                                OnClientClick="return confirmSave();" CommandArgument='<%# Container.DataItemIndex %>'
                                                Text='<%$ Resources:resource, Save %>' CssClass="btn btn-submit"/>
                                            <asp:Button runat="server" ID="btnDeleteRow" OnClick="btnDeleteRow_Click"
                                                OnClientClick="return confirmDelete();" CommandArgument='<%# Container.DataItemIndex %>'
                                                Text='<%$ Resources:resource, Delete %>' CssClass="btn btn-danger" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:Button runat="server" Text='<%$ Resources:resource, Save_All %>' ID="btnSave" OnClick="btnSave_Click"
                                OnClientClick="return confirmSave();" CssClass="btn btn-submit" />
                            <asp:Button runat="server" Text='<%$ Resources:resource, Refresh %>' ID="btnRefresh" OnClick="btnRefresh_Click"
                                CssClass="btn btn-searh"/>
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </section>
    </aside>
</asp:Content>