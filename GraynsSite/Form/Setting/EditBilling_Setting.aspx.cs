﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Controller;
using Synergy.Helper;
using Synergy.Model;
using Synergy.Util;

namespace WMElegance.Form.Setting
{
    public partial class EditBilling_Setting : System.Web.UI.Page
    {
        LoginUserModel login_user = null;
        private const string sectionReportPrice = "ReportPriceAndCreditLimit";
        private const string sectionPayment = "PaymentSetting";

        protected void Page_Load(object sender, EventArgs e)
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];
            if (!IsPostBack)
            {
                string section;
                //id = secure.Decrypt(Validation.GetUrlParameter(this.Page, "id", ""), true);
                section = secure.Decrypt(Validation.GetUrlParameter(this.Page, "section", ""), true);

                if (section.Equals(sectionReportPrice))
                {
                    divPriceLimit.Visible = true;
                    bindControl();
                    divPayment.Visible = false;
                    //bindControl2();
                }
                else if(section.Equals(sectionPayment))
                {
                    divPriceLimit.Visible = false;
                    //bindControl();
                    divPayment.Visible = true;
                    bindControl2();
                }
            }
        }

        private void bindControl()
        {
            string id;
            id = secure.Decrypt(Validation.GetUrlParameter(this.Page, "id", ""), true);
            sqlString.bindControl(ddlBranch, "SELECT * FROM (SELECT 0 AS '1', '*' AS '0' UNION ALL  SELECT BranchID AS '1', BranchName AS '0' FROM tbl_Branch WHERE isDeleted = 0 AND Status = 'A') AS A Order By A.[0]", "0", "1", true);

            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            sql.AppendFormat(@"
                SELECT tbs.Id, tbs.BranchId, 
                    CASE WHEN tbs.BranchId = 0 
                        THEN '*' 
                    ELSE tb.BranchName 
                END as BranchName, tbs.PerRequestPrice, tbs.PerSuccessfulCasePrice,
                tbs.CreditLimit, tbs.DateStart, tbs.DateEnd 
                FROM tbl_BillingSetting tbs LEFT JOIN 
                tbl_Branch tb ON tbs.BranchId = tb.BranchID
                Where tbs.Id = {0}
            ", id);
            db.OpenTable(sql.ToString());
            if (db.RecordCount() > 0)
            {
                ddlBranch.SelectedValue = db.Item("BranchId").ToString(); //db.Item("BranchName").ToString();
                txtPerRequestPrice.Text = db.Item("PerRequestPrice").ToString();
                txtPerSuccessfulCasePrice.Text = db.Item("PerSuccessfulCasePrice").ToString();
                txtCreditLimit.Text = db.Item("CreditLimit").ToString();
                txtFrom.Text = Convert.ToDateTime(db.Item("DateStart").ToString()).ToString("yyyy-MM-dd");
                txtTo.Text = Convert.ToDateTime(db.Item("DateEnd").ToString()).ToString("yyyy-MM-dd");
            }

            ddlBranch.Enabled = false;
            if (Convert.ToInt32(id) == 0)
            {
                txtFrom.Enabled = false;
                txtTo.Enabled = false;
            }
        }
        
        private void bindControl2()
        {
            string id = string.Empty;
            id = secure.Decrypt(Validation.GetUrlParameter(this.Page, "id", ""), true);
            
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            StringBuilder sqlFilter = new StringBuilder();
            
            string searchFilter = string.Empty;

            sql.AppendFormat(@"
                SELECT ts.[Text], ts.[TopupAmount], ts.[AdditionalCredit], ts.[FreeRequestCount], Sort, 
                    ts.[CreatedAt], ts.[CreatedBy], ts.[UpdatedAt], ts.[UpdatedBy]
                FROM [tbl_TopupSetting] ts
                where isDeleted=0 AND ts.ID = '{0}'
            ", id);
            db.OpenTable(sql.ToString());
            if (db.RecordCount() > 0)
            {
                txtText.Text = db.Item("Text");
                txtTopupAmount.Text = db.Item("TopupAmount");
                txtFreeCredit.Text = db.Item("AdditionalCredit");
                txtFreeRequest.Text = db.Item("FreeRequestCount");
                txtSort.Text = db.Item("Sort");
                txtCreatedDate.Text = db.Item("CreatedAt");
                txtCreatedBy.Text = db.Item("CreatedBy");
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            bool proceed = false;
            string id, section;
            section = secure.Decrypt(Validation.GetUrlParameter(this.Page, "section", ""), true);
            id = secure.Decrypt(Validation.GetUrlParameter(this.Page, "id", ""), true);

            if (section.Equals(sectionReportPrice))
                proceed = validateSubmit();
            if (section.Equals(sectionPayment))
                proceed = validatePaymentSubmit();

            if (proceed)
            {
                wwdb db = new wwdb();
                StringBuilder sql = new StringBuilder();
                if (section.Equals(sectionReportPrice))
                {
                    sql.Clear();
                    sql.Append("UPDATE tbl_BillingSetting SET PerRequestPrice =" + txtPerRequestPrice.Text +
                        ",PerSuccessfulCasePrice =" + txtPerSuccessfulCasePrice.Text + ", CreditLimit=" + txtCreditLimit.Text);
                    if (Convert.ToInt32(id) != 0)
                    {
                        sql.Append(",DateStart ='" + txtFrom.Text + "', DateEnd ='" + txtTo.Text + "'");
                    }
                    sql.Append(" WHERE Id =" + id);
                    db.Execute(sql.ToString(), true);
                }
                else if (section.Equals(sectionPayment))
                {
                    sql.Clear();
                    sql.AppendFormat(@"
                        UPDATE tbl_TopupSetting SET Text = '{0}', TopupAmount = '{1}', AdditionalCredit = '{2}', FreeRequestCount = '{3}', 
                            Sort = '{4}', UpdatedAt = GETDATE(), UpdatedBy = '{5}' WHERE ID = '{6}';
                    ", txtText.Text, txtTopupAmount.Text, txtFreeCredit.Text, txtFreeRequest.Text
                    , txtSort.Text, login_user.UserId, id);
                    db.Execute(sql.ToString());
                }

                if (db.HasError)
                {
                    LogUtil.logError(db.ErrorMessage, sql.ToString());
                    sqlString.displayAlert(this, Resources.resource.unknownError);
                }
                else //if (!db.HasError)
                    sqlString.displayAlert(this, Resources.resource.Action_Successful, "/Form/Setting/BillingList_Setting.aspx");
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("BillingList_Setting.aspx");
        }

        private bool validateSubmit()
        {
            string id = string.Empty;
            id = secure.Decrypt(Validation.GetUrlParameter(this.Page, "id", ""), true);
            bool result = false;
            wwdb db = new wwdb();
            StringBuilder errMsg = new StringBuilder();
            StringBuilder sql = new StringBuilder();


            if (String.IsNullOrEmpty(txtPerRequestPrice.Text))
            {
                errMsg.Append(Resources.resource.EmptyPerRequestPrice + "\\n");
            }

            if (String.IsNullOrEmpty(txtPerSuccessfulCasePrice.Text))
            {
                errMsg.Append(Resources.resource.EmptyPreSuccessfulCasePrice + "\\n");
            }

            if (String.IsNullOrEmpty(txtCreditLimit.Text))
            {
                errMsg.Append(Resources.resource.EmptyCreditLimit + "\\n");
            }

            if (!String.IsNullOrEmpty(txtPerRequestPrice.Text))
            {
                if (!Validation.isDecimal(txtPerRequestPrice.Text))
                {
                    errMsg.Append(Resources.resource.InvalidPerRequestPrice + "\\n");
                }
            }

            if (!String.IsNullOrEmpty(txtPerSuccessfulCasePrice.Text))
            {
                if (!Validation.isDecimal(txtPerSuccessfulCasePrice.Text))
                {
                    errMsg.Append(Resources.resource.InvalidPreSuccessfulCasePrice + "\\n");
                }
            }

            if (!String.IsNullOrEmpty(txtCreditLimit.Text))
            {
                if (!Validation.isDecimal(txtCreditLimit.Text))
                {
                    errMsg.Append(Resources.resource.InvalidCreditLimit + "\\n");
                }
            }

            if (Convert.ToInt32(id) != 0)
            {
                bool isValidateDate = true;

                if (!(!string.IsNullOrWhiteSpace(txtFrom.Text) && DateTime.TryParse(txtFrom.Text, out var from)))
                {
                    isValidateDate = false;
                    errMsg.Append(Resources.resource.InvalidFromDate + "\\n");
                }
                if (!(!string.IsNullOrWhiteSpace(txtTo.Text) && DateTime.TryParse(txtTo.Text, out var to)))
                {
                    isValidateDate = false;
                    errMsg.Append(Resources.resource.InvalidToDate + "\\n");
                }

                if (isValidateDate)
                {
                    if (DateTime.Parse(txtFrom.Text) > DateTime.Parse(txtTo.Text))
                    {
                        errMsg.Append(Resources.resource.InvalidRangeFromToDate + "\\n");
                    }
                }

                sql.AppendFormat(@"
                    SELECT 1 FROM tbl_BillingSetting tbs 
                    WHERE IsActive = 0 AND BranchId = {0} AND Id != {1} 
                    AND ((CONVERT(DATE,'{2}') BETWEEN DateStart AND DateEnd) OR (CONVERT(DATE,'{3}') BETWEEN DateStart AND DateEnd))
                ", ddlBranch.SelectedItem.Value, id, txtFrom.Text, txtTo.Text);
                db.OpenTable(sql.ToString());
                if (db.RecordCount() > 0)
                {
                    errMsg.Append(Resources.resource.InvalidBranchBillingSettingDate + "\\n");
                }
            }

            if (errMsg.ToString().Trim() != string.Empty)
            {
                sqlString.displayAlert2(this, errMsg.ToString());
                return false;
            }
            else
            {
                result = true;
            }

            return result;

        }

        private bool validatePaymentSubmit()
        {
            string id = string.Empty;
            id = secure.Decrypt(Validation.GetUrlParameter(this.Page, "id", ""), true);
            wwdb db = new wwdb();
            StringBuilder errMsg = new StringBuilder();
            StringBuilder sql = new StringBuilder();
            
            if (string.IsNullOrEmpty(txtText.Text))
                errMsg.Append(Resources.resource.Empty_Text + "\\n");

            if (string.IsNullOrEmpty(txtTopupAmount.Text))
                errMsg.Append(Resources.resource.Empty_TopupAmount + "\\n");

            txtFreeCredit.Text = Math.Round(ConvertHelper.ConvertToDecimal(txtFreeCredit.Text, 0)).ToString().Replace(",", "");
            if (string.IsNullOrEmpty(txtFreeCredit.Text))
                txtFreeCredit.Text = "0";
            else if (!Validation.isNumeric(txtFreeCredit.Text))
                errMsg.Append(Resources.resource.Invalid_FreeCredit + "\\n");

            if (string.IsNullOrEmpty(txtFreeRequest.Text))
                txtFreeRequest.Text = "0";
            else if (!Validation.isDecimal(txtFreeRequest.Text) && !Validation.isNumeric(txtFreeRequest.Text))
                errMsg.Append(Resources.resource.Invalid_FreeRequest + "\\n");

            if (string.IsNullOrEmpty(txtSort.Text))
            {
                sql.Clear();
                sql.AppendFormat(@"
                    SELECT top 1 ts.Sort FROM tbl_TopupSetting ts 
                    where isDeleted = 0 
                    order by [Sort] desc
                ");
                db.OpenTable(sql.ToString());
                if (db.RecordCount() > 0)
                    txtSort.Text = (ConvertHelper.ConvertToInt(db.Item("Sort"), 0) + 1).ToString();
            }
            else if (!Validation.isNumeric(txtSort.Text))
                errMsg.Append(Resources.resource.Invalid_SortOrer + "\\n");
            
            if (errMsg.ToString().Trim() != string.Empty)
            {
                sqlString.displayAlert2(this, errMsg.ToString());
                return false;
            }
            else
                return true;
        }
    }
}