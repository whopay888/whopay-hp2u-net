﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Controller;
using Synergy.Model;
using Synergy.Util;

namespace WMElegance.Form.Setting
{
    public partial class BillingList_Setting : System.Web.UI.Page
    {
        private const string sectionReportPrice = "ReportPriceAndCreditLimit";
        private const string sectionPayment = "PaymentSetting";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindControl();
                sqlString.bindControl(gv, getRequestList());
                sqlString.bindControl(gvPayment, getPaymentSetting(null));
            }

            if (gv.Rows.Count > 0)
                gridView.ApplyPaging(gv, -1);

            if (gvPayment.Rows.Count > 0)
                gridView.ApplyPaging(gvPayment, -1);
        }

        private void bindControl()
        {
            sqlString.bindControl(ddlBranch, "SELECT BranchID AS '1' , BranchName AS '0' FROM tbl_Branch WHERE isDeleted = 0 AND Status='A' Order By BranchName", "0", "1", true);

        }

        #region Report Price & Credit Limit Setting
        private string getRequestList()
        {
            StringBuilder sql = new StringBuilder();
            StringBuilder sqlFilter = new StringBuilder();


            string searchFilter = string.Empty;

            sql.AppendFormat("SELECT tbs.Id,tbs.BranchId,CASE WHEN tbs.BranchId = 0 THEN '*' ELSE tb.BranchName END as BranchName ,tbs.PerRequestPrice, tbs.PerSuccessfulCasePrice,");
            sql.Append("tbs.CreditLimit, tbs.DateStart, tbs.DateEnd FROM dbo.tbl_BillingSetting tbs LEFT JOIN dbo.tbl_Branch tb ON tbs.BranchId = tb.BranchID WHERE IsActive = 1");

            if (!string.IsNullOrWhiteSpace(txtFrom.Text) && !string.IsNullOrWhiteSpace(txtTo.Text) && DateTime.TryParse(txtFrom.Text, out var from) && DateTime.TryParse(txtTo.Text, out var to))
                sqlFilter.Append(" AND ((CONVERT(DATE,'" + txtFrom.Text + "') BETWEEN DateStart AND DateEnd) OR (CONVERT(DATE,'" + txtTo.Text + "') BETWEEN DateStart AND DateEnd)) ");

            if (ddlBranch.SelectedIndex != 0)
            {
                sqlFilter.Append(" AND tbs.BranchId =" + ddlBranch.SelectedItem.Value);
            }

            if (sqlFilter.ToString().Trim() != string.Empty)
                sql.Append(sqlFilter);
            sql.Append(" ORDER BY tbs.BranchId DESC");

            return sql.ToString();

        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button btnDelete = (Button)e.Row.FindControl("btnDelete");
                btnDelete.Visible = true;
                int branchId = int.Parse(DataBinder.Eval(e.Row.DataItem, "BranchId").ToString());
                if (branchId == 0)
                    btnDelete.Visible = false;
            }
        }
        protected void gv_DataBound(object sender, EventArgs e)
        {

        }
        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gv.PageIndex = e.NewPageIndex;
            sqlString.bindControl(gv, getRequestList());

            if (gv.Rows.Count > 0)
                gridView.ApplyPaging(gv, e.NewPageIndex);
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddBilling_Setting.aspx?section=" + sqlString.encryptURL(sectionReportPrice));
        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            Button btnsender = (Button)sender;

            string id = btnsender.CommandArgument.ToString();

            Response.Redirect("EditBilling_Setting.aspx?id=" + sqlString.encryptURL(id) + "&section=" + sqlString.encryptURL(sectionReportPrice));
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            sqlString.bindControl(gv, getRequestList());

            if (gv.Rows.Count > 0)
                gridView.ApplyPaging(gv, -1);

            //Page.RegisterStartupScript(this.GetType(), "CallMyFunction", "NewFunction()", true);
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "MyFun1", "NewFunction();", true);
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            wwdb db = new wwdb();
            Button btnSender = (Button)sender;
            db.Execute("UPDATE tbl_BillingSetting SET IsActive = 0 WHERE Id='" + btnSender.CommandArgument.ToString() + "'");
            sqlString.displayAlert2(this, Resources.resource.Action_Successful, Request.Url.ToString());
        }
        #endregion

        #region Payment Setting
        protected void gvPayment_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Dictionary<string, string> sortBy = new Dictionary<string, string>();
            if (ViewState["SortBy"] != null)
            { sortBy = (Dictionary<string, string>)ViewState["SortBy"]; }

            gvPayment.PageIndex = e.NewPageIndex;
            sqlString.bindControl(gvPayment, getPaymentSetting(sortBy));

            if (gvPayment.Rows.Count > 0)
                gridView.ApplyPaging(gvPayment, e.NewPageIndex);
        }
        protected void gvPayment_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //UpdatedBy
                string updatedByID = DataBinder.Eval(e.Row.DataItem, "UpdatedBy").ToString();
                string updatedBy = string.Empty;
                if (!string.IsNullOrEmpty(updatedByID))
                {
                    wwdb db = new wwdb();
                    db.OpenTable("SELECT mi.Fullname FROM tbl_MemberInfo mi with (nolock) WHERE mi.MemberId='" + updatedByID + "';");
                    if (db.RecordCount() > 0)
                        updatedBy = db.Item("Fullname");
                }
                Label lblUpdatedBy = (Label)e.Row.Cells[5].FindControl("lblUpdatedBy");
                TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                if (!string.IsNullOrWhiteSpace(updatedBy))
                    lblUpdatedBy.Text = textInfo.ToTitleCase(updatedBy.ToLower());
            }
        }
        private string getPaymentSetting(IDictionary<string, string> sortBy)
        {
            StringBuilder sql = new StringBuilder();
            wwdb db = new wwdb();

            sql.AppendFormat(@"
                SELECT ts.ID, ts.[Text], ts.[TopupAmount], ts.[AdditionalCredit], ts.[FreeRequestCount], Sort
                        , ts.[CreatedAt], ts.[CreatedBy], ts.[UpdatedAt], ts.[UpdatedBy]
                FROM [tbl_TopupSetting] ts
                where isDeleted=0
            ");

            sql.Append(" order by ");
            if (sortBy != null && sortBy.Any())
            {
                int i = 0;
                foreach (var item in sortBy)
                {
                    string key = item.Key;
                    string value = item.Value;

                    if (i > 0)
                        sql.Append(",");

                    switch (key)
                    {
                        case "TopupAmount":
                            sql.Append("ts.[TopupAmount] " + value);
                            break;
                        case "AdditionalCredit":
                            sql.Append("ts.[AdditionalCredit] " + value);
                            break;
                        case "FreeRequestCount":
                            sql.Append("ts.[FreeRequestCount] " + value);
                            break;
                        case "Sort":
                            sql.Append("ts.Sort " + value);
                            break;
                        case "CreatedAt":
                            sql.Append("ts.CreatedAt " + value);
                            break;
                        case "UpdatedAt":
                            sql.Append("ts.UpdatedAt " + value);
                            break;
                        default:
                            break;
                    }
                    i++;
                }
            }
            else
                sql.Append("ts.Sort asc");

            return sql.ToString();
        }
        
        protected void btnDelete_Click1(object sender, EventArgs e)
        {
            wwdb db = new wwdb();
            Button btnSender = (Button)sender;
            db.Execute("UPDATE tbl_TopupSetting SET IsDeleted = 1 WHERE Id='" + btnSender.CommandArgument.ToString() + "'");
            sqlString.displayAlert2(this, Resources.resource.Action_Successful, Request.Url.ToString());
        }
        protected void btnEdit_Click1(object sender, EventArgs e)
        {
            Button btnsender = (Button)sender;

            string id = btnsender.CommandArgument.ToString();

            Response.Redirect("EditBilling_Setting.aspx?id=" + sqlString.encryptURL(id) + "&section=" + sqlString.encryptURL(sectionPayment));
        }
        protected void btnAdd_Click1(object sender, EventArgs e)
        {
            Response.Redirect("AddBilling_Setting.aspx?section=" + sqlString.encryptURL(sectionPayment));
        }

        protected void gvPayment_Sorting(object sender, GridViewSortEventArgs e)
        {
            string expression = e.SortExpression;
            string direction = null;

            //3 state sorting
            //ASC,DESC,null
            if (GetGridViewSortDirection(expression) == "ASC")
            {
                SetGridViewSortDirection(expression, "DESC");
                direction = "DESC";
            }
            else if (GetGridViewSortDirection(expression) == "DESC")
            {
                SetGridViewSortDirection(expression, null);
                direction = null;
            }
            else if (GetGridViewSortDirection(expression) == null)
            {
                SetGridViewSortDirection(expression, "ASC");
                direction = "ASC";
            }

            Dictionary<string, string> sortBy = new Dictionary<string, string>();
            if (direction != null)
                sortBy.Add(e.SortExpression, direction);

            ViewState["SortBy"] = sortBy;

            sqlString.bindControl(gvPayment, getPaymentSetting(sortBy));

            if (gvPayment.Rows.Count > 0)
                gridView.ApplyPaging(gvPayment, -1);
        }
        private string GetGridViewSortDirection(string sortExpression)
        {
            if (ViewState["sortDirection" + sortExpression] == null)
                return null;
            else
                return (string)ViewState["sortDirection" + sortExpression];
        }
        private void SetGridViewSortDirection(string sortExpression, string value)
        {
            ViewState["sortDirection" + sortExpression] = value;
        }
        #endregion
    }
}