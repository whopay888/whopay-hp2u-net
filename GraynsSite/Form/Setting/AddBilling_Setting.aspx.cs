﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Controller;
using Synergy.Helper;
using Synergy.Model;
using Synergy.Util;

namespace WMElegance.Form.Setting
{
    public partial class AddBilling_Setting : System.Web.UI.Page
    {
        LoginUserModel login_user = null;
        private const string sectionReportPrice = "ReportPriceAndCreditLimit";
        private const string sectionPayment = "PaymentSetting";
        protected void Page_Load(object sender, EventArgs e)
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];
            if (!IsPostBack)
            {
                string section = string.Empty;
                section = secure.Decrypt(Validation.GetUrlParameter(this.Page, "section", ""), true);

                if (section.Equals(sectionReportPrice))
                {
                    divPriceLimit.Visible = true;
                    divPayment.Visible = false;
                    bindControl();
                }
                else if (section.Equals(sectionPayment))
                {
                    divPriceLimit.Visible = false;
                    divPayment.Visible = true;
                }
            }
        }

        #region Report Price and Credit Limit Setting
        private void bindControl()
        {
            sqlString.bindControl(ddlBranch, "SELECT BranchID AS '1' , BranchName AS '0' FROM tbl_Branch WHERE isDeleted = 0 AND Status='A' Order By BranchName", "0", "1", true);

        }
        private bool validateSubmit()
        {
            bool result = false;
            wwdb db = new wwdb();
            StringBuilder errMsg = new StringBuilder();
            StringBuilder sql = new StringBuilder();

            if (ddlBranch.SelectedIndex == 0)
            {
                errMsg.Append(Resources.resource.EmptyBranch + "\\n");
            }

            if (String.IsNullOrEmpty(txtPerRequestPrice.Text))
            {
                errMsg.Append(Resources.resource.EmptyPerRequestPrice + "\\n");
            }

            if (String.IsNullOrEmpty(txtPerSuccessfulCasePrice.Text))
            {
                errMsg.Append(Resources.resource.EmptyPreSuccessfulCasePrice + "\\n");
            }

            if (String.IsNullOrEmpty(txtCreditLimit.Text))
            {
                errMsg.Append(Resources.resource.EmptyCreditLimit + "\\n");
            }

            if (!String.IsNullOrEmpty(txtPerRequestPrice.Text))
            {
                if (!Validation.isDecimal(txtPerRequestPrice.Text))
                {
                    errMsg.Append(Resources.resource.InvalidPerRequestPrice + "\\n");
                }
            }

            if (!String.IsNullOrEmpty(txtPerSuccessfulCasePrice.Text))
            {
                if (!Validation.isDecimal(txtPerSuccessfulCasePrice.Text))
                {
                    errMsg.Append(Resources.resource.InvalidPreSuccessfulCasePrice + "\\n");
                }
            }

            if (!String.IsNullOrEmpty(txtCreditLimit.Text))
            {
                if (!Validation.isDecimal(txtCreditLimit.Text))
                {
                    errMsg.Append(Resources.resource.InvalidCreditLimit + "\\n");
                }
            }

            bool isValidateDate = true;

            if (!(!string.IsNullOrWhiteSpace(txtFrom.Text) && DateTime.TryParse(txtFrom.Text, out var from)))
            {
                isValidateDate = false;
                errMsg.Append(Resources.resource.InvalidFromDate + "\\n");
            }
            if (!(!string.IsNullOrWhiteSpace(txtTo.Text) && DateTime.TryParse(txtTo.Text, out var to)))
            {
                isValidateDate = false;
                errMsg.Append(Resources.resource.InvalidToDate + "\\n");
            }

            if (isValidateDate)
            {
                if (DateTime.Parse(txtFrom.Text) > DateTime.Parse(txtTo.Text))
                {
                    errMsg.Append(Resources.resource.InvalidRangeFromToDate + "\\n");
                }
            }

            sql.Append("SELECT 1 FROM dbo.tbl_BillingSetting tbs WHERE BranchId = " + ddlBranch.SelectedItem.Value + " AND ((CONVERT(DATE,'" + txtFrom.Text + "') BETWEEN DateStart AND DateEnd) OR (CONVERT(DATE,'" + txtTo.Text + "') BETWEEN DateStart AND DateEnd))");
            db.OpenTable(sql.ToString());
            if (db.RecordCount() > 0)
            {
                errMsg.Append(Resources.resource.InvalidBranchBillingSettingDate + "\\n");
            }


            if (errMsg.ToString().Trim() != string.Empty)
            {
                sqlString.displayAlert2(this, errMsg.ToString());
                return false;
            }
            else
            {
                result = true;
            }

            return result;

        }
        #endregion

        #region Payment Setting
        private bool validatePaymentSubmit()
        {
            wwdb db = new wwdb();
            StringBuilder errMsg = new StringBuilder();
            StringBuilder sql = new StringBuilder();

            if (string.IsNullOrEmpty(txtText.Text))
                errMsg.Append(Resources.resource.Empty_Text + "\\n");

            if (string.IsNullOrEmpty(txtTopupAmount.Text))
                errMsg.Append(Resources.resource.Empty_TopupAmount + "\\n");

            txtFreeCredit.Text = Math.Round(ConvertHelper.ConvertToDecimal(txtFreeCredit.Text, 0)).ToString().Replace(",", "");
            if (string.IsNullOrEmpty(txtFreeCredit.Text))
                txtFreeCredit.Text = "0";
            else if (!Validation.isNumeric(txtFreeCredit.Text))
                errMsg.Append(Resources.resource.Invalid_FreeCredit + "\\n");

            if (string.IsNullOrEmpty(txtFreeRequest.Text))
                txtFreeRequest.Text = "0";
            else if (!Validation.isDecimal(txtFreeRequest.Text) && !Validation.isNumeric(txtFreeRequest.Text))
                errMsg.Append(Resources.resource.Invalid_FreeRequest + "\\n");

            if (string.IsNullOrEmpty(txtSort.Text))
            {
                sql.Clear();
                sql.AppendFormat(@"
                    SELECT top 1 ts.Sort FROM tbl_TopupSetting ts 
                    where isDeleted = 0 
                    order by [Sort] desc
                ");
                db.OpenTable(sql.ToString());
                if (db.RecordCount() > 0)
                    txtSort.Text = (ConvertHelper.ConvertToInt(db.Item("Sort"), 0) + 1).ToString();
            }
            else if (!Validation.isNumeric(txtSort.Text))
                errMsg.Append(Resources.resource.Invalid_SortOrer + "\\n");

            if (errMsg.ToString().Trim() != string.Empty)
            {
                sqlString.displayAlert2(this, errMsg.ToString());
                return false;
            }
            else
                return true;
        }
        #endregion
        
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            bool proceed = false;
            string section = string.Empty;
            section = secure.Decrypt(Validation.GetUrlParameter(this.Page, "section", ""), true);

            if (section.Equals(sectionReportPrice))
                proceed = validateSubmit();
            if (section.Equals(sectionPayment))
                proceed = validatePaymentSubmit();

            if (proceed)
            {
                wwdb db = new wwdb();
                StringBuilder sql = new StringBuilder();
                if (section.Equals(sectionReportPrice))
                {
                    sql.Clear();
                    sql.AppendFormat(@"
                        INSERT INTO tbl_BillingSetting(BranchId, PerRequestPrice, PerSuccessfulCasePrice, CreditLimit, DateStart, DateEnd, IsActive, CreatedBy, CreatedAt)
                        VALUES({0}, {1}, {2}, {3}, '{4}', '{5}', 1, '{6}', GETDATE())
                    ", ddlBranch.SelectedItem.Value, txtPerRequestPrice.Text, txtPerSuccessfulCasePrice.Text
                    , txtCreditLimit.Text, txtFrom.Text, txtTo.Text, login_user.UserId);
                    db.Execute(sql.ToString(), true);
                }
                else if (section.Equals(sectionPayment))
                {
                    sql.Clear();
                    sql.AppendFormat(@"
                        INSERT INTO tbl_TopupSetting 
                        (Text, TopupAmount, AdditionalCredit, FreeRequestCount, Sort, isDeleted, CreatedAt, CreatedBy, UpdatedAt, UpdatedBy)
                        values('{0}', '{1}', '{2}', '{3}', '{4}', 0, GETDATE(), '{5}', GETDATE(), '{6}')
                    ", txtText.Text, txtTopupAmount.Text, txtFreeCredit.Text, txtFreeRequest.Text, txtSort.Text
                    , login_user.UserId, login_user.UserId);
                    db.Execute(sql.ToString());
                }

                if (db.HasError)
                {
                    LogUtil.logError(db.ErrorMessage, sql.ToString());
                    sqlString.displayAlert(this, Resources.resource.unknownError);
                }
                else //if (!db.HasError)
                    sqlString.displayAlert(this, Resources.resource.Action_Successful, "/Form/Setting/BillingList_Setting.aspx");
            }
        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("BillingList_Setting.aspx");
        }
    }
}