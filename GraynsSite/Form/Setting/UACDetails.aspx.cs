﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Util;

namespace WMElegance.Form.Setting
{
    public partial class UACDetails : System.Web.UI.Page
    {
        String ReturnPage = "UACList.aspx";
        private bool updatingTreeView;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Populate Functionality Access
                this.GenerateFunctionalMenu();

                //Populate Tree View -----------------
                this.Generate_MainMenu();
                this.trAccessMenu.ExpandAll();
                //-------------------------------------
                btnSelectAll.Text = "Select All";
                //Determine Action --------------------
                this.Parameter_Setting();
                //-------------------------------------
            }
        }

        #region FUNCTION
        private void GenerateFunctionalMenu()
        {
            String RoleID = this.GET_EditRoleId();

            string SQL = "SELECT A.[RowId] AS 'ID', A.[Name] AS 'Name',";
            if (!String.IsNullOrEmpty(RoleID))
            { SQL += " (SELECT COUNT(*) FROM tbl_RoleMenu TRM WHERE TRM.RoleId = N'" + secure.RC(RoleID) + "' AND TRM.MenuId = A.[RowID]) AS 'IsAccessible'"; }
            else { SQL += " '0' AS 'IsAccessible'"; }
            SQL += " FROM tbl_Menu A WHERE A.[ParentID] = 0 AND A.[IsDeleted] = 0 AND A.[Type] = 2";

            //sqlString.bindControl(chkFunctionalityAccessList, SQL, "Name", "ID", "IsAccessible");
            sqlString.bindControl(rptFunctionalityAccessList, SQL);
        }

        private void Parameter_Setting()
        {
            String Action_Val = Request["action"];
            if (!String.IsNullOrEmpty(Action_Val))
            {
                switch (Action_Val.ToLower())
                {
                    case "add":

                        this.TxtRoleCode.Enabled = true;
                        break;
                    case "edit":
                        this.TxtRoleCode.Enabled = false;
                        this.Bind_RoleInfo();
                        break;
                    default:
                        Response.Redirect(ReturnPage);
                        break;
                }
            }
            else { Response.Redirect(ReturnPage); }

            this.HFAction.Value = Action_Val.ToLower();

            this.btnSubmit.OnClientClick = "return confirm('" + Resources.resource.UpdateRoleNotification + "')";
        }

        private void Bind_RoleInfo()
        {
            Boolean RoleId_IsValid = false;
            String RoleID_Val = Request["Roleid"];
            if (!String.IsNullOrEmpty(RoleID_Val))
            {
                //Verify If Role Id Exists -------------------
                wwdb db = new wwdb();
                String SQL = String.Empty;
                try
                {
                    String Decry_RoleId = secure.Decrypt(secure.DURC(RoleID_Val), true);
                    SQL = "SELECT RoleId, RoleName FROM tbl_RoleControl WHERE RoleId = N'" + Decry_RoleId + "'" +
                          " AND IsDeleted = 0 ";
                    db.OpenTable(SQL);
                    if (!db.HasError && db.RecordCount() == 1)
                    {
                        String RoleID = db.Item("RoleID");
                        this.TxtRoleCode.Text = RoleID;

                        String RoleName = db.Item("RoleName");
                        this.TxtRoleName.Text = RoleName;

                        RoleId_IsValid = true;
                    }
                }
                catch { db.Close(); }
                //--------------------------------------------
            }


            if (!RoleId_IsValid)
            { sqlString.displayAlert2(this, "Role Id does not exists or has been removed", ReturnPage); }

        }

        private String GET_EditRoleId()
        {
            String Decry_RoleId = String.Empty;
            try
            {
                String Action_Val = Request["action"];
                if ((!String.IsNullOrEmpty(Action_Val)) && (Action_Val.ToLower() == "edit"))
                {
                    String RoleID_Val = Request["Roleid"];
                    Decry_RoleId = secure.Decrypt(secure.DURC(RoleID_Val), true);
                }
            }
            catch { }
            return Decry_RoleId;
        }

        private void Generate_MainMenu()
        {

            wwdb db = new wwdb();
            String SQL = String.Empty;
            DataTable dt = new DataTable();
            String RoleID = this.GET_EditRoleId();
            try
            {
                SQL = "SELECT A.[RowId] AS 'ID', A.[Name] AS 'Menu',";

                if (!String.IsNullOrEmpty(RoleID))
                { SQL += " (SELECT COUNT(*) FROM tbl_RoleMenu TRM WHERE TRM.RoleId = N'" + secure.RC(RoleID) + "' AND TRM.MenuId = A.[RowID]) AS 'IsAccessible',"; }
                else { SQL += " '0' AS 'IsAccessible',"; }

                SQL += " (SELECT COUNT(*) FROM tbl_Menu B WHERE b.[ParentId] = A.[RowId] AND IsDeleted = 0 AND b.menulevel != 999 ) AS 'ChildNodeCount'";
                SQL += " FROM tbl_Menu A WHERE A.[ParentID] = 0 AND A.[IsDeleted] = 0 AND a.type = 1 ";

                dt = db.getDataTable(SQL);
            }
            catch (Exception ex) { LogUtil.logError(ex.ToString(), SQL); sqlString.displayAlert(this, KeyVal.UnknownError); }
            finally { db.Close(); }

            if (dt != null)
            {
                this.PopulateNodes(dt, trAccessMenu.Nodes, "M");
            }
        }

        private void Generate_SubMenu(String parentid, TreeNode parentNode)
        {
            wwdb db = new wwdb();
            String SQL = String.Empty;
            DataTable dt = new DataTable();
            String RoleID = this.GET_EditRoleId();
            try
            {
                SQL = " SELECT a.[rowID] AS 'ID', a.[name] AS 'Menu', ";

                if (!String.IsNullOrEmpty(RoleID))
                { SQL += " (SELECT COUNT(*) FROM tbl_RoleMenu TRM WHERE TRM.RoleId = N'" + secure.RC(RoleID) + "' AND TRM.MenuId = A.[RowId]) AS 'IsAccessible',"; }
                else { SQL += " '0' AS 'IsAccessible',"; }

                SQL += " (SELECT COUNT(*) FROM tbl_menu b WITH (NOLOCK) WHERE b.parentID = a.rowid AND b.[isdeleted] = 0 AND b.menulevel != 999 ) AS 'childnodecount' ";
                SQL += " FROM tbl_menu a WITH (NOLOCK) WHERE a.[parentID] = N'" + parentid + "' AND a.isdeleted = 0 AND a.type = 1 ORDER BY a.Sort ";
                dt = db.getDataTable(SQL);
            }
            catch (Exception ex) { LogUtil.logError(ex.ToString(), SQL); sqlString.displayAlert(this, KeyVal.UnknownError); }
            finally { db.Close(); }

            if (dt != null)
            {
                PopulateNodes(dt, parentNode.ChildNodes, "S");
            }
        }

        private void Generate_SubMenuControl(String ParentID, TreeNode ParentNode)
        {
            wwdb db = new wwdb();
            String SQL = String.Empty;
            DataTable dt = new DataTable();
            String RoleID = this.GET_EditRoleId();
            try
            {
                SQL = " SELECT A.[Ctrl_Id] AS 'ID', a.[Ctrl_Name] AS 'Menu', ";

                if (!String.IsNullOrEmpty(RoleID))
                { SQL += " (SELECT COUNT(*) FROM [tbl_Role_MenuControl] TRMC WHERE TRMC.[RCtrl_RoleId] = N'" + secure.RC(RoleID) + "' AND TRMC.[Ctrl_Id] = A.[Ctrl_Id]) AS 'IsAccessible',"; }
                else { SQL += " '0' AS 'IsAccessible',"; }

                SQL += " '0' AS 'childnodecount' ";
                SQL += " FROM tbl_MenuControl A WITH (NOLOCK) WHERE A.[Ctrl_MenuId] = N'" + ParentID + "' AND A.[Ctrl_Status] = 1 ORDER BY A.[Ctrl_Name] ";
                dt = db.getDataTable(SQL);
            }
            catch (Exception ex) { LogUtil.logError(ex.ToString(), SQL); sqlString.displayAlert(this, KeyVal.UnknownError); }
            finally { db.Close(); }

            if (dt != null)
            {
                PopulateNodes(dt, ParentNode.ChildNodes, "N");
            }
        }

        private void PopulateNodes(DataTable dt, TreeNodeCollection nodes, String MenuType)
        {
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    TreeNode tn = new TreeNode();
                    tn.Text = dr["Menu"].ToString();
                    tn.Value = dr["ID"].ToString();

                    if (tn.Text == "[BUTTON] Edit Profile")
                    { }
                    else
                    {
                        String IsAccessible = dr["IsAccessible"].ToString();
                        if (IsAccessible == "1")
                        { tn.Checked = true; }
                        tn.Target = MenuType;
                        tn.SelectAction = TreeNodeSelectAction.Expand;
                        //If node has child nodes, then enable on-demand populating

                        Int32 ChilNodeCount = Convert.ToInt32(dr["ChildNodeCount"]);
                        tn.PopulateOnDemand = (ChilNodeCount) > 0;
                        nodes.Add(tn);

                        if ((MenuType == "S") && (ChilNodeCount == 0)) //Generate Sub Menu Control (For Sub Menu Only)
                        {
                            this.Generate_SubMenuControl(dr["ID"].ToString(), tn);
                        }

                    }

                }
            }
        }

        private Boolean Remove_ExistingAccess()
        {
            wwdb db = new wwdb();
            Boolean Upd_Val = false;
            String RoleId = this.GET_EditRoleId();

            String SQL = "DELETE FROM tbl_RoleMenu WHERE RoleId = N'" + secure.RC(RoleId) + "'";
            Upd_Val = db.Execute(SQL);

            db.Close();
            return Upd_Val;
        }

        private Boolean Update_Role(String Action)
        {
            Boolean Update_Success = false;
            String SQL = String.Empty;
            String RoleID = String.Empty;
            String RoleName = this.TxtRoleName.Text.Trim();
            wwdb db = new wwdb();
            switch (Action)
            {
                case "add":
                    RoleID = this.TxtRoleCode.Text.Trim();
                    SQL = "INSERT INTO tbl_RoleControl (RoleId, RoleName, HaveBranch) VALUES (N'" + secure.RC(RoleID) + "',N'" + secure.RC(RoleName) + "',1)";
                    break;
                case "edit":
                    RoleID = this.GET_EditRoleId();
                    SQL = "UPDATE tbl_RoleControl  SET RoleName= N'" + secure.RC(RoleName) + "'" +
                          " WHERE RoleId = N'" + secure.RC(RoleID) + "'";
                    break;
            }
            Update_Success = db.Execute(SQL);
            db.Close();
            return Update_Success;
        }

        private Boolean Add_NewAccess()
        {
            List<int> menuIds = new List<int>();
            Boolean Upd_Val = false;
            String SQL = String.Empty;
            String RoleId = this.TxtRoleCode.Text.Trim();
            SQL = "INSERT INTO tbl_RoleMenu (MenuId, RoleId) VALUES ";


            //foreach (ListItem item in chkFunctionalityAccessList.Items)
            //{
            //    if (item.Selected)
            //    {
            //        SQL += "('" + secure.RC(item.Value) + "','" + secure.RC(RoleId) + "'),";
            //    }
            //}
            foreach (RepeaterItem item in rptFunctionalityAccessList.Items)
            {
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                {
                    var checkBox = (CheckBox)item.FindControl("chkFunctionalityAccess");
                    if (checkBox.Checked)
                    {
                        SQL += "('" + secure.RC(checkBox.Attributes["value"]) + "','" + secure.RC(RoleId) + "'),";
                        menuIds.Add(Convert.ToInt32(secure.RC(checkBox.Attributes["value"])));
                    }
                }
            }
            for (int a = 0; a < trAccessMenu.Nodes.Count; a++)
            {
                if (trAccessMenu.Nodes[a].Checked)
                {
                    SQL += "('" + secure.RC(trAccessMenu.Nodes[a].Value) + "','" + secure.RC(RoleId) + "'),";
                    menuIds.Add(Convert.ToInt32(secure.RC(trAccessMenu.Nodes[a].Value)));

                    SQL = this.Append_SubNode(trAccessMenu.Nodes[a], secure.RC(RoleId), SQL, ref menuIds);
                }
            }

            if (!String.IsNullOrEmpty(SQL))
            { SQL = SQL.Remove(SQL.Length - 1, 1); }

            if (menuIds.Count > 0)
            {
                SQL += "; INSERT INTO tbl_RoleMenu(MenuId, RoleId) SELECT rowID, N'" + secure.RC(RoleId) + "' FROM tbl_Menu WITH(NOLOCK) WHERE " +
                    " isDeleted = 0 AND parentID IN (" + string.Join(",", menuIds) + ") AND menulevel = 999 AND rowID NOT IN (SELECT MenuId FROM tbl_RoleMenu WITH (NOLOCK) WHERE RoleId = N'" + secure.RC(RoleId) + "'); ";
            }
            wwdb db = new wwdb();
            Upd_Val = db.Execute(SQL);
            db.Close();

            return Upd_Val;
        }

        private String Append_SubNode(TreeNode ParentNode, String RoleId, String SQL, ref List<int> menuIds)
        {
            //Update on Children Pages ---------------------------------------------
            for (int i = 0; i < ParentNode.ChildNodes.Count; i++)
            {
                String Menu_Id = ParentNode.ChildNodes[i].Value;
                Boolean Menu_Access = (ParentNode.ChildNodes[i].Checked) ? true : false;
                if (Menu_Access)
                {
                    SQL += "('" + secure.RC(ParentNode.ChildNodes[i].Value) + "','" + secure.RC(RoleId) + "'),";
                    menuIds.Add(Convert.ToInt32(secure.RC(ParentNode.ChildNodes[i].Value)));
                }

                //Update on Child Control Node (Gridview / Button and e.t.c )---
                SQL = this.Append_SubNode(ParentNode.ChildNodes[i], RoleId, SQL, ref menuIds);
                //--------------------------------------------------------------

            }

            return SQL;
        }

        #endregion

        #region Validation Function

        private Boolean validationForm()
        {
            String Submit_Action = this.HFAction.Value;
            Boolean Result = false;
            this.lblErr.Text = "";
            this.lblErr.Visible = false;
            this.divErrorMessage.Visible = false;

            String RoleID = this.TxtRoleCode.Text.Trim();
            String RoleName = this.TxtRoleName.Text.Trim();
            StringBuilder errMsg = new StringBuilder();

            //Perform Checking ON [ADD] --------------------------------
            if (Submit_Action == "add")
            {
                if (String.IsNullOrEmpty(RoleID))
                { errMsg.Append(Resources.resource.RoleCodeEmpty + "\\n"); }

                //Check if Role Is Unique ------------------------------
                wwdb db = new wwdb();
                String SQL = String.Empty;
                SQL = "SELECT TOP 1 1 FROM tbl_RoleControl WHERE RoleId = N'" + secure.RC(RoleID) + "'";
                db.OpenTable(SQL); db.Close();
                if (db.RecordCount() > 0)
                { errMsg.Append(Resources.resource.RoleCodeDuplicateError + "\\n"); }
                db.Close();
                //------------------------------------------------------
            }
            //-----------------------------------------------------------


            if (String.IsNullOrEmpty(RoleName))
            { errMsg.Append(Resources.resource.RoleNameEmpty + "\\n"); }


            if (!String.IsNullOrEmpty(errMsg.ToString().Trim()))
            {
                sqlString.displayAlert2(this, errMsg.ToString());
                lblErr.Text = errMsg.Replace("\\n", "<br />").ToString();
                lblErr.Visible = true;
                divErrorMessage.Visible = true;
                Result = false;
            }
            else { Result = true; }

            return Result;
        }

        #endregion

        #region Control Event

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Form/Setting/UACList.aspx");
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (this.validationForm())
            {

                //Update Role (Insert / Update) -----------------------
                Boolean Remove_Success = true;
                String Action_Val = this.HFAction.Value;
                if (this.Update_Role(Action_Val))
                {
                    //-------------------------------------------------

                    //Remove ALL Existing Access Records (For Edit Only)
                    if (Action_Val == "edit")
                    {
                        Remove_Success = this.Remove_ExistingAccess();
                    }
                    //--------------------------------------------------


                    if (Remove_Success)
                    {
                        //Add New Access --------------------------------
                        if (this.Add_NewAccess())
                        {
                            sqlString.displayAlert2(this, Resources.resource.UpdateSuccess, ReturnPage);
                        }
                        else
                        {
                            String ErrMsg = Resources.resource.unknownError;
                            sqlString.displayAlert2(this, ErrMsg.ToString());
                            lblErr.Text = ErrMsg; lblErr.Visible = true;
                        }
                        //--------------------------------------------------
                    }
                    else
                    {
                        String ErrMsg = Resources.resource.unknownError;
                        sqlString.displayAlert2(this, ErrMsg.ToString());
                        lblErr.Text = ErrMsg; lblErr.Visible = true;
                    }
                }
                else
                {
                    String ErrMsg = Resources.resource.unknownError;
                    sqlString.displayAlert2(this, ErrMsg.ToString());
                    lblErr.Text = ErrMsg; lblErr.Visible = true;
                }

            }
        }

        #endregion

        #region TreeNode Control

        protected void trAccessMenu_TreeNodePopulate(object sender, TreeNodeEventArgs e)
        {
            this.Generate_SubMenu(e.Node.Value.ToString(), e.Node);
        }

        protected void trAccessMenu_SelectedNodeChanged(object sender, EventArgs e)
        {

        }

        protected void rptFunctionalityAccessList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string id = DataBinder.Eval(e.Item.DataItem, "ID").ToString();
                string name = DataBinder.Eval(e.Item.DataItem, "Name").ToString();
                bool isAccessible = Convert.ToBoolean(Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "IsAccessible")));
                CheckBox chkFunctionalityAccess = (CheckBox)e.Item.FindControl("chkFunctionalityAccess");

                chkFunctionalityAccess.Text = name;
                chkFunctionalityAccess.Attributes.Add("value", id);
                chkFunctionalityAccess.Checked = isAccessible;
            }
        }
        #endregion

        #region CheckBox Checked

        //protected void ChkAll_CheckedChanged(object sender, EventArgs e)
        //{

        //    for (int i = 0; i < trAccessMenu.Nodes.Count; i++)
        //    {
        //        trAccessMenu.Nodes[i].Checked = this.ChkAll.Checked;

        //        this.Check_SubNode(trAccessMenu.Nodes[i], this.ChkAll.Checked);
        //    }

        //    if (this.ChkAll.Checked)
        //    {
        //        this.lblSelectAction.Text = Resources.resource.DeSelectAll;
        //    }
        //    else { this.lblSelectAction.Text = Resources.resource.SelectAll; }

        //}

        private void Check_SubNode(TreeNode ParentNode, Boolean CheckOption)
        {
            //Update on Children Pages ---------------------------------------------
            for (int i = 0; i < ParentNode.ChildNodes.Count; i++)
            {
                ParentNode.ChildNodes[i].Checked = CheckOption;

                //Update on Child Control Node (Gridview / Button and e.t.c )---
                this.Check_SubNode(ParentNode.ChildNodes[i], CheckOption);
                //--------------------------------------------------------------

            }
        }


        #endregion

        protected void btnSelectAll_Click(object sender, EventArgs e)
        {
            if (this.ChkAll.Checked)
            {
                ChkAll.Checked = false;
            }
            else
            {
                ChkAll.Checked = true;
            }

            //foreach (ListItem item in chkFunctionalityAccessList.Items)
            //{
            //    item.Selected = this.ChkAll.Checked;
            //}
            foreach (RepeaterItem item in rptFunctionalityAccessList.Items)
            {
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                {
                    var checkBox = (CheckBox)item.FindControl("chkFunctionalityAccess");
                    checkBox.Checked = this.ChkAll.Checked;
                }
            }

            for (int i = 0; i < trAccessMenu.Nodes.Count; i++)
            {
                trAccessMenu.Nodes[i].Checked = this.ChkAll.Checked;

                this.Check_SubNode(trAccessMenu.Nodes[i], this.ChkAll.Checked);
            }

            if (this.ChkAll.Checked)
            {
                this.btnSelectAll.Text = Resources.resource.DeSelectAll;
            }
            else { this.btnSelectAll.Text = Resources.resource.SelectAll; }
        }

        protected void trAccessMenu_TreeNodeCheckChanged(object sender, TreeNodeEventArgs e)
        {
            updatingTreeView = true;

            TreeNode node = e.Node;

            SelectChilds(node);
            SelectParents(node);
        }

        private void SelectChilds(TreeNode node)
        {
            if (node != null)
            {
                if (node.Checked == false)
                {
                    foreach (TreeNode childNode in node.ChildNodes)
                    {
                        childNode.Checked = node.Checked;
                        SelectChilds(childNode);
                    }
                }
            }
        }


        private void SelectParents(TreeNode node)
        {
            if (node.Parent != null)
            {
                Boolean bln = false;

                foreach (TreeNode nodes in node.Parent.ChildNodes)
                {
                    if (nodes.Checked == true)
                    {
                        bln = true;
                        break;
                    }
                }

                if (bln == false)
                {
                    node.Parent.Checked = false;
                }
                else
                {
                    node.Parent.Checked = true;
                }

                SelectParents(node.Parent);
            }
            else
            {
                return;
            }
        }


    }
}