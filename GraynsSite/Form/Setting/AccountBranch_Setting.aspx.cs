﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Model;
using Synergy.Util;
using Synergy.Helper;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;
using System.IO;

namespace WMElegance.Form.Setting
{
	public partial class AccountBranch_Setting : System.Web.UI.Page
	{
		LoginUserModel login_user = null;
		string branchImage;

		protected void Page_Load(object sender, EventArgs e)
		{
			login_user = (LoginUserModel)Session[KeyVal.loginsecure];

			if (!IsPostBack)
			{
				bindControl();
				sqlString.bindControl(gv, getExData(null, null, true));
				Session["branchImage"] = branchImage;
			}
			branchImage = (string)Session["branchImage"];
			//if (gv.Rows.Count > 0)
			//{
			//    ApplyPaging(-1);
			//}
		}

		private void bindControl()
		{
            string stateSql = string.Empty;

			sqlString.bindControl(ddlBranch, "SELECT ID AS '1' , BranchName AS '0' FROM tbl_Branch WHERE isDeleted = 0 AND Status='A' AND ParentId IS NULL Order By BranchName", "0", "1", true);

            //State
            stateSql = string.Format(@"
                            SELECT st.StateName as '0', st.StateID as '1'
                            FROM tbl_State st WITH (nolock)
                            WHERE st.isDeleted = 0
                            ORDER BY st.StateName
                        ");
            sqlString.bindControl(ddlState, stateSql, "0", "1", true);
		}

		#region GetData

		// Action[0], MemberID[1], Name[2], Ranking[3]*, Date[4], Status[5]
		protected string getExData(string columnName, string sortOrder, bool hasOrder)
		{
			StringBuilder sql = new StringBuilder();
			sql.Clear();
            sql.AppendFormat(@" 
                SELECT * FROM ( 
	                SELECT a.ID,CAST(a.BranchID AS INT) AS 'BranchID', a.BranchName AS 'BranchName', a.ParentId AS 'ParentId', 
	                ISNULL(b.BranchName,'-') AS 'ParentBranchName', ISNULL(a.ImageURL,'-') AS 'ImageURL',
	                (SELECT '['+ STUFF(
		                (SELECT ', ' + '{{ ""ID"" :""' + CAST(ID AS VARCHAR) + '"", ""Name"" :""' + BranchName + '""}}' 
		                FROM tbl_Branch WITH(NOLOCK) 
		                WHERE  isdeleted = 0 AND ParentId IS NULL 
		                FOR XML PATH(''), TYPE).value('(./text())[1]', 'VARCHAR(MAX)') , 1, 2, ''
		                ) + ']'
	                ) AS BranchList,
	                s.StateName as 'State'
	                FROM tbl_Branch a WITH (NOLOCK) LEFT JOIN 
	                dbo.tbl_Branch b WITH (NOLOCK) ON a.ParentId  = b.ID left join
	                tbl_State s with (nolock) on a.StateID = s.StateID
	                WHERE a.status = 'A' AND a.isDeleted = 'False' {0}
                ) AS A
                ORDER BY A.[BranchID] ASC
            ", !string.IsNullOrEmpty(txtSearchBranchName.Text) ?
                string.Format(@"AND (b.BranchName LIKE '%{0}%') ", txtSearchBranchName.Text) : "");

            string tempsql = @"
                    (SELECT '[' + 
                        STUFF(
                            (SELECT ', ' + '{ ""ID"" :""' + CAST(ID AS VARCHAR) + '"", ""Name"" :""' + BranchName + '""}'
                            FROM tbl_Branch WITH(NOLOCK)
                            WHERE isdeleted = 0 AND ParentId IS NULL
                            FOR XML PATH('')
                        , TYPE).value('(./text())[1]', 'VARCHAR(MAX)') , 1, 2, '') 
                    +']')
                    AS BranchList";
            sql.AppendFormat(@" 
                SELECT * FROM ( 
			        SELECT a.ID, CAST(a.BranchID AS INT) AS 'BranchID', 
			        a.BranchName AS 'BranchName', 
			        a.ParentId AS 'ParentId', 
			            ISNULL(b.BranchName,'-') 
                    AS 'ParentBranchName',
			            ISNULL(a.ImageURL,'-') 
                    AS 'ImageURL',
			            {1}
			        FROM tbl_Branch a WITH (NOLOCK) LEFT JOIN dbo.tbl_Branch b WITH (NOLOCK) ON a.ParentId  = b.ID 
			        WHERE a.status = 'A' AND a.isDeleted = 'False' {0}) 
                AS A 
                ORDER BY A.[BranchID] ASC
            ", txtSearchBranchName.Text != string.Empty ?
                string.Format("AND (b.BranchName LIKE '%{0}%')", txtSearchBranchName.Text) : ""
            , tempsql);

			LogUtil.logSQL(sql.ToString());
			return sql.ToString();
		}

		#endregion

		#region GridView - GV

		private SortDirection GridViewSortDirection1
		{
			get
			{
				if (ViewState["sortDirection"] == null)
					ViewState["sortDirection"] = SortDirection.Ascending;

				return (SortDirection)ViewState["sortDirection"];
			}
			set { ViewState["sortDirection"] = value; }
		}

		protected void gv_DataBound(object sender, EventArgs e)
		{
			if (gv.Rows.Count > 0)
			{
				gv.HeaderRow.Cells[1].Visible = false;
				gv.FooterRow.Cells[1].Visible = false;

				for (int a = 0; a < gv.Rows.Count; a++)
				{
					gv.Rows[a].Cells[1].Visible = false;

					Button btn = (Button)gv.Rows[a].FindControl("btnActive");
					Button btn2 = (Button)gv.Rows[a].FindControl("btnDeactive");

					if (gv.Rows[a].Cells[4].Text == "Active")
					{
						btn.Visible = false;
						btn2.Visible = true;
					}
					else
					{
						btn.Visible = true;
						btn2.Visible = false;
					}


					TableCell cell = gv.Rows[a].Cells[0];
					gv.Rows[a].Cells.Remove(cell);
					gv.Rows[a].Cells.Add(cell);
				}

				TableCell headerCell = gv.HeaderRow.Cells[0];
				gv.HeaderRow.Cells.Remove(headerCell);
				gv.HeaderRow.Cells.Add(headerCell);

				TableCell footerCell = gv.FooterRow.Cells[0];
				gv.FooterRow.Cells.Remove(footerCell);
				gv.FooterRow.Cells.Add(footerCell);
			}
		}

		protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				string imageurl = DataBinder.Eval(e.Row.DataItem, "ImageURL").ToString();
				Image gv_img_fu1 = (Image)e.Row.FindControl("gv_img_fu1");
				Image imgDisplay = gv_img_fu1;
				if (imageurl != "-")
				{
					imgDisplay.Visible = true;
					imgDisplay.Attributes.Add("onclick", "window.open('" + imageurl + "')");
				}
			}
		}

		protected void gv_Sorting(object sender, GridViewSortEventArgs e)
		{
			string direction = "ASC";

			if (GridViewSortDirection1 == SortDirection.Ascending)
			{
				GridViewSortDirection1 = SortDirection.Descending;
				direction = "DESC";
			}
			else
			{
				GridViewSortDirection1 = SortDirection.Ascending;
				direction = "ASC";
			}

			ViewState["Field1"] = e.SortExpression; ViewState["Direction1"] = direction;

			sqlString.bindControl(gv, getExData(e.SortExpression, direction, true));
		}

		#endregion

		#region Control Event

		protected void btnSearch_Click(object sender, EventArgs e)
		{
			sqlString.bindControl(gv, getExData(null, null, true));
		}

		protected void btnExport_Click(object sender, EventArgs e)
		{
			sqlString.exportFunc("City List", getExData(null, null, false));
		}

		protected void btnReset_Click(object sender, EventArgs e)
		{
			txtSearchBranchName.Text = "";
			sqlString.bindControl(gv, getExData(null, null, true));
		}

		protected void btnUpdate_Click(object sender, EventArgs e)
		{
			//Button btn = (Button)sender;
			//TextBox txt = (TextBox)((GridViewRow)btn.NamingContainer).Cells[1].FindControl("txtBranchName");
			//GridViewRow gvr = (GridViewRow)btn.NamingContainer;
			//DropDownList ddlgvBranch = (DropDownList)gvr.FindControl("ddlgvBranch");
			//string id = ((GridViewRow)btn.NamingContainer).Cells[0].Text.ToString().Trim();
			//wwdb db = new wwdb();
			//StringBuilder sql = new StringBuilder();
			//int? branchid = null;

			//if (int.Parse(ddlgvBranch.SelectedItem.Value) > 0)
			//{
			//	var res = secure.RC(ddlgvBranch.SelectedItem.Value.ToString());
			//	if (res != string.Empty)
			//		branchid = Convert.ToInt32(res);
			//}

			//try
			//{
			//	sql.Clear();
			//	if (txt.Text.Trim() != String.Empty)
			//	{

			//		sql.Append(" UPDATE tbl_Branch SET BranchName=N'" + secure.RC(txt.Text.ToString()) + "',updatedBy = N'" + secure.RC(login_user.UserId) + "', UpdatedAt = GETDATE()");
			//		if (branchid > 0)
			//		{
			//			sql.Append(", ParentId=" + branchid);
			//		}
			//		else
			//		{
			//			sql.Append(", ParentId= @" + DBNull.Value);

			//		}
			//		sql.Append(" WHERE BranchID = N'" + secure.RC(id) + "'; ");
			//		db.Execute(sql.ToString());

			//		LogUtil.logAction(sql.ToString(), "Update Account Branch");

			//		sqlString.displayAlert(this, "Action_Successful");
			//		sqlString.bindControl(gv, getExData(null, null, true));
			//	}
			//	else
			//	{
			//		sqlString.displayAlert(this, "Textbox cannot be empty");
			//		sqlString.bindControl(gv, getExData(null, null, true));
			//	}
			//}

			//catch (Exception ex) { LogUtil.logError(ex.ToString(), sql.ToString()); sqlString.displayAlert(this, KeyVal.UnknownError); }
			//finally { db.Close(); }

			Button btn = (Button)sender;
			int idIndex = gridView.GetColumnIndexByName(gv, Resources.resource.ID);
			string id = ((GridViewRow)btn.NamingContainer).Cells[idIndex].Text.ToString().Trim();
			Response.Redirect("/Form/Setting/EditAccountBranch_Setting.aspx?bid=" + sqlString.encryptURL(id));
		}
		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			if (validateForm())
			{
				wwdb db = new wwdb();
				StringBuilder sql = new StringBuilder();
				int newID;
				int? branchid = null;

				try
				{
					sql.Clear();
					sql.Append("Select TOP 1 CAST(BranchID AS INT) AS BranchID from tbl_Branch WITH (NOLOCK) ORDER BY BranchID desc");
					db.OpenTable(sql.ToString());
					if (db.RecordCount() > 0)
					{
						newID = ConvertHelper.ConvertToInt(db.Item("BranchID").ToString(), 0) + 1;
					}
					else
					{
						newID = 1;
					}
					if (int.Parse(ddlBranch.SelectedItem.Value) > 0)
					{
						var res = secure.RC(ddlBranch.SelectedItem.Value.ToString());
						if (res != string.Empty)
							branchid = Convert.ToInt32(res);
					}

					sql.Clear();
					sql.Append(" Insert INTO tbl_Branch (BranchID, BranchName, Status, IsDeleted, CreatedBy, CreatedAt, UpdatedAt, UpdatedBy ");
					if (branchid != null)
					{
						sql.Append(", ParentId");
					}
					if ((string)Session["branchImage"] != "" && (string)Session["branchImage"] != null)
					{
						sql.Append(", ImageURL");
					}
                    if (ddlState.SelectedIndex > 0)
                    {
                        sql.Append(", StateID");
                    }
					sql.Append(")");
					sql.Append(" VALUES ('" + secure.RC(newID.ToString()) + "','" + secure.RC(txtNewBranch.Text) + "','A','false','" + secure.RC(login_user.UserId) + "', GETDate(), GetDate(), '" + secure.RC(login_user.UserId) + "'");
					if (branchid != null)
					{
						sql.Append("," + branchid);
					}
					if ((string)Session["branchImage"] != "" && (string)Session["branchImage"] != null)
					{
						string newPath = "/Upload/BranchImage/";
						if (!Directory.Exists(Server.MapPath("~" + newPath)))
							Directory.CreateDirectory(Server.MapPath("~" + newPath));
						FileInfo FromFile = new FileInfo((string)Session["branchImage"]);
						string toFilePath = newPath + FromFile.Name;
						FromFile.MoveTo(Server.MapPath("~" + toFilePath));
						sql.Append(",'" + toFilePath + "'");
					}
                    if (ddlState.SelectedIndex > 0)
                    {
                        sql.Append(", '" + ddlState.SelectedValue + "'");
                    }
					sql.Append(");");
					//sql.Append(" INSERT INTO tbl_Area (Area_Name, State_RowID, isActive, CreatedBy,UpdatedBy,UpdatedAt) VALUES " +
					//    " (N'" + secure.RCL(txtNewBank.Text) + "', N'" + secure.RC(ddlState.SelectedValue) + "', 1, N'" + secure.RC(login_user.UserId) + "', N'" + secure.RC(login_user.UserId) + "',GetDate());");

					db.Execute(sql.ToString());

					sqlString.displayAlert(this, "Action_Successful");

					txtNewBranch.Text = "";
					ddlBranch.SelectedIndex = 0;

				}
				catch (Exception ex)
				{ LogUtil.logError(ex.ToString(), sql.ToString()); sqlString.displayAlert(this, KeyVal.UnknownError); }
				finally
				{
					db.Close();
					sqlString.bindControl(gv, getExData(null, null, true));
				}
			}
		}

		protected void btnDelete_Click(object sender, EventArgs e)
		{
			Button btn = (Button)sender;
			string id = ((GridViewRow)btn.NamingContainer).Cells[0].Text.ToString().Trim();
			wwdb db = new wwdb();
			string sql = "";

			try
			{

				//terminal
				//sql = " UPDATE tbl_login SET login_status = '0' WHERE login_id = (SELECT memberId FROM tbl_memberInfo WITH (NOLOCK) WHERE memberid = N'" + secure.RC(id) + "'); " +
				//    " UPDATE tbl_memberInfo SET status = 'D', updatedBy = N'" + secure.RC(login_user.UserId) + "' " +
				//    " , UpdatedAt = GETDATE() WHERE memberid = N'" + secure.RC(id) + "'; ";
				sql = " UPDATE tbl_Branch SET status = 'T', IsDeleted='True',updatedBy = N'" + secure.RC(login_user.UserId) + "' " +
						" , UpdatedAt = GETDATE() WHERE BranchID = N'" + secure.RC(id) + "'; ";
				db.Execute(sql);
				LogUtil.logAction(sql, "Delete Account Branch");

				sqlString.displayAlert(this, "Action_Successful");
				sqlString.bindControl(gv, getExData(null, null, true));
			}

			catch (Exception ex) { LogUtil.logError(ex.ToString(), sql); sqlString.displayAlert(this, KeyVal.UnknownError); }
			finally { db.Close(); }
		}
		protected bool validateForm()
		{
			bool result = false;
			StringBuilder errMsg = new StringBuilder();
			StringBuilder sql = new StringBuilder();
			wwdb db = new wwdb();

			try
			{
				if (String.IsNullOrEmpty(txtNewBranch.Text.Trim()))
				{
					errMsg.Append(Resources.resource.Account_Branch_Note + "\\n");
				}
			}
			catch (Exception ex) { LogUtil.logError(ex.ToString(), sql.ToString()); errMsg.Append(sqlString.changeLanguage(KeyVal.UnknownError) + "\\n"); }
			finally { db.Close(); }

			if (errMsg.ToString().Trim() != string.Empty)
			{
				sqlString.displayAlert2(this, errMsg.ToString());
				lblErr.Text = errMsg.Replace("\\n", "<br />").ToString();
				lblErr.Visible = true;
				divErrorMessage.Visible = true;
				lblErr.Focus();
			}
			else
			{
				lblErr.Text = "";
				lblErr.Visible = false;
				divErrorMessage.Visible = false;
				result = true;
			}

			return result;
		}

		#region GridView Control

		protected void btnView_Click(object sender, EventArgs e)
		{
			Button btn = (Button)sender;
			//int row = Convert.ToInt32(btn.CommandArgument);
			Response.Redirect("/Form/Setting/AddCity.aspx?mid=" + sqlString.encryptURL(((GridViewRow)btn.NamingContainer).Cells[0].Text));
		}

		#endregion

		#endregion

		protected void btnActive_Click(object sender, EventArgs e)
		{
			wwdb db = new wwdb();
			StringBuilder sql = new StringBuilder();

			try
			{
				Button btn = (Button)sender;

				sql.Clear();
				sql.Append(" UPDATE tbl_Area SET IsActive = 1, isDeleted = 0 WHERE id = '" + secure.RC(((GridViewRow)btn.NamingContainer).Cells[0].Text) + "'; ");
				db.Execute(sql.ToString());

				sqlString.displayAlert(this, KeyVal.ActionSuccess);
				sqlString.bindControl(gv, getExData(null, null, true));

				LogUtil.logAction(sql.ToString(), "Activate Country");

				sqlString.bindControl(gv, getExData(null, null, true));
			}
			catch (Exception ex) { LogUtil.logError(ex.ToString(), sql.ToString()); sqlString.displayAlert(this, KeyVal.UnknownError); }
			finally { db.Close(); }
		}

		protected void btnDeactive_Click(object sender, EventArgs e)
		{
			wwdb db = new wwdb();
			StringBuilder sql = new StringBuilder();

			try
			{
				Button btn = (Button)sender;

				sql.Clear();
				sql.Append(" UPDATE tbl_Area SET IsActive = 0 WHERE id = '" + secure.RC(((GridViewRow)btn.NamingContainer).Cells[0].Text) + "'; ");
				db.Execute(sql.ToString());

				sqlString.displayAlert(this, KeyVal.ActionSuccess);
				sqlString.bindControl(gv, getExData(null, null, true));

				LogUtil.logAction(sql.ToString(), "Deactivate Country");

				//sqlString.bindControl(gv,getExData(null,null,true));
			}
			catch (Exception ex) { LogUtil.logError(ex.ToString(), sql.ToString()); sqlString.displayAlert(this, KeyVal.UnknownError); }
			finally { db.Close(); }
		}

		private void ApplyPaging(int pageindex)
		{
			if (pageindex > 0)
			{
				gv.PageIndex = pageindex;
			}
			GridViewRow row = gv.BottomPagerRow;
			PlaceHolder phPaging;
			LinkButton lnkPaging;

			LinkButton lnkPrevPage;
			LinkButton lnkNextPage;

			phPaging = (PlaceHolder)row.FindControl("phPaging");
			//lnkPrevPage = new LinkButton();
			//lnkPrevPage.Text = Server.HtmlEncode("← Previous");
			//lnkPrevPage.CommandName = "Page";
			//lnkPrevPage.CommandArgument = "prev";
			//lnkPrevPage.CssClass = "prev";

			lnkPrevPage = new LinkButton();
			lnkPrevPage.Text = Server.HtmlEncode(Resources.resource.First);
			lnkPrevPage.CommandName = "Page";
			lnkPrevPage.CommandArgument = "First";
			lnkPrevPage.CssClass = "prev";

			// ltrPaging.Controls.Add(lnkFirstPage);
			if (gv.PageIndex == 0)
			{
				lnkPrevPage.Enabled = false;


			}
			phPaging.Controls.Add(lnkPrevPage);
			int startpage = 1;
			int endpage = gv.PageCount;
			if (gv.PageIndex - 3 < 0 && startpage + 3 < gv.PageCount)
			{
				startpage = 1;
				endpage = startpage + 4;
			}
			else if (gv.PageIndex - 2 > 0 && gv.PageIndex + 2 < gv.PageCount)
			{
				startpage = gv.PageIndex - 2;
				endpage = gv.PageIndex + 2;
				startpage += 1;
				endpage += 1;
			}
			else if (gv.PageIndex - 2 > 0 && gv.PageIndex + 2 >= gv.PageCount)
			{
				startpage = gv.PageCount - 4;
				endpage = gv.PageCount;
			}



			for (int i = startpage; i <= endpage; i++)
			{
				if (i > 0)
				{
					lnkPaging = new LinkButton();

					lnkPaging.Text = i.ToString();
					lnkPaging.CommandName = "Page";
					lnkPaging.CommandArgument = i.ToString();
					if (i == gv.PageIndex + 1)
					{
						lnkPaging.BackColor = System.Drawing.Color.FromArgb(66, 139, 202);
						lnkPaging.Enabled = false; lnkPaging.Attributes.Add("style", "font-weight: bold;color: black;");
					}
					phPaging = (PlaceHolder)row.FindControl("phPaging");
					phPaging.Controls.Add(lnkPaging);
				}
			}

			//lnkNextPage = new LinkButton();
			//lnkNextPage.Text = Server.HtmlEncode("Next → ");
			//lnkNextPage.CommandName = "Page";
			//lnkNextPage.CommandArgument = "next";
			//lnkNextPage.CssClass = "next";

			lnkNextPage = new LinkButton();
			lnkNextPage.Text = Server.HtmlEncode(Resources.resource.Last);
			lnkNextPage.CommandName = "Page";
			lnkNextPage.CommandArgument = "Last";
			lnkNextPage.CssClass = "next";

			phPaging = (PlaceHolder)row.FindControl("phPaging");
			phPaging.Controls.Add(lnkNextPage);
			if (gv.PageIndex == gv.PageCount - 1)
			{
				lnkNextPage.Enabled = false;
			}

		}

		protected void btnUpload_Click(object sender, EventArgs e)
		{
			FileUpload fupload = fu1;

			if (fupload != null && fupload.HasFile)
			{
				string path = "/Upload/Temp/BranchImage/";
				string physicalPath = Server.MapPath("~" + path);
				FileInfo Finfo = new FileInfo(fupload.PostedFile.FileName);
				string filename = DateTime.Now.ToString("ddMMyyyyhhmmssfff") + "_" + login_user.UserId + Finfo.Extension;
				string fullPhysical = physicalPath + filename;
				if (!Directory.Exists(physicalPath))
					Directory.CreateDirectory(physicalPath);

				if (File.Exists(physicalPath))
				{
					File.Delete(fullPhysical);
				}

				fupload.SaveAs(fullPhysical);
				branchImage = fullPhysical;
				Session["branchImage"] = branchImage;
				Image imgDisplay = img_fu1;
				imgDisplay.Visible = true;
				imgDisplay.Attributes.Add("onclick", "window.open('" + path + filename + "')");
			}

		}
	}
}