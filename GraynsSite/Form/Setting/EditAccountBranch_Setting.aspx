﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Global1.Master" AutoEventWireup="true" CodeBehind="EditAccountBranch_Setting.aspx.cs" Inherits="WMElegance.Form.Setting.EditAccountBranch_Setting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<script>
		function FileUpload(fuid) {
			$('#<%= hid_FUID.ClientID %>').val(fuid);
			$('#<%= btnUpload.ClientID %>').click();
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

	<aside class="right-side">

		<section class="content-header">
			<h1>
				<asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><%= GetGlobalResourceObject("resource", "AccountBranchSetting")%></asp:Label>
			</h1>

			<asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
		</section>
		<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div id="divErrorMessage" runat="server" visible="false" class="alert alert-danger alert-dismissable">
						<i class="fa fa-ban"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<b>Alert!</b>
						<br />
						<asp:Label runat="server" ID="lblErr"></asp:Label>
					</div>
					<div class="box">
                        <asp:Panel runat="server" DefaultButton="btnSubmit">
						    <table class="table table-bordered">
							    <tbody>
								    <tr style="background: #dec18c;">
									    <td colspan="2">
										    <h4><strong><%= GetGlobalResourceObject("resource", "Edit_Account_Branch")%></strong></h4>
									    </td>
								    </tr>
								    <tr class="form-group" runat="server" id="trBranchID" visible="false">
                                            <td style="width:20%; border-left: 0px!important;">
                                                <label class="control-label"><%= GetGlobalResourceObject("resource", "Branch_ID")%>&nbsp;<span style="color: Red;">*</span></label>
                                            </td>
                                            <td style="width:80%;" class="controls">
                                                <asp:Label ID="lblBranchID" runat="server" CssClass="form-control"></asp:Label>
                                            </td>
                                        </tr>

								    <tr class="form-group" runat="server" id="trRegisterCountry">
									    <td style="width: 35%; border-left: 0px!important;">
										    <label class="control-label"><%= GetGlobalResourceObject("resource", "Branch_Name")%></label>
									    </td>
									    <td style="width: 50%;" class="controls">
										    <asp:TextBox ID="txtNewBranch" CssClass="form-control" runat="server" placeholder="<%$ Resources:Resource, Edit_Account_Branch%>"></asp:TextBox>
									    </td>
								    </tr>
								    <tr class="form-group" runat="server">
									    <td style="width: 35%; border-left: 0px!important;">
										    <label class="control-label"><%= GetGlobalResourceObject("resource", "State")%></label>
									    </td>
									    <td style="width: 50%;" class="controls">
										    <asp:DropDownList runat="server" ID="ddlState" CssClass="form-control" />
									    </td>
								    </tr>
								    <tr class="form-group" runat="server" id="tr1">
									    <td style="width: 35%; border-left: 0px!important;">
										    <label class="control-label"><%= GetGlobalResourceObject("resource", "Parent_Account_Branch")%></label>
									    </td>
									    <td style="width: 50%;" class="controls">
										    <asp:DropDownList runat="server" ID="ddlBranch" CssClass="form-control">
										    </asp:DropDownList>
									    </td>
								    </tr>
								    <tr class="form-group" runat="server" id="tr2">
									    <td style="width: 35%; border-left: 0px!important;">
										    <asp:FileUpload runat="server" ID="fu1" onchange="FileUpload('fu1')" />
									    </td>
									    <td>
										    <asp:Image runat="server" ID="img_fu1" ImageUrl="/Images/Doc_Icon.png" Width="50px" Visible="false" />
										    <asp:Button runat="server" ID="btnUpload" Style="display: none;" OnClick="btnUpload_Click" />
										    <asp:HiddenField runat="server" ID="hid_FUID" />
									    </td>
								    </tr>
							    </tbody>
						    </table>
						    <div class="box-footer">
							    <asp:Button runat="server" ID="btnSubmit" Visible="true" CssClass="btn btn-edit" OnClick="btnSubmit_Click" Text="<%$ Resources:Resource, Update%>" OnClientClick="return confirm('Comfirm to update record ?')" />
						    </div>
                        </asp:Panel>
					</div>
				</div>
			</div>
		</section>
	</aside>

</asp:Content>
