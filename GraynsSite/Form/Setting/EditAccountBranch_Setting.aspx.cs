﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Model;
using Synergy.Util;
using Synergy.Helper;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;
using System.IO;

namespace WMElegance.Form.Setting
{
	public partial class EditAccountBranch_Setting : System.Web.UI.Page
	{
		LoginUserModel login_user = null;
		string branchImage;

		protected void Page_Load(object sender, EventArgs e)
		{
			login_user = (LoginUserModel)Session[KeyVal.loginsecure];

			if (!IsPostBack)
			{
				int bid = 0;
				if (Request["bid"] != null)
				{
					Session["branchImage"] = branchImage;
					bindControl();
					bid = ConvertHelper.ConvertToInt32(sqlString.decryptURL(Request["bid"].ToString()).Trim(), 0);
					GetBranchInformation();
				}
			}
			branchImage = (string)Session["branchImage"];
			//if (gv.Rows.Count > 0)
			//{
			//    ApplyPaging(-1);
			//}
		}

		private void bindControl()
		{
            string stateSql = string.Empty;

			sqlString.bindControl(ddlBranch, "SELECT ID AS '1' , BranchName AS '0' FROM tbl_Branch WHERE isDeleted = 0 AND Status='A' AND ParentId IS NULL Order By BranchName", "0", "1", true);

            //State
            stateSql = string.Format(@"
                            SELECT st.StateName as '0', st.StateID as '1'
                            FROM tbl_State st WITH (nolock)
                            WHERE st.isDeleted = 0
                            ORDER BY st.StateName
                        ");
            sqlString.bindControl(ddlState, stateSql, "0", "1", true);
        }

		protected void GetBranchInformation()
		{

			wwdb db = new wwdb();
			StringBuilder sql = new StringBuilder();
			if (Request["bid"] != null)
			{
				int bid = ConvertHelper.ConvertToInt32(sqlString.decryptURL(Request["bid"].ToString()).Trim(), 0);
				try
				{
					sql.Clear();
                    sql.AppendFormat(@"
                        SELECT BranchID, StateID, BranchName, ImageURL, ParentId
                        FROM tbl_Branch WITH (NOLOCK) 
                        WHERE Status = 'A' And IsDeleted = 'False' AND BranchID = N'{0}';
                    ", bid.ToString());
					db.OpenTable(sql.ToString());
					if (!db.HasError && db.RecordCount() > 0)
					{
						lblBranchID.Text = db.Item("BranchID");
						txtNewBranch.Text = db.Item("BranchName");
                        if (db.Item("StateID") != null)
                            ddlState.SelectedValue = db.Item("StateID");
						if (db.Item("ParentId") != null)
							ddlBranch.SelectedValue = db.Item("ParentId");
						if (db.Item("ImageURL") != null && db.Item("ImageURL") != string.Empty)
						{
							Image imgDisplay = img_fu1;
							imgDisplay.Visible = true;
							imgDisplay.Attributes.Add("onclick", "window.open('" + db.Item("ImageURL") + "')");
						}
						Session["branchImage"] = db.Item("ImageURL");
					}
				}
				catch (Exception ex){ LogUtil.logError(ex.ToString(), sql.ToString()); }
				finally { db.Close(); }
			}
		}

		#region GetData

		// Action[0], MemberID[1], Name[2], Ranking[3]*, Date[4], Status[5]
		protected string getExData(string columnName, string sortOrder, bool hasOrder)
		{
            StringBuilder sql = new StringBuilder();
            sql.Clear();
            sql.AppendFormat(@" 
                SELECT * FROM ( 
	                SELECT a.ID,CAST(a.BranchID AS INT) AS 'BranchID', a.BranchName AS 'BranchName', a.ParentId AS 'ParentId', 
	                ISNULL(b.BranchName,'-') AS 'ParentBranchName', ISNULL(a.ImageURL,'-') AS 'ImageURL',
	                (SELECT '['+ STUFF(
		                (SELECT ', ' + '{{ ""ID"" :""' + CAST(ID AS VARCHAR) + '"", ""Name"" :""' + BranchName + '""}}' 
		                FROM tbl_Branch WITH(NOLOCK) 
		                WHERE  isdeleted = 0 AND ParentId IS NULL 
		                FOR XML PATH(''), TYPE).value('(./text())[1]', 'VARCHAR(MAX)') , 1, 2, ''
		                ) + ']'
	                ) AS BranchList,
	                s.StateName as 'State'
	                FROM tbl_Branch a WITH (NOLOCK) LEFT JOIN 
	                dbo.tbl_Branch b WITH (NOLOCK) ON a.ParentId  = b.ID left join
	                tbl_State s with (nolock) on a.StateID = s.StateID
	                WHERE a.status = 'A' AND a.isDeleted = 'False'
                ) AS A
                ORDER BY A.[BranchID] ASC
            ");

            LogUtil.logSQL(sql.ToString());
            return sql.ToString();
        }

		#endregion

		#region Control Event
		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			if (validateForm())
			{
				wwdb db = new wwdb();
				int? branchid = null;
				string toFilePath = string.Empty;
				int id = Convert.ToInt32(lblBranchID.Text.Trim());

				if (int.Parse(ddlBranch.SelectedItem.Value) > 0)
				{
					var res = secure.RC(ddlBranch.SelectedItem.Value.ToString());
					if (res != string.Empty)
						branchid = Convert.ToInt32(res);
				}

				if ((string)Session["branchImage"] != "" && (string)Session["branchImage"] != null && Session["branchImage"].ToString().Contains("Temp"))
				{
					string newPath = "/Upload/BranchImage/";
					if (!Directory.Exists(Server.MapPath("~" + newPath)))
						Directory.CreateDirectory(Server.MapPath("~" + newPath));
					FileInfo FromFile = new FileInfo((string)Session["branchImage"]);
					toFilePath = newPath + FromFile.Name;
					FromFile.MoveTo(Server.MapPath("~" + toFilePath));
				}
				else
				{
					toFilePath = Session["branchImage"].ToString();
				}
				try
				{
					if (txtNewBranch.Text.Trim() != String.Empty)
					{
						SqlCommand sqlCommand = new SqlCommand();
                        sqlCommand.CommandText = string.Format(@"
                            UPDATE tbl_Branch SET BranchName = @branchname, updatedBy = @updatedby, 
                                updatedAt = GETDATE(), ParentId = @parentid, ImageURL = @imageurl,
                                stateID = @stateid
                            WHERE BranchId = @branchId
                        ");

						sqlCommand.Parameters.AddWithValue("@branchname", secure.RC(txtNewBranch.Text.ToString()));
						sqlCommand.Parameters.AddWithValue("@updatedby", secure.RC(login_user.UserId));
						sqlCommand.Parameters.AddWithValue("@parentid", branchid == null ? (object)DBNull.Value : branchid);
						sqlCommand.Parameters.AddWithValue("@imageurl", toFilePath == string.Empty ? (object)DBNull.Value : toFilePath);
						sqlCommand.Parameters.AddWithValue("@branchId", lblBranchID.Text.Trim());
                        sqlCommand.Parameters.AddWithValue("@stateid", ddlState.SelectedValue);

                        db.Execute(sqlCommand, true, true);

						LogUtil.logAction(lblBranchID.Text.Trim(), "Update Account Branch");

                        sqlString.displayAlert(this, Resources.resource.ActionSuccessfulMsg, "/Form/Setting/AccountBranch_Setting.aspx");
						//Response.Redirect("/Form/Setting/AccountBranch_Setting.aspx");
					}
					else
					{
						sqlString.displayAlert(this, "Textbox cannot be empty");
					}
				}

				catch (Exception ex) { LogUtil.logError(ex.ToString(), ""); sqlString.displayAlert(this, KeyVal.UnknownError); }
				finally { db.Close(); }
			}
		}

		protected bool validateForm()
		{
			bool result = false;
			StringBuilder errMsg = new StringBuilder();
			StringBuilder sql = new StringBuilder();
			wwdb db = new wwdb();

			try
			{
				if (String.IsNullOrEmpty(txtNewBranch.Text.Trim()))
				{
					errMsg.Append(Resources.resource.Account_Branch_Note + "\\n");
				}
			}
			catch (Exception ex) { LogUtil.logError(ex.ToString(), sql.ToString()); errMsg.Append(sqlString.changeLanguage(KeyVal.UnknownError) + "\\n"); }
			finally { db.Close(); }

			if (errMsg.ToString().Trim() != string.Empty)
			{
				sqlString.displayAlert2(this, errMsg.ToString());
				lblErr.Text = errMsg.Replace("\\n", "<br />").ToString();
				lblErr.Visible = true;
				divErrorMessage.Visible = true;
				lblErr.Focus();
			}
			else
			{
				lblErr.Text = "";
				lblErr.Visible = false;
				divErrorMessage.Visible = false;
				result = true;
			}

			return result;
		}

		#region GridView Control

		protected void btnView_Click(object sender, EventArgs e)
		{
			Button btn = (Button)sender;
			//int row = Convert.ToInt32(btn.CommandArgument);
			Response.Redirect("/Form/Setting/AddCity.aspx?mid=" + sqlString.encryptURL(((GridViewRow)btn.NamingContainer).Cells[0].Text));
		}

		#endregion

		#endregion

		protected void btnUpload_Click(object sender, EventArgs e)
		{
			FileUpload fupload = fu1;

			if (fupload != null && fupload.HasFile)
			{
				string path = "/Upload/Temp/BranchImage/";
				string physicalPath = Server.MapPath("~" + path);
				FileInfo Finfo = new FileInfo(fupload.PostedFile.FileName);
				string filename = DateTime.Now.ToString("ddMMyyyyhhmmssfff") + "_" + login_user.UserId + Finfo.Extension;
				string fullPhysical = physicalPath + filename;
				if (!Directory.Exists(physicalPath))
					Directory.CreateDirectory(physicalPath);

				if (File.Exists(physicalPath))
				{
					File.Delete(fullPhysical);
				}

				fupload.SaveAs(fullPhysical);
				branchImage = fullPhysical;
				Session["branchImage"] = branchImage;
				Image imgDisplay = img_fu1;
				imgDisplay.Visible = true;
				imgDisplay.Attributes.Add("onclick", "window.open('" + path + filename + "')");
			}

		}
	}
}