﻿using HJT.Cls.Controller;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Synergy.Model;
using Synergy.Util;

namespace WMElegance.Form.Setting
{
    public partial class AddFreeRequest : System.Web.UI.Page
    {
        LoginUserModel login_user = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];
            if (!IsPostBack)
            {
                bindControl();
                sqlString.bindControl(gv, GetGvDate());
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (validation())
            {
                wwdb db = new wwdb();
                //StringBuilder sql = new StringBuilder();
                //sql.Clear();
                //sql.Append("INSERT INTO tbl_UserTransactions(dbo.tbl_UserTransactions.MemberId,dbo.tbl_UserTransactions.Amount,dbo.tbl_UserTransactions.Status,dbo.tbl_UserTransactions.RequestType,dbo.tbl_UserTransactions.CreatedAt,dbo.tbl_UserTransactions.IsFreeRequest,dbo.tbl_UserTransactions.FreeRequestCount,dbo.tbl_UserTransactions.CreatedBy,dbo.tbl_UserTransactions.Remark)");
                //sql.Append("VALUES ('" + ddlFullname.SelectedItem.Value.ToString() + "',0,1,'Free Request',GETDATE(),0," + txtFreeCount.Text.Trim() + ",'" + login_user.UserId + "','" + txtRemark.Text.Trim() + "');");
                //sql.Append("IF (SELECT 1 FROM tbl_UserCreditBalance WHERE MemberID ='" + ddlFullname.SelectedItem.Value.ToString() + "') > 0 ");
                //sql.Append("UPDATE tbl_UserCreditBalance SET FreeRequestCount = ISNULL(FreeRequestCount,0) + (" + txtFreeCount.Text.Trim() + ") WHERE MemberId ='" + ddlFullname.SelectedItem.Value.ToString() + "';");
                //sql.Append("ELSE ");
                //sql.Append("INSERT INTO tbl_UserCreditBalance(MemberId,CreditUsed,CreatedAt,FreeRequestCount) VALUES('" + ddlFullname.SelectedItem.Value.ToString() + "',0,GETDATE()," + txtFreeCount.Text.Trim() + ");");
                //db.Execute(sql.ToString());

                SqlParameter[] billingSPParams = {
                     new SqlParameter("@MemberId", ddlFullname.SelectedItem.Value),
                      new SqlParameter("@Amount", secure.RC(txtFreeCount.Text.Trim())),
                       new SqlParameter("@RequestType", "Free Request"),
                        new SqlParameter("@CreatedBy", login_user.UserId),
                         new SqlParameter("@Remark", secure.RC(txtRemark.Text.Trim()))
                    };

                db.executeScalarSP("SP_ManualAddBillingTransaction", billingSPParams);

                clearControl();
            }
        }

        private bool validation()
        {
            bool result = false;
            StringBuilder errMsg = new StringBuilder();
            if (txtFreeCount.Text.Trim() == string.Empty)
                errMsg.Append(Resources.resource.FreeRequestCannotEmpty + "\\n");

            if (Convert.ToInt32(txtFreeCount.Text.Trim()) < 0)
            {
                wwdb db = new wwdb();
                DataTable dt = db.getDataTable("SELECT ISNULL(FreeRequestCount,0) AS FreeRequestCount FROM tbl_UserCreditBalance WHERE MemberId ='" + ddlFullname.SelectedItem.Value.ToString() + "'");
                string branchId = string.Empty;
                if (dt.Rows.Count > 0)
                {
                    int FreeCount = Convert.ToInt32(dt.Rows[0]["FreeRequestCount"].ToString());
                    if(!((FreeCount + Convert.ToInt32(txtFreeCount.Text.Trim())) >= 0))
                        errMsg.Append(Resources.resource.FreeRequestCountError + "\\n");
                }
            }

            if (errMsg.ToString().Trim() != string.Empty)
            {
                sqlString.displayAlert2(this, errMsg.ToString());
                return false;
            }
            else
            {
                result = true;
            }

            return result;
        }

        private void bindControl()
        {
            wwdb db = new wwdb();

            sqlString.bindControl(ddlFullname, bindDdlFullnameSQL(), "0", "1", true);

            ddlFullName_SelectedIndexChanged(null, null);
        }
        private string bindDdlFullnameSQL(string name = "")
        {
            StringBuilder sql = new StringBuilder();

            sql.Clear();
            sql.AppendFormat(@"
                SELECT mi.FullName + ' (' + mi.Username + ')' AS '0', 
                    mi.MemberId AS '1' 
                FROM tbl_MemberInfo mi with (nolock) 
                WHERE mi.Status = 'A' {0}
                Order By mi.FullName
            ", string.IsNullOrEmpty(name) ? "" : string.Format(@"and mi.Fullname LIKE '%{0}%'", name));

            return sql.ToString();
        }
        private string GetGvDate(string name = "")
        {
            StringBuilder sql = new StringBuilder();
            sql.Clear();
            sql.AppendFormat(@"
                SELECT tmi.Fullname AS Name, tut.FreeRequestCount AS FreeRequest, ISNULL(tmi2.Fullname, 'SYSTEM') AS CreatedBy, 
                    tut.CreatedAt, tut.Remark FROM dbo.tbl_UserTransactions tut INNER JOIN 
                tbl_MemberInfo tmi ON tut.MemberId = tmi.MemberId LEFT JOIN 
                tbl_MemberInfo tmi2 ON tut.CreatedBy = tmi2.MemberId
                WHERE tut.RequestType = 'Free Request'
                ORDER BY tut.CreatedAt DESC
            ");

            LogUtil.logSQL(sql.ToString());

            return sql.ToString();
        }

        private void bindMember(string memberId)
        {
            wwdb db = new wwdb();
            DataTable dt = db.getDataTable(
                string.Format(@"
                    SELECT tmi.MemberId, tmi.DisplayName, tmi.Username, tmi.Fullname, tmi.Mobile, tmi.Email, 
                        tl.BranchID, tl.login_password, tb.BranchName 
                    FROM dbo.tbl_MemberInfo tmi INNER JOIN dbo.tbl_Login tl ON tmi.MemberId = tl.login_id LEFT JOIN 
                    tbl_Branch tb ON tl.BranchID = tb.BranchId 
                    WHERE tmi.MemberId = '{0}'
                ", memberId));
            string branchId = string.Empty;
            if (dt.Rows.Count > 0)
            {
                txtBranchName.Text = dt.Rows[0]["BranchName"].ToString();
                txtContactNumber.Text = dt.Rows[0]["Mobile"].ToString();
            }

        }

        protected void ddlFullName_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindMember(ddlFullname.SelectedItem.Value.ToString());
        }

        private void clearControl()
        {
            txtBranchName.Text = string.Empty;
            txtContactNumber.Text = string.Empty;
            txtFreeCount.Text = string.Empty;
            txtRemark.Text = string.Empty;
            ddlFullname.SelectedIndex = 0;
            sqlString.bindControl(gv, GetGvDate());
        }

        protected void btnSearchName_Click(object sender, EventArgs e)
        {
            ReportController reportController = new ReportController();
            reportController.removeInvalidChar("", txtSearchName);

            sqlString.bindControl(ddlFullname, bindDdlFullnameSQL(txtSearchName.Text), "0", "1", true);
        }
    }
}