﻿using HJT.Cls.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Helper;
using Synergy.Model;
using Synergy.Util;

namespace HJT.Form.Setting
{
    public partial class LoanCodesSetting : System.Web.UI.Page
    {
        LoginUserModel login_user = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];
            if (!IsPostBack)
            {
                binddata();
            }
        }

        #region Loan Code
        #region GV LoanCodeList
        private string getLoanCodeList()
        {
            StringBuilder sql = new StringBuilder();

            sql.AppendFormat(@"
                SELECT ltx.ID, ltx.LoanID, LoanTypeCode, lt.[Name]
                FROM tbl_LoanTypesXML ltx with (nolock) inner join
                tbl_LoanTypes lt with (nolock) on ltx.LoanID = lt.LoanID
                WHERE ltx.isDeleted = 0 and lt.IsDeleted = 0
                order by lt.Name
            ");

            return sql.ToString();
        }
        private string getLoanCodeName()
        {
            StringBuilder sql = new StringBuilder();

            sql.Clear();
            sql.AppendFormat(@"
                select lt.LoanID as '1', lt.[Name] as '0'
                from tbl_LoanTypes lt with (nolock)
                where lt.IsDeleted = 0
                order by lt.[Name]
            ");

            return sql.ToString();
        }
        protected void gvLoanCode_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //Bind Minimum and Maximum
                DropDownList ddlLoanType = (DropDownList)e.Row.Cells[1].FindControl("ddlLoanType");
                TextBox txtLoanCode = (TextBox)e.Row.Cells[2].FindControl("txtLoanCode");

                sqlString.bindControl(ddlLoanType, getLoanCodeName(), "0", "1", true);
                ddlLoanType.SelectedValue = DataBinder.Eval(e.Row.DataItem, "LoanID").ToString();
                txtLoanCode.Text = DataBinder.Eval(e.Row.DataItem, "LoanTypeCode").ToString();
            }
        }
        #endregion

        private void binddata()
        {
            sqlString.bindControl(ddlLoanType, getLoanCodeName(), "0", "1", true);
            sqlString.bindControl(gvLoanCode, getLoanCodeList());
        }
        private bool validateLoanCodeRow(object sender)
        {
            bool result = false;
            DropDownList ddlLoanType;
            TextBox txtLoanCode;
            string ID;
            StringBuilder errMsg = new StringBuilder();
            ReportController rc = new ReportController();
            GridViewRow row = (GridViewRow)((Button)sender).NamingContainer;

            ID = row.Cells[0].Text;
            ddlLoanType = (DropDownList)row.Cells[1].FindControl("ddlLoanType");
            txtLoanCode = (TextBox)row.Cells[2].FindControl("txtLoanCode");
            rc.removeInvalidChar(txtLoanCode);

            if (ddlLoanType.SelectedIndex <= 0)
                errMsg.Append(Resources.resource.Please_Select_One + "\\n");
            if (string.IsNullOrEmpty(secure.RC(txtLoanCode.Text)) || string.IsNullOrWhiteSpace(secure.RC(txtLoanCode.Text)))
                errMsg.Append(Resources.resource.InvalidLoanCode + "\\n");

            if (errMsg.ToString().Trim() != string.Empty)
            {
                sqlString.displayAlert2(this, errMsg.ToString());
                lblErr.Text = errMsg.Replace("\\n", "<br />").ToString();
                lblErr.Visible = true;
                divErrorMessage.Visible = true;
                lblErr.Focus();
            }
            else
            {
                lblErr.Text = "";
                lblErr.Visible = false;
                divErrorMessage.Visible = false;
                result = true;
            }

            return result;
        }
        protected void btnSaveRow_Click(object sender, EventArgs e)
        {
            if (validateLoanCodeRow(sender))
            {
                Button btnSaveRow = (Button)sender;
                string ID = ((GridViewRow)btnSaveRow.NamingContainer).Cells[0].Text;
                DropDownList ddlLoanType = (DropDownList)((GridViewRow)btnSaveRow.NamingContainer).Cells[1].FindControl("ddlLoanType");
                TextBox txtLoanCode = (TextBox)((GridViewRow)btnSaveRow.NamingContainer).Cells[2].FindControl("txtLoanCode");
                string loanID = ddlLoanType.SelectedValue;
                string loanCode = txtLoanCode.Text;

                StringBuilder sql = new StringBuilder();
                wwdb db = new wwdb();

                sql.Clear();
                sql.AppendFormat(@"
                    UPDATE tbl_LoanTypesXML SET LoanID = '{0}', LoanTypeCode = '{1}', 
                        LoanTypeName = (select top 1 [Name] from tbl_LoanTypes lt with (nolock)
                                        inner join tbl_LoanTypesXML ltx with (nolock) on ltx.LoanID = lt.LoanID
                                        where lt.LoanID = '{0}')  
                    WHERE ID = '{2}';
                ", loanID
                , loanCode
                , ID);

                db.Execute(sql.ToString());
                LogUtil.logAction(sql.ToString(), "Updating LoanTypesXML. ID: " + ID);
                if (db.HasError)
                {
                    LogUtil.logError(db.ErrorMessage, sql.ToString());
                    sqlString.displayAlert(this, Resources.resource.unknownError);
                }
                else
                    sqlString.displayAlert(this, Resources.resource.ActionSuccessfulMsg, Request.Url.ToString());
            }
        }
        protected void btnDeleteRow_Click(object sender, EventArgs e)
        {
            string ID = ((GridViewRow)((Button)sender).NamingContainer).Cells[0].Text;
            StringBuilder sql = new StringBuilder();
            wwdb db = new wwdb();

            sql.Clear();
            sql.AppendFormat(@"
                UPDATE tbl_LoanTypesXML SET isDeleted = 1 WHERE ID = '{0}';
            ", ID);
            db.Execute(sql.ToString());
            LogUtil.logAction(sql.ToString(), "Deleting LoanTypes. ID: " + ID);
            if (db.HasError)
                LogUtil.logError(db.ErrorMessage, sql.ToString());
            else
                sqlString.displayAlert(this, Resources.resource.ActionSuccessfulMsg, Request.Url.ToString());
        }
        private bool validateLoanCode()
        {
            bool result = false;
            DropDownList ddlLoanType;
            TextBox txtLoanCode;
            string ID, loanCode;
            StringBuilder errMsg = new StringBuilder();
            ReportController rc = new ReportController();
            for (int i = 0; i < gvLoanCode.Rows.Count; i++)
            {
                ID = gvLoanCode.Rows[i].Cells[0].Text;
                ddlLoanType = (DropDownList)gvLoanCode.Rows[i].Cells[1].FindControl("ddlLoanType");
                txtLoanCode = (TextBox)gvLoanCode.Rows[i].Cells[2].FindControl("txtLoanCode");
                rc.removeInvalidChar(txtLoanCode);
                loanCode = secure.RC(txtLoanCode.Text);

                if (ddlLoanType != null)
                    if (ddlLoanType.SelectedIndex <= 0)
                        errMsg.Append("ID: " + ID + ". " + Resources.resource.Please_Select_One + "\\n");
                if (string.IsNullOrEmpty(loanCode) || string.IsNullOrWhiteSpace(loanCode))
                    errMsg.Append("ID: " + ID + ". " + Resources.resource.InvalidLoanCode + "\\n");
            }

            if (errMsg.ToString().Trim() != string.Empty)
            {
                sqlString.displayAlert2(this, errMsg.ToString());
                lblErr.Text = errMsg.Replace("\\n", "<br />").ToString();
                lblErr.Visible = true;
                divErrorMessage.Visible = true;
                lblErr.Focus();
            }
            else
            {
                lblErr.Text = "";
                lblErr.Visible = false;
                divErrorMessage.Visible = false;
                result = true;
            }

            return result;
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (validateLoanCode())
            {
                StringBuilder sql = new StringBuilder();
                wwdb db = new wwdb();

                sql.Clear();
                DropDownList ddlLoanType;
                TextBox txtLoanCode;
                string ID, loanID, loanCode;
                ReportController rc = new ReportController();
                for (int i = 0; i < gvLoanCode.Rows.Count; i++)
                {
                    ID = gvLoanCode.Rows[i].Cells[0].Text;
                    ddlLoanType = (DropDownList)gvLoanCode.Rows[i].Cells[1].FindControl("ddlLoanType");
                    txtLoanCode = (TextBox)gvLoanCode.Rows[i].Cells[2].FindControl("txtLoanCode");
                    rc.removeInvalidChar(txtLoanCode);
                    loanID = ddlLoanType.SelectedValue;
                    loanCode = secure.RC(txtLoanCode.Text);

                    sql.AppendFormat(@"
                        UPDATE tbl_LoanTypesXML SET LoanID = '{0}', LoanTypeCode = '{1}', 
                            LoanTypeName = (select top 1 [Name] from tbl_LoanTypes lt with (nolock)
                                            inner join tbl_LoanTypesXML ltx with (nolock) on ltx.LoanID = lt.LoanID
                                            where lt.LoanID = '{0}') 
                        WHERE ID = '{2}';
                    ", loanID
                    , loanCode
                    , ID);
                }
                db.Execute(sql.ToString());
                LogUtil.logAction(sql.ToString(), "Updating LoanTypesXML");
                if (db.HasError)
                {
                    LogUtil.logError(db.ErrorMessage, sql.ToString());
                    sqlString.displayAlert(this, Resources.resource.unknownError);
                }
                else
                    sqlString.displayAlert(this, Resources.resource.ActionSuccessfulMsg, Request.Url.ToString());
            }
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            sqlString.bindControl(gvLoanCode, getLoanCodeList());
        }
        private bool validateNewLoanCode()
        {
            bool result = false;
            StringBuilder errMsg = new StringBuilder();
            ReportController rc = new ReportController();

            rc.removeInvalidChar(txtLoanCode);
            if (ddlLoanType.SelectedIndex <= 0)
                errMsg.Append(Resources.resource.Please_Select_One + "\\n");
            if (string.IsNullOrEmpty(secure.RC(txtLoanCode.Text)) || string.IsNullOrWhiteSpace(secure.RC(txtLoanCode.Text)))
                errMsg.Append(Resources.resource.InvalidLoanCode + "\\n");

            if (errMsg.ToString().Trim() != string.Empty)
            {
                sqlString.displayAlert2(this, errMsg.ToString());
                lblErr.Text = errMsg.Replace("\\n", "<br />").ToString();
                lblErr.Visible = true;
                divErrorMessage.Visible = true;
                lblErr.Focus();
            }
            else
            {
                lblErr.Text = "";
                lblErr.Visible = false;
                divErrorMessage.Visible = false;
                result = true;
            }

            return result;
        }
        protected void btnAddLoanCode_Click(object sender, EventArgs e)
        {
            if (validateNewLoanCode())
            {
                StringBuilder sql = new StringBuilder();
                wwdb db = new wwdb();

                sql.Clear();
                sql.AppendFormat(@"
                    INSERT INTO tbl_LoanTypesXML (LoanID, LoanTypeCode, LoanTypeName, CreatedBy, CreatedAt, isDeleted)
                    VALUES ('{0}', '{1}', (select top 1 [Name] from tbl_LoanTypes lt with (nolock)
                                            inner join tbl_LoanTypesXML ltx with (nolock) on ltx.LoanID = lt.LoanID
                                            where lt.LoanID = '{0}'), 'SYSTEM', getdate(), 0);
                ", ddlLoanType.SelectedValue
                , txtLoanCode.Text);

                db.Execute(sql.ToString());
                if (db.HasError)
                {
                    LogUtil.logError(db.ErrorMessage, sql.ToString());
                    sqlString.displayAlert(this, Resources.resource.unknownError);
                }
                else
                    sqlString.displayAlert(this, Resources.resource.ActionSuccessfulMsg, Request.Url.ToString());
            }
        }
        #endregion
    }
}