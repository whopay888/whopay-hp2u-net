﻿using HJT.Cls.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Helper;
using Synergy.Model;
using Synergy.Util;

namespace WMElegance.Form.Setting
{
    public partial class UACList : System.Web.UI.Page
    {
        LoginUserModel login_user = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];

            if (!IsPostBack)
            {
                this.btnSearch_Click(null, null);
                bindData();
            }
        }

        private void bindData()
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            #region Speed Mode Report Type rbl
            sql.Append(@" 
                SELECT p.ParameterName as '0', p.ParameterValue as '1' 
                FROM tbl_Parameter p with (nolock) where p.Category = 'Report';
            ");
            sqlString.bindControl(rblReportType, sql.ToString(), "0", "1");

            db.OpenTable("SELECT Name FROM tbl_PageController WITH (nolock) WHERE Category = 'ReportSetting' AND IsTrue = 1;");
            if (db.RecordCount() > 0)
                rblReportType.SelectedValue = db.Item("Name");
            #endregion

            #region DSR Report Advise Section Visibility
            db.OpenTable("SELECT isTrue FROM tbl_PageController WITH (nolock) WHERE Category = 'Advise Section' AND Name = 'Visible';");
            if (db.RecordCount() > 0)
                if (ConvertHelper.ConvertToBoolen(db.Item("isTrue"), false))
                    rblAdviseVisibility.SelectedIndex = 0;
                else
                    rblAdviseVisibility.SelectedIndex = 1;
            #endregion

            #region Reuse Report function activation
            db.OpenTable(@"SELECT isTrue FROM tbl_PageController WITH (nolock) WHERE Category = 'Reuse Report' AND Name = 'Active';");
            if (db.RecordCount() > 0)
                if (ConvertHelper.ConvertToBoolen(db.Item("isTrue"), false))
                    rblReportUse.SelectedIndex = 0;
                else
                    rblReportUse.SelectedIndex = 1;
            #endregion

            #region Report Expiration Days
            sql.Clear();
            sql.Append(@"
                select p.ParameterValue as 'Day' from tbl_Parameter p with (nolock) 
                where p.Category = 'ReportExpiration' and p.ParameterName = 'ReportExpirationDay'
            ");
            db.OpenTable(sql.ToString());
            if (db.RecordCount() > 0)
                txtReportExpiration.Text = db.Item("Day");
            #endregion

            #region Admin Whatsapp Number
            sql.Clear();
            sql.Append(@"
                select p.ParameterValue as 'whatsAppNumber' from tbl_Parameter p with (nolock) 
                where p.Category = 'Whatsapp' and p.ParameterName = 'AccountNumber'
            ");
            db.OpenTable(sql.ToString());
            if (db.RecordCount() > 0)
                txtWhatsAppNumber.Text = db.Item("whatsAppNumber");
            #endregion

            #region ClientList No.of rows
            sql.Clear();
            sql.Append(@"
                SELECT ParameterValue as 'Rows' FROM tbl_Parameter with (nolock)
                where Category = 'ClientList' and ParameterName = 'NumberOfDataRows'
            ");
            db.OpenTable(sql.ToString());
            if (db.RecordCount() > 0)
                txtNoOfRows.Text = db.Item("Rows");
            #endregion
        }

        #region Control Event

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            sqlString.bindControl(gv, getExData(null, null, true));
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            sqlString.exportFunc("List", getExData(null, null, false));
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            this.TxtSearch_RoleCode.Text = String.Empty;
            this.TxtSearch_RoleName.Text = String.Empty;

            this.btnSearch_Click(sender, e);
        }

        protected void btnAddRole_Click(object sender, EventArgs e)
        {
            Response.Redirect("UACDetails.aspx?action=ADD");
        }

        #region GridView Control

        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            Int32 Index_Val = Convert.ToInt32(btn.CommandArgument) % this.gv.PageSize;
            String RoleId = this.gv.DataKeys[Index_Val].Values[Resources.resource.RoleCode].ToString();

            wwdb db = new wwdb();
            string sql = "";
            try
            {
                sql = " update tbl_RoleControl set isDeleted='1' where RoleID='" + RoleId + "'";
                db.Execute(sql);
                LogUtil.logAction(sql, "UACList.aspx");
            }
            catch (Exception ex)
            { LogUtil.logError(ex.ToString(), sql); sqlString.displayAlert2(this, "* Unknown Error Occured."); }
            //Validation
            finally
            {
                db.Close();
                sqlString.bindControl(gv, getExData(null, null, true));
            }
        }

        protected void BtnEdit_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            Int32 Index_Val = Convert.ToInt32(btn.CommandArgument) % this.gv.PageSize;
            String RoleId = this.gv.DataKeys[Index_Val].Values[Resources.resource.RoleCode].ToString();
            String Encry_RoleId = secure.EURC(secure.Encrypt(RoleId, true));
            Response.Redirect("UACDetails.aspx?action=EDIT&Roleid=" + Encry_RoleId);
        }

        #endregion

        #endregion

        #region GetData

        protected string getExData(string columnName, string sortOrder, bool hasOrder)
        {
            String SQL = String.Empty;
            SQL = "SELECT * FROM (" +

            " SELECT " +
            " RoleId AS N'" + Resources.resource.RoleCode + "'," +
            " RoleName AS N'" + Resources.resource.RoleName + "'," +
            " ISNULL((SELECT COUNT(*) FROM tbl_MemberRole TMR with(nolock) WHERE TMR.RoleCode = TRC.RoleId and TMR.MemberId in (select MemberId from tbl_MemberInfo where Status='A')),0) AS N'" + Resources.resource.RegisteredUser + "', " +
            " CONVERT(NCHAR(1), isMandatory) AS N'" + Resources.resource.MandatoryRole + "' " +
            " FROM tbl_RoleControl TRC with(nolock) WHERE IsDeleted = 0 ";

            SQL += sqlString.searchTextBox("RoleId", this.TxtSearch_RoleCode, true, true, false);
            SQL += sqlString.searchTextBox("RoleName", this.TxtSearch_RoleName, true, true, false);

            SQL += ") AS A ";

            if (hasOrder)
            {
                if (columnName != null && sortOrder != null)
                {
                    if (columnName.Trim() != string.Empty && sortOrder.Trim() != string.Empty)
                    {
                        SQL += " ORDER BY A.[" + columnName + "] " + sortOrder;
                    }
                }
                else
                {
                    SQL += " ORDER BY A.[" + Resources.resource.RoleCode + "] ASC ";
                }
            }

            LogUtil.logSQL(SQL);

            return SQL;
        }

        #endregion

        #region GridView - GV

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) || (e.Row.RowType == DataControlRowType.Header) || (e.Row.RowType == DataControlRowType.Footer))
            {
                e.Row.Cells[4].Visible = false;
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button BtnDelete = (Button)e.Row.FindControl("BtnDelete");
                BtnDelete.OnClientClick = Resources.resource.Confirm_DeleteRole;

                //IsMandatory : Indicate this role is a compulsory role, cannot be delete --------------
                String IsMandatory = DataBinder.Eval(e.Row.DataItem, Resources.resource.MandatoryRole).ToString();
                if (IsMandatory == "1")
                { BtnDelete.Visible = false; }
                //---------------------------------------------------------------------------------------

                //Registered_User : IF Registerd User > 0 , unable to remove this role ------------------
                int Registered_User = int.Parse(DataBinder.Eval(e.Row.DataItem, Resources.resource.RegisteredUser).ToString());
                if (Registered_User > 0)
                { BtnDelete.Visible = false; }
                //---------------------------------------------------------------------------------------                
            }
        }

        private SortDirection GridViewSortDirection1
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;

                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            string columnName = null;
            string sortOrder = null;

            if (ViewState["Field1"] != null)
            { columnName = ViewState["Field1"].ToString(); }

            if (ViewState["Direction1"] != null)
            { sortOrder = ViewState["Direction1"].ToString(); }

            gv.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue.Trim());
            sqlString.bindControl(gv, getExData(columnName, sortOrder, true));
        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            string columnName = null;
            string sortOrder = null;
            if (ViewState["Field1"] != null)
            { columnName = ViewState["Field1"].ToString(); }

            if (ViewState["Direction1"] != null)
            { sortOrder = ViewState["Direction1"].ToString(); }

            gv.PageIndex = e.NewPageIndex;
            sqlString.bindControl(gv, getExData(columnName, sortOrder, true));
        }

        protected void gv_Sorting(object sender, GridViewSortEventArgs e)
        {
            string direction = "ASC";

            if (GridViewSortDirection1 == SortDirection.Ascending)
            {
                GridViewSortDirection1 = SortDirection.Descending;
                direction = "DESC";
            }
            else
            {
                GridViewSortDirection1 = SortDirection.Ascending;
                direction = "ASC";
            }

            ViewState["Field1"] = e.SortExpression; ViewState["Direction1"] = direction;

            sqlString.bindControl(gv, getExData(e.SortExpression, direction, true));
        }

        #endregion

        private bool validateWhatsAppNumber()
        {
            ReportController rc = new ReportController();
            rc.removeInvalidChar(txtWhatsAppNumber);

            if (string.IsNullOrEmpty(txtWhatsAppNumber.Text) || string.IsNullOrWhiteSpace(txtWhatsAppNumber.Text))
                return false;
            else if (!(txtWhatsAppNumber.Text[0].ToString().Equals("0") || txtWhatsAppNumber.Text[0].ToString().Equals("6")))
                return false;
            else if (!(txtWhatsAppNumber.Text.Length >= 10 && txtWhatsAppNumber.Text.Length <= 12))
                return false;
            else if (!(long.TryParse(txtWhatsAppNumber.Text, out _)))
                return false;

            return true;
        }
        private bool validateReportExpiration()
        {
            ReportController rc = new ReportController();
            rc.removeInvalidChar(txtReportExpiration);

            if (string.IsNullOrEmpty(txtReportExpiration.Text) || string.IsNullOrWhiteSpace(txtReportExpiration.Text))
                return false;
            else if (!(int.TryParse(txtReportExpiration.Text, out _)))
                return false;
            else if (!long.TryParse(txtReportExpiration.Text, out _))
                return false;

            return true;
        }
        private bool validateDataRowNumber()
        {
            ReportController rc = new ReportController();
            rc.removeInvalidChar(txtNoOfRows);

            if (string.IsNullOrEmpty(txtNoOfRows.Text) || string.IsNullOrWhiteSpace(txtNoOfRows.Text))
                return false;
            else if (!(int.TryParse(txtNoOfRows.Text, out _)))
                return false;

            return true;
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            sql.Clear();

            #region Speed Mode Report Type
            sql.Append("UPDATE tbl_PageController SET IsTrue = 0 WHERE Category = 'ReportSetting'; ");
            sql.AppendFormat(@"
                UPDATE tbl_PageController SET IsTrue = 1
                WHERE Category = 'ReportSetting' AND Name = '{0}'; 
            ", rblReportType.SelectedValue.Trim());
            #endregion

            #region DSR Report Advise Section Visibility
            int rblAdviseVisIndex = rblAdviseVisibility.SelectedIndex;
            sql.AppendFormat(@"
                UPDATE tbl_PageController SET IsTrue = '{0}' 
                WHERE Category = 'Advise Section' AND Name = 'Visible'; 
            ", rblAdviseVisIndex == 0 ? "1" : "0");
            #endregion

            #region Reuse Report function activation
            int rblReuseIndex = rblReportUse.SelectedIndex;
            sql.AppendFormat(@"
                UPDATE tbl_PageController SET isTrue = '{0}'
                WHERE Category = 'Reuse Report' AND Name = 'Active';
            ", rblReuseIndex == 0 ? "1" : "0");
            #endregion

            #region Admin Whatsapp Number
            if (validateWhatsAppNumber())
            {
                sql.AppendFormat(@"
                    update tbl_Parameter set ParameterValue = '{0}' 
                    where Category = 'Whatsapp' and ParameterName = 'AccountNumber';
                ", secure.RC(txtWhatsAppNumber.Text));
            }
            #endregion

            #region Report Expiration Days
            if (validateReportExpiration())
            {
                sql.AppendFormat(@"
                    update tbl_Parameter set ParameterValue = '{0}' 
                    where Category = 'ReportExpiration' and ParameterName = 'ReportExpirationDay'
                ", secure.RC(txtReportExpiration.Text));
            }
            #endregion

            #region Data rows limit in ClientList
            if (validateDataRowNumber())
            {
                sql.AppendFormat(@"
                    UPDATE tbl_Parameter SET ParameterValue = '{0}' 
                    WHERE Category = 'ClientList' AND ParameterName = 'NumberOfDataRows';
                ", txtNoOfRows.Text);
            }
            #endregion

            if (!string.IsNullOrEmpty(sql.ToString())) //should be always true due to reportType and adviseVis Section
            {
                db.Execute(sql.ToString());

                if (db.HasError)
                {
                    LogUtil.logError(db.ErrorMessage, sql.ToString());
                    sqlString.displayAlert(this, Resources.resource.Submit_Error);
                }
                else
                    sqlString.displayAlert(this, Resources.resource.UpdateSuccess, Request.Url.ToString());
            }
        }
    }
}