﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Synergy.Model;
using Synergy.Util;
using System.Text;
using System.Data;
using System.Web.UI;

namespace HJT.Form.Setting
{
    public partial class ConsentForm_Setting : System.Web.UI.Page
    {
        LoginUserModel login_user = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];
            spHeader.InnerHtml = Resources.resource.ConsentFormList;
            if (!IsPostBack)
            {
                sqlString.bindControl(gv, getConsentFormList());
                if (Request["OID"] != null)
                {
                    // txtOutletID.Text = sqlString.decryptURL(Request["OID"].ToString()).Trim().ToUpper();
                }

                //  sqlString.bindControl(gv, getExData(null, null, true, false));
            }
        }
        private DataTable MakePivotTable(DataTable mainTable)
        {
            DataTable pivotTable = new DataTable();
            DataRow dr = null;


            for (int i = 0; i <= mainTable.Rows.Count; i++)
            {
                pivotTable.Columns.Add(new DataColumn(mainTable.Columns[i].ColumnName, typeof(String)));
            }


            for (int cols = 0; cols < mainTable.Columns.Count; cols++)
            {
                dr = pivotTable.NewRow();
                for (int rows = 0; rows < mainTable.Rows.Count; rows++)
                {
                    if (rows < mainTable.Columns.Count)
                    {
                        dr[0] = mainTable.Columns[cols].ColumnName;
                        dr[rows + 1] = mainTable.Rows[rows][cols];
                    }
                }
                pivotTable.Rows.Add(dr);
            }
            return pivotTable;
        }

        #region GridView - GV

        private SortDirection GridViewSortDirection1
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;

                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }
        }

        protected void gv_DataBound(object sender, EventArgs e)
        {
            if (gv.Rows.Count > 0)
            {
                if (!login_user.isStaffAdmin() && !login_user.UserType.Contains(KeyVal.CreditAdvisor))
                {
                    int actionIndex = gridView.GetColumnIndexByName(gv, Resources.resource.Action);
                    gridView.hideColumn(gv, new int[] { actionIndex }, true, true);
                }

            }
        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button btnActive = (Button)e.Row.FindControl("btnActive");
                bool isActive = Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "IsActive"));

                btnActive.Visible = !isActive;
            }
        }

        private string getConsentFormList()
        {
            StringBuilder sql = new StringBuilder();

            sql.Clear();
            sql.Append("SELECT Id AS 'ID', DocumentName AS 'Document Name', CreatedAt AS 'Date', Remarks, IsActive FROM tbl_ConsentDocs WHERE isDeleted = 0");

            return sql.ToString();
        }

        #endregion

        protected void btnAddRow_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Form/Setting/AddConsentForm_Setting.aspx");
            //not directing after running this line of code
            //Server.Transfer("/Form/Setting/AddConsentForm_Setting.aspx");
        }

        protected void btnReplace_Click(object sender, EventArgs e)
        {

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            int idIndex = gridView.GetColumnIndexByName(gv, Resources.resource.ID);
            string id = ((GridViewRow)btn.NamingContainer).Cells[idIndex].Text.ToString().Trim();
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            try
            {
                sql.Clear();
                sql.AppendFormat(@" 
                    UPDATE tbl_ConsentDocs SET IsDeleted = 'True', 
                        updatedBy = N'{0}', UpdatedAt = GETDATE() 
                    WHERE Id = N'{1}'; 
                ", secure.RC(login_user.UserId), secure.RC(id));
                db.Execute(sql.ToString());
                LogUtil.logAction(sql.ToString(), "Delete Consent Form Document. ID='" + secure.RC(id) + "'");

                sqlString.displayAlert(this, "Action_Successful");
                sqlString.bindControl(gv, getConsentFormList());
            }

            catch (Exception ex) { LogUtil.logError(ex.ToString(), sql.ToString()); sqlString.displayAlert(this, KeyVal.UnknownError); }
            finally { db.Close(); }
        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            //wwdb db = new wwdb();
            //StringBuilder sql = new StringBuilder();

            //sql.Clear();
            //sql.Append("SELECT TOP(1) DocsURL FROM tbl_ConsentDocs WHERE isDeleted = 0 OR isDeleted IS NULL"); //ToDo: add active column and enable to set by admin
            //db.OpenTable(sql.ToString());

            //if (db.RecordCount() > 0)
            //{
            //    string filePath = db.Item("DocsURL");

            //    sqlString.OpenNewWindow_Center(filePath, "Consent Form", 600, 600, this);
            //}
            Button btnsender = (Button)sender;
            string cId = btnsender.CommandArgument.ToString();
            sqlString.OpenNewWindow_Center("/Form/Setting/ConsentForm.aspx?CID=" + sqlString.encryptURL(cId), "Consent Form", 600, 800, this);
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            Button btnsender = (Button)sender;
            string cId = btnsender.CommandArgument.ToString();
            Response.Redirect("/Form/Setting/EditConsentForm_Setting.aspx?CID=" + sqlString.encryptURL(cId));
        }

        protected void btnActive_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            string id = btn.CommandArgument.ToString();
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            try
            {
                sql.Clear();
                sql.AppendFormat(@" 
                    UPDATE tbl_ConsentDocs SET IsActive = 0;
                    UPDATE tbl_ConsentDocs SET IsActive = 1, 
                        updatedBy = N'{0}', UpdatedAt = GETDATE() 
                        WHERE Id = N'{1}'; 
                ", secure.RC(login_user.UserId)
                , secure.RC(id));

                db.Execute(sql.ToString());
                LogUtil.logAction(sql.ToString(), "Update Consent Form Document. ConsentFormID='" + id + "'");

                sqlString.displayAlert(this, "Action_Successful");
                sqlString.bindControl(gv, getConsentFormList());
            }

            catch (Exception ex) { LogUtil.logError(ex.ToString(), sql.ToString()); sqlString.displayAlert(this, KeyVal.UnknownError); }
            finally { db.Close(); }
        }
    }
}