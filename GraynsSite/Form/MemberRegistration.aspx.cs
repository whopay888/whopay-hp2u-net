﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Util;
using Synergy.Model;
using Synergy.UI;
using Synergy.Controller;
using System.IO;
using System.Data;
using System.Configuration;
using System.Text;

namespace WMElegance.Form
{
    public partial class MemberRegistration : System.Web.UI.Page
    {
        #region ErrorMsg Class

        error errormessage = new error();

        #endregion

        #region Captchar Variable

        public Captcha Captchar_Class;

        //private String Captchar_Text = "qpweirutyghfjdksamxncbzvZABCDZABCD#EFGHJK%MNPQR&STUVWX$YZ23456789";
        private String Captchar_Text = "0123456789";
        private Int16 Captchar_Length = 4;
        private Double CaptChar_FontSize = 15;
        private String CaptChar_FontFamily = "Verdana";
        private String CaptChar_BackgroundImgPath = "/images/CaptCharBG.png";
        private String CaptChar_TextColor = "Black";

        #endregion

        string uploadImage = string.Empty;

        #region PAGE EVENT

        protected void Page_Load(object sender, EventArgs e)
        {
            string Password = txtPassword.Text;
            txtPassword.Attributes.Add("value", Password);

            string ConfirmPassword = txtConfirmPassword.Text;
            txtConfirmPassword.Attributes.Add("value", ConfirmPassword);

            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request["rid"] as string))
                {
                    string requestID = secure.Decrypt(Validation.GetUrlParameter(Page, "rid", ""), true);
                    wwdb db = new wwdb();
                    StringBuilder sql = new StringBuilder();

                    sql.Clear();
                    sql.AppendFormat(@"
                        SELECT a.Name, a.MyCard, a.Age
                        from tbl_Applicants a with (nolock)
                        where a.RequestID = N'{0}' and a.IsDeleted = 0
                    ", requestID);
                    db.OpenTable(sql.ToString());
                    if (db.RecordCount() > 0)
                    {

                    }
                }
                else
                {
                    //Clear All Session On Page Load -------------------------------
                    Session.Remove("CaptchaClass");
                    Session.Remove(KeyVal.session_member);
                    Session.Remove(KeyVal.session_clogin);
                    //--------------------------------------------------------------

                    //Load CaptChar() ----------------------------------------------
                    while (Session["CaptchaClass"] == null)
                    { this.LoadCaptcha(); }
                    //--------------------------------------------------------------

                    //this.TxtUserId.Focus();

                    //this.Bind_BackgroundImage();
                    Session["registrationImage"] = uploadImage;
                }
            }
            uploadImage = (string)Session["registrationImage"];
        }

        #endregion

        #region CAPTCHAR

        public void LoadCaptcha()
        {
            String text = this.Generate_RandomText();
            Session["cctext"] = text;
            ViewState.Add("captcha", text);
            Session.Add("CaptchaClass", this.GET_CaptCharClass());//add captcha object to Session
            Session.Add("captcha", text);//add captcha text to session             
            //Im1.ImageUrl = "/captcha/CaptchaHandler.ashx";
            LogUtil.logError("Forgotpassword.aspx", Session["captcha"] + "||" + text);


        }
        protected void Unnamed_ServerClick(object sender, EventArgs e)
        {
            string a = HttpContext.Current.Request.Url.AbsolutePath;

            a += "?a=" + sqlString.encryptURL("asdfNSIDOF");

            //if (Request.QueryString["path"] != null)
            //    a += "&path=" + sqlString.encryptURL(sqlString.decryptURL(Request.QueryString["path"].ToString()));

            //if (TxtUserId.Text.Trim() != string.Empty)
            //    a += "&id=" + sqlString.encryptURL(TxtUserId.Text);

            //if (TxtPassword.Text.Trim() != string.Empty)
            //    a += "&pa=" + sqlString.encryptURL(TxtPassword.Text);

            Response.Redirect(a);
        }

        private Captcha GET_CaptCharClass()
        {
            Session.Remove("CaptchaClass");
            Captchar_Class = new Captcha();

            Captchar_Class.FontSize = this.CaptChar_FontSize;
            Captchar_Class.FontFamily = this.CaptChar_FontFamily;
            Captchar_Class.BackgroundImagePath = this.CaptChar_BackgroundImgPath;
            Captchar_Class.TextColor = this.CaptChar_TextColor;
            return Captchar_Class;
        }

        private string Generate_RandomText()
        {

            char[] letters = Captchar_Text.ToCharArray();

            string text = string.Empty;
            Random r = new Random();
            int num = -1;

            for (int i = 0; i < this.Captchar_Length; i++)
            {
                num = (int)(r.NextDouble() * (letters.Length - 1));
                text += letters[num].ToString();
            }
            return text;
        }

        #endregion

        #region VALIDATION FUNCTION




        #endregion

        #region FUNCTION

        private bool checkUsername(string username)
        {
            string returnStr = string.Empty;
            if (username != string.Empty)
            {
                wwdb db = new wwdb();
                db.OpenTable("select top 1 1 from tbl_memberinfo where username=N'" + secure.RC(txtUserName.Text) + "'");

                if (db.RecordCount() > 0)
                {
                    return true;

                }

            }

            return false;

        }

        private Boolean RequestOTP_Validation()
        {
            this.LblError.Text = String.Empty;
            Boolean Validate_Success = false;
            if (this.RequireField_Validation())
            {
                if (this.RequireField_Format_Validation())
                {
                    if (this.System_Validation())
                    {
                        Validate_Success = true;
                    }
                }
            }
            return Validate_Success;
        }

        private Boolean RequireField_Validation()
        {
            String NewLine = "<br/>";
            String ErrMsg = Resources.resource.InputRequiredField;
            String ErrField = String.Empty;


            String UserName = this.txtUserName.Text.Trim();
            String FullName = this.txtName.Text.Trim();
            String Email = this.txtEmail.Text.Trim();
            //String Phone = this.txtMobile.Text.Trim();
            String Password = this.txtPassword.Text.Trim();
            String ConfirmPassword = this.txtConfirmPassword.Text.Trim();

            if (String.IsNullOrEmpty(UserName))
            { ErrField += "-" + Resources.resource.Contact_Number + "." + NewLine; }

            if (String.IsNullOrEmpty(FullName))
            { ErrField += "-" + Resources.resource.Name + NewLine; }

            if (String.IsNullOrEmpty(Email))
            { ErrField += "-" + Resources.resource.Email + NewLine; }

            //if (String.IsNullOrEmpty(Phone))
            //{ ErrField += "-" + Resources.resource.Phone_Number + NewLine; }

            if (String.IsNullOrEmpty(Password))
            { ErrField += "-" + Resources.resource.Password + NewLine; }

            if (String.IsNullOrEmpty(ConfirmPassword))
            { ErrField += "-" + Resources.resource.confirm_Password + NewLine; }

            if (String.IsNullOrEmpty(ErrField))
            { return true; }
            else
            {
                string strMsg = (ErrMsg + ErrField).Replace("<br/>", "\\n");
                //Show error Message ---------------------------                      
                this.LblError.Text = errormessage.AlertText(ErrMsg + ErrField);
                //----------------------------------------------
                sqlString.displayAlert(this, strMsg);
                return false;
            }
        }

        private Boolean RequireField_Format_Validation()
        {
            String NewLine = "<br/>";
            String ErrMsg = Resources.resource.InputRequiredField;
            String ErrField = String.Empty;


            String Email = this.txtEmail.Text.Trim();
            String Password = this.txtPassword.Text.Trim();
            String ConfirmPassword = this.txtConfirmPassword.Text.Trim();


            if (!Validation.IsValidEmail(Email))
            { ErrField += "-" + Resources.resource.emailFormatError + NewLine; }

            if (Password != ConfirmPassword)
            { ErrField += "-" + Resources.resource.confirm_Password_Validation + NewLine; }

            if (String.IsNullOrEmpty(ErrField))
            { return true; }
            else
            {
                string strMsg = (ErrMsg + ErrField).Replace("<br/>", "\\n");
                //Show error Message ---------------------------                      
                this.LblError.Text = errormessage.AlertText(ErrMsg + ErrField);
                //----------------------------------------------
                sqlString.displayAlert(this, strMsg);
                return false;
            }
        }

        private Boolean RequestSubmit_Validation()
        {
            this.LblError.Text = String.Empty;
            Boolean Validate_Success = false;
            if (this.RequestOTP_Validation())
            {
                if (this.ValidateOTP(txtotp.Text.Trim(), txtUserName.Text.Trim()))
                {
                    Validate_Success = true;
                }
            }
            return Validate_Success;
        }

        private Boolean System_Validation()
        {
            String NewLine = "<br/>";
            String ErrMsg = Resources.resource.InputRequiredField;
            String ErrField = String.Empty;


            String UserName = this.txtUserName.Text.Trim();

            if (checkUsername(UserName))
            { ErrField += "-" + Resources.resource.usernameDuplicateError + "." + NewLine; }


            if (String.IsNullOrEmpty(ErrField))
            { return true; }
            else
            {
                string strMsg = (ErrMsg + ErrField).Replace("<br/>", "\\n");
                //Show error Message ---------------------------                      
                this.LblError.Text = errormessage.AlertText(ErrMsg + ErrField);
                //----------------------------------------------
                sqlString.displayAlert(this, strMsg);
                return false;
            }
        }

        public void AddFreeRequesttoUser(string memberId)
        {
            if (memberId != string.Empty)
            {
                wwdb db = new wwdb();
                db.OpenTable("select top 1 1 from tbl_UserCreditBalance where MemberId=N'" + secure.RC(memberId) + "'");
                
                if (db.RecordCount() <= 0)
                {
                    int FreeCount = int.Parse(ConfigurationManager.AppSettings["FreeRequestCount"]);
                    StringBuilder sql = new StringBuilder();
                    sql.AppendFormat(@"
                        INSERT INTO tbl_UserTransactions (tbl_UserTransactions.MemberId, tbl_UserTransactions.Amount, tbl_UserTransactions.Status, tbl_UserTransactions.RequestType, 
                            tbl_UserTransactions.CreatedAt, tbl_UserTransactions.IsFreeRequest, tbl_UserTransactions.FreeRequestCount, tbl_UserTransactions.CreatedBy, tbl_UserTransactions.Remark)
                        VALUES ('{0}', 0, 1, 'Free Request', GETDATE(), 0, {1}, 'SYSTEM', 'SYSTEM FREE REQUEST');
                        IF (SELECT 1 FROM tbl_UserCreditBalance WHERE MemberID = '{0}') > 0 
                        UPDATE tbl_UserCreditBalance SET FreeRequestCount = ISNULL(FreeRequestCount,0) + ({1}) WHERE MemberId = '{0}';
                        ELSE 
                        INSERT INTO tbl_UserCreditBalance(MemberId, CreditUsed, CreatedAt, FreeRequestCount) VALUES('{0}', 0, GETDATE(), {1});
                    ", secure.RC(memberId), FreeCount);

                    db.Execute(sql.ToString());
                }
            }
        }

        //private Boolean Send_ResetLink()
        //{
        //    Boolean Send_Complete = false;
        //    String User_Id = this.TxtUserId.Text.Trim();

        //    String SQL = "SELECT TOP 1 Login_Password AS 'Password', fullName AS 'MemberName' , username as 'loginID', memberId AS 'MemberID', email AS 'MemberEmail'  FROM tbl_login TL " +
        //    " LEFT JOIN tbl_MemberInfo TM ON TL.Login_Id = TM.MemberId " +
        //    " WHERE (Login_Id = N'" + secure.RC(User_Id) + "' or tm.username=N'" + secure.RC(User_Id) + "')";


        //    wwdb db = new wwdb();
        //    db.OpenTable(SQL);
        //    if (!db.HasError && db.RecordCount() == 1)
        //    {
        //        String Member_Id = db.Item("MemberID");
        //        string username = db.Item("loginID");
        //        String Member_Email = db.Item("MemberEmail"); this.HFEmail.Value = Member_Email;
        //        String Member_Name = db.Item("MemberName");
        //        String Member_Password = db.Item("Password");

        //        //Insert into [tbl_ResetPassword] As A Record -------------------------------------------
        //        //SQL = "EXEC [SPR_PasswordResetRequest] N'" + secure.RC(Member_Id) + "'";
        //        //db.OpenTable(SQL);

        //        //if (!db.HasError && db.RecordCount() == 1)
        //        //{
        //        //    String Token_ID = db.Item("TokenID");
        //        //    String Encry_TokenID = secure.EURC(secure.Encrypt(Token_ID, true));
        //        //    String Start_Time = db.Item("StartTime");
        //        //    String End_Time = db.Item("EndTime");

        //        //Send Email Notification To Member -----------------------------------------------------

        //        Random rnd = new Random();
        //        string rndPass = string.Empty;

        //        //for (int i = 0; i < 6; i++)
        //        //{
        //        //    rndPass += rnd.Next(0, 9);
        //        //}

        //        //if (rndPass != "")
        //        //{
        //        //    rndPass = secure.Encrypt(rndPass,true);
        //        //}

        //        rndPass = GetRandomText();

        //        if (rndPass != string.Empty)
        //        {
        //            rndPass = secure.Encrypt(rndPass, true);
        //        }

        //        EmailController EmailCtrl = new EmailController();
        //        //temporary change from Member_Password to 
        //        if (EmailCtrl.SendForgotPassword(username, Member_Name, Member_Email, rndPass))
        //        {
        //            Send_Complete = true;

        //            if (rndPass != "")
        //            {
        //                SQL = "";
        //                SQL = " UPDATE tbl_Login SET login_password=N'" + secure.RC(rndPass) + "',firstLogin=0 WHERE login_id=N'" + secure.RC(Member_Id) + "';";

        //                db.Execute(SQL);

        //                LogUtil.logAction(SQL, "Reset New Password (From ForgotPassword.aspx)");
        //            }
        //        }
        //        else { this.LblError.Text = errormessage.AlertText("An error occured while sending email notification, please try again later."); }
        //        //---------------------------------------------------------------------------------------
        //        //}
        //        //else { this.LblError.Text = errormessage.AlertText("An error occued while updating records, please try again later."); }
        //        ////---------------------------------------------------------------------------------------                
        //    }
        //    else { this.LblError.Text = errormessage.AlertText(Resources.resource.usernameInvalidError); }

        //    db.Close();
        //    return Send_Complete;
        //}

        //private void Bind_BackgroundImage()
        //{
        //    String SQL = String.Empty;
        //    wwdb db = new wwdb();

        //    SQL = "SELECT TOP 1 FullPath, ImageURl, ToolTip, URL FROM tbl_ImagePath TI WHERE TI.[IsDeleted] = 0 " +
        //          " ORDER BY RowId DESC ";
        //    db.OpenTable(SQL); db.Close();
        //    if (!db.HasError && db.RecordCount() == 1)
        //    {
        //        String ImgPath = db.Item("ImageUrl");
        //        String ToolTip = db.Item("ToolTip");
        //        String LinkURL = db.Item("URL");

        //        String ALink_StartTag = ""; String ALink_EndTag = "";
        //        if (!String.IsNullOrEmpty(LinkURL))
        //        {
        //            ALink_StartTag = "<a href='" + LinkURL + "' target='_blank'>";
        //            ALink_EndTag = "</a>";
        //        }

        //        String ImgTag = "<img src='" + ImgPath + "' width='600px'; height='600px' alt='" + ToolTip + "'; title='" + ToolTip + "';></img>";
        //        //this.LtrBackground.Text = ALink_StartTag + ImgTag + ALink_EndTag;
        //        string hiddenValue = ImgPath ;


        //        Uri myUri;
        //        if (Uri.TryCreate(ImgPath, UriKind.RelativeOrAbsolute, out myUri))
        //        {
        //            imgsrc.Value = ImgPath;
        //        }
        //        else
        //        {
        //            imgsrc.Value = "0";
        //        }


        //    }

        //}
        #endregion

        #region BUTTON EVENT

        protected void txtUserName_TextChanged(object sender, EventArgs e)
        {

            //lblusernameerror.Text = checkUsername(txtUserName.Text).Replace("\\n", "</br>");
            //lblusernameerror.Visible = lblusernameerror.Text.Length > 0;

        }


        protected void btnUpload_Click(object sender, EventArgs e)
        {
            FileUpload fupload = (FileUpload)fu1;

            if (fupload != null && fupload.HasFile)
            {
                string path = "/Upload/Temp/Registraion/";
                string physicalPath = Server.MapPath("~" + path);
                FileInfo Finfo = new FileInfo(fupload.PostedFile.FileName);
                string filename = DateTime.Now.ToString("ddMMyyyyhhmmssfff") + Finfo.Extension;
                string fullPhysical = physicalPath + filename;
                if (!Directory.Exists(physicalPath))
                    Directory.CreateDirectory(physicalPath);

                if (File.Exists(physicalPath))
                {
                    File.Delete(fullPhysical);
                }

                fupload.SaveAs(fullPhysical);
                uploadImage = fullPhysical;
                Session["registrationImage"] = uploadImage;
                Image imgDisplay = (Image)img_fu1;
                imgDisplay.Visible = true;
                imgDisplay.Attributes.Add("onclick", "window.open('" + path + filename + "')");
            }

        }

        protected void btnOTP_Click(object sender, EventArgs e)
        {
            String ErrMsg = Resources.resource.Action_Successful;
            String ErrField = String.Empty;


            if (RequestOTP_Validation())
            {
                wwdb db = new wwdb();
                db.OpenTable("select Email from tbl_OneTimePasscode where username=N'" + secure.RC(txtUserName.Text) + "' AND ExpiryAt > '" + DateTime.Now + "'");
                if (db.RecordCount() > 0)
                {

                    ErrField += "-" + Resources.resource.OTP_Sent_Validation;

                    string strMsg = (ErrMsg + ErrField).Replace("<br/>", "\\n");
                    //Show error Message ---------------------------                      
                    this.LblError.Text = errormessage.AlertText(ErrMsg + ErrField);
                    //----------------------------------------------
                    sqlString.displayAlert(this, strMsg);
                }
                else
                {
                    string otp = GenerateOTP(txtUserName.Text, txtEmail.Text);
                    EmailController EmailCtrl = new EmailController();
                    MessagingService messagingService = new MessagingService();
                    //if (EmailCtrl.SendOTP(txtEmail.Text.Trim(), otp) && messagingService.sendOTP(txtUserName.Text, otp))
                    if (messagingService.sendOTP(txtUserName.Text, otp)) //change to verify using only mobileNo
                    {
                        if (otp != string.Empty && otp != null)
                        {
                            //ErrField += "-" + Resources.resource.OTP_Sent_Successfully.Replace("@#email", txtEmail.Text);
                            ErrField += "-" + Resources.resource.OTP_Sent_Successfully.Replace("@#mobile", txtUserName.Text);

                            string strMsg = (ErrMsg + ErrField).Replace("<br/>", "\\n");
                            //Show error Message ---------------------------                      
                            this.LblError.Text = errormessage.AlertText(ErrMsg + ErrField);
                            //----------------------------------------------
                            sqlString.displayAlert(this, strMsg);
                            btnOTP.Visible = false;
                            divotp.Visible = true;
                            btnSubmit.Visible = true;
                        }
                        else {
                            ErrField += "- Invalid mobile number format.";
                        }
                    }
                }
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (RequestSubmit_Validation())
            {
                wwdb db = new wwdb();
                MemberController MC = new MemberController();
                MemberModel mm = GetMemberModel();
                if (MC.AddNewUser(ref mm))
                {
                    if (mm.UploadDocument != string.Empty)
                    {
                        string newPath = "/Upload/Registraion/";
                        newPath += mm.MemberId + "/";
                        if (!Directory.Exists(Server.MapPath("~" + newPath)))
                            Directory.CreateDirectory(Server.MapPath("~" + newPath));

                        FileInfo FromFile = new FileInfo(mm.UploadDocument);
                        string toFilePath = newPath + FromFile.Name;
                        FromFile.MoveTo(Server.MapPath("~" + toFilePath));
                        string sql = "UPDATE tbl_MemberInfo SET UploadDoucment ='" + toFilePath + "' WHERE MemberId = '" + mm.MemberId + "'";
                        db.Execute(sql.ToString());
                    }
                    AddFreeRequesttoUser(mm.MemberId);
                    sqlString.displayAlert2(this, Resources.resource.Action_Successful, "Login.aspx");
                }
                else
                    sqlString.displayAlert2(this, Resources.resource.unknownError);

            }
        }

        protected string GenerateOTP(string userName, string email)
        {
            Random generator = new Random();
            String r = generator.Next(0, 999999).ToString("D6");
            wwdb db = new wwdb();
            db.OpenTable("select top 1 1 from tbl_OneTimePasscode where username=N'" + secure.RC(userName) + "' AND Email='" + secure.RC(email) + "'");
            if (db.RecordCount() > 0)
            {
                db.Execute("DELETE FROM tbl_OneTimePasscode WHERE username ='" + secure.RC(userName) + "'");
                db.Execute("DELETE FROM tbl_OneTimePasscode WHERE ExpiryAt <'" + DateTime.Now + "'");
            }
            else
            {
                db.Execute("INSERT INTO tbl_OneTimePasscode(PassCode,UserName,Email,ExpiryAt) VALUES('" + r + "','" + secure.RC(userName) + "','" + secure.RC(email) + "','" + DateTime.Now.AddMinutes(10) + "')");
            }
            return r;
        }

        protected bool ValidateOTP(string otp, string userName)
        {
            String NewLine = "<br/>";
            String ErrMsg = Resources.resource.InputRequiredField;
            String ErrField = String.Empty;


            String UserName = this.txtUserName.Text.Trim();
            String Email = this.txtEmail.Text.Trim();
            String OTP = this.txtotp.Text.Trim();

            if (String.IsNullOrEmpty(UserName))
            { ErrField += "-" + Resources.resource.Username + "." + NewLine; }

            if (String.IsNullOrEmpty(OTP))
            { ErrField += "-" + Resources.resource.OTP + NewLine; }

            if (OTP != null && otp != string.Empty)
            {
                wwdb db = new wwdb();
                db.OpenTable("select top 1 1 from tbl_OneTimePasscode where username=N'" + secure.RC(userName) + "' AND Email = '" + secure.RC(Email) + "' AND PassCode='" + secure.RC(otp) + "' AND ExpiryAt > '" + DateTime.Now + "'");
                if (db.RecordCount() <= 0)
                {
                    ErrField += "-" + Resources.resource.OTP_Incorrect + NewLine;
                }
            }

            if (String.IsNullOrEmpty(ErrField))
            { return true; }
            else
            {
                string strMsg = (ErrMsg + ErrField).Replace("<br/>", "\\n");
                //Show error Message ---------------------------                      
                this.LblError.Text = errormessage.AlertText(ErrMsg + ErrField);
                //----------------------------------------------
                sqlString.displayAlert(this, strMsg);
                return false;
            }

        }

        private MemberModel GetMemberModel()
        {
            List<string> role = new List<string>();
            string branchId = null;
            wwdb db = new wwdb();
            DataTable dt = db.getDataTable("select RoleID, (SELECT top 1 BranchID from tbl_branch where isdefault =1 and isdeleted = 0) AS BranchId from tbl_RoleControl where IsDefault = 1 AND IsDeleted = 0");
            if (dt.Rows.Count > 0)
            {
                role = dt.AsEnumerable().Select(_ => _.Field<string>("RoleId")).ToList();
                branchId = dt.AsEnumerable().Select(_ => _.Field<string>("BranchId")).FirstOrDefault();
            }

            MemberModel mm = new MemberModel();
            mm.Username = txtUserName.Text;
            mm.Password = txtPassword.Text;
            mm.RoleID = role[0];
            mm.UserRole = role;
            mm.UploadDocument = uploadImage;
            mm.Mobile = txtUserName.Text; //txtMobile.Text;
            mm.Fullname = txtName.Text;
            mm.Email = txtEmail.Text;
            mm.BranchList.BranchID = branchId;
            mm.IsChangedPassword = true;
            return mm;
        }
        //protected void BtnSubmit_Click(object sender, EventArgs e)
        //{
        //    if (this.Submit_Validation())
        //    {
        //        //Send Reset password Link To Member
        //        if (this.Send_ResetLink())
        //        {
        //            String Email_Val = this.HFEmail.Value;
        //            String Complete_Msg = Resources.resource.ForgotPassword_Complete;
        //            this.LblResetComplete.Text = Complete_Msg.Replace("[EMAIL]", Email_Val);

        //            //this.Pnl_ResetComplete.Visible = true;

        //            //this.Pnl_ResetPassword.Visible = false;
        //        }
        //    }
        //}

        //protected void IBtnRefresh_Click(object sender, ImageClickEventArgs e)
        //{
        //    this.LoadCaptcha();
        //}

        #endregion

        //protected void BtnBack_Click(object sender, EventArgs e)
        //{
        //    Response.Redirect("/Form/Login.aspx");
        //}
    }
}