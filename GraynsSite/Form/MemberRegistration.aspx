﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Login.Master" CodeBehind="MemberRegistration.aspx.cs" Inherits="WMElegance.Form.MemberRegistration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../js/jquery-1.7.1.min.js"></script>
    <script>

        $(window).resize(function () {

            //centerContent();

        });

        function FileUpload(fuid) {
            var uploadControl = "";
            if (fuid == "fu1") {
                uploadControl = document.getElementById('<%= fu1.ClientID %>');
            }
            var maxsize = 5242889;
            var filesize = uploadControl.files[0].size;
            if (filesize >= maxsize) {
                if (fuid == "fu1") {
                    document.getElementById('<%= fu1.ClientID %>').value = '';
                }
                alert("File is Too large... Maximum allowed file size is 5 MB");
            }
            else {

                $('#<%= hid_FUID.ClientID %>').val(fuid);
                $('#<%= btnUpload.ClientID %>').click();
            }
        }

        function centerContent() {
            var container = $(window);
            var content = $('#slick .register-form');
            content.css("left", (container.width() - content.width()) / 2);
            content.css("top", (container.height() - content.height()) / 2);
        }

        $(document).ready(function () {
            var w = $(window).width();

        });
    </script>
    <style type="text/css">
        body {
            background-image: url('/img/login-bg.jpg');
            position: relative;
            width: 100%;
            overflow: hidden;
            background-position: center left;
            z-index: 1;
            background-repeat: no-repeat;
            background-size: 95%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="page-2">
        <section id="slick">
            <div class="register-form" style="width: 100%;" id="actual" runat="server">
                <div class="col-md-12">
                    <p class="image" style="width: 100%; margin: 20px auto 0;">
                        <img src="../Images/Company_Logo_Big.png" />
                    </p>

                    <p class="intro" style="text-align: center; color: #222; font-weight: 700; font-size: 16px;">
                        <% = Synergy.Util.KeyVal.ProjectName %>
                    </p>

                    <p style="text-align: center; margin-bottom: 15px;"><%= GetGlobalResourceObject("resource", "Registration")%></p>
                </div>
                <div id="divLogin" class="col-md-9">
                    <div>
                        <div class="field">
                            <asp:TextBox ID="txtUserName" runat="server" placeholder="<%$ Resources:Resource, Contact_Number%>" CssClass="form-control" MaxLength="50" data-rule-required="true" TabIndex="1"></asp:TextBox>
                            <%--<span class="entypo-user icon"></span>--%>
                            <span class="entypo-mobile icon"></span>
                            <span class="slick-tip left"><%= GetGlobalResourceObject("resource", "Contact_Number")%></span>
                        </div>
                    </div>
                    <div class="field">
                        <asp:TextBox ID="txtName" runat="server" placeholder="<%$ Resources:Resource, FullName%>" CssClass="form-control" MaxLength="50" data-rule-required="true" TabIndex="1"></asp:TextBox>
                        <span class="entypo-user icon"></span>
                        <span class="slick-tip left"><%= GetGlobalResourceObject("resource", "FullName")%></span>
                    </div>

                    <div class="field">
                        <asp:TextBox ID="txtEmail" runat="server" placeholder="<%$ Resources:Resource, Email%>" CssClass="form-control" MaxLength="50"></asp:TextBox>
                        <span class="entypo-mail icon"></span>
                        <span class="slick-tip left"><%= GetGlobalResourceObject("resource", "Email")%></span>
                    </div>
<%--                    <div class="field">
                        <asp:TextBox ID="txtMobile" runat="server" placeholder="<%$ Resources:Resource, ContactNumber%>" CssClass="form-control" MaxLength="50"></asp:TextBox>
                        <span class="entypo-mobile icon"></span>
                        <span class="slick-tip left"><%= GetGlobalResourceObject("resource", "ContactNumber")%></span>
                    </div>--%>
                    <div class="field">
                        <asp:TextBox ID="txtPassword" runat="server" placeholder="<%$ Resources:Resource, Password%>" CssClass="form-control" TextMode="Password" MaxLength="50"></asp:TextBox>
                        <span class="entypo-lock icon"></span>
                        <span class="slick-tip left"><%= GetGlobalResourceObject("resource", "Password")%></span>
                    </div>
                    <div class="field">
                        <asp:TextBox ID="txtConfirmPassword" runat="server" placeholder="<%$ Resources:Resource, confirm_Password%>" CssClass="form-control" TextMode="Password" MaxLength="50"></asp:TextBox>
                        <span class="entypo-lock icon"></span>
                        <span class="slick-tip left"><%= GetGlobalResourceObject("resource", "confirm_Password")%></span>
                    </div>
                    <table>
                        <tr>
                            <td style="width: 95%">
                                <div class="field" style="width: 80%">
                                    <asp:FileUpload runat="server" ID="fu1" onchange="FileUpload('fu1')" />
                                    <span class="entypo-upload icon"></span>
                                    <span class="slick-tip left"><%= GetGlobalResourceObject("resource", "Upload_Document")%></span>
                                </div>
                            </td>
                            <td style="width: 5%; vertical-align: top">
                                <asp:Image runat="server" ID="img_fu1" ImageUrl="/Images/Doc_Icon.png" Width="50px" Height="40px" Visible="false" />
                                <asp:Button runat="server" ID="btnUpload" Style="display: none;" OnClick="btnUpload_Click" />
                                <asp:HiddenField runat="server" ID="hid_FUID" />
                            </td>
                        </tr>
                    </table>
                    <div id="divotp" runat="server" visible="false">
                        <div class="field" style="margin-bottom: 5px">
                            <asp:TextBox ID="txtotp" runat="server" placeholder="<%$ Resources:Resource, OTP%>" CssClass="form-control" MaxLength="6"></asp:TextBox>
                            <span class="entypo-key icon"></span>
                            <span class="slick-tip left"><%= GetGlobalResourceObject("resource", "OTP")%></span>
                        </div>
                        <asp:Label ID="Label1" CssClass="form-control" Font-Size="Smaller" runat="server" Text="I don't receive a code."></asp:Label><asp:LinkButton ID="LinkButton1" runat="server" Font-Underline="true" OnClick="btnOTP_Click" ForeColor="Blue" CssClass="form-control">Resend</asp:LinkButton>
                    </div>
                <div class="field" style="margin-top: 15px">
                    <asp:Button ID="btnSubmit" Text="<%$ Resources:Resource, Submit%>" runat="server" Visible="false" class="send" OnClick="btnSubmit_Click" />
                    <asp:Button ID="btnOTP" Text="<%$ Resources:Resource, OTP_Request%>" class="send" runat="server" UseSubmitBehavior="true" ValidationGroup="g1" OnClick="btnOTP_Click" />
                </div>

            </div>
    </div>
    <div style="color: white; position: relative; float: left; font-size: 11px; margin: auto; padding: 0px;">
        <asp:Label ID="LblError" runat="server" Text="" ForeColor="Red"></asp:Label>
    </div>

    </section>
    </div>

    <%--<aside class="right-side">
        <section class="content-header">
            <h1>
                <span class="icon fa fa-fw fa-book" aria-hidden="true"></span>
                <asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><span runat="server" id="spHeader"><%= GetGlobalResourceObject("resource", "Member_Registration")%></span></asp:Label>
            </h1>
            <asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
        </section>
        <section class="content">
            <div class="col-md-12">
                <div runat="server">
                    <div class="box">
                        <h4>
                            <asp:Label ID="panel" runat="server" Text="<%$ Resources:Resource, Register_New_Member%>"></asp:Label>

                        </h4>

                        <div id="divErrorMessage" runat="server" visible="false" class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <b><%= GetGlobalResourceObject("resource", "Alert")%></b>
                            <br />
                            <asp:Label runat="server" ID="lblErr"></asp:Label>
                        </div>
                        <div class="box-body">



                            <table class="table table-bordered">
                                <tr class="form-group">
                                    <td><%= GetGlobalResourceObject("resource", "Username")%><span style="color: Red;">&nbsp;*</span></td>
                                    <td class="controls">
                                        <asp:TextBox runat="server" ID="txtUserName" CssClass="form-control" AutoPostBack="true" OnTextChanged="txtUserName_TextChanged"> </asp:TextBox>

                                    </td>

                                </tr>

                                <tr class="form-group">
                                    <td><%= GetGlobalResourceObject("resource", "Password")%><span style="color: Red;">&nbsp;*</span></td>
                                    <td class="controls">
                                        <asp:TextBox runat="server" ID="txtPassword" CssClass="form-control" TextMode="Password"> </asp:TextBox>

                                    </td>

                                </tr>


                                <tr class="form-group">
                                    <td><%= GetGlobalResourceObject("resource", "FullName")%><span style="color: Red;">&nbsp;*</span></td>
                                    <td class="controls">
                                        <asp:TextBox runat="server" ID="txtName" CssClass="form-control"> </asp:TextBox>

                                    </td>

                                </tr>


                                <tr class="form-group">
                                    <td><%= GetGlobalResourceObject("resource", "ContactNumber")%><span style="color: Red;">&nbsp;*</span></td>
                                    <td class="controls">
                                        <asp:TextBox runat="server" ID="txtMobile" CssClass="form-control"> </asp:TextBox>

                                    </td>

                                </tr>



                                <tr class="form-group">
                                    <td><%= GetGlobalResourceObject("resource", "Email")%><span style="color: Red;">&nbsp;*</span></td>
                                    <td class="controls">
                                        <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control"> </asp:TextBox>

                                    </td>

                                </tr>


                            </table>

                        </div>

                        <div class="box-footer">

                            <asp:Button ID="btnSubmit" Text="<%$ Resources:Resource, Submit%>" runat="server" CssClass="btn btn-primary" OnClientClick="return confirmSubmit(); " OnClick="btnSubmit_Click" />

                        </div>
                    </div>
                </div>
                <asp:Literal ID="ltlCount" runat="server" Text="0" Visible="false" />
                <asp:Literal ID="ltlRemoved" runat="server" Visible="false" />
            </div>

        </section>
    </aside>--%>
    <script src="../Vielee/login/js/demo-2.js"></script>
</asp:Content>
