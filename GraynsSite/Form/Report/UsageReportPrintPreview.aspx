﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UsageReportPrintPreview.aspx.cs" Inherits="HJT.Form.Report.UsageReportPrintPreview" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
        .mygrdContent td {
            padding: 10px;
        }

        .mygrdContent th {
            padding: 10px;
        }

        .header {
            background-color: #fff;
            font-family: Arial;
            color: #000;
            border-top: solid 1px grey;
            border-bottom: solid 1px grey;
            border-left: none 0px transparent;
            border-right: none 0px transparent;
            height: 25px;
            text-align: left;
            font-size: 16px;
        }

        .rows {
            background-color: #fff;
            font-family: Arial;
            font-size: 14px;
            color: #000;
            min-height: 25px;
            text-align: left;
            border: none 0px transparent;
        }

        .rowBlock {
            display: flex;
            width: 800px;
            padding: 10px;
            margin: 10px auto;
        }

        .fixPrintBackground {
            -webkit-print-color-adjust: exact;
        }

        @page {
            size: auto; /* auto is the initial value */
            margin: 10mm; /* this affects the margin in the printer settings */
        }

        html {
            background-color: #FFFFFF;
            margin: 0px; /* this affects the margin on the html before sending to printer */
        }

        .table tbody tr td {
            padding: 0 8px;
        }

        .text-center {
            text-align: center !important;
        }

        .center-align {
            margin-left: auto;
            margin-right: auto;
        }

        .td-padding tr td {
            padding: 15px 0 0;
        }

        @media print {
            #btnPrint {
                display: none;
            }
        }

        .logo {
            height: 80px;
            /*margin-top: -40px;
            background: white;
            padding-left: 15px;
            padding-right: 15px;*/
        }
    </style>
    <script>
        function printPage() {
            var printButton = document.getElementById("<%= btnPrint.ClientID %>");
            printButton.style.visibility = 'hidden';
            window.print();
        }
    </script>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="center-align text-center">
            <br />
              <div class="center-align" style="border-style: none; width: 800px; margin: 10px auto;">
                <table style="width: 100%; text-align: left;" class="mygrdContent">
                    <tr class="rows">
                        <td style="width: 15%;">
                            <asp:Literal runat="server" Text="<%$ Resources:Resource, Usage_Report %>"></asp:Literal>
                        </td>
                    </tr>
                    <tr style="height:30px"/>
                    <tr class="rows">
                        <td colspan="6" style="width: 15%; position:center; text-align:center !important">
                            <asp:Label runat="server" ID="lblParentBranch" Font-Size="24px" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr style="height:40px"/>
                    <tr class="rows">
                        <td style="width: 15%;"><asp:Literal runat="server" Text="<%$ Resources:Resource, From%>" />
                        </td>
                        <td style="width: 5%;">:
                        </td>
                        <td style="width: 30%;">
                            <asp:Label runat="server" ID="lblFrom"></asp:Label>
                        </td>
                        <td style="width: 15%;"><asp:Literal runat="server" Text="<%$ Resources:Resource, To%>" />
                        </td>
                        <td style="width: 5%;">:
                        </td>
                        <td style="width: 30%;">
                            <asp:Label runat="server" ID="lblTo"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="center-align" style="border-style: none; width: 800px; margin: 10px auto;">
                <asp:GridView ID="gv" runat="server" AutoGenerateColumns="false" GridLines="None"
                    EmptyDataText="<%$ Resources:Resource, No_Record_Found%>" CssClass="mygrdContent" HeaderStyle-CssClass="header" 
                    RowStyle-CssClass="rows" Width="100%" >
                    <Columns>
                        <asp:TemplateField HeaderText="<%$ Resources:Resource, Index_No%>">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="<%$ Resources:Resource, Parent_Company %>" DataField="ParentName" />
                        <asp:BoundField HeaderText="<%$ Resources:Resource, Project_Name%>" DataField="ProjectName" />
                        <asp:BoundField HeaderText="<%$ Resources:Resource, Green%>" DataField="Green" />
                        <asp:BoundField HeaderText="<%$ Resources:Resource, Yellow%>" DataField="Yellow" />
                        <asp:BoundField HeaderText="<%$ Resources:Resource, Red%>" DataField="Red" />
                        <asp:BoundField HeaderText="<%$ Resources:Resource, Total_Usage%>" DataField="Total" />
                    </Columns>
                </asp:GridView>
            </div>
            <div class="center-align text-center" style="width:800px">
                <table style="width: 100%; text-align: left;" class="mygrdContent">
                    <tr class="rows">
                        <td style="width: 25%"/>
                        <td style="width: 65%; text-align:right; font-size:18px; font-weight:600">
                            <asp:Literal runat="server" Text="<%$ Resources:Resource, Grand_Total %>"/>:
                        </td>
                        <td style="width: 10%;">
                            <asp:Label runat="server" ID="lblGrandTotal" Font-Size="18px" Font-Underline="true" Font-Bold="true"/>
                        </td>
                    </tr>
                </table>
            </div>
            <asp:Button runat="server" ID="btnPrint" Text="Print" OnClientClick="printPage();" />
            <asp:Button runat="server" ID="btnExport" Text="<%$ Resources:Resource, Export %>" OnClick="btnExport_Click" />
        </div>
    </form>
</body>
</html>
