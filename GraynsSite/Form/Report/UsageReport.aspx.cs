﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using Synergy.Helper;
using Synergy.Model;
using Synergy.Util;

namespace HJT.Form.Report
{
    public partial class UsageReport : System.Web.UI.Page
    {
        LoginUserModel login_user = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];
            if (!IsPostBack)
            {
                CheckACL();
                binddata();
                ddlParentBranch.SelectedIndex = 0;
                //sqlString.bindControl(gv1, getParentBranchUsage(null));
                //sqlString.bindControl(gv2, getSubBranchUsage(null));
                //sqlString.bindControl(gv3, getProjectUsage(null));
            }
            resetTotalCount();
            sqlString.bindControl(gv1, getParentBranchUsage(null));
            sqlString.bindControl(gv2, getSubBranchUsage(null));
            sqlString.bindControl(gv3, getProjectUsage(null));
            bindCharts(null, null, null);

            if (gv1.Rows.Count > 0)
                gridView.ApplyPaging(gv1, -1);
            if (gv2.Rows.Count > 0)
                gridView.ApplyPaging(gv2, -1);
            if (gv3.Rows.Count > 0)
                gridView.ApplyPaging(gv3, -1);
        }
        private void resetTotalCount()
        {
            litTotal1.Text = Resources.resource.Total + ": 0";
            litTotal2.Text = Resources.resource.Total + ": 0";
            litTotal3.Text = Resources.resource.Total + ": 0";
            hfTotal1.Value = "0";
            hfTotal2.Value = "0";
            hfTotal3.Value = "0";
        }
        protected void CheckACL()
        {
            wwdb db = new wwdb();
            string sql = "";
            sql = string.Format(@" SELECT * FROM tbl_roleMenu a WITH (NOLOCK) 
		                                    WHERE a.MenuID = 60 AND a.roleID IN ({0})", string.Join(",", login_user.UserRole));

            db.OpenTable(sql);
            db.Close();

            if (db.RecordCount() > 0)
            {
                //gv.Columns[8].Visible = true;
                gv1.Visible = true;
                gv2.Visible = true;
                gv3.Visible = true;
            }
            else
            {
                //gv.Columns[8].Visible = false;
                gv1.Visible = false;
                gv2.Visible = false;
                gv3.Visible = false;
            }
        }
        private void binddata()
        {
            #region Controls
            if (ddlParentBranch.SelectedIndex.Equals(-1))
            {
                StringBuilder sql = new StringBuilder();
                sql.Append("select distinct b.BranchName as '0', b.ID as '1' from tbl_Branch b where b.IsDeleted=0 and b.ParentId is null ");
                if (!login_user.isStaffAdmin())
                    sql.Append("and (b.id= '" + login_user.BranchUniqueID + "' or b.parentid = '" + login_user.BranchUniqueID + "') ");
                sql.Append("order by [0]");
                sqlString.bindControl(ddlParentBranch, sql.ToString(), "0", "1", true);
            }

            if (!login_user.isStaffAdmin())
                ddlParentBranch.SelectedIndex = 1;

            if (ddlParentBranch.SelectedIndex >= 0 || ddlSubBranch.SelectedIndex.Equals(-1))
            {
                StringBuilder sqlSubBranch = new StringBuilder();
                sqlSubBranch.Clear();
                sqlSubBranch.Append(@"
                    select distinct s.BranchName as '0', s.ID as '1'
                    from tbl_Branch pa with (nolock), tbl_Branch s with (nolock)
                    where s.ParentId is not null and pa.ParentId is null 
                ");
                if (ddlParentBranch.SelectedIndex > 0)
                {
                    sqlSubBranch.Append("AND s.ParentId = pa.ID "); //must have parentbranch.id
                    sqlSubBranch.Append(sqlString.searchDropDownList("pa.ID", ddlParentBranch, true, true, false));
                }
                sqlSubBranch.Append(@"
                    and pa.IsDeleted = 0 and pa.Status = 'A'
                    and s.IsDeleted = 0 and s.Status = 'A' and s.parentid = pa.id 
                ");
                if (!login_user.isStaffAdmin())
                    sqlSubBranch.Append("AND (pa.BranchID = " + login_user.BranchID + " OR s.BranchID = " + login_user.BranchID + ") ");
                sqlSubBranch.Append("order by s.BranchName ");
                sqlString.bindControl(ddlSubBranch, sqlSubBranch.ToString(), "0", "1", true);
            }

            if (ddlParentBranch.SelectedIndex >= 0 || ddlSubBranch.SelectedIndex >= 0 || ddlProjectName.SelectedIndex.Equals(-1))
            {
                StringBuilder sqlProject = new StringBuilder();
                sqlProject.Clear();
                sqlProject.Append(@"
                    select distinct ps.ProjectName as '0', ps.ID as '1'
                    from tbl_ProjectSettings ps, tbl_Branch b
                    where ps.IsDeleted = 0 and ps.Status = 'A'
                    and b.IsDeleted = 0 and b.Status = 'A' 
                ");
                if (ddlParentBranch.SelectedIndex > 0)
                {
                    sqlProject.Append("and (b.ParentId = N'" + ddlParentBranch.SelectedValue + "' ");
                    sqlProject.Append(sqlString.searchDropDownList("b.ID", ddlParentBranch, true, false, true) + ")");
                }
                if (ddlSubBranch.SelectedIndex > 0)
                    sqlProject.Append(sqlString.searchDropDownList("s.ID", ddlSubBranch, true, true, false));
                sqlProject.Append(@"
                    and (ps.BranchId = b.ID or ps.BranchId = b.ParentId)
                    ORDER BY ps.ProjectName 
                ");
                sqlString.bindControl(ddlProjectName, sqlProject.ToString(), "0", "1", true);
            }
            #endregion

            #region Gridview
            //wwdb db = new wwdb();
            //DataTable dt = db.getDataTable(getParentBranchUsage(null));
            //gv1.DataSource = dt;
            //gv1.DataBind();
            //Literal litTotal = (Literal)gv1.BottomPagerRow.FindControl("litTotal");
            //if (litTotal != null)
            //    if (string.IsNullOrEmpty(litTotal.Text))
            //        litTotal.Text = Resources.resource.Total + " : " + dt.Rows.Count.ToString("N0");

            //dt = db.getDataTable(getSubBranchUsage(null));
            //gv2.DataSource = dt;
            //gv2.DataBind();
            //litTotal = (Literal)gv2.BottomPagerRow.FindControl("litTotal");
            //if (litTotal != null)
            //    if (string.IsNullOrEmpty(litTotal.Text))
            //        litTotal.Text = Resources.resource.Total + " : " + dt.Rows.Count.ToString("N0");

            //dt = db.getDataTable(getProjectUsage(null));
            //gv3.DataSource = dt;
            //gv3.DataBind();
            //litTotal = (Literal)gv3.BottomPagerRow.FindControl("litTotal");
            //if (litTotal != null)
            //    if (string.IsNullOrEmpty(litTotal.Text))
            //        litTotal.Text = Resources.resource.Total + " : " + dt.Rows.Count.ToString("N0");
            #endregion

            bindCharts(null, null, null);
        }

        #region Charts
        private void bindCharts(IDictionary<string, string> sortBy1, IDictionary<string, string> sortBy2, IDictionary<string, string> sortBy3)
        {
            bindParentChart(sortBy1);
            bindBranchChart(sortBy2);
            bindProjectChart(sortBy3);
        }
        private void bindParentChart(IDictionary<string, string> sortBy)
        {
            wwdb db = new wwdb();
            DataTable dt = db.getDataTable(getParentBranchUsage(sortBy, true));
            if (dt != null && dt.Rows.Count > 0)
            {
                chartParentUsage.DataSource = dt;
                chartParentUsage.DataBind();
                chartSetting(chartParentUsage);
                ChartArea chartArea = chartParentUsage.ChartAreas.FindByName("chartAreaParentUsage");
                chartAreaSetting(chartArea);
                chartSeriesSetting(chartParentUsage.Series);
            }
            else
                chartParentUsage.Visible = false;
        }
        private void bindBranchChart(IDictionary<string, string> sortBy)
        {
            wwdb db = new wwdb();
            DataTable dt = db.getDataTable(getSubBranchUsage(sortBy, true));
            if (dt != null && dt.Rows.Count > 0)
            {
                chartBranchUsage.DataSource = dt;
                chartBranchUsage.DataBind();
                chartSetting(chartBranchUsage);
                ChartArea chartArea = chartBranchUsage.ChartAreas.FindByName("chartAreaBranchUsage");
                chartAreaSetting(chartArea);
                chartSeriesSetting(chartBranchUsage.Series);
            }
            else
                chartBranchUsage.Visible = false;
        }
        private void bindProjectChart(IDictionary<string, string> sortBy)
        {
            wwdb db = new wwdb();
            DataTable dt = db.getDataTable(getProjectUsage(sortBy, true));
            if (dt != null && dt.Rows.Count > 0)
            {
                chartProjectUsage.DataSource = dt;
                chartProjectUsage.DataBind();
                chartSetting(chartProjectUsage);
                ChartArea chartArea = chartProjectUsage.ChartAreas.FindByName("chartAreaProjectUsage");
                chartAreaSetting(chartArea);
                chartSeriesSetting(chartProjectUsage.Series);
            }
            else
                chartProjectUsage.Visible = false;
        }
        private void chartSetting(Chart chart)
        {
            const int chartWidth = 500;
            const int chartHeight = 500;

            chart.Width = new Unit(chartWidth, UnitType.Pixel);
            chart.Height = new Unit(chartHeight, UnitType.Pixel);
        }
        private void chartAreaSetting(ChartArea chartArea)
        {
            const float fontSize = 10f;
            const string fontFamily = "Verdana";
            const int labelInterval = 1;

            chartArea.AxisX.LabelStyle.Font = new Font(fontFamily, fontSize);
            chartArea.AxisX.LabelStyle.Interval = labelInterval;
            chartArea.AxisX.LabelAutoFitStyle = LabelAutoFitStyles.None;

            //chartArea.AxisY.IntervalAutoMode = IntervalAutoMode.VariableCount;
        }
        private void chartSeriesSetting(SeriesCollection seriesCol)
        {
            for (int count = 0; count < seriesCol.Count(); count++)
            {
                Series series = seriesCol[count];
                series.ChartType = SeriesChartType.StackedBar100;

                //data label
                //series.IsValueShownAsLabel = true;
                for (int dataRow = 0; dataRow < series.Points.Count(); dataRow++)
                {
                    var point = series.Points[dataRow];
                    string value = point.YValues.FirstOrDefault().ToString();
                    if (!value.Equals("0"))
                        point.Label = value;
                }
                switch (count)
                {
                    case 0: //Green
                        series.Name = "Green";
                        series.Color = Color.FromArgb(0, 204, 102);
                        break;
                    case 1: //Yellow
                        series.Name = "Yellow";
                        series.Color = Color.FromArgb(250, 255, 107);
                        break;
                    case 2: //Red
                        series.Name = "Red";
                        series.Color = Color.FromArgb(255, 0, 0);
                        break;
                    default:
                        break;
                }
            }
        }
        #endregion

        #region Old Implementation
        private string getParentBranchUsage2(IDictionary<string, string> sortBy)
        {
            StringBuilder sql = new StringBuilder();
            string dateFilter = "";

            if (!string.IsNullOrWhiteSpace(txtFrom.Text) && !string.IsNullOrWhiteSpace(txtTo.Text) &&
                DateTime.TryParse(txtFrom.Text, out var from) && DateTime.TryParse(txtTo.Text, out var to))
                dateFilter = " and r.RequestAt between '" + txtFrom.Text + "' and '" + txtTo.Text + " 23:59' ";

            sql.Append("SELECT pa.BranchName AS 'Parent Branch Name', ");
            sql.Append("(SELECT count(distinct r.RequestID) ");
            sql.Append("FROM tbl_Request r ");
            sql.Append("WHERE r.BranchID = pa.BranchID and r.isDeleted = 0 and r.StatusID='S' ");
            if (ddlParentBranch.SelectedIndex > 0)
                sql.Append(sqlString.searchDropDownList("pa.ID", ddlParentBranch, true, true, false));
            sql.Append(dateFilter + ") as 'Usage', ");
            sql.Append("count(distinct r.RequestID) as 'Total', ");
            sql.Append("(SELECT count(distinct r.RequestID) ");
            sql.Append("FROM tbl_Request r ");
            sql.Append("WHERE r.BranchID = pa.BranchID and r.isDeleted = 0 and r.StatusID='S' ");
            if (ddlParentBranch.SelectedIndex > 0)
                sql.Append(sqlString.searchDropDownList("pa.ID", ddlParentBranch, true, true, false));
            sql.Append("and r.ApplicationResult = 'A' ) as 'Green', ");
            sql.Append("(SELECT count(distinct r.RequestID) ");
            sql.Append("FROM tbl_Request r ");
            sql.Append("WHERE r.BranchID = pa.BranchID and r.isDeleted = 0 and r.StatusID='S' ");
            if (ddlParentBranch.SelectedIndex > 0)
                sql.Append(sqlString.searchDropDownList("pa.ID", ddlParentBranch, true, true, false));
            sql.Append("and r.ApplicationResult = 'M' ) as 'Yellow', ");
            sql.Append("(SELECT count(distinct r.RequestID) ");
            sql.Append("FROM tbl_Request r ");
            sql.Append("WHERE r.BranchID = pa.BranchID and r.isDeleted = 0 and r.StatusID='S' ");
            if (ddlParentBranch.SelectedIndex > 0)
                sql.Append(sqlString.searchDropDownList("pa.ID", ddlParentBranch, true, true, false));
            sql.Append("and r.ApplicationResult = 'D' ) as 'Red' ");
            sql.Append("FROM tbl_Request r ");
            sql.Append("inner join tbl_ProjectSettings ps on r.ProjectID = ps.ID ");
            sql.Append("inner join tbl_Branch s on (ps.BranchId = s.ID or ps.BranchId = s.ParentId) ");
            sql.Append("right join tbl_Branch pa on pa.ID = s.ParentId where pa.ParentId IS NULL ");
            sql.Append("and pa.IsDeleted = 0 and ps.IsDeleted=0 and s.IsDeleted=0 and r.isDeleted=0 ");
            if (ddlParentBranch.SelectedIndex > 0)
                sql.Append(sqlString.searchDropDownList("pa.ID", ddlParentBranch, true, true, false));
            sql.Append("and r.StatusID='S' ");
            sql.Append("group by pa.BranchName, pa.ID, pa.BranchID ");
            sql.Append("order by ");

            //ordering
            if (sortBy != null && sortBy.Any())
            {
                int i = 0;
                foreach (var item in sortBy)
                {
                    string key = item.Key;
                    string value = item.Value;

                    if (i > 0)
                        sql.Append(",");

                    switch (key)
                    {
                        case "ParentBranchName":
                            sql.Append("[Parent Branch Name] " + value);
                            break;
                        case "ParentUsage":
                            sql.Append("[Usage] " + value);
                            break;
                        case "ParentTotal":
                            sql.Append("[Total] " + value);
                            break;
                        case "ParentGreen":
                            sql.Append("[Green] " + value);
                            break;
                        case "ParentYellow":
                            sql.Append("[Yellow] " + value);
                            break;
                        case "ParentRed":
                            sql.Append("[Red] " + value);
                            break;
                        default:
                            break;
                    }

                    i++;
                }
            }
            else
            {
                if (gv1.Columns[2].Visible.Equals(true))
                    sql.Append("[Total] DESC ");
                else if (gv1.Columns[1].Visible.Equals(true))
                    sql.Append("[Usage] DESC ");
            }

            return sql.ToString();
        }
        private string getSubBranchUsage2(IDictionary<string, string> sortBy)
        {
            StringBuilder sql = new StringBuilder();
            string dateFilter = "";

            if (!string.IsNullOrWhiteSpace(txtFrom.Text) && !string.IsNullOrWhiteSpace(txtTo.Text) &&
                DateTime.TryParse(txtFrom.Text, out var from) && DateTime.TryParse(txtTo.Text, out var to))
                dateFilter = " and tbl_Request.RequestAt between '" + txtFrom.Text + "' and '" + txtTo.Text + " 23:59' ";

            sql.Append("SELECT distinct b.BranchName AS 'Sub Branch Name', ");
            sql.Append("(SELECT count(tbl_Request.RequestID) ");
            sql.Append("FROM tbl_Request ");
            sql.Append("WHERE tbl_Request.BranchID = b.BranchID AND tbl_Request.isdeleted = 0 AND tbl_Request.statusID = 'S' ");
            if (ddlSubBranch.SelectedIndex > 0)
                sql.Append(sqlString.searchDropDownList("b.ID", ddlSubBranch, true, true, false));
            if (ddlParentBranch.SelectedIndex > 0)
                sql.Append(sqlString.searchDropDownList("a.ID", ddlParentBranch, true, true, false));
            sql.Append(") AS 'Usage', ");
            sql.Append("(SELECT count(tbl_Request.RequestID) ");
            sql.Append("FROM tbl_Request ");
            sql.Append("WHERE tbl_Request.BranchID = b.BranchID AND tbl_Request.isdeleted = 0 AND tbl_Request.statusID = 'S' ");
            if (ddlSubBranch.SelectedIndex > 0)
                sql.Append(sqlString.searchDropDownList("b.ID", ddlSubBranch, true, true, false));
            if (ddlParentBranch.SelectedIndex > 0)
                sql.Append(sqlString.searchDropDownList("a.ID", ddlParentBranch, true, true, false));
            sql.Append("and tbl_Request.ApplicationResult = 'A' ) AS 'Green', ");
            sql.Append("(SELECT count(tbl_Request.RequestID) ");
            sql.Append("FROM tbl_Request ");
            sql.Append("WHERE tbl_Request.BranchID = b.BranchID AND tbl_Request.isdeleted = 0 AND tbl_Request.statusID = 'S' ");
            if (ddlSubBranch.SelectedIndex > 0)
                sql.Append(sqlString.searchDropDownList("b.ID", ddlSubBranch, true, true, false));
            if (ddlParentBranch.SelectedIndex > 0)
                sql.Append(sqlString.searchDropDownList("a.ID", ddlParentBranch, true, true, false));
            sql.Append("and tbl_Request.ApplicationResult = 'M' ) AS 'Yellow', ");
            sql.Append("(SELECT count(tbl_Request.RequestID) ");
            sql.Append("FROM tbl_Request ");
            sql.Append("WHERE tbl_Request.BranchID = b.BranchID AND tbl_Request.isdeleted = 0 AND tbl_Request.statusID = 'S' ");
            if (ddlSubBranch.SelectedIndex > 0)
                sql.Append(sqlString.searchDropDownList("b.ID", ddlSubBranch, true, true, false));
            if (ddlParentBranch.SelectedIndex > 0)
                sql.Append(sqlString.searchDropDownList("a.ID", ddlParentBranch, true, true, false));
            sql.Append("and tbl_Request.ApplicationResult = 'D' ) AS 'Red' ");
            sql.Append("FROM tbl_Branch b, tbl_Branch a ");
            sql.Append("WHERE b.parentid IS NOT NULL and b.IsDeleted = 0 and a.ParentId is null and a.IsDeleted = 0 and b.ParentId = a.ID ");
            if (ddlSubBranch.SelectedIndex > 0)
                sql.Append(sqlString.searchDropDownList("b.ID", ddlSubBranch, true, true, false));
            if (ddlParentBranch.SelectedIndex > 0)
                sql.Append(sqlString.searchDropDownList("a.ID", ddlParentBranch, true, true, false));
            //sql.Append("order by[Usage] desc ");

            sql.Append(" order by ");

            //ordering
            if (sortBy != null && sortBy.Any())
            {
                int i = 0;
                foreach (var item in sortBy)
                {
                    string key = item.Key;
                    string value = item.Value;

                    if (i > 0)
                        sql.Append(",");

                    switch (key)
                    {
                        case "SubBranchName":
                            sql.Append("[Sub Branch Name] " + value);
                            break;
                        case "SubUsage":
                            sql.Append("[Usage] " + value);
                            break;
                        case "SubGreen":
                            sql.Append("[Green] " + value);
                            break;
                        case "SubYellow":
                            sql.Append("[Yellow] " + value);
                            break;
                        case "SubRed":
                            sql.Append("[Red] " + value);
                            break;
                        default:
                            break;
                    }

                    i++;
                }
            }
            else
            {
                sql.Append("[Usage] DESC ");
            }

            return sql.ToString();
        }
        private string getProjectUsage2(IDictionary<string, string> sortBy)
        {
            StringBuilder sql = new StringBuilder();
            string dateFilter = "";

            if (!string.IsNullOrWhiteSpace(txtFrom.Text) && !string.IsNullOrWhiteSpace(txtTo.Text) &&
                DateTime.TryParse(txtFrom.Text, out var from) && DateTime.TryParse(txtTo.Text, out var to))
                dateFilter = " and tbl_Request.RequestAt between '" + txtFrom.Text + "' and '" + txtTo.Text + " 23:59' ";

            sql.Append("SELECT distinct p.ProjectName as 'Project Name'  , ");
            sql.Append("(SELECT count(distinct RequestID) ");
            sql.Append("FROM tbl_Request ");
            sql.Append("WHERE tbl_Request.ProjectID = p.ID and tbl_Request.isDeleted = 0 AND tbl_Request.statusID='S' ");

            if (ddlSubBranch.SelectedIndex > 0)
            {
                sql.Append("and p.BranchId = s.ID ");
                sql.Append(sqlString.searchDropDownList("s.ID", ddlSubBranch, true, true, false));
            }
            if (ddlParentBranch.SelectedIndex > 0)
            {
                sql.Append("and s.ParentId = pa.ID ");
                sql.Append(sqlString.searchDropDownList("pa.ID", ddlParentBranch, true, true, false));
            }
            if (ddlProjectName.SelectedIndex > 0)
                sql.Append(sqlString.searchDropDownList("p.ID", ddlProjectName, true, true, false));
            sql.Append(dateFilter + ") ");
            sql.Append("as 'Usage', ");
            sql.Append("(SELECT count(distinct RequestID) ");
            sql.Append("FROM tbl_Request ");
            sql.Append("WHERE tbl_Request.ProjectID = p.ID and tbl_Request.isDeleted = 0 and tbl_Request.StatusID='S' ");
            if (ddlSubBranch.SelectedIndex > 0)
            {
                sql.Append("and p.BranchId = s.ID ");
                sql.Append(sqlString.searchDropDownList("s.ID", ddlSubBranch, true, true, false));
            }
            if (ddlParentBranch.SelectedIndex > 0)
            {
                sql.Append("and s.ParentId = pa.ID ");
                sql.Append(sqlString.searchDropDownList("pa.ID", ddlParentBranch, true, true, false));
            }
            if (ddlProjectName.SelectedIndex > 0)
                sql.Append(sqlString.searchDropDownList("p.ID", ddlProjectName, true, true, false));
            sql.Append("and tbl_Request.ApplicationResult='A') as 'Green', ");
            sql.Append("(SELECT count(distinct RequestID) ");
            sql.Append("FROM tbl_Request ");
            sql.Append("WHERE tbl_Request.ProjectID = p.ID and tbl_Request.isDeleted = 0 and tbl_Request.StatusID='S' ");
            if (ddlSubBranch.SelectedIndex > 0)
            {
                sql.Append("and p.BranchId = s.ID ");
                sql.Append(sqlString.searchDropDownList("s.ID", ddlSubBranch, true, true, false));
            }
            if (ddlParentBranch.SelectedIndex > 0)
            {
                sql.Append("and s.ParentId = pa.ID ");
                sql.Append(sqlString.searchDropDownList("pa.ID", ddlParentBranch, true, true, false));
            }
            if (ddlProjectName.SelectedIndex > 0)
                sql.Append(sqlString.searchDropDownList("p.ID", ddlProjectName, true, true, false));
            sql.Append("and tbl_Request.ApplicationResult='M') as 'Yellow', ");
            sql.Append("(SELECT count(distinct RequestID) ");
            sql.Append("FROM tbl_Request ");
            sql.Append("WHERE tbl_Request.ProjectID = p.ID and tbl_Request.isDeleted = 0 and tbl_Request.StatusID='S' ");
            if (ddlSubBranch.SelectedIndex > 0)
            {
                sql.Append("and p.BranchId = s.ID ");
                sql.Append(sqlString.searchDropDownList("s.ID", ddlSubBranch, true, true, false));
            }
            if (ddlParentBranch.SelectedIndex > 0)
            {
                sql.Append("and s.ParentId = pa.ID ");
                sql.Append(sqlString.searchDropDownList("pa.ID", ddlParentBranch, true, true, false));
            }
            if (ddlProjectName.SelectedIndex > 0)
                sql.Append(sqlString.searchDropDownList("p.ID", ddlProjectName, true, true, false));
            sql.Append("and tbl_Request.ApplicationResult='D') as 'Red' ");
            sql.Append("FROM tbl_ProjectSettings p, tbl_Branch pa, tbl_Branch s ");
            sql.Append("where (p.IsDeleted = 0 and p.ProjectID IS NOT NULL and pa.IsDeleted = 0 and s.IsDeleted = 0 ");
            sql.Append("and pa.ParentId is null and s.ParentId is not null)  and s.ParentId = pa.ID and (p.BranchId = s.ID OR p.BranchId = pa.ID) ");
            if (ddlSubBranch.SelectedIndex > 0)
                sql.Append(sqlString.searchDropDownList("s.ID", ddlSubBranch, true, true, false));
            if (ddlParentBranch.SelectedIndex > 0)
                sql.Append(sqlString.searchDropDownList("pa.ID", ddlParentBranch, true, true, false));
            if (ddlProjectName.SelectedIndex > 0)
                sql.Append(sqlString.searchDropDownList("p.ID", ddlProjectName, true, true, false));
            if ((ddlParentBranch.SelectedIndex.Equals(0)
                && ddlSubBranch.SelectedIndex.Equals(0)
                && ddlProjectName.SelectedIndex.Equals(0)) ||
                (ddlParentBranch.SelectedIndex.Equals(-1)
                && ddlSubBranch.SelectedIndex.Equals(-1)
                && ddlProjectName.SelectedIndex.Equals(-1)))//not binded or not selected
            {
                sql.Append("OR((p.BranchId = '' OR p.BranchId is null) AND p.isDeleted = 0 AND pa.isDeleted = 0 ");
                sql.Append("AND s.isDeleted = 0 and pa.ParentId is null and s.ParentId is not null and p.ProjectID IS NOT NULL) ");
            }

            //sql.Append("order by 'Usage' desc ");

            sql.Append(" order by ");

            //ordering
            if (sortBy != null && sortBy.Any())
            {
                int i = 0;
                foreach (var item in sortBy)
                {
                    string key = item.Key;
                    string value = item.Value;

                    if (i > 0)
                        sql.Append(",");

                    switch (key)
                    {
                        case "ProjectName":
                            sql.Append("Project Name " + value);
                            break;
                        case "Usage":
                            sql.Append("Usage " + value);
                            break;
                        case "ProjectGreen":
                            sql.Append("Green " + value);
                            break;
                        case "ProjectYellow":
                            sql.Append("Yellow " + value);
                            break;
                        case "ProjectRed":
                            sql.Append("Red " + value);
                            break;
                        default:
                            break;
                    }

                    i++;
                }
            }
            else
            {
                sql.Append("[Usage] DESC ");
            }

            return sql.ToString();
        }

        private void bindParentBranchTotalUsage(IDictionary<string, string> sortBy)
        {
            wwdb db = new wwdb();
            wwdb datadb = new wwdb();
            string dateFilter = "";
            StringBuilder sql = new StringBuilder();

            if (!string.IsNullOrWhiteSpace(txtFrom.Text) && !string.IsNullOrWhiteSpace(txtTo.Text) &&
                DateTime.TryParse(txtFrom.Text, out var from) && DateTime.TryParse(txtTo.Text, out var to))
                dateFilter = " and r.RequestAt between '" + txtFrom.Text + "' and '" + txtTo.Text + " 23:59' ";

            sql.Append("select ID from tbl_Branch b where b.ParentId is null and b.IsDeleted=0 ");
            if (ddlParentBranch.SelectedIndex > 0)
                sql.Append(sqlString.searchDropDownList("ID", ddlParentBranch, true, true, false));
            db.OpenTable(sql.ToString());

            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Parent Branch Name", typeof(string)));
            dt.Columns.Add(new DataColumn("Total", typeof(int)));
            dt.Columns.Add(new DataColumn("Other", typeof(int)));
            dt.Columns.Add(new DataColumn("Green", typeof(int)));
            dt.Columns.Add(new DataColumn("Yellow", typeof(int)));
            dt.Columns.Add(new DataColumn("Red", typeof(int)));
            if (db.RecordCount() > 0)
            {
                while (!db.Eof())
                {
                    string ID = "";
                    ID = db.Item("ID");

                    sql.Clear();
                    sql.Append("	with noDupReq as(			");
                    sql.Append("	SELECT distinct cast(RequestID as int) as 'RequestID' ,			");
                    sql.Append("	CASE WHEN r.ProjectID IS NULL THEN '-' ELSE r.ProjectID END AS 'ProjectID' ,			");
                    sql.Append("	CASE WHEN r.BranchID IS NULL or r.BranchID = '' THEN '-' ELSE r.BranchID END AS 'BranchID' ,			");
                    sql.Append("	r.ApplicationResult			");
                    sql.Append("	FROM tbl_Request r, tbl_Branch b, tbl_ProjectSettings ps 			");
                    sql.Append("	where r.isDeleted=0 and b.IsDeleted=0 and StatusID='S' 			");
                    sql.Append("	and (b.ParentId=N'" + ID + "' or b.id=N'" + ID + "') 			" + dateFilter);
                    sql.Append("	and ((r.BranchID = b.BranchID) or (r.ProjectID=ps.ID) 			");
                    sql.Append("	or (r.ProjectID is null and r.BranchID=b.BranchID)) and ps.BranchId=b.ID ) 			");
                    sql.Append("				");
                    sql.Append("	, parentOther as (  			");
                    sql.Append("	select distinct r.RequestID, s.ID as 'SubID',s.BranchID as 'SubBranchID',  pa.ID as 'ParentID', pa.BranchID as 'ParentBranchID', r.ApplicationResult  			");
                    sql.Append("	FROM tbl_Request r, tbl_Branch s, tbl_Branch pa  			");
                    sql.Append("	where ProjectID is null and r.isDeleted=0 and s.IsDeleted=0 and pa.IsDeleted=0 and pa.ParentId is null 			" + dateFilter);
                    sql.Append("	and s.ParentId = pa.ID  and (r.BranchID=s.BranchID or r.BranchID=pa.BranchID) and s.ParentId=N'" + ID + "' )   			");

                    sql.Append("	, countReq as (			");
                    sql.Append("	select distinct			");
                    sql.Append("		(count(RequestID)) as 'Total',		");
                    sql.Append("		(select count(distinct otr.RequestID) from parentOther otr where 1=1) as 'Other',          		");
                    sql.Append("		(select count(tr.requestID)");
                    sql.Append("		from noDupReq tr where tr.ApplicationResult='A' and 1=1) as 'Green',		");
                    sql.Append("		(select count(tr.requestID)");
                    sql.Append("		from noDupReq tr where tr.ApplicationResult='M' and 1=1) as 'Yellow',		");
                    sql.Append("		(select count(tr.requestID)");
                    sql.Append("		from noDupReq tr where tr.ApplicationResult='D' and 1=1) as 'Red'        	");
                    sql.Append("	from noDupReq tr   			");
                    sql.Append("	)        			");
                    sql.Append("				");
                    sql.Append("	select b.BranchName as 'Branch Name', 			");
                    sql.Append("	(sum(cr.Total))			");
                    sql.Append("	as 'Total',        			");
                    sql.Append("	cr.Green, cr.Yellow, cr.Red, cr.Other        			");
                    sql.Append("	from countReq cr, tbl_Branch b   			");
                    sql.Append("	where b.ID = N'" + ID + "'			");
                    sql.Append("	group by b.BranchName, cr.Green, cr.Yellow, cr.Red, cr.Other      			");

                    datadb.OpenTable(sql.ToString());//breakpoint

                    if (datadb.RecordCount() > 0)
                    {
                        DataRow dr = dt.NewRow();
                        dr["Parent Branch Name"] = datadb.Item("Branch Name");
                        dr["Total"] = datadb.Item("Total");
                        dr["Other"] = datadb.Item("Other");
                        dr["Green"] = datadb.Item("Green");
                        dr["Yellow"] = datadb.Item("Yellow");
                        dr["Red"] = datadb.Item("Red");
                        dt.Rows.Add(dr);
                    }
                    db.MoveNext();
                }
            }

            if (sortBy != null && sortBy.Any())
            {
                int i = 0;
                foreach (var item in sortBy)
                {
                    string key = item.Key;
                    string value = item.Value;

                    if (i > 0)
                        sql.Append(",");

                    switch (key)
                    {
                        case "ParentBranchName":
                            dt.DefaultView.Sort = "[Parent Branch Name] " + value;
                            break;
                        case "ParentOther":
                            dt.DefaultView.Sort = "[Other] " + value;
                            break;
                        case "ParentTotal":
                            dt.DefaultView.Sort = "[Total] " + value;
                            break;
                        case "ParentGreen":
                            dt.DefaultView.Sort = "[Green] " + value;
                            break;
                        case "ParentYellow":
                            dt.DefaultView.Sort = "[Yellow] " + value;
                            break;
                        case "ParentRed":
                            dt.DefaultView.Sort = "[Red] " + value;
                            break;
                        default:
                            break;
                    }
                    i++;
                }
            }
            else
            {
                //if (gv1.Columns[2].Visible.Equals(true))
                //    sql.Append("[Total] DESC ");
                //else if (gv1.Columns[1].Visible.Equals(true))
                //    sql.Append("[Usage] DESC ");
                dt.DefaultView.Sort = "[Total] DESC";
            }
            dt = dt.DefaultView.ToTable();

            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            gv1.DataSource = ds.Tables[0];
            gv1.DataBind();
        }
        private void bindProjectTotalUsage(string totalUsage, string otherUsage, IDictionary<string, string> sortBy)
        {
            StringBuilder sql = new StringBuilder();
            wwdb db = new wwdb();

            //int parentGreen = 0, parentYellow = 0, parentRed = 0, parentOther = 0;//temp

            sql.Clear();
            sql.Append(totalUsage);

            db.OpenTable(totalUsage);

            if (db.RecordCount() > 0)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add(new DataColumn("ProjectName", typeof(string)));
                dt.Columns.Add(new DataColumn("Total", typeof(int)));
                dt.Columns.Add(new DataColumn("Green", typeof(int)));
                dt.Columns.Add(new DataColumn("Yellow", typeof(int)));
                dt.Columns.Add(new DataColumn("Red", typeof(int)));

                while (!db.Eof())
                {
                    DataRow dr = dt.NewRow();
                    dr["ProjectName"] = db.Item("ProjectName");
                    dr["Total"] = ConvertHelper.ConvertToInt(db.Item("Total"), 0);
                    dr["Green"] = ConvertHelper.ConvertToInt(db.Item("Green"), 0);
                    dr["Yellow"] = ConvertHelper.ConvertToInt(db.Item("Yellow"), 0);
                    dr["Red"] = ConvertHelper.ConvertToInt(db.Item("Red"), 0);
                    dt.Rows.Add(dr);
                    //parentGreen += ConvertHelper.ConvertToInt(db.Item("Green"), 0);
                    //parentYellow += ConvertHelper.ConvertToInt(db.Item("Yellow"), 0);
                    //parentRed += ConvertHelper.ConvertToInt(db.Item("Red"), 0);
                    db.MoveNext();
                }
                //temp
                //DataTable dt2 = new DataTable();
                //dt2.Columns.Add(new DataColumn("Parent Branch Name", typeof(string)));
                //dt2.Columns.Add(new DataColumn("Total", typeof(int)));
                //dt2.Columns.Add(new DataColumn("Other", typeof(int)));
                //dt2.Columns.Add(new DataColumn("Green", typeof(int)));
                //dt2.Columns.Add(new DataColumn("Yellow", typeof(int)));
                //dt2.Columns.Add(new DataColumn("Red", typeof(int)));

                sql.Clear();
                if (!(ddlProjectName.SelectedIndex > 0))
                {
                    sql.Append(otherUsage);
                    db.OpenTable(sql.ToString());
                    if (db.RecordCount() > 0)
                    {
                        while (!db.Eof())
                        {
                            DataRow dr = dt.NewRow();
                            dr["ProjectName"] = db.Item("ProjectName");
                            dr["Total"] = ConvertHelper.ConvertToInt(db.Item("Total"), 0);
                            dr["Green"] = ConvertHelper.ConvertToInt(db.Item("Green"), 0);
                            dr["Yellow"] = ConvertHelper.ConvertToInt(db.Item("Yellow"), 0);
                            dr["Red"] = ConvertHelper.ConvertToInt(db.Item("Red"), 0);
                            dt.Rows.Add(dr);
                            //parentGreen += ConvertHelper.ConvertToInt(db.Item("Green"), 0);
                            //parentYellow += ConvertHelper.ConvertToInt(db.Item("Yellow"), 0);
                            //parentRed += ConvertHelper.ConvertToInt(db.Item("Red"), 0);
                            //parentOther += ConvertHelper.ConvertToInt(db.Item("Total"), 0);
                            db.MoveNext();
                        }

                        if (ddlParentBranch.SelectedIndex <= 0 && ddlProjectName.SelectedIndex <= 0)
                        {
                            sql.Clear();
                            sql.Append("	with easyMode as ( 		");
                            sql.Append("		select distinct r.RequestID, r.ApplicationResult 	");
                            sql.Append("		FROM tbl_Request r	");
                            sql.Append("		where ProjectID is null and r.ProjectName='EasyMode'	");
                            sql.Append("		and r.isDeleted=0 and r.ReportType='L'	");
                            sql.Append("	) 		");
                            sql.Append("	select distinct 'EasyMode' as 'ProjectName', count(distinct RequestID) as 'Total', 		");
                            sql.Append("	(SELECT count(distinct RequestID) FROM easyMode WHERE ApplicationResult='A') AS 'Green', 		");
                            sql.Append("	(SELECT count(distinct RequestID) FROM easyMode WHERE ApplicationResult='M') AS 'Yellow', 		");
                            sql.Append("	(SELECT count(distinct RequestID) FROM easyMode WHERE ApplicationResult='D') AS 'Red' 		");
                            sql.Append("	from easyMode		");
                            sql.Append("	where 1=" + (ddlParentBranch.SelectedIndex <= 0 ? "1" : "0") +
                                " AND 1=" + (ddlProjectName.SelectedIndex <= 0 ? "1" : "0"));
                            db.OpenTable(sql.ToString());
                            if (db.RecordCount() > 0)
                            {
                                while (!db.Eof())
                                {
                                    DataRow dr = dt.NewRow();
                                    dr["ProjectName"] = db.Item("ProjectName");
                                    dr["Total"] = ConvertHelper.ConvertToInt(db.Item("Total"), 0);
                                    dr["Green"] = ConvertHelper.ConvertToInt(db.Item("Green"), 0);
                                    dr["Yellow"] = ConvertHelper.ConvertToInt(db.Item("Yellow"), 0);
                                    dr["Red"] = ConvertHelper.ConvertToInt(db.Item("Red"), 0);
                                    dt.Rows.Add(dr);
                                    db.MoveNext();
                                }
                            }
                        }
                    }
                }
                //temp
                //StringBuilder parentName = new StringBuilder();
                //parentName.Append("SELECT BranchName FROM");
                //DataRow dr2 = dt2.NewRow();
                //dr2["Parent Branch Name"] = 
                //dr2["Total"] = parentGreen + parentYellow + parentRed;
                //dr2["Other"] = parentOther;
                //dr2["Green"] = parentGreen;
                //dr2["Yellow"] = parentYellow;
                //dr2["Red"] = parentRed;
                //dt2.Rows.Add(dr2);

                if (sortBy != null && sortBy.Any())
                {
                    int i = 0;
                    foreach (var item in sortBy)
                    {
                        string key = item.Key;
                        string value = item.Value;

                        if (i > 0)
                            sql.Append(",");

                        switch (key)
                        {
                            case "ProjectName":
                                dt.DefaultView.Sort = "[ProjectName] " + value;
                                break;
                            case "ProjectUsage":
                                dt.DefaultView.Sort = "[Total] " + value;
                                break;
                            case "ProjectGreen":
                                dt.DefaultView.Sort = "[Green] " + value;
                                break;
                            case "ProjectYellow":
                                dt.DefaultView.Sort = "[Yellow] " + value;
                                break;
                            case "ProjectRed":
                                dt.DefaultView.Sort = "[Red] " + value;
                                break;
                            default:
                                break;
                        }

                        i++;
                    }
                }
                else
                {
                    dt.DefaultView.Sort = "[Total] DESC";
                }
                dt = dt.DefaultView.ToTable();

                DataSet ds = new DataSet();
                ds.Tables.Add(dt);
                gv3.DataSource = ds.Tables[0];
                gv3.DataBind();
            }
        }
        private string getProjectTotalUsage()
        {
            StringBuilder sql = new StringBuilder();
            string cte = "", dateFilter = "";

            if (!string.IsNullOrWhiteSpace(txtFrom.Text) && !string.IsNullOrWhiteSpace(txtTo.Text) &&
                DateTime.TryParse(txtFrom.Text, out var from) && DateTime.TryParse(txtTo.Text, out var to))
                dateFilter = " and r.RequestAt between '" + txtFrom.Text + "' and '" + txtTo.Text + " 23:59' ";

            cte += "with cte as( ";
            cte += "SELECT distinct ps.ProjectName, ps.ID, ps.BranchId, s.ParentId, ";
            cte += "(select count(distinct r.requestID) ";
            cte += "from tbl_Request r ";
            cte += "where r.ProjectID = ps.ID and r.isDeleted=0 and r.StatusID='S' ";
            if (!dateFilter.Equals(""))
                cte += dateFilter;
            cte += ") as 'Total', ";
            cte += "(select count(distinct r.requestID) ";
            cte += "from tbl_Request r ";
            cte += "where r.ProjectID = ps.ID and r.isDeleted=0 and r.StatusID='S' ";
            cte += "and r.ApplicationResult='A' ";
            if (!dateFilter.Equals(""))
                cte += dateFilter;
            cte += ") as 'Green', ";
            cte += "(select count(distinct r.requestID) ";
            cte += "from tbl_Request r ";
            cte += "where r.ProjectID = ps.ID and r.isDeleted=0 and r.StatusID='S' ";
            cte += "and r.ApplicationResult='M' ";
            if (!dateFilter.Equals(""))
                cte += dateFilter;
            cte += ") as 'Yellow', ";
            cte += "(select count(distinct r.requestID) ";
            cte += "from tbl_Request r ";
            cte += "where r.ProjectID = ps.ID and r.isDeleted=0 and r.StatusID='S' ";
            cte += "and r.ApplicationResult='D' ";
            if (!dateFilter.Equals(""))
                cte += dateFilter;
            cte += ") as 'Red' ";
            cte += "from tbl_ProjectSettings ps, tbl_Request r, tbl_Branch s ";
            cte += "where ps.IsDeleted=0 and r.isDeleted=0 and s.IsDeleted=0 and r.StatusID='S' ";
            cte += "and ((ps.ID=r.ProjectID and ps.BranchId=s.ID) or (r.BranchID = s.BranchID and s.ID=ps.BranchId)) ";
            if (!dateFilter.Equals(""))
                cte += dateFilter;
            if (ddlProjectName.SelectedIndex > 0)
                cte += sqlString.searchDropDownList("ps.ID", ddlProjectName, true, true, false);
            if (ddlParentBranch.SelectedIndex > 0)
            {
                cte += "AND (s.ParentID = N'" + ddlParentBranch.SelectedValue.ToString() + "'";
                cte += sqlString.searchDropDownList("s.ID", ddlParentBranch, true, false, true);
                cte += ") ";
            }
            cte += ") ";

            sql.Append(cte);
            sql.Append("select ProjectName, Total, Green, Yellow, Red ");
            sql.Append("from cte ");
            sql.Append("where 1=1 ");
            sql.Append("order by ProjectName ");

            return sql.ToString();
        }
        private string getOtherProjectUsage()
        {
            StringBuilder sql = new StringBuilder();
            string parentOther = "", dateFilter = "";

            if (!string.IsNullOrWhiteSpace(txtFrom.Text) && !string.IsNullOrWhiteSpace(txtTo.Text) &&
                DateTime.TryParse(txtFrom.Text, out var from) && DateTime.TryParse(txtTo.Text, out var to))
                dateFilter = " and r.RequestAt between '" + txtFrom.Text + "' and '" + txtTo.Text + " 23:59' ";

            parentOther += "with parentOther as ( ";
            parentOther += "select distinct r.RequestID, s.ID as 'SubID',s.BranchID as 'SubBranchID', pa.ID as 'ParentID', pa.BranchID as 'ParentBranchID', r.ApplicationResult ";
            parentOther += "FROM tbl_Request r, tbl_Branch s, tbl_Branch pa ";
            parentOther += "where ProjectID is null and r.isDeleted=0 and s.IsDeleted=0 and pa.IsDeleted=0 ";
            parentOther += "and pa.ParentId is null and r.ReportType = 'B' ";
            if (ddlParentBranch.SelectedIndex > 0)
                parentOther += sqlString.searchDropDownList("s.ParentId", ddlParentBranch, true, true, false);
            parentOther += "and s.ParentId = pa.ID and (r.BranchID=s.BranchID or r.BranchID=pa.BranchID) ";
            if (!dateFilter.Equals(""))
                parentOther += dateFilter;
            parentOther += ") ";

            sql.Append(parentOther);
            sql.Append("select distinct 'Others' as 'ProjectName', ");
            sql.Append("count(distinct RequestID) as 'Total', ");
            sql.Append("(SELECT count(distinct RequestID) ");
            sql.Append("FROM parentOther ");
            sql.Append("WHERE ApplicationResult='A') AS 'Green', ");
            sql.Append("(SELECT count(distinct RequestID) ");
            sql.Append("FROM parentOther ");
            sql.Append("WHERE ApplicationResult='M') AS 'Yellow', ");
            sql.Append("(SELECT count(distinct RequestID) ");
            sql.Append("FROM parentOther ");
            sql.Append("WHERE ApplicationResult='D') AS 'Red' ");
            sql.Append("from parentOther ");

            return sql.ToString();
        }

        #endregion

        #region Get Data
        private string getAllBranchUsage()
        {
            StringBuilder sql = new StringBuilder();

            string dateFilter = "";

            if (!string.IsNullOrWhiteSpace(txtFrom.Text) && !string.IsNullOrWhiteSpace(txtTo.Text) &&
                DateTime.TryParse(txtFrom.Text, out _) && DateTime.TryParse(txtTo.Text, out _))
                dateFilter = " and {0}.RequestAt between '" + txtFrom.Text + "' and '" + txtTo.Text + " 23:59' ";

            sql.Clear();
            sql.AppendFormat(@"
                with allBranch as (
                    select b.ID, b.BranchName as 'MainName', b.ParentId, pa.BranchName as 'ParentName', 
		            count(*) as 'Total', count(r.ProjectID) as 'NotOther',
	                    (select count(tr.RequestID) from tbl_Request tr with (nolock)
	                    inner join tbl_Branch tb with (nolock) on tb.BranchID = tr.BranchID
	                    where tr.ApplicationResult = 'A' and tr.isDeleted = 0 and tb.IsDeleted = 0
	                    and tb.ID = b.ID and tr.StatusID = 'S' {0}) 
                    as 'Green',
	                    (select count(*) from tbl_Request tr with (nolock)
	                    inner join tbl_Branch tb with (nolock) on tb.BranchID = tr.BranchID
	                    where tr.ApplicationResult = 'M' and tr.isDeleted = 0 and tb.IsDeleted = 0
	                    and tb.ID = b.ID and tr.StatusID = 'S' {0}) 
                    as 'Yellow',
	                    (select count(*) from tbl_Request tr with (nolock)
	                    inner join tbl_Branch tb with (nolock) on tb.BranchID = tr.BranchID
	                    where tr.ApplicationResult = 'D' and tr.isDeleted = 0 and tb.IsDeleted = 0
	                    and tb.ID = b.ID and tr.StatusID = 'S' {0}) 
                    as 'Red'
                    from tbl_Request r with (nolock) 
                    inner join tbl_Branch b with (nolock) on b.BranchID = r.BranchID
                    left join tbl_Branch pa with (nolock) on pa.ID = b.ParentId
                    where r.isDeleted = 0 and b.IsDeleted = 0 and r.StatusID = 'S' {1}
                    group by b.ID, b.BranchName, b.ParentId, pa.BranchName
                )

                --select * from allBranch
                ----where ParentId = 1013 or ID=1013
                --order by MainName
            ", string.Format(dateFilter, "tr")
            , string.Format(dateFilter, "r"));

            return sql.ToString();
        }
        private string getParentBranchUsage(IDictionary<string, string> sortBy, bool inverse = false)
        {
            StringBuilder sql = new StringBuilder();
            string addZeroBranch = @"
                union 
                select b.BranchName, 0, '0', 0, 0, 0 
                from tbl_Branch b with (nolock) 
                where b.IsDeleted = 0 and b.ParentId is null 
                and b.ID not in (select ab1.ID 
			                from allBranch ab1
			                union
			                select ab2.ParentId 
			                from allBranch ab2)
            ";

            sql.Clear();
            sql.Append(getAllBranchUsage());
            sql.AppendFormat(@"
                select isnull(pa.MainName, (select b.BranchName from tbl_branch b with (nolock) where b.ID = sub.ParentId)) as 'ParentBranchName', 
	                (ISNULL(pa.Total, 0) + ISNULL(SUM(sub.Total), 0)) as 'Total1',
                    (cast((ISNULL(pa.Total, 0) + ISNULL(SUM(sub.Total), 0)) as varchar) + ' (' + 
	                cast(isnull(pa.Total, 0) as varchar) + ' + ' + 
	                cast(isnull(sum(sub.Total), 0) as varchar) + ')') as 'Total', 
                    (ISNULL(pa.Green, 0) + ISNULL(SUM(sub.Green), 0)) as 'Green',
                    (ISNULL(pa.Yellow, 0) + ISNULL(SUM(sub.Yellow), 0)) as 'Yellow',
                    (ISNULL(pa.Red, 0) + ISNULL(SUM(sub.Red), 0)) as 'Red'
                from allBranch sub
                full join allBranch pa on sub.ParentId = pa.ID
                where pa.ParentId is null and isnull(pa.MainName, (select b.BranchName from tbl_branch b with (nolock) where b.ID = sub.ParentId)) is not null {0}
                group by pa.MainName, sub.ParentId, pa.Total, pa.Green, pa.Yellow, pa.Red, pa.Total

                {1}
            ", ddlParentBranch.SelectedIndex > 0 ?
                (sqlString.searchDropDownList("(pa.ID", ddlParentBranch, true, true, false) +
                sqlString.searchDropDownList("sub.ParentId", ddlParentBranch, true, false, true) + ")") : ""
            , (!string.IsNullOrWhiteSpace(txtFrom.Text) && !string.IsNullOrWhiteSpace(txtTo.Text) &&
                DateTime.TryParse(txtFrom.Text, out _) && DateTime.TryParse(txtTo.Text, out _)) ? "" : addZeroBranch);

            sql.Append(" order by ");
            if (sortBy != null && sortBy.Any())
            {
                int i = 0;
                foreach (var item in sortBy)
                {
                    string key = item.Key;
                    string value = item.Value;
                    if(inverse)
                        switch (value)
                        {
                            case "DESC":
                                value = "ASC";
                                break;
                            case "ASC":
                                value = "DESC";
                                break;
                            default:
                                break;
                        }

                    if (i > 0)
                        sql.Append(",");

                    switch (key)
                    {
                        case "ParentBranchName":
                            sql.Append("[Parent Branch Name] " + value);
                            break;
                        case "ParentTotal":
                            sql.Append("[Total1] " + value);
                            break;
                        case "ParentGreen":
                            sql.Append("[Green] " + value);
                            break;
                        case "ParentYellow":
                            sql.Append("[Yellow] " + value);
                            break;
                        case "ParentRed":
                            sql.Append("[Red] " + value);
                            break;
                        default:
                            break;
                    }
                    i++;
                }
            }
            else
                sql.Append("[Total1] " + (inverse ? "ASC" : "DESC"));

            return sql.ToString();
        }
        private string getSubBranchUsage(IDictionary<string, string> sortBy, bool inverse = false)
        {
            StringBuilder sql = new StringBuilder();

            string addZeroBranch = @"
                union
                select b.BranchName, 0, '0', 0, 0, 0, 0 
                from tbl_Branch b with (nolock)
                where b.IsDeleted = 0 and b.ParentId is not null 
                and b.ID not in (select ab1.ID 
			                from allBranch ab1
			                union
			                select ab2.ParentId 
			                from allBranch ab2)
            ";

            sql.Clear();
            sql.Append(getAllBranchUsage());
            sql.AppendFormat(@"
                select sub.MainName as 'BranchName', 
	                ISNULL(SUM(sub.Total), 0) as 'Total1', 
	                cast(ISNULL(SUM(sub.Total), 0) as varchar) + ' (' +
	                cast(ISNULL(SUM(sub.NotOther), 0) as varchar) + ' + ' +
	                cast((ISNULL(SUM(sub.Total), 0) - ISNULL(SUM(sub.NotOther), 0)) as varchar) + ')' as 'Total',
	                (ISNULL(SUM(sub.Total), 0) - ISNULL(SUM(sub.NotOther), 0)) as 'Other',
	                ISNULL(SUM(sub.Green), 0) as 'Green',
	                ISNULL(SUM(sub.Yellow), 0) as 'Yellow',
	                ISNULL(SUM(sub.Red), 0) as 'Red'
                from allBranch sub
                where sub.ParentId is not null {0} {1}
                group by sub.MainName

                {2}
            ", sqlString.searchDropDownList("sub.ID", ddlSubBranch, true, true, false)
            , (ddlParentBranch.SelectedIndex > 0) ?
                (sqlString.searchDropDownList("(sub.ID", ddlParentBranch, true, true, false) +
                    sqlString.searchDropDownList("sub.ParentId", ddlParentBranch, true, false, true) + ")") : ""
            , (!string.IsNullOrWhiteSpace(txtFrom.Text) && !string.IsNullOrWhiteSpace(txtTo.Text) &&
                DateTime.TryParse(txtFrom.Text, out _) && DateTime.TryParse(txtTo.Text, out _)) ? "" : addZeroBranch);

            sql.Append(" order by ");
            //ordering
            if (sortBy != null && sortBy.Any())
            {
                int i = 0;
                foreach (var item in sortBy)
                {
                    string key = item.Key;
                    string value = item.Value;
                    if (inverse)
                        switch (value)
                        {
                            case "DESC":
                                value = "ASC";
                                break;
                            case "ASC":
                                value = "DESC";
                                break;
                            default:
                                break;
                        }

                    if (i > 0)
                        sql.Append(",");

                    switch (key)
                    {
                        case "SubBranchName":
                            sql.Append("[BranchName] " + value);
                            break;
                        case "SubUsage":
                            sql.Append("[Total1] " + value);
                            break;
                        case "SubGreen":
                            sql.Append("[Green] " + value);
                            break;
                        case "SubYellow":
                            sql.Append("[Yellow] " + value);
                            break;
                        case "SubRed":
                            sql.Append("[Red] " + value);
                            break;
                        default:
                            break;
                    }

                    i++;
                }
            }
            else
                sql.Append("[Total1] " + (inverse ? "ASC" : "DESC"));

            return sql.ToString();
        }
        private string getProjectUsage(IDictionary<string, string> sortBy, bool inverse = false)
        {
            StringBuilder sql = new StringBuilder();

            string dateFilter = "";

            if (!string.IsNullOrWhiteSpace(txtFrom.Text) && !string.IsNullOrWhiteSpace(txtTo.Text) &&
                DateTime.TryParse(txtFrom.Text, out _) && DateTime.TryParse(txtTo.Text, out _))
                dateFilter = " and {0}.RequestAt between '" + txtFrom.Text + "' and '" + txtTo.Text + " 23:59' ";

            sql.Clear();
            sql.AppendFormat(@"
                with allProject as (
                    select ps.ID as 'PID', ps.ProjectName, b.ID as 'BID', 
					isnull(b.BranchName, 'All Agency') as 'BranchName', 
	                b.ParentId, pa.BranchName as 'ParentName', 
	                    count(*) as 'Total',
	                    (select count(*) 
	                    from tbl_Request tr with (nolock)
	                    inner join tbl_ProjectSettings tps with (nolock) on tr.ProjectID = tps.ID
	                    where tr.ApplicationResult = 'A' and tps.ID = ps.ID and tr.isDeleted = 0 {3}) 
                    as 'Green',
	                    (select count(*) 
	                    from tbl_Request tr with (nolock)
	                    inner join tbl_ProjectSettings tps with (nolock) on tr.ProjectID = tps.ID
	                    where tr.ApplicationResult = 'M' and tps.ID = ps.ID and tr.isDeleted = 0 {3}) 
                    as 'Yellow',
	                    (select count(*) 
	                    from tbl_Request tr with (nolock)
	                    inner join tbl_ProjectSettings tps with (nolock) on tr.ProjectID = tps.ID
	                    where tr.ApplicationResult = 'D' and tps.ID = ps.ID and tr.isDeleted = 0 {3}) 
                    as 'Red'
                    from tbl_Request r with (nolock)
                    inner join tbl_ProjectSettings ps with (nolock) on r.ProjectID = ps.ID
                    left join tbl_Branch b with (nolock) on ps.BranchId = b.ID
                    left join tbl_Branch pa with (nolock) on pa.ID = b.ParentId
                    where r.isDeleted = 0 and ps.IsDeleted = 0 and r.StatusID = 'S' {4}
                    group by ps.ID, ps.ProjectName, b.ID, b.BranchName, b.ParentId, pa.BranchName, ps.ID
                )

                select ap.ProjectName, ap.Total, ap.Green, ap.Yellow, ap.Red 
                from allProject ap
                where 1=1 {0} {1} {2}

                ----To display all 0 usage projects
                --union
                --select ps.ProjectName, '0', 0, 0, 0 
                --from tbl_ProjectSettings ps with (nolock)
                --where ps.IsDeleted = 0 
                --and ps.ID not in (select ap1.PID 
                --	from allProject ap1)
            ", (ddlParentBranch.SelectedIndex > 0) ?
                sqlString.searchDropDownList("(ap.ParentId", ddlParentBranch, true, true, false) +
                    sqlString.searchDropDownList("ap.BID", ddlParentBranch, true, false, true) + ")" : ""
            , sqlString.searchDropDownList("ap.BID", ddlSubBranch, true, true, false)
            , sqlString.searchDropDownList("ap.PID", ddlProjectName, true, true, false)
            , string.Format(dateFilter, "tr")
            , string.Format(dateFilter, "r"));

            sql.Append(" order by ");
            //ordering
            if (sortBy != null && sortBy.Any())
            {
                int i = 0;
                foreach (var item in sortBy)
                {
                    string key = item.Key;
                    string value = item.Value;
                    if (inverse)
                        switch (value)
                        {
                            case "DESC":
                                value = "ASC";
                                break;
                            case "ASC":
                                value = "DESC";
                                break;
                            default:
                                break;
                        }

                    if (i > 0)
                        sql.Append(",");

                    switch (key)
                    {
                        case "ProjectName":
                            sql.Append("[BranchName] " + value);
                            break;
                        case "ProjectUsage":
                            sql.Append("[Total] " + value);
                            break;
                        case "ProjectGreen":
                            sql.Append("[Green] " + value);
                            break;
                        case "ProjectYellow":
                            sql.Append("[Yellow] " + value);
                            break;
                        case "ProjectRed":
                            sql.Append("[Red] " + value);
                            break;
                        default:
                            break;
                    }

                    i++;
                }
            }
            else
                sql.Append("[Total] " + (inverse ? "ASC" : "DESC"));

            return sql.ToString();
        }
        #endregion

        #region Controls
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> sortBy1 = new Dictionary<string, string>();
            Dictionary<string, string> sortBy2 = new Dictionary<string, string>();
            Dictionary<string, string> sortBy3 = new Dictionary<string, string>();
            if (ViewState["SortBy1"] != null)
            { sortBy1 = (Dictionary<string, string>)ViewState["SortBy1"]; }
            if (ViewState["SortBy2"] != null)
            { sortBy2 = (Dictionary<string, string>)ViewState["SortBy2"]; }
            if (ViewState["SortBy3"] != null)
            { sortBy3 = (Dictionary<string, string>)ViewState["SortBy3"]; }

            if (ddlProjectName.SelectedIndex.Equals(0))
            {
                if (ddlSubBranch.SelectedIndex > 0)
                    divParentBranch.Visible = false;
                else
                    divParentBranch.Visible = true;
            }

            if (!login_user.isStaffAdmin())
                if (ddlParentBranch.SelectedIndex.Equals(0))
                    ddlParentBranch.SelectedIndex = 1;
            hfTotal1.Value = "0";
            hfTotal2.Value = "0";
            hfTotal3.Value = "0";
            sqlString.bindControl(gv1, getParentBranchUsage(sortBy1));
            sqlString.bindControl(gv2, getSubBranchUsage(sortBy2));
            sqlString.bindControl(gv3, getProjectUsage(sortBy3));
            bindCharts(sortBy1, sortBy2, sortBy3);

            if (gv1.Rows.Count > 0)
                gridView.ApplyPaging(gv1, -1);
            if (gv2.Rows.Count > 0)
                gridView.ApplyPaging(gv2, -1);
            if (gv3.Rows.Count > 0)
                gridView.ApplyPaging(gv3, -1);
        }
        protected void btnPrintPreview_Click(object sender, EventArgs e)
        {
            string reportIrl = "/Form/Report/UsageReportPrintPreview.aspx?from=" + sqlString.encryptURL(txtFrom.Text) +
                "&to=" + sqlString.encryptURL(txtTo.Text) + "&parentbranchid=" +
                sqlString.encryptURL(ddlParentBranch.SelectedValue) + "&subbranchid=" +
                sqlString.encryptURL(ddlSubBranch.SelectedValue) + "&projectname=" +
                sqlString.encryptURL(ddlProjectName.Items[ddlProjectName.SelectedIndex].Text) + "&projectid=" +
                sqlString.encryptURL(ddlProjectName.SelectedValue) + "&parentbranch=" +
                sqlString.encryptURL(ddlParentBranch.Items[ddlParentBranch.SelectedIndex].Text) + "&subbranch=" +
                sqlString.encryptURL(ddlSubBranch.Items[ddlSubBranch.SelectedIndex].Text);

            sqlString.OpenNewWindow_Center(reportIrl, "PrintPreview", 800, 600, this);
        }

        protected void ddlSubBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            StringBuilder sqlProject = new StringBuilder();
            sqlProject.Clear();
            sqlProject.AppendFormat(@"
                select distinct p.ProjectName as '0', p.ID as '1' 
                from tbl_ProjectSettings p, tbl_Branch s, tbl_Branch pa 
                where p.IsDeleted = 0 and p.Status = 'A' 
                and s.IsDeleted = 0 and s.Status = 'A' 
                and pa.IsDeleted = 0 and pa.Status = 'A' 
                and s.ParentId is not null and pa.ParentId is null 
                {0}
                {1}
                and (p.BranchId = s.id OR p.BranchId = pa.id) 
                {2}
                ORDER BY p.ProjectName
            ", ddlParentBranch.SelectedIndex > 0 ? sqlString.searchDropDownList("pa.ID", ddlParentBranch, true, true, false) : ""
            , ddlSubBranch.SelectedIndex > 0 ? sqlString.searchDropDownList("s.ID", ddlSubBranch, true, true, false) : ""
            , !login_user.isStaffAdmin() ? string.Format(@"AND(s.BranchID = '{0}' OR pa.BranchId = '{0}')", login_user.BranchID) : "");

            sqlString.bindControl(ddlProjectName, sqlProject.ToString(), "0", "1", true);

            //if (ddlSubBranch.SelectedIndex > 0)
            //    divParentBranch.Visible = false;
            //else
            //    divParentBranch.Visible = true;
        }
        protected void ddlProjectName_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddlProjectName.SelectedIndex > 0)
            //{
            //    divParentBranch.Visible = false;
            //    divSubBranch.Visible = false;
            //}
            //else
            //{
            //    if (ddlSubBranch.SelectedIndex > 0)
            //        divParentBranch.Visible = false;
            //    else
            //        divParentBranch.Visible = true;
            //    divSubBranch.Visible = true;
            //}
        }
        protected void ddlParentBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            StringBuilder sqlSubBranch = new StringBuilder();
            if (!login_user.isStaffAdmin())
                ddlParentBranch.SelectedIndex = 1;
            sqlSubBranch.Clear();
            sqlSubBranch.AppendFormat(@"
                select distinct b.BranchName as '0', b.ID as '1'
                from tbl_Branch a, tbl_Branch b
                where b.ParentId is not null and a.ParentId is null 
                {0}
                and a.IsDeleted = 0 and a.Status = 'A'
                and b.IsDeleted = 0 and b.Status = 'A'
                {1}
                order by b.BranchName 
            ", ddlParentBranch.SelectedIndex > 0 ? "AND b.ParentId = a.ID " + sqlString.searchDropDownList("a.ID", ddlParentBranch, true, true, false) : ""
            , !login_user.isStaffAdmin() ? string.Format(@"AND (a.BranchID = '{0}' OR b.BranchID = '{0}')", login_user.BranchID) : "");

            sqlString.bindControl(ddlSubBranch, sqlSubBranch.ToString(), "0", "1", true);

            StringBuilder sqlProject = new StringBuilder();
            sqlProject.Clear();
            sqlProject.AppendFormat(@"
                select distinct p.ProjectName as '0', p.ID as'1'
                from tbl_ProjectSettings p, tbl_Branch s, tbl_Branch pa
                where p.IsDeleted = 0 and p.Status = 'A'
                and s.IsDeleted = 0 and s.Status = 'A'
                and pa.IsDeleted = 0 and pa.Status = 'A'
                and s.ParentId is not null and pa.ParentId is null 
                {0}
                {1}
                and (p.BranchId = s.id OR p.BranchId = pa.id) 
                {2}
                ORDER BY p.ProjectName
            ", ddlParentBranch.SelectedIndex > 0 ?
                string.Format(@"and ((s.ParentId = pa.ID {0}) or ( p.BranchID = N'{1}'))
                ", sqlString.searchDropDownList("pa.ID", ddlParentBranch, true, true, false)
                , ddlParentBranch.SelectedValue.ToString()) : ""
            , ddlSubBranch.SelectedIndex > 0 ? sqlString.searchDropDownList("s.ID", ddlSubBranch, true, true, false) : ""
            , !login_user.isStaffAdmin() ? string.Format(@"AND (s.BranchID = '{0}' OR pa.BranchId = '{0}')", login_user.BranchID) : "");

            sqlString.bindControl(ddlProjectName, sqlProject.ToString(), "0", "1", true);
        }
        #endregion

        #region GridView
        //gv
        protected void gv1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                LinkButton sortLink;

                Dictionary<string, string> sortBy1 = new Dictionary<string, string>();
                if (ViewState["SortBy1"] != null)
                { sortBy1 = (Dictionary<string, string>)ViewState["SortBy1"]; }

                // GridView rows with sortable columns will have a linkbutton
                // generated. Compare to the CommandArgument since this is what
                // we set our sortColumn based on.
                foreach (System.Web.UI.WebControls.TableCell currCell in e.Row.Cells)
                {
                    if (currCell.HasControls())
                    {
                        sortLink = ((LinkButton)currCell.Controls[0]);
                        if (sortBy1.ContainsKey(sortLink.CommandArgument))
                        {
                            // Use the HTML safe codes for the up arrow ▲ and down arrow ▼.
                            string sortArrow = sortBy1[sortLink.CommandArgument] == "ASC" ? "&#9650;" : "&#9660;";

                            sortLink.Text = sortLink.Text + " " + sortArrow;
                        }
                        else
                        {
                            sortLink.Text = sortLink.Text;
                        }
                    }
                }
            }
            else if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int total = ConvertHelper.ConvertToInt(DataBinder.Eval(e.Row.DataItem, "Total1").ToString(), 0);
                total += ConvertHelper.ConvertToInt(hfTotal1.Value, 0);
                hfTotal1.Value = total.ToString();
                litTotal1.Text = Resources.resource.Total + ": " + total.ToString();
            }
        }
        protected void gv1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Dictionary<string, string> sortBy1 = new Dictionary<string, string>();
            Dictionary<string, string> sortBy2 = new Dictionary<string, string>();
            Dictionary<string, string> sortBy3 = new Dictionary<string, string>();
            if (ViewState["SortBy1"] != null)
            { sortBy1 = (Dictionary<string, string>)ViewState["SortBy1"]; }
            if (ViewState["SortBy2"] != null)
            { sortBy2 = (Dictionary<string, string>)ViewState["SortBy2"]; }
            if (ViewState["SortBy3"] != null)
            { sortBy3 = (Dictionary<string, string>)ViewState["SortBy3"]; }

            gv1.PageIndex = e.NewPageIndex;
            hfTotal1.Value = "0";
            sqlString.bindControl(gv1, getParentBranchUsage(sortBy1));
            bindCharts(sortBy1, sortBy2, sortBy3);

            if (gv1.Rows.Count > 0)
                gridView.ApplyPaging(gv1, e.NewPageIndex);
        }
        protected void gv1_Sorting(object sender, GridViewSortEventArgs e)
        {
            string expression = e.SortExpression;
            string direction = null;

            //3 state sorting
            //ASC,DESC,null
            if (GetGridViewSortDirection(expression) == "ASC")
            {
                SetGridViewSortDirection(expression, "DESC");
                direction = "DESC";
            }
            else if (GetGridViewSortDirection(expression) == "DESC")
            {
                SetGridViewSortDirection(expression, null);
                direction = null;
            }
            else if (GetGridViewSortDirection(expression) == null)
            {
                SetGridViewSortDirection(expression, "ASC");
                direction = "ASC";
            }

            Dictionary<string, string> sortBy1 = new Dictionary<string, string>();
            if (direction != null)
                sortBy1.Add(e.SortExpression, direction);

            ViewState["SortBy1"] = sortBy1;
            Dictionary<string, string> sortBy2 = new Dictionary<string, string>();
            Dictionary<string, string> sortBy3 = new Dictionary<string, string>();
            if (ViewState["SortBy2"] != null)
            { sortBy2 = (Dictionary<string, string>)ViewState["SortBy2"]; }
            if (ViewState["SortBy3"] != null)
            { sortBy3 = (Dictionary<string, string>)ViewState["SortBy3"]; }

            hfTotal1.Value = "0";
            sqlString.bindControl(gv1, getParentBranchUsage(sortBy1));
            bindCharts(sortBy1, sortBy2, sortBy3);

            if (gv1.Rows.Count > 0)
                gridView.ApplyPaging(gv1, -1);
        }
        protected void gv1_DataBound(object sender, EventArgs e)
        {
            //litTotal.Text = gv.Rows.Count.ToString("N0"); //doesn't work while paging
            //Literal litTotal = (Literal)gv1.BottomPagerRow.FindControl("litTotal");
            //if (litTotal != null)
            //    if (string.IsNullOrEmpty(litTotal.Text))
            //    {
            //        wwdb db = new wwdb();
            //        litTotal.Text = Resources.resource.Total + " : " + db.getDataTable(getParentBranchUsage(null)).Rows.Count.ToString("N0");
            //    }
        }

        protected void gv2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                LinkButton sortLink;

                Dictionary<string, string> sortBy2 = new Dictionary<string, string>();
                if (ViewState["SortBy2"] != null)
                { sortBy2 = (Dictionary<string, string>)ViewState["SortBy2"]; }

                // GridView rows with sortable columns will have a linkbutton
                // generated. Compare to the CommandArgument since this is what
                // we set our sortColumn based on.
                foreach (System.Web.UI.WebControls.TableCell currCell in e.Row.Cells)
                {
                    if (currCell.HasControls())
                    {
                        sortLink = ((LinkButton)currCell.Controls[0]);
                        if (sortBy2.ContainsKey(sortLink.CommandArgument))
                        {
                            // Use the HTML safe codes for the up arrow ▲ and down arrow ▼.
                            string sortArrow = sortBy2[sortLink.CommandArgument] == "ASC" ? "&#9650;" : "&#9660;";

                            sortLink.Text = sortLink.Text + " " + sortArrow;
                        }
                        else
                        {
                            sortLink.Text = sortLink.Text;
                        }
                    }
                }
            }
            else if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int total = ConvertHelper.ConvertToInt(DataBinder.Eval(e.Row.DataItem, "Total1").ToString(), 0);
                total += ConvertHelper.ConvertToInt(hfTotal2.Value, 0);
                hfTotal2.Value = total.ToString();
                litTotal2.Text = Resources.resource.Total + ": " + total.ToString();
            }
        }
        protected void gv2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Dictionary<string, string> sortBy1 = new Dictionary<string, string>();
            Dictionary<string, string> sortBy2 = new Dictionary<string, string>();
            Dictionary<string, string> sortBy3 = new Dictionary<string, string>();
            if (ViewState["SortBy1"] != null)
            { sortBy1 = (Dictionary<string, string>)ViewState["SortBy1"]; }
            if (ViewState["SortBy2"] != null)
            { sortBy2 = (Dictionary<string, string>)ViewState["SortBy2"]; }
            if (ViewState["SortBy3"] != null)
            { sortBy3 = (Dictionary<string, string>)ViewState["SortBy3"]; }

            gv2.PageIndex = e.NewPageIndex;
            hfTotal2.Value = "0";
            sqlString.bindControl(gv2, getSubBranchUsage(sortBy2));
            bindCharts(sortBy1, sortBy2, sortBy3);

            if (gv2.Rows.Count > 0)
                gridView.ApplyPaging(gv2, e.NewPageIndex);
        }
        protected void gv2_Sorting(object sender, GridViewSortEventArgs e)
        {
            string expression = e.SortExpression;
            string direction = null;

            //3 state sorting
            //ASC,DESC,null
            if (GetGridViewSortDirection(expression) == "ASC")
            {
                SetGridViewSortDirection(expression, "DESC");
                direction = "DESC";
            }
            else if (GetGridViewSortDirection(expression) == "DESC")
            {
                SetGridViewSortDirection(expression, null);
                direction = null;
            }
            else if (GetGridViewSortDirection(expression) == null)
            {
                SetGridViewSortDirection(expression, "ASC");
                direction = "ASC";
            }

            Dictionary<string, string> sortBy2 = new Dictionary<string, string>();
            if (direction != null)
                sortBy2.Add(e.SortExpression, direction);

            ViewState["SortBy2"] = sortBy2;
            Dictionary<string, string> sortBy1 = new Dictionary<string, string>();
            Dictionary<string, string> sortBy3 = new Dictionary<string, string>();
            if (ViewState["SortBy1"] != null)
            { sortBy1 = (Dictionary<string, string>)ViewState["SortBy1"]; }
            if (ViewState["SortBy3"] != null)
            { sortBy3 = (Dictionary<string, string>)ViewState["SortBy3"]; }

            hfTotal2.Value = "0";
            sqlString.bindControl(gv2, getSubBranchUsage(sortBy2));
            bindCharts(sortBy1, sortBy2, sortBy3);

            if (gv2.Rows.Count > 0)
                gridView.ApplyPaging(gv2, -1);
        }
        protected void gv2_DataBound(object sender, EventArgs e)
        {
            //litTotal.Text = gv.Rows.Count.ToString("N0"); //doesn't work while paging
            //Literal litTotal = (Literal)gv1.BottomPagerRow.FindControl("litTotal");
            //if (litTotal != null)
            //    if (string.IsNullOrEmpty(litTotal.Text))
            //    {
            //        wwdb db = new wwdb();
            //        litTotal.Text = Resources.resource.Total + " : " + db.getDataTable(getSubBranchUsage(null)).Rows.Count.ToString("N0");
            //    }
        }

        protected void gv3_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                LinkButton sortLink;

                Dictionary<string, string> sortBy3 = new Dictionary<string, string>();
                if (ViewState["SortBy3"] != null)
                { sortBy3 = (Dictionary<string, string>)ViewState["SortBy3"]; }

                // GridView rows with sortable columns will have a linkbutton
                // generated. Compare to the CommandArgument since this is what
                // we set our sortColumn based on.
                foreach (System.Web.UI.WebControls.TableCell currCell in e.Row.Cells)
                {
                    if (currCell.HasControls())
                    {
                        sortLink = ((LinkButton)currCell.Controls[0]);
                        if (sortBy3.ContainsKey(sortLink.CommandArgument))
                        {
                            // Use the HTML safe codes for the up arrow ▲ and down arrow ▼.
                            string sortArrow = sortBy3[sortLink.CommandArgument] == "ASC" ? "&#9650;" : "&#9660;";

                            sortLink.Text = sortLink.Text + " " + sortArrow;
                        }
                        else
                        {
                            sortLink.Text = sortLink.Text;
                        }
                    }
                }
            }
            else if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int total = ConvertHelper.ConvertToInt(DataBinder.Eval(e.Row.DataItem, "Total").ToString(), 0);
                total += ConvertHelper.ConvertToInt(hfTotal3.Value, 0);
                hfTotal3.Value = total.ToString();
                litTotal3.Text = Resources.resource.Total + ": " + total.ToString();
            }
        }
        protected void gv3_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Dictionary<string, string> sortBy1 = new Dictionary<string, string>();
            Dictionary<string, string> sortBy2 = new Dictionary<string, string>();
            Dictionary<string, string> sortBy3 = new Dictionary<string, string>();
            if (ViewState["SortBy1"] != null)
            { sortBy1 = (Dictionary<string, string>)ViewState["SortBy1"]; }
            if (ViewState["SortBy2"] != null)
            { sortBy2 = (Dictionary<string, string>)ViewState["SortBy2"]; }
            if (ViewState["SortBy3"] != null)
            { sortBy3 = (Dictionary<string, string>)ViewState["SortBy3"]; }

            gv3.PageIndex = e.NewPageIndex;
            hfTotal3.Value = "0";
            sqlString.bindControl(gv3, getProjectUsage(sortBy3));
            bindCharts(sortBy1, sortBy2, sortBy3);

            if (gv3.Rows.Count > 0)
                gridView.ApplyPaging(gv3, e.NewPageIndex);
        }
        protected void gv3_Sorting(object sender, GridViewSortEventArgs e)
        {
            string expression = e.SortExpression;
            string direction = null;

            //3 state sorting
            //ASC,DESC,null
            if (GetGridViewSortDirection(expression) == "ASC")
            {
                SetGridViewSortDirection(expression, "DESC");
                direction = "DESC";
            }
            else if (GetGridViewSortDirection(expression) == "DESC")
            {
                SetGridViewSortDirection(expression, null);
                direction = null;
            }
            else if (GetGridViewSortDirection(expression) == null)
            {
                SetGridViewSortDirection(expression, "ASC");
                direction = "ASC";
            }

            Dictionary<string, string> sortBy3 = new Dictionary<string, string>();
            if (direction != null)
                sortBy3.Add(e.SortExpression, direction);

            ViewState["SortBy3"] = sortBy3;
            Dictionary<string, string> sortBy1 = new Dictionary<string, string>();
            Dictionary<string, string> sortBy2 = new Dictionary<string, string>();
            if (ViewState["SortBy1"] != null)
            { sortBy1 = (Dictionary<string, string>)ViewState["SortBy1"]; }
            if (ViewState["SortBy2"] != null)
            { sortBy2 = (Dictionary<string, string>)ViewState["SortBy2"]; }

            hfTotal3.Value = "0";
            sqlString.bindControl(gv3, getProjectUsage(sortBy3));
            bindCharts(sortBy1, sortBy2, sortBy3);

            if (gv3.Rows.Count > 0)
                gridView.ApplyPaging(gv3, -1);
        }
        protected void gv3_DataBound(object sender, EventArgs e)
        {
            //litTotal.Text = gv.Rows.Count.ToString("N0"); //doesn't work while paging
            //Literal litTotal = (Literal)gv1.BottomPagerRow.FindControl("litTotal");
            //if (litTotal != null)
            //    if (string.IsNullOrEmpty(litTotal.Text))
            //    {
            //        wwdb db = new wwdb();
            //        litTotal.Text = Resources.resource.Total + " : " + db.getDataTable(getProjectUsage(null)).Rows.Count.ToString("N0");
            //    }
        }
        private string GetGridViewSortDirection(string sortExpression)
        {
            if (ViewState["sortDirection" + sortExpression] == null)
                return null;
            else
                return (string)ViewState["sortDirection" + sortExpression];
        }
        private void SetGridViewSortDirection(string sortExpression, string value)
        {
            ViewState["sortDirection" + sortExpression] = value;
        }
        #endregion
    }
}