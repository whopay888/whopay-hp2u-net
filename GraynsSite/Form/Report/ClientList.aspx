﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Global1.Master" CodeBehind="ClientList.aspx.cs" Inherits="HJT.Form.Report.ClientList" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function confirmDelete() {
            return confirm('<%= GetGlobalResourceObject("resource","Confirm_Delete")%>');
        }

        function confirmSend() {
            return confirm('<%= GetGlobalResourceObject("resource","Confirm_SendSMS")%>');
        }

        //$(window).load(function () {
        //    var x = document.getElementById("filtercontentR");
        //    var lblfilterR = document.getElementById("lblfilterR");
        //    if (x.style.display === "none") {
        //        x.style.display = "block";
        //        lblfilterR.innerHTML = "Hide Filters"
        //    } else {
        //        x.style.display = "none";
        //        lblfilterR.innerHTML = "Show Filters"
        //    }
        //});

        //function myFunctionR() {
        //    var x = document.getElementById("filtercontentR");
        //    var lblfilter1 = document.getElementById("lblfilterR");
        //    if (x.style.display === "none") {
        //        x.style.display = "block";
        //        lblfilterR.innerHTML = "Hide Filters"
        //    } else {
        //        x.style.display = "none";
        //        lblfilterR.innerHTML = "Show Filters"
        //    }
        //}

        $(window).load(function () {
            var x = document.getElementById("filtercontent");
            var lblfilter = document.getElementById("lblfilter");
            if (x.style.display === "none") {
                x.style.display = "block";
                lblfilter.innerHTML = "Hide Filters"
            } else {
                x.style.display = "none";
                lblfilter.innerHTML = "Show Filters"
            }
        });

        function myFunction() {
            var x = document.getElementById("filtercontent");
            var lblfilter = document.getElementById("lblfilter");
            if (x.style.display === "none") {
                x.style.display = "block";
                lblfilter.innerHTML = "Hide Filters"
            } else {
                x.style.display = "none";
                lblfilter.innerHTML = "Show Filters"
            }
        }

        function NewFunction() {
            var lblfilter = document.getElementById("lblfilter");
            lblfilter.innerHTML = "Hide Filters"
        }

        function NewFunctionR() {
            var lblfilterR = document.getElementById("lblfilterR");
            lblfilterR.innerHTML = "Hide Filters"
        }
    </script>
    <script type="text/javascript">
        function confirmCalculate(id) {
            var gv = document.getElementById('<%= gvR.ClientID%>');
            var value = 'ContentPlaceHolder1_gvR_btnCalculate_' + id;
            var btn = document.getElementById(value);
            var hf = document.getElementById('<%= hfConfirmCalculate.ClientID %>');
            if (confirm('<%= GetGlobalResourceObject("resource","Confirm_Calculate")%>')) {
                hf.value = "true";
                btn.click();
            }
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                <span class="icon fa fa-fw fa-book" aria-hidden="true"></span>
                <asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><span runat="server" id="spHeader"><%= GetGlobalResourceObject("resource", "Client_List")%></span></asp:Label>
            </h1>
            <asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
        </section>
        <section class="content">
            <div runat="server" class="col-md-12">
                <div id="filterR" runat="server" visible="false">
                    <a href="#0" id="showfilterR" onclick="myFunctionR()">
                        <label id="lblfilterR" class="btn btn-searh" style="text-transform:capitalize !important;">Show Filters</label>
                    </a>
                    <div class="box" id="filtercontentR">
                        <h4><asp:Label ID="panelR" runat="server" Text="<%$ Resources:Resource, Selection_Option%>"></asp:Label></h4>
                        <div class="box-body">
                            <div class="form-group col-md-6">
                                <label class="control-label"><%= GetGlobalResourceObject("resource", "Applicant_Name")%></label>
                                <div class="controls">
                                    <asp:TextBox runat="server" ID="txtName" CssClass="form-control" Width="50%"/>
                                </div>
                            </div>
                            <div class="form-group col-md-6" runat="server" id="divBranch" style="width:50%">
                                <label class="control-label"><%= GetGlobalResourceObject("resource", "Branch")%></label>
                                <div class="controls">
                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlBranch">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group col-md-6" style="width:50%">
                                <label class="control-label"><%= GetGlobalResourceObject("resource", "Member_Name")%></label>
                                <div class="controls">
                                    <asp:TextBox runat="server" ID="txtMemberNameR" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group col-md-6" runat="server" id="divreqIdR" style="width:50%">
                                <label class="control-label"><%= GetGlobalResourceObject("resource", "Request_ID")%></label>
                                <div class="controls">
                                    <asp:TextBox runat="server" ID="txtRequestIDR" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label"><%= GetGlobalResourceObject("resource", "Status")%></label>
                                <div class="controls">
                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlStatus">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <asp:Button ID="btnSearchR" Text="<%$ Resources:Resource, Search%>" OnClientClick="myFunctionR()" 
                                runat="server" style="text-transform:capitalize !important;"
                                CssClass="btn btn-searh" OnClick="btnSearchR_Click" />
                        </div>
                    </div>
                </div>
                <header>
                    <div class = "table-title"><asp:Literal runat="server" Text="<%$ Resources:Resource, Current_Request%>" /></div>
                </header>
                <div class="box">
                    <div class="box-body">
                        <div class="box-body table-responsive">
                            <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow" ID="gvR" runat="server" AutoGenerateColumns="false" GridLines="None" AllowPaging="True" ShowFooter="true"
                                EmptyDataText="<%$ Resources:Resource, No_Request%>" CssClass="table table-bordered table-striped" Width="100%" OnPageIndexChanging="gvR_PageIndexChanging"
                                OnRowDataBound="gvR_RowDataBound" OnDataBound="gvR_DataBound">
                                <PagerSettings Mode="NumericFirstLast" />
                                <Columns>
                                    <asp:BoundField HeaderText="<%$ Resources:Resource, Request_ID%>" Visible="false" DataField="requestid" />
                                    <asp:BoundField HeaderText="<%$ Resources:Resource, Date%>" DataField="RequestAt" ItemStyle-width="100px"/>
                                    <asp:BoundField HeaderText="<%$ Resources:Resource, Applicant_Name%>" DataField="ApplicantName" />
                                    <asp:BoundField HeaderText="<%$ Resources:Resource, Branch_Name%>" DataField="BranchName" Visible="false"/>
                                    <asp:BoundField HeaderText="<%$ Resources:Resource, Member_Name%>" DataField="Fullname" Visible="false"/>
                                    <asp:BoundField HeaderText="<%$ Resources:Resource, Status%>" DataField="StatusID" />

                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Uploaded_File%>">
                                        <ItemTemplate>
                                            <asp:Repeater runat="server" ID="rptFiles">
                                                <ItemTemplate>
                                                    <p><a href='<%# Eval("URL") %>' target="_blank"><%# Eval("DocumentType") %> </a></p>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Action%>">
                                        <ItemTemplate>
                                            <asp:Button runat="server" ID="btnCalculate" Text="<%$ Resources:Resource, Calculate%>" 
                                                Visible="false" CssClass="btn btn-check" style="width:100px; padding-left:7px !important; text-transform:capitalize !important;"
                                                OnClick="btnCalculate_Click" CommandArgument='<%#Eval("requestid") %>'/>
                                            <asp:Button runat="server" ID="btnRequestViewConsent" Text="<%$ Resources:Resource, Consent%>" 
                                                Visible="false" CssClass="btn btn-check" style="width:70px; padding-left:5px !important; text-transform:capitalize !important;"
                                                CommandArgument='<%#Eval("ConsentFormId") %>' OnClick="btnViewConsent_Click"/>
                                            <asp:Button ID="btnSendSMS" runat="server" Text="<%$ Resources:Resource, SMS%>"
                                                CssClass="btn btn-check" Style="width:47px; padding-left:8px !important" OnClick="btnSendSMS_Click"
                                                OnClientClick="return confirmSend();" CommandArgument='<%#Eval("requestid") %>' Visible="false"/>
                                            <asp:Button runat="server" ID="btnDelete" Text="<%$ Resources:Resource, Del%>" 
                                                CommandArgument='<%#Eval("requestid") %>' Visible="false" OnClick="btnDelete_Click" 
                                                OnClientClick="return confirmDelete();" CssClass="btn btn-danger" 
                                                Style="margin-bottom: 0px !important; padding-left:7px !important; width:47px;" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerTemplate>
                                    <div>
                                        <div class="dataTables_paginate paging_bootstrap">
                                            <ul class="pagination">
                                                <li>
                                                    <asp:PlaceHolder runat="server" ID="phPreviousPaging"></asp:PlaceHolder>
                                                </li>

                                                <li>
                                                    <asp:PlaceHolder runat="server" ID="phPaging"></asp:PlaceHolder>
                                                </li>

                                                <li>
                                                    <asp:PlaceHolder runat="server" ID="phNextPaging"></asp:PlaceHolder>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </PagerTemplate>
                            </asp:GridView>
                            <asp:HiddenField runat="server" ID="hfConfirmCalculate" />
                        </div>
                    </div>
                </div>
                <div id="filter">
                    <a href="#0" id="showfilter" onclick="myFunction()">
                        <label id="lblfilter" class="btn btn-searh" style="text-transform:capitalize !important;">Show Filters</label>
                    </a>
                    <asp:Panel runat="server" DefaultButton="btnSearch">
                        <div class="box" id="filtercontent">
                            <h4><asp:Label ID="panel" runat="server" Text="<%$ Resources:Resource, Selection_Option%>"></asp:Label></h4>
                            <div class="box-body">
                                <div class="form-group col-md-6">
                                    <label class="control-label"><%= GetGlobalResourceObject("resource", "From")%></label>
                                    <div class="controls">
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtFrom" TextMode="Date" Height="40px"/>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label"><%= GetGlobalResourceObject("resource", "To")%></label>
                                    <div class="controls">
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtTo" TextMode="Date" Height="40px"/>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label"><%= GetGlobalResourceObject("resource", "Applicant_Name")%></label>
                                    <div class="controls">
                                        <asp:TextBox runat="server" ID="txtApplicantName" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label"><%= GetGlobalResourceObject("resource", "Member_Name")%></label>
                                    <div class="controls">
                                        <asp:TextBox runat="server" ID="txtMemberName" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group col-md-6" id="divreqId" runat="server">
                                    <label class="control-label"><%= GetGlobalResourceObject("resource", "Request_ID")%></label>
                                    <div class="controls">
                                        <asp:TextBox runat="server" ID="txtRequestID" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group col-md-6" runat="server" id="divFilterParent" visible="false">
                                    <label class="control-label"><%= GetGlobalResourceObject("resource", "Parent_Company")%></label>
                                    <div class="controls">
                                        <asp:DropDownList runat="server" CssClass="form-control" ID="ddlParentBranch" AutoPostBack="true" OnSelectedIndexChanged="ddlParentBranch_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group col-md-6" runat="server" id="divFilterSub" visible="true">
                                    <label class="control-label"><%= GetGlobalResourceObject("resource", "Branch_Name")%></label>
                                    <div class="controls">
                                        <asp:DropDownList runat="server" CssClass="form-control" ID="ddlClientListBranch" AutoPostBack="true" OnSelectedIndexChanged="ddlClientListBranch_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label"><%= GetGlobalResourceObject("resource", "ProjectName")%></label>
                                    <div class="controls">
                                        <asp:DropDownList runat="server" CssClass="form-control" ID="ddlProject">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label"><%= GetGlobalResourceObject("resource", "Customer_Profile")%></label>
                                    <div class="controls">
                                        <asp:DropDownList runat="server" CssClass="form-control" ID="ddlApplicationStatus">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label"><%= GetGlobalResourceObject("resource", "State")%></label>
                                    <div class="controls">
                                        <asp:DropDownList runat="server" CssClass="form-control" ID="ddlState" />
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer col-md-12">
                                <asp:Button ID="btnSearch" Text="<%$ Resources:Resource, Search%>" runat="server" 
                                    style="text-transform:capitalize !important;" CssClass="btn btn-searh" OnClick="btnSearch_Click" />
                                <asp:Button ID="btnPrintPreview" Text="<%$ Resources:Resource, Print_Preview%>" runat="server" 
                                    style="text-transform:capitalize !important;" CssClass="btn btn-searh" OnClick="btnPrintPreview_Click"/>
                                <asp:Button ID="btnDetailedReport" Visible="false" Text="<%$ Resources:Resource, Detailed_Report%>" runat="server" 
                                    style="text-transform:capitalize !important;" CssClass="btn btn-searh" OnClick="btnDetailedReport_Click"/>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <div class="box">
                    <div class="box-body">
                        <div class="box-body table-responsive">
                            <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow" ID="gv" runat="server" AutoGenerateColumns="false" GridLines="None" AllowPaging="True" ShowFooter="true"
                                EmptyDataText="<%$ Resources:Resource, No_Record_Found%>" CssClass="table table-bordered" Width="100%" OnPageIndexChanging="gv_PageIndexChanging"
                                OnRowDataBound="gv_RowDataBound" AllowSorting="true" OnSorting="gv_Sorting" OnDataBound="gv_DataBound">
                                <PagerSettings Mode="NumericFirstLast" />
                                <Columns>
                                    <asp:BoundField HeaderText="<%$ Resources:Resource, Request_ID%>" DataField="requestid" SortExpression="RequestId" Visible="false" />
                                    <asp:BoundField HeaderText="<%$ Resources:Resource, Date%>" DataField="RequestAt" SortExpression="RequestAt" />
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Applicant_Name%>" SortExpression="ApplicantName" ItemStyle-Font-Bold="true">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblApplicantName" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Member_Name%>" Visible="false" SortExpression="Fullname">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" ID="btnMemberName_Whatsapp" OnClick="btnMemberName_Whatsapp_Click" Visible="false" 
                                                CommandArgument='<%# Eval("requestid") %>'>
                                                <%# Eval("Fullname") %>
                                            </asp:LinkButton>
                                            <asp:Label runat="server" ID="lblMemberName" Visible="false" ><%# Eval("Fullname") %></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="<%$ Resources:Resource, State %>" DataField="StateName" />
                                    <asp:BoundField HeaderText="<%$ Resources:Resource, Branch %>" DataField="BranchName" Visible="false" />
                                    <asp:BoundField HeaderText="<%$ Resources:Resource, Price%>" DataField="Price" Visible="false" />
                                    <asp:BoundField HeaderText="<%$ Resources:Resource, ProjectName%>" DataField="ProjectName" SortExpression="ProjectName" />

                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Customer_Profile%>" SortExpression="ApplicationResult" Visible="false">
                                        <ItemTemplate>
                                            <b><asp:Label runat="server" ID="lblStatus"></asp:Label></b>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Report_Type%>" Visible="false">
                                        <ItemTemplate>
                                            <asp:DropDownList runat="server" ID="ddlBank" CssClass="form-control"></asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Action%>">
                                        <ItemTemplate>
                                            <asp:Button runat="server" ID="btnView" Text="<%$ Resources:Resource, View%>" CommandArgument='<%#Eval("requestid") %>'
                                                OnClick="btnView_Click" CssClass="btn btn-view"
                                                Style="padding: 3px 3px !important; text-transform: capitalize !important;" />
                                            <asp:Button runat="server" ID="btnEdit" Visible="false" Text="<%$ Resources:Resource, Edit%>" CommandArgument='<%#Eval("requestid") %>'
                                                OnClick="btnEdit_Click" CssClass="btn btn-edit"
                                                Style="padding: 3px 3px !important; text-transform: capitalize !important;" />
                                            <asp:ImageButton ID="btnEditStatus" runat="server" ImageUrl="/Images/editStatusButton.png" 
                                                OnClick="btnEditStatus_Click" CommandArgument='<%#Eval("requestid") %>' Visible="false" 
                                                style="padding:3px 3px !important; height:40px; width:40px; vertical-align:bottom;
                                                margin-bottom:7px"/>
                                             <%--<Re-open delete button>--%>
                                             <asp:Button runat="server" ID="btnDelete" Text="<%$ Resources:Resource, Delete%>" CommandArgument='<%#Eval("requestid") %>' 
                                                OnClick="btnDelete_Click" Visible="false"
                                                OnClientClick="return confirmDelete();" CssClass="btn btn-danger" 
                                                Style="padding: 3px 3px !important; text-transform: capitalize !important;" />
                                              <%--<Re-open delete button>--%>
                                            <br />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, CCRIS%>" Visible="false">
                                        <ItemTemplate>
                                            <asp:Button runat="server" ID="btnMACCRIS" Text="<%$ Resources:Resource, Main_Applicant%>" 
                                                CommandArgument='<%#Eval("requestid") %>' OnClick="btnMACCRIS_Click" CssClass="btn btn-view"
                                                style="text-transform:capitalize !important; margin-top:12px;"/>
                                            <asp:Button runat="server" ID="btnCACCRIS" Text="<%$ Resources:Resource, Co_Applicant%>" 
                                                CommandArgument='<%#Eval("requestid") %>' OnClick="btnCACCRIS_Click" CssClass="btn btn-view"
                                                style="text-transform:capitalize !important; margin-top:12px;"/>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Uploaded_File%>" Visible="false">
                                        <ItemTemplate>
                                            <asp:Repeater runat="server" ID="rptFiles">
                                                <ItemTemplate>
                                                    <p><a href='<%# Eval("DocsURL") %>' target="_blank">
                                                        <%= GetGlobalResourceObject("resource","Uploaded_File") %>
                                                    </a></p>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <asp:LinkButton runat="server" ID="btnClientViewConsent" Text="<%$ Resources:Resource, Uploaded_File%>"
                                                Visible="false" CommandArgument='<%#Eval("ConsentFormId") %>' OnClick="btnViewConsent_Click"/>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerTemplate>
                                    <div class="dataTables_paginate paging_bootstrap">
                                        <ul class="pagination">
                                            <li>
                                                <asp:PlaceHolder runat="server" ID="phPreviousPaging"></asp:PlaceHolder>
                                            </li>
                                            <li>
                                                <asp:PlaceHolder runat="server" ID="phPaging"></asp:PlaceHolder>
                                            </li>
                                            <li>
                                                <asp:PlaceHolder runat="server" ID="phNextPaging"></asp:PlaceHolder>
                                            </li>
                                        </ul>
                                    </div>
                                </PagerTemplate>
                            </asp:GridView>
                            <div style="text-align: left; position: initial;">
                                <h4><strong><i><b>
                                    <asp:Literal runat="server" ID="litCurrent" />&nbsp&nbsp
                                    <asp:Literal runat="server" ID="litTotal" />
                                </b></i></strong></h4>
                            </div>
                            <div style="float:left; position: absolute; margin-left: 250px; margin-top: -25px;">
                                <asp:LinkButton runat="server" ID="btnShowAllData" OnClick="btnShowAllData_Click" 
                                    Text='<%$ Resources:resource, LoadAll %>' Font-Size="Medium" Font-Bold="true"
                                    Font-Italic="true"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </aside>
</asp:Content>
