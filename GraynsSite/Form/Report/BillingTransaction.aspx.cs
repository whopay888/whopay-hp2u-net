﻿using HJT.Cls.Helper;
using HJT.Cls.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Controller;
using Synergy.Helper;
using Synergy.Model;
using Synergy.Util;

namespace EDV.Form.Report
{
    public partial class BillingTransaction : System.Web.UI.Page
    {

        LoginUserModel login_user = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];
            if (!IsPostBack)
            {
                bindControl();
                sqlString.bindControl(gv, getRequestList(null));
            }

            if (gv.Rows.Count > 0)
                gridView.ApplyPaging(gv, -1);
        }

        private void bindControl()
        {
            ddlTrasactionType.Items.Add(new ListItem("Please Select One...", "0"));
            ddlTrasactionType.Items.Add(new ListItem("Request", "1"));
            ddlTrasactionType.Items.Add(new ListItem("Successful Case", "2"));
            ddlTrasactionType.Items.Add(new ListItem("Payment", "3"));
            ddlTrasactionType.Items.Add(new ListItem("Manual Payment", "4"));
        }

        private string getRequestList(IDictionary<string, string> sortBy)
        {
            StringBuilder sql = new StringBuilder();
            StringBuilder sqlFilter = new StringBuilder();


            string searchFilter = string.Empty;

            /*
             SELECT tut.CreatedAt, tmi.Username, tmi.Fullname, tut.RequestType, tut.RequestId, 
	                                CASE WHEN tut.RequestType like '%Payment%'
		                                THEN '+'+cast(tut.Amount as varchar)
		                                ELSE cast(tut.Amount as varchar)
	                                END AS CreditUsed, ISNULL(ipt.PaymentId,' ') AS PaymentId, isFreeRequest, Remark 
                                FROM dbo.tbl_UserTransactions tut INNER JOIN 
                                dbo.tbl_MemberInfo tmi ON tut.MemberId = tmi.MemberId LEFT JOIN 
                                tbl_Ipay88Transaction ipt ON tut.Id = ipt.UserTransactionId 
                                WHERE 1=1 AND tut.RequestType != 'Free Request' AND tut.[Status] = 1
                                and tut.Amount != 0
             */

            sql.AppendFormat(@"SELECT tut.CreatedAt, tmi.Username, tmi.Fullname, 
	                                CASE WHEN tut.RequestType LIKE 'Free Request'
		                                THEN tut.RequestType + ' awarded + ' + cast(tut.FreeRequestCount as varchar)
		                            WHEN tut.RequestType LIKE 'Payment'
		                                THEN tut.RequestType + '. PaymentID: ' + ISNULL(ipt.PaymentId,'') 
		                            WHEN tut.IsFreeCredit = 1 OR tut.RequestType LIKE 'Free Credit'
		                                THEN tut.RequestType + '. ' + ISNULL(tut.Remark,'')
                                    WHEN tut.RequestType LIKE 'Request'
                                        THEN tut.RequestType + '. ' + 
										    ISNULL('<br />Applicant Name 1: ' +
											    (
											    SELECT a.[Name] FROM tbl_Applicants a
											    WHERE a.RequestID=tut.RequestId AND a.ApplicantType='MA'
											    ),
											    ISNULL('<br />Applicant Name 1: ' +
												    (
												    SELECT a.[Name] FROM tbl_TApplicants a
												    WHERE a.RequestID=tut.RequestId AND a.ApplicantType='MA'
												    )
											    ,'')
										    ) + ' ' + ISNULL('<br />Applicant Name 2: ' +
												    (
												    SELECT a.[Name] FROM tbl_Applicants a
												    WHERE a.RequestID=tut.RequestId AND a.ApplicantType='CA'
												    ),
												    ISNULL('<br />Applicant Name 2: ' +
													    (
													    SELECT a.[Name] FROM tbl_TApplicants a
													    WHERE a.RequestID=tut.RequestId AND a.ApplicantType='CA'
													    )
												    ,'')
											    )
		                                ELSE tut.RequestType + '. ' + ISNULL(tut.Remark,'')
	                                END AS 'Description', tut.RequestId, 
	                                CASE WHEN tut.Amount < 0
		                                THEN cast(tut.Amount as varchar)
		                                ELSE '+' + cast(tut.Amount as varchar)
	                                END AS 'Used', 
                                        (select SUM(ut.Amount)-isnull((select sum(b.Amount) from tbl_UserTransactions b
				                            where b.MemberId=tut.MemberId and b.IsFreeRequest=1
				                            and b.CreatedAt between (
					                            select top 1 c.CreatedAt from tbl_UserTransactions c
					                            where c.MemberId=b.MemberId
					                            order by c.CreatedAt asc) and tut.CreatedAt),0)
	                                        from tbl_UserTransactions ut 
	                                        where ut.MemberId=tut.MemberId 
		                                        and (ut.CreatedAt between 
			                                        (select top 1 a.CreatedAt from tbl_UserTransactions a
				                                        where a.MemberId=ut.MemberId
				                                        order by a.CreatedAt asc) and tut.CreatedAt)
		                                        and ut.Status=1)
	                                as 'Balance', isFreeRequest, Remark 
                                FROM dbo.tbl_UserTransactions tut INNER JOIN 
                                dbo.tbl_MemberInfo tmi ON tut.MemberId = tmi.MemberId LEFT JOIN 
                                tbl_Ipay88Transaction ipt ON tut.Id = ipt.UserTransactionId 
                                WHERE 1=1 AND tut.RequestType NOT LIKE '%Free%' AND tut.[Status] = 1
                                and tut.Amount != 0
            ");

            if (!string.IsNullOrWhiteSpace(txtFrom.Text) && !string.IsNullOrWhiteSpace(txtTo.Text) && DateTime.TryParse(txtFrom.Text, out var from) && DateTime.TryParse(txtTo.Text, out var to))
                sqlFilter.Append(" AND tut.CreatedAt BETWEEN CONVERT(DATE,'" + secure.RC(txtFrom.Text) + "') AND CONVERT(DATE,'" + secure.RC(txtTo.Text) + "')");

            if (ddlTrasactionType.SelectedIndex != 0)
            {
                string transactionType = "";
                if (ddlTrasactionType.SelectedValue == "1")
                    transactionType = "Request";
                else if (ddlTrasactionType.SelectedValue == "2")
                    transactionType = "Successful Case";
                else if (ddlTrasactionType.SelectedValue == "3")
                    transactionType = "Payment";
                else if (ddlTrasactionType.SelectedValue == "4")
                    transactionType = "Manual Payment";
                sqlFilter.Append(" AND tut.RequestType ='" + transactionType + "'");
            }

            if (txtRequestId.Text.Trim() != string.Empty)
                sqlFilter.Append(" AND tut.RequestId ='" + secure.RC(txtRequestId.Text.Trim()) + "'");

            if (txtfullname.Text.Trim() != string.Empty)
                sqlFilter.Append(" AND tmi.Fullname LIKE '%" + secure.RC(txtfullname.Text.Trim()) + "%'");

            if (sqlFilter.ToString().Trim() != string.Empty)
                sql.Append(sqlFilter);

            sql.Append("order by ");
            //ordering
            if (sortBy != null && sortBy.Any())
            {
                int i = 0;
                foreach (var item in sortBy)
                {
                    string key = item.Key;
                    string value = item.Value;

                    if (i > 0)
                        sql.Append(",");

                    switch (key)
                    {
                        case "CreatedAt":
                            sql.Append(" tut.CreatedAt " + value);
                            break;
                        default:
                            break;
                    }
                    i++;
                }
            }
            else
                sql.Append(" tut.CreatedAt DESC ");

            return sql.ToString();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> sortBy = new Dictionary<string, string>();
            if (ViewState["SortBy"] != null)
            { sortBy = (Dictionary<string, string>)ViewState["SortBy"]; }
            sqlString.bindControl(gv, getRequestList(sortBy));

            if (gv.Rows.Count > 0)
                gridView.ApplyPaging(gv, -1);

            //Page.RegisterStartupScript(this.GetType(), "CallMyFunction", "NewFunction()", true);
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "MyFun1", "NewFunction();", true);
        }
        
        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //DEFAULT BACKUP
                ////string isFreeRequest = DataBinder.Eval(e.Row.DataItem, "IsFreeRequest").ToString();
                //string amount = DataBinder.Eval(e.Row.DataItem, "CreditUsed").ToString();
                //string isFreeRequest = DataBinder.Eval(e.Row.DataItem, "isFreeRequest").ToString();
                ////Label lblPaymentStatus = (Label)e.Row.FindControl("lblPaymentStatus");

                //Label lblAmount = (Label)e.Row.Cells[6].FindControl("lblAmount");
                //lblAmount.Text = amount;
                //if (isFreeRequest.ToUpper() == "TRUE")
                //{
                //    Label lblFree = (Label)e.Row.Cells[6].FindControl("lblFree");
                //    lblFree.Text = " FREE";
                //    lblAmount.Attributes.Add("style", "text-decoration:line-through;");
                //}

                //DEBIT
                string usedAmount = DataBinder.Eval(e.Row.DataItem, "Used").ToString();
                string isFreeRequest = DataBinder.Eval(e.Row.DataItem, "isFreeRequest").ToString();
                Label lblUsedAmount = (Label)e.Row.Cells[3].FindControl("lblUsedAmount");
                lblUsedAmount.Text = usedAmount;
                if (isFreeRequest.ToUpper() == "TRUE")
                {
                    Label lblFree = (Label)e.Row.Cells[3].FindControl("lblUsedFree");
                    lblFree.Text = " FREE";
                    lblUsedAmount.Attributes.Add("style", "text-decoration:line-through;");
                }

                //Description
                string description = DataBinder.Eval(e.Row.DataItem, "Description").ToString();
                Label lblDescription = (Label)e.Row.Cells[1].FindControl("lblDescription");
                TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                lblDescription.Text = textInfo.ToTitleCase(description.ToLower());
            }
        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Dictionary<string, string> sortBy = new Dictionary<string, string>();
            if (ViewState["SortBy"] != null)
            { sortBy = (Dictionary<string, string>)ViewState["SortBy"]; }

            gv.PageIndex = e.NewPageIndex;
            sqlString.bindControl(gv, getRequestList(sortBy));

            if (gv.Rows.Count > 0)
                gridView.ApplyPaging(gv, e.NewPageIndex);
        }

        protected void gv_DataBound(object sender, EventArgs e)
        {

        }
        
        private string GetGridViewSortDirection(string sortExpression)
        {
            if (ViewState["sortDirection" + sortExpression] == null)
                return null;
            else
                return (string)ViewState["sortDirection" + sortExpression];
        }

        private void SetGridViewSortDirection(string sortExpression, string value)
        {
            ViewState["sortDirection" + sortExpression] = value;
        }

        protected void gv_Sorting(object sender, GridViewSortEventArgs e)
        {
            string expression = e.SortExpression;
            string direction = null;

            //3 state sorting
            //ASC,DESC,null
            if (GetGridViewSortDirection(expression) == "ASC")
            {
                SetGridViewSortDirection(expression, "DESC");
                direction = "DESC";
            }
            else if (GetGridViewSortDirection(expression) == "DESC")
            {
                SetGridViewSortDirection(expression, null);
                direction = null;
            }
            else if (GetGridViewSortDirection(expression) == null)
            {
                SetGridViewSortDirection(expression, "ASC");
                direction = "ASC";
            }

            Dictionary<string, string> sortBy = new Dictionary<string, string>();
            if (direction != null)
                sortBy.Add(e.SortExpression, direction);

            ViewState["SortBy"] = sortBy;

            sqlString.bindControl(gv, getRequestList(sortBy));

            if (gv.Rows.Count > 0)
                gridView.ApplyPaging(gv, -1);
        }
    }
}