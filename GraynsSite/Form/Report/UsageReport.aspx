﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Global1.Master" CodeBehind="UsageReport.aspx.cs" Inherits="HJT.Form.Report.UsageReport" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        $(window).load(function () {
            var x = document.getElementById("filtercontent");
            var lblfilter = document.getElementById("lblfilter");
            if (x.style.display === "none") {
                x.style.display = "block";
                lblfilter.innerHTML = "Hide Filters"
            } else {
                x.style.display = "none";
                lblfilter.innerHTML = "Show Filters"
            }
        });

        function myFunction() {
            var x = document.getElementById("filtercontent");
            var lblfilter = document.getElementById("lblfilter");
            if (x.style.display === "none") {
                x.style.display = "block";
                lblfilter.innerHTML = "Hide Filters"
            } else {
                x.style.display = "none";
                lblfilter.innerHTML = "Show Filters"
            }
        }

        function NewFunction() {
            var lblfilter = document.getElementById("lblfilter");
            lblfilter.innerHTML = "Hide Filters"
        }

    </script>
    <style>
        .table-bordered{
            width: 100%;
        }

        @media only screen and (max-width: 1500px) {
            .table-bordered {
                width: 100%;
            }
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                <span class="icon fa fa-fw fa-book" aria-hidden="true"></span>
                <asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><span runat="server" id="spHeader"><%= GetGlobalResourceObject("resource", "Usage_Report")%></span></asp:Label>
            </h1>
            <asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
        </section>
        <section class="content">
            <div class="col-md-12">
                <div runat="server">
                    <div id="filter">
                        <a href="#0" id="showfilter" onclick="myFunction()">
                            <label id="lblfilter" class="btn btn-searh" style="text-transform:capitalize !important;">Show Filters</label>
                        </a>
                        <div id="filtercontent">
                            <div class="box">
                                <h4><asp:Label ID="panel" runat="server" Text="<%$ Resources:Resource, Selection_Option%>"></asp:Label></h4>
                                <asp:Panel runat="server" DefaultButton="btnSearch">
                                    <div class="box-body">
                                        <div class="form-group col-md-6">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "From")%></label>
                                            <div class="controls">
                                                <asp:TextBox runat="server" CssClass="form-control" ID="txtFrom" TextMode="Date">
                                                </asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "To")%></label>
                                            <div class="controls">
                                                <asp:TextBox runat="server" CssClass="form-control" ID="txtTo" TextMode="Date">
                                                </asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "Parent_Company")%></label>
                                            <div class="controls">
                                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlParentBranch" 
                                                    OnSelectedIndexChanged="ddlParentBranch_SelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
									    </div>
									
									    <div id="filterSubBranch" runat="server" class="form-group col-md-6">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "Sub_Branch")%></label>
                                            <div class="controls">
                                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlSubBranch" 
                                                    OnSelectedIndexChanged="ddlSubBranch_SelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>									
                                        </div>
									
									    <div class="form-group col-md-6">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "Project_Name") %></label>
                                            <div class="controls">
                                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlProjectName" 
                                                    OnSelectedIndexChanged="ddlProjectName_SelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>	
									    </div>	
                                    </div>
                                    <div class="box-footer col-md-12">
                                        <asp:Button ID="btnSearch" Text="<%$ Resources:Resource, Search%>" runat="server"
                                            style="text-transform:capitalize !important;" CssClass="btn btn-searh" OnClick="btnSearch_Click" />
                                        <asp:Button ID="btnPrintPreview" Text="<%$ Resources:Resource, Print_Preview%>" runat="server"
                                            style="text-transform:capitalize !important;" CssClass="btn btn-searh" OnClick="btnPrintPreview_Click"
                                            Visible="true"/>
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-body">
                            <div>
                                <table style="width:100%;">
                                    <tr style="width:100%;">
                                        <td style="width:50%;">
                                            <div class="box-body table-responsive" runat="server" id="divParentBranch">
                                                <header>
                                                     <div class = "table-title"><asp:Literal runat="server" Text="<%$ Resources:Resource, Parent_Company%>" /></div>
                                                </header>
                                                <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow" ID="gv1" runat="server" AutoGenerateColumns="false" 
                                                    GridLines="None" EmptyDataText="<%$ Resources:Resource, No_Record_Found%>" AllowPaging="True" ShowFooter="true" 
                                                    CssClass="table table-bordered" OnPageIndexChanging="gv1_PageIndexChanging"
                                                    OnRowDataBound="gv1_RowDataBound" AllowSorting="true" OnSorting="gv1_Sorting" PageSize="10" OnDataBound="gv1_DataBound">
                                                    <PagerSettings Mode="NumericFirstLast" />
                                                    <Columns>
                                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Branch_Name%>" DataField="ParentBranchName" 
                                                            SortExpression="ParentBranchName" HeaderStyle-Width="350px" ItemStyle-Width="45%"/>
                                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Total%>" DataField="Total" SortExpression="ParentTotal" 
                                                            ItemStyle-Width="25%"/>
                                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Other%>" DataField="Other" SortExpression="ParentOther" 
                                                            ItemStyle-Width="15%" Visible="false"/>
                                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Green%>" DataField="Green" SortExpression="ParentGreen" 
                                                            ItemStyle-Width="10%" Visible="true"/>
                                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Yellow%>" DataField="Yellow" SortExpression="ParentYellow" 
                                                            ItemStyle-Width="10%" Visible="true"/>
                                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Red%>" DataField="Red" SortExpression="ParentRed" 
                                                            ItemStyle-Width="10%" Visible="true"/>
                                                    </Columns>
                                                    <PagerTemplate>
                                                        <div class="dataTables_paginate paging_bootstrap">
                                                            <ul class="pagination">
                                                                <li>
                                                                    <asp:PlaceHolder runat="server" ID="phPreviousPaging"></asp:PlaceHolder>
                                                                </li>

                                                                <li>
                                                                    <asp:PlaceHolder runat="server" ID="phPaging"></asp:PlaceHolder>
                                                                </li>

                                                                <li>
                                                                    <asp:PlaceHolder runat="server" ID="phNextPaging"></asp:PlaceHolder>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </PagerTemplate>
                                                </asp:GridView>
                                                <div style="text-align: left; position: initial;">
                                                    <h4><strong><i><b>
                                                        <asp:Literal runat="server" ID="litTotal1"/>
                                                    </b></i></strong></h4>
                                                    <asp:HiddenField runat="server" ID="hfTotal1" Value="0" />
                                                    <h5><strong style="color: red">
                                                        <asp:Literal runat="server" Text="<%$ Resources:resource, UsageReport_Remark_Parent %>"/>
                                                    </strong></h5>
                                                </div>
                                            </div>
                                        </td>
                                        <td style="width:50%;">
                                            <asp:Chart ID="chartParentUsage" runat="server" CssClass="chart-responsive">
                                                <Titles>
                                                    <asp:Title Text="<%$ Resources:resource, Parent_Company %>"/>
                                                </Titles>
                                                <Series>
                                                    <asp:Series XValueMember="ParentBranchName" YValueMembers="Green"/>
                                                    <asp:Series XValueMember="ParentBranchName" YValueMembers="Yellow"/>
                                                    <asp:Series XValueMember="ParentBranchName" YValueMembers="Red"/>
                                                </Series>
                                                <ChartAreas>
                                                    <asp:ChartArea Name="chartAreaParentUsage">
                                                        <AxisX Title="<%$ Resources:resource, Parent_Company %>"/>
                                                        <AxisY Title="<%$ Resources:resource, Usage %>"/>
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                            </asp:Chart>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:50%;">
                                            <div class="box-body table-responsive" runat="server" id="divSubBranch">
                                                <header>
                                                    <div class = "table-title"><asp:Literal runat="server" Text="<%$ Resources:Resource, Branch%>" /></div>
                                                </header>
                                                <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow" ID="gv2" runat="server" AutoGenerateColumns="false" 
                                                    GridLines="None" AllowPaging="True" ShowFooter="true" EmptyDataText="<%$ Resources:Resource, No_Record_Found%>" 
                                                    CssClass="table table-bordered" OnPageIndexChanging="gv2_PageIndexChanging"
                                                    OnRowDataBound="gv2_RowDataBound" OnSorting="gv2_Sorting" AllowSorting="true" OnDataBound="gv2_DataBound">
                                                    <PagerSettings Mode="NumericFirstLast" />
                                                    <Columns>
                                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Branch_Name%>" DataField="BranchName" 
                                                            SortExpression="SubBranchName" HeaderStyle-Width="350px" ItemStyle-Width="50%"/>
                                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Usage%>" DataField="Total" SortExpression="SubUsage" 
                                                            ItemStyle-Width="20%" />
                                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Green%>" DataField="Green" SortExpression="SubGreen" 
                                                            ItemStyle-Width="10%"/>
                                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Yellow%>" DataField="Yellow" SortExpression="SubYellow" 
                                                            ItemStyle-Width="10%"/>
                                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Red%>" DataField="Red" SortExpression="SubRed" 
                                                            ItemStyle-Width="10%"/>
                                                    </Columns>
                                                    <PagerTemplate>
                                                        <div class="dataTables_paginate paging_bootstrap">
                                                            <ul class="pagination">
                                                                <li>
                                                                    <asp:PlaceHolder runat="server" ID="phPreviousPaging"></asp:PlaceHolder>
                                                                </li>

                                                                <li>
                                                                    <asp:PlaceHolder runat="server" ID="phPaging"></asp:PlaceHolder>
                                                                </li>

                                                                <li>
                                                                    <asp:PlaceHolder runat="server" ID="phNextPaging"></asp:PlaceHolder>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </PagerTemplate>
                                                </asp:GridView>
                                                <div style="text-align: left; position: initial;">
                                                    <h4><strong><i><b>
                                                        <asp:Literal runat="server" ID="litTotal2"/>
                                                    </b></i></strong></h4>
                                                    <asp:HiddenField runat="server" ID="hfTotal2" Value="0" />
                                                    <h5><strong style="color: red">
                                                        <asp:Literal runat="server" Text="<%$ Resources:resource, UsageReport_Remark_SubBranch %>"/>
                                                    </strong></h5>
                                                </div>
                                            </div>
                                        </td>
                                        <td style="width:50%;">
                                            <asp:Chart ID="chartBranchUsage" runat="server" CssClass="chart-responsive">
                                                <Titles>
                                                    <asp:Title Text="<%$ Resources:resource, Branch %>"/>
                                                </Titles>
                                                <Series>
                                                    <asp:Series XValueMember="BranchName" YValueMembers="Green"/>
                                                    <asp:Series XValueMember="BranchName" YValueMembers="Yellow"/>
                                                    <asp:Series XValueMember="BranchName" YValueMembers="Red"/>
                                                </Series>
                                                <ChartAreas>
                                                    <asp:ChartArea Name="chartAreaBranchUsage">
                                                        <AxisX Title="<%$ Resources:resource, Branch_Name %>"/>
                                                        <AxisY Title="<%$ Resources:resource, Usage %>"/>
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                            </asp:Chart>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:50%;">
                                            <div class="box-body table-responsive" runat="server" id="divProjectName">
                                                <header>
                                                     <div class = "table-title"><asp:Literal runat="server" Text="<%$ Resources:Resource, Project_Usage %>" /></div>
                                                </header>
                                                <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow" ID="gv3" runat="server" AutoGenerateColumns="false" 
                                                    GridLines="None" AllowPaging="True" ShowFooter="true" EmptyDataText="<%$ Resources:Resource, No_Record_Found%>" 
                                                    CssClass="table table-bordered" OnPageIndexChanging="gv3_PageIndexChanging"
                                                    OnRowDataBound="gv3_RowDataBound" OnSorting="gv3_Sorting" AllowSorting="true" OnDataBound="gv3_DataBound">
                                                    <PagerSettings Mode="NumericFirstLast" />
                                                    <Columns>
                                                        <asp:BoundField HeaderText="<%$ Resources:Resource, ProjectName%>" DataField="ProjectName" SortExpression="ProjectName" 
                                                            HeaderStyle-Width="350px" ItemStyle-Width="50%"/>
                                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Usage%>" DataField="Total" SortExpression="ProjectUsage" 
                                                            ItemStyle-Width="20%"/>
                                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Green%>" DataField="Green" SortExpression="ProjectGreen" 
                                                            ItemStyle-Width="10%"/>
                                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Yellow%>" DataField="Yellow" SortExpression="ProjectYellow" 
                                                            ItemStyle-Width="10%"/>
                                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Red%>" DataField="Red" SortExpression="ProjectRed" 
                                                            ItemStyle-Width="10%"/>
                                                    </Columns>
                                                    <PagerTemplate>
                                                        <div class="dataTables_paginate paging_bootstrap">
                                                            <ul class="pagination">
                                                                <li>
                                                                    <asp:PlaceHolder runat="server" ID="phPreviousPaging"></asp:PlaceHolder>
                                                                </li>
                                                                <li>
                                                                    <asp:PlaceHolder runat="server" ID="phPaging"></asp:PlaceHolder>
                                                                </li>
                                                                <li>
                                                                    <asp:PlaceHolder runat="server" ID="phNextPaging"></asp:PlaceHolder>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </PagerTemplate>
                                                </asp:GridView>
                                                <div style="text-align: left; position: initial;">
                                                    <h4><strong><i><b>
                                                        <asp:Literal runat="server" ID="litTotal3"/>
                                                    </b></i></strong></h4>
                                                    <asp:HiddenField runat="server" ID="hfTotal3" Value="0" />
                                                    <h5><strong style="color: red">
                                                        <asp:Literal runat="server" Text="<%$ Resources:resource, UsageReport_Remark_Project %>"/>
                                                    </strong></h5>
                                                </div>
                                            </div>
                                        </td>
                                        <td style="width:50%;">
                                            <asp:Chart ID="chartProjectUsage" runat="server" CssClass="chart-responsive">
                                                <Titles>
                                                    <asp:Title Text="<%$ Resources:resource, Project_Usage %>"/>
                                                </Titles>
                                                <Series>
                                                    <asp:Series XValueMember="ProjectName" YValueMembers="Green"/>
                                                    <asp:Series XValueMember="ProjectName" YValueMembers="Yellow"/>
                                                    <asp:Series XValueMember="ProjectName" YValueMembers="Red"/>
                                                </Series>
                                                <ChartAreas>
                                                    <asp:ChartArea Name="chartAreaProjectUsage">
                                                        <AxisX Title="<%$ Resources:resource, Project_Name %>"/>
                                                        <AxisY Title="<%$ Resources:resource, Usage %>"/>
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                            </asp:Chart>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </aside>
</asp:Content>
