﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Global1.Master" CodeBehind="BillingTransaction.aspx.cs" Inherits="EDV.Form.Report.BillingTransaction" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function confirmSubmit() {
            return confirm('<%= GetGlobalResourceObject("resource" , "ConfirmToRegister") %>');
        }

        $(window).load(function () {
            var x = document.getElementById("filtercontent");
            var lblfilter = document.getElementById("lblfilter");
            if (x.style.display === "none") {
                x.style.display = "block";
                lblfilter.innerHTML = "Hide Filters"
            } else {
                x.style.display = "none";
                lblfilter.innerHTML = "Show Filters"
            }
        });

        function myFunction() {
            var x = document.getElementById("filtercontent");
            var lblfilter = document.getElementById("lblfilter");
            if (x.style.display === "none") {
                x.style.display = "block";
                lblfilter.innerHTML = "Hide Filters"
            } else {
                x.style.display = "none";
                lblfilter.innerHTML = "Show Filters"
            }
        }

        function NewFunction() {
            var lblfilter = document.getElementById("lblfilter");
            lblfilter.innerHTML = "Hide Filters"
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                <span class="icon fa fa-fw fa-book" aria-hidden="true"></span>
                <asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><span runat="server" id="spHeader"><%= GetGlobalResourceObject("resource", "Billing_Transaction_Report")%></span></asp:Label>
            </h1>
            <asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
        </section>
        <section class="content" style="margin-top: 10px">
            <div class="col-md-12">
                <div runat="server">
                    <div id="filter">
                        <div>
                            <a href="#0" id="showfilter" onclick="myFunction()">
                                <label id="lblfilter" class="btn btn-searh" style="text-transform:capitalize !important;">Show Filters</label>
                            </a>
                            <div id="filtercontent">
                                <div class="body box-body">
                                    <asp:Panel runat="server" DefaultButton="btnSearch">
                                        <div class="col-md-12">
                                            <div class="form-group col-md-6">
                                                <label class="control-label"><%= GetGlobalResourceObject("resource", "From")%></label>
                                                <div class="controls" style="height:50% !important;">
                                                    <asp:TextBox ID="txtFrom" runat="server" CssClass="form-control" TextMode="Date"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="control-label"><%= GetGlobalResourceObject("resource", "To")%></label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtTo" runat="server" CssClass="form-control" TextMode="Date"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6" runat="server" visible="false">
                                                <label class="control-label"><%= GetGlobalResourceObject("resource", "Request_ID")%></label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtRequestId" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="control-label"><%= GetGlobalResourceObject("resource", "Fullname")%></label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtfullname" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6" runat="server" id="divBranch">
                                                <label class="control-label"><%= GetGlobalResourceObject("resource", "Transaction_Type")%></label>
                                                <div class="controls">
                                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlTrasactionType">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box box-footer">
                                            <asp:Button ID="btnSearch" Text="<%$ Resources:Resource, Search%>" OnClientClick="myFunction()" runat="server" CssClass="btn btn-searh" OnClick="btnSearch_Click" />
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-body">
                            <div class="box-body table-responsive">
                                <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow" ID="gv" runat="server" AutoGenerateColumns="false" GridLines="None" 
                                    AllowPaging="True" ShowFooter="true" EmptyDataText="<%$ Resources:Resource, No_Record_Found%>" CssClass="table table-bordered table-striped" 
                                    Width="100%" OnPageIndexChanging="gv_PageIndexChanging" OnRowDataBound="gv_RowDataBound" OnDataBound="gv_DataBound" AllowSorting="true"
                                    OnSorting="gv_Sorting">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <Columns>
                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Date%>" DataField="CreatedAt" ItemStyle-Width="15%" SortExpression="CreatedAt" />
                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Username%>" DataField="Username" Visible="false" />
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, Description%>" ItemStyle-Width="50%" >
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblDescription"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="<%$ Resources:Resource, FullName%>" DataField="Fullname" ItemStyle-Width="15%" />
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, Amount%>" ItemStyle-Width="10%" >
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblUsedAmount"></asp:Label>
                                                <asp:Label runat="server" ID="lblUsedFree"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Balance%>" DataField="Balance" ItemStyle-Width="10%" />
                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Request_ID%>" DataField="RequestId" Visible="false" />
                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Remark%>" DataField="Remark" ItemStyle-Width="25%" Visible="false" />
                                    </Columns>
                                    <PagerTemplate>
                                        <div>
                                            <div class="dataTables_paginate paging_bootstrap">
                                                <ul class="pagination">
                                                    <li>
                                                        <asp:PlaceHolder runat="server" ID="phPreviousPaging"></asp:PlaceHolder>
                                                    </li>
                                                    <li>
                                                        <asp:PlaceHolder runat="server" ID="phPaging"></asp:PlaceHolder>
                                                    </li>
                                                    <li>
                                                        <asp:PlaceHolder runat="server" ID="phNextPaging"></asp:PlaceHolder>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </PagerTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </aside>
</asp:Content>
