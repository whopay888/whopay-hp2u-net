﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Helper;
using Synergy.Model;
using Synergy.Util;

namespace HJT.Form.Report
{
    public partial class ClientListDetailedReport : System.Web.UI.Page
    {
        LoginUserModel login_user = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];
            if (!IsPostBack)
            {
                string from = secure.Decrypt(Validation.GetUrlParameter(this.Page, "from", ""), true);
                string to = secure.Decrypt(Validation.GetUrlParameter(this.Page, "to", ""), true);
                //string membername = secure.Decrypt(Validation.GetUrlParameter(this.Page, "membername", ""), true);
                string applicantname = secure.Decrypt(Validation.GetUrlParameter(this.Page, "applicantname", ""), true);
                string requestid = secure.Decrypt(Validation.GetUrlParameter(this.Page, "requestid", ""), true);
                string projectid = secure.Decrypt(Validation.GetUrlParameter(this.Page, "projectid", ""), true);
                string projectname = secure.Decrypt(Validation.GetUrlParameter(this.Page, "projectname", ""), true);
                string applicationstatus = secure.Decrypt(Validation.GetUrlParameter(this.Page, "applicationstatus", ""), true);
                
                lblProject.Text = projectid.Equals("0") ? "-" : projectname;
                lblFrom.Text = string.IsNullOrWhiteSpace(from) ? "-" : from;
                lblTo.Text = string.IsNullOrWhiteSpace(to) ? "-" : to;
                
                sqlString.bindControl(gv, convertTable(getReport1(from, to, applicantname, requestid, applicationstatus, projectid),
                    getReport2(from, to, applicantname, requestid, applicationstatus, projectid)));
            }
        }

        private string getClientList(string from, string to, string memberName, string applicantName, string requestId, string projectId, string applicationStatus, IDictionary<string, string> sortBy)
        {
            StringBuilder sql = new StringBuilder();
            
            string searchFilter = string.Empty;
            if (login_user.isStaffAdmin())
                searchFilter = string.Empty;
            else if (login_user.UserAccessList.Where(_ => _.Key.Equals("function_view_branch_request")).Count() >= 1)
                searchFilter = " AND(A.memberid = '" + login_user.UserId + "' OR A.BranchId IN (SELECT BranchId FROM tbl_Branch Where ParentId = " + login_user.BranchUniqueID + " OR Id = " + login_user.BranchUniqueID + ")) ";
            else
                searchFilter = " AND A.MemberID='" + login_user.UserId + "'";
            
            sql.AppendFormat(@"SELECT A.RequestID, A.RequestAt , D.Name  as 'ApplicantName' , C.Fullname , B.BANKLIST , A.ApplicationResult, A.MemberId, A.BranchId, D.MyCard,
	                    CASE WHEN PS.ProjectName is null 
						    THEN '-'--(SELECT TOP 1 ProjectName FROM tbl_loanlegalaction L WITH(NOLOCK) WHERE A.RequestID = L.RequestID)
						    ELSE  PS.ProjectName
						END AS ProjectName
                                FROM 
                                (SELECT * FROM tbl_Request WITH (NOLOCK) WHERE  StatusID ='S')A
                                LEFT JOIN
                                (
                                SELECT 
                                  requestid,'['+
                                  STUFF((
                                    SELECT ', ' +'{{ ""ID"" :""'+ BankID + '"", ""Name"" :""' + BankName+'""}}'
                                    FROM TBL_REQUESTBANK WITH (NOLOCK)
                                    WHERE  isdeleted=0 AND (requestid = TRB.requestid) 
                                    FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
                                  ,1,2,'')+']' AS BANKLIST
                                FROM TBL_REQUESTBANK TRB WITH (NOLOCK)
                                GROUP BY requestid
                                ) B ON A.RequestID = B.RequestID
                                LEFT JOIN tbl_MemberInfo C WITH (NOLOCK) ON A.MemberID = C.MemberId
                                LEFT JOIN tbl_applicants D WITH (NOLOCK) ON A.RequestID = D.RequestID AND D.ApplicantType='MA'
                                LEFT JOIN tbl_projectsettings PS WITH(NOLOCK) ON A.ProjectId = PS.ID
                                WHERE 1=1 ");

            sql.Append(searchFilter);

            if (!string.IsNullOrWhiteSpace(from) && !string.IsNullOrWhiteSpace(to) && DateTime.TryParse(from, out var outfrom) && DateTime.TryParse(to, out var outto))
                sql.Append(" and A.RequestAt between '" + from + "' and '" + to + " 23:59' ");

            TextBox txtMemberName = new TextBox();
            txtMemberName.Text = memberName;
            sql.Append(sqlString.searchTextBox("C.Fullname", txtMemberName, true, true, false));

            TextBox txtApplicantName = new TextBox();
            txtApplicantName.Text = applicantName;
            sql.Append(sqlString.searchTextBox("D.Name", txtApplicantName, true, true, false));

            TextBox txtRequestID = new TextBox();
            txtRequestID.Text = requestId;
            sql.Append(sqlString.searchTextBox("A.RequestID", txtRequestID, false, true, false));

            DropDownList ddlProjectName = new DropDownList();
            ddlProjectName.Items.Add(new ListItem { Text = "Select", Value = "0", Selected = projectId.Equals("0") ? true : false });
            ddlProjectName.Items.Add(new ListItem { Text = projectId, Value = projectId, Selected = projectId.Equals("0") ? false : true });
            sql.Append(sqlString.searchDropDownList("A.ProjectId", ddlProjectName, true, true, false));

            DropDownList ddlApplicationStatus = new DropDownList();
            ddlApplicationStatus.Items.Add(new ListItem { Text = "Select", Value = "0", Selected = applicationStatus.Equals("0") ? true : false });
            ddlApplicationStatus.Items.Add(new ListItem { Text = applicationStatus, Value = applicationStatus, Selected = applicationStatus.Equals("0") ? false : true });
            sql.Append(sqlString.searchDropDownList("A.ApplicationResult", ddlApplicationStatus, true, true, false));

            sql.Append(" order by ");

            //ordering
            if (sortBy != null && sortBy.Any())
            {
                int i = 0;
                foreach (var item in sortBy)
                {
                    string key = item.Key;
                    string value = item.Value;

                    if (i > 0)
                        sql.Append(",");

                    switch (key)
                    {
                        case "RequestId":
                            sql.Append("cast(A.RequestID as int) " + value);
                            break;
                        case "RequestAt":
                            sql.Append("RequestAt " + value);
                            break;
                        case "Fullname":
                            sql.Append("Fullname " + value);
                            break;
                        case "ApplicantName":
                            sql.Append("ApplicantName " + value);
                            break;
                        case "ProjectName":
                            sql.Append("ProjectName" + value);
                            break;
                        case "ApplicationResult":
                            sql.Append("ApplicationResult " + value);
                            break;
                        default:
                            break;
                    }

                    i++;
                }
            }
            else
            {
                sql.Append("RequestAt asc");
            }

            return sql.ToString();

        }
        
        private void updateTable() {
            //initialisation
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            string mainName = "", coName = "", mainIC = "", coIC = "", reqID = "";

            sql.Append("SELECT * FROM tbl_TDetailReport WITH (NOLOCK)");
            db.OpenTable(sql.ToString());
            if (db.RecordCount() > 0)
            {
                while (!db.Eof())
                {
                    reqID = db.Item("RequestID");
                    if (!db.Item("MainName").Equals(""))
                    {
                        mainName = db.Item("MainName");
                        mainIC = db.Item("MainMyCard");
                    }
                    if (!db.Item("CoName").Equals(""))
                    {
                        coName = db.Item("CoName");
                        coIC = db.Item("CoMyCard");
                    }

                    db.MoveNext();
                    if (reqID.Equals(db.Item("RequestID")))
                    {
                        if (!db.Item("MainName").Equals(""))
                        {
                            mainName = db.Item("MainName");
                            mainIC = db.Item("MainMyCard");
                        }
                        if (!db.Item("CoName").Equals(""))
                        {
                            coName = db.Item("CoName");
                            coIC = db.Item("CoMyCard");
                        }


                    }
                    else
                    {
                        db.MovePrevious();

                    }
                }
            }
        }

        private string convertTable(string inputSql1, string inputSql2)
        {
            wwdb db = new wwdb();
            wwdb actionDb = new wwdb();
            DataTable dataTable = new DataTable();
            StringBuilder sql = new StringBuilder();
            StringBuilder insertSql = new StringBuilder();
            StringBuilder selectSql = new StringBuilder();
            int count = 2;
            //sql.Append(inputSql1);

            actionDb.Execute("DELETE FROM tbl_TDetailReport WHERE CreatedBy=N'" + secure.RC(login_user.UserId) + "'");
            while (count > 0)
            {
                if (count.Equals(2))
                {
                    sql.Clear();
                    sql.Append(inputSql1);
                }
                else if (count.Equals(1)) {
                    sql.Clear();
                    sql.Append(inputSql2);
                }

                db.OpenTable(sql.ToString());
                if (db.RecordCount() > 0)
                {
                    string comment = "";
                    string tempReqID = "", reqID = "", mainName = "", coName = "", mainIC = "", coIC = "";
                    decimal creditCard = 0, houseLoan = 0, carLoan = 0, personalLoan = 0, others = 0;
                    
                    while (!db.Eof())
                    {
                        if (tempReqID.Equals(""))
                            tempReqID = db.Item("RequestID");

                        reqID = db.Item("RequestID");

                        if (tempReqID.Equals(reqID)) //same request
                        {
                            comment = db.Item("Comment").Replace("</br>", "\n");
                            comment = comment.Replace("&amp;", "&");
                            comment = comment.Replace("&gt;", ">");
                            comment = comment.Replace("**", "\n**");

                            if (mainIC.Equals("") || mainIC.Equals("0"))
                            {
                                mainIC = db.Item("MainMyCard");
                                mainName = db.Item("MainName");
                            }
                            if (coIC.Equals("") || coIC.Equals("0"))
                            {
                                coIC = db.Item("CoMyCard");
                                coName = db.Item("CoName");
                            }
                            creditCard += ConvertHelper.ConvertToDecimal(db.Item("CreditCard"), 0);
                            houseLoan += ConvertHelper.ConvertToDecimal(db.Item("HousingLoan"), 0);
                            carLoan += ConvertHelper.ConvertToDecimal(db.Item("CarLoan"), 0);
                            personalLoan += ConvertHelper.ConvertToDecimal(db.Item("PersonalLoan"), 0);
                            others += ConvertHelper.ConvertToDecimal(db.Item("Others"), 0);
                        }
                        else //next request
                        {
                            if (mainIC.Equals("0") || mainIC.Equals(""))
                                mainIC = "-";
                            if (coIC.Equals("0") || coIC.Equals(""))
                                coIC = "-";
                            if (mainName.Equals("0") || mainName.Equals(""))
                                mainName = "-";
                            if (coName.Equals("0") || coName.Equals(""))
                                coName = "-";

                            tempReqID = reqID;

                            db.MovePrevious();
                            //check if requestID already exist before insert
                            selectSql.Clear();
                            selectSql.Append("SELECT * FROM tbl_TDetailReport WITH (NOLOCK) WHERE CreatedBy=N'"+secure.RC(login_user.UserId)+"' " +
                                "AND RequestID=N'"+secure.RC(db.Item("RequestID"))+"'");
                            actionDb.OpenTable(selectSql.ToString());
                            if (actionDb.RecordCount() == 0)//not exist
                            {
                                insertSql.Clear();
                                insertSql.Append("INSERT INTO tbl_TDetailReport (RequestID, MainName, MainMyCard, CoName, CoMyCard, ApplicationResult, RequestAt, ProjectName, ");
                                insertSql.Append("Tenure, LoanAmount, TotalCommitment, CreditCard, HousingLoan, CarLoan, PersonalLoan, Others, ");
                                insertSql.Append("Comment, ");
                                insertSql.Append("IncomeRequired, CreatedBy) ");
                                insertSql.Append("VALUES(N'" + secure.RC(db.Item("RequestID")) + "'");
                                insertSql.Append(",'" + mainName + "','" + mainIC + "'");
                                insertSql.Append(",'" + coName + "','" + coIC + "','" + db.Item("ApplicationResult") + "'");
                                insertSql.Append(",'" + db.Item("RequestAt") + "','" + db.Item("ProjectName") + "'," + db.Item("Tenure"));
                                insertSql.Append("," + db.Item("LoanAmount") + ",'" + db.Item("TotalCommitment") + "'," + creditCard);
                                insertSql.Append("," + houseLoan + "," + carLoan + "," + personalLoan);
                                insertSql.Append("," + others + ",'" + secure.RC(comment) + "'," + db.Item("IncomeRequired") +
                                    ",N'" + secure.RC(login_user.UserId) + "');");
                                actionDb.Execute(insertSql.ToString());
                                //db.MoveNext();
                                //reset
                                comment = "";
                                //tempReqID = reqID; //tempReqID = db.Item("RequestID");
                                //creditCard = ConvertHelper.ConvertToDecimal(db.Item("CreditCard"), 0);
                                //houseLoan = ConvertHelper.ConvertToDecimal(db.Item("HousingLoan"), 0); ;
                                //carLoan = ConvertHelper.ConvertToDecimal(db.Item("CarLoan"), 0); ;
                                //personalLoan = ConvertHelper.ConvertToDecimal(db.Item("PersonalLoan"), 0); ;
                                //others = ConvertHelper.ConvertToDecimal(db.Item("Others"), 0); ;
                                mainName = "";
                                coName = "";
                                mainIC = "";
                                coIC = "";
                                creditCard = 0;
                                houseLoan = 0;
                                carLoan = 0;
                                personalLoan = 0;
                                others = 0;
                            }
                        }
                        db.MoveNext(); //next row
                        if (db.Eof())
                        {
                            db.MovePrevious();
                            //check if requestID already exist before insert
                            selectSql.Clear();
                            selectSql.Append("SELECT * FROM tbl_TDetailReport WHERE CreatedBy=N'" + secure.RC(login_user.UserId) + "' " +
                                "AND RequestID=N'" + secure.RC(db.Item("RequestID")) + "'");
                            actionDb.OpenTable(selectSql.ToString());
                            if (actionDb.RecordCount() == 0)//not exist
                            {
                                if (mainIC.Equals("0") || mainIC.Equals(""))
                                    mainIC = "-";
                                if (coIC.Equals("0") || coIC.Equals(""))
                                    coIC = "-";
                                if (mainName.Equals("0") || mainName.Equals(""))
                                    mainName = "-";
                                if (coName.Equals("0") || coName.Equals(""))
                                    coName = "-";

                                insertSql.Clear();
                                insertSql.Append("INSERT INTO tbl_TDetailReport (RequestID, MainName, MainMyCard, CoName, CoMyCard, ApplicationResult, RequestAt, ProjectName, ");
                                insertSql.Append("Tenure, LoanAmount, TotalCommitment, CreditCard, HousingLoan, CarLoan, PersonalLoan, Others, ");
                                insertSql.Append("Comment, ");
                                insertSql.Append("IncomeRequired, CreatedBy) ");
                                insertSql.Append("VALUES(N'" + secure.RC(db.Item("RequestID")) + "'");
                                insertSql.Append(",'" + mainName + "','" + mainIC + "'");
                                insertSql.Append(",'" + coName + "','" + coIC + "','" + db.Item("ApplicationResult") + "'");
                                insertSql.Append(",'" + db.Item("RequestAt") + "','" + db.Item("ProjectName") + "'," + db.Item("Tenure"));
                                insertSql.Append("," + db.Item("LoanAmount") + ",'" + db.Item("TotalCommitment") + "'," + creditCard);
                                insertSql.Append("," + houseLoan + "," + carLoan + "," + personalLoan);
                                insertSql.Append("," + others + ",'" + secure.RC(comment) + "'," + db.Item("IncomeRequired") +
                                    ",N'" + secure.RC(login_user.UserId) + "');");
                                actionDb.Execute(insertSql.ToString());
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                    count--;
                }
            }
            //updateTable();

            string openSql = "SELECT RequestID, MainName, MainMyCard, CoName, CoMyCard, ApplicationResult, RequestAt, ProjectName, Tenure, CAST(LoanAmount AS int) AS LoanAmount, " +
                "CAST(TotalCommitment AS int) AS TotalCommitment, CAST(CreditCard AS int) AS CreditCard, " +
                "CAST(HousingLoan AS int) AS HousingLoan, CAST(CarLoan AS int) AS CarLoan, CAST(PersonalLoan AS int) AS PersonalLoan, " +
                "CAST(Others AS int) AS Others, Comment, CAST(IncomeRequired AS int) AS IncomeRequired FROM tbl_TDetailReport WHERE CreatedBy = N'" + secure.RC(login_user.UserId) + "'" +
                "ORDER BY MainName";

            return openSql;
        }

        private string getReport1(string from, string to, string applicantname, string requestID, string applicationStatus, string projectID)
        {
            StringBuilder sql = new StringBuilder();

            sql.Append("	SELECT DISTINCT 	");
            sql.Append("	                         CAST(r.RequestID AS int) AS RequestID, CASE WHEN a.ApplicantType = 'MA' THEN a.Name ELSE '-' END AS 'MainName', CASE WHEN a.ApplicantType = 'MA' THEN a.MyCard ELSE '-' END AS 'MainMyCard', 	");
            sql.Append("	                         CASE WHEN a.ApplicantType = 'CA' THEN a.Name ELSE '-' END AS 'CoName', CASE WHEN a.ApplicantType = 'CA' THEN a.MyCard ELSE '-' END AS 'CoMyCard', 	");
            sql.Append("	                         CASE WHEN r.ApplicationResult = 'A' THEN 'Green' WHEN r.ApplicationResult = 'M' THEN 'Yellow' WHEN r.ApplicationResult = 'D' THEN 'Red' END AS ApplicationResult, r.RequestAt, ps.ProjectName, lla.Tenure, 	");
            sql.Append("	                         lla.LoanAmount, CASE WHEN ld.Repayment IS NULL THEN 0 ELSE	");
            sql.Append("	                             (SELECT        SUM(a.Repayment)	");
            sql.Append("	                               FROM            (SELECT        Repayment	");
            sql.Append("	                                                         FROM            tbl_LoanDetail tld	");
            sql.Append("	                                                         WHERE        ld.RequestID = r.RequestID AND tld.RequestID = ld.RequestID) a) END AS 'TotalCommitment', CASE WHEN ld.LoanType = 'CC' THEN	");
            sql.Append("	                             (SELECT        SUM(a.Repayment)	");
            sql.Append("	                               FROM            (SELECT        Repayment	");
            sql.Append("	                                                         FROM            tbl_LoanDetail	");
            sql.Append("	                                                         WHERE        tbl_LoanDetail.RequestID = r.RequestID AND tbl_LoanDetail.RequestID = ld.RequestID AND LoanType = 'CC' AND ApplicantType = a.ApplicantType) a) ELSE 0 END AS 'CreditCard', 	");
            sql.Append("	                         CASE WHEN ld.LoanType = 'HL' THEN	");
            sql.Append("	                             (SELECT        SUM(a.Repayment)	");
            sql.Append("	                               FROM            (SELECT        Repayment	");
            sql.Append("	                                                         FROM            tbl_LoanDetail	");
            sql.Append("	                                                         WHERE        tbl_LoanDetail.RequestID = r.RequestID AND tbl_LoanDetail.RequestID = ld.RequestID AND LoanType = 'HL' AND ApplicantType = a.ApplicantType) a) ELSE 0 END AS 'HousingLoan', 	");
            sql.Append("	                         CASE WHEN ld.LoanType = 'HP' THEN	");
            sql.Append("	                             (SELECT        SUM(a.Repayment)	");
            sql.Append("	                               FROM            (SELECT        Repayment	");
            sql.Append("	                                                         FROM            tbl_LoanDetail	");
            sql.Append("	                                                         WHERE        tbl_LoanDetail.RequestID = r.RequestID AND tbl_LoanDetail.RequestID = ld.RequestID AND LoanType = 'HP' AND ApplicantType = a.ApplicantType) a) ELSE 0 END AS 'CarLoan', 	");
            sql.Append("	                         CASE WHEN ld.LoanType = 'PL' THEN	");
            sql.Append("	                             (SELECT        SUM(a.Repayment)	");
            sql.Append("	                               FROM            (SELECT        Repayment	");
            sql.Append("	                                                         FROM            tbl_LoanDetail	");
            sql.Append("	                                                         WHERE        tbl_LoanDetail.RequestID = r.RequestID AND tbl_LoanDetail.RequestID = ld.RequestID AND LoanType = 'PL' AND ApplicantType = a.ApplicantType) a) ELSE 0 END AS 'PersonalLoan', 	");
            sql.Append("	                         CASE WHEN (ld.LoanType = 'XP' OR	");
            sql.Append("	                         ld.LoanType = 'CL' OR	");
            sql.Append("	                         ld.LoanType = 'OD') THEN	");
            sql.Append("	                             (SELECT        SUM(a.Repayment)	");
            sql.Append("	                               FROM            (SELECT        Repayment	");
            sql.Append("	                                                         FROM            tbl_LoanDetail	");
            sql.Append("	                                                         WHERE        tbl_LoanDetail.RequestID = r.RequestID AND tbl_LoanDetail.RequestID = ld.RequestID AND (LoanType = 'XP' OR	");
            sql.Append("	                                                                                   LoanType = 'CL' OR	");
            sql.Append("	                                                                                   LoanType = 'OD') AND ApplicantType = a.ApplicantType) a) ELSE 0 END AS 'Others', aca.Comment, li.IncomeRequired	");
            sql.Append("	FROM            tbl_LoanInfo AS li WITH (NOLOCK) INNER JOIN	");
            sql.Append("	                         tbl_Request AS r WITH (NOLOCK) ON li.RequestID = r.RequestID INNER JOIN	");
            sql.Append("	                         tbl_LoanLegalAction AS lla WITH (NOLOCK) ON r.RequestID = lla.RequestID INNER JOIN	");
            sql.Append("	                         tbl_ApplicantCreditAdvisor AS aca WITH (NOLOCK) ON r.RequestID = aca.RequestID INNER JOIN	");
            sql.Append("	                         tbl_ProjectSettings AS ps WITH (NOLOCK) ON r.ProjectID = ps.ID LEFT OUTER JOIN	");
            sql.Append("	                         tbl_LoanDetail AS ld WITH (NOLOCK) ON r.RequestID = ld.RequestID FULL OUTER JOIN	");
            sql.Append("	                         tbl_Applicants AS a WITH (NOLOCK) ON ld.RequestID = a.RequestID	");
            sql.Append("	WHERE        (lla.IsDeleted = 0) AND (ld.IsDeleted = 0) AND (r.isDeleted = 0) AND (aca.IsDeleted = 0) AND (a.IsDeleted = 0) AND (li.IsDeleted = 0) AND (r.StatusID = 'S') AND (ps.IsDeleted = 0)	");

            if (!string.IsNullOrWhiteSpace(from) && !string.IsNullOrWhiteSpace(to) && DateTime.TryParse(from, out var outfrom) && DateTime.TryParse(to, out var outto))
                sql.Append(" and r.RequestAt between '" + from + "' and '" + to + " 23:59' ");

            TextBox txtApplicantName = new TextBox();
            txtApplicantName.Text = applicantname;
            sql.Append(sqlString.searchTextBox("a.Name", txtApplicantName, true, true, false));

            TextBox txtRequestID = new TextBox();
            txtRequestID.Text = requestID;
            sql.Append(sqlString.searchTextBox("r.RequestID", txtRequestID, false, true, false));

            DropDownList ddlProjectName = new DropDownList();
            ddlProjectName.Items.Add(new ListItem { Text = "Select", Value = "0", Selected = projectID.Equals("0") ? true : false });
            ddlProjectName.Items.Add(new ListItem { Text = projectID, Value = projectID, Selected = projectID.Equals("0") ? false : true });
            sql.Append(sqlString.searchDropDownList("r.ProjectId", ddlProjectName, true, true, false));

            DropDownList ddlApplicationStatus = new DropDownList();
            ddlApplicationStatus.Items.Add(new ListItem { Text = "Select", Value = "0", Selected = applicationStatus.Equals("0") ? true : false });
            ddlApplicationStatus.Items.Add(new ListItem { Text = applicationStatus, Value = applicationStatus, Selected = applicationStatus.Equals("0") ? false : true });
            sql.Append(sqlString.searchDropDownList("r.ApplicationResult", ddlApplicationStatus, true, true, false));

            sql.Append("	GROUP BY r.RequestID, a.Name, ld.RequestID, a.ApplicantType, a.MyCard, r.ApplicationResult, r.RequestAt, ps.ProjectName, lla.Tenure, lla.LoanAmount, ld.Repayment, ld.LoanType, aca.Comment, li.IncomeRequired, 	");
            sql.Append("	                         ld.ApplicantType	");
            sql.Append("	ORDER BY RequestID	");

            return sql.ToString();
        }
        private string getReport2(string from, string to, string applicantname, string requestID, string applicationStatus, string projectID)
        {
            StringBuilder sql = new StringBuilder();

            sql.Append("	SELECT DISTINCT 	");
            sql.Append("	                         CAST(r.RequestID AS int) AS RequestID, CASE WHEN a.ApplicantType = 'MA' THEN a.Name ELSE '-' END AS 'MainName', CASE WHEN a.ApplicantType = 'MA' THEN a.MyCard ELSE '-' END AS 'MainMyCard', 	");
            sql.Append("	                         CASE WHEN a.ApplicantType = 'CA' THEN a.Name ELSE '-' END AS 'CoName', CASE WHEN a.ApplicantType = 'CA' THEN a.MyCard ELSE '-' END AS 'CoMyCard', 	");
            sql.Append("	                         CASE WHEN r.ApplicationResult = 'A' THEN 'Green' WHEN r.ApplicationResult = 'M' THEN 'Yellow' WHEN r.ApplicationResult = 'D' THEN 'Red' END AS ApplicationResult, r.RequestAt, ps.ProjectName, lla.Tenure, 	");
            sql.Append("	                         lla.LoanAmount, 0 AS 'TotalCommitment', 0 AS 'CreditCard', 0 AS 'HousingLoan', 0 AS 'CarLoan', 0 AS 'PersonalLoan', 0 AS 'Others', aca.Comment, li.IncomeRequired	");
            sql.Append("	FROM            tbl_Request AS r WITH (NOLOCK) INNER JOIN	");
            sql.Append("	                         tbl_Applicants AS a WITH (NOLOCK) ON r.RequestID = a.RequestID INNER JOIN	");
            sql.Append("	                         tbl_ProjectSettings AS ps WITH (NOLOCK) ON r.ProjectID = ps.ID INNER JOIN	");
            sql.Append("	                         tbl_LoanLegalAction AS lla WITH (NOLOCK) ON r.RequestID = lla.RequestID INNER JOIN	");
            sql.Append("	                         tbl_ApplicantCreditAdvisor AS aca WITH (NOLOCK) ON r.RequestID = aca.RequestID INNER JOIN	");
            sql.Append("	                         tbl_LoanInfo AS li WITH (NOLOCK) ON r.RequestID = li.RequestID CROSS JOIN	");
            sql.Append("	                         tbl_LoanDetail WITH (NOLOCK)	");
            sql.Append("	WHERE        (r.RequestID NOT IN (tbl_LoanDetail.RequestID)) AND (r.isDeleted = 0) AND (a.IsDeleted = 0)	");

            if (!string.IsNullOrWhiteSpace(from) && !string.IsNullOrWhiteSpace(to) && DateTime.TryParse(from, out var outfrom) && DateTime.TryParse(to, out var outto))
                sql.Append(" and r.RequestAt between '" + from + "' and '" + to + " 23:59' ");

            TextBox txtApplicantName = new TextBox();
            txtApplicantName.Text = applicantname;
            sql.Append(sqlString.searchTextBox("a.Name", txtApplicantName, true, true, false));

            TextBox txtRequestID = new TextBox();
            txtRequestID.Text = requestID;
            sql.Append(sqlString.searchTextBox("r.RequestID", txtRequestID, false, true, false));

            DropDownList ddlProjectName = new DropDownList();
            ddlProjectName.Items.Add(new ListItem { Text = "Select", Value = "0", Selected = projectID.Equals("0") ? true : false });
            ddlProjectName.Items.Add(new ListItem { Text = projectID, Value = projectID, Selected = projectID.Equals("0") ? false : true });
            sql.Append(sqlString.searchDropDownList("r.ProjectId", ddlProjectName, true, true, false));

            DropDownList ddlApplicationStatus = new DropDownList();
            ddlApplicationStatus.Items.Add(new ListItem { Text = "Select", Value = "0", Selected = applicationStatus.Equals("0") ? true : false });
            ddlApplicationStatus.Items.Add(new ListItem { Text = applicationStatus, Value = applicationStatus, Selected = applicationStatus.Equals("0") ? false : true });
            sql.Append(sqlString.searchDropDownList("r.ApplicationResult", ddlApplicationStatus, true, true, false));

            return sql.ToString(); ;
        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string ApplicationResult = DataBinder.Eval(e.Row.DataItem, "ApplicationResult").ToString();
                int resultIndex = gridView.GetColumnIndexByName(gv, Resources.resource.Customer_Profile);
                Label lblStatus = (Label)e.Row.FindControl("lblStatus");

                Dictionary<string, Color> colorlist = new Dictionary<string, Color>();
                colorlist.Add("A", Color.Green);
                colorlist.Add("D", Color.Red);
                colorlist.Add("M", Color.Yellow);

                lblStatus.Text = colorlist[ApplicationResult].Name;

                e.Row.Cells[14].Width = Unit.Pixel(250);
            }
        }
        
    }
}