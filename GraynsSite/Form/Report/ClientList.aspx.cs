﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Model;
using Synergy.Util;
using System.Text;
using Newtonsoft.Json.Linq;
using System.Drawing;
using Synergy.Helper;
using Synergy.Controller;
using System.Data.SqlClient;
using System.Globalization;
using System.Data;

namespace HJT.Form.Report
{
    public partial class ClientList : Page
    {
        LoginUserModel login_user = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];
            divreqId.Visible = false;
            divreqIdR.Visible = false;
            if (!IsPostBack)
            {
                binddata();
                CheckACL();
                bindTotalCount();
                bindClientList(null);
                sqlString.bindControl(gvR, getRequestList());
            }

            if (gv.Rows.Count > 0)
                gridView.ApplyPaging(gv, -1);

            if (gvR.Rows.Count > 0)
                gridView.ApplyPaging(gvR, -1);
        }

        #region Data Binding
        private void bindTotalCount()
        {
            wwdb db = new wwdb();
            db.OpenTable(getClientList(null, true, true));
            if (db.RecordCount() > 0)
                if (string.IsNullOrEmpty(litTotal.Text))
                    litTotal.Text = Resources.resource.Total + " : " + ConvertHelper.ConvertToInt(db.Item("Count"), 0).ToString("N0");
        }
        private void bindClientList(IDictionary<string, string> sortBy, bool selectAll = false)
        {
            wwdb db = new wwdb();
            DataTable dt = db.getDataTable(getClientList(sortBy, selectAll));

            gv.DataSource = dt;
            gv.DataBind();
            bindShowingCount(dt);
        }
        private void bindShowingCount(DataTable dt)
        {
            if (dt.Rows.Count > 0)
                litCurrent.Text = Resources.resource.Showing + " : " + dt.Rows.Count.ToString("N0");
        }
        private void binddata()
        {

            sqlString.bindControl(ddlBranch, "select BranchName as '0', BranchID as '1' " +
                "from tbl_branch where isdeleted = '0' " +
                "order by '0'", "0", "1", true);

            //State
            string stateSql = string.Format(@"
                SELECT st.StateName as '0', st.StateID as '1'
                FROM tbl_State st WITH (nolock)
                WHERE st.isDeleted = 0
                ORDER BY st.StateName
            ");
            sqlString.bindControl(ddlState, stateSql, "0", "1", true);

            divBranch.Visible = login_user.isStaffAdmin();

            if (login_user.isBranchManager())
            {
                //ddlBranch.SelectedValue = login_user.BranchID;
            }
            sqlString.bindControl(ddlStatus, "select  ParameterName as '0' , ParameterValue  as '1' from tbl_parameter where category='ApplicationStatus'", "0", "1", true);
            //combine requestList

            sqlString.bindControl(ddlApplicationStatus, "SELECT ParameterName AS '0' , ParameterValue AS '1' FROM tbl_Parameter WITH (NOLOCK) WHERE Category='ApplicationResult'", "0", "1", true);

            foreach (ListItem li in ddlApplicationStatus.Items)
            {
                if (li.Value != "0")
                {
                    li.Text = sqlString.changeLanguage("ResultColor_" + li.Value);

                }
            }

            wwdb db = new wwdb();
            int branchId = 0;
            int parentID = 0;
            StringBuilder sql = new StringBuilder();
            sql.Clear();
            sql.AppendFormat(@"
                SELECT tb.ID FROM tbl_MemberInfo tmi 
                INNER JOIN tbl_Login tl ON tmi.MemberId = tl.login_id 
                INNER JOIN tbl_Branch tb ON tl.BranchID = tb.BranchID
                INNER JOIN (
                    SELECT DISTINCT trm.MenuID, '{0}' AS MemberId 
                    FROM tbl_RoleMenu trm 
                    WHERE trm.RoleID IN (
                        SELECT RoleCode 
                        FROM tbl_MemberRole 
                        WHERE MemberId = '{0}')) 
                    AS trm ON tmi.MemberId = trm.MemberId
                INNER JOIN tbl_Menu tm ON trm.MenuID = tm.rowID
                WHERE tmi.MemberId = '{0}' AND tm.[Type] = 2 AND tm.[Key] = 'function_project_restrict_branch'
            ", login_user.UserId);
            db.OpenTable(sql.ToString());
            StringBuilder branchQuery = new StringBuilder();
            branchQuery.Append("select ProjectName as '0', ID as '1' from tbl_ProjectSettings WHERE status = 'A' AND IsDeleted='false' ");
            if (login_user.isStaffAdmin()) //disable uac for admin
                branchQuery.Append("order by [0] ");
            else
            {
                if (db.RecordCount() > 0)//UAC is checked
                {
                    branchId = ConvertHelper.ConvertToInt(db.Item("ID").ToString(), 0);
                    branchQuery.Append(" AND BranchId IN (SELECT ID FROM tbl_Branch Where Id = " + branchId + ") ORDER BY ProjectName");
                }
                else//UAC unchecked
                {
                    sql.Clear();
                    sql.AppendFormat(@"
                        select b.ID, b.parentID 
                        from tbl_MemberInfo mi with (nolock)
                        inner join tbl_Login l with (nolock) on mi.MemberId = l.login_id 
                        inner join tbl_Branch b with (nolock) on l.BranchID = b.BranchID 
                        where mi.MemberId = '{0}' 
                    ", login_user.UserId);
                    db.OpenTable(sql.ToString());
                    branchId = ConvertHelper.ConvertToInt(db.Item("ID").ToString(), 0);
                    parentID = ConvertHelper.ConvertToInt(db.Item("parentID").ToString(), 0);
                    if (parentID.Equals(0))//parent
                        branchQuery.AppendFormat(@" 
                            AND BranchId IN (
                                SELECT distinct ID 
                                FROM tbl_Branch with (nolock)
                                Where ParentId = {0} OR Id = {0}) 
                            ORDER BY ProjectName
                        ", branchId);
                    else//sub
                        branchQuery.AppendFormat(@" 
                            AND BranchId IN( 
                            SELECT distinct p.ID FROM tbl_Branch s with (nolock), tbl_Branch p with (nolock)
                            Where (s.ParentId = '{0}' OR p.Id = '{0}') or (s.ParentId = p.Id and s.ID = '{0}')) 
                            ORDER BY ProjectName
                        ", branchId);
                }
            }
            sqlString.bindControl(ddlProject, branchQuery.ToString(), "0", "1", true);

            if (login_user.isStaffAdmin())
            {
                sql.Clear();
                sql.Append(@"
                    select pa.id as '1', pa.BranchName as '0' 
                    from tbl_Branch pa with (nolock)
                    where pa.ParentId is null and pa.IsDeleted = 0
                    order by BranchName; 
                ");
                sqlString.bindControl(ddlParentBranch, sql.ToString(), "0", "1", true);
            }

            //Sub Branch Filter
            sql.Clear();
            if (!login_user.isStaffAdmin())
            {
                sql.AppendFormat(@"
                    select distinct b.ID as '1', b.BranchName as '0' from tbl_Branch b with (nolock)
                    where b.ParentId = (
	                    select isnull(s.ParentId, s.ID) from tbl_Branch s with (nolock) 
	                    where s.ID='{0}')
                    and b.IsDeleted = 0
                    order by [0] asc
            ", login_user.BranchUniqueID);
            }
            else
            {
                sql.AppendFormat(@"
                    select distinct b.ID as '1', b.BranchName as '0' from tbl_Branch b with (nolock)
                    where b.ParentId in (
	                    select isnull(s.ParentId, s.ID) from tbl_Branch s with (nolock)
					    where s.ParentId is not null)
                    and b.IsDeleted = 0
                    order by [0] asc
                ");
            }
            sqlString.bindControl(ddlClientListBranch, sql.ToString(), "0", "1", true);
        }
        protected void ddlParentBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (login_user.isStaffAdmin())
            {
                StringBuilder sql = new StringBuilder();

                sql.Clear();
                sql.Append(@"
                    select ps.ProjectName as '0', ps.ID as '1' from tbl_ProjectSettings ps, tbl_Branch b
                    WHERE ps.status = 'A'AND ps.IsDeleted = 'false' and b.IsDeleted = 0 and b.ID = ps.BranchId 
                ");
                //sql.Append("and(b.ID = '1013' or b.ParentId = '1013') ");
                if (ddlParentBranch.SelectedIndex > 0)
                {
                    sql.Append(sqlString.searchDropDownList("(b.ID", ddlParentBranch, true, true, false));
                    sql.Append(sqlString.searchDropDownList("b.ParentId", ddlParentBranch, true, false, true));
                    sql.Append(") ");
                }
                sql.Append(" ORDER BY [0]");
                sqlString.bindControl(ddlProject, sql.ToString(), "0", "1", true);

                sql.Clear();
                sql.AppendFormat(@"
                    select distinct b.ID as '1', b.BranchName as '0' from tbl_Branch b with (nolock)
                    where b.ParentId in (
	                    select isnull(s.ParentId, s.ID) from tbl_Branch s with (nolock)
					    where s.ParentId is not null {0})
                    and b.IsDeleted = 0
                    order by [0] asc
                ", ddlParentBranch.SelectedIndex > 0 ? sqlString.searchDropDownList("s.ParentId", ddlParentBranch, true, true, false) : "");
                sqlString.bindControl(ddlClientListBranch, sql.ToString(), "0", "1", true);
            }
        }

        private string getClientList(IDictionary<string, string> sortBy, bool selectAll = false, bool getCount = false)
        {
            StringBuilder sql = new StringBuilder();
            StringBuilder sql2 = new StringBuilder();
            StringBuilder sql3 = new StringBuilder();
            StringBuilder selectData = new StringBuilder();

            string searchFilter = string.Empty;
            if (login_user.isStaffAdmin())
                searchFilter = string.Empty;
            else if (login_user.UserAccessList.Where(_ => _.Key.Equals("function_view_branch_request")).Count() >= 1)
                searchFilter = " AND(A.memberid = '" + login_user.UserId + "' OR A.BranchId IN (SELECT BranchId FROM tbl_Branch Where ParentId = " + login_user.BranchUniqueID + " OR Id = " + login_user.BranchUniqueID + ")) ";
            else
                searchFilter = " AND A.MemberID='" + login_user.UserId + "'";

            sql2.Clear();
            sql3.Clear();
            if (login_user.isStaffAdmin())
            {
                sql2.AppendFormat(@"left join dbo.tbl_ProjectSettings ps with(nolock) on tbl_Request.ProjectId = PS.ID 
                            left join dbo.tbl_Branch br with(nolock) on br.BranchID = tbl_Request.BranchID or br.ID = ps.BranchId ");

                sql3.Append(sqlString.searchDropDownList("(br.ID", ddlParentBranch, true, true, false));
                sql3.Append(sqlString.searchDropDownList("br.ParentId", ddlParentBranch, true, false, true));
                if (ddlParentBranch.SelectedIndex > 0)
                    sql3.Append(") ");
                if (ddlClientListBranch.SelectedIndex > 0)
                    sql3.Append(sqlString.searchDropDownList("br.ID", ddlClientListBranch, true, true, false));
            }

            if (!(string.IsNullOrEmpty(txtFrom.Text) && string.IsNullOrEmpty(txtTo.Text)
                && string.IsNullOrEmpty(txtMemberName.Text) && string.IsNullOrEmpty(txtRequestID.Text)
                && string.IsNullOrEmpty(txtApplicantName.Text)
                && ddlProject.SelectedIndex <= 0 && ddlApplicationStatus.SelectedIndex <= 0
                && ddlParentBranch.SelectedIndex <= 0))
                selectAll = true; //searching, search from all data

            string noOfrows = getNumberOfRows();
            selectData.Clear();
            selectData.AppendFormat(@"
                {0} A.RequestID, A.RequestAt , A.ShowMACCRIS, A.ShowCACCRIS, 
                                (ISNULL('Name 1: ' +
	                                (
	                                SELECT distinct ta.[Name] FROM tbl_Applicants ta
	                                WHERE ta.RequestID=a.RequestId AND ta.ApplicantType = 'MA'
	                                ),
	                                ISNULL('Name 1: ' +
		                                (
		                                SELECT distinct ta.[Name] FROM tbl_TApplicants ta
		                                WHERE ta.RequestID=a.RequestId AND ta.ApplicantType = 'MA'
		                                )
		                                ,'')
	                                ) + ' ' + ISNULL('<br />Name 2: ' +
				                                (
				                                SELECT distinct ta.[Name] FROM tbl_Applicants ta
				                                WHERE ta.RequestID=a.RequestId AND ta.ApplicantType = 'CA'
				                                ),
				                                ISNULL('<br />Name 2: ' +
					                                (
					                                SELECT distinct ta.[Name] FROM tbl_TApplicants ta
					                                WHERE ta.RequestID=a.RequestId AND ta.ApplicantType = 'CA'
					                                )
				                                ,'')
				                                )
                                ) as 'ApplicantName', A.URL,
                                C.Fullname, B.BANKLIST, A.ApplicationResult, A.MemberId, A.BranchId, A.ConsentFormId, ISNULL(A.Price, '-') AS Price, 
                                A.PaymentStatus, A.PayStatus,
                                CASE WHEN PS.ProjectName is null 
	                                THEN 
                                        (CASE WHEN A.ProjectName IS NULL
		                                THEN '-'
		                                ELSE A.ProjectName
		                                END)
	                                ELSE  PS.ProjectName 
                                END AS ProjectName,
                                E.DocList, A.isFinal, ts.StateName, sb.BranchName
            "
            , selectAll ? "" :
                string.Format("TOP ({0})", noOfrows)
            );

            sql.Clear();
            sql.AppendFormat(@"SELECT {4}
                                FROM 
                                    (SELECT distinct tbl_Request.*,
                                    (SELECT COUNT(*) FROM tbl_CTOS_Result tcr 
		                                WHERE tcr.Request_Id = tbl_Request.RequestID AND tcr.IsActive = 1 AND IsCoApplicant = 0) as ShowMACCRIS, 
	                                (SELECT COUNT(*) FROM tbl_CTOS_Result tcr 
		                                WHERE tcr.Request_Id = tbl_Request.RequestID AND tcr.IsActive = 1 AND IsCoApplicant = 1) as ShowCACCRIS, 
                                    'Request: MYR ' + CONVERT(VARCHAR,(ISNULL(PerRequestPrice,0) * NumofApplicant))  
                                    + ' Successful: MYR ' + CONVERT(VARCHAR,(ISNULL(PerSuccessfulCasePrice,0) * NumofApplicant)) AS Price, 
	                                PaymentStatus AS PayStatus,	aca.isFinal,
										('https://api.whatsapp.com/send?phone=' + 
										(SELECT 
											case when tmi.Mobile like '%*%'
												then null
                                            when tmi.Mobile like '6%'
                                                then tmi.Mobile
											else
												'6' + tmi.Mobile
											end
										from tbl_MemberInfo tmi with (nolock)
										where tmi.MemberId = tbl_Request.RequestBy) +
										'&text=Hi I''m staff from WhoPay Sdn Bhd')
									as 'URL'
                                    FROM tbl_Request WITH (NOLOCK) 
                                    LEFT JOIN 
	                                (SELECT tbl_Applicants.RequestID, COUNT(*) AS NumofApplicant 
										FROM tbl_Applicants GROUP BY tbl_Applicants.RequestID) ta 
										ON tbl_Request.RequestID = ta.RequestID
	                                left join tbl_ApplicantCreditAdvisor aca on aca.RequestID=tbl_Request.RequestID 
                                    {0}
                                    WHERE tbl_Request.StatusID ='S' {1}
									)A
                                LEFT JOIN
                                    (
                                    SELECT 
                                        requestid,'['+
                                        STUFF((
                                        SELECT ', ' +'{{ ""ID"" :""'+ BankID + '"", ""Name"" :""' + BankName+'""}}'
                                        FROM TBL_REQUESTBANK WITH (NOLOCK)
                                        WHERE  isdeleted=0 AND (requestid = TRB.requestid) 
                                        FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
                                        ,1,2,'')+']' AS BANKLIST
                                    FROM TBL_REQUESTBANK TRB WITH (NOLOCK)
                                    GROUP BY requestid
                                    ) B ON A.RequestID = B.RequestID
                                LEFT JOIN
                                    (
                                    SELECT 
                                        RequestID,'['+
                                        STUFF((
                                        SELECT ', ' +'{{""DocsURL"" :""'+ DocsURL + '""}}'
                                        FROM tbl_RequestDocs WITH (NOLOCK)
                                        WHERE  IsDeleted=0 AND (RequestID = trd.RequestID) 
                                        FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
                                        ,1,2,'')+']' AS DocList
                                    FROM tbl_RequestDocs trd WITH (NOLOCK)
                                    GROUP BY trd.RequestID
                                    ) E ON A.RequestID = E.RequestID
                                LEFT JOIN tbl_MemberInfo C WITH (NOLOCK) ON A.MemberID = C.MemberId
                                LEFT JOIN tbl_projectsettings PS WITH(NOLOCK) ON A.ProjectId = PS.ID
								left join tbl_Branch sb with (nolock) on a.BranchID = sb.BranchID
								left join tbl_State ts with (nolock) on ts.StateID = sb.StateID
                                {2}
                                WHERE 1=1 AND A.isdeleted=0 {3}
            "
            , sql2.ToString()
            , sql3.ToString()
            , string.IsNullOrEmpty(txtApplicantName.Text) ? "" :
                "LEFT JOIN tbl_Applicants ta WITH (NOLOCK) ON ta.RequestID = A.RequestID "
            , sqlString.searchTextBox("ta.Name", txtApplicantName, true, true, false)
            , getCount ? "count(A.RequestID) as 'Count'" : selectData.ToString());

            sql.Append(searchFilter);
            if (!string.IsNullOrWhiteSpace(txtFrom.Text) && !string.IsNullOrWhiteSpace(txtTo.Text) && DateTime.TryParse(txtFrom.Text, out var from) && DateTime.TryParse(txtTo.Text, out var to))
                sql.Append(" and A.RequestAt between '" + txtFrom.Text + "' and '" + txtTo.Text + " 23:59' ");
            sql.Append(sqlString.searchTextBox("C.Fullname", txtMemberName, true, true, false));
            sql.Append(sqlString.searchTextBox("A.RequestID", txtRequestID, false, true, false));
            sql.Append(sqlString.searchDropDownList("A.ProjectId", ddlProject, true, true, false));
            sql.Append(sqlString.searchDropDownList("A.ApplicationResult", ddlApplicationStatus, true, true, false));
            sql.Append(sqlString.searchDropDownList("ts.StateID", ddlState, true, true, false));

            if (!getCount)
            {
                sql.Append("and a.IsFinal is not null order by ");

                //ordering
                if (sortBy != null && sortBy.Any())
                {
                    int i = 0;
                    foreach (var item in sortBy)
                    {
                        string key = item.Key;
                        string value = item.Value;

                        if (i > 0)
                            sql.Append(",");

                        switch (key)
                        {
                            case "RequestId":
                                sql.Append("cast(A.RequestID as int) " + value);
                                break;
                            case "RequestAt":
                                sql.Append("A.RequestAt " + value);
                                break;
                            case "Fullname":
                                sql.Append("C.Fullname " + value);
                                break;
                            case "ApplicantName":
                                sql.Append("ApplicantName " + value);
                                break;
                            case "ProjectName":
                                sql.Append("ProjectName " + value);
                                break;
                            case "ApplicationResult":
                                sql.Append("A.ApplicationResult " + value);
                                break;
                            default:
                                break;
                        }
                        i++;
                    }
                }
                else
                    sql.Append("a.IsFinal asc, cast(A.RequestID as int) DESC ");
            }

            return sql.ToString();
        }
        private string getNumberOfRows()
        {
            wwdb db = new wwdb();
            string noOfRows = "100";

            db.OpenTable(@"
                SELECT ParameterValue as 'Rows'
                FROM tbl_Parameter
                WHERE Category = 'ClientList' AND ParameterName = 'NumberOfDataRows'
            ");
            if (db.RecordCount() > 0)
                noOfRows = db.Item("Rows");

            return noOfRows;
        }
        #endregion

        #region ClientList - GV
        protected void gv_Sorting(object sender, GridViewSortEventArgs e)
        {
            string expression = e.SortExpression;
            string direction = null;

            //3 state sorting
            //ASC,DESC,null
            if (GetGridViewSortDirection(expression) == "ASC")
            {
                SetGridViewSortDirection(expression, "DESC");
                direction = "DESC";
            }
            else if (GetGridViewSortDirection(expression) == "DESC")
            {
                SetGridViewSortDirection(expression, null);
                direction = null;
            }
            else if (GetGridViewSortDirection(expression) == null)
            {
                SetGridViewSortDirection(expression, "ASC");
                direction = "ASC";
            }

            Dictionary<string, string> sortBy = new Dictionary<string, string>();
            if (direction != null)
                sortBy.Add(e.SortExpression, direction);

            ViewState["SortBy"] = sortBy;

            bool selectAllb = false;
            if (ViewState["SelectAll"] != null)
            {
                string selectAll = ViewState["SelectAll"].ToString();
                if (!string.IsNullOrEmpty(selectAll))
                    if (selectAll.Equals("1"))
                        selectAllb = true;
            }
            bindClientList(sortBy, selectAllb);

            if (gv.Rows.Count > 0)
                gridView.ApplyPaging(gv, -1);
        }

        private string GetGridViewSortDirection(string sortExpression)
        {
            if (ViewState["sortDirection" + sortExpression] == null)
                return null;
            else
                return (string)ViewState["sortDirection" + sortExpression];
        }

        private void SetGridViewSortDirection(string sortExpression, string value)
        {
            ViewState["sortDirection" + sortExpression] = value;
        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                LinkButton sortLink;

                Dictionary<string, string> sortBy = new Dictionary<string, string>();
                if (ViewState["SortBy"] != null)
                { sortBy = (Dictionary<string, string>)ViewState["SortBy"]; }

                // GridView rows with sortable columns will have a linkbutton
                // generated. Compare to the CommandArgument since this is what
                // we set our sortColumn based on.
                foreach (TableCell currCell in e.Row.Cells)
                {
                    if (currCell.HasControls())
                    {
                        sortLink = ((LinkButton)currCell.Controls[0]);
                        if (sortBy.ContainsKey(sortLink.CommandArgument))
                        {
                            // Use the HTML safe codes for the up arrow ▲ and down arrow ▼.
                            string sortArrow = sortBy[sortLink.CommandArgument] == "ASC" ? "&#9650;" : "&#9660;";

                            sortLink.Text = sortLink.Text + " " + sortArrow;
                        }
                        else
                        {
                            sortLink.Text = sortLink.Text;
                        }
                    }
                }
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button buttonEdit = e.Row.FindControl("btnEdit") as Button;
                ImageButton buttonEditStatus = e.Row.FindControl("btnEditStatus") as ImageButton;
                Button buttonDelete = e.Row.FindControl("btnDelete") as Button; // re-open delete button

                wwdb db = new wwdb();
                string requestID = DataBinder.Eval(e.Row.DataItem, "RequestID").ToString();
                
                if (login_user.isStaffAdmin())
                { 
                    buttonEdit.Visible = true;
                    buttonDelete.Visible = true; // re-open delete buton
                }
                //btnEditStatus
                //if (login_user.isStaffAdmin())
                //    buttonEditStatus.Visible = true;
                //else if (login_user.isAccountManager() || login_user.isBranchManager())
                //    buttonEditStatus.Visible = true;
                //else
                //{
                //db.OpenTable("SELECT RequestBy FROM tbl_Request WHERE RequestID=N'" + requestID + "'");
                //if (db.RecordCount() > 0)
                //    if (db.Item("RequestBy").Equals(login_user.UserId))
                //        buttonEditStatus.Visible = true;

                string sql = string.Format(@" 
                    select m.[Key] from tbl_MemberRole mr with (nolock)
                    inner join tbl_RoleMenu rm with (nolock) on rm.RoleID = mr.RoleCode
                    inner join tbl_Menu m with (nolock) on m.rowID = rm.MenuID
                    where (m.[Key] = 'function_edit_request' or m.[Key] = 'function_sequence_of_events') 
				    and mr.MemberId = '{0}'
                ", login_user.UserId);

                    db.OpenTable(sql);
                    if (db.RecordCount() > 0)
                        buttonEditStatus.Visible = true;
                //}

                string applicantName = DataBinder.Eval(e.Row.DataItem, "ApplicantName").ToString();
                string consentFormId = DataBinder.Eval(e.Row.DataItem, "ConsentFormId").ToString();
                string BANKLIST = DataBinder.Eval(e.Row.DataItem, "BANKLIST").ToString();
                string DocList = DataBinder.Eval(e.Row.DataItem, "DocList").ToString();
                string isFinalS = string.IsNullOrEmpty(DataBinder.Eval(e.Row.DataItem, "isFinal").ToString()) ?
                    "-1" : DataBinder.Eval(e.Row.DataItem, "isFinal").ToString();
                int isFinal = -1;
                if (isFinalS.Equals("True"))
                    isFinal = 1;
                else if (isFinalS.Equals("False"))
                    isFinal = 0;
                string ApplicationResult = DataBinder.Eval(e.Row.DataItem, "ApplicationResult").ToString();
                int resultIndex = gridView.GetColumnIndexByName(gv, Resources.resource.Customer_Profile);
                int nameIndex = gridView.GetColumnIndexByName(gv, Resources.resource.Applicant_Name);
                int actionIndex = gridView.GetColumnIndexByName(gv, Resources.resource.Action);
                Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                Label lblApplicantName = (Label)e.Row.Cells[2].FindControl("lblApplicantName");
                TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                lblApplicantName.Text = textInfo.ToTitleCase(applicantName.ToLower());

                Dictionary<string, Color> colorlist = new Dictionary<string, Color>();
                colorlist.Add("A", Color.Green);
                colorlist.Add("D", Color.Red);
                colorlist.Add("M", Color.Yellow);
                colorlist.Add("S", Color.LightGreen); //temp for SpeedMode

                DropDownList ddlBank = (DropDownList)e.Row.FindControl("ddlBank");
                Repeater rptFiles = (Repeater)e.Row.FindControl("rptFiles");
                LinkButton btnClientViewConsent = (LinkButton)e.Row.FindControl("btnClientViewConsent");

                db.OpenTable(string.Format(@"
                    select cf.* from tbl_Request r with (nolock) inner join 
                    tbl_ConsentForm cf with(nolock) on r.ConsentFormId = cf.Id
                    where cf.Id = '{0}'
                ",consentFormId));
                btnClientViewConsent.Visible = (db.RecordCount() > 0);

                //  lblStatus.Text = sqlString.changeLanguage("ApplicationResult_" + ApplicationResult);
                lblStatus.Text = colorlist[ApplicationResult].Name;
                e.Row.Cells[resultIndex].BackColor = colorlist[ApplicationResult];
                e.Row.Cells[nameIndex].BackColor = colorlist[ApplicationResult];

                if (isFinal == 0)
                    e.Row.Cells[actionIndex].BackColor = colorlist["S"];
                //if (isFinal > 0)
                //    e.Row.Cells[actionIndex].BackColor = colorlist["A"];
                //else if (isFinal == 0) 
                //    e.Row.Cells[actionIndex].BackColor = colorlist["D"];
                //else //if(isFinal < 0)
                //    e.Row.Cells[actionIndex].BackColor = colorlist["M"];


                if (BANKLIST != string.Empty)
                {
                    var array = JArray.Parse(BANKLIST);

                    sqlString.bindControl(ddlBank, array, "Name", "ID", true);
                }

                if (DocList != string.Empty)
                {
                    var array = JArray.Parse(DocList);
                    if (array != null)
                    {
                        rptFiles.DataSource = array;
                        rptFiles.DataBind();
                    }
                }

                Button btnMACCRIS = (Button)e.Row.FindControl("btnMACCRIS");
                Button btnCACCRIS = (Button)e.Row.FindControl("btnCACCRIS");
                btnMACCRIS.Visible = false;
                btnCACCRIS.Visible = false;
                int showMACCRIS = int.Parse(DataBinder.Eval(e.Row.DataItem, "ShowMACCRIS").ToString());
                int showCACCRIS = int.Parse(DataBinder.Eval(e.Row.DataItem, "ShowCACCRIS").ToString());
                if (showMACCRIS > 0)
                    btnMACCRIS.Visible = true;
                if (showCACCRIS > 0)
                    btnCACCRIS.Visible = true;

                //PayMent Button
                string PaymentStatus = DataBinder.Eval(e.Row.DataItem, "PaymentStatus").ToString();
                //Label lblPaymentStatus = (Label)e.Row.FindControl("lblPaymentStatus");
                //Button btnSuccess = (Button)e.Row.FindControl("btnSuccess");
                //Button btnFail = (Button)e.Row.FindControl("btnFail");
                if (PaymentStatus == "1")
                {
                    //lblPaymentStatus.Text = Resources.resource.Success;
                    //lblPaymentStatus.Visible = true;
                    //btnSuccess.Visible = false;
                    //btnFail.Visible = false;
                }
                else if (PaymentStatus == "0")
                {
                    //lblPaymentStatus.Text = Resources.resource.Fail;
                    //lblPaymentStatus.Visible = true;
                    //btnSuccess.Visible = false;
                    //btnFail.Visible = false;
                }

                string url = DataBinder.Eval(e.Row.DataItem, "URL").ToString();
                Label lblMembername = (Label)e.Row.FindControl("lblMembername");
                LinkButton btnMemberName = (LinkButton)e.Row.FindControl("btnMemberName_Whatsapp");
                if (string.IsNullOrEmpty(url))
                {
                    lblMembername.Visible = true;
                    btnMemberName.Visible = false;
                }
                else
                {
                    lblMembername.Visible = false;
                    btnMemberName.Visible = true;
                }
            }
            //e.Row.Cells[5].Visible = false;
        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Dictionary<string, string> sortBy = new Dictionary<string, string>();
            if (ViewState["SortBy"] != null)
            { sortBy = (Dictionary<string, string>)ViewState["SortBy"]; }

            gv.PageIndex = e.NewPageIndex;

            bool selectAllb = false;
            if (ViewState["SelectAll"] != null)
            {
                string selectAll = ViewState["SelectAll"].ToString();
                if (!string.IsNullOrEmpty(selectAll))
                    if (selectAll.Equals("1"))
                        selectAllb = true;
            }
            bindClientList(sortBy, selectAllb);
            
            if (gv.Rows.Count > 0)
                gridView.ApplyPaging(gv, e.NewPageIndex);
        }

        protected void gv_DataBound(object sender, EventArgs e)
        {
            //litTotal.Text = gv.Rows.Count.ToString("N0"); //doesn't work while paging

            //Literal litTotal = (Literal)gv.BottomPagerRow.FindControl("litTotal");
            //if (litTotal != null)
            //    if (string.IsNullOrEmpty(litTotal.Text))
            //    {
            //        wwdb db = new wwdb();
            //        litTotal.Text = Resources.resource.Total + " : " + db.getDataTable(getClientList(null)).Rows.Count.ToString("N0");
            //    }
        }
        #endregion

        #region REQUESTLIST
        //requestList
        private string getRequestList()
        {
            StringBuilder sql = new StringBuilder();

            string searchFilter = string.Empty;
            if (login_user.isStaffAdmin() || login_user.UserType.Contains(KeyVal.CreditAdvisor))
                searchFilter = string.Empty;
            else if (login_user.UserAccessList.Where(_ => _.Key.Equals("function_view_branch_request")).Count() >= 1)
                searchFilter = " AND (a.memberid = '" + login_user.UserId + "' OR A.BranchID IN(SELECT BranchId FROM tbl_Branch Where ParentId = " + login_user.BranchUniqueID + " OR Id = " + login_user.BranchUniqueID + "))";
            else
                searchFilter = " AND a.memberid='" + login_user.UserId + "'";


            sql.AppendFormat(@";WITH cte AS
								(
								SELECT *,
								ROW_NUMBER() OVER (PARTITION BY dbo.tbl_RequestApplicant.RequestID ORDER BY dbo.tbl_RequestApplicant.CreateAt ASC) AS rn
								FROM tbl_RequestApplicant
								)			
								SELECT A.RequestID, a.RequestAt ,E.Fullname ,D.Name   AS 'ApplicantName' ,a.StatusID , ~a.isdeleted AS 'canedit' , b.DocumentList , ISNULL(c.BranchName ,'--') AS BranchName, a.MemberId, a.BranchId, a.ConsentFormId
								FROM (
                                SELECT * FROM tbl_Request WITH (NOLOCK) 
                                )a
                                LEFT JOIN 
                                (	
                                SELECT 
                                  requestid,'['+
                                  STUFF((
                                    SELECT ', ' +'{{""DocumentType"" :""'+ DocumentType + '"", ""URL"" :""' + DocsURL+'""}}'
                                    FROM tbl_RequestDocs WITH (NOLOCK)
                                    WHERE  isdeleted=0 AND (requestid = trd.requestid)  AND DocumentType = 'N'
                                    FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
                                  ,1,2,'')+']' AS DocumentList
                                FROM tbl_RequestDocs trd WITH (NOLOCK)
                                GROUP BY requestid
                                ) b ON a.RequestID = b.requestid
                                LEFT JOIN tbl_branch c WITH (NOLOCK) ON a.BranchID = c.BranchID
                                LEFT JOIN 
								(
								 SELECT *
								 FROM cte
								 WHERE rn = 1
								) d
								ON a.RequestID = d.requestid
                                LEFT JOIN tbl_MemberInfo E WITH (NOLOCK) ON A.MemberID = E.MemberId
								LEFT JOIN tbl_projectsettings PS WITH(NOLOCK) ON A.ProjectId = PS.ID
                                WHERE 1=1 AND a.StatusID != 'S' AND a.StatusID != 'D' AND a.isDeleted = 0 ");

            sql.Append(searchFilter);

            sql.Append(sqlString.searchTextBox("D.Name", txtName, true, true, false));

            sql.Append(sqlString.searchTextBox("E.Fullname", txtMemberNameR, true, true, false));

            sql.Append(sqlString.searchDropDownList("a.BranchID", ddlBranch, true, true, false));

            sql.Append(sqlString.searchTextBox("A.RequestID", txtRequestIDR, false, true, false));

            sql.Append(sqlString.searchDropDownList("a.StatusID", ddlStatus, true, true, false));
            sql.Append("ORDER BY a.RequestAt DESC");

            return sql.ToString();

        }
        protected void gvR_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvR.PageIndex = e.NewPageIndex;
            sqlString.bindControl(gvR, getRequestList());

            if (gvR.Rows.Count > 0)
                gridView.ApplyPaging(gvR, e.NewPageIndex);
        }
        protected void gvR_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Repeater rptFiles = (Repeater)e.Row.FindControl("rptFiles");
                string filesList = DataBinder.Eval(e.Row.DataItem, "DocumentList").ToString();
                string canedit = DataBinder.Eval(e.Row.DataItem, "canedit").ToString();
                string StatusID = DataBinder.Eval(e.Row.DataItem, "StatusID").ToString();
                string requestID = DataBinder.Eval(e.Row.DataItem, "requestid").ToString();
                string consentFormId = DataBinder.Eval(e.Row.DataItem, "ConsentFormId").ToString();
                Button btnDelete = (Button)e.Row.FindControl("btnDelete");
                Button btnSendSMS = (Button)e.Row.FindControl("btnSendSMS");
                Button btnCalculate = (Button)e.Row.FindControl("btnCalculate");
                Button btnRequestViewConsent = (Button)e.Row.FindControl("btnRequestViewConsent");
                int statusIndex = gridView.GetColumnIndexByName(gvR, Resources.resource.Status);


                btnRequestViewConsent.Visible = !string.IsNullOrWhiteSpace(consentFormId);

                if (filesList != string.Empty)
                {
                    var array = JArray.Parse(filesList);
                    array.ToList().ForEach(x => x["DocumentType"] = sqlString.changeLanguage("DocumentType_" + x["DocumentType"]));
                    if (array != null)
                    {
                        rptFiles.DataSource = array;
                        rptFiles.DataBind();
                    }
                }

                if (canedit.ToUpper() == "TRUE")
                {
                    if (login_user.isStaffAdmin() && StatusID != ApplicationStatus.Sent)
                    {
                        btnCalculate.Visible = true;
                        btnSendSMS.Visible = true;
                    }
                    if (login_user.UserType.Contains(KeyVal.CreditAdvisor) && StatusID == ApplicationStatus.InProgress)
                    {
                        btnCalculate.Visible = true;
                        btnSendSMS.Visible = true;
                    }

                    //if (StatusID == ApplicationStatus.New)
                    //    btnCalculate.Attributes.Add("onclick", "window.location.href = '" + "/Form/Loan/Calculator.aspx?rid=" + sqlString.encryptURL(requestID) + "'");
                    //else if (StatusID == ApplicationStatus.Processing)
                    //    btnCalculate.Attributes.Add("OnClientClick", "return confirmCalculate(" + sqlString.encryptURL(requestID) + ");");
                    //btnCalculate.Attributes.Add("onclick", "window.location.href ='" + "/Form/Loan/CalculateReport.aspx?rid=" + sqlString.encryptURL(requestID) + "'");

                    if (login_user.isStaffAdmin() && StatusID != ApplicationStatus.Sent)
                    {
                        btnDelete.Visible = true;
                    }
                }
                e.Row.Cells[statusIndex].Text = sqlString.changeLanguage("ApplicationStatus_" + e.Row.Cells[statusIndex].Text);
            }
        }
        protected void gvR_DataBound(object sender, EventArgs e)
        {
            if (gvR.Rows.Count > 0)
            {
                if (!login_user.isStaffAdmin() && !login_user.UserType.Contains(KeyVal.CreditAdvisor))
                {
                    int actionIndex = gridView.GetColumnIndexByName(gvR, Resources.resource.Action);
                    gridView.hideColumn(gvR, new int[] { actionIndex }, true, true);
                }

            }
        }
        protected void btnSearchR_Click(object sender, EventArgs e)
        {
            sqlString.bindControl(gvR, getRequestList());

            if (gvR.Rows.Count > 0)
                gridView.ApplyPaging(gvR, -1);

            //Page.RegisterStartupScript(this.GetType(), "CallMyFunction", "NewFunction()", true);
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "MyFun1", "NewFunctionR();", true);
        }
        protected void btnSendSMS_Click(object sender, EventArgs e)
        {
            wwdb db = new wwdb();
            Button btnSender = (Button)sender;

            //send sms
            db.OpenTable(string.Format(@"
                SELECT (SELECT TOP 1 Mobile FROM
                tbl_MemberInfo WITH(NOLOCK) WHERE MemberID = tr.MemberID) AS mobile FROM
                tbl_request tr WITH(NOLOCK) WHERE requestid = '{0}'
            ", btnSender.CommandArgument.ToString()));
            //if mobile phone found
            if (db.RecordCount() > 0)
            {
                string mobileNo = db.Item("Mobile");
                bool validFormat = false;
                if (mobileNo.StartsWith("0") && !(mobileNo.StartsWith("6")))
                {
                    mobileNo = "6";
                    mobileNo += db.Item("Mobile");
                    validFormat = true;
                }
                else if (mobileNo.StartsWith("601"))
                    validFormat = true;

                if (validFormat)//only send sms when mobileNo is in valid format to save cost
                {
                    MessagingService SMS = new MessagingService();
                    SMS.sendInvalidFileNotification(mobileNo);
                }
            }

            sqlString.displayAlert2(this, Resources.resource.Action_Successful, Request.Url.ToString());
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            wwdb db = new wwdb();
            Button btnSender = (Button)sender;
            db.Execute("UPDATE tbl_Request SET isDeleted = 1, StatusID = 'D' WHERE RequestID='" + btnSender.CommandArgument.ToString() + "'");
            sqlString.displayAlert2(this, Resources.resource.Action_Successful, Request.Url.ToString());

        }
        protected void btnCalculate_Click(object sender, EventArgs e)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            Button btnsender = (Button)sender;
            string rid = btnsender.CommandArgument.ToString();

            string statusID = string.Empty;
            sql.Clear();
            sql.AppendFormat(@"
                select distinct r.StatusID, r.ReportType from tbl_Request r with (nolock) inner join
                tbl_Parameter p on r.StatusID = p.ParameterValue or r.ReportType = p.ParameterValue
                where r.RequestID='{0}' and (p.Category='ApplicationStatus' or p.Category='ReportType');", rid);
            db.OpenTable(sql.ToString());
            if (db.RecordCount() > 0)
                statusID = db.Item("StatusID");

            if (!statusID.Equals(ApplicationStatus.Sent)) //not finalised
            {
                if (string.IsNullOrEmpty(hfConfirmCalculate.Value))
                {
                    if (statusID.Equals(ApplicationStatus.Processing))
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "confirmCalculate",
                            "confirmCalculate(" + btnsender.ClientID.Substring(btnsender.ClientID.Length - 1) + ");", true);
                    else //if(statusID.Equals(ApplicationStatus.New))
                    {
                        sql.Clear();
                        sql.Append("UPDATE tbl_Request SET StatusID='" + ApplicationStatus.Processing + "' WHERE RequestID=N'" + rid + "';");
                        db.Execute(sql.ToString());
                        LogUtil.logAction(sql.ToString(), "UPDATE Request Processing. RequestID:" + rid);
                        Response.Redirect("/Form/Loan/Calculator.aspx?rid=" + sqlString.encryptURL(rid));
                    }
                }
                else if (hfConfirmCalculate.Value.Equals("true"))
                    Response.Redirect("/Form/Loan/Calculator.aspx?rid=" + sqlString.encryptURL(rid));
            }
            else
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "alreadyFinalized",
                    "alert('" + Resources.resource.Already_Finalised + "');", true);
        }
        #endregion

        #region ClientList - Controls
        protected void btnView_Click(object sender, EventArgs e)
        {
            Button btnsender = (Button)sender;

            GridViewRow gvr = (GridViewRow)btnsender.NamingContainer;
            DropDownList ddlBank = (DropDownList)gvr.FindControl("ddlBank");

            string rid = btnsender.CommandArgument.ToString();
            if (ddlBank.SelectedValue == "0")
            {
                //ddlBank is hidden due to only Classic Bank is used for temp
                //will only enter this conditional if
                try
                {
                    ddlBank.ClearSelection();
                    ddlBank.SelectedIndex = 1;//auto select first item 1 because 0 is none
                    string reportIrl = "/Form/Loan/DsrReport.aspx?rid=" + sqlString.encryptURL(rid) + "&BankID=" + sqlString.encryptURL(ddlBank.SelectedValue);
                    sqlString.OpenNewWindow_Center(reportIrl, "Report", 800, 600, this);

                }
                catch (Exception ex)
                {
                    LogUtil.logError(ex.Message, "Error viewing DsrReport. RequestID:" + rid);
                    sqlString.displayAlert2(this, Resources.resource.Err_Bank_Name_Select_Empty);
                }
            }
            else
            {
                string reportIrl = "/Form/Loan/DsrReport.aspx?rid=" + sqlString.encryptURL(rid) + "&BankID=" + sqlString.encryptURL(ddlBank.SelectedValue);
                sqlString.OpenNewWindow_Center(reportIrl, "Report", 800, 600, this);
            }
        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            Button btnsender = (Button)sender;

            string rid = btnsender.CommandArgument.ToString();

            Response.Redirect("/Form/Loan/EditRequest.aspx?rid=" + sqlString.encryptURL(rid));
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> sortBy = new Dictionary<string, string>();
            if (ViewState["SortBy"] != null)
            { sortBy = (Dictionary<string, string>)ViewState["SortBy"]; }

            bool selectAllb = false;
            if (ViewState["SelectAll"] != null)
            {
                string selectAll = ViewState["SelectAll"].ToString();
                if (!string.IsNullOrEmpty(selectAll))
                    if (selectAll.Equals("1"))
                        selectAllb = true;
            }
            bindClientList(sortBy, selectAllb);

            if (gv.Rows.Count > 0)
                gridView.ApplyPaging(gv, -1);
        }
        protected void btnPrintPreview_Click(object sender, EventArgs e)
        {
            string reportIrl = 
                string.Format("/Form/Report/ClientListPrintPreview.aspx?from={0}&to={1}&membername={2}&applicantname={3}&requestid={4}&projectid={5}&applicationstatus={6}&projectname={7}&parentbranch={8}"
                , sqlString.encryptURL(txtFrom.Text)
                , sqlString.encryptURL(txtTo.Text)
                , sqlString.encryptURL(txtMemberName.Text)
                , sqlString.encryptURL(txtApplicantName.Text)
                , sqlString.encryptURL(txtRequestID.Text)
                , sqlString.encryptURL(ddlProject.SelectedValue)
                , sqlString.encryptURL(ddlApplicationStatus.SelectedValue)
                , sqlString.encryptURL(ddlProject.Items[ddlProject.SelectedIndex].Text)
                , sqlString.encryptURL(ddlParentBranch.SelectedValue));

            sqlString.OpenNewWindow_Center(reportIrl, "PrintPreview", 800, 600, this);
        }
        protected void btnMACCRIS_Click(object sender, EventArgs e)
        {
            Button btnsender = (Button)sender;

            string rid = btnsender.CommandArgument.ToString();

            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            sql.Clear();
            sql.AppendFormat(@" 
                SELECT ID, IsCoApplicant, Type
                FROM tbl_CTOS_Result WITH(NOLOCK)
                WHERE IsActive = 1 AND IsCoApplicant = 0 AND Request_Id = N'{0}' 
                Order By ID
            ", secure.RC(rid));
            db.OpenTable(sql.ToString());
            if (db.RecordCount() > 0)
            {
                string type = db.Item("Type").ToString();

                if (type.Equals("CTOS"))
                    sqlString.OpenNewWindow_Center("/Form/Loan/CTOSReport.aspx?rid=" + sqlString.encryptURL(rid) + "&type=" + sqlString.encryptURL("0"), "DSR Report", 600, 800, this);
                if (type.Equals("RAMCI"))
                    sqlString.OpenNewWindow_Center("/Form/Loan/RAMCIReport.aspx?rid=" + sqlString.encryptURL(rid) + "&type=" + sqlString.encryptURL("0"), "DSR Report", 800, 800, this);
            }
        }
        protected void btnCACCRIS_Click(object sender, EventArgs e)
        {
            Button btnsender = (Button)sender;

            string rid = btnsender.CommandArgument.ToString();

            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            sql.Clear();
            sql.AppendFormat(@" 
                SELECT ID, IsCoApplicant, Type
                FROM tbl_CTOS_Result WITH(NOLOCK)
                WHERE IsActive = 1 AND IsCoApplicant = 1 AND Request_Id = N'{0}' 
                Order By ID
            ", secure.RC(rid));
            db.OpenTable(sql.ToString());
            if (db.RecordCount() > 0)
            {
                string type = db.Item("Type").ToString();

                if (type.Equals("CTOS"))
                    sqlString.OpenNewWindow_Center("/Form/Loan/CTOSReport.aspx?rid=" + sqlString.encryptURL(rid) + "&type=" + sqlString.encryptURL("1"), "DSR Report", 600, 800, this);
                if (type.Equals("RAMCI"))
                    sqlString.OpenNewWindow_Center("/Form/Loan/RAMCIReport.aspx?rid=" + sqlString.encryptURL(rid) + "&type=" + sqlString.encryptURL("1"), "DSR Report", 800, 800, this);
            }
        }
        protected void btnDetailedReport_Click(object sender, EventArgs e)
        {
            string reportIrl = "/Form/Report/ClientListDetailedReport.aspx?from=" + sqlString.encryptURL(txtFrom.Text) + "&to=" + sqlString.encryptURL(txtTo.Text)
                + "&applicantname=" + sqlString.encryptURL(txtApplicantName.Text)
                + "&requestid=" + sqlString.encryptURL(txtRequestID.Text) + "&projectid=" + sqlString.encryptURL(ddlProject.SelectedValue)
                + "&applicationstatus=" + sqlString.encryptURL(ddlApplicationStatus.SelectedValue) + "&projectname=" + sqlString.encryptURL(ddlProject.Items[ddlProject.SelectedIndex].Text);
            sqlString.OpenNewWindow_Center(reportIrl, "Detailed Report", 800, 600, this);
        }
        protected void btnViewConsent_Click(object sender, EventArgs e)
        {
            string consentFormId = string.Empty;

            try
            {
                if (sender is LinkButton)
                {
                    LinkButton btnSender = (LinkButton)sender;
                    consentFormId = btnSender.CommandArgument.ToString();
                }
                else if (sender is Button)
                {
                    Button btnSender = (Button)sender;
                    consentFormId = btnSender.CommandArgument.ToString();
                }

                sqlString.OpenNewWindow_Center("/Form/Loan/ConsentForm.aspx?id=" + sqlString.encryptURL(consentFormId) +
                "&maid=" + sqlString.encryptURL("") + "&man=" + sqlString.encryptURL("") +
                "&caid=" + sqlString.encryptURL("") + "&can=" + sqlString.encryptURL("")
                , "Consent Form", 800, 800, this);
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.Message, "Viewing Consent Documents");
            }
        }

        protected void btnEditStatus_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnsender = (ImageButton)sender;

            string rid = btnsender.CommandArgument.ToString();

            Response.Redirect("/Form/Loan/NewNote.aspx?rid=" + sqlString.encryptURL(rid));
        }

        protected void ddlClientListBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            StringBuilder sql = new StringBuilder();

            sql.AppendFormat(@"
                select ps.ProjectName as '0', ps.ID as '1', b.ID, b.BranchName 
                from tbl_ProjectSettings ps with (nolock) inner join
                tbl_Branch b with (nolock) on ps.BranchId = b.id
                WHERE ps.status = 'A' AND ps.IsDeleted = 0 and b.IsDeleted = 0 
            ");
            //sql.Append("and(b.ID = '1013' or b.ParentId = '1013') ");
            if (ddlClientListBranch.SelectedIndex > 0)
            {
                sql.AppendFormat(@"
                    and b.ParentId is not null and b.ID = {0}
                ", ddlClientListBranch.SelectedValue);
            }
            sql.Append(" ORDER BY [0]");
            sqlString.bindControl(ddlProject, sql.ToString(), "0", "1", true);
        }

        protected void btnMemberName_Whatsapp_Click(object sender, EventArgs e)
        {
            LinkButton btnSender = (LinkButton)sender;
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            string requestid = btnSender.CommandArgument, phoneNumber = string.Empty;

            //obtaining phoneNumber
            sql.Clear();
            sql.AppendFormat(@"
                SELECT case when mi.Mobile like '%*%'
			                then null
		                when mi.Mobile like '6%'
			                then mi.Mobile
		                else
			                '6' + mi.Mobile
		                end as 'Mobile'
                from tbl_MemberInfo mi with (nolock) inner join
                tbl_Request r with (nolock) on mi.memberID = r.RequestBy
                WHERE r.RequestID = N'{0}'
            ", requestid);
            db.OpenTable(sql.ToString());
            if (db.RecordCount() > 0)
                if (!string.IsNullOrEmpty(db.Item("Mobile")))
                    phoneNumber = db.Item("Mobile");

            if (!string.IsNullOrEmpty(phoneNumber))
            {
                string url = "https://api.whatsapp.com/send?phone=";
                string message, requestAt = string.Empty, mainName = string.Empty, coName = string.Empty;

                //constructing message
                db.OpenTable("SELECT r.RequestAt FROM tbl_Request r with (nolock) WHERE requestID = N'" + requestid + "'");
                if (db.RecordCount() > 0)
                    requestAt = "Regarding the request submitted at " + db.Item("RequestAt") + ". ";
                db.OpenTable("SELECT a.Name, a.ApplicantType FROM tbl_Applicants a with (nolock) WHERE requestID = N'" + requestid + "'");
                if (db.RecordCount() > 0)
                    while (!db.Eof())
                    {
                        if (db.RecordCount() == 1)
                        {
                            if (db.Item("ApplicantType").Equals("MA"))
                                mainName = db.Item("Name");
                            else //if (db.Item("ApplicantType").Equals("CA"))
                                coName = db.Item("Name"); //shouldn't be reached
                        }
                        else //if (db.RecordCount() > 1)
                        {
                            if (db.Item("ApplicantType").Equals("MA"))
                                mainName = db.Item("Name");
                            else //if (db.Item("ApplicantType").Equals("CA"))
                                coName = ". Applicant Name 2: " + db.Item("Name") + ". ";
                        }
                        db.MoveNext();
                    }
                message = string.Format(@"Hi, this is staff from WhoPay Sdn Bhd. {0}Applicant Name 1: {1}{2}"
                            , requestAt, mainName, coName);
                //encrypting message
                message = message.Replace("%", "%25").Replace("&", "%26");

                //appending url with phoneNumber and text message
                url += phoneNumber + (string.IsNullOrEmpty(message) ? "" : "&text=" + message);
                //encrypting url
                url = url.Replace("'", "%27").Replace(" ", "%20")
                         .Replace("@", "%40").Replace("#", "%23").Replace("$", "%24")//.Replace("%", "%25")
                         .Replace("^", "%5E")//.Replace("&", "%26")
                         .Replace(";", "%3B").Replace(",", "%2C")//.Replace(":", "%3A").Replace("/", "%2F").Replace("?", "%3F")
                         ;
                //open new tab
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWhatsappTab_Admin", "window.open('" + url + "');", true);
            }
        }
        protected void btnShowAllData_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> sortBy = new Dictionary<string, string>();
            if (ViewState["SortBy"] != null)
            { sortBy = (Dictionary<string, string>)ViewState["SortBy"]; }

            txtFrom.Text = string.Empty;
            txtTo.Text = string.Empty;
            txtMemberName.Text = string.Empty;
            txtRequestID.Text = string.Empty;
            txtApplicantName.Text = string.Empty;
            ddlProject.SelectedIndex = 0;
            ddlApplicationStatus.SelectedIndex = 0;
            if (login_user.isStaffAdmin())
                ddlParentBranch.SelectedIndex = 0;

            bindClientList(sortBy, true);
            if (gv.Rows.Count > 0)
                gridView.ApplyPaging(gv, -1);
            ViewState["SelectAll"] = "1";
        }
        #endregion

        protected void CheckACL()
        {
            wwdb db = new wwdb();
            string sql = "";

            if (login_user.isStaffAdmin())
            {
                gvR.Columns[3].Visible = true;
                gvR.Columns[4].Visible = true;
                gv.Columns[3].Visible = true; //member name
                gv.Columns[5].Visible = true; //branch
                gv.Columns[6].Visible = true; //price
                divFilterParent.Visible = true;
                gv.Columns[10].Visible = true; //action
                gv.Columns[11].Visible = true; //ccris
                gv.Columns[12].Visible = true; //file
            }
            if (login_user.isAccountManager())
            {
                gv.Columns[5].Visible = true; //branch
            }

            sql = string.Format(@" select rm.* from tbl_RoleMenu rm with (nolock)
                            inner join tbl_Menu m with (nolock) on rm.MenuID=m.rowID
                            where m.[Key] = 'function_view_CCRIS' 
                            and rm.RoleID IN ({0})", string.Join(",", login_user.UserRole));
            db.OpenTable(sql);
            if (db.RecordCount() > 0)
                gv.Columns[10].Visible = true; //ccris
        }
    }
}