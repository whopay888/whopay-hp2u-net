﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using Synergy.Helper;
using Synergy.Model;
using Synergy.Util;

namespace HJT.Form.Report
{
    public partial class UsageReportPrintPreview : System.Web.UI.Page
    {
        LoginUserModel login_user = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];
            if (!IsPostBack)
            {
                string from = secure.Decrypt(Validation.GetUrlParameter(this.Page, "from", ""), true);
                string to = secure.Decrypt(Validation.GetUrlParameter(this.Page, "to", ""), true);
                string parentBranch = secure.Decrypt(Validation.GetUrlParameter(this.Page, "parentbranch", ""), true);
                string subBranch = secure.Decrypt(Validation.GetUrlParameter(this.Page, "subbranch", ""), true);
                string projectName = secure.Decrypt(Validation.GetUrlParameter(this.Page, "projectname", ""), true);
                string parentBranchID = secure.Decrypt(Validation.GetUrlParameter(this.Page, "parentbranchid", ""), true);
                string subBranchID = secure.Decrypt(Validation.GetUrlParameter(this.Page, "subbranchid", ""), true);
                string projectID = secure.Decrypt(Validation.GetUrlParameter(this.Page, "projectid", ""), true);

                lblParentBranch.Text = parentBranchID.Equals("0") ? "All Companies" : parentBranch;
                lblFrom.Text = string.IsNullOrWhiteSpace(from) ? "-" : from;
                lblTo.Text = string.IsNullOrWhiteSpace(to) ? "-" : to;

                //if (parentBranchID.Equals("0"))
                //    gv.Columns[1].Visible = false;
                //if (subBranchID.Equals("0"))
                //    gv.Columns[2].Visible = false;
                //if (projectID.Equals("0"))
                //    gv.Columns[3].Visible = false;

                //if (!(parentBranchID.Equals("0")))
                //    sqlString.bindControl(gv, getParentBranchUsage(from, to, parentBranchID, null));
                //else if (!(parentBranchID.Equals("0")) && !(subBranchID.Equals("0")))
                //    sqlString.bindControl(gv, getSubBranchUsage(from, to, parentBranchID, subBranchID, null));
                //else if (!(parentBranchID.Equals("0")) && !(subBranchID.Equals("0")) && !(projectID.Equals("0")))
                //    sqlString.bindControl(gv, getProjectUsage(from, to, parentBranchID, subBranchID, projectID, null));
                //else
                //sqlString.bindControl(gv, getAll(from, to, parentBranchID));
                //sqlString.bindControl(gv, getAllIncludeNull(from, to, parentBranchID, null));

                //bindProjectTotalUsage(getProjectTotalUsage(from, to, parentBranchID, projectID),
                //    getOtherProjectUsage(from, to, parentBranchID, projectID), null);

                wwdb db = new wwdb();
                DataTable dt = db.getDataTable(getProjectUsage(from, to, parentBranchID, subBranchID, projectID, null));
                gv.DataSource = dt;
                gv.DataBind();
                //sqlString.bindControl(gv, getProjectUsage(from, to, parentBranchID, subBranchID, projectID, null));

                lblGrandTotal.Text = countGrandTotal(dt).ToString();
            }
        }

        private string getAllIncludeNull(string from, string to, string parentBranchID, IDictionary<string, string> sortBy)
        {
            wwdb db = new wwdb();
            string dateFilter = "";
            int grandTotal = 0;
            StringBuilder sql = new StringBuilder();

            DropDownList ddlParentBranch = new DropDownList();
            ddlParentBranch.Items.Add(new ListItem { Text = "Select", Value = "0", Selected = parentBranchID.Equals("0") ? true : false });
            ddlParentBranch.Items.Add(new ListItem { Text = parentBranchID, Value = parentBranchID, Selected = parentBranchID.Equals("0") ? false : true });

            if (!string.IsNullOrWhiteSpace(from) && !string.IsNullOrWhiteSpace(to) && DateTime.TryParse(from, out _) && DateTime.TryParse(to, out _))
                dateFilter = " and r.RequestAt between '" + from + "' and '" + to + " 23:59' ";

            sql.Clear();
            sql.AppendFormat(@" 
                SELECT s.BranchName,   
                count(distinct r.RequestID) as 'Total',  
                    (SELECT count(distinct r.RequestID) FROM tbl_Request r  
                    WHERE r.ProjectID is null and r.BranchID=s.BranchID and r.isDeleted = 0 and r.StatusID='S' ) 
                as 'Other',  
                    (SELECT count(distinct r.RequestID) FROM tbl_Request r  
                    WHERE r.BranchID = s.BranchID and r.isDeleted = 0 and r.StatusID='S' and r.ApplicationResult = 'A' ) 
                as 'Green',  
                    (SELECT count(distinct r.RequestID) FROM tbl_Request r  
                    WHERE r.BranchID = s.BranchID and r.isDeleted = 0 and r.StatusID='S' and r.ApplicationResult = 'M' ) 
                as 'Yellow',  
                    (SELECT count(distinct r.RequestID) FROM tbl_Request r  
                    WHERE r.BranchID = s.BranchID and r.isDeleted = 0 and r.StatusID='S' and r.ApplicationResult = 'D' ) 
                as 'Red'  
                FROM tbl_Request r, tbl_Branch s   
                where (r.isDeleted = 0 and s.IsDeleted=0)  
                and (r.BranchID = s.BranchID)
                {0}
                {1}
                group by s.BranchName, s.BranchID
            ", !(parentBranchID.Equals("0") || string.IsNullOrEmpty(parentBranchID) || string.IsNullOrWhiteSpace(parentBranchID)) ?
                string.Format(@"and (s.ID = N'{0}' {1})", ddlParentBranch.SelectedValue.ToString(), sqlString.searchDropDownList("s.ParentId", ddlParentBranch, true, false, true)) : ""
            , dateFilter);

            db.OpenTable(sql.ToString());

            if (db.RecordCount() > 0)
            {
                while (!db.Eof())
                {
                    grandTotal += ConvertHelper.ConvertToInt(db.Item("Total"), 0);
                    //grandTotalOther += ConvertHelper.ConvertToInt(db.Item("Other"), 0);
                    db.MoveNext();
                }
                sql.Append("order by ");
                if (sortBy != null && sortBy.Any())
                {
                    int i = 0;
                    foreach (var item in sortBy)
                    {
                        string key = item.Key;
                        string value = item.Value;

                        if (i > 0)
                            sql.Append(",");

                        switch (key)
                        {
                            case "ParentBranchName":
                                sql.Append("[ParentBranchName] " + value);
                                break;
                            case "ParentOther":
                                sql.Append("[Other] " + value);
                                break;
                            case "ParentTotal":
                                sql.Append("[Total] " + value);
                                break;
                            case "ParentGreen":
                                sql.Append("[Green] " + value);
                                break;
                            case "ParentYellow":
                                sql.Append("[Yellow] " + value);
                                break;
                            case "ParentRed":
                                sql.Append("[Red] " + value);
                                break;
                            default:
                                break;
                        }
                        i++;
                    }
                }
                else
                {
                    //if (gv1.Columns[2].Visible.Equals(true))
                    //    sql.Append("[Total] DESC ");
                    //else if (gv1.Columns[1].Visible.Equals(true))
                    //    sql.Append("[Usage] DESC ");
                    sql.Append("[Total] DESC");
                }
                lblGrandTotal.Text = grandTotal.ToString();
            }

            return sql.ToString();
        }
        private void bindProjectTotalUsage(string totalUsage, string otherUsage, IDictionary<string, string> sortBy)
        {
            StringBuilder sql = new StringBuilder();
            wwdb db = new wwdb();
            int grandTotal = 0;

            sql.Clear();
            sql.Append(totalUsage);

            db.OpenTable(totalUsage);

            if (db.RecordCount() > 0)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add(new DataColumn("ParentName", typeof(string)));
                dt.Columns.Add(new DataColumn("ProjectName", typeof(string)));
                dt.Columns.Add(new DataColumn("Total", typeof(int)));
                dt.Columns.Add(new DataColumn("Green", typeof(int)));
                dt.Columns.Add(new DataColumn("Yellow", typeof(int)));
                dt.Columns.Add(new DataColumn("Red", typeof(int)));

                while (!db.Eof())
                {
                    DataRow dr = dt.NewRow();
                    grandTotal += ConvertHelper.ConvertToInt(db.Item("Total"), 0);
                    dr["ParentName"] = db.Item("ParentName");
                    dr["ProjectName"] = db.Item("ProjectName");
                    dr["Total"] = ConvertHelper.ConvertToInt(db.Item("Total"), 0);
                    dr["Green"] = ConvertHelper.ConvertToInt(db.Item("Green"), 0);
                    dr["Yellow"] = ConvertHelper.ConvertToInt(db.Item("Yellow"), 0);
                    dr["Red"] = ConvertHelper.ConvertToInt(db.Item("Red"), 0);
                    dt.Rows.Add(dr);

                    db.MoveNext();
                }

                sql.Clear();
                sql.Append(otherUsage);
                db.OpenTable(sql.ToString());
                if (db.RecordCount() > 0)
                {
                    while (!db.Eof())
                    {
                        DataRow dr = dt.NewRow();
                        grandTotal += ConvertHelper.ConvertToInt(db.Item("Total"), 0);
                        dr["ParentName"] = db.Item("ParentName");
                        dr["ProjectName"] = db.Item("ProjectName");
                        dr["Total"] = ConvertHelper.ConvertToInt(db.Item("Total"), 0);
                        dr["Green"] = ConvertHelper.ConvertToInt(db.Item("Green"), 0);
                        dr["Yellow"] = ConvertHelper.ConvertToInt(db.Item("Yellow"), 0);
                        dr["Red"] = ConvertHelper.ConvertToInt(db.Item("Red"), 0);
                        dt.Rows.Add(dr);

                        db.MoveNext();
                    }
                }

                if (sortBy != null && sortBy.Any())
                {
                    int i = 0;
                    foreach (var item in sortBy)
                    {
                        string key = item.Key;
                        string value = item.Value;

                        if (i > 0)
                            sql.Append(",");

                        switch (key)
                        {
                            case "ParentName":
                                dt.DefaultView.Sort = "[ParentName] " + value;
                                break;
                            case "ProjectName":
                                dt.DefaultView.Sort = "[ProjectName] " + value;
                                break;
                            case "ProjectUsage":
                                dt.DefaultView.Sort = "[Total] " + value;
                                break;
                            case "ProjectGreen":
                                dt.DefaultView.Sort = "[Green] " + value;
                                break;
                            case "ProjectYellow":
                                dt.DefaultView.Sort = "[Yellow] " + value;
                                break;
                            case "ProjectRed":
                                dt.DefaultView.Sort = "[Red] " + value;
                                break;
                            default:
                                break;
                        }

                        i++;
                    }
                }
                else
                {
                    dt.DefaultView.Sort = "[Total] DESC";
                }
                lblGrandTotal.Text = grandTotal.ToString();
                dt = dt.DefaultView.ToTable();
                DataSet ds = new DataSet();
                ds.Tables.Add(dt);
                gv.DataSource = ds.Tables[0];
                gv.DataBind();
            }
        }
        private string getProjectTotalUsage(string from, string to, string parentBranchID, string projectID)
        {
            StringBuilder sql = new StringBuilder();
            string cte, dateFilter = string.Empty;

            if (!string.IsNullOrWhiteSpace(from) && !string.IsNullOrWhiteSpace(to) &&
                DateTime.TryParse(from, out _) && DateTime.TryParse(to, out _))
                dateFilter = string.Format(@" 
                                and r.RequestAt between '{0}' and '{1} 23:59'
                             ", from, to);

            DropDownList ddlProjectName = new DropDownList();
            ddlProjectName.Items.Add(new ListItem { Text = "Select", Value = "0", Selected = projectID.Equals("0") ? true : false });
            ddlProjectName.Items.Add(new ListItem { Text = projectID, Value = projectID, Selected = projectID.Equals("0") ? false : true });

            DropDownList ddlParentBranch = new DropDownList();
            ddlParentBranch.Items.Add(new ListItem { Text = "Select", Value = "0", Selected = parentBranchID.Equals("0") ? true : false });
            ddlParentBranch.Items.Add(new ListItem { Text = parentBranchID, Value = parentBranchID, Selected = parentBranchID.Equals("0") ? false : true });

            cte = string.Format(@"
                    with cte as(
                        SELECT distinct
                        isnull((select tb.BranchName
                            from tbl_branch tb with(nolock)
                            where tb.ID = s.ParentId),(select tb.BranchName
                            from tbl_Branch tb with(nolock)
                            where tb.BranchID = s.BranchID)) 
                        as 'ParentName', 
                        ps.ProjectName, ps.ID, ps.BranchId, s.ParentId, 
                            (select count(distinct r.requestID) 
                            from tbl_Request r with (nolock)
                            where r.ProjectID = ps.ID and r.isDeleted = 0 and r.StatusID = 'S' {0}) 
                        as 'Total', 
                            (select count(distinct r.requestID)
                            from tbl_Request r with (nolock)
                            where r.ProjectID = ps.ID and r.isDeleted = 0 and r.StatusID = 'S'
                            and r.ApplicationResult = 'A' {0})
                        as 'Green',
                            (select count(distinct r.requestID)
                            from tbl_Request r with (nolock)
                            where r.ProjectID = ps.ID and r.isDeleted = 0 and r.StatusID = 'S'
                            and r.ApplicationResult='M' {0})
                        as 'Yellow',
                            (select count(distinct r.requestID)
                            from tbl_Request r with (nolock)
                            where r.ProjectID = ps.ID and r.isDeleted=0 and r.StatusID='S'
                            and r.ApplicationResult='D' {0})
                        as 'Red'
                        from tbl_ProjectSettings ps with (nolock), tbl_Request r with (nolock), tbl_Branch s with (nolock)
                        where ps.IsDeleted = 0 and r.isDeleted = 0 and s.IsDeleted = 0 and r.StatusID = 'S'
                        and ((ps.ID = r.ProjectID and ps.BranchId = s.ID) or (r.BranchID = s.BranchID and s.ID = ps.BranchId)) 
                        {0} {1} {2}
                    )
            ", string.IsNullOrEmpty(dateFilter) ? "" : dateFilter
            , ddlProjectName.SelectedIndex > 0 ? sqlString.searchDropDownList("ps.ID", ddlProjectName, true, true, false) : ""
            , ddlParentBranch.SelectedIndex > 0 ?
                string.Format(@" AND (s.ParentID = N'{0}' {1}) "
                , ddlParentBranch.SelectedValue.ToString()
                , sqlString.searchDropDownList("s.ID", ddlParentBranch, true, false, true)) : "");

            sql.Append(cte);
            sql.Append(@"
                select ParentName, ProjectName, Total, Green, Yellow, Red 
                from cte 
                where 1=1 
                order by ProjectName 
            ");

            return sql.ToString();
        }
        private string getOtherProjectUsage(string from, string to, string parentBranchID, string projectID)
        {
            StringBuilder sql = new StringBuilder();
            if (ConvertHelper.ConvertToInt(projectID, 0) <= 0) 
            {
                string parentOther, dateFilter = string.Empty;

                if (!string.IsNullOrWhiteSpace(from) && !string.IsNullOrWhiteSpace(to) &&
                    DateTime.TryParse(from, out _) && DateTime.TryParse(to, out _))
                    dateFilter = " and r.RequestAt between '" + from + "' and '" + to + " 23:59' ";

                //DropDownList ddlProjectName = new DropDownList();
                //ddlProjectName.Items.Add(new ListItem { Text = "Select", Value = "0", Selected = projectID.Equals("0") ? true : false });
                //ddlProjectName.Items.Add(new ListItem { Text = projectID, Value = projectID, Selected = projectID.Equals("0") ? false : true });

                DropDownList ddlParentBranch = new DropDownList();
                ddlParentBranch.Items.Add(new ListItem { Text = "Select", Value = "0", Selected = parentBranchID.Equals("0") ? true : false });
                ddlParentBranch.Items.Add(new ListItem { Text = parentBranchID, Value = parentBranchID, Selected = parentBranchID.Equals("0") ? false : true });

                parentOther = string.Format(@"
                                with parentOther as ( 
                                    select distinct r.RequestID, s.ID as 'SubID', s.BranchID as 'SubBranchID', 
                                        pa.ID as 'ParentID', pa.BranchID as 'ParentBranchID', r.ApplicationResult
                                    FROM tbl_Request r, tbl_Branch s, tbl_Branch pa
                                    where ProjectID is null and r.isDeleted = 0 
                                    and s.IsDeleted = 0 and pa.IsDeleted = 0
                                    and pa.ParentId is null {0}
                                    and s.ParentId = pa.ID 
                                    and (r.BranchID = s.BranchID or r.BranchID = pa.BranchID) {1}
                                )
                                ", ddlParentBranch.SelectedIndex > 0 ? sqlString.searchDropDownList("s.ParentId", ddlParentBranch, true, true, false) : ""
                                , string.IsNullOrEmpty(dateFilter) ? "" : dateFilter);

                sql.Append(parentOther);
                sql.AppendFormat(@"
                    select distinct 
                    'All Other Projects' as 'ParentName',
                    'Others' as 'ProjectName', 
                        count(distinct RequestID) 
                    as 'Total', 
                        (SELECT count(distinct RequestID) 
                        FROM parentOther 
                        WHERE ApplicationResult='A') 
                    AS 'Green', 
                        (SELECT count(distinct RequestID) 
                        FROM parentOther 
                        WHERE ApplicationResult='M') 
                    AS 'Yellow', 
                        (SELECT count(distinct RequestID) 
                        FROM parentOther 
                        WHERE ApplicationResult='D') 
                    AS 'Red' 
                    from parentOther 
                ");
            }

            return sql.ToString();
        }

        private string getAll(string from, string to, string parentBranchID)
        {
            StringBuilder sql = new StringBuilder();
            string dateFilter = "";

            gv.Columns[1].Visible = false;
            gv.Columns[2].Visible = false;
            gv.Columns[3].Visible = false;

            if (!parentBranchID.Equals("0"))
            {
                gv.Columns[3].Visible = true;
            }
            else
            {
                gv.Columns[1].Visible = true;
                gv.Columns[2].Visible = true;
                gv.Columns[3].Visible = true;
            }

            if (!string.IsNullOrWhiteSpace(from) && !string.IsNullOrWhiteSpace(to) &&
                DateTime.TryParse(from, out var outfrom) && DateTime.TryParse(to, out var outto))
                dateFilter = " and r.RequestAt between '" + from + "' and '" + to + " 23:59' ";

            DropDownList ddlParentBranch = new DropDownList();
            ddlParentBranch.Items.Add(new ListItem { Text = "Select", Value = "0", Selected = parentBranchID.Equals("0") ? true : false });
            ddlParentBranch.Items.Add(new ListItem { Text = parentBranchID, Value = parentBranchID, Selected = parentBranchID.Equals("0") ? false : true });

            sql.AppendFormat(@"
                SELECT distinct
                {0}
                ps.ProjectName AS 'ProjectName', 
                    (SELECT count(distinct r.RequestID) 
                    FROM tbl_Request r 
                    WHERE r.ProjectID = ps.ID and r.isDeleted = 0 and r.StatusID = 'S' {1} {2}) 
                as 'Usage', 
                    (SELECT count(distinct r.RequestID) 
                    FROM tbl_Request r 
                    WHERE r.ProjectID = ps.ID and r.isDeleted = 0 and r.StatusID='S' and r.ApplicationResult = 'A' {1} {2}) 
                as 'Green', 
                    (SELECT count(distinct r.RequestID) 
                    FROM tbl_Request r 
                    WHERE r.ProjectID = ps.ID and r.isDeleted = 0 and r.StatusID='S' and r.ApplicationResult = 'M' {1} {2}) 
                as 'Yellow', 
                    (SELECT count(distinct r.RequestID) 
                    FROM tbl_Request r 
                    WHERE r.ProjectID = ps.ID and r.isDeleted = 0 and r.StatusID='S' and r.ApplicationResult = 'D' {1} {2}) 
                as 'Red' 
                FROM tbl_Request r 
                inner join tbl_ProjectSettings ps on r.ProjectID = ps.ID 
                inner join tbl_Branch s on (ps.BranchId = s.ID or ps.BranchId = s.ParentId) 
                right join tbl_Branch pa on pa.ID = s.ParentId 
                where pa.ParentId IS NULL and pa.IsDeleted = 0 and ps.IsDeleted = 0 and s.IsDeleted = 0 and r.isDeleted = 0 
                and r.StatusID = 'S' and s.ParentId = pa.ID and (ps.BranchId = s.ID OR ps.BranchId = pa.ID) {1} {2}
                group by pa.BranchName, s.BranchName,pa.ID, ps.ID, ps.ProjectName, ps.ProjectID
                {3}
            ", parentBranchID.Equals("0") ? @"
                pa.BranchName AS 'ParentBranchName', 
                s.BranchName AS 'SubBranchName', " : ""
            , dateFilter
            , sqlString.searchDropDownList("pa.ID", ddlParentBranch, true, true, false)
            , !parentBranchID.Equals("0") ? "order by [Usage] DESC " : "order by [ParentBranchName] ");

            return sql.ToString();
        }
        private int countGrandTotal(DataTable dt)
        {
            int total;

            total = ConvertHelper.ConvertToInt(dt.Rows[0].ItemArray[6].ToString(), 0);
            //if (dt != null && dt.Rows.Count > 0)
            //    for (int row = 0; row < dt.Rows.Count; row++)
            //        total += ConvertHelper.ConvertToInt(dt.Rows[row].ItemArray[6].ToString(), 0);

            return total;
        }
        private string getParentBranchUsage(string from, string to, string parentBranch, IDictionary<string, string> sortBy)
        {
            StringBuilder sql = new StringBuilder();
            string dateFilter = "";

            if (!string.IsNullOrWhiteSpace(from) && !string.IsNullOrWhiteSpace(to) &&
                DateTime.TryParse(from, out var outfrom) && DateTime.TryParse(to, out var outto))
                dateFilter = " and r.RequestAt between '" + from + "' and '" + to + " 23:59' ";

            DropDownList ddlParentBranch = new DropDownList();
            ddlParentBranch.Items.Add(new ListItem { Text = "Select", Value = "0", Selected = parentBranch.Equals("0") ? true : false });
            ddlParentBranch.Items.Add(new ListItem { Text = parentBranch, Value = parentBranch, Selected = parentBranch.Equals("0") ? false : true });

            sql.AppendFormat(@"
                SELECT pa.BranchName AS 'Parent Branch Name', 
                    (SELECT count(distinct r.RequestID) 
                    FROM tbl_Request r 
                    WHERE r.BranchID = pa.BranchID and r.isDeleted = 0 and r.StatusID='S' {0} {1}) 
                as 'Usage', 
                    count(distinct r.RequestID) 
                as 'Total', 
                    (SELECT count(distinct r.RequestID) 
                    FROM tbl_Request r 
                    WHERE r.BranchID = pa.BranchID and r.isDeleted = 0 and r.StatusID='S' {0} {1}
                    and r.ApplicationResult = 'A' ) 
                as 'Green', 
                    (SELECT count(distinct r.RequestID) 
                    FROM tbl_Request r 
                    WHERE r.BranchID = pa.BranchID and r.isDeleted = 0 and r.StatusID='S' {0} {1}
                    and r.ApplicationResult = 'M' ) 
                as 'Yellow', 
                    (SELECT count(distinct r.RequestID) 
                    FROM tbl_Request r 
                    WHERE r.BranchID = pa.BranchID and r.isDeleted = 0 and r.StatusID='S' {0} {1}
                    and r.ApplicationResult = 'D' ) 
                as 'Red' 
                FROM tbl_Request r 
                inner join tbl_ProjectSettings ps on r.ProjectID = ps.ID 
                inner join tbl_Branch s on (ps.BranchId = s.ID or ps.BranchId = s.ParentId) 
                right join tbl_Branch pa on pa.ID = s.ParentId where pa.ParentId IS NULL 
                and pa.IsDeleted = 0 and ps.IsDeleted = 0 and s.IsDeleted = 0 and r.isDeleted = 0 
                {0} and r.StatusID = 'S' 
                group by pa.BranchName, pa.ID, pa.BranchID 
                order by 
            ", sqlString.searchDropDownList("pa.ID", ddlParentBranch, true, true, false)
            , dateFilter);

            //ordering
            if (sortBy != null && sortBy.Any())
            {
                int i = 0;
                foreach (var item in sortBy)
                {
                    string key = item.Key;
                    string value = item.Value;

                    if (i > 0)
                        sql.Append(",");

                    switch (key)
                    {
                        case "ParentBranchName":
                            sql.Append("[Parent Branch Name] " + value);
                            break;
                        case "ParentUsage":
                            sql.Append("[Usage] " + value);
                            break;
                        case "ParentTotal":
                            sql.Append("[Total] " + value);
                            break;
                        case "ParentGreen":
                            sql.Append("[Green] " + value);
                            break;
                        case "ParentYellow":
                            sql.Append("[Yellow] " + value);
                            break;
                        case "ParentRed":
                            sql.Append("[Red] " + value);
                            break;
                        default:
                            break;
                    }

                    i++;
                }
            }
            else
            {
                sql.Append("[Usage] DESC ");
            }

            return sql.ToString();
        }
        private string getSubBranchUsage(string from, string to, string parentBranch, string subBranch, IDictionary<string, string> sortBy)
        {
            StringBuilder sql = new StringBuilder();
            string dateFilter = "";
            if (!string.IsNullOrWhiteSpace(from) && !string.IsNullOrWhiteSpace(to) &&
                DateTime.TryParse(from, out _) && DateTime.TryParse(to, out _))
                dateFilter = " and tbl_Request.RequestAt between '" + from + "' and '" + to + " 23:59' ";

            DropDownList ddlSubBranch = new DropDownList();
            ddlSubBranch.Items.Add(new ListItem { Text = "Select", Value = "0", Selected = subBranch.Equals("0") ? true : false });
            ddlSubBranch.Items.Add(new ListItem { Text = subBranch, Value = subBranch, Selected = subBranch.Equals("0") ? false : true });

            DropDownList ddlParentBranch = new DropDownList();
            ddlParentBranch.Items.Add(new ListItem { Text = "Select", Value = "0", Selected = parentBranch.Equals("0") ? true : false });
            ddlParentBranch.Items.Add(new ListItem { Text = parentBranch, Value = parentBranch, Selected = parentBranch.Equals("0") ? false : true });

            sql.AppendFormat(@"
                SELECT distinct b.BranchName AS 'Sub Branch Name', 
                    (SELECT count(tbl_Request.RequestID) 
                    FROM tbl_Request 
                    WHERE tbl_Request.BranchID = b.BranchID AND tbl_Request.isdeleted = 0 AND tbl_Request.statusID = 'S' {0} {1} {2}) 
                AS 'Usage', 
                    (SELECT count(tbl_Request.RequestID) 
                    FROM tbl_Request 
                    WHERE tbl_Request.BranchID = b.BranchID AND tbl_Request.isdeleted = 0 AND tbl_Request.statusID = 'S' {0} {1} {2}
                    and tbl_Request.ApplicationResult = 'A' ) 
                AS 'Green', 
                    (SELECT count(tbl_Request.RequestID) 
                    FROM tbl_Request  
                    WHERE tbl_Request.BranchID = b.BranchID AND tbl_Request.isdeleted = 0 AND tbl_Request.statusID = 'S' {0} {1} {2}
                    and tbl_Request.ApplicationResult = 'M' ) 
                AS 'Yellow', 
                    (SELECT count(tbl_Request.RequestID) 
                    FROM tbl_Request 
                    WHERE tbl_Request.BranchID = b.BranchID AND tbl_Request.isdeleted = 0 AND tbl_Request.statusID = 'S' {0} {1} {2}
                    and tbl_Request.ApplicationResult = 'D' ) 
                AS 'Red' 
                FROM tbl_Branch b, tbl_Branch a 
                WHERE b.parentid IS NOT NULL and b.IsDeleted = 0 and a.ParentId is null and a.IsDeleted = 0 and b.ParentId = a.ID 
                {0} {1} {2}
                order by
            ", sqlString.searchDropDownList("b.ID", ddlSubBranch, true, true, false)
            , sqlString.searchDropDownList("a.ID", ddlParentBranch, true, true, false)
            , dateFilter);

            //ordering
            if (sortBy != null && sortBy.Any())
            {
                int i = 0;
                foreach (var item in sortBy)
                {
                    string key = item.Key;
                    string value = item.Value;

                    if (i > 0)
                        sql.Append(",");

                    switch (key)
                    {
                        case "SubBranchName":
                            sql.Append("[Sub Branch Name] " + value);
                            break;
                        case "SubUsage":
                            sql.Append("[Usage] " + value);
                            break;
                        case "SubGreen":
                            sql.Append("[Green] " + value);
                            break;
                        case "SubYellow":
                            sql.Append("[Yellow] " + value);
                            break;
                        case "SubRed":
                            sql.Append("[Red] " + value);
                            break;
                        default:
                            break;
                    }
                    i++;
                }
            }
            else
                sql.Append("[Usage] DESC ");

            return sql.ToString();
        }

        private string getProjectUsage(string from, string to, string parentBranchID, string subBranchID, string projectID, IDictionary<string, string> sortBy)
        {
            StringBuilder sql = new StringBuilder();

            DropDownList ddlParentBranch = new DropDownList();
            ddlParentBranch.Items.Add(new ListItem { Text = "Select", Value = "0", Selected = parentBranchID.Equals("0") ? true : false });
            ddlParentBranch.Items.Add(new ListItem { Text = parentBranchID, Value = parentBranchID, Selected = parentBranchID.Equals("0") ? false : true });
            DropDownList ddlSubBranch = new DropDownList();
            ddlSubBranch.Items.Add(new ListItem { Text = "Select", Value = "0", Selected = subBranchID.Equals("0") ? true : false });
            ddlSubBranch.Items.Add(new ListItem { Text = subBranchID, Value = subBranchID, Selected = subBranchID.Equals("0") ? false : true });
            DropDownList ddlProjectName = new DropDownList();
            ddlProjectName.Items.Add(new ListItem { Text = "Select", Value = "0", Selected = projectID.Equals("0") ? true : false });
            ddlProjectName.Items.Add(new ListItem { Text = projectID, Value = projectID, Selected = projectID.Equals("0") ? false : true });

            string dateFilter = "";

            if (!string.IsNullOrWhiteSpace(from) && !string.IsNullOrWhiteSpace(to) &&
                DateTime.TryParse(from, out _) && DateTime.TryParse(to, out _))
                dateFilter = " and {0}.RequestAt between '" + from + "' and '" + to + " 23:59' ";

            sql.Clear();
            sql.AppendFormat(@"
                with allProject as (
                    select ps.ID as 'PID', ps.ProjectName, b.ID as 'BID', 
					isnull(b.BranchName, 'All Agency') as 'BranchName', 
	                b.ParentId, pa.BranchName as 'ParentName', 
	                    count(*) as 'Total',
	                    (select count(*) 
	                    from tbl_Request tr with (nolock)
	                    inner join tbl_ProjectSettings tps with (nolock) on tr.ProjectID = tps.ID
	                    where tr.ApplicationResult = 'A' and tps.ID = ps.ID and tr.isDeleted = 0 {3}) 
                    as 'Green',
	                    (select count(*) 
	                    from tbl_Request tr with (nolock)
	                    inner join tbl_ProjectSettings tps with (nolock) on tr.ProjectID = tps.ID
	                    where tr.ApplicationResult = 'M' and tps.ID = ps.ID and tr.isDeleted = 0 {3}) 
                    as 'Yellow',
	                    (select count(*) 
	                    from tbl_Request tr with (nolock)
	                    inner join tbl_ProjectSettings tps with (nolock) on tr.ProjectID = tps.ID
	                    where tr.ApplicationResult = 'D' and tps.ID = ps.ID and tr.isDeleted = 0 {3}) 
                    as 'Red'
                    from tbl_Request r with (nolock)
                    inner join tbl_ProjectSettings ps with (nolock) on r.ProjectID = ps.ID
                    left join tbl_Branch b with (nolock) on ps.BranchId = b.ID
                    left join tbl_Branch pa with (nolock) on pa.ID = b.ParentId
                    where r.isDeleted = 0 and ps.IsDeleted = 0 and r.StatusID = 'S' {4}
                    group by ps.ID, ps.ProjectName, b.ID, b.BranchName, b.ParentId, pa.BranchName, ps.ID
                )

                select isnull(ap.ParentName, ap.BranchName) as 'ParentName', 
	                ap.ProjectName, ap.Total, ap.Green, ap.Yellow, ap.Red,
	                sum(ap.Total) over() as 'GrandTotal'
                from allProject ap
                where 1=1 {0} {1} {2}

                ----To display all 0 usage projects
                --union
                --select ps.ProjectName, '0', 0, 0, 0 
                --from tbl_ProjectSettings ps with (nolock)
                --where ps.IsDeleted = 0 
                --and ps.ID not in (select ap1.PID 
                --	from allProject ap1)
            ", (ddlParentBranch.SelectedIndex > 0) ?
                sqlString.searchDropDownList("(ap.ParentId", ddlParentBranch, true, true, false) +
                    sqlString.searchDropDownList("ap.BID", ddlParentBranch, true, false, true) + ")" : ""
            , sqlString.searchDropDownList("ap.BID", ddlSubBranch, true, true, false)
            , sqlString.searchDropDownList("ap.PID", ddlProjectName, true, true, false)
            , string.Format(dateFilter, "tr")
            , string.Format(dateFilter, "r"));

            sql.Append(" order by ");
            //ordering
            if (sortBy != null && sortBy.Any())
            {
                int i = 0;
                foreach (var item in sortBy)
                {
                    string key = item.Key;
                    string value = item.Value;

                    if (i > 0)
                        sql.Append(",");

                    switch (key)
                    {
                        case "ProjectName":
                            sql.Append("[BranchName] " + value);
                            break;
                        case "ProjectUsage":
                            sql.Append("[Total] " + value);
                            break;
                        case "ProjectGreen":
                            sql.Append("[Green] " + value);
                            break;
                        case "ProjectYellow":
                            sql.Append("[Yellow] " + value);
                            break;
                        case "ProjectRed":
                            sql.Append("[Red] " + value);
                            break;
                        default:
                            break;
                    }

                    i++;
                }
            }
            else
                sql.Append("[Total] " + "DESC");

            return sql.ToString();
        }
        private string getProjectUsageOld(string from, string to, string parentBranch, string subBranch, string projectName, IDictionary<string, string> sortBy)
        {
            StringBuilder sql = new StringBuilder();
            string dateFilter = "";
            if (!string.IsNullOrWhiteSpace(from) && !string.IsNullOrWhiteSpace(to) &&
                DateTime.TryParse(from, out _) && DateTime.TryParse(to, out _))
                dateFilter = " and tbl_Request.RequestAt between '" + from + "' and '" + to + " 23:59' ";

            DropDownList ddlSubBranch = new DropDownList();
            ddlSubBranch.Items.Add(new ListItem { Text = "Select", Value = "0", Selected = subBranch.Equals("0") ? true : false });
            ddlSubBranch.Items.Add(new ListItem { Text = subBranch, Value = subBranch, Selected = subBranch.Equals("0") ? false : true });

            DropDownList ddlParentBranch = new DropDownList();
            ddlParentBranch.Items.Add(new ListItem { Text = "Select", Value = "0", Selected = parentBranch.Equals("0") ? true : false });
            ddlParentBranch.Items.Add(new ListItem { Text = parentBranch, Value = parentBranch, Selected = parentBranch.Equals("0") ? false : true });

            DropDownList ddlProjectName = new DropDownList();
            ddlProjectName.Items.Add(new ListItem { Text = "Select", Value = "0", Selected = projectName.Equals("0") ? true : false });
            ddlProjectName.Items.Add(new ListItem { Text = projectName, Value = projectName, Selected = projectName.Equals("0") ? false : true });

            sql.AppendFormat(@"
                SELECT distinct p.ProjectName as 'Project Name', 
                    (SELECT count(distinct RequestID) 
                    FROM tbl_Request 
                    WHERE tbl_Request.ProjectID = p.ID and tbl_Request.isDeleted = 0 and tbl_Request.StatusID = 'S' 
                    {0} {1} {2} {3})
                as 'Usage', 
                    (SELECT count(distinct RequestID) 
                    FROM tbl_Request 
                    WHERE tbl_Request.ProjectID = p.ID and tbl_Request.isDeleted = 0 and tbl_Request.StatusID = 'S' 
                    {0} {1} {2} {3} 
                    and tbl_Request.ApplicationResult='A')
                as 'Green', 
                    (SELECT count(distinct RequestID) 
                    FROM tbl_Request 
                    WHERE tbl_Request.ProjectID = p.ID and tbl_Request.isDeleted = 0 and tbl_Request.StatusID = 'S' 
                    {0} {1} {2} {3}
                    and tbl_Request.ApplicationResult='M')
                as 'Yellow', 
                    (SELECT count(distinct RequestID) 
                    FROM tbl_Request 
                    WHERE tbl_Request.ProjectID = p.ID and tbl_Request.isDeleted = 0 and tbl_Request.StatusID = 'S' 
                    {0} {1} {2} {3}
                    and tbl_Request.ApplicationResult='D')
                as 'Red'
                FROM tbl_ProjectSettings p, tbl_Branch pa, tbl_Branch s 
                where (p.IsDeleted = 0 and p.ProjectID IS NOT NULL and pa.IsDeleted = 0 and s.IsDeleted = 0 
                and pa.ParentId is null and s.ParentId is not null)  and s.ParentId = pa.ID and (p.BranchId = s.ID OR p.BranchId = pa.ID) 
                {0} {1} {2} {4}
                order by
            ", ddlSubBranch.SelectedIndex > 0 ? sqlString.searchDropDownList("s.ID", ddlSubBranch, true, true, false) : ""
            , ddlParentBranch.SelectedIndex > 0 ? sqlString.searchDropDownList("pa.ID", ddlParentBranch, true, true, false) : ""
            , ddlProjectName.SelectedIndex > 0 ? sqlString.searchDropDownList("p.ID", ddlProjectName, true, true, false) : ""
            , dateFilter
            , (ddlParentBranch.SelectedIndex.Equals(0) && ddlSubBranch.SelectedIndex.Equals(0) && ddlProjectName.SelectedIndex.Equals(0)) ||
                (ddlParentBranch.SelectedIndex.Equals(-1) && ddlSubBranch.SelectedIndex.Equals(-1) && ddlProjectName.SelectedIndex.Equals(-1)) ? @"
                    OR((p.BranchId = '' OR p.BranchId is null) AND p.isDeleted = 0 AND pa.isDeleted = 0 
                    AND s.isDeleted = 0 and pa.ParentId is null and s.ParentId is not null and p.ProjectID IS NOT NULL)" : "");

            //ordering
            if (sortBy != null && sortBy.Any())
            {
                int i = 0;
                foreach (var item in sortBy)
                {
                    string key = item.Key;
                    string value = item.Value;

                    if (i > 0)
                        sql.Append(",");

                    switch (key)
                    {
                        case "ProjectName":
                            sql.Append("Project Name " + value);
                            break;
                        case "Usage":
                            sql.Append("Usage " + value);
                            break;
                        case "ProjectGreen":
                            sql.Append("Green " + value);
                            break;
                        case "ProjectYellow":
                            sql.Append("Yellow " + value);
                            break;
                        case "ProjectRed":
                            sql.Append("Red " + value);
                            break;
                        default:
                            break;
                    }

                    i++;
                }
            }
            else
                sql.Append("[Usage] DESC ");

            return sql.ToString();
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            StringBuilder sql = new StringBuilder();

            string from = secure.Decrypt(Validation.GetUrlParameter(this.Page, "from", ""), true);
            string to = secure.Decrypt(Validation.GetUrlParameter(this.Page, "to", ""), true);
            string parentBranchID = secure.Decrypt(Validation.GetUrlParameter(this.Page, "parentbranchid", ""), true);
            string subBranchID = secure.Decrypt(Validation.GetUrlParameter(this.Page, "subbranchid", ""), true);
            string projectID = secure.Decrypt(Validation.GetUrlParameter(this.Page, "projectid", ""), true);

            sql.Clear();
            sql.Append(getProjectUsage(from, to, parentBranchID, subBranchID, projectID, null));

            sqlString.exportFunc("WhoPay_UsageReport_" + (DateTime.UtcNow.ToShortDateString().ToString()).Replace("_", "-"), sql.ToString(), true);
        }
    }
}