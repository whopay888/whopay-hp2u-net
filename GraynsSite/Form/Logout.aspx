﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Logout.aspx.cs" Inherits="WMElegance.Form.Logout" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <script>
                function confirmDelete() {

                    var x = confirm('<%= GetGlobalResourceObject("resource", "LogoutMessage")%>');
                    if (x) {
                        //alert('yahoo');
                        document.getElementById('<%= btnLogout.ClientID %>').click();
                    }
                    else {
                        document.getElementById('<%= btnBack.ClientID %>').click();
                    }
                }
            </script>
            <asp:HiddenField runat="server" ID="Hidden1" />
            <asp:Button runat="server" ID="btnLogout" style="display: none;" OnClick="btnLogout_Click" />
            <asp:Button runat="server" ID="btnBack" style="display: none;" OnClick="btnBack_Click" />
        </div>
    </form>
</body>
</html>
