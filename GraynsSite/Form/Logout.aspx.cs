﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using Synergy.Util;

namespace WMElegance.Form
{
    public partial class Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //ClientScript.RegisterStartupScript(GetType(), "messagebox", "<script language='javascript' type='text/javascript'>confirm('Do you want to save the WH XML file created?');</script>");

                if (Session[KeyVal.ProjectName + "_CartList"] != null)
                {
                    //ScriptManager.RegisterStartupScript(this, typeof(string), "Message",
                    //"var a = confirm('" + Resources.resource.No + "'); if (a) { document.getElementById('hfLogout').value = '1'; } else { document.getElementById('hfLogout').value = '0'; } ", true);
                    
                    //temporary comment until further notice
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "ajax", "<script language='javascript'>confirmDelete();</script>", false);
                    btnLogout_Click(null,null);
                }
                else
                    btnLogout_Click(null, null);



                //if (Session[KeyVal.session_member] != null)
                //{
                //    Session.Remove(KeyVal.session_member);
                //    Response.Redirect("/Form/user/MemberList.aspx");
                //}
                //else if (Session[KeyVal.session_clogin] != null)
                //{
                //    Session.Remove(KeyVal.session_clogin);
                //    Response.Redirect("/Form/Login.aspx");
                //}
                //else
                //{
                //    Session.RemoveAll();
                //    Response.Redirect("/Form/Login.aspx");
                //}

                //Response.Redirect("/Form/User/MemberList.aspx");
                //Response.Redirect("/Form/Home.aspx");
            }
        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            if (Session[KeyVal.session_member] != null)
            {
                Session.Remove(KeyVal.session_member);
                Response.Redirect("/Form/user/MemberList.aspx");
            }
            else if (Session[KeyVal.session_clogin] != null)
            {
                Session.Remove(KeyVal.session_clogin);
                Response.Redirect("/Form/Login.aspx");
            }
            else
            {
                Session.RemoveAll();
                Response.Redirect("/Form/Login.aspx");
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (Request["ru"] != null)
            {
                Response.Redirect(sqlString.decryptURL(Request["ru"]));
            }
            else
                Response.Redirect("/Form/Home.aspx");
        }
    }
}