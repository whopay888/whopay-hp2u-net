﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditProfile.aspx.cs" Inherits="HJT.Form.User.EditProfile" MasterPageFile="~/Global1.Master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function confirmSave() {
            return confirm('<%= GetGlobalResourceObject("resource" , "Confirm_Changes") %>');
        }
        function confirmRemove() {
            return confirm('<%= GetGlobalResourceObject("resource" , "Confirm_remove") %>');
        }

        function FileUpload(fuid) {
            $('#<%= hid_FUID.ClientID %>').val(fuid);
            $('#<%= btnUpload.ClientID %>').click();
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                <span class="icon fa fa-fw fa-book" aria-hidden="true"></span>
                <asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><span runat="server" id="spHeader"><%= GetGlobalResourceObject("resource", "Profile_Setting")%></span></asp:Label>
            </h1>
            <asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
        </section>
        <section class="content">
            <div class="col-md-12">
                <div runat="server">
                    <div class="box-body">
                        <div id="divErrorMessage" runat="server" visible="false" class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <b>Alert!</b>
                            <br />
                            <asp:Label runat="server" ID="lblErr"></asp:Label>
                        </div>
                        <asp:Panel runat="server" DefaultButton="btnSave">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr class="form-group">
                                        <td style="width: 20%; border-left: 0px!important;">
                                            <asp:Label runat="server" Text="<%$ Resources:resource, Signup_Date %>" />
                                        </td>
                                        <td style="width: 80%;" class="controls">
                                            <asp:Label runat="server" ID="lblSignupDate" />
                                        </td>
                                    </tr>
                                    <tr class="form-group">
                                        <td style="width: 20%; border-left: 0px!important;">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "Name")%>&nbsp;<span style="color: Red;">*</span></label>
                                        </td>
                                        <td style="width: 80%;" class="controls">
                                            <asp:TextBox ID="txtName" runat="server" CssClass="form-control" />
                                        </td>
                                    </tr>
                                    <tr class="form-group">
                                        <td style="width: 20%; border-left: 0px!important;">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "Profile_Picture")%></label>
                                        </td>
                                        <td style="width: 80%;" class="controls">
									        <asp:FileUpload runat="server" ID="fu1" onchange="FileUpload('fu1')" />
                                            <asp:Image runat="server" ID="img_fu1" ImageUrl="/Images/Doc_Icon.png" Width="50px" Visible="false" />
                                            <asp:Button runat="server" ID="btnUpload" Style="display: none;" OnClick="btnUpload_Click" />
                                            <asp:Button runat="server" ID="btnRemove" CssClass="btn btn-danger" OnClick="btnRemove_Click"
                                                Text="<%$ Resources:resource, Remove %>" OnClientClick="return confirmRemove();"/>
                                            <asp:Image runat="server" ID="imgPreview" Visible="false" />
									        <asp:HiddenField runat="server" ID="hid_FUID" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="box-footer">
                                <asp:Button runat="server" ID="btnSave" CssClass="btn btn-edit" OnClick="btnSave_Click"
                                    Text="<%$ Resources:Resource, Save%>" OnClientClick="return confirmSave();" />
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </section>
    </aside>
</asp:Content>