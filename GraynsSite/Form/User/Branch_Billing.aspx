﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Branch_Billing.aspx.cs" Inherits="HJT.Form.User.Branch_Billing" MasterPageFile="~/Global1.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function confirmSubmit() {
            return confirm('<%= GetGlobalResourceObject("resource" , "Confirm_submit") %>');
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                <span class="icon fa fa-fw fa-book" aria-hidden="true"></span>
                <asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><span runat="server" id="spHeader"><%= GetGlobalResourceObject("resource", "Add_Credit")%></span></asp:Label>
            </h1>
            <asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
        </section>
        <section class="content">
            <div class="col-md-12">
                <div runat="server">
                    <div class="box-body">
                        <asp:Panel runat="server" DefaultButton="btnSearchName">
                            <table class="table table-bordered">
                            <tbody>
                                <tr class="form-group" runat="server">
                                    <td style="width: 20%; border-left: 0px!important;">
                                        <label style="width:100%" class="control-label"><%= GetGlobalResourceObject("resource", "SearchByFullName")%>&nbsp;<span style="color: Red;">*</span></label>
                                    </td>
                                    <td style="width: 80%;" class="controls">
                                        <div style="margin-left:-10px;">
                                            <asp:Panel runat="server" DefaultButton="btnSearchName">
                                                <table style="width:100%;">
                                                    <tr>
                                                        <td style="width: 80%;" class="controls">
                                                            <asp:TextBox runat="server" ID="txtSearchName" Width="100%" CssClass= "form-control" />
                                                        </td>
                                                        <td style="width: 20%;" class="controls">
                                                            <asp:Button runat="server" ID="btnSearchName" OnClick="btnSearchName_Click" CssClass="btn btn-searh" 
                                                                Text='<%$ Resources:resource, Search %>' />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="form-group">
                                    <td style="width: 20%; border-left: 0px!important;">
                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "FullName")%>&nbsp;<span style="color: Red;">*</span></label>
                                    </td>
                                    <td style="width: 80%;" class="controls">
                                        <asp:DropDownList runat="server" ID="ddlFullname" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlFullName_SelectedIndexChanged" />
                                    </td>
                                </tr>
                                <tr class="form-group">
                                    <td style="width: 20%; border-left: 0px!important;">
                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Branch")%></label>
                                    </td>
                                    <td style="width: 80%;" class="controls">
                                        <asp:Label ID="lblBranchName" runat="server" Text="<%$ Resources:resource, Please_Select_Name %>"/>
                                    </td>
                                </tr>
                                <tr class="form-group">
                                    <td style="width: 20%; border-left: 0px!important;">
                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "ContactNumber")%></label>
                                    </td>
                                    <td style="width: 80%;" class="controls">
                                        <asp:Label ID="lblContactNumber" runat="server"/>
                                    </td>
                                </tr>
                                <tr class="form-group">
                                    <td style="width: 20%; border-left: 0px!important;">
                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Balance")%></label>
                                    </td>
                                    <td style="width: 80%;" class="controls">
                                        <asp:Label ID="lblBalance" runat="server"/>
                                        <asp:Label ID="lblCreditBalancePreview" runat="server" Font-Size="Small" ForeColor="LightGreen" Font-Bold="true" Visible="false" />
                                    </td>
                                </tr>
                                <tr class="form-group">
                                    <td style="width: 20%; border-left: 0px!important;">
                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "FreeRequest")%></label>
                                    </td>
                                    <td style="width: 80%;" class="controls">
                                        <asp:Label ID="lblFreeRequest" runat="server"/>
                                        <asp:Label ID="lblFreeRequestPreview" runat="server" Font-Size="Small" ForeColor="LightGreen" Font-Bold="true" Visible="false" />
                                    </td>
                                </tr>
                                <tr class="form-group">
                                    <td style="width: 20%; border-left: 0px!important;">
                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Amount_Paid")%>&nbsp;<span style="color: Red;">*</span></label>
                                    </td>
                                    <td style="width: 80%;" class="controls">
                                        <asp:DropDownList ID="ddlPayAmount" runat="server" Font-Size="Medium" CssClass="form-control" 
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlPayAmount_SelectedIndexChanged" />
                                    </td>
                                </tr>
                                <tr class="form-group">
                                    <td style="width: 20%; border-left: 0px!important;">
                                        <label class="control-label"><%= GetGlobalResourceObject("resource", "Remark")%></label>
                                    </td>
                                    <td style="width: 80%;" class="controls">
                                        <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" CssClass="form-control" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </asp:Panel>
                        <div class="box-footer">
                            <asp:Button runat="server" ID="btnSubmit" Visible="false" CssClass="btn btn-edit" OnClick="btnSubmit_Click" 
                                Text="<%$ Resources:Resource, Pay_Now%>" OnClientClick="return confirmSubmit();" />
                        </div>
                        <br /><br />
                        <div>
                            <header>    
                                <div class = "table-title"><asp:Literal runat="server" Text="<%$ Resources:Resource, PaymentHistory%>" /></div>
                            </header>
                            <div class="box-body table-responsive">
                                <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow" ID="gv" runat="server" AutoGenerateColumns="false" GridLines="None" ShowFooter="false"
                                    EmptyDataText="<%$ Resources:Resource, No_Record_Found%>" CssClass="table table-bordered table-striped" Width="100%" Visible="false">
                                    <Columns>
                                        <asp:BoundField ItemStyle-Width="15%" DataField="Fullname" HeaderText="<%$ Resources:Resource, Name%>" />
                                        <asp:BoundField ItemStyle-Width="10%" DataField="Amount" HeaderText="<%$ Resources:Resource, Credit_Amount%>" />
                                        <asp:BoundField ItemStyle-Width="20%" Visible="false" DataField="CreatedBy" HeaderText="<%$ Resources:Resource, CreatedBy%>" />
                                        <asp:BoundField ItemStyle-Width="25%" DataField="CreatedAt" HeaderText="<%$ Resources:Resource, Dates%>" />
                                        <asp:BoundField ItemStyle-Width="60%" DataField="Remark" HeaderText="<%$ Resources:Resource, Remark%>" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </aside>
</asp:Content>
