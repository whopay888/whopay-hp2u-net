﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Global1.Master" AutoEventWireup="true" CodeBehind="MemberList.aspx.cs" Inherits="EDV.Form.User.MemberList" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script>

        function confirmUpdate() {
            return confirm('<%= GetGlobalResourceObject("resource" , "Confirm_Save_Changes")%>');
        }


        function confirmSuspend() {
            return confirm('<%= GetGlobalResourceObject("resource" , "Confirm_Suspend_Member")%>');
        }

        $(window).load(function () {
            var x = document.getElementById("filtercontent");
            var lblfilter = document.getElementById("lblfilter");
            if (x.style.display === "none") {
                x.style.display = "block";
                lblfilter.innerHTML = "Hide Filters"
            } else {
                x.style.display = "none";
                lblfilter.innerHTML = "Show Filters"
            }
        });

        function myFunction() {
            var x = document.getElementById("filtercontent");
            var lblfilter = document.getElementById("lblfilter");
            if (x.style.display === "none") {
                x.style.display = "block";
                lblfilter.innerHTML = "Hide Filters"
            } else {
                x.style.display = "none";
                lblfilter.innerHTML = "Show Filters"
            }
        }

        function NewFunction() {
            var lblfilter = document.getElementById("lblfilter");
            lblfilter.innerHTML = "Hide Filters"
        }
    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <aside class="right-side">
        <section class="content-header">
            <h1>
                <span class="icon fa fa-fw fa-book" aria-hidden="true"></span>
                <asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><span runat="server" id="spHeader"><%= GetGlobalResourceObject("resource", "Member_List")%></span></asp:Label>
            </h1>
            <asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
        </section>
        <section class="content">
            <div class="col-md-12">
                <div runat="server">
                    <div id="filter">
                        <a href="#0" id="showfilter" onclick="myFunction()">
                            <label id="lblfilter" class="btn btn-searh">Show Filters</label>
                        </a>
                        <div class="box">
                            <div id="filtercontent">
                                <asp:Panel runat="server" DefaultButton="btnSearch">
                                    <h4>
                                        <asp:Label ID="panel" runat="server" Text="<%$ Resources:Resource, Selection_Option%>"/>
                                    </h4>
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "SearchByUsername")%></label>
                                            <div class="controls">
                                                <asp:TextBox runat="server" ID="txtUsername" CssClass="form-control"/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "SearchByFullName")%></label>
                                            <div class="controls">
                                                <asp:TextBox runat="server" ID="txtFullName" CssClass="form-control"/>
                                            </div>
                                        </div>

                                        <div class="form-group" runat="server" id="divBranch">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "Branch")%></label>
                                            <div class="controls">
                                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlBranch">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        <asp:Button ID="btnSearch" Text="<%$ Resources:Resource, Search%>" runat="server" CssClass="btn btn-searh" OnClick="btnSearch_Click" />
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                        <div class="box">
                            <h4>
                                <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, member_list%>"/>
                            </h4>

                            <div class="box-body table-responsive">
                                <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow" ID="gv" runat="server" AutoGenerateColumns="false" GridLines="None" AllowPaging="True" ShowFooter="true"
                                    EmptyDataText="<%$ Resources:Resource, No_Record_Found%>" CssClass="table table-bordered table-striped" Width="100%" OnPageIndexChanging="gv_PageIndexChanging"
                                    OnRowDataBound="gv_RowDataBound" AllowSorting="true" OnSorting="gv_Sorting">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, Username%>" SortExpression="Username">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblUsername" Text='<%# Eval("Username") %>'/>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, FullName%>" SortExpression="FullName">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblName" Text='<%# Eval("Fullname") %>'/>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, Password_updated%>">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblpasswordupdated" Text='<%# Eval("PasswordStatus") %>'/>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, Role%>">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblRole" Text='<%# Eval("RoleName") %>'/>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, Branch%>">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblBranchName" Text='<%# Eval("BranchName") %>'/>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, ContactNumber%>">
                                            <ItemTemplate>
                                                <%--<asp:TextBox runat="server" ID="txtMobile" CssClass="form-control" Text='<%# Eval("Mobile") %>'></asp:TextBox>--%>
                                                <asp:LinkButton runat="server" ID="btnMobile" Text='<%# Eval("Mobile") %>' OnClick="btnMobile_Click" CommandArgument='<%# Eval("MEMBERID") %>'/>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, Credit_Balance%>" SortExpression="CreditBalance">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblCreditUsed" Text='<%# Eval("CreditUsed") %>'/>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, Credit_Limit%>" SortExpression="CreditLimit">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblCreditLimit" Text='<%# Eval("CreditLimit") %>'/>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, Email%>">
                                            <ItemTemplate>
                                                <%--<asp:TextBox runat="server" ID="txtEmail" CssClass="form-control" Text='<%# Eval("Email") %>'></asp:TextBox>--%>
                                                <asp:Label runat="server" ID="lblEmail" Text='<%# Eval("Email") %>'/>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, Action%>">
                                            <ItemTemplate>
                                                <%--<asp:Button runat="server" ID="btnUpdate" OnClick="btnUpdate_Click"  Text="<%$ Resources:Resource, Update%>" OnClientClick="return confirmUpdate();" CssClass="btn btn-edit"/>--%>
                                                <asp:Button runat="server" ID="btnUpdate" Text="<%$ Resources:Resource, Edit%>" Visible="true" 
                                                    CssClass="btn btn-edit" Style="width:100px;"/>
                                                <asp:Button runat="server" ID="btnSuspend" OnClick="btnSuspend_Click" Text="<%$ Resources:Resource,
                                                    Suspend%>" CssClass="btn btn-danger" OnClientClick="return confirmSuspend();" Style="width:100px;"/>
                                                <asp:Button runat="server" ID="btnReactive" OnClick="btnReactive_Click" Visible="false" 
                                                    Text="<%$ Resources:Resource, Reactive%>" CssClass="btn btn-danger" />
                                                <asp:Label runat="server" Visible="false" ID="Hid_MemberID" Text='<%# Eval("MEMBERID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerTemplate>
                                        <div>
                                            <div class="dataTables_paginate paging_bootstrap">
                                                <ul class="pagination">
                                                    <li>
                                                        <asp:PlaceHolder runat="server" ID="phPreviousPaging"></asp:PlaceHolder>
                                                    </li>

                                                    <li>
                                                        <asp:PlaceHolder runat="server" ID="phPaging"></asp:PlaceHolder>
                                                    </li>

                                                    <li>
                                                        <asp:PlaceHolder runat="server" ID="phNextPaging"></asp:PlaceHolder>
                                                    </li>
                                                </ul>

                                            </div>
                                        </div>
                                    </PagerTemplate>
                                </asp:GridView>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </aside>


</asp:Content>
