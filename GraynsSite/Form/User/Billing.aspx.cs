﻿using HJT.Cls.Helper;
using HJT.Cls.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Controller;
using Synergy.Helper;
using Synergy.Model;
using Synergy.Util;

namespace EDV.Form.User
{
    public partial class Billing : System.Web.UI.Page
    {
        LoginUserModel login_user = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            scriptManager.RegisterPostBackControl(this.btnPay);
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];
            if (!IsPostBack)
            {
                bindControl();
                sqlString.bindControl(gv, getRequestList(null));
            }

            if (gv.Rows.Count > 0)
                gridView.ApplyPaging(gv, -1);
        }

        private void bindControl()
        {
            ddlTrasactionType.Items.Add(new ListItem("Please Select One...", "0"));
            ddlTrasactionType.Items.Add(new ListItem("Request", "1"));
            ddlTrasactionType.Items.Add(new ListItem("Successful Case", "2"));
            ddlTrasactionType.Items.Add(new ListItem("Payment", "3"));
            ddlTrasactionType.Items.Add(new ListItem("Free Request", "4"));

            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            sql.Append("SELECT tucb.CreditUsed AS CreditUsed FROM dbo.tbl_UserCreditBalance tucb WHERE tucb.MemberId = '" + login_user.UserId + "'");
            db.OpenTable(sql.ToString());
            if (db.RecordCount() > 0)
            {
                lblCreditBalance.Text = db.Item("CreditUsed").ToString();
                btnPay.Visible = true;
            }
            else
            {
                lblCreditBalance.Text = "0.00";
            }

            decimal perRequestPrice = 0;
            decimal perSuccessfulcasePrice = 0;

            if (login_user.BranchID != string.Empty)
            {
                sql.Clear();
                sql.Append("SELECT creditLimit,PerRequestPrice,PerSuccessfulCasePrice FROM dbo.tbl_BillingSetting WHERE BranchId = " + login_user.BranchID);


                db.OpenTable(sql.ToString());
                if (db.RecordCount() > 0)
                {
                    lblCreditLimit.Text = db.Item("creditLimit").ToString();
                    perRequestPrice = Convert.ToDecimal(db.Item("PerRequestPrice").ToString());
                    perSuccessfulcasePrice = Convert.ToDecimal(db.Item("PerSuccessfulCasePrice").ToString());
                }
                else
                {
                    sql.Clear();
                    sql.Append("SELECT creditLimit,PerRequestPrice,PerSuccessfulCasePrice FROM dbo.tbl_BillingSetting WHERE BranchId = 0");
                    db.OpenTable(sql.ToString());
                    if (db.RecordCount() > 0)
                    {
                        lblCreditLimit.Text = db.Item("creditLimit").ToString();
                        perRequestPrice = Convert.ToDecimal(db.Item("PerRequestPrice").ToString());
                        perSuccessfulcasePrice = Convert.ToDecimal(db.Item("PerSuccessfulCasePrice").ToString());
                    }
                }
            }
            else
            {
                sql.Clear();
                sql.Append("SELECT creditLimit,PerRequestPrice,PerSuccessfulCasePrice FROM dbo.tbl_BillingSetting WHERE BranchId = 0");
                db.OpenTable(sql.ToString());
                if (db.RecordCount() > 0)
                {
                    lblCreditLimit.Text = db.Item("creditLimit").ToString();
                    perRequestPrice = Convert.ToDecimal(db.Item("PerRequestPrice").ToString());
                    perSuccessfulcasePrice = Convert.ToDecimal(db.Item("PerSuccessfulCasePrice").ToString());
                }
            }

            //PerRequestPrice
            lblRequestPrice.Text = perRequestPrice.ToString();

            int UserFreeCount = 0;
            sql.Clear();
            sql.Append("SELECT FreeRequestCount FROM tbl_UserCreditBalance WHERE MemberID ='" + login_user.UserId + "'");
            db.OpenTable(sql.ToString());
            if (db.RecordCount() > 0)
            {
                UserFreeCount = ConvertHelper.ConvertToInt(db.Item("FreeRequestCount").ToString(), 0);
            }

            int FreeCount = UserFreeCount;

            if (FreeCount > 0)
                lblFreeRequest.Text = FreeCount.ToString(); //((FreeCount * perRequestPrice) + (FreeCount * perSuccessfulcasePrice)).ToString();
            else
                lblFreeRequest.Text = "0";

            //Payment Amount
            sql.Clear();
            sql.AppendFormat(@"select ts.[Text] as '0', ts.ID as '1', ts.TopupAmount as 'Amount', ts.FreeRequestCount
                                from tbl_topupsetting ts with (nolock) 
                                where isDeleted=0
                                order by sort
            ");
            db.OpenTable(sql.ToString());
            if (db.RecordCount() > 0)
            {
                sqlString.bindControl(ddlPayAmount, sql.ToString(), "0", "1", true);
            }

            //sql.Clear();
            //sql.Append("SELECT COUNT(*) + SUM(CASE WHEN FreeRequestCount > 1 THEN 1 ELSE 0 END) as RequestCount FROM tbl_Request WHERE MemberID ='" + login_user.UserId + "'");
            //db.OpenTable(sql.ToString());
            //if (db.RecordCount() > 0)
            //{
            //    int RequestCount = ConvertHelper.ConvertToInt(db.Item("RequestCount").ToString(), 0);
            //    int FreeCreditCount = FreeCount - RequestCount;
            //    if (FreeCreditCount > 0)
            //        lblFreeCredit.Text = FreeCreditCount.ToString(); //((FreeCreditCount * perRequestPrice) + (FreeCreditCount * perSuccessfulcasePrice)).ToString();
            //    else
            //        lblFreeCredit.Text = "0";
            //}
            //else
            //{
            //    if (FreeCount > 0)
            //        lblFreeCredit.Text = FreeCount.ToString(); //((FreeCount * perRequestPrice) + (FreeCount * perSuccessfulcasePrice)).ToString();
            //    else
            //        lblFreeCredit.Text = "0";
            //}
        }

        private string getRequestList(IDictionary<string, string> sortBy)
        {
            StringBuilder sql = new StringBuilder();
            StringBuilder sqlFilter = new StringBuilder();


            string searchFilter = string.Empty;

            sql.AppendFormat(@"SELECT tut.CreatedAt, 
	                                CASE WHEN tut.RequestType LIKE 'Free Request'
		                                THEN tut.RequestType + ' awarded + ' + cast(tut.FreeRequestCount as varchar)
		                            WHEN tut.RequestType LIKE 'Payment'
		                                THEN tut.RequestType + '. PaymentID: ' + ISNULL(tit.PaymentId,'') 
		                            WHEN tut.IsFreeCredit = 1 OR tut.RequestType LIKE 'Free Credit'
		                                THEN tut.RequestType + '. ' + ISNULL(tut.Remark,'')
                                    WHEN tut.RequestType LIKE 'Request'
                                        THEN tut.RequestType + '. ' + 
										    ISNULL('<br />Applicant Name 1: ' +
											    (
											    SELECT a.[Name] FROM tbl_Applicants a
											    WHERE a.RequestID=tut.RequestId AND a.ApplicantType='MA'
											    ),
											    ISNULL('<br />Applicant Name 1: ' +
												    (
												    SELECT a.[Name] FROM tbl_TApplicants a
												    WHERE a.RequestID=tut.RequestId AND a.ApplicantType='MA'
												    )
											    ,'')
										    ) + ' ' + ISNULL('<br />Applicant Name 2: ' +
												    (
												    SELECT a.[Name] FROM tbl_Applicants a
												    WHERE a.RequestID=tut.RequestId AND a.ApplicantType='CA'
												    ),
												    ISNULL('<br />Applicant Name 2: ' +
													    (
													    SELECT a.[Name] FROM tbl_TApplicants a
													    WHERE a.RequestID=tut.RequestId AND a.ApplicantType='CA'
													    )
												    ,'')
											    )
		                                ELSE tut.RequestType + '. ' + ISNULL(tut.Remark,'')
	                                END AS 'Description', tut.RequestId,
                                    CASE WHEN tut.Amount <= 0
		                                THEN '-'+replace(cast(tut.Amount as varchar),'-','')
		                                ELSE '+' + cast(tut.Amount as varchar)
	                                END AS 'Debit',
                                    CASE WHEN tut.Amount > 0
		                                THEN '+' + cast(tut.Amount as varchar)
		                                ELSE ''
	                                END AS 'Credit', 
	                                (select SUM(ut.Amount)-isnull((select sum(b.Amount) from tbl_UserTransactions b
				                    where b.MemberId=tut.MemberId and b.IsFreeRequest=1
				                    and b.CreatedAt between (
					                    select top 1 c.CreatedAt from tbl_UserTransactions c
					                    where c.MemberId=b.MemberId
					                    order by c.CreatedAt asc) and tut.CreatedAt),0)
	                                from tbl_UserTransactions ut 
	                                where ut.MemberId=tut.MemberId 
		                                and (ut.CreatedAt between 
			                                (select top 1 a.CreatedAt from tbl_UserTransactions a
				                                where a.MemberId=ut.MemberId
				                                order by a.CreatedAt asc) and tut.CreatedAt)
		                                and ut.Status=1)
	                                as 'Balance',
                                    isFreeRequest--, Remark 
                                FROM dbo.tbl_UserTransactions tut LEFT JOIN 
                                dbo.tbl_Ipay88Transaction tit ON tut.Id = tit.UserTransactionId
                                WHERE tut.Status = 1 
							    AND tut.MemberId = '{0}'
            ", login_user.UserId);

            if (!string.IsNullOrWhiteSpace(txtFrom.Text) && !string.IsNullOrWhiteSpace(txtTo.Text) && DateTime.TryParse(txtFrom.Text, out var from) && DateTime.TryParse(txtTo.Text, out var to))
                sqlFilter.Append(" AND tut.CreatedAt BETWEEN CONVERT(DATE,'" + secure.RC(txtFrom.Text) + "') AND CONVERT(DATE,'" + secure.RC(txtTo.Text) + "')");

            if (ddlTrasactionType.SelectedIndex != 0)
            {
                string transactionType = "";
                if (ddlTrasactionType.SelectedValue == "1")
                    transactionType = "Request";
                else if (ddlTrasactionType.SelectedValue == "2")
                    transactionType = "Successful Case";
                else if (ddlTrasactionType.SelectedValue == "3")
                    transactionType = "Payment";
                else if (ddlTrasactionType.SelectedValue == "4")
                    transactionType = "Free Request";
                sqlFilter.Append(" AND tut.RequestType ='" + transactionType + "'");
            }

            if (txtRequestId.Text.Trim() != string.Empty)
                sqlFilter.Append(" AND tut.RequestId ='" + secure.RC(txtRequestId.Text.Trim()) + "'");

            if (sqlFilter.ToString().Trim() != string.Empty)
                sql.Append(sqlFilter);

            sql.Append(" order by ");
            //ordering
            if (sortBy != null && sortBy.Any())
            {
                int i = 0;
                foreach (var item in sortBy)
                {
                    string key = item.Key;
                    string value = item.Value;

                    if (i > 0)
                        sql.Append(",");

                    switch (key)
                    {
                        case "CreatedAt":
                            sql.Append(" tut.CreatedAt " + value);
                            break;
                        default:
                            break;
                    }
                    i++;
                }
            }
            else
                sql.Append(" tut.CreatedAt DESC ");

            return sql.ToString();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> sortBy = new Dictionary<string, string>();
            if (ViewState["SortBy"] != null)
            { sortBy = (Dictionary<string, string>)ViewState["SortBy"]; }
            sqlString.bindControl(gv, getRequestList(sortBy));

            if (gv.Rows.Count > 0)
                gridView.ApplyPaging(gv, -1);

            //Page.RegisterStartupScript(this.GetType(), "CallMyFunction", "NewFunction()", true);
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "MyFun1", "NewFunction();", true);
        }

        protected void btnPay_Click(object sender, EventArgs e)
        {
            if (ValidatePayment())
            {
                string IPAY_MERCHANTCODE, IPAY_MERCHANTKEY, IPAY_CURRENCY, IPAY_URL, IPAY_RESPONSEURL, IPAY_BACKENDURL;
                IPAY_MERCHANTCODE = ConfigurationManager.AppSettings["IPAY_MERCHANTCODE"];
                IPAY_MERCHANTKEY = ConfigurationManager.AppSettings["IPAY_MERCHANTKEY"];
                IPAY_CURRENCY = ConfigurationManager.AppSettings["IPAY_CURRENCY"];
                IPAY_URL = ConfigurationManager.AppSettings["IPAY_URL"];
                IPAY_RESPONSEURL = ConfigurationManager.AppSettings["IPAY_RESPONSEURL"];
                IPAY_BACKENDURL = ConfigurationManager.AppSettings["IPAY_BACKENDURL"];
                
                wwdb db = new wwdb();
                string formId = "ePayment";

                decimal amount = 0, additionalCredit = 0;
                int freeCount = 0;
                string sqlGetAmount = string.Format(@"  
                    select ts.TopupAmount as 'Amount', ts.AdditionalCredit as 'Add', 
                        ts.FreeRequestCount as 'FreeCount' 
                    from tbl_TopupSetting ts
                    where isDeleted=0 and ts.ID='{0}'
                ",ddlPayAmount.SelectedValue);
                db.OpenTable(sqlGetAmount);
                if (db.RecordCount() > 0)
                {
                    amount = ConvertHelper.ConvertToDecimal(db.Item("Amount"), 0);
                    additionalCredit = ConvertHelper.ConvertToDecimal(db.Item("Add"), 0);
                    freeCount = ConvertHelper.ConvertToInt(db.Item("FreeCount"), 0);
                }

                string sql1 = 
                    "INSERT INTO tbl_UserTransactions(MemberId, Amount, Status, RequestType, CreatedAt, IsFreeRequest, freeRequestCount) " +
                    "Values('" + login_user.UserId + "', '" + amount + "', 0, 'Payment', GETDATE(), 0, '" + freeCount + "'); " +
                    "SELECT TOP 1 Id FROM tbl_UserTransactions WHERE MemberId = '" + login_user.UserId + "' AND RequestType = 'Payment' " +
                    "ORDER BY dbo.tbl_UserTransactions.Id DESC ";
                var res = db.executeScalar(sql1.ToString());
                int userTransactionId = Convert.ToInt32(res.ToString());
                string refId = userTransactionId.ToString() + "-" + Guid.NewGuid().ToString().Replace("-", "").Substring(0,10);

                //ADDITIONAL CREDIT
                if (additionalCredit > 0)// && amount > 0)
                {
                    string sqlFreeCredit =
                        string.Format(@"
                    INSERT INTO tbl_UserTransactions(MemberId, Amount, Status, RequestType, CreatedAt, IsFreeRequest, freeRequestCount, IsFreeCredit, Remark)
                        Values('{0}', '{1}', 0, 'Free Credit', GETDATE(), 0, 0, 1, 'Reference ID: {2}');
                        ", login_user.UserId, additionalCredit, refId);
                    db.Execute(sqlFreeCredit);
                    if (db.HasError)
                        LogUtil.logError(db.ErrorMessage, sqlFreeCredit);
                }
                //Payment amount with two decimals and thousand symbols.
                string iPayAmount = String.Format("{0:#,#.00}", Convert.ToDecimal(amount));

                var requestString = IPAY_MERCHANTKEY + IPAY_MERCHANTCODE + refId + iPayAmount.Replace(",", "").Replace(".", "") + IPAY_CURRENCY;
                var Signature = StringHelper.GenerateSHA256String(requestString);

                string userName = string.Empty;
                string userEmail = string.Empty;
                string userContact = string.Empty;
                string sqlUserInfo = "SELECT * FROM tbl_memberinfo WHERE MemberID ='" + login_user.UserId + "'";
                db.OpenTable(sqlUserInfo.ToString());
                if (db.RecordCount() > 0)
                {
                    userName = db.Item("Fullname");
                    userEmail = db.Item("Email");
                    userContact = db.Item("Mobile");
                }

                StringBuilder htmlForm = new StringBuilder();
                htmlForm.AppendLine("<html>");
                htmlForm.AppendLine(string.Format("<body onload='document.forms[\"{0}\"].submit()'>", formId));
                htmlForm.AppendLine(string.Format("<form name='{0}' method='POST' action='{1}'>", formId, IPAY_URL));
                htmlForm.AppendLine(string.Format("<input type='hidden' name='MerchantCode' value='{0}' />", IPAY_MERCHANTCODE));
                htmlForm.AppendLine(string.Format("<input type='hidden' name='PaymentId' value='{0}' />", "")); //aaron remove 2
                htmlForm.AppendLine(string.Format("<input type='hidden' name='RefNo' value='{0}' />", refId));
                htmlForm.AppendLine(string.Format("<input type='hidden' name='Amount' value='{0}' />", iPayAmount));
                htmlForm.AppendLine(string.Format("<input type='hidden' name='Currency' value='{0}' />", IPAY_CURRENCY));
                htmlForm.AppendLine(string.Format("<input type='hidden' name='ProdDesc' value='{0}' />", "Synergy Payment")); // change from Whopay Payment
                htmlForm.AppendLine(string.Format("<input type='hidden' name='UserName' value='{0}' />", userName));
                htmlForm.AppendLine(string.Format("<input type='hidden' name='UserEmail' value='{0}' />", userEmail));
                htmlForm.AppendLine(string.Format("<input type='hidden' name='UserContact' value='{0}' />", userContact));
                htmlForm.AppendLine(string.Format("<input type='hidden' name='Remark' value='{0}' />", refId));
                htmlForm.AppendLine(string.Format("<input type='hidden' name='Lang' value='{0}' />", "UTF-8"));
                htmlForm.AppendLine(string.Format("<input type='hidden' name='SignatureType' value='{0}' />", "SHA256"));
                htmlForm.AppendLine(string.Format("<input type='hidden' name='Signature' value='{0}' />", Signature));
                htmlForm.AppendLine(string.Format("<input type='hidden' name='ResponseURL' value='{0}' />", IPAY_RESPONSEURL));
                htmlForm.AppendLine(string.Format("<input type='hidden' name='BackendURL' value='{0}' />", IPAY_BACKENDURL));
                htmlForm.AppendLine("<input type='submit' value='Proceed with Payment' name='Submit'>");
                htmlForm.AppendLine("</form>");
                htmlForm.AppendLine("</body>");
                htmlForm.AppendLine("</html>");

                IPay88Model reqModel = new IPay88Model
                {
                    Amount = iPayAmount,
                    MerchantCode = IPAY_MERCHANTCODE,
                    MerchantKey = IPAY_MERCHANTKEY,
                    RefNo = refId,
                    ProdDesc = "Synergy Payment", // Aaron change this to Synergy
                    UserName = userName,
                    UserEmail = userEmail,
                    UserContact = userContact,
                    Currency = IPAY_CURRENCY,
                    SignatureType = "SHA256",
                    Signature = Signature,
                    ResponseURL = IPAY_RESPONSEURL,
                    BackendURL = IPAY_BACKENDURL,
                };

                var json = new JavaScriptSerializer().Serialize(reqModel);

                string sql2 = "INSERT INTO tbl_Ipay88Transaction(UserTransactionId,Amount,RefNo,RequestData,RequestDate,ResponseStatus) " +
                    "Values(" + userTransactionId + "," + iPayAmount + ",'" + refId + "','" + json + "',GETDATE(),0)";
                db.Execute(sql2.ToString());

                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.Write(htmlForm.ToString());
                HttpContext.Current.Response.End();

            }
        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Dictionary<string, string> sortBy = new Dictionary<string, string>();
            if (ViewState["SortBy"] != null)
            { sortBy = (Dictionary<string, string>)ViewState["SortBy"]; }
            gv.PageIndex = e.NewPageIndex;
            sqlString.bindControl(gv, getRequestList(sortBy));

            if (gv.Rows.Count > 0)
                gridView.ApplyPaging(gv, e.NewPageIndex);

        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string description = DataBinder.Eval(e.Row.DataItem, "Description").ToString();
                Label lblDescription = (Label)e.Row.Cells[1].FindControl("lblDescription");
                TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                lblDescription.Text = textInfo.ToTitleCase(description.ToLower());

                //string isFreeRequest = DataBinder.Eval(e.Row.DataItem, "IsFreeRequest").ToString();
                string amount = DataBinder.Eval(e.Row.DataItem, "Debit").ToString();
                string isFreeRequest = DataBinder.Eval(e.Row.DataItem, "isFreeRequest").ToString();
                //Label lblPaymentStatus = (Label)e.Row.FindControl("lblPaymentStatus");

                Label lblDebitAmount = (Label)e.Row.Cells[3].FindControl("lblDebitAmount");
                lblDebitAmount.Text = amount;
                if (isFreeRequest.ToUpper() == "TRUE")
                {
                    Label lblFree = (Label)e.Row.Cells[3].FindControl("lblFree");
                    lblFree.Text = " FREE";
                    lblDebitAmount.Attributes.Add("style", "text-decoration:line-through;");
                }
                
                string creditAmount = DataBinder.Eval(e.Row.DataItem, "Credit").ToString();
                Label lblCreditAmount = (Label)e.Row.Cells[4].FindControl("lblCreditAmount");
                lblCreditAmount.Text = creditAmount;
            }
        }

        protected void gv_DataBound(object sender, EventArgs e)
        {
        }

        protected bool ValidatePayment()
        {
            StringBuilder errMsg = new StringBuilder();
            StringBuilder sql = new StringBuilder();

            //CUSTOM PAYMENT
            //if (!Validation.isDecimal(txtpayamount.Text))
            //{
            //    errMsg.Append(Resources.resource.OnlyAcceptDecimalNumber + "\\n");
            //}

            //if(Convert.ToDecimal(txtpayamount.Text.Trim()) == 0)
            //{
            //    errMsg.Append(Resources.resource.amountZeroErrMsg + "\\n");
            //}

            //FIXED PAYMENT
            if (ddlPayAmount.SelectedIndex <= 0)
            {
                errMsg.Append(Resources.resource.PleaseSelectOneAmount + "\\n");
            }

            if (errMsg.ToString().Trim() != string.Empty)
            {
                sqlString.displayAlert2(this, errMsg.ToString());
                return false;
            }

            return true;
        }
        
        private string GetGridViewSortDirection(string sortExpression)
        {
            if (ViewState["sortDirection" + sortExpression] == null)
                return null;
            else
                return (string)ViewState["sortDirection" + sortExpression];
        }

        private void SetGridViewSortDirection(string sortExpression, string value)
        {
            ViewState["sortDirection" + sortExpression] = value;
        }

        protected void gv_Sorting(object sender, GridViewSortEventArgs e)
        {
            string expression = e.SortExpression;
            string direction = null;

            //3 state sorting
            //ASC,DESC,null
            if (GetGridViewSortDirection(expression) == "ASC")
            {
                SetGridViewSortDirection(expression, "DESC");
                direction = "DESC";
            }
            else if (GetGridViewSortDirection(expression) == "DESC")
            {
                SetGridViewSortDirection(expression, null);
                direction = null;
            }
            else if (GetGridViewSortDirection(expression) == null)
            {
                SetGridViewSortDirection(expression, "ASC");
                direction = "ASC";
            }

            Dictionary<string, string> sortBy = new Dictionary<string, string>();
            if (direction != null)
                sortBy.Add(e.SortExpression, direction);

            ViewState["SortBy"] = sortBy;

            sqlString.bindControl(gv, getRequestList(sortBy));

            if (gv.Rows.Count > 0)
                gridView.ApplyPaging(gv, -1);
        }

        protected void ddlPayAmount_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlPayAmount.SelectedIndex > 0)
            {
                StringBuilder sql = new StringBuilder();
                wwdb db = new wwdb();

                sql.Clear();
                sql.AppendFormat(@"
                    select ts.TopupAmount as 'Amount', ts.AdditionalCredit as 'Add', 
                        ts.FreeRequestCount as 'FreeCount' 
                    from tbl_TopupSetting ts
                    where isDeleted=0 and ts.ID='{0}'
                ", ddlPayAmount.SelectedValue);
                db.OpenTable(sql.ToString());
                if (db.RecordCount() > 0)
                {
                    int freeCount = ConvertHelper.ConvertToInt(db.Item("FreeCount"), 0);
                    int currentFreeCount = ConvertHelper.ConvertToInt(lblFreeRequest.Text, 0);
                    decimal amount = ConvertHelper.ConvertToDecimal(db.Item("Amount"), 0);
                    decimal additionalCredit = ConvertHelper.ConvertToDecimal(db.Item("Add"), 0);
                    decimal balance = ConvertHelper.ConvertToDecimal(lblCreditBalance.Text, 0);
                    lblFreeRequestPreview.Visible = true;
                    lblCreditBalancePreview.Visible = true;
                    if (freeCount > 0)
                        lblFreeRequestPreview.Text = " + " + freeCount.ToString() + "   (" + (currentFreeCount + freeCount).ToString() + ")";
                    if (amount > 0)
                        lblCreditBalancePreview.Text = " + " + amount.ToString()
                            + (additionalCredit > 0 ? (" + FREE " + additionalCredit.ToString()) : "")
                            + "   (" + (balance + amount + additionalCredit).ToString() + ")";
                }
            }
            else
            {
                lblFreeRequestPreview.Text = "";
                lblCreditBalancePreview.Text = "";
                lblFreeRequestPreview.Visible = false;
                lblCreditBalancePreview.Visible = false;
            }
        }
    }
}