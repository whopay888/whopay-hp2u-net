﻿using HJT.Cls.Helper;
using HJT.Cls.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Controller;
using Synergy.Model;
using Synergy.Util;

namespace EDV.Form.User
{
    public partial class BackEnd_PaymentSuccessful : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                StringBuilder errMsg = new StringBuilder();

                string IPAY_MERCHANTCODE, IPAY_MERCHANTKEY, IPAY_CURRENCY, IPAY_URL, IPAY_RESPONSEURL, IPAY_BACKENDURL;
                IPAY_MERCHANTCODE = ConfigurationManager.AppSettings["IPAY_MERCHANTCODE"];
                IPAY_MERCHANTKEY = ConfigurationManager.AppSettings["IPAY_MERCHANTKEY"];
                IPAY_CURRENCY = ConfigurationManager.AppSettings["IPAY_CURRENCY"];
                IPAY_URL = ConfigurationManager.AppSettings["IPAY_URL"];
                IPAY_RESPONSEURL = ConfigurationManager.AppSettings["IPAY_RESPONSEURL"];
                IPAY_BACKENDURL = ConfigurationManager.AppSettings["IPAY_BACKENDURL"];

                var ResponseMerchantCode = Request.Form["MerchantCode"];
                var ResponsePaymentId = Request.Form["PaymentId"];
                var ResponseRefId = Request.Form["RefNo"];
                var ResponseAmount = Request.Form["Amount"];
                var ResponseCurrency = Request.Form["Currency"];
                var ResponseRemark = Request.Form["Remark"];
                var ResponseTransId = Request.Form["TransId"];
                var ResponseAuthCode = Request.Form["AuthCode"];
                var ResponseStatus = Request.Form["Status"];
                var ResponseErrDesc = Request.Form["ErrDesc"];
                var ResponseSignature = Request.Form["Signature"];
                var ResponseCCName = Request.Form["CCName"];
                var ResponseCCNo = Request.Form["CCNo"];
                var Responsesbankname = Request.Form["S_bankname"];
                var Responsescountry = Request.Form["S_country"];

                IPay88Model reqModel = new IPay88Model
                {
                    MerchantCode = ResponseMerchantCode,
                    PaymentId = ResponsePaymentId,
                    RefNo = ResponseRefId,
                    Amount = ResponseAmount,
                    Currency = ResponseCurrency,
                    Remark = ResponseRemark,
                    TransId = ResponseTransId,
                    AuthCode = ResponseAuthCode,
                    Status = ResponseStatus,
                    ErrDesc = ResponseErrDesc,
                    Signature = ResponseSignature,
                    CCName = ResponseCCName,
                    CCNo = ResponseCCNo,
                    SBankname = Responsesbankname,
                    SCountry = Responsescountry,
                };

                var json = new JavaScriptSerializer().Serialize(reqModel);
                LogUtil.logAction(json, "ipay88 backend response");

                if (reqModel.Status == "1")
                {
                    wwdb db = new wwdb();
                    string sql1 = "SELECT UserTransactionId FROM tbl_Ipay88Transaction WHERE RefNo = '" + ResponseRefId + "' AND (ResponseStatus = 0 OR ResponseStatus IS NULL)";
                    db.OpenTable(sql1.ToString());
                    if (db.RecordCount() > 0)
                    {
                        int transactionId = int.Parse(db.Item("UserTransactionId").ToString());
                        string sql2 = "SELECT * FROM tbl_UserTransactions WHERE Id = " + transactionId;
                        db.OpenTable(sql2.ToString());
                        if (db.RecordCount() > 0)
                        {
                            var Amount = Convert.ToDecimal(db.Item("Amount").ToString());

                            var requestString = IPAY_MERCHANTKEY + IPAY_MERCHANTCODE + ResponsePaymentId + ResponseRefId.ToString() + Amount.ToString().Trim().Replace(".", "") + IPAY_CURRENCY + ResponseStatus;
                            var Signature = StringHelper.GenerateSHA256String(requestString);

                            if (ResponseSignature == Signature)
                            {
                                var MemberId = db.Item("MemberId").ToString();

                                string sql3 = "UPDATE tbl_Ipay88Transaction SET ResponseData = '" + json + "',  PaymentId = '" + ResponsePaymentId + "', ResponseDate = GETDATE(), ResponseStatus = 1 WHERE RefNo = '" + ResponseRefId + "'; " +
                                              "UPDATE tbl_UserTransactions SET Status = 1 WHERE Id = " + transactionId + ";";
                                //"UPDATE tbl_UserCreditBalance SET CreditUsed = CreditUsed + " + Amount + " WHERE MemberId = '" + MemeberId + "'";

                                string sql4 =
                                    string.Format(@"
                                            UPDATE tbl_UserCreditBalance SET CreditUsed = 
	                                            (select SUM(Amount) from tbl_UserTransactions where MemberId='{0}' and IsFreeRequest = 0 and status = 1), 
		                                            FreeRequestCount = (select ISNULL(SUM(ISNULL(FreeRequestCount,0)),0) FROM tbl_UserTransactions 
								                                            WHERE MemberID = '{0}' and status = 1)
		                                            WHERE MemberId = '{0}';
                                    ", MemberId);
                                sql3 += sql4;

                                db.executeScalar(sql3.ToString());

                                Response.Write("RECEIVEOK");
                            }
                        }
                    }

                }
            }
        }
    }
}