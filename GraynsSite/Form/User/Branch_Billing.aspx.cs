﻿using HJT.Cls.Controller;
using HJT.Cls.Helper;
using HJT.Cls.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Helper;
using Synergy.Model;
using Synergy.Util;

namespace HJT.Form.User
{
    public partial class Branch_Billing : System.Web.UI.Page
    {
        LoginUserModel login_user = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];
            if (!IsPostBack)
            {
                checkAccessControl();
                bindControl();
            }
        }
        private void checkAccessControl()
        {
            if (login_user.isAccountManager() || login_user.isStaffAdmin())
            {
                gv.Visible = true;
                btnSubmit.Visible = true;
            }
            else
            {
                gv.Visible = false;
                btnSubmit.Visible = false;
            }
        }

        private void bindControl()
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            //Payment Amount
            sql.Clear();
            sql.AppendFormat(@"select ts.[Text] as '0', ts.ID as '1', ts.TopupAmount as 'Amount', ts.FreeRequestCount
                                from tbl_topupsetting ts with (nolock) 
                                where isDeleted=0
                                order by sort
            ");
            db.OpenTable(sql.ToString());
            if (db.RecordCount() > 0)
                sqlString.bindControl(ddlPayAmount, sql.ToString(), "0", "1", true);

            sqlString.bindControl(ddlFullname, bindDdlFullnameSQL(), "0", "1", true);
            ddlFullName_SelectedIndexChanged(null, null);
        }
        private string bindDdlFullnameSQL(string name = "")
        {
            StringBuilder sql = new StringBuilder();

            sql.Clear();
            if (!login_user.isStaffAdmin())
                sql.AppendFormat(@"
                    SELECT mi.FullName + ' (' + mi.Username + ')' AS '0', 
                        mi.MemberId AS '1'
                    FROM tbl_MemberInfo mi with (nolock) 
                    inner join tbl_Login l with (nolock) on mi.MemberId = l.login_id
                    inner join (select b.BranchID 
                                from tbl_Branch b with (nolock)
			                    inner join tbl_Login l with (nolock) on l.BranchID = b.BranchID
			                    where l.login_id = '{0}' and b.IsDeleted = 0)a on l.BranchID = a.BranchID
                    WHERE mi.Status = 'A' {1}
                    Order By mi.FullName
                ", login_user.UserId
                , string.IsNullOrEmpty(name) ? "" : string.Format("and mi.Fullname LIKE '%{0}%'", name));
            else
                sql.AppendFormat(@"
                    SELECT mi.FullName + ' (' + mi.Username + ')' AS '0', 
                        mi.MemberId AS '1' 
                    FROM tbl_MemberInfo mi with (nolock) 
                    WHERE mi.Status = 'A' {0}
                    Order By mi.FullName
                ", string.IsNullOrEmpty(name) ? "" : string.Format(@"and mi.Fullname LIKE '%{0}%'", name));

            return sql.ToString();
        }

        private string getAddCreditHistory()
        {
            StringBuilder sql = new StringBuilder();

            sql.Clear();
            if (!login_user.isStaffAdmin())
                sql.AppendFormat(@"
                    SELECT mi.Fullname, 
	                    case when ut.Amount > 0 
		                    then '+' + cast(ut.Amount as varchar)
	                    else
		                    cast(ut.Amount as varchar)
	                    end AS 'Amount', ut.[CreatedAt], 
	                    case when ut.[CreatedBy] is null or ut.CreatedBy like ''
		                    then '-'
		                    else ut.CreatedBy
	                    end as 'CreatedBy', 
	                    case when ut.[Remark] is null or ut.Remark like ''
		                    then '-'
		                    else ut.Remark
	                    end as 'Remark'
                    FROM [tbl_UserTransactions] ut with (nolock) inner join
                    tbl_MemberInfo mi with (nolock) on mi.MemberId = ut.MemberId
                    inner join tbl_Login l with (nolock) on l.login_id = mi.MemberId
                    inner join (select b.BranchID 
			                    from tbl_Branch b with (nolock)
			                    inner join tbl_Login l with (nolock) on b.BranchID = l.BranchID
			                    where l.login_id = '{0}' and b.IsDeleted = 0)a on a.BranchID = l.BranchID
                    where ut.RequestType like '%Payment%'
                    and ut.RequestId is null and ut.Status = 1
                    order by CreatedAt desc
                ", login_user.UserId);
            else
                sql.AppendFormat(@"
                    SELECT mi.Fullname, 
	                    case when ut.Amount > 0 
		                    then '+' + cast(ut.Amount as varchar)
	                    else
		                    cast(ut.Amount as varchar)
	                    end AS 'Amount', ut.[CreatedAt], 
	                    case when ut.[CreatedBy] is null or ut.CreatedBy like ''
		                    then '-'
		                    else ut.CreatedBy
	                    end as 'CreatedBy', 
	                    case when ut.[Remark] is null or ut.Remark like ''
		                    then '-'
		                    else ut.Remark
	                    end as 'Remark'
                    FROM [tbl_UserTransactions] ut with (nolock) inner join
                    tbl_MemberInfo mi with (nolock) on mi.MemberId = ut.MemberId
                    where ut.RequestType like '%Payment%'
                    and ut.RequestId is null and ut.Status=1
                    order by CreatedAt desc; 
                ");

            return sql.ToString();
        }

        private bool validation()
        {
            StringBuilder errorMessage = new StringBuilder();

            if (ddlFullname.SelectedIndex <= 0)
                errorMessage.Append(Resources.resource.memberNameEmptyErrorMsg + "\\n");

            if (ddlPayAmount.SelectedIndex <= 0)
                errorMessage.Append(Resources.resource.PleaseSelectOneAmount + "\\n");

            if (errorMessage.ToString().Trim() != string.Empty)
            {
                sqlString.displayAlert(this, errorMessage.ToString());
                return false;
            }
            else
                return true;
        }

        private void clearControl()
        {
            lblBranchName.Text = Resources.resource.Please_Select_Name;
            lblContactNumber.Text = string.Empty;
            ddlPayAmount.SelectedIndex = 0;
            txtRemark.Text = string.Empty;
            lblBalance.Text = string.Empty;
            lblFreeRequest.Text = string.Empty;
            ddlFullname.SelectedIndex = 0;
            //bindControl();
            sqlString.bindControl(gv, getAddCreditHistory());
        }

        protected void ddlFullName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFullname.SelectedIndex != 0)
                bindMember(ddlFullname.SelectedItem.Value.ToString());
            else
                clearControl();
        }
        private void bindMember(string memberId)
        {
            wwdb db = new wwdb();
            DataTable dt = db.getDataTable(string.Format(@"
                        SELECT tmi.MemberId, tmi.DisplayName, tmi.Username, tmi.Fullname,  
	                        tmi.Mobile, tmi.Email, tl.BranchID, tb.BranchName,
	                        ucb.CreditUsed, ucb.FreeRequestCount
                        FROM dbo.tbl_MemberInfo tmi INNER JOIN 
                        dbo.tbl_Login tl ON tmi.MemberId = tl.login_id LEFT JOIN 
                        tbl_Branch tb ON tl.BranchID = tb.BranchId left join
                        tbl_UserCreditBalance ucb on ucb.MemberId = tl.login_id
                        WHERE tmi.MemberId ='{0}'
            ", memberId));
            if (dt.Rows.Count > 0)
            {
                lblBranchName.Text = dt.Rows[0]["BranchName"].ToString();
                lblContactNumber.Text = dt.Rows[0]["Mobile"].ToString();
                lblBalance.Text = string.IsNullOrEmpty(dt.Rows[0]["CreditUsed"].ToString()) ?
                    "0" : dt.Rows[0]["CreditUsed"].ToString();
                lblFreeRequest.Text = string.IsNullOrEmpty(dt.Rows[0]["FreeRequestCount"].ToString()) ?
                    "0" : dt.Rows[0]["FreeRequestCount"].ToString();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (validation())
            {
                string IPAY_MERCHANTCODE, IPAY_MERCHANTKEY, IPAY_CURRENCY, IPAY_URL, IPAY_RESPONSEURL, IPAY_BACKENDURL;
                IPAY_MERCHANTCODE = ConfigurationManager.AppSettings["IPAY_MERCHANTCODE"];
                IPAY_MERCHANTKEY = ConfigurationManager.AppSettings["IPAY_MERCHANTKEY"];
                IPAY_CURRENCY = ConfigurationManager.AppSettings["IPAY_CURRENCY"];
                IPAY_URL = ConfigurationManager.AppSettings["IPAY_URL"];
                IPAY_RESPONSEURL = ConfigurationManager.AppSettings["IPAY_RESPONSEURL"];
                IPAY_BACKENDURL = ConfigurationManager.AppSettings["IPAY_BACKENDURL"];

                wwdb db = new wwdb();
                string formId = "ePayment";
                string userID = ddlFullname.SelectedValue;
                decimal amount = 0, additionalCredit = 0;
                int freeCount = 0;
                string sqlGetAmount = string.Format(@"  
                    select ts.TopupAmount as 'Amount', ts.AdditionalCredit as 'Add', 
                        ts.FreeRequestCount as 'FreeCount' 
                    from tbl_TopupSetting ts
                    where isDeleted=0 and ts.ID='{0}'
                ", ddlPayAmount.SelectedValue);
                db.OpenTable(sqlGetAmount);
                if (db.RecordCount() > 0)
                {
                    amount = ConvertHelper.ConvertToDecimal(db.Item("Amount"), 0);
                    additionalCredit = ConvertHelper.ConvertToDecimal(db.Item("Add"), 0);
                    freeCount = ConvertHelper.ConvertToInt(db.Item("FreeCount"), 0);
                }

                string sql1 =
                    "INSERT INTO tbl_UserTransactions(MemberId, Amount, Status, RequestType, CreatedAt, IsFreeRequest, freeRequestCount, CreatedBy) " +
                    "Values('" + userID + "', '" + amount + "', 0, 'Payment', GETDATE(), 0, '" + freeCount + "', '" + login_user.UserId + "'); " +
                    "SELECT TOP 1 Id FROM tbl_UserTransactions WHERE MemberId = '" + userID + "' AND RequestType = 'Payment' " +
                    "ORDER BY dbo.tbl_UserTransactions.Id DESC ";
                var res = db.executeScalar(sql1.ToString());
                int userTransactionId = Convert.ToInt32(res.ToString());
                string refId = userTransactionId.ToString() + "-" + Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10);

                //ADDITIONAL CREDIT
                if (additionalCredit > 0)// && amount > 0)
                {
                    string sqlFreeCredit =
                        string.Format(@"
                    INSERT INTO tbl_UserTransactions(MemberId, Amount, Status, RequestType, CreatedAt, IsFreeRequest, freeRequestCount, IsFreeCredit, Remark, CreatedBy)
                        Values('{0}', '{1}', 0, 'Free Credit', GETDATE(), 0, 0, 1, 'Reference ID: {2}', '{3}');
                        ", userID, additionalCredit, refId, login_user.UserId);
                    db.Execute(sqlFreeCredit);
                    if (db.HasError)
                        LogUtil.logError(db.ErrorMessage, sqlFreeCredit);
                }
                //Payment amount with two decimals and thousand symbols.
                string iPayAmount = String.Format("{0:#,#.00}", Convert.ToDecimal(amount));

                var requestString = IPAY_MERCHANTKEY + IPAY_MERCHANTCODE + refId + iPayAmount.Replace(",", "").Replace(".", "") + IPAY_CURRENCY;
                var Signature = StringHelper.GenerateSHA256String(requestString);

                string userName = string.Empty;
                string userEmail = string.Empty;
                string userContact = string.Empty;
                string sqlUserInfo = "SELECT * FROM tbl_memberinfo WHERE MemberID ='" + userID + "'";
                db.OpenTable(sqlUserInfo.ToString());
                if (db.RecordCount() > 0)
                {
                    userName = db.Item("Fullname");
                    userEmail = db.Item("Email");
                    userContact = db.Item("Mobile");
                }

                StringBuilder htmlForm = new StringBuilder();
                htmlForm.AppendLine("<html>");
                htmlForm.AppendLine(string.Format("<body onload='document.forms[\"{0}\"].submit()'>", formId));
                htmlForm.AppendLine(string.Format("<form name='{0}' method='POST' action='{1}'>", formId, IPAY_URL));
                htmlForm.AppendLine(string.Format("<input type='hidden' name='MerchantCode' value='{0}' />", IPAY_MERCHANTCODE));
                htmlForm.AppendLine(string.Format("<input type='hidden' name='PaymentId' value='{0}' />", "2"));
                htmlForm.AppendLine(string.Format("<input type='hidden' name='RefNo' value='{0}' />", refId));
                htmlForm.AppendLine(string.Format("<input type='hidden' name='Amount' value='{0}' />", iPayAmount));
                htmlForm.AppendLine(string.Format("<input type='hidden' name='Currency' value='{0}' />", IPAY_CURRENCY));
                htmlForm.AppendLine(string.Format("<input type='hidden' name='ProdDesc' value='{0}' />", "WhoPay Payment"));
                htmlForm.AppendLine(string.Format("<input type='hidden' name='UserName' value='{0}' />", userName));
                htmlForm.AppendLine(string.Format("<input type='hidden' name='UserEmail' value='{0}' />", userEmail));
                htmlForm.AppendLine(string.Format("<input type='hidden' name='UserContact' value='{0}' />", userContact));
                htmlForm.AppendLine(string.Format("<input type='hidden' name='Remark' value='{0}' />", refId));
                htmlForm.AppendLine(string.Format("<input type='hidden' name='Lang' value='{0}' />", "UTF-8"));
                htmlForm.AppendLine(string.Format("<input type='hidden' name='SignatureType' value='{0}' />", "SHA256"));
                htmlForm.AppendLine(string.Format("<input type='hidden' name='Signature' value='{0}' />", Signature));
                htmlForm.AppendLine(string.Format("<input type='hidden' name='ResponseURL' value='{0}' />", IPAY_RESPONSEURL));
                htmlForm.AppendLine(string.Format("<input type='hidden' name='BackendURL' value='{0}' />", IPAY_BACKENDURL));
                htmlForm.AppendLine("<input type='submit' value='Proceed with Payment' name='Submit'>");
                htmlForm.AppendLine("</form>");
                htmlForm.AppendLine("</body>");
                htmlForm.AppendLine("</html>");

                IPay88Model reqModel = new IPay88Model
                {
                    Amount = iPayAmount,
                    MerchantCode = IPAY_MERCHANTCODE,
                    MerchantKey = IPAY_MERCHANTKEY,
                    RefNo = refId,
                    ProdDesc = "WhoPay Payment",
                    UserName = userName,
                    UserEmail = userEmail,
                    UserContact = userContact,
                    Currency = IPAY_CURRENCY,
                    SignatureType = "SHA256",
                    Signature = Signature,
                    ResponseURL = IPAY_RESPONSEURL,
                    BackendURL = IPAY_BACKENDURL,
                };

                var json = new JavaScriptSerializer().Serialize(reqModel);

                string sql2 = "INSERT INTO tbl_Ipay88Transaction(UserTransactionId,Amount,RefNo,RequestData,RequestDate,ResponseStatus) " +
                    "Values(" + userTransactionId + "," + iPayAmount + ",'" + refId + "','" + json + "',GETDATE(),0)";
                db.Execute(sql2.ToString());

                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.Write(htmlForm.ToString());
                HttpContext.Current.Response.End();
            }
        }

        protected void btnSearchName_Click(object sender, EventArgs e)
        {
            ReportController rc = new ReportController();
            rc.removeInvalidChar("", txtSearchName);

            sqlString.bindControl(ddlFullname, bindDdlFullnameSQL(txtSearchName.Text), "0", "1", true);
        }

        protected void ddlPayAmount_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlPayAmount.SelectedIndex > 0)
            {
                StringBuilder sql = new StringBuilder();
                wwdb db = new wwdb();

                sql.Clear();
                sql.AppendFormat(@"
                    select ts.TopupAmount as 'Amount', ts.AdditionalCredit as 'Add', 
                        ts.FreeRequestCount as 'FreeCount' 
                    from tbl_TopupSetting ts
                    where isDeleted=0 and ts.ID='{0}'
                ", ddlPayAmount.SelectedValue);
                db.OpenTable(sql.ToString());
                if (db.RecordCount() > 0)
                {
                    int freeCount = ConvertHelper.ConvertToInt(db.Item("FreeCount"), 0);
                    int currentFreeCount = ConvertHelper.ConvertToInt(lblFreeRequest.Text, 0);
                    decimal amount = ConvertHelper.ConvertToDecimal(db.Item("Amount"), 0);
                    decimal additionalCredit = ConvertHelper.ConvertToDecimal(db.Item("Add"), 0);
                    decimal balance = ConvertHelper.ConvertToDecimal(lblBalance.Text, 0);
                    lblFreeRequestPreview.Visible = true;
                    lblCreditBalancePreview.Visible = true;
                    if (freeCount > 0)
                        lblFreeRequestPreview.Text = " + " + freeCount.ToString() + "   (" + (currentFreeCount + freeCount).ToString() + ")";
                    if (amount > 0)
                        lblCreditBalancePreview.Text = " + " + amount.ToString()
                            + (additionalCredit > 0 ? (" + FREE " + additionalCredit.ToString()) : "")
                            + "   (" + (balance + amount + additionalCredit).ToString() + ")";
                }
            }
            else
            {
                lblFreeRequestPreview.Text = "";
                lblCreditBalancePreview.Text = "";
                lblFreeRequestPreview.Visible = false;
                lblCreditBalancePreview.Visible = false;
            }
        }
    }
}