﻿using HJT.Cls.Helper;
using HJT.Cls.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Controller;
using Synergy.Model;
using Synergy.Util;

namespace EDV.Form.User
{
    public partial class PaymentConfirmation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                StringBuilder errMsg = new StringBuilder();

                string IPAY_MERCHANTCODE, IPAY_MERCHANTKEY, IPAY_CURRENCY, IPAY_URL, IPAY_RESPONSEURL, IPAY_BACKENDURL;
                IPAY_MERCHANTCODE = ConfigurationManager.AppSettings["IPAY_MERCHANTCODE"];
                IPAY_MERCHANTKEY = ConfigurationManager.AppSettings["IPAY_MERCHANTKEY"];
                IPAY_CURRENCY = ConfigurationManager.AppSettings["IPAY_CURRENCY"];
                IPAY_URL = ConfigurationManager.AppSettings["IPAY_URL"];
                IPAY_RESPONSEURL = ConfigurationManager.AppSettings["IPAY_RESPONSEURL"];
                IPAY_BACKENDURL = ConfigurationManager.AppSettings["IPAY_BACKENDURL"];

                var ResponseMerchantCode = Request.Form["MerchantCode"];
                var ResponsePaymentId = Request.Form["PaymentId"];
                var ResponseRefId = Request.Form["RefNo"];
                var ResponseAmount = Request.Form["Amount"];
                var ResponseCurrency = Request.Form["Currency"];
                var ResponseRemark = Request.Form["Remark"];
                var ResponseTransId = Request.Form["TransId"];
                var ResponseAuthCode = Request.Form["AuthCode"];
                var ResponseStatus = Request.Form["Status"];
                var ResponseErrDesc = Request.Form["ErrDesc"];
                var ResponseSignature = Request.Form["Signature"];
                var ResponseCCName = Request.Form["CCName"];
                var ResponseCCNo = Request.Form["CCNo"];
                var Responsesbankname = Request.Form["S_bankname"];
                var Responsescountry = Request.Form["S_country"];

                IPay88Model reqModel = new IPay88Model
                {
                    MerchantCode = ResponseMerchantCode,
                    PaymentId = ResponsePaymentId,
                    RefNo = ResponseRefId,
                    Amount = ResponseAmount,
                    Currency = ResponseCurrency,
                    Remark = ResponseRemark,
                    TransId = ResponseTransId,
                    AuthCode = ResponseAuthCode,
                    Status = ResponseStatus,
                    ErrDesc = ResponseErrDesc,
                    Signature = ResponseSignature,
                    CCName = ResponseCCName,
                    CCNo = ResponseCCNo,
                    SBankname = Responsesbankname,
                    SCountry = Responsescountry,
                };

                var json = new JavaScriptSerializer().Serialize(reqModel);
                LogUtil.logAction(json, "ipay88 response");

                if (reqModel.Status == null)
                {
                    Response.Redirect("/Form/User/Billing.aspx");
                }
                else if (reqModel.Status == "0")
                {
                    divSuccess.Visible = false;
                    divError.Visible = true;
                    errorMsg.Text = reqModel.ErrDesc;
                }
                else
                {
                    //AND (ResponseStatus = 0 OR ResponseStatus IS NULL)
                    wwdb db = new wwdb();
                    string sql1 = "SELECT UserTransactionId, ResponseStatus FROM tbl_Ipay88Transaction WHERE RefNo = '" + ResponseRefId + "'";
                    db.OpenTable(sql1.ToString());
                    if (db.RecordCount() > 0)
                    {
                        int transactionId = int.Parse(db.Item("UserTransactionId").ToString());
                        string responseStatus = db.Item("ResponseStatus").ToString();
                        if (responseStatus != null && responseStatus.Equals("1"))
                        {
                            divSuccess.Visible = true;
                            divError.Visible = false;
                        }
                        else
                        {
                            string sql2 = "SELECT * FROM tbl_UserTransactions WHERE Id = " + transactionId;
                            db.OpenTable(sql2.ToString());
                            if (db.RecordCount() > 0)
                            {
                                var Amount = Convert.ToDecimal(db.Item("Amount").ToString());

                                var requestString = IPAY_MERCHANTKEY + IPAY_MERCHANTCODE + ResponsePaymentId + ResponseRefId.ToString() + Amount.ToString().Trim().Replace(".", "") + IPAY_CURRENCY + ResponseStatus;
                                var Signature = StringHelper.GenerateSHA256String(requestString);

                                if (ResponseSignature == Signature)
                                {
                                    var MemberId = db.Item("MemberId").ToString();

                                    string sql3 = "UPDATE tbl_Ipay88Transaction SET ResponseData = '" + json + "', PaymentId = '" + ResponsePaymentId + "',  ResponseDate = GETDATE(), ResponseStatus = 1 WHERE RefNo = '" + ResponseRefId + "'; " +
                                                  "UPDATE tbl_UserTransactions SET Status = 1 WHERE Id = " + transactionId + ";";

                                    string sql4 =
                                        string.Format(@"
                                            UPDATE tbl_UserCreditBalance SET CreditUsed = 
	                                            (select SUM(Amount) from tbl_UserTransactions where MemberId='{0}' and IsFreeRequest = 0 and status = 1), 
		                                            FreeRequestCount = (select ISNULL(SUM(ISNULL(FreeRequestCount,0)),0) FROM tbl_UserTransactions 
								                                            WHERE MemberID = '{0}' and status = 1)
		                                            WHERE MemberId = '{0}';
                                        ", MemberId);
                                    sql3 += sql4;

                                    //"UPDATE tbl_UserCreditBalance SET CreditUsed = CreditUsed + " + Amount + " WHERE MemberId = '" + MemberId + "'";
                                    db.executeScalar(sql3.ToString());
                                    
                                    //CHECK FREE CREDIT/ADDITIONAL CREDIT
                                    wwdb dbFreeCredit = new wwdb();
                                    string sqlGetFreeCreditID =
                                        string.Format(@"
                                            SELECT TOP 1 * FROM tbl_UserTransactions WHERE Remark LIKE '%{0}%' 
                                                AND Status = 0 AND RequestType = 'Free Credit'
                                                AND IsFreeCredit = 1 AND MemberId = '{1}'
                                                ORDER BY ID DESC
                                        ", ResponseRefId, MemberId);
                                    dbFreeCredit.OpenTable(sqlGetFreeCreditID);
                                    if (dbFreeCredit.RecordCount() > 0) //has free credit
                                    {
                                        string sqlValidateFreeCredit =
                                            string.Format(@"
                                                UPDATE tbl_UserTransactions SET Status = 1 WHERE Id = '{0}';
                                                UPDATE tbl_UserCreditBalance SET CreditUsed = CreditUsed + '{1}' WHERE MemberId = '{2}';
                                            ", dbFreeCredit.Item("ID").Trim()
                                            , dbFreeCredit.Item("Amount").Replace(",", "").Trim()
                                            , MemberId);

                                        dbFreeCredit.Execute(sqlValidateFreeCredit);
                                        if (dbFreeCredit.HasError)
                                            LogUtil.logError(dbFreeCredit.ErrorMessage, sqlValidateFreeCredit);
                                    }

                                    divSuccess.Visible = true;
                                    divError.Visible = false;
                                }
                                else
                                {
                                    divSuccess.Visible = false;
                                    divError.Visible = true;
                                    errorMsg.Text = "Invalid signature";
                                }

                            }
                            else
                            {
                                divSuccess.Visible = false;
                                divError.Visible = true;
                                errorMsg.Text = "Transaction doesn't exists";
                            }
                        }
                    }
                    else
                    {
                        divSuccess.Visible = false;
                        divError.Visible = true;
                        errorMsg.Text = "RefId doesn't exists";
                    }
                }
            }
        }

        protected void btnRedirect_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Form/User/Billing.aspx");
        }
    }
}