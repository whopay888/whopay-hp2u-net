﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Global1.Master" CodeBehind="Billing.aspx.cs" Inherits="EDV.Form.User.Billing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function confirmSubmit() {
            return confirm('<%= GetGlobalResourceObject("resource" , "ConfirmToRegister") %>');
        }

        $(window).load(function () {
            var x = document.getElementById("filtercontent");
            var lblfilter = document.getElementById("lblfilter");
            if (x.style.display === "none") {
                x.style.display = "block";
                lblfilter.innerHTML = "Hide Filters"
            } else {
                x.style.display = "none";
                lblfilter.innerHTML = "Show Filters"
            }
        });

        function myFunction() {
            var x = document.getElementById("filtercontent");
            var lblfilter = document.getElementById("lblfilter");
            if (x.style.display === "none") {
                x.style.display = "block";
                lblfilter.innerHTML = "Hide Filters"
            } else {
                x.style.display = "none";
                lblfilter.innerHTML = "Show Filters"
            }
        }

        function NewFunction() {
            var lblfilter = document.getElementById("lblfilter");
            lblfilter.innerHTML = "Hide Filters"
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <aside class="right-side">
        <section class="content-header">
            <h1>
                <span class="icon fa fa-fw fa-book" aria-hidden="true"></span>
                <asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><span runat="server" id="spHeader"><%= GetGlobalResourceObject("resource", "Billing_Information")%></span></asp:Label>
            </h1>
            <asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
        </section>
        <section class="content" style="margin-top: 10px">
            <div class="col-md-12">
                <div runat="server">
                    <div id="filter">
                        <div class="box">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr style="background: #dec18c;">
                                        <td colspan="2">
                                            <h4><strong><%= GetGlobalResourceObject("resource", "Billing_Information")%></strong></h4>
                                        </td>
                                    </tr>
                                    <tr class="form-group" runat="server" id="tr1">
                                        <td style="width: 35%; border-left: 0px!important;">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "Credit_Limit")%></label>
                                        </td>
                                        <td style="width: 50%;" class="controls">
                                            <asp:Label ID="lblCreditLimit" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr class="form-group" runat="server" id="tr2">
                                        <td style="width: 35%; border-left: 0px!important;">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "FreeUnitCredit")%></label>
                                        </td>
                                        <td style="width: 50%;" class="controls">
                                            <asp:Label ID="lblFreeRequest" runat="server"></asp:Label>
                                            <asp:Label ID="lblFreeRequestPreview" runat="server" Font-Size="Small" ForeColor="LightGreen" Font-Bold="true" Visible="false"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr class="form-group" runat="server">
                                        <td style="width: 35%; border-left: 0px!important;">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "Balance")%></label>
                                        </td>
                                        <td style="width: 50%;" class="controls">
                                            <asp:Label ID="lblCreditBalance" runat="server"></asp:Label>
                                            <asp:Label ID="lblCreditBalancePreview" runat="server" Font-Size="Small" ForeColor="LightGreen" Font-Bold="true" Visible="false"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr class="form-group" runat="server">
                                        <td style="width: 35%; border-left: 0px!important;">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "Per_Request_Price")%></label>
                                        </td>
                                        <td style="width: 50%;" class="controls">
                                            <asp:Label ID="lblRequestPrice" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr class="form-group" runat="server" id="tr3">
                                        <td style="width: 35%; border-left: 0px!important;">
                                            <label class="control-label"><%= GetGlobalResourceObject("resource", "Free_Credit_Amount_Pay")%></label>
                                        </td>
                                        <td style="width: 50%;" class="controls">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList runat="server" Font-Size="Medium" ID="ddlPayAmount" OnSelectedIndexChanged="ddlPayAmount_SelectedIndexChanged"
                                                             AutoPostBack="true"/>
                                                        <%--<asp:TextBox runat="server" ID="txtpayamount" CssClass="form-control"></asp:TextBox>--%>
                                                    </td>
                                                    <td style="width: 70%;">
                                                        <asp:Button runat="server" ID="btnPay" Visible="true" CssClass="btn btn-edit" OnClick="btnPay_Click" 
                                                            Text="<%$ Resources:Resource, Pay_Now%>" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div>
                            <a href="#0" id="showfilter" onclick="myFunction()">
                                <label id="lblfilter" class="btn btn-searh" style="text-transform:capitalize !important;" >Show Filters</label></a>
                            <div id="filtercontent">
                                <div>
                                    <h4><asp:Label ID="panel" runat="server" Text="<%$ Resources:Resource, Selection_Option%>"></asp:Label></h4>
                                    <asp:Panel runat="server" DefaultButton="btnSearch">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label class="control-label"><%= GetGlobalResourceObject("resource", "From")%></label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtFrom" runat="server" CssClass="form-control" TextMode="Date"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label"><%= GetGlobalResourceObject("resource", "To")%></label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtTo" runat="server" CssClass="form-control" TextMode="Date"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label"><%= GetGlobalResourceObject("resource", "Request_ID")%></label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtRequestId" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group" runat="server" id="divBranch">
                                                <label class="control-label"><%= GetGlobalResourceObject("resource", "Transaction_Type")%></label>
                                                <div class="controls">
                                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlTrasactionType">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-footer">
                                            <asp:Button ID="btnSearch" Text="<%$ Resources:Resource, Search%>" OnClientClick="myFunction()" runat="server" CssClass="btn btn-searh" OnClick="btnSearch_Click" />
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-body">
                            <header>    
                                <div class = "table-title"><asp:Literal runat="server" Text="<%$ Resources:Resource, PaymentHistory%>" /></div>
                            </header>
                            <div class="box-body table-responsive">
                                <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow" ID="gv" runat="server" AutoGenerateColumns="false" GridLines="None" AllowPaging="True" ShowFooter="true"
                                    EmptyDataText="<%$ Resources:Resource, No_Record_Found%>" CssClass="table table-bordered table-striped" Width="100%" OnPageIndexChanging="gv_PageIndexChanging"
                                    OnRowDataBound="gv_RowDataBound" OnDataBound="gv_DataBound" AllowSorting="true" OnSorting="gv_Sorting">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <Columns>
                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Date%>" DataField="CreatedAt" ItemStyle-Width="10%" SortExpression="CreatedAt" />
                                        <%--<asp:BoundField HeaderText="<%$ Resources:Resource, Description%>" DataField="Description" ItemStyle-Width="80%" />--%>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, Description%>" ItemStyle-Width="74%" >
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblDescription"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--<asp:BoundField HeaderText="<%$ Resources:Resource, Request_ID%>" DataField="RequestId" Visible="false" />--%>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, Amount%>" ItemStyle-Width="8%">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblDebitAmount"></asp:Label>
                                                <asp:Label runat="server" ID="lblFree"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, Credit%>" ItemStyle-Width="10%" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblCreditAmount"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="<%$ Resources:Resource, Balance%>" DataField="Balance" ItemStyle-Width="8%" />
                                        <%--<asp:BoundField HeaderText="<%$ Resources:Resource, Remark%>" DataField="Remark" ItemStyle-Width="30%" />--%>
                                    </Columns>
                                    <PagerTemplate>
                                        <div>
                                            <div class="dataTables_paginate paging_bootstrap">
                                                <ul class="pagination">
                                                    <li>
                                                        <asp:PlaceHolder runat="server" ID="phPreviousPaging"></asp:PlaceHolder>
                                                    </li>

                                                    <li>
                                                        <asp:PlaceHolder runat="server" ID="phPaging"></asp:PlaceHolder>
                                                    </li>

                                                    <li>
                                                        <asp:PlaceHolder runat="server" ID="phNextPaging"></asp:PlaceHolder>
                                                    </li>
                                                </ul>

                                            </div>
                                        </div>
                                    </PagerTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </aside>


</asp:Content>
