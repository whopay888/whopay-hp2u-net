﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConsentForm.aspx.cs" Inherits="HJT.Form.Loan.ConsentForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, width=device-width, user-scalable=no" />
    <style>
        .signature {
            border: 2px dotted black;
            background-color: lightgrey;
        }

        table.ccrisTable {
            border-spacing: 0px;
            font-size: 0.8em;
        }

        table.redTable tr, table.redTable td {
            padding: 5px;
            margin: 5px;
            border: 1.5px solid red;
            border-spacing: 0px;
        }

        table.redTable {
            border-collapse: collapse;
        }

        .rowBlock {
            display: flex;
            width: 800px;
            padding: 10px;
            margin: 10px auto;
        }

        .fixPrintBackground {
            -webkit-print-color-adjust: exact;
        }

        @page {
            size: auto; /* auto is the initial value */
            margin: 10mm; /* this affects the margin in the printer settings */
        }

        html {
            background-color: #FFFFFF;
            margin: 0px; /* this affects the margin on the html before sending to printer */
        }

        .table tbody tr td {
            padding: 0 8px;
        }

        .text-center {
            text-align: center !important;
        }

        .center-align {
            margin-left: auto;
            margin-right: auto;
        }

        .td-padding tr td {
            padding: 15px 0 0;
        }

        @media print {
            #btnPrint {
                display: none;
            }

            #btnSave {
                display: none;
            }

            .jsignatureundobutton{
                display: none;
            }
        }

        .logo {
            height: 80px;
            /*margin-top: -40px;
            background: white;
            padding-left: 15px;
            padding-right: 15px;*/
        }
    </style>
    <script src="../../Lib/jsignature/modernizr.js"></script>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="../../Lib/jsignature/flashcanvas.js"></script>
    <![endif]-->
    <script>

        function printPage() {
            var printButton = document.getElementById("<%= btnPrint.ClientID %>");
            printButton.style.visibility = 'hidden';
            window.print();
        }

        function closePage() {
            alert('<%= GetGlobalResourceObject("resource" , "Result_Saved") %>');

            window.opener = null
            window.close();
        }
    </script>
    <title></title>



</head>
<body>
    <form id="form1" runat="server">
        <div class="center-align">

            <asp:Literal runat="server" ID="ltrContent"></asp:Literal>


            <asp:HiddenField runat="server" ID="hfMaSig" EnableViewState="true"/>
            <asp:HiddenField runat="server" ID="hfCaSig" EnableViewState="true"/>

            <asp:Button runat="server" ID="btnSave" Text="Save" OnClientClick="saveSignatureToHiddenField();" OnClick="btnSave_Click" />
            <asp:Button runat="server" ID="btnPrint" Text="Print" OnClientClick="printPage();" />
            <br />
            <br />
            <br />
            <br />
            <br />
        </div>


    </form>

    <script src="../../Lib/jsignature/jquery.js"></script>
    <script type="text/javascript">
        jQuery.noConflict()
    </script>
    <script src="../../Lib/jsignature/jSignature.min.noconflict.js"></script>
    <script>
        var hfMaSigClientId = '<%= hfMaSig.ClientID %>';
        var hfCaSigClientId = '<%= hfCaSig.ClientID %>';

        (function ($) {
            $(document).ready(function () {
                var isNewRequest = <%= isNewRequest.ToString().ToLower() %>;
                $("#masignature").jSignature({ 'UndoButton': isNewRequest })
                $("#casignature").jSignature({ 'UndoButton': isNewRequest })

                var maSigData = $("#" + hfMaSigClientId).val();
                var caSigData = $("#" + hfCaSigClientId).val();
                if (maSigData != '' && maSigData != 'image/jsignature;base30,') {
                    $("#masignature").jSignature("setData", maSigData, 'base30');
                }
                if (caSigData != '' && caSigData != 'image/jsignature;base30,') {
                    $("#casignature").jSignature("setData", caSigData, 'base30');
                }
            })
        })(jQuery)

        function saveSignatureToHiddenField() {
            (function ($) {
                var maSigData = $("#masignature").jSignature('getData', 'base30');
                var caSigData = $("#casignature").jSignature('getData', 'base30');
                $("#" + hfMaSigClientId).val(maSigData);
                $("#" + hfCaSigClientId).val(caSigData);

                //alert($("#" + hfMaSigClientId).val());
            })(jQuery)
        };
    </script>
</body>
</html>
