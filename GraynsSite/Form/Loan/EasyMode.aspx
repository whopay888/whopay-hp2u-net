﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Global1.Master" CodeBehind="EasyMode.aspx.cs" Inherits="HJT.Form.EasyMode" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function confirmSubmit() {
            var x = document.getElementById('<%= chkterms.ClientID %>').checked;
            if (x == false) {
                document.getElementById('<%= chkterms.ClientID %>').focus();
                alert('You must agree to the terms first.');
                return false;
            }
            return confirm('<%= GetGlobalResourceObject("resource" , "Confirm_submit") %>');
        }
        
        function setTab() {
            if ($("#HidTab").val() == '' || $("#HidTab").val() === undefined) {
                $("#HidTab").val("#tab_1");
            }
            $("a[data-toggle='tab'] ");

            $("a[href='" + $("#HidTab").val() + "' ]").click();
        }

        function pageLoad() {

            $("a[data-toggle='tab']").each(
                function (index) {
                    $(this).on("click", function () {
                        $("#HidTab").val($(this).attr("href"));

                    });
                }
            )
            setTab();
            focusName();
        }

        function focusName() {
            document.getElementById('<%= txtName.ClientID %>').focus();
        }
    </script>
    <style>
        .nav-tabs-custom > .tab-content {
            display: inline-block;
            width: 100%;
        }

        @media (max-width:767px) {
            td div.col-sm-6 {
                padding: 0px !important;
            }
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" width="400px">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                <span class="icon fa fa-fw fa-book" aria-hidden="true"></span>
                <asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><span runat="server" id="spHeader"><%= GetGlobalResourceObject("resource", "Easy_Mode")%></span></asp:Label>
            </h1>
            <asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
        </section>
        <section class="content">
            <div class="col-md-12">
                <div runat="server">
                    <div class="box">
                        <h4>
                            <asp:Label ID="panel" runat="server" Text="<%$ Resources:Resource, Request_Report%>"></asp:Label>
                        </h4>
                        <div id="divErrorMessage" runat="server" visible="false" class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <b><%= GetGlobalResourceObject("resource", "Alert")%></b>
                            <br />
                            <asp:Label runat="server" ID="lblErr"></asp:Label>
                        </div>
                        <div class="box-body">
                            <div class="col-md-12 no-padding">
                                <div class="nav-tabs-custom">
                                    <ul class="nav nav-tabs">
                                        <li class=""><a href="#tab_1" id="aTab1" data-toggle="tab" aria-expanded="false"><%= GetGlobalResourceObject("resource", "Applicant_Information") %></a></li>
                                        <li class=""><a href="#tab_2" id="aTab2" data-toggle="tab" aria-expanded="false"><%= GetGlobalResourceObject("resource", "Purchase_Price") %></a></li>
                                        <li class=""><a href="#tab_3" id="aTab3" data-toggle="tab" aria-expanded="false"><%= GetGlobalResourceObject("resource", "Terms_N_Conditions") %></a></li>
                                        <asp:HiddenField runat="server" ID="HidTab" ClientIDMode="Static" />
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane" id="tab_1">
                                            <asp:Panel runat="server" DefaultButton="btnNext">
                                                <div class="box box-primary">
                                                    <div class="box-header">
                                                        <h4>
                                                            <asp:Label ID="lblTitle" runat="server">
                                                            </asp:Label>
                                                        </h4>
                                                    </div>
                                                    <div class="form-group">
                                                        <div><%= GetGlobalResourceObject("resource", "Name")%><span style="color: Red;">&nbsp;*</span></div>
                                                        <div class="controls">
                                                            <asp:TextBox runat="server" ID="txtName" CssClass="form-control"> </asp:TextBox>
                                                            <asp:HiddenField ID="hfRequestID" runat="server" Value="0" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div><%= GetGlobalResourceObject("resource", "IC")%><span style="color: Red;">&nbsp;*</span></div>
                                                        <div class="controls">
                                                            <asp:TextBox runat="server" ID="txtIC" CssClass="form-control"> </asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box-footer">
                                                    <asp:Button ID="btnNext" Text="<%$ Resources:Resource, Next%>" CssClass="btn btn-next" runat="server"
                                                        OnClick="btnNext_Click"/>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                        <div class="tab-pane" id="tab_2">
                                            <asp:Panel runat="server" DefaultButton="btnNext2">
                                                <div class="box box-primary">
                                                    <div class="box-header">
                                                        <h4>
                                                            <asp:Label ID="Label1" runat="server">
                                                            </asp:Label>
                                                        </h4>
                                                    </div>
                                                    <div class="form-group">
                                                        <div><%= GetGlobalResourceObject("resource", "Purchase_Price")%><span style="color: Red;">&nbsp;*</span></div>
                                                        <div class="controls">
                                                            <asp:TextBox runat="server" ID="txtPrice" CssClass="form-control"> </asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <asp:Label runat="server" ID="lblFileUpload" Visible="false"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="box-footer">
                                                    <asp:Button ID="btnNext2" Text="<%$ Resources:Resource, Next%>" CssClass="btn btn-next" runat="server"
                                                        OnClick="btnNext2_Click" />
                                                </div>
                                            </asp:Panel>
                                        </div>
                                        <div class="tab-pane" id="tab_3">
                                            <asp:Panel runat="server" DefaultButton="btnSend">
                                                <div class="box box-primary">
                                                    <div class="form-group">
                                                        <div class="check">
                                                            <div>
                                                                <span>
                                                                    <input id="chkterms" runat="server" type="checkbox" class='icheck-me' data-skin="square" data-color="blue"/>
                                                                </span>
                                                                <%= GetGlobalResourceObject("resource", "Request_TermsandCondition")%>&nbsp;
                                                                <asp:LinkButton ID="btnDownload" OnClick="btnDownload_Click" runat="server">(*Click here to download Consent Form.*)</asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box-footer">
                                                    <asp:Button runat="server" ID="btnSend" Text="<%$ Resources:Resource, send%>" 
                                                        CssClass="btn btn-primary" OnClientClick="return confirmSubmit(); " 
                                                        OnClick="btnSend_Click" />
                                                    <asp:HiddenField ID="hftemprequestId" runat="server" />
                                                    <asp:HiddenField ID="hfMainCCRIS" runat="server" />
                                                    <asp:HiddenField ID="hfAKPK" runat="server" />
                                                    <asp:HiddenField ID="hfLegalStatus" runat="server" />
                                                </div>
                                            </asp:Panel>
                                        </div>
                                        <div runat="server" id="divGv" visible="false">
                                            <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow gvColumnSize" ID="gvMainHousingLoan" runat="server" AutoGenerateColumns="false"
                                                CssClass="table table-bordered table-striped" GridLines="None"
                                                EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                                Width="100%" ShowFooter="false">
                                                <PagerSettings Mode="NumericFirstLast" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Original%>" ItemStyle-Width="33%">
                                                        <ItemTemplate>
                                                            <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainOriginalVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Outstanding%>" ItemStyle-Width="33%">
                                                        <ItemTemplate>
                                                            <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainOutstandinglVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Repayment%>" ItemStyle-Width="33%">
                                                        <ItemTemplate>
                                                            <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainRepaymentlVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                            <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow gvColumnSize" ID="gvMainHirePurchase" runat="server" AutoGenerateColumns="false"
                                                CssClass="table table-bordered table-striped" GridLines="None"
                                                EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                                Width="100%" ShowFooter="false">
                                                <PagerSettings Mode="NumericFirstLast" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Original%>" ItemStyle-Width="33%">
                                                        <ItemTemplate>
                                                            <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainOriginalVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Outstanding%>" ItemStyle-Width="33%">
                                                        <ItemTemplate>
                                                            <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainOutstandinglVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Repayment%>" ItemStyle-Width="33%">
                                                        <ItemTemplate>
                                                            <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainRepaymentlVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                            <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow gvColumnSize" ID="gvMainPersonalLoan" runat="server" AutoGenerateColumns="false"
                                                CssClass="table table-bordered table-striped" GridLines="None"
                                                EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                                Width="100%" ShowFooter="false">
                                                <PagerSettings Mode="NumericFirstLast" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Original%>" ItemStyle-Width="33%">
                                                        <ItemTemplate>
                                                            <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainOriginalVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Outstanding%>" ItemStyle-Width="33%">
                                                        <ItemTemplate>
                                                            <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainOutstandinglVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Repayment%>" ItemStyle-Width="33%">
                                                        <ItemTemplate>
                                                            <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainRepaymentlVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                            <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow gvColumnSize" ID="gvMainCreditCard" runat="server" AutoGenerateColumns="false"
                                                CssClass="table table-bordered table-striped" GridLines="None"
                                                EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                                Width="100%" ShowFooter="false">
                                                <PagerSettings Mode="NumericFirstLast" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Original%>" ItemStyle-Width="33%">
                                                        <ItemTemplate>
                                                            <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainOriginalVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Outstanding%>" ItemStyle-Width="33%">
                                                        <ItemTemplate>
                                                            <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainOutstandinglVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Repayment%>" ItemStyle-Width="33%">
                                                        <ItemTemplate>
                                                            <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainRepaymentlVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                            <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow gvColumnSize" ID="gvMainXProduct" runat="server" AutoGenerateColumns="false"
                                                CssClass="table table-bordered table-striped" GridLines="None"
                                                EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                                Width="100%" ShowFooter="false">
                                                <PagerSettings Mode="NumericFirstLast" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Original%>" ItemStyle-Width="33%">
                                                        <ItemTemplate>
                                                            <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainOriginalVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Outstanding%>" ItemStyle-Width="33%">
                                                        <ItemTemplate>
                                                            <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainOutstandinglVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Repayment%>" ItemStyle-Width="33%">
                                                        <ItemTemplate>
                                                            <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainRepaymentlVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                            <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow gvColumnSize" ID="gvMainCommercialLoan" runat="server" AutoGenerateColumns="false"
                                                CssClass="table table-bordered table-striped" GridLines="None"
                                                EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                                Width="100%" ShowFooter="false">
                                                <PagerSettings Mode="NumericFirstLast" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Original%>" ItemStyle-Width="33%">
                                                        <ItemTemplate>
                                                            <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainOriginalVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Outstanding%>" ItemStyle-Width="33%">
                                                        <ItemTemplate>
                                                            <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainOutstandinglVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Repayment%>" ItemStyle-Width="33%">
                                                        <ItemTemplate>
                                                            <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainRepaymentlVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                            <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow gvColumnSize" ID="gvMainOD" runat="server" AutoGenerateColumns="false"
                                                CssClass="table table-bordered table-striped" GridLines="None"
                                                EmptyDataText="<%$ Resources:Resource, No_Record_Found%>"
                                                Width="100%" ShowFooter="false">
                                                <PagerSettings Mode="NumericFirstLast" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Original%>" ItemStyle-Width="33%">
                                                        <ItemTemplate>
                                                            <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainOriginalVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Outstanding%>" ItemStyle-Width="33%">
                                                        <ItemTemplate>
                                                            <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainOutstandinglVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Repayment%>" ItemStyle-Width="33%">
                                                        <ItemTemplate>
                                                            <asp:TextBox runat="server" Style="width: 100%;" CssClass="input-small" ID="txtMainRepaymentlVal" AutoPostBack="false"></asp:TextBox><%--OnTextChanged="txtBankName_TextChanged"--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </aside>


</asp:Content>
