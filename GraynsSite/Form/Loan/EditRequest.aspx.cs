﻿using HJT.Cls.Controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Controller;
using Synergy.Helper;
using Synergy.Model;
using Synergy.Util;

namespace HJT.Form.Loan
{
    public partial class EditRequest : Page
    {
        LoginUserModel login_user = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];
            if (!IsPostBack)
            {
                string requestID = Request["rid"] as string;

                if (!string.IsNullOrEmpty(requestID))
                {
                    requestID = secure.Decrypt(Validation.GetUrlParameter(this.Page, "RID", ""), true);
                    hfRequestID.Value = requestID;
                    binddata1();
                    binddata2();
                }
            }
        }

        #region Data Binding
        private void binddata1()
        {
            if (!string.IsNullOrEmpty(hfRequestID.Value)) 
            {
                wwdb db = new wwdb();
                StringBuilder sql = new StringBuilder();
                string memberID, branchID, projectID;
                int applicationResult = -1;

                //get tab_1 data
                sql.AppendFormat(@"
                    SELECT ID, RequestID, MemberID, BranchID, ApplicationResult, 
                        ProjectID, ProjectName, PaymentStatus
                    FROM tbl_Request with (nolock)
                    WHERE RequestID = N'{0}'
                ", hfRequestID.Value);
                db.OpenTable(sql.ToString());
                if (db.RecordCount() > 0)
                {
                    txtID.Text = db.Item("ID");
                    txtRequestID.Text = db.Item("RequestID");
                    memberID = db.Item("MemberID");
                    branchID = db.Item("BranchID");
                    if (!string.IsNullOrEmpty(db.Item("PaymentStatus")))
                    {
                        lblPaymentStatus.Text = db.Item("PaymentStatus").Equals("1") ? Resources.resource.Success : Resources.resource.Failed;
                        btnSuccess.Visible = false;
                        btnFail.Visible = false;
                    }
                    else
                    {
                        lblPaymentStatus.Text = Resources.resource.None;
                        btnSuccess.Visible = true;
                        btnFail.Visible = true;
                    }
                    sqlString.bindControl(rblApplicationResult, "select ParameterName as '0' , ParameterValue as '1' from tbl_parameter where Category = 'ApplicationResult'", "0", "1");
                    string applicationResultString = db.Item("ApplicationResult");
                    if (applicationResultString.Equals(ApplicationResult.Approved))
                        applicationResult = 0;
                    else if (applicationResultString.Equals(ApplicationResult.Denied))
                        applicationResult = 1;
                    else if (applicationResultString.Equals(ApplicationResult.Modify))
                        applicationResult = 2;
                    if (applicationResult != -1)
                        rblApplicationResult.SelectedIndex = applicationResult;

                    //get projectname
                    projectID = db.Item("ProjectID");
                    if (!db.Item("ProjectName").Equals(string.Empty))
                        txtProjectName.Text = db.Item("ProjectName");//get projectname if it's other proj

                    if (!projectID.Equals(string.Empty))//get ProjectName if project in ProjectSettings
                    {
                        sql.Clear();
                        sql.Append("SELECT ProjectName FROM tbl_ProjectSettings with (nolock) WHERE ID = '" + projectID + "'");
                        db.OpenTable(sql.ToString());

                        if (db.RecordCount() > 0)
                            txtProjectName.Text = db.Item("ProjectName");
                    }

                    //get all projects for that user's branch
                    bindProject();

                    //get member name
                    sql.Clear();
                    sql.Append("SELECT Fullname FROM tbl_MemberInfo with (nolock) WHERE MemberID = '" + memberID + "'");
                    db.OpenTable(sql.ToString());

                    if (db.RecordCount() > 0)
                        txtMemberName.Text = db.Item("Fullname");

                    //get member branch
                    sql.Clear();
                    sql.Append("SELECT BranchName FROM tbl_Branch with (nolock) WHERE BranchID = '" + branchID + "'");
                    db.OpenTable(sql.ToString());

                    if (db.RecordCount() > 0)
                        txtBranch.Text = db.Item("BranchName");

                    //get advise
                    txtAdvise.Text = "";
                    DataTable dt = db.getDataTable("SELECT Comment FROM tbl_ApplicantCreditAdvisor WITH(NOLOCK) WHERE RequestID = N'" + hfRequestID.Value + "' AND IsDeleted = 0");
                    foreach (DataRow dr in dt.Rows)
                        if (dr["Comment"].ToString() != string.Empty)
                            txtAdvise.Text += dr["Comment"].ToString();
                    txtAdvise.Text = txtAdvise.Text.Replace("</br>", Environment.NewLine).Replace("&amp;", "&")
                        .Replace("&gt;", ">").Replace("&#x0D;", Environment.NewLine).Trim('<').Trim('>');

                    txtMessage.Text = "";
                    dt = db.getDataTable("SELECT Message FROM tbl_ApplicantCreditAdvisor WITH(NOLOCK) WHERE RequestID = N'" + hfRequestID.Value + "' AND IsDeleted = 0");
                    foreach (DataRow dr in dt.Rows)
                        if (dr["Message"].ToString() != string.Empty)
                            txtMessage.Text += dr["Message"].ToString();
                    txtMessage.Text = txtMessage.Text.Replace("</br>", Environment.NewLine).Replace("&amp;", "&")
                        .Replace("&gt;", ">").Replace("&#x0D;", Environment.NewLine).Trim('<').Trim('>');

                    //get applicant name
                    sql.Clear();
                    sql.AppendFormat(@"
                            with cte as 
                            (select cr.Request_Id, cr.ID, cr.[Type], cr.IsCoApplicant 
                            from tbl_CTOS_Result cr with (nolock)
                            WHERE cr.IsActive=1 and cr.Result is not null)

                            SELECT a.[Name] as 'Name'
	                            ,a.MyCard as 'MyCard'
	                            ,a.ApplicantType as 'ApplicantType'
	                            ,cte.ID as 'CCRIS'
                                ,cte.[Type] as 'ReportType'
                            FROM tbl_Applicants a with (nolock) left join
                            cte on cte.Request_Id=a.RequestID and 
	                            cte.IsCoApplicant= 
		                            (select case when ta.ApplicantType='CA' 
					                            then 1 else 0 
				                            end as 'ApplicantType'
		                            from tbl_Applicants ta with (nolock) 
		                            where ta.ID=a.ID)
                            where a.RequestID='{0}' and a.IsDeleted=0 
                            order by a.ApplicantType desc;
                    ", hfRequestID.Value);
                    db.OpenTable(sql.ToString());
                    if (db.RecordCount() > 0)
                    {
                        while (!db.Eof())
                        {
                            if (db.Item("ApplicantType").Equals("MA"))
                            {
                                txtMainApplicantName.Text = db.Item("Name");
                                txtMainIC.Text = db.Item("MyCard");
                                if (!string.IsNullOrEmpty(db.Item("CCRIS")))
                                {
                                    btnMainCCRIS.Visible = true;
                                    btnMainCCRIS.Text += " (" + db.Item("ReportType") + ")";
                                }
                            }
                            else //if (db.Item("ApplicantType").Equals("CA"))
                            {
                                trCoApplicantName.Visible = true;
                                txtCoApplicantName.Text = db.Item("Name");
                                txtCoIC.Text = db.Item("MyCard");
                                if (!string.IsNullOrEmpty(db.Item("CCRIS")))
                                {
                                    btnCoCCRIS.Visible = true;
                                    btnCoCCRIS.Text += " (" + db.Item("ReportType") + ")";
                                }
                            }
                            db.MoveNext();
                        }
                    }
                }
            }
        }
        private void binddata2()
        {
            if (!string.IsNullOrEmpty(hfRequestID.Value))
            {
                wwdb db = new wwdb();
                StringBuilder sql = new StringBuilder();
                ReportController reportController = new ReportController();

                //get tab_2 data
                sql.Clear();
                sql.AppendFormat(@"
                    SELECT lla.Tenure, lla.PurchasePrice, lla.LoanAmount as 'LoanAmount1', li.LoanAmount as 'LoanAmount2',
                    li.NewLoanRepayment as 'NewInstallment', li.IncomeRequired, r.ApplicationResult as 'Profile',
					li.DSR, li.LoanAmountEligible as 'LoanEligible', li.DeclareIncome as 'NetIncome'
                    FROM tbl_LoanLegalAction lla inner join 
                    tbl_LoanInfo li on lla.RequestID = li.RequestID inner join
					tbl_Request r on li.RequestID = r.RequestID inner join
						(select ra.RequestID, sum(ra.NetIncome) as 'NetIncome' from tbl_RequestApplicant ra
						where ra.RequestID = N'{0}'
						group by ra.RequestID)tra on tra.RequestID = r.RequestID
                    where lla.RequestID = N'{0}'
                ", hfRequestID.Value);
                db.OpenTable(sql.ToString());
                if (db.RecordCount() > 0)
                {
                    txtTenure.Text = !string.IsNullOrEmpty(db.Item("Tenure")) ? db.Item("Tenure") : "N/A";
                    txtPurchasePrice.Text = !string.IsNullOrEmpty(db.Item("PurchasePrice")) ?
                        double.Parse(db.Item("PurchasePrice")).ToString("N0") : "N/A";
                    if (!string.IsNullOrEmpty(db.Item("LoanAmount1")))
                        txtLoanAmount.Text = double.Parse(db.Item("LoanAmount1")).ToString("N0");
                    else if (!string.IsNullOrEmpty(db.Item("LoanAmount2")))
                        txtLoanAmount.Text = double.Parse(db.Item("LoanAmount2")).ToString("N0");
                    txtNewInstallment.Text = !string.IsNullOrEmpty(db.Item("NewInstallment")) ?
                        ConvertHelper.ConvertToDecimal(db.Item("NewInstallment"), 0).ToString("N0") : "N/A";
                    txtIncomeRequired.Text = !string.IsNullOrEmpty(db.Item("IncomeRequired")) ?
                        ConvertHelper.ConvertToDecimal(db.Item("IncomeRequired"), 0).ToString("N0") : "N/A";

                    Dictionary<string, string> statusColor = new Dictionary<string, string>();
                    statusColor.Add(ApplicationResult.Approved, "#A3DAA3");
                    statusColor.Add(ApplicationResult.Modify, "#FFFF00");
                    statusColor.Add(ApplicationResult.Denied, "#FF9898");

                    Dictionary<string, string> statusColorName = new Dictionary<string, string>();
                    statusColorName.Add(ApplicationResult.Approved, "Green");
                    statusColorName.Add(ApplicationResult.Modify, "Yellow");
                    statusColorName.Add(ApplicationResult.Denied, "Red");

                    txtApplicationResult.Text = !string.IsNullOrEmpty(db.Item("Profile")) ? statusColorName[db.Item("Profile")] : "N/A";
                    tdApplicationResult.BgColor = !string.IsNullOrEmpty(db.Item("Profile")) ? statusColor[db.Item("Profile")] : "#FFFFFF";
                    txtDSR.Text = !string.IsNullOrEmpty(db.Item("DSR")) ? db.Item("DSR") : "N/A";
                    txtLoanEligible.Text = !string.IsNullOrEmpty(db.Item("LoanEligible")) ? 
                        double.Parse(db.Item("LoanEligible")).ToString("N0") : "N/A";
                    txtNetIncome.Text = !string.IsNullOrEmpty(db.Item("NetIncome")) ? 
                        double.Parse(db.Item("NetIncome")).ToString("N0") : "N/A";

                    bool isMainOnly = true;
                    if (trCoApplicantName.Visible || !string.IsNullOrEmpty(txtCoApplicantName.Text))
                        isMainOnly = false;
                    decimal oldRepayment = reportController.CalculateTotalOldBankRepayments("12", hfRequestID.Value, isMainOnly);
                    txtCurrentCommitment.Text = !string.IsNullOrEmpty(oldRepayment.ToString()) ?
                        double.Parse(oldRepayment.ToString()).ToString("N0") : "N/A";

                    sqlString.bindControl(gvLoanEligible, getBankList());
                }
            }
        }
        private void bindProject()
        {
            wwdb db = new wwdb();
            int branchId;
            int parentID;

            StringBuilder sql = new StringBuilder();
            db.OpenTable("select r.requestby as 'userid' from tbl_request r with (nolock) where r.requestid = '" + hfRequestID.Value + "';");
            string request_userid = db.Item("userid");

            sql.Clear();
            sql.AppendFormat(@"
                SELECT tb.ID 
                FROM dbo.tbl_MemberInfo tmi INNER JOIN 
                dbo.tbl_Login tl ON tmi.MemberId = tl.login_id INNER JOIN 
                dbo.tbl_Branch tb ON tl.BranchID = tb.BranchID INNER JOIN 
	                (SELECT DISTINCT trm.MenuID,'{0}' AS MemberId 
                    FROM dbo.tbl_RoleMenu trm 
                    WHERE trm.RoleID IN (
                        SELECT RoleCode 
                        FROM tbl_MemberRole 
                        WHERE MemberId = '{0}')
                    )AS trm ON tmi.MemberId = trm.MemberId INNER JOIN 
                dbo.tbl_Menu tm ON trm.MenuID = tm.rowID
                WHERE tmi.MemberId = '{0}' AND tm.[Type] = 2 AND tm.[Key] = 'function_project_restrict_branch'
            ", request_userid);
            db.OpenTable(sql.ToString());

            StringBuilder branchQuery = new StringBuilder();
            branchQuery.Append("select ProjectName as '0', ID as '1' from tbl_ProjectSettings WHERE status = 'A' AND IsDeleted = 'false' ");

            if (db.RecordCount() > 0)//UAC is checked
            {
                branchId = ConvertHelper.ConvertToInt(db.Item("ID").ToString(), 0);
                branchQuery.Append(" AND BranchId IN (SELECT ID FROM tbl_Branch Where Id = " + branchId + ") ORDER BY ProjectName");
            }
            else//UAC unchecked
            {
                sql.Clear();
                sql.AppendFormat(@"
                    select b.ID, b.parentID
                    from tbl_MemberInfo mi
                    inner join tbl_Login l on mi.MemberId = l.login_id
                    inner join tbl_Branch b on l.BranchID = b.BranchID
                    where mi.MemberId = '{0}'
                ", request_userid);
                db.OpenTable(sql.ToString());
                branchId = ConvertHelper.ConvertToInt(db.Item("ID").ToString(), 0);
                parentID = ConvertHelper.ConvertToInt(db.Item("parentID").ToString(), 0);
                if (parentID.Equals(0))//parent
                    branchQuery.AppendFormat(@" 
                        AND BranchId IN 
                        (SELECT ID FROM tbl_Branch 
                        Where ParentId = {0} OR Id = {0}) 
                        ORDER BY ProjectName
                    ", branchId);
                else//sub
                    branchQuery.AppendFormat(@" 
                        AND BranchId IN
                        (SELECT p.ID FROM tbl_Branch s, tbl_Branch p
                        Where (s.ParentId = {0} OR p.Id = {0}) or (s.ParentId = p.Id and s.ID = {0}))
                        ORDER BY ProjectName
                    ", branchId);
            }

            sqlString.bindControl(ddlProjectName, branchQuery.ToString(), "0", "1", true);
        }
        private string getBankList()
        {
            StringBuilder sql = new StringBuilder();

            sql.AppendFormat(@"
                SELECT
                    bn.BankID, bn.BankName, bn.IsSelectedDefault as 'Selected',
                    --isnull(cast(isnull(li.LoanAmount, tli.ActualLoanAmount) as varchar), 'N/A') as 'LoanAmount', 
                    case WHEN isnull(li.LoanAmountEligible, tli.LoanAmountEligible) < 0
	                    THEN '0'
	                    ELSE isnull(cast(isnull(li.LoanAmountEligible, tli.LoanAmountEligible) as varchar), 'N/A')
                    END as 'LoanAmountEligible'--, 
                    --isnull(cast(isnull(li.NewLoanRepayment, tli.NewLoanRepayment) as varchar), 'N/A') as 'NewLoanRepayment', 
                    --isnull(cast(isnull(li.IncomeRequired, tli.IncomeRequired) as varchar), 'N/A') as 'IncomeRequired', 
                    --isnull(cast(isnull(li.DSR, tli.DSR) as varchar), 'N/A') as 'DSR', 
                    --isnull(cast(isnull(li.DeclareIncome, tli.DeclareIncome) as varchar), 'N/A') as 'DeclareIncome'
                FROM tbl_BankName bn with (nolock) left join
                tbl_LoanInfo li WITH (NOLOCK) on li.BankID = bn.BankID and li.RequestID = '{0}' left join
                tbl_tloaninfo tli with (nolock) on tli.BankID = bn.BankID and tli.RequestID = '{0}'
                WHERE bn.IsDeleted = 0	
                order by cast(bn.BankID as int)
            ", hfRequestID.Value);

            return sql.ToString();
        }
        #endregion

        private bool validateForm1()
        {
            StringBuilder errMsg = new StringBuilder();

            lblErr.Text = errMsg.ToString().Replace("\\n", "</br>");

            if (lblErr.Text != string.Empty)
            {
                divErrorMessage.Visible = true;
                sqlString.displayAlert2(this, errMsg.ToString());
                return false;
            }
            else
                divErrorMessage.Visible = false;

            return true;
        }
        private bool validateForm2()
        {
            StringBuilder errMsg = new StringBuilder();
            ReportController reportController = new ReportController();

            txtLoanAmount.Text = reportController.removeComma(txtLoanAmount);
            txtPurchasePrice.Text = reportController.removeComma(txtPurchasePrice);
            txtTenure.Text = reportController.removeComma(txtTenure);
            txtIncomeRequired.Text = reportController.removeComma(txtIncomeRequired);
            txtNewInstallment.Text = reportController.removeComma(txtNewInstallment);
            txtNetIncome.Text = reportController.removeComma(txtNetIncome);
            txtLoanEligible.Text = reportController.removeComma(txtLoanEligible);
            txtCurrentCommitment.Text = reportController.removeComma(txtCurrentCommitment);

            bool gvHasError = false;
            for (int i = 0; i < gvLoanEligible.Rows.Count; i++)
            {
                TextBox gvtxtLoanEligible = (TextBox)gvLoanEligible.Rows[i].Cells[2].FindControl("gvtxtLoanEligible");
                gvtxtLoanEligible.Text = reportController.removeComma(gvtxtLoanEligible);
                if (!gvHasError)
                    if (!long.TryParse(gvtxtLoanEligible.Text, out _))
                        if (!decimal.TryParse(gvtxtLoanEligible.Text, out _))
                        {
                            errMsg.Append("12 Banks' " + Resources.resource.Loan_Eligible_Invalid_Format + "\\n");
                            gvHasError = true; //cant change to break; due to removing comma
                        }
            }

            if (!long.TryParse(txtCurrentCommitment.Text, out _))
                if (!decimal.TryParse(txtCurrentCommitment.Text, out _))
                    errMsg.Append(Resources.resource.Current_Commitment_Invalid_Format + "\\n");
            if (!long.TryParse(txtNetIncome.Text, out _))
                if (!decimal.TryParse(txtNetIncome.Text, out _))
                    errMsg.Append(Resources.resource.Net_Income_Invalid_Format + "\\n");
            if (!long.TryParse(txtLoanAmount.Text, out _))
                if (!decimal.TryParse(txtLoanAmount.Text, out _))
                    errMsg.Append(Resources.resource.Loan_Amount_Invalid_Format + "\\n");
            if (!long.TryParse(txtPurchasePrice.Text, out _))
                if (!decimal.TryParse(txtPurchasePrice.Text, out _))
                    errMsg.Append(Resources.resource.Purchase_Price_Invalid_Format + "\\n");
            if (!int.TryParse(txtTenure.Text, out _))
                errMsg.Append(Resources.resource.TenureShouldBeInteger + "\\n");
            if (!long.TryParse(txtIncomeRequired.Text, out _))
                if (!decimal.TryParse(txtIncomeRequired.Text, out _))
                    errMsg.Append(Resources.resource.Income_Required_Only_Number + "\\n");
            if (!long.TryParse(txtNewInstallment.Text, out _))
                if (!decimal.TryParse(txtNewInstallment.Text, out _))
                    errMsg.Append(Resources.resource.New_Installment_Only_Number + "\\n");
            if (!long.TryParse(txtLoanEligible.Text, out _))
                if (!decimal.TryParse(txtLoanEligible.Text, out _))
                    errMsg.Append(Resources.resource.Loan_Eligible_Invalid_Format + "\\n");

            lblErr.Text = errMsg.ToString().Replace("\\n", "</br>");

            if (lblErr.Text != string.Empty)
            {
                divErrorMessage.Visible = true;
                sqlString.displayAlert2(this, errMsg.ToString());
                return false;
            }
            else
                divErrorMessage.Visible = false;

            return true;
        }

        #region Controls
        protected void btnSuccess_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(hfRequestID.Value))
            {
                wwdb db = new wwdb();
                Button btnsender = (Button)sender;
                //string rid = btnsender.CommandArgument.ToString();
                string rid = hfRequestID.Value;
                string sql = "UPDATE tbl_Request SET PaymentStatus = 1 WHERE RequestID = " + rid;
                db.Execute(sql);

                #region Billing
                SqlParameter[] billingSPParams = {
                     new SqlParameter("@RequestID", rid),
                      new SqlParameter("@RequestType", "Successful Case"),
                       new SqlParameter("@CreatedBy", login_user.UserId)
                };

                db.executeScalarSP("SP_AddBillingTransaction", billingSPParams);

                //StringBuilder sql1 = new StringBuilder();
                //sql1.Append("SELECT tr.PerSuccessfulCasePrice,tr.PerRequestPrice,tr.FreeRequestCount, COUNT(ta.ID) AS ApplicantCount, tr.MemberID FROM dbo.tbl_Request tr LEFT JOIN dbo.tbl_Applicants ta ON tr.RequestID = ta.RequestID WHERE  tr.RequestId =" + rid + " GROUP BY tr.PerSuccessfulCasePrice,tr.PerRequestPrice,tr.FreeRequestCount, tr.MemberID");
                //db.OpenTable(sql1.ToString());
                //if (db.RecordCount() > 0)
                //{
                //    var requestAmount = db.Item("PerSuccessfulCasePrice");
                //    var MemberId = db.Item("MemberId").ToString();
                //    var FreeRequestCount = ConvertHelper.ConvertToDecimal(db.Item("FreeRequestCount").ToString(), 0);
                //    var ApplicantCount = db.Item("ApplicantCount").ToString();
                //    Decimal amount = Convert.ToDecimal(requestAmount) * (FreeRequestCount > 0 ? FreeRequestCount : Convert.ToInt32(ApplicantCount));
                //    int IsFreeRequest = (Convert.ToDecimal(FreeRequestCount) > 0 ? 1 : 0);

                //    StringBuilder sql3 = new StringBuilder();
                //    sql3.Append("SELECT * FROM tbl_UserTransactions WHERE RequestType = 'Request' AND RequestId =" + rid);
                //    db.OpenTable(sql3.ToString());

                //    StringBuilder sql2 = new StringBuilder();
                //    if (db.RecordCount() > 1)
                //    {
                //        sql2.Append("INSERT INTO tbl_UserTransactions(MemberId,Amount,Status,RequestType,RequestId,CreatedAt,IsFreeRequest,Remark) VALUES('" + MemberId + "',-" + amount + ",1,'Successful Case'," + rid + ", GETDATE(),1,'TWO APPLICANTS (1 FREE REQUEST)');");
                //        sql2.Append("INSERT INTO tbl_UserTransactions(MemberId,Amount,Status,RequestType,RequestId,CreatedAt,IsFreeRequest,Remark) VALUES('" + MemberId + "',-" + amount + ",1,'Successful Case'," + rid + ", GETDATE(),0,'TWO APPLICANTS (NON FREE REQUEST)');");
                //        sql2.Append("IF (SELECT 1 FROM tbl_UserCreditBalance WHERE MemberID ='" + MemberId + "') > 0 ");
                //        sql2.Append("UPDATE tbl_UserCreditBalance SET CreditUsed = ISNULL(CreditUsed,0) - " + amount + " WHERE MemberId ='" + db.Item("MemberId").ToString() + "';");
                //        sql2.Append("ELSE ");
                //        sql2.Append("INSERT INTO tbl_UserCreditBalance(MemberId,CreditUsed,CreatedAt) VALUES('" + MemberId + "',-" + amount + ",GETDATE()); ");
                //    }
                //    else if(db.RecordCount() == 1 && IsFreeRequest == 1)
                //    {
                //        sql2.Append("INSERT INTO tbl_UserTransactions(MemberId,Amount,Status,RequestType,RequestId,CreatedAt,IsFreeRequest) VALUES('" + MemberId + "',-" + amount + ",1,'Successful Case'," + rid + ", GETDATE(),1);");
                //    }

                //    if (IsFreeRequest == 0)
                //    {
                //        sql2.Append("INSERT INTO tbl_UserTransactions(MemberId,Amount,Status,RequestType,RequestId,CreatedAt,IsFreeRequest) VALUES('" + MemberId + "',-" + amount + ",1,'Successful Case'," + rid + ", GETDATE(),0);");
                //        sql2.Append("IF (SELECT 1 FROM tbl_UserCreditBalance WHERE MemberID ='" + MemberId + "') > 0 ");
                //        sql2.Append("UPDATE tbl_UserCreditBalance SET CreditUsed = ISNULL(CreditUsed,0) - " + amount + " WHERE MemberId ='" + db.Item("MemberId").ToString() + "';");
                //        sql2.Append("ELSE ");
                //        sql2.Append("INSERT INTO tbl_UserCreditBalance(MemberId,CreditUsed,CreatedAt) VALUES('" + MemberId + "',-" + amount + ",GETDATE());");
                //    }
                //    db.Execute(sql2.ToString());
                //}
                #endregion

                // sqlString.bindControl(gv, getClientList(null));
                lblPaymentStatus.Text = Resources.resource.Success;
                btnSuccess.Visible = false;
                btnFail.Visible = false;
            }
        }
        protected void btnFailed_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(hfRequestID.Value))
            {
                wwdb db = new wwdb();
                Button btnsender = (Button)sender;
                //string rid = btnsender.CommandArgument.ToString();
                string rid = hfRequestID.Value;
                string sql = "UPDATE tbl_Request SET PaymentStatus = 0 WHERE RequestID = " + rid;
                db.Execute(sql);
                //sqlString.bindControl(gv, getClientList(null));
                lblPaymentStatus.Text = Resources.resource.Failed;
                btnSuccess.Visible = false;
                btnFail.Visible = false;
            }
        }

        protected void btnRefresh1_Click(object sender, EventArgs e)
        {
            binddata1();
        }
        protected void btnRefresh2_Click(object sender, EventArgs e)
        {
            binddata2();
        }

        protected void btnSave1_Click(object sender, EventArgs e)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            string requestID = hfRequestID.Value.ToString();
            if (!string.IsNullOrEmpty(requestID) && validateForm1())
            {
                string advise = txtAdvise.Text.Replace("</br>", Environment.NewLine).Replace("&amp;", "&")
                    .Replace("&gt;", ">").Replace("&#x0D;", Environment.NewLine).Trim('<').Trim('>');
                string message = txtMessage.Text.Replace("</br>", Environment.NewLine).Replace("&amp;", "&")
                    .Replace("&gt;", ">").Replace("&#x0D;", Environment.NewLine).Trim('<').Trim('>');

                //advise
                sql.AppendFormat(@"
                    UPDATE tbl_ApplicantCreditAdvisor 
                    SET Comment = '{0}', Message = '{1}', isFinal = 1 
                    WHERE RequestID = N'{2}';
                ", advise + Environment.NewLine
                , message + Environment.NewLine
                , requestID);
                db.Execute(sql.ToString());
                LogUtil.logAction(sql.ToString(), "Update tbl_ApplicantCreditAdvisor Comment, RequestID:" + requestID);

                //result
                if (rblApplicationResult.SelectedIndex >= 0)
                {
                    string status = string.Empty;
                    if (rblApplicationResult.SelectedIndex == 0)
                        status = ApplicationResult.Approved;
                    else if (rblApplicationResult.SelectedIndex == 1)
                        status = ApplicationResult.Denied;
                    else if (rblApplicationResult.SelectedIndex == 2)
                        status = ApplicationResult.Modify;

                    if (!string.IsNullOrEmpty(status))
                    {
                        sql.Clear();
                        sql.AppendFormat(@"
                            UPDATE tbl_Request 
                            SET ApplicationResult = '{0}' 
                            WHERE RequestID = '{1}';
                        ", status, requestID);
                        db.Execute(sql.ToString());
                        LogUtil.logAction(sql.ToString(), "Update tbl_Request ApplicationResult, RequestID:" + requestID);
                    }
                }

                //project
                if (ddlProjectName.SelectedIndex > 0)
                {
                    sql.Clear();
                    sql.AppendFormat(@"
                        UPDATE tbl_request set projectid = '{0}' where requestid = N'{1}';
                        UPDATE tbl_loanlegalaction set projectname = '{2}' where requestid = N'{1}';
                    ", ddlProjectName.SelectedValue
                    , requestID
                    , ddlProjectName.SelectedItem.ToString());
                    db.Execute(sql.ToString());
                    LogUtil.logAction(sql.ToString(), "Update tbl_Request ProjectID, RequestID:" + requestID);
                }

                Response.Redirect("/Form/Report/ClientList.aspx");
            }
        }
        protected void btnSave2_Click(object sender, EventArgs e)
        {
            btnRecalculate_Click(btnRecalculate, EventArgs.Empty);

            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            string requestID = hfRequestID.Value.ToString();
            if (!string.IsNullOrEmpty(requestID) && validateForm2())
            {
                //loanDetail
                sql.Clear();
                sql.AppendFormat(@"
                    UPDATE tbl_LoanInfo SET LoanAmount = '{0}', IncomeRequired = '{1}', NewLoanRepayment = '{2}', 
                        LoanAmountEligible = '{3}', DSR = '{4}', DeclareIncome = '{5}' 
                    WHERE RequestID = N'{6}';
                    UPDATE tbl_LoanLegalAction SET LoanAmount = '{0}', Tenure = '{7}', PurchasePrice = '{8}' 
                    WHERE RequestID = N'{6}';
                ", string.IsNullOrEmpty(secure.RC(txtLoanAmount.Text)) ? "0" : secure.RC(txtLoanAmount.Text)
                , string.IsNullOrEmpty(secure.RC(txtIncomeRequired.Text)) ? "0" : secure.RC(txtIncomeRequired.Text)
                , string.IsNullOrEmpty(secure.RC(txtNewInstallment.Text)) ? "0" : secure.RC(txtNewInstallment.Text)
                , string.IsNullOrEmpty(secure.RC(txtLoanEligible.Text)) ? "0" : secure.RC(txtLoanEligible.Text)
                , string.IsNullOrEmpty(secure.RC(txtDSR.Text)) ? "0" : secure.RC(txtDSR.Text)
                , string.IsNullOrEmpty(secure.RC(txtNetIncome.Text)) ? "0" : secure.RC(txtNetIncome.Text)
                , hfRequestID.Value
                , string.IsNullOrEmpty(secure.RC(txtTenure.Text)) ? "0" : secure.RC(txtTenure.Text)
                , string.IsNullOrEmpty(secure.RC(txtPurchasePrice.Text)) ? "0" : secure.RC(txtPurchasePrice.Text));

                //Loan Eligible - GV
                for (int i = 0; i < gvLoanEligible.Rows.Count; i++)
                {
                    TextBox gvtxtLoanEligible = (TextBox)gvLoanEligible.Rows[i].Cells[2].FindControl("gvtxtLoanEligible");
                    string bankID = gvLoanEligible.Rows[i].Cells[0].Text;//(i + 1).ToString();
                    sql.AppendFormat(@"
                        UPDATE tbl_TLoanInfo SET LoanAmountEligible = '{0}' WHERE RequestID = N'{1}' and BankID = '{2}';
                    ", string.IsNullOrEmpty(secure.RC(gvtxtLoanEligible.Text)) ?
                        secure.RC(gvtxtLoanEligible.Text.Equals("N/A") ? "0" : "0") : secure.RC(gvtxtLoanEligible.Text)
                    , hfRequestID.Value
                    , bankID);
                }
                db.Execute(sql.ToString());

                if (db.HasError)
                {
                    sqlString.displayAlert(this, Resources.resource.Submit_Error);
                    LogUtil.logError("Updating Request. RequestID: " + hfRequestID.Value, sql.ToString());
                }
                else
                    Response.Redirect("/Form/Report/ClientList.aspx");
            }
        }

        protected void btnRecalculate_Click(object sender, EventArgs e)
        {
            wwdb db = new wwdb();
            ReportController rc = new ReportController();

            string mainAge = string.Empty, coAge = string.Empty;

            bool isMainOnly = true;
            if (trCoApplicantName.Visible || !string.IsNullOrEmpty(txtCoApplicantName.Text))
                isMainOnly = false;

            decimal purchasePrice = ConvertHelper.ConvertToDecimal(secure.RC(txtPurchasePrice.Text), 0);
            decimal netIncome = ConvertHelper.ConvertToDecimal(secure.RC(txtNetIncome.Text), 0);
            decimal policy = calculatorController.getBankPolicy("12", netIncome, isMainOnly);
            decimal oldRepayment = ConvertHelper.ConvertToDecimal(txtCurrentCommitment.Text, 0);

            //Default Tenure
            decimal defaultTenure = rc.getTenureYear(6);
            decimal maxYear = 70 - rc.YoungerAge(ConvertHelper.ConvertToInt(mainAge, 0), ConvertHelper.ConvertToInt(coAge, 0));
            if (maxYear > defaultTenure)
                maxYear = defaultTenure;
            defaultTenure = maxYear;

            //LoanAmount (Temp off)
            db.OpenTable("SELECT HouseLoanNumber, Age, ApplicantType from tbl_Applicants WHERE RequestID = N'" + hfRequestID.Value + "' ORDER BY ApplicantType DESC;");
            if (db.RecordCount() > 0)
            {
                //double rate = 90;
                while (!db.Eof())
                {
                    //if (ConvertHelper.ConvertToInt(db.Item("HouseLoanNumber"), 0) > 1)
                    //    rate = 70;

                    if (db.Item("ApplicantType").Equals("MA"))
                        mainAge = db.Item("Age");
                    else //(db.Item("ApplicantType").Equals("CA"))
                        coAge = db.Item("Age");
                    db.MoveNext();
                }
                //txtLoanAmount.Text = double.Parse((purchasePrice * ConvertHelper.ConvertToDecimal(rate / 100.00, 0)).ToString("N0")).ToString();
            }

            //NewInstallment
            decimal newInstallment = calculatorController.calculateNewRepayment(ConvertHelper.ConvertToDecimal(txtLoanAmount.Text, 0),
                ConvertHelper.ConvertToDecimal(txtTenure.Text, defaultTenure), rc.getIntrestRate(6));
            txtNewInstallment.Text = double.Parse(newInstallment.ToString()).ToString("N0");

            //DSR
            decimal dsr = calculatorController.calculateDSR(oldRepayment, newInstallment, netIncome);
            txtDSR.Text = double.Parse(dsr.ToString()).ToString("N0");

            //IncomeRequired
            decimal incomeRequired = calculatorController.calculateIncomeRequired(policy, oldRepayment, ConvertHelper.ConvertToDecimal(txtNewInstallment.Text, 0));
            txtIncomeRequired.Text = double.Parse(incomeRequired.ToString()).ToString("N0");

            //LoanEligible
            decimal loanEligible = calculatorController.calculateLoanEligible(policy, oldRepayment,
                rc.YoungerAge(ConvertHelper.ConvertToInt(mainAge, 0), ConvertHelper.ConvertToInt(coAge, 0)),
                netIncome, ConvertHelper.ConvertToDecimal(txtTenure.Text, defaultTenure), "12");
            if (loanEligible < 0)
                loanEligible = 0;
            txtLoanEligible.Text = loanEligible.ToString("N0");

            //LoanEligible - GV
            for (int i = 0; i < gvLoanEligible.Rows.Count; i++)
            {
                TextBox gvtxtLoanEligible = (TextBox)gvLoanEligible.Rows[i].Cells[2].FindControl("gvtxtLoanEligible");
                decimal youngerAge = rc.YoungerAge(ConvertHelper.ConvertToInt(mainAge, 0),
                                        ConvertHelper.ConvertToInt(coAge, 0));
                string bankID = gvLoanEligible.Rows[i].Cells[0].Text;//(i + 1).ToString();

                //Calculating tenure for each bank
                //Default Tenure
                defaultTenure = rc.getTenureYear(6, bankID);
                maxYear = 70 - youngerAge;
                if (maxYear > defaultTenure)
                    maxYear = defaultTenure;
                defaultTenure = maxYear;
                decimal tenure = ConvertHelper.ConvertToDecimal(txtTenure.Text, defaultTenure);

                //Calculating policy for each bank
                policy = calculatorController.getBankPolicy(bankID, netIncome, isMainOnly);

                //Calculating repayment for each bank
                oldRepayment = rc.CalculateTotalOldBankRepayments(bankID, hfRequestID.Value, isMainOnly);

                //Calculating loan eligible using policy, tenure, and oldRepayment from each bank
                loanEligible = calculatorController.calculateLoanEligible(policy, oldRepayment,
                                    youngerAge, netIncome, tenure, bankID);
                if (loanEligible < 0)
                    loanEligible = 0;

                gvtxtLoanEligible.Text = loanEligible.ToString("N0");
            }
        }

        protected void btnMainCCRIS_Click(object sender, EventArgs e)
        {
            string rid = hfRequestID.Value;

            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            sql.Clear();
            sql.AppendFormat(@" 
                SELECT ID, IsCoApplicant, Type
                FROM tbl_CTOS_Result WITH(NOLOCK)
                WHERE IsActive = 1 AND IsCoApplicant = 0 AND Request_Id = N'{0}' Order By ID
            ", secure.RC(rid));
            db.OpenTable(sql.ToString());
            if (db.RecordCount() > 0)
            {
                string type = db.Item("Type").ToString();

                if (type.Equals("CTOS"))
                    sqlString.OpenNewWindow_Center("/Form/Loan/CTOSReport.aspx?rid=" + sqlString.encryptURL(rid) + "&type=" + sqlString.encryptURL("0"), "DSR Report", 600, 800, this);
                if (type.Equals("RAMCI"))
                    sqlString.OpenNewWindow_Center("/Form/Loan/RAMCIReport.aspx?rid=" + sqlString.encryptURL(rid) + "&type=" + sqlString.encryptURL("0"), "DSR Report", 800, 800, this);
            }
        }
        protected void btnCoCCRIS_Click(object sender, EventArgs e)
        {
            string rid = hfRequestID.Value;

            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            sql.Clear();
            sql.AppendFormat(@" 
                SELECT ID, IsCoApplicant, Type
                FROM tbl_CTOS_Result WITH(NOLOCK)
                WHERE IsActive = 1 AND IsCoApplicant = 1 AND Request_Id = N'{0}' Order By ID
            ", secure.RC(rid));
            db.OpenTable(sql.ToString());
            if (db.RecordCount() > 0)
            {
                string type = db.Item("Type").ToString();

                if (type.Equals("CTOS"))
                    sqlString.OpenNewWindow_Center("/Form/Loan/CTOSReport.aspx?rid=" + sqlString.encryptURL(rid) + "&type=" + sqlString.encryptURL("1"), "DSR Report", 600, 800, this);
                if (type.Equals("RAMCI"))
                    sqlString.OpenNewWindow_Center("/Form/Loan/RAMCIReport.aspx?rid=" + sqlString.encryptURL(rid) + "&type=" + sqlString.encryptURL("1"), "DSR Report", 800, 800, this);
            }
        }
        #endregion

        protected void gvLoanEligible_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if(e.Row.RowType == DataControlRowType.DataRow)
            {
                //Loan Amount Eligible
                string loanAmountEligible = ConvertHelper.ConvertToDecimal(
                                                DataBinder.Eval(e.Row.DataItem, "LoanAmountEligible").ToString(), 0
                                            ).ToString("N0");
                TextBox gvtxtLoanEligible = (TextBox)e.Row.Cells[2].FindControl("gvtxtLoanEligible");

                gvtxtLoanEligible.Text = loanAmountEligible;

                //Selected
                string selected = DataBinder.Eval(e.Row.DataItem, "Selected").ToString();
                CheckBox chkSelected = (CheckBox)e.Row.Cells[3].FindControl("chkSelected");

                chkSelected.Checked = selected.Equals("True");
            }
        }
    }
}