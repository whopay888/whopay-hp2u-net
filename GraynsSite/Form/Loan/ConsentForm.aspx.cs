﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Util;
using Synergy.Helper;
using HJT.Cls.Helper;
using Newtonsoft.Json;
using HJT.Cls.Model;

namespace HJT.Form.Loan
{
    public partial class ConsentForm : System.Web.UI.Page
    {
        ConsentFormParams contentParams = new ConsentFormParams();
        string id = string.Empty;
        string rawHtmlContent = string.Empty;
        public bool isNewRequest = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            id = secure.Decrypt(Validation.GetUrlParameter(this.Page, "id", ""), true);
            //logic to control display of save button
            contentParams.MainApplicantIC = secure.Decrypt(Validation.GetUrlParameter(this.Page, "maid", ""), true);
            contentParams.MainApplicantName = secure.Decrypt(Validation.GetUrlParameter(this.Page, "man", ""), true);
            contentParams.CoApplicantIC = secure.Decrypt(Validation.GetUrlParameter(this.Page, "caid", ""), true);
            contentParams.CoApplicantName = secure.Decrypt(Validation.GetUrlParameter(this.Page, "can", ""), true);
            //validate on link button click to make sure ic & name is filled
            isNewRequest = (!string.IsNullOrWhiteSpace(contentParams.MainApplicantIC) || !string.IsNullOrWhiteSpace(contentParams.MainApplicantName) ||
                !string.IsNullOrWhiteSpace(contentParams.CoApplicantIC) || !string.IsNullOrWhiteSpace(contentParams.CoApplicantName));

            if (!IsPostBack)
            {
                if (!string.IsNullOrWhiteSpace(id))
                {
                    wwdb db = new wwdb();
                    StringBuilder sql = new StringBuilder();

                    sql.AppendFormat(@"SELECT * FROM tbl_ConsentForm WITH(NOLOCK) WHERE Id ='{0}'", id);
                    db.OpenTable(sql.ToString());
                    if (db.RecordCount() > 0)
                    {
                        DateTime signedDate = Convert.ToDateTime(db.Item("CreatedAt"));
                        string htmlContent = db.Item("HtmlContent");
                        string htmlParams = db.Item("Parameters");                      
                        ConsentFormParams savedParams = JsonConvert.DeserializeObject<ConsentFormParams>(htmlParams);

                        //replace
                        htmlContent = htmlContent.Replace("{SignedDate}", isNewRequest ? DateTime.Now.ToShortDateString() : signedDate.ToShortDateString());
                        htmlContent = htmlContent.Replace("{MainApplicantIC}", isNewRequest ? contentParams.MainApplicantIC : savedParams.MainApplicantIC);
                        htmlContent = htmlContent.Replace("{MainApplicantName}", isNewRequest ? contentParams.MainApplicantName : savedParams.MainApplicantName);
                        htmlContent = htmlContent.Replace("{MainApplicantSignature}", "<div id='masignature' class='signature'></div>");
                        htmlContent = htmlContent.Replace("{CoApplicantIC}", isNewRequest ? contentParams.CoApplicantIC : savedParams.CoApplicantIC);
                        htmlContent = htmlContent.Replace("{CoApplicantName}", isNewRequest ? contentParams.CoApplicantName : savedParams.CoApplicantName);
                        htmlContent = htmlContent.Replace("{CoApplicantSignature}", "<div id='casignature' class='signature'></div>");

                        ltrContent.Text = htmlContent;
                        hfMaSig.Value = savedParams.MainApplicantSignature;
                        hfCaSig.Value = savedParams.CoApplicantSignature;
                    }
                    else
                    {
                        sql.Clear();
                        sql.AppendFormat(@"SELECT TOP(1) Content FROM tbl_ConsentDocs WHERE (isDeleted = 0 OR isDeleted IS NULL) AND IsActive = 1 ORDER BY CreatedAt DESC");
                        db.OpenTable(sql.ToString());
                        if (db.RecordCount() > 0)
                        {
                            rawHtmlContent = db.Item("Content");
                            string htmlContent = db.Item("Content");
                            //replace
                            htmlContent = htmlContent.Replace("{SignedDate}", DateTime.Now.ToShortDateString());
                            htmlContent = htmlContent.Replace("{MainApplicantIC}", contentParams.MainApplicantIC);
                            htmlContent = htmlContent.Replace("{MainApplicantName}", contentParams.MainApplicantName);
                            htmlContent = htmlContent.Replace("{MainApplicantSignature}", "<div id='masignature' class='signature'></div>");
                            htmlContent = htmlContent.Replace("{CoApplicantIC}", contentParams.CoApplicantIC);
                            htmlContent = htmlContent.Replace("{CoApplicantName}", contentParams.CoApplicantName);
                            htmlContent = htmlContent.Replace("{CoApplicantSignature}", "<div id='casignature' class='signature'></div>");

                            ltrContent.Text = htmlContent;
                            hfMaSig.Value = contentParams.MainApplicantSignature;
                            hfCaSig.Value = contentParams.CoApplicantSignature;
                        }
                        else
                        {
                            ltrContent.Text = "Consent Form wasn't setup properly, please contact support.";
                            btnPrint.Visible = false;
                        }
                    }
                }

                Session["consentFormParams"] = contentParams;
                Session["consentFormRawHtmlContent"] = rawHtmlContent;
            }

            contentParams = (ConsentFormParams)Session["consentFormParams"];
            rawHtmlContent = Session["consentFormRawHtmlContent"].ToString();

            initControl();
        }

        private void initControl()
        {
            btnSave.Visible = isNewRequest;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!isNewRequest)
                return;

            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            contentParams.MainApplicantSignature = hfMaSig.Value;
            contentParams.CoApplicantSignature = hfCaSig.Value;

            sql.AppendFormat(@"SELECT * FROM tbl_ConsentForm WITH(NOLOCK) WHERE Id ='{0}'", id);
            db.OpenTable(sql.ToString());
            if (db.RecordCount() > 0)
            {
                sql.Clear();
                sql.AppendFormat(@"update tbl_ConsentForm set Parameters = N'{0}' WHERE Id = '{1}'", JsonConvert.SerializeObject(contentParams), id);
                db.Execute(sql.ToString());
            }
            else
            {
                sql.Clear();
                sql.AppendFormat(@"insert into tbl_ConsentForm(Id,HtmlContent,Parameters,CreatedAt)values('{0}',N'{1}',N'{2}', GETDATE()) ", id, rawHtmlContent, JsonConvert.SerializeObject(contentParams));
                db.Execute(sql.ToString());
            }

            ScriptManager.RegisterStartupScript(Page, typeof(Page), "closepage", "closePage();", true);
            //Response.Redirect(Request.RawUrl);
        }
    }
}