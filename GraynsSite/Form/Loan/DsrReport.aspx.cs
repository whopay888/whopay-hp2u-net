﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Util;
using Synergy.Helper;
using HJT.Cls.Helper;
using System.Globalization;
using System.Drawing;
using System.Web.UI.HtmlControls;
using HJT.Cls.Controller;
using Newtonsoft.Json.Linq;

namespace HJT.Form.Loan
{
    public partial class DsrReport : System.Web.UI.Page
    {
        List<string> applicantList = new List<string>();
        DataTable rawCommissionData = new DataTable();
        string requestID = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request["RID"] as string)
                    && !string.IsNullOrEmpty(Request["BankID"] as string))
                {
                    this.requestID = secure.Decrypt(Validation.GetUrlParameter(this.Page, "RID", ""), true);
                    string bankID = secure.Decrypt(Validation.GetUrlParameter(this.Page, "BankID", ""), true);

                    #region ReportExpirationCheck
                    //Check if report expired
                    wwdb db = new wwdb();
                    StringBuilder sql = new StringBuilder();

                    DateTime requestDate = DateTime.Now;
                    double expirationDay = 0;
                    sql.Clear();
                    sql.AppendFormat(@"
                        select top 1 p.ParameterValue as 'Day' from tbl_Parameter p with (nolock) 
                        where p.Category = 'ReportExpiration' and p.ParameterName = 'ReportExpirationDay'
                    ");
                    db.OpenTable(sql.ToString());
                    if (db.RecordCount() > 0)
                    {
                        expirationDay = double.Parse(db.Item("Day"));
                        sql.Clear();
                        sql.AppendFormat(@"
                            select r.RequestAt from tbl_Request r with (nolock) 
                            where r.RequestID = N'{0}'
                        ", requestID);
                        db.OpenTable(sql.ToString());
                        if (db.RecordCount() > 0)
                            requestDate = ConvertHelper.ConvertToDateTime(db.Item("RequestAt"), DateTime.Now);
                    }
                    #endregion
                    if (DateTime.Now < requestDate.AddDays(expirationDay))
                    {
                        if (requestID != string.Empty)
                        {
                            try
                            {
                                sql.Clear();
                                sql.AppendFormat(@"
                                    SELECT  b.Fullname, b.Mobile, d.Unit, d.ProjectName, c.Name AS 'Client1Name', c.myCard AS 'Client1ID', 
                                    c2.Name As 'Client2Name', c2.mycard AS 'Client2ID', d.Tenure, d.PurchasePrice, a.applicationresult, 
                                    br.ImageUrl, 
	                                    (
                                        SELECT 
                                            distinct '['+
                                            STUFF((
                                            SELECT ', ' +'{{ ""ID"" :""'+ cast(sp.ID as varchar) + '"", ""ImageURL"" :""' + sp.ImageURL +'""}}'
                                            FROM tbl_SponsorshipImage sp WITH (NOLOCK)
                                            WHERE sp.isdeleted = 0 AND sp.isActive = 1
										        AND (sp.[From] > GETDATE() AND sp.[To] < GETDATE()
											        OR (sp.[From] is null and sp.[To] is null))
                                            FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
                                            ,1,2,'')+']' AS SponsorshipList
                                        FROM tbl_SponsorshipImage sp WITH (NOLOCK)
                                        ) as 'SponsorImageURL'
                                    FROM (
	                                    SELECT * FROM tbl_Request WITH(NOLOCK) WHERE RequestID = '{0}'
                                    ) a 
                                    LEFT JOIN tbl_MemberInfo b WITH(NOLOCK) ON a.MemberID = b.MemberId 
                                    LEFT JOIN tbl_Applicants c WITH(NOLOCK) ON a.requestid = c.requestid AND c.ApplicantType='MA'
                                    LEFT JOIN tbl_Applicants c2 WITH(NOLOCK) ON a.requestid = c2.requestid AND c2.ApplicantType='CA'
                                    LEFT JOIN tbl_LoanLegalAction d WITH(NOLOCK) ON a.RequestID = d.RequestID 
                                    LEFT JOIN tbl_login l WITH(NOLOCK) ON b.memberid = l.login_id
                                    LEFT JOIN tbl_branch br WITH(NOLOCK) ON l.branchid = br.branchid
                                ", requestID);
                                //ORDER BY CASE WHEN c.ApplicantType ='MA' THEN '1' ELSE c.ApplicantType  END DESC  

                                DataTable ApplicantInfo = db.getDataTable(sql.ToString());

                                #region Profile
                                Dictionary<string, string> statusColor = new Dictionary<string, string>();

                                statusColor.Add(ApplicationResult.Approved, "#A3DAA3");
                                statusColor.Add(ApplicationResult.Modify, "#FFFF00");
                                statusColor.Add(ApplicationResult.Denied, "#FF9898");

                                Dictionary<string, string> statusColorName = new Dictionary<string, string>();

                                statusColorName.Add(ApplicationResult.Approved, "Green");
                                statusColorName.Add(ApplicationResult.Modify, "Yellow");
                                statusColorName.Add(ApplicationResult.Denied, "Red");
                                statusColorName.Add(string.Empty, "Red");

                                Dictionary<string, string> contentList = new Dictionary<string, string>();

                                contentList.Add(ApplicationResult.Approved, Resources.resource.Report_Message_Green);
                                contentList.Add(ApplicationResult.Modify, Resources.resource.Report_Message_Yellow);
                                contentList.Add(ApplicationResult.Denied, Resources.resource.Report_Message_Red);
                                contentList.Add(string.Empty, Resources.resource.Report_Message_Empty);

                                string message = string.Empty;
                                sql.Clear();
                                sql.Append("SELECT aca.Message FROM tbl_ApplicantCreditAdvisor aca WHERE aca.RequestID=N'" + requestID + "' and aca.isdeleted=0;");
                                db.OpenTable(sql.ToString());
                                if (db.RecordCount() > 0)
                                {
                                    ReportController reportController = new ReportController();
                                    message = db.Item("Message");
                                    string tempMessage = message.Replace("</br>", "").Replace("&gt;", "").Replace("&amp;", "&").Replace("&#x0D;", "");
                                    reportController.removeInvalidChar("", "", message);
                                    if //(!string.IsNullOrEmpty(message) && !string.IsNullOrWhiteSpace(message) && 
                                        (!string.IsNullOrEmpty(reportController.value3) && !string.IsNullOrWhiteSpace(reportController.value3)
                                        && !string.IsNullOrWhiteSpace(tempMessage))//)
                                        lblResultContent.Text = message;
                                }

                                string ResultStatus = ApplicantInfo.Rows[0]["applicationresult"].ToString();
                                divStatus.Attributes.Add("style", "background:" + statusColor[ResultStatus] + "; border-radius: 10px;padding:10px; margin-left:10px;");

                                imgTrafficLight.ImageUrl = "/Images/DsrReport/Traffic-Light-" + statusColorName[ResultStatus].ToString() + ".png";
                                if (string.IsNullOrEmpty(lblResultContent.Text) || string.IsNullOrWhiteSpace(lblResultContent.Text))
                                    lblResultContent.Text = contentList[ResultStatus].ToString().Replace("xxx", ApplicantInfo.Rows[0]["Fullname"].ToString());// ApplicantInfo.Rows[0]["Client1Name"].ToString().ShortenName());

                                imgLogo.ImageUrl = string.IsNullOrWhiteSpace(ApplicantInfo.Rows[0]["ImageUrl"].ToString()) ? "/Images/Company_Logo_Big.png" : ApplicantInfo.Rows[0]["ImageUrl"].ToString();
                                //lblUnit.Text = ApplicantInfo.Rows[0]["Unit"].ToString();
                                tdProfile.Attributes.Add("style", "background:" + statusColor[ResultStatus] + ";border-radius:8px;text-align: center");
                                lblProfile.Text = statusColorName[ResultStatus].ToString();
                                lblClient1.Text = ApplicantInfo.Rows[0]["Client1Name"].ToString().Remove(2).AppendICNumber(ApplicantInfo.Rows[0]["Client1ID"].ToString());
                                lblClient2.Text = string.IsNullOrWhiteSpace(ApplicantInfo.Rows[0]["Client2Name"].ToString()) ? "-NIL" : ApplicantInfo.Rows[0]["Client2Name"].ToString().Remove(2).AppendICNumber(ApplicantInfo.Rows[0]["Client2ID"].ToString());
                                lblProject.Text = ApplicantInfo.Rows[0]["ProjectName"].ToString();
                                lblPurchase.Text = ConvertHelper.ConvertToDecimal(ApplicantInfo.Rows[0]["PurchasePrice"].ToString(), 0).ToString("N0");
                                lblUser.Text = ApplicantInfo.Rows[0]["Fullname"].ToString() + " (" + ApplicantInfo.Rows[0]["Mobile"].ToString() + ")";
                                lblTenure.Text = ApplicantInfo.Rows[0]["Tenure"].ToString();
                                #endregion
                                #region Commitment
                                DataTable dtCommitment = new DataTable();

                                //dtCommitment = db.getDataTable(string.Format(@"
                                //;WITH cte AS
                                //(
                                //    SELECT   ApplicantType , LoanType  ,RequestID , SUM(repayment) AS 'total'  FROM tbl_loandetail WITH(NOLOCK) WHERE RequestID = '{0}' AND BankID='{1}'
                                //                                                                GROUP BY  ApplicantType ,LoanType ,RequestID
                                //)

                                //SELECT a.ApplicantType , b.ShortForm AS 'LoanType' , d.Name AS 'ApplicantName' , isnull(c.total,0) as total , b.Name  , b.IconImage FROM
                                //(SELECT ApplicantType , RequestID FROM cte GROUP BY ApplicantType , RequestID ) a
                                //LEFT JOIN tbl_LoanTypes b WITH(NOLOCK) ON 1=1
                                //LEFT JOIN cte c WITH(NOLOCK) ON c.LoanType =  b.ShortForm  AND a.ApplicantType = c.ApplicantType
                                //LEFT JOIN tbl_Applicants d WITH(NOLOCK) ON d.RequestID = a.RequestID AND d.ApplicantType = a.ApplicantType
                                //Order by b.sort asc
                                //", requestID, bankID));
                                dtCommitment = db.getDataTable(string.Format(@"
                                    ;WITH cte AS
                                    (
                                    SELECT  ApplicantType , LoanType ,RequestID , SUM(repayment) AS 'total'  
                                    FROM tbl_loandetail WITH(NOLOCK) WHERE RequestID = '{0}' AND BankID = '{1}'
                                    GROUP BY ApplicantType, LoanType, RequestID
                                    )  
 

                                    SELECT D.ApplicantType, b.ShortForm AS 'LoanType', d.Name AS 'ApplicantName', isnull(CTE.total,0) as total, b.Name, b.IconImage 
                                    FROM
                                    tbl_Applicants d 
                                    LEFT JOIN tbl_LoanTypes b WITH(NOLOCK) ON 1 = 1
                                    LEFT JOIN CTE ON D.RequestID = CTE.RequestID AND D.ApplicantType = CTE.ApplicantType  AND B.ShortForm = CTE.LoanType
                                    WHERE D.RequestID = '{0}' 
                                    Order by b.sort asc
                                ", requestID, bankID));
                                //dtCommitment = dab.getDataTable(string.Format(@"SELECT a.ApplicantType , a.LoanType , c.Name as 'ApplicantName' , a.total , b.Name  , b.IconImage FROM 
                                //(
                                //SELECT ApplicantType, LoanType, RequestID, SUM(Repayment) AS 'total'  FROM tbl_loandetail WITH(NOLOCK) WHERE RequestID = '{0}' AND BankID = '{1}'
                                //GROUP BY ApplicantType , LoanType ,RequestID
                                //)a
                                //LEFT JOIN tbl_LoanTypes b ON a.LoanType = b.ShortForm AND b.IsDeleted = 0
                                //LEFT JOIN tbl_Applicants c ON c.RequestID = a.RequestID AND c.ApplicantType = a.ApplicantType", requestID,bankID));

                                rawCommissionData = dtCommitment;
                                lblTotalCommitment.Text = ConvertHelper.ConvertToDecimal(dtCommitment.AsEnumerable().Sum(x => x.Field<decimal>("total")).ToString(), 0).ToString("N0");

                                var applicantRaw = from a in dtCommitment.AsEnumerable()
                                                   group a by new
                                                   {
                                                       applicant = a.Field<string>("ApplicantType"),
                                                       Name = a.Field<string>("ApplicantName").Remove(2)
                                                   } into g
                                                   select new { applicant = g.Key.applicant, Name = g.Key.Name }; //sqlString.changeLanguage("Report_ApplicantType_" + g.Key.applicant) + " " + 
                                applicantRaw = applicantRaw.OrderBy(x => x.applicant == "MA" ? 0 : 1);
                                foreach (var appl in applicantRaw)
                                {
                                    applicantList.Add(appl.applicant.ToString());
                                }

                                List<string> OthersLoanType = new List<string>();
                                OthersLoanType.Add("CL");
                                OthersLoanType.Add("OD");
                                OthersLoanType.Add("XP");

                                rptApplicants.DataSource = applicantRaw;
                                rptApplicants.DataBind();

                                #region Count Facilities
                                //db.OpenTable(string.Format(@" SELECT ApplicantType, 
                                //                                 CASE WHEN loantype = 'CL'
                                //                                  then 'OS'
                                //                                  WHEN loantype = 'OD'
                                //                                  THEN 'OS'
                                //                                  WHEN loantype = 'XP'
                                //                                  THEN 'OS'
                                //                                 ELSE loantype
                                //                                 END AS 'LoanType'
                                //                                FROM tbl_loandetail WITH(NOLOCK) 
                                //                                WHERE RequestID = '{0}' AND BankID='{1}'
                                //                                order by ApplicantType ", requestID, bankID));
                                //int HL = 0, CC = 0, PL = 0, HP = 0, OS = 0;
                                //if (db.RecordCount() > 0)
                                //{
                                //    while (!db.Eof())
                                //    {
                                //        switch (db.Item("LoanType"))
                                //        {
                                //            case "HL":
                                //                HL++;
                                //                break;
                                //            case "CC":
                                //                CC++;
                                //                break;
                                //            case "PL":
                                //                PL++;
                                //                break;
                                //            case "HP":
                                //                HP++;
                                //                break;
                                //            case "OS":
                                //                OS++;
                                //                break;
                                //            default: //error, should never reach
                                //                break;
                                //        }
                                //        db.MoveNext();
                                //    }
                                //}
                                #endregion

                                if (dtCommitment.AsEnumerable().Where(x => OthersLoanType.Contains(x.Field<string>("LoanType"))).ToList<DataRow>().Count > 0)
                                {
                                    dtCommitment.AsEnumerable().Where(x => OthersLoanType.Contains(x.Field<string>("LoanType"))).ToList<DataRow>().
                                        ForEach(r => { r["LoanType"] = "OS"; r["Name"] = "Others"; r["IconImage"] = "/Images/DsrReport/OS_Icon.png"; });

                                    //dtCommitment.AsEnumerable().Where(s => s.Field<string>("LoanType").Equals("HL")).ToList<DataRow>().
                                    //    ForEach(r => { r["Name"] = HL.ToString() + " * " + r["Name"]; });
                                    //dtCommitment.AsEnumerable().Where(s => s.Field<string>("LoanType").Equals("OS")).ToList<DataRow>().
                                    //    ForEach(r => { r["Name"] = OS.ToString() + " * " + r["Name"]; });
                                    //dtCommitment.AsEnumerable().Where(s => s.Field<string>("LoanType").Equals("HP")).ToList<DataRow>().
                                    //    ForEach(r => { r["Name"] = HP.ToString() + " * " + r["Name"]; });
                                    //dtCommitment.AsEnumerable().Where(s => s.Field<string>("LoanType").Equals("CC")).ToList<DataRow>().
                                    //    ForEach(r => { r["Name"] = CC.ToString() + " * " + r["Name"]; });
                                    //dtCommitment.AsEnumerable().Where(s => s.Field<string>("LoanType").Equals("PL")).ToList<DataRow>().
                                    //    ForEach(r => { r["Name"] = PL.ToString() + " * " + r["Name"]; });
                                }

                                dtCommitment = dtCommitment.AsEnumerable().OrderBy(t => t.Field<string>("LoanType") == "OS" ?
                                                     2 : 1).CopyToDataTable();

                                rptCommitment.DataSource = from a in dtCommitment.AsEnumerable()
                                                           group a by new
                                                           {
                                                               loantype = a.Field<string>("LoanType"),
                                                               loanname = a.Field<string>("Name"),
                                                               loanurl = a.Field<string>("IconImage")
                                                           }
                                                           into g
                                                           select new { g.Key.loantype, g.Key.loanname, g.Key.loanurl };
                                rptCommitment.DataBind();
                                #endregion
                                #region Advise
                                db.OpenTable("SELECT isTrue from tbl_PageController with (nolock) where Category = 'Advise Section' and Name = 'Visible'");
                                if (db.RecordCount() > 0)
                                {
                                    string visibleConf = db.Item("isTrue");
                                    string isFinal = string.Empty;
                                    db.OpenTable(
                                        string.Format(@"
                                            SELECT a.isFinal FROM tbl_ApplicantCreditAdvisor a with (nolock) where a.RequestID = N'{0}'
                                        ", requestID));
                                    if (db.RecordCount() > 0)
                                        isFinal = db.Item("isFinal");

                                    if (visibleConf.Equals("True") || isFinal.Equals("True"))
                                        divAdvise.Visible = true;
                                    else
                                        divAdvise.Visible = false;
                                }
                                DataTable advisorDT = db.getDataTable(
                                    string.Format(@"SELECT CASE WHEN b.Description IS NULL THEN a.Comment ELSE b.Description END AS 'DESC' 
                                        FROM (
                                        SELECT * FROM tbl_ApplicantCreditAdvisor WITH(NOLOCK) WHERE RequestID = '{0}' and IsDeleted = 0 
                                        ) a
                                        LEFT JOIN tbl_CreditAdvisor b WITH(NOLOCK) ON a.AdvisorID = b.AdvisorID AND Status = 'A'
                                        ORDER BY CASE WHEN b.Description is null THEN 0 ELSE 1 END DESC 
                                    ", requestID));

                                advisorDT.AsEnumerable().ToList<DataRow>().ForEach(x => x["DESC"] = x["DESC"].ToString().Replace("\n", "<br/>").Replace("&#x0D;", "<br/>").Replace("<br/><br/><br/><br/>", "<br/>"));

                                rptAdvise.DataSource = advisorDT;
                                rptAdvise.DataBind();
                                #endregion
                                #region LoanInfo
                                sql.Clear();
                                sql.AppendFormat(@"
                                    SELECT LoanAmount, LoanAmountEligible, NewLoanRepayment, IncomeRequired, dsr, DeclareIncome
                                    FROM tbl_LoanInfo WITH(NOLOCK) WHERE RequestID = '{0}' AND IsDeleted = 0 AND BankID = '{1}'
                                ", requestID, bankID);
                                db.OpenTable(sql.ToString());
                                if (db.RecordCount() > 0)
                                {
                                    lblDSR.Text = ConvertHelper.ConvertToDecimal(db.Item("dsr"), 0).ToString("N0");
                                    lblLoanAmount.Text = ConvertHelper.ConvertToDecimal(db.Item("LoanAmount"), 0).ToString("N0");
                                    lblRepayment.Text = ConvertHelper.ConvertToDecimal(db.Item("NewLoanRepayment"), 0).ToString("N0");
                                    lblIncome.Text = ConvertHelper.ConvertToDecimal(db.Item("IncomeRequired"), 0).ToString("N0");
                                    lblNett.Text = ConvertHelper.ConvertToDecimal(db.Item("DeclareIncome"), 0).ToString("N0");
                                    //if (ConvertHelper.ConvertToDecimal(db.Item("LoanAmountEligible"), 0) < 0)
                                    //    lblLoanEligible.Text = 0.ToString("N0");
                                    //else
                                    //    lblLoanEligible.Text = ConvertHelper.ConvertToDecimal(db.Item("LoanAmountEligible"), 0).ToString("N0");
                                }
                                #endregion
                                #region LoanEligible
                                //New-LoanEligible
                                DataTable dt = db.getDataTable(getLoanEligible());
                                rptLEBankName.DataSource = dt;
                                rptLEBankName.DataBind();
                                rptLEAmount.DataSource = dt;
                                rptLEAmount.DataBind();
                                if (dt.Rows.Count > 0)
                                    divLoanEligible.Visible = true;
                                #endregion
                                bindCTOSRepeater(requestID);
                                #region SponsorShip Image
                                string allSponsorshipImageURL = string.IsNullOrEmpty(ApplicantInfo.Rows[0]["SponsorImageURL"].ToString())
                                    ? "" : ApplicantInfo.Rows[0]["SponsorImageURL"].ToString();

                                if (!string.IsNullOrEmpty(allSponsorshipImageURL))
                                {
                                    var array = JArray.Parse(allSponsorshipImageURL);
                                    rptSponsorImages.DataSource = array;
                                    rptSponsorImages.DataBind();
                                    divSponsorshipImages.Visible = true;
                                }
                                #endregion
                            }
                            catch (Exception ex)
                            {
                                divMainDisplay.Visible = false;
                                divErrorReport.Visible = true;
                                LogUtil.logError(ex.Message, "Error Displaying DSR Report");
                            }
                        }
                    }
                    else
                    {
                        divMainDisplay.Visible = false;
                        divReportExpiration.Visible = true;
                    }
                }
                else
                {
                    divMainDisplay.Visible = false;
                    divErrorReport.Visible = true;
                }
            }
        }

        private void bindCTOSRepeater(string requestId)
        {
            wwdb db = new wwdb();
            int resultId;
            StringBuilder sql = new StringBuilder();
            sql.Clear();
            sql.Append(" SELECT ID,IsCoApplicant ");
            sql.Append(" FROM tbl_CTOS_Result WITH(NOLOCK) ");
            sql.Append(" WHERE IsActive = 1 AND Request_Id = N'" + secure.RC(requestId) + "' Order By ID");
            db.OpenTable(sql.ToString());

            if (db.RecordCount() > 0)
            {
                for (int i = 0; i < db.RecordCount(); i++)
                {
                    bool IsCoApplicant;
                    resultId = int.Parse(db.Item("ID").ToString());
                    IsCoApplicant = Convert.ToBoolean(db.Item("IsCoApplicant"));

                    DataTable dt, SAAdt;
                    string color = "#FFCCFF";
                    string PTPTNcolor = "#4EC573";
                    string AKPKcolor = "#FFFF38";
                    StringBuilder CCRISSsql = new StringBuilder();
                    string SAAsql = " AND ([Status] like '%SAA%') ORDER BY sortorder ";
                    
                    CCRISSsql.Clear();
                    CCRISSsql.AppendFormat(@"
                        SELECT ID,CTOS_Result_Id, [No], 
                        CASE WHEN Date_R_R IS NULL THEN NULL ELSE REPLACE(CONVERT(varchar, Date_R_R,101), '-', '/') END AS Date_R_R, 
                        Sts, Capacity, Lender_Type, 
                            CASE when [Status] = 'PTPTN' then 'PTPTN'
                                When [Col_Type] ='23' then 'Unit Trusts'
                                When [Col_Type] ='10' then 'Property'
                                When [Facility] ='PCPASCAR' then 'Car Loan'
                                When [Facility] ='OHRPCREC' then 'Car Loan'
                                When [Facility] ='CRDTCARD' then 'Credit Card' 
                                When [Facility] ='CHRGCARD' then 'Charge Card' 
                                When [Facility] ='PELNFNCE' then 'Personal Loan' 
                                When [Facility] ='HSLNFNCE' then 'Housing Loan' 
                                When [Facility] ='HLFNNNPS' then 'Housing Loan' 
                                When [Facility] ='SHMARGIN' then 'Share Margin' 
                                When [Facility] ='ISPWNBKG' then 'Pawn Broking' 
                                When [Facility] ='OVRDRAFT' then 'Over Draft' 
                                When [Facility] ='TEMPOVDT' then 'Temp O.Draft' 
                                When [Facility] ='STLNFNCE' then 'Staff Loan' 
                                when ([Facility] ='OTLNFNCE' And [Col_Type]='10') then 'Commercial'
                                When [Facility] ='OTLNFNCE' then 'Term Loan'                                 
                            ELSE [Facility] end as [Facility], 
                        Total_Outstanding_Balance,	
                        CASE WHEN Date_Balance_Updated IS NULL THEN NULL ELSE REPLACE(CONVERT(varchar, Date_Balance_Updated, 101), '-', '/') END AS Date_Balance_Updated,	
                        Limit_Installment_Amount, Prin_Repmt_Term, Col_Type, LGL_STS,	
                        CASE WHEN Date_Status_Updated IS NULL THEN NULL ELSE REPLACE(CONVERT(varchar, Date_Status_Updated, 101), '-', '/') END AS Date_Status_Updated,	
                        IsShowinReport, 
                            CASE WHEN [Status] = 'PTPTN'   THEN CAST('{0}' AS varchar(8)) 
						        WHEN [Status] = 'AKPK'         THEN CAST('{3}' AS varchar(8)) 
                                WHEN [STS] = 'C'               THEN CAST('{3}' AS varchar(8)) 
                                WHEN [STS] = 'T'               THEN CAST('{3}' AS varchar(8)) 
						    ELSE CAST(NULL AS VARCHAR(2)) END as 'FacilityColor',	
						    CASE WHEN [Status] = 'AKPK' THEN CAST('{3}' AS varchar(8)) 
                                WHEN [STS] = 'C'               THEN CAST('{3}' AS varchar(8)) 
                                WHEN [STS] = 'T'               THEN CAST('{3}' AS varchar(8)) 
                            ELSE CAST(NULL AS VARCHAR(2)) END as 'StsColor',    	
                        CASE WHEN M12_Count IS NULL THEN CAST('.' AS varchar(2)) ELSE CAST(M12_Count as VARCHAR(3)) END as M12_Count,	
                        CASE WHEN M12_Count IS NOT NULL AND M12_Count > 0 THEN CAST('{1}' AS varchar(8)) ELSE CAST(NULL AS VARCHAR(2)) END as M12_Color,	
                        CASE WHEN M11_Count IS NULL THEN CAST(' ' AS varchar(2)) ELSE CAST(M11_Count as VARCHAR(3)) END as M11_Count,	
                        CASE WHEN M11_Count IS NOT NULL AND M11_Count > 0 THEN CAST('{1}' AS varchar(8)) ELSE CAST(NULL AS VARCHAR(2)) END as M11_Color,	
                        CASE WHEN M10_Count IS NULL THEN CAST(' ' AS varchar(2)) ELSE CAST(M10_Count as VARCHAR(3)) END as M10_Count,	
                        CASE WHEN M10_Count IS NOT NULL AND M10_Count > 0 THEN CAST('{1}' AS varchar(8)) ELSE CAST(NULL AS VARCHAR(2)) END as M10_Color,	
                        CASE WHEN M9_Count IS NULL THEN CAST(' ' AS varchar(2)) ELSE CAST(M9_Count as VARCHAR(3)) END as M9_Count,	
                        CASE WHEN M9_Count IS NOT NULL AND M9_Count > 0 THEN CAST('{1}' AS varchar(8)) ELSE CAST(NULL AS VARCHAR(2)) END as M9_Color,	
                        CASE WHEN M8_Count IS NULL THEN CAST(' ' AS varchar(2)) ELSE CAST(M8_Count as VARCHAR(3)) END as M8_Count,	
                        CASE WHEN M8_Count IS NOT NULL AND M8_Count > 0 THEN CAST('{1}' AS varchar(8)) ELSE CAST(NULL AS VARCHAR(2)) END as M8_Color,	
                        CASE WHEN M7_Count IS NULL THEN CAST(' ' AS varchar(2)) ELSE CAST(M7_Count as VARCHAR(3)) END as M7_Count,	
                        CASE WHEN M7_Count IS NOT NULL AND M7_Count > 0 THEN CAST('{1}' AS varchar(8)) ELSE CAST(NULL AS VARCHAR(2)) END as M7_Color,	
                        CASE WHEN M6_Count IS NULL THEN CAST(' ' AS varchar(2)) ELSE CAST(M6_Count as VARCHAR(3)) END as M6_Count,	
                        CASE WHEN M6_Count IS NOT NULL AND M6_Count > 0 THEN CAST('{1}' AS varchar(8)) ELSE CAST(NULL AS VARCHAR(2)) END as M6_Color,	
                        CASE WHEN M5_Count IS NULL THEN CAST(' ' AS varchar(2)) ELSE CAST(M5_Count as VARCHAR(3)) END as M5_Count,	
                        CASE WHEN M5_Count IS NOT NULL AND M5_Count > 0 THEN CAST('{1}' AS varchar(8)) ELSE CAST(NULL AS VARCHAR(2)) END as M5_Color,	
                        CASE WHEN M4_Count IS NULL THEN CAST(' ' AS varchar(2)) ELSE CAST(M4_Count as VARCHAR(3)) END as M4_Count,	
                        CASE WHEN M4_Count IS NOT NULL AND M4_Count > 0 THEN CAST('{1}' AS varchar(8)) ELSE CAST(NULL AS VARCHAR(2)) END as M4_Color,	
                        CASE WHEN M3_Count IS NULL THEN CAST(' ' AS varchar(2)) ELSE CAST(M3_Count as VARCHAR(3)) END as M3_Count,	
                        CASE WHEN M3_Count IS NOT NULL AND M3_Count > 0 THEN CAST('{1}' AS varchar(8)) ELSE CAST(NULL AS VARCHAR(2)) END as M3_Color,	
                        CASE WHEN M2_Count IS NULL THEN CAST(' ' AS varchar(2)) ELSE CAST(M2_Count as VARCHAR(3)) END as M2_Count,	
                        CASE WHEN M2_Count IS NOT NULL AND M2_Count > 0 THEN CAST('{1}' AS varchar(8)) ELSE CAST(NULL AS VARCHAR(2)) END as M2_Color,	
                        CASE WHEN M1_Count IS NULL THEN CAST(' ' AS varchar(2)) ELSE CAST(M1_Count as VARCHAR(3)) END as M1_Count,	
                        CASE WHEN M1_Count IS NOT NULL AND M1_Count > 0 THEN CAST('{1}' AS varchar(8)) ELSE CAST(NULL AS VARCHAR(2)) END as M1_Color	
                        FROM tbl_CTOS_Report_Loan_Details WHERE CTOS_Result_Id = '{2}' AND IsShowInReport = 1 
                    ", PTPTNcolor, color, resultId, AKPKcolor);
                    SAAsql = CCRISSsql.ToString() + SAAsql;
                    CCRISSsql.Append(" AND ([Status] is null or [Status] NOT LIKE '%SAA%') ORDER BY sortorder ");
                    dt = db.getDataTable(CCRISSsql.ToString());
                    SAAdt = db.getDataTable(SAAsql);
                    
                    if (IsCoApplicant == false)
                    {
                        rptLoanDetails.DataSource = dt;
                        rptLoanDetails.DataBind();
                        if (dt.Rows.Count > 0)
                        {
                            ctosTable.Visible = true;
                            ctosTable_MA.Visible = true;
                        }
                        rptLoanDetailsSAA.DataSource = SAAdt;
                        rptLoanDetailsSAA.DataBind();
                        if (SAAdt.Rows.Count > 0)
                        {
                            ctosTable.Visible = true;
                            ctosTable_MA.Visible = true;
                            ctosTable_MA_SAA.Visible = true;
                        }
                    }
                    else
                    {
                        rptCoLoanDetails.DataSource = dt;
                        rptCoLoanDetails.DataBind();
                        if (dt.Rows.Count > 0)
                        {
                            ctosTable.Visible = true;
                            ctosTable_CA.Visible = true;
                        }
                        rptCoLoanDetailsSAA.DataSource = SAAdt;
                        rptCoLoanDetailsSAA.DataBind();
                        if (SAAdt.Rows.Count > 0)
                        {
                            ctosTable.Visible = true;
                            ctosTable_CA.Visible = true;
                            ctosTable_CA_SAA.Visible = true;
                        }
                    }
                    
                    db.MoveNext();
                }
            }
        }
        public Color ConvertFromHexToColor(string hex)
        {
            Color clr;
            string colorcode = "#FFFFFF";
            if (!string.IsNullOrEmpty(hex) && !string.IsNullOrWhiteSpace(hex))
                colorcode = hex;
            int argb = Int32.Parse(colorcode.Replace("#", ""), NumberStyles.HexNumber);
            clr = Color.FromArgb(argb);
            return clr;
        }

        protected void rptCommitment_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string loantype = DataBinder.Eval(e.Item.DataItem, "loantype").ToString();
                string LoanName = DataBinder.Eval(e.Item.DataItem, "loanname").ToString();
                string LoanUrl = DataBinder.Eval(e.Item.DataItem, "loanurl").ToString();
                var imgCommitment = (System.Web.UI.WebControls.Image)e.Item.FindControl("imgCommitment");
                Label lblCommission = (Label)e.Item.FindControl("lblCommission");
                Label lblTotal = (Label)e.Item.FindControl("lblTotal");
                
                imgCommitment.ImageUrl = LoanUrl;
                lblCommission.Text = LoanName;

                #region Count Facilities
                wwdb db = new wwdb();
                db.OpenTable(string.Format(@"with cte as (SELECT ID, IsCoApplicant
	                                            from [tbl_CTOS_Result] cr with (nolock)
	                                            where Request_Id = '{0}' and IsActive = 1)
                                            select crld.ID, crld.[No ],
	                                            CASE WHEN lt.ShortForm = 'CL'
		                                            then 'OS'
		                                            WHEN lt.ShortForm = 'OD'
		                                            THEN 'OS'
		                                            WHEN lt.ShortForm = 'XP'
		                                            THEN 'OS'
	                                            ELSE lt.ShortForm
	                                            END AS 'LoanType', ~cte.IsCoApplicant as 'isMain'
                                            from cte, tbl_CTOS_Report_Loan_Details crld with (nolock) left join
                                            tbl_LoanTypesXML ltx with (nolock) on ltx.LoanTypeCode=crld.Facility left join
                                            tbl_LoanTypes lt with (nolock) on lt.LoanID=ltx.LoanID
                                            where CTOS_Result_Id in (cte.ID)
                                            order by crld.ID, sortOrder", requestID));
                int mainHL = 0, mainCC = 0, mainPL = 0, mainHP = 0, mainOS = 0;
                int coHL = 0, coCC = 0, coPL = 0, coHP = 0, coOS = 0;
                bool count = false;
                if (db.RecordCount() > 0)
                {
                    while (!db.Eof())
                    {
                        if (db.Item("isMain").Equals("True"))
                        {
                            if (string.IsNullOrEmpty(db.Item("LoanType")))
                            {
                                count = true;
                            }
                            else if (count)
                            {
                                switch (db.Item("LoanType"))
                                {
                                    case "HL":
                                        mainHL++;
                                        break;
                                    case "CC":
                                        mainCC++;
                                        break;
                                    case "PL":
                                        mainPL++;
                                        break;
                                    case "HP":
                                        mainHP++;
                                        break;
                                    case "OS":
                                        mainOS++;
                                        break;
                                    default: //error, should never reach
                                        break;
                                }
                                count = false;
                            }
                            db.MoveNext();
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(db.Item("LoanType")))
                            {
                                count = true;
                            }
                            else if (count)
                            {
                                switch (db.Item("LoanType"))
                                {
                                    case "HL":
                                        coHL++;
                                        break;
                                    case "CC":
                                        coCC++;
                                        break;
                                    case "PL":
                                        coPL++;
                                        break;
                                    case "HP":
                                        coHP++;
                                        break;
                                    case "OS":
                                        coOS++;
                                        break;
                                    default: //error, should never reach
                                        break;
                                }
                                count = false;
                            }
                            db.MoveNext();
                        }
                    }
                }
                Dictionary<string, int> countList = new Dictionary<string, int>();
                countList.Add("MAHL", mainHL);
                countList.Add("MACC", mainCC);
                countList.Add("MAPL", mainPL);
                countList.Add("MAHP", mainHP);
                countList.Add("MAOS", mainOS);
                countList.Add("CAHL", coHL);
                countList.Add("CACC", coCC);
                countList.Add("CAPL", coPL);
                countList.Add("CAHP", coHP);
                countList.Add("CAOS", coOS);
                #endregion

                foreach (string applicantID in applicantList)
                {
                    string totalvalues = rawCommissionData.AsEnumerable().Where(x => x.Field<string>("ApplicantType") == applicantID 
                        && x.Field<string>("LoanType") == loantype).Sum(x => x.Field<decimal>("total")).ToString("N0");
                    lblTotal.Text += "<p>" + totalvalues + " (" + countList[applicantID+loantype].ToString() + ")" + "</p>";
                }

            }
        }

        protected void rptLoanDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
            {
                if (e.Item.FindControl("litStartYear") != null)
                {
                    wwdb db = new wwdb();
                    StringBuilder sql = new StringBuilder();
                    sql.Clear();
                    sql.Append(" SELECT GeneratedDate ");
                    sql.Append(" FROM tbl_CTOS_Result WITH(NOLOCK) ");
                    sql.Append(" WHERE IsActive = 1 AND Request_Id = N'" + secure.RC(requestID) + "' AND IsCoApplicant =0");
                    db.OpenTable(sql.ToString());
                    DateTime GeneratedDate = DateTime.Now;
                    if (db.RecordCount() > 0)
                    {
                        GeneratedDate = Convert.ToDateTime(db.Item("GeneratedDate").ToString());
                    }

                    DateTime date = GeneratedDate;
                    int StartYear = date.Year;
                    int EndYear = date.AddMonths(-11).Year;
                    Literal ltrStartYear = e.Item.FindControl("litStartYear") as Literal;
                    ltrStartYear.Text = StartYear.ToString();
                    Literal litEndYear = e.Item.FindControl("litEndYear") as Literal;
                    litEndYear.Text = EndYear.ToString();
                    for (int i = 0; i < 12; i++)
                    {
                        string Month = date.AddMonths(-i).ToString("MMM");
                        if (i == 0)
                        {
                            Literal lbl12thMonth = e.Item.FindControl("lbl12thMonth") as Literal;
                            lbl12thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 1)
                        {
                            Literal lbl11thMonth = e.Item.FindControl("lbl11thMonth") as Literal;
                            lbl11thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 2)
                        {
                            Literal lbl10thMonth = e.Item.FindControl("lbl10thMonth") as Literal;
                            lbl10thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 3)
                        {
                            Literal lbl9thMonth = e.Item.FindControl("lbl9thMonth") as Literal;
                            lbl9thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 4)
                        {
                            Literal lbl8thMonth = e.Item.FindControl("lbl8thMonth") as Literal;
                            lbl8thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 5)
                        {
                            Literal lbl7thMonth = e.Item.FindControl("lbl7thMonth") as Literal;
                            lbl7thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 6)
                        {
                            Literal lbl6thMonth = e.Item.FindControl("lbl6thMonth") as Literal;
                            lbl6thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 7)
                        {
                            Literal lbl5thMonth = e.Item.FindControl("lbl5thMonth") as Literal;
                            lbl5thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 8)
                        {
                            Literal lbl4thMonth = e.Item.FindControl("lbl4thMonth") as Literal;
                            lbl4thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 9)
                        {
                            Literal lbl3thMonth = e.Item.FindControl("lbl3thMonth") as Literal;
                            lbl3thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 10)
                        {
                            Literal lbl2thMonth = e.Item.FindControl("lbl2thMonth") as Literal;
                            lbl2thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 11)
                        {
                            Literal lbl1thMonth = e.Item.FindControl("lbl1thMonth") as Literal;
                            lbl1thMonth.Text = Month.Substring(0, 1);
                        }
                    }
                }
            }
            else if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

            }
        }

        protected void rptCoLoanDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
            {
                if (e.Item.FindControl("litStartYear") != null)
                {
                    wwdb db = new wwdb();
                    StringBuilder sql = new StringBuilder();
                    sql.Clear();
                    sql.Append(" SELECT GeneratedDate ");
                    sql.Append(" FROM tbl_CTOS_Result WITH(NOLOCK) ");
                    sql.Append(" WHERE IsActive = 1 AND Request_Id = N'" + secure.RC(requestID) + "' AND IsCoApplicant =1");
                    db.OpenTable(sql.ToString());
                    DateTime GeneratedDate = DateTime.Now;
                    if (db.RecordCount() > 0)
                    {
                        GeneratedDate = Convert.ToDateTime(db.Item("GeneratedDate").ToString());
                    }

                    DateTime date = GeneratedDate;
                    int StartYear = date.Year;
                    int EndYear = date.AddMonths(-11).Year;
                    Literal ltrStartYear = e.Item.FindControl("litStartYear") as Literal;
                    ltrStartYear.Text = StartYear.ToString();
                    Literal litEndYear = e.Item.FindControl("litEndYear") as Literal;
                    litEndYear.Text = EndYear.ToString();
                    for (int i = 0; i < 12; i++)
                    {
                        string Month = date.AddMonths(-i).ToString("MMM");
                        if (i == 0)
                        {
                            Literal lbl12thMonth = e.Item.FindControl("lbl12thMonth") as Literal;
                            lbl12thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 1)
                        {
                            Literal lbl11thMonth = e.Item.FindControl("lbl11thMonth") as Literal;
                            lbl11thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 2)
                        {
                            Literal lbl10thMonth = e.Item.FindControl("lbl10thMonth") as Literal;
                            lbl10thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 3)
                        {
                            Literal lbl9thMonth = e.Item.FindControl("lbl9thMonth") as Literal;
                            lbl9thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 4)
                        {
                            Literal lbl8thMonth = e.Item.FindControl("lbl8thMonth") as Literal;
                            lbl8thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 5)
                        {
                            Literal lbl7thMonth = e.Item.FindControl("lbl7thMonth") as Literal;
                            lbl7thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 6)
                        {
                            Literal lbl6thMonth = e.Item.FindControl("lbl6thMonth") as Literal;
                            lbl6thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 7)
                        {
                            Literal lbl5thMonth = e.Item.FindControl("lbl5thMonth") as Literal;
                            lbl5thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 8)
                        {
                            Literal lbl4thMonth = e.Item.FindControl("lbl4thMonth") as Literal;
                            lbl4thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 9)
                        {
                            Literal lbl3thMonth = e.Item.FindControl("lbl3thMonth") as Literal;
                            lbl3thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 10)
                        {
                            Literal lbl2thMonth = e.Item.FindControl("lbl2thMonth") as Literal;
                            lbl2thMonth.Text = Month.Substring(0, 1);
                        }
                        else if (i == 11)
                        {
                            Literal lbl1thMonth = e.Item.FindControl("lbl1thMonth") as Literal;
                            lbl1thMonth.Text = Month.Substring(0, 1);
                        }
                    }
                }
            }
            else if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

            }
        }

        private string getLoanEligible()
        {
            StringBuilder sql = new StringBuilder();

            sql.Clear();
            sql.AppendFormat(@"
                SELECT bn.BankID, bn.BankName as 'LEBankName',
                    case WHEN isnull(li.LoanAmountEligible, tli.LoanAmountEligible) < 0
	                    THEN '0'
	                    ELSE isnull(cast(isnull(li.LoanAmountEligible, tli.LoanAmountEligible) as varchar), 'N/A')
                    END as 'LEAmount'
                FROM tbl_BankName bn with (nolock) left join
                    tbl_LoanInfo li WITH (NOLOCK) on li.BankID = bn.BankID and li.RequestID = '{0}' left join
                    tbl_tloaninfo tli with (nolock) on tli.BankID = bn.BankID and tli.RequestID = '{0}'
                WHERE bn.IsSelectedDefault = 1 AND bn.IsDeleted = 0	
                order by cast(bn.BankID as int) desc
            ", requestID);

            return sql.ToString();
        }
        protected void rptLEBankName_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

            }
        }

        protected void rptLEAmount_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string LEamount = 
                    Math.Round(
                        ConvertHelper.ConvertToDecimal(
                            DataBinder.Eval(e.Item.DataItem, "LEAmount").ToString()
                        ,0)
                    ).ToString("N0");

                try
                {
                    Label lblLoanEligible = (Label)e.Item.FindControl("lblLoanEligible");
                    if (lblLoanEligible != null)
                        lblLoanEligible.Text = LEamount;
                }catch(Exception ex) { LogUtil.logError(ex.Message, "Error finding lblLoanEligible in rptLEAmount"); }
            }
        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.requestID))
                Response.Redirect(string.Format(@"/Form/MemberRegistration.aspx?rid={0}", sqlString.encryptURL(this.requestID)));
            else
            {
                string requestID = secure.Decrypt(Validation.GetUrlParameter(this.Page, "RID", ""), true);
                Response.Redirect(string.Format(@"/Form/MemberRegistration.aspx?rid={0}", sqlString.encryptURL(requestID)));
            }
        }
    }
}