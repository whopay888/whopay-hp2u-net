﻿using HJT.Cls.Controller;
using HJT.Cls.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Controller;
using Synergy.Helper;
using Synergy.Model;
using Synergy.Util;

namespace HJT.Form.Loan
{
    public partial class NewRequest : Page
    {
        string consentId = string.Empty;
        List<string> uploadImages = new List<string>();
        LoginUserModel login_user = null;
        ReportController rc = new ReportController();
        protected void Page_Load(object sender, EventArgs e)
        {
            login_user = (LoginUserModel)Session[KeyVal.loginsecure];
            if (!IsPostBack)
            {
                checkCredit();
                if (login_user.isTestUser())
                {
                    btnSend.Attributes.Remove("OnClientClick");
                    divUploadDoc.Visible = false;
                    lblIC.Text = Resources.resource.Total_Commitment;
                }
                else
                    lblIC.Text = Resources.resource.IC;
                consentId = Guid.NewGuid().ToString();
                txtRemark.Attributes.Add("placeholder", Resources.resource.RemarkHint);
                txtStatus.Attributes.Add("placeholder", Resources.resource.StatusHint);
                bindData();
                Session["uploadImages"] = uploadImages;
                Session["consentguid"] = consentId;
            }

            consentId = Session["consentguid"].ToString();
            uploadImages = (List<string>)Session["uploadImages"];
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "ddlHaveCoApplicantChanged();", true);
        }

        private void checkCredit()
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            sql.AppendFormat(@"
                SELECT mi.MemberId, mi.Username, mi.Fullname, b.BranchName, 
	                case when bs.PerRequestPrice is null
		                then (SELECT PerRequestPrice FROM tbl_BillingSetting WHERE IsActive = 1 AND BranchId = 0)
		                else bs.PerRequestPrice
	                end as 'PerRequestPrice', 
	                case when bs.PerSuccessfulCasePrice is null
		                then (SELECT PerSuccessfulCasePrice FROM tbl_BillingSetting WHERE IsActive = 1 AND BranchId = 0)
		                else bs.PerSuccessfulCasePrice
	                end as 'PerSuccessfulCasePrice', 
	                case when bs.CreditLimit is null
		                then (SELECT CreditLimit FROM tbl_BillingSetting WHERE IsActive = 1 AND BranchId = 0)
		                else bs.CreditLimit
	                end as 'CreditLimit', 
	                case when ucb.CreditUsed is null
		                then '0'
		                else ucb.CreditUsed
	                end as 'CreditUsed',
	                case when ucb.FreeRequestCount is null
		                then '0'
		                else ucb.FreeRequestCount
	                end as 'FreeRequestCount'
                FROM tbl_BillingSetting bs inner join
                tbl_Branch b on bs.BranchId=b.BranchID inner join
                tbl_Login l on b.BranchID = l.BranchID inner join
                tbl_UserCreditBalance ucb on l.login_id=ucb.MemberId right join
                tbl_MemberInfo mi on mi.MemberId = l.login_id
                where mi.MemberId = '{0}'", login_user.UserId);

            db.OpenTable(sql.ToString());
            if (db.RecordCount() > 0)
            {
                string _reqPrice = db.Item("PerRequestPrice");
                string _successPrice = db.Item("PerSuccessfulCasePrice");
                string _creditLimit = db.Item("CreditLimit");
                string _creditUsed = db.Item("CreditUsed");
                string _freeReq = db.Item("FreeRequestCount");
                if (!string.IsNullOrEmpty(_reqPrice) && !string.IsNullOrEmpty(_successPrice) && !string.IsNullOrEmpty(_creditLimit)
                    && !string.IsNullOrEmpty(_creditUsed) && Math.Abs(ConvertHelper.ConvertToDecimal(_creditUsed, 0)) > 0)
                {
                    decimal reqPrice = ConvertHelper.ConvertToDecimal(_reqPrice, 0);
                    //decimal successPrice = ConvertHelper.ConvertToDecimal(_successPrice, 0);
                    decimal creditLimit = ConvertHelper.ConvertToDecimal(_creditLimit, 0);
                    decimal creditBalance = ConvertHelper.ConvertToDecimal(_creditUsed, 0);
                    decimal freeReq = ConvertHelper.ConvertToDecimal(_freeReq, 0);

                    //if (freeReq <= 0) //no free request left
                    //if (creditLimit - Math.Abs(creditUsed) <= reqPrice * 2) //close to used up to limit //ERROR
                    if (((-creditLimit) >= creditBalance) || (-creditLimit) >= (creditBalance - (reqPrice * 2))) //Insufficient balance OR close to used up to limit
                        sqlString.displayAlert(this, Resources.resource.CreditNearLimit +
                            "\\n Credit left : " + (creditLimit - Math.Abs(creditBalance)) +
                            "\\n Free Request left : " + freeReq);
                }
            }
        }

        private void bindData()
        {
            wwdb db = new wwdb();
            int branchId = 0;
            int parentID = 0;
            sqlString.bindControl(rblReport, "SELECT ParameterName as '0' , ParameterValue as '1' FROM tbl_parameter WITH (NOLOCK) WHERE category='ReportType'", "0", "1");
            rblReport.SelectedIndex = 0;
            if (login_user.isSpeedUser())
            {
                //sqlString.bindControl(rblReportType, "SELECT ParameterName as '0' , ParameterValue as '1' FROM tbl_parameter WITH (NOLOCK) WHERE category='Report' order by [0]", "0", "1");
                //rblReportType.SelectedIndex = 0;
                //divReportType.Visible = true;
            }

            StringBuilder sql = new StringBuilder();
            sql.Clear();
            sql.AppendFormat(@"
                SELECT tb.ID FROM dbo.tbl_MemberInfo tmi INNER JOIN dbo.tbl_Login tl ON tmi.MemberId = tl.login_id INNER JOIN dbo.tbl_Branch tb ON tl.BranchID = tb.BranchID
                INNER JOIN 
                    (SELECT DISTINCT trm.MenuID, '{0}' AS MemberId 
                    FROM dbo.tbl_RoleMenu trm 
                    WHERE trm.RoleID IN 
                        (SELECT RoleCode 
                        FROM tbl_MemberRole 
                        WHERE MemberId = '{0}')) 
                    AS trm ON tmi.MemberId = trm.MemberId
                INNER JOIN dbo.tbl_Menu tm ON trm.MenuID = tm.rowID
                WHERE tmi.MemberId = '{0}' AND tm.[Type] = 2 AND tm.[Key] = 'function_project_restrict_branch'
            ",login_user.UserId);
            db.OpenTable(sql.ToString());

            StringBuilder branchQuery = new StringBuilder();
            branchQuery.Append("select ProjectName as '0', ID as '1' from tbl_ProjectSettings WHERE status = 'A' AND IsDeleted='false' ");
            if (login_user.isStaffAdmin())
                branchQuery.Append("order by [0] ");
            else
            {
                if (db.RecordCount() > 0)//UAC is checked
                {
                    branchId = ConvertHelper.ConvertToInt(db.Item("ID").ToString(), 0);
                    branchQuery.Append(" AND BranchId IN (SELECT ID FROM tbl_Branch Where Id = " + branchId + ") ORDER BY ProjectName");
                }
                else//UAC unchecked
                {
                    sql.Clear();
                    sql.AppendFormat(@"
                        select b.ID, b.parentID
                        from tbl_MemberInfo mi
                        inner join tbl_Login l on mi.MemberId = l.login_id
                        inner join tbl_Branch b on l.BranchID = b.BranchID
                        where mi.MemberId = '{0}' 
                    ", login_user.UserId);
                    db.OpenTable(sql.ToString());
                    branchId = ConvertHelper.ConvertToInt(db.Item("ID").ToString(), 0);
                    parentID = ConvertHelper.ConvertToInt(db.Item("parentID").ToString(), 0);
                    if (parentID.Equals(0))//parent
                        branchQuery.AppendFormat(@" 
                            AND BranchId IN 
                                (SELECT ID FROM tbl_Branch 
                                Where ParentId = {0} OR Id = {0}) 
                            ORDER BY ProjectName
                        ", branchId);
                    else//sub
                        branchQuery.AppendFormat(@" 
                            AND BranchId IN
                                (SELECT p.ID FROM tbl_Branch s, tbl_Branch p
                                Where (s.ParentId = {0} OR p.Id = {0}) or (s.ParentId = p.Id and s.ID = {0}))
                            ORDER BY ProjectName
                        ", branchId);
                }
            }

            //project sorting
            sqlString.bindControl(ddlProject, branchQuery.ToString(), "0", "1", true);
            ddlProject.Items.Insert(ddlProject.Items.Count, "Others");

            //Get Price Details
            lblpricedetails.Text = Resources.resource.Request_PriceDetails;
            if (login_user.BranchID.Trim() != string.Empty)
            {
                sql.Clear();
                sql.Append(@"
                    SELECT PerRequestPrice, PersuccessfulCasePrice 
                    FROM tbl_BillingSetting 
                    WHERE IsActive = 1 AND GETDATE() BETWEEN DateStart AND DateEnd 
                    AND BranchId =" + login_user.BranchID);
                db.OpenTable(sql.ToString());
                if (db.RecordCount() > 0)
                {
                    ViewState["PerRequestPrice"] = db.Item("PerRequestPrice").ToString();
                    ViewState["PersuccessfulCasePrice"] = db.Item("PersuccessfulCasePrice").ToString();
                    lblpricedetails.Text = lblpricedetails.Text.Replace("{0}", db.Item("PerRequestPrice").ToString());
                    lblpricedetails.Text = lblpricedetails.Text.Replace("{1}", db.Item("PersuccessfulCasePrice").ToString());
                }
                else
                {
                    sql.Clear();
                    sql.Append(@"
                        SELECT PerRequestPrice, PersuccessfulCasePrice 
                        FROM tbl_BillingSetting WHERE IsActive = 1 AND GETDATE() BETWEEN DateStart AND DateEnd 
                        AND BranchId = 0
                    ");
                    db.OpenTable(sql.ToString());
                    if (db.RecordCount() > 0)
                    {
                        ViewState["PerRequestPrice"] = db.Item("PerRequestPrice").ToString();
                        ViewState["PersuccessfulCasePrice"] = db.Item("PersuccessfulCasePrice").ToString();
                        lblpricedetails.Text = lblpricedetails.Text.Replace("{0}", db.Item("PerRequestPrice").ToString());
                        lblpricedetails.Text = lblpricedetails.Text.Replace("{1}", db.Item("PersuccessfulCasePrice").ToString());
                    }
                }
            }
            else
            {
                sql.Clear();
                sql.Append(@"
                    SELECT PerRequestPrice, PersuccessfulCasePrice 
                    FROM tbl_BillingSetting 
                    WHERE IsActive = 1 AND GETDATE() BETWEEN DateStart AND DateEnd 
                    AND BranchId = 0
                ");
                db.OpenTable(sql.ToString());
                if (db.RecordCount() > 0)
                {
                    ViewState["PerRequestPrice"] = db.Item("PerRequestPrice").ToString();
                    ViewState["PersuccessfulCasePrice"] = db.Item("PersuccessfulCasePrice").ToString();
                    lblpricedetails.Text = lblpricedetails.Text.Replace("{0}", db.Item("PerRequestPrice").ToString());
                    lblpricedetails.Text = lblpricedetails.Text.Replace("{1}", db.Item("PersuccessfulCasePrice").ToString());
                }
            }

            //Get Reprot Type - 17/03/2021
            //db.OpenTable("SELECT Name FROM tbl_PageController WITH (nolock) WHERE Category = 'ReportSetting' AND IsTrue = 1;");
            db.OpenTable("SELECT Name FROM tbl_PageController WITH (nolock) WHERE Category = 'ReportSetting' AND IsTrue = 1 AND Name = 'CTOS';");
            // if (db.RecordCount() > 0)
            //   lblReportType.Text = db.Item("Name");

            if (db.RecordCount() > 0)
                lblReportType.Text = "CTOS";
            else
                lblReportType.Text = "EXPERIAN";

        }

        private bool validateForm()
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            StringBuilder errMsg = new StringBuilder();
            ConsentFormParams contentParams = new ConsentFormParams();
            sql.AppendFormat(@"SELECT * FROM tbl_ConsentForm WITH(NOLOCK) WHERE Id ='{0}'", consentId);
            db.OpenTable(sql.ToString());
            if (db.RecordCount() > 0)
            {
                contentParams = JsonConvert.DeserializeObject<ConsentFormParams>(db.Item("Parameters"));
            }

            rc.removeInvalidChar(txtIC, txtName, txtRemark);
            rc.removeInvalidChar(txtcoapplicantId);            
            rc.removeInvalidChar(txtPrice, txtcoapplicantName, txtStatus);
            rc.removeInvalidChar(txtAmount);
            rc.removeInvalidChar(txtNet);
            if (string.IsNullOrEmpty(txtName.Text))
            {
                errMsg.Append(Resources.resource.nameEmpty + "\\n");
            }

            if (!login_user.isTestUser())
                if (string.IsNullOrEmpty(txtIC.Text))
                    errMsg.Append(Resources.resource.icEmpty + "\\n");
                else if (!Validation.isValidMyCard(txtIC.Text))
                    errMsg.Append(Resources.resource.Err_Mycard_Format + "\\n");
                else if (!((ConvertHelper.ConvertToInt(DateTime.Now.Year.ToString(), 2019) -
                    ConvertHelper.ConvertToInt(GetDateFromIc(txtIC.Text).Substring(0, 4), 0)) > 17
                    && (ConvertHelper.ConvertToInt(DateTime.Now.Year.ToString(), 2019) -
                    ConvertHelper.ConvertToInt(GetDateFromIc(txtIC.Text).Substring(0, 4), 0)) < 71))
                    errMsg.Append(Resources.resource.AgeNotAllow + "\\n");

            if (string.IsNullOrEmpty(txtNet.Text))
                txtNet.Text = "0";
            else if (!Validation.isDecimal(txtNet.Text))
                errMsg.Append(Resources.resource.Net_Income_Invalid_Format + "\\n");

            if (string.IsNullOrEmpty(txtPrice.Text))
                errMsg.Append(Resources.resource.PriceEmptyErrMsg + "\\n");
            else if (!Validation.isDecimal(txtPrice.Text))
                errMsg.Append(Resources.resource.Purchase_Price_Invalid_Format + "\\n");
            else if (ConvertHelper.ConvertToDecimal(txtPrice.Text, 0) <= 0)
                errMsg.Append(Resources.resource.Err_Zero_Purchase_Price + "\\n");

            if (string.IsNullOrEmpty(txtAmount.Text))
                txtAmount.Text = "0";
            else if (!Validation.isDecimal(txtAmount.Text))
                errMsg.Append(Resources.resource.Loan_Amount_Invalid_Format + "\\n");

            if (ddlProject.SelectedIndex == 0)
                errMsg.Append(Resources.resource.ProjectEmpty + "\\n");

            if (ddlProject.SelectedItem.Text == "Others")
                if (string.IsNullOrEmpty(txtprojectName.Text.Trim()))
                    errMsg.Append("* " + Resources.resource.ProjectnameCannotEmpty + "\\n");

            if (ddlHaveCoApplicant.SelectedItem.Text == "Yes")
            {
                if (string.IsNullOrEmpty(txtcoapplicantName.Text))
                    errMsg.Append(Resources.resource.Co_Applicant_NameEmpty + "\\n");

                if (string.IsNullOrEmpty(txtcoapplicantId.Text))
                    errMsg.Append(Resources.resource.Co_Applicant_ICEmpty + "\\n");
                else if (!Validation.isValidMyCard(txtcoapplicantId.Text))
                    errMsg.Append(Resources.resource.Co_Applicant_Err_Mycard_Format + "\\n");
                else if (!((ConvertHelper.ConvertToInt(DateTime.Now.Year.ToString(), 2019) -
                    ConvertHelper.ConvertToInt(GetDateFromIc(txtcoapplicantId.Text).Substring(0, 4), 0)) > 17
                    && (ConvertHelper.ConvertToInt(DateTime.Now.Year.ToString(), 2019) -
                    ConvertHelper.ConvertToInt(GetDateFromIc(txtcoapplicantId.Text).Substring(0, 4), 0)) < 71))
                    errMsg.Append(Resources.resource.AgeNotAllow + "\\n");

                if (string.IsNullOrEmpty(txtcoapplicantNetIncome.Text))
                    txtcoapplicantNetIncome.Text = "0";
                else if (!Validation.isDecimal(txtcoapplicantNetIncome.Text))
                    errMsg.Append(Resources.resource.Co_Applicant_Net_Income_Invalid_Format + "\\n");
            }
            else
            {
                txtcoapplicantName.Text = "";
                txtcoapplicantId.Text = "";
                txtcoapplicantNetIncome.Text = "";
            }

            if (rblReport.SelectedIndex == -1)
                errMsg.Append(Resources.resource.Report_Type_Empty + "\\n");

            if (!login_user.isTestUser())
            {
                if (uploadImages.Count <= 0)
                {
                    bool isMainSign = !(string.IsNullOrWhiteSpace(contentParams.MainApplicantSignature) || contentParams.MainApplicantSignature.Equals("image/jsignature;base30,"));
                    bool isCoSign = !(string.IsNullOrWhiteSpace(contentParams.CoApplicantSignature) || contentParams.CoApplicantSignature.Equals("image/jsignature;base30,"));

                    if (ddlHaveCoApplicant.SelectedItem.Text == "Yes" && !isMainSign && !isCoSign)
                        errMsg.Append(Resources.resource.ConsentUploadOrSign + "\\n");
                    else if (!isMainSign)
                        errMsg.Append(Resources.resource.ConsentUploadOrSign + "\\n");
                }

                for (int count1 = 0; count1 < uploadImages.Count; count1++)
                {
                    string imgpath = uploadImages[count1];
                    if (!File.Exists(imgpath))
                    {
                        errMsg.Append("* Fail to upload the file, please try again." + "\\n");

                        Session["uploadImages"] = new List<string>();
                        img_fu1.Visible = false;
                        img_fu1.Attributes.Remove("onclick");
                        img_fu2.Visible = false;
                        img_fu2.Attributes.Remove("onclick");
                        img_fu3.Visible = false;
                        img_fu3.Attributes.Remove("onclick");
                        break;
                    }
                }
            }

            if (login_user.BranchID != string.Empty)
            {
                sql.Clear();
                sql.Append("SELECT CreditLimit FROM tbl_BillingSetting WHERE IsActive = 1 AND BranchId =" + login_user.BranchID);
                db.OpenTable(sql.ToString());
                if (db.RecordCount() > 0)
                {
                    decimal creditLimit = Convert.ToDecimal(db.Item("CreditLimit").ToString());
                    sql.Clear();
                    sql.Append("SELECT CreditUsed, FreeRequestCount FROM tbl_UserCreditBalance WHERE MemberId = '" + login_user.UserId + "'");
                    db.OpenTable(sql.ToString());
                    if (db.RecordCount() > 0)
                    {
                        decimal userLimit = Convert.ToDecimal(db.Item("CreditUsed").ToString());
                        userLimit = userLimit * -1;
                        decimal requestPrice = Convert.ToDecimal(ViewState["PerRequestPrice"].ToString());
                        if ((creditLimit < (userLimit + requestPrice)) && ConvertHelper.ConvertToDecimal(db.Item("FreeRequestCount"), 0) <= 0)
                            errMsg.Append(Resources.resource.Request_PayMentValidation);
                    }
                }
                else //default setting
                {
                    sql.Clear();
                    sql.Append("SELECT CreditLimit FROM tbl_BillingSetting WHERE IsActive = 1 AND BranchId = 0");
                    db.OpenTable(sql.ToString());
                    if (db.RecordCount() > 0)
                    {
                        decimal creditLimit = Convert.ToDecimal(db.Item("CreditLimit").ToString());
                        sql.Clear();
                        sql.Append("SELECT CreditUsed, FreeRequestCount FROM tbl_UserCreditBalance WHERE MemberId = '" + login_user.UserId + "'");
                        db.OpenTable(sql.ToString());
                        if (db.RecordCount() > 0)
                        {
                            decimal userLimit = Convert.ToDecimal(db.Item("CreditUsed").ToString());
                            userLimit = userLimit * -1;
                            decimal requestPrice = Convert.ToDecimal(ViewState["PerRequestPrice"].ToString());
                            if ((creditLimit < (userLimit + requestPrice)) && ConvertHelper.ConvertToDecimal(db.Item("FreeRequestCount"), 0) <= 0)
                                errMsg.Append(Resources.resource.Request_PayMentValidation);
                        }
                    }
                }
            }
            else
            {
                sql.Clear();
                sql.Append("SELECT CreditLimit FROM tbl_BillingSetting WHERE IsActive = 1 AND BranchId = 0");
                db.OpenTable(sql.ToString());
                if (db.RecordCount() > 0)
                {
                    decimal creditLimit = Convert.ToDecimal(db.Item("CreditLimit").ToString());
                    sql.Clear();
                    sql.Append("SELECT CreditUsed, FreeRequestCount FROM tbl_UserCreditBalance WHERE MemberId = '" + login_user.UserId + "'");
                    db.OpenTable(sql.ToString());
                    if (db.RecordCount() > 0)
                    {
                        decimal userLimit = Convert.ToDecimal(db.Item("CreditUsed").ToString());
                        userLimit = userLimit * -1;
                        decimal requestPrice = Convert.ToDecimal(ViewState["PerRequestPrice"].ToString());
                        if ((creditLimit < (userLimit + requestPrice)) && ConvertHelper.ConvertToDecimal(db.Item("FreeRequestCount"), 0) <= 0)
                            errMsg.Append(Resources.resource.Request_PayMentValidation);
                    }
                }
            }



            lblErr.Text = errMsg.ToString().Replace("\\n", "</br>");

            if (lblErr.Text != string.Empty)
            {
                divErrorMessage.Visible = true;
                sqlString.displayAlert2(this, errMsg.ToString());
                return false;
            }

            return true;
        }

        private string GetDateFromIc(string ic)
        {
            //1955-11-22
            int icShortYear = Convert.ToInt32(ic.Substring(0, 2));
            int currentShortYear = Convert.ToInt32(DateTime.Now.Year.ToString().Substring(2, 2));
            int reduceYear = icShortYear > currentShortYear ? -1 : 0;
            string year = (Convert.ToInt32(DateTime.Now.Year.ToString().Substring(0, 2)) + reduceYear).ToString() + ic.Substring(0, 2);
            string month = ic.Substring(2, 2);
            string day = ic.Substring(4, 2);

            return year + "-" + month + "-" + day;
        }

        private bool validateConsentFormRequiredField()
        {
            StringBuilder errMsg = new StringBuilder();

            rc.removeInvalidChar(txtIC, txtName);
            rc.removeInvalidChar(txtcoapplicantId, txtcoapplicantName);

            if (string.IsNullOrEmpty(txtName.Text))
                errMsg.Append(Resources.resource.nameEmpty + "\\n");

            if (!login_user.isTestUser())
                if (string.IsNullOrEmpty(txtIC.Text))
                    errMsg.Append(Resources.resource.icEmpty + "\\n");
                else if (!Validation.isValidMyCard(txtIC.Text))
                    errMsg.Append(Resources.resource.Err_Mycard_Format + "\\n");

            if (ddlHaveCoApplicant.SelectedItem.Text == "Yes")
            {
                if (string.IsNullOrEmpty(txtcoapplicantName.Text))
                    errMsg.Append(Resources.resource.Co_Applicant_NameEmpty + "\\n");

                if (string.IsNullOrEmpty(txtcoapplicantId.Text))
                    errMsg.Append(Resources.resource.Co_Applicant_ICEmpty + "\\n");
                else if (!Validation.isValidMyCard(txtcoapplicantId.Text))
                    errMsg.Append(Resources.resource.Co_Applicant_Err_Mycard_Format + "\\n");
            }

            lblErr.Text = errMsg.ToString().Replace("\\n", "</br>");

            if (lblErr.Text != string.Empty)
            {
                divErrorMessage.Visible = true;
                sqlString.displayAlert2(this, errMsg.ToString());
                return false;
            }

            return true;
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            if (validateForm())
            {
                string requestid = rc.InsertRequestRecord(login_user.UserId, login_user.BranchID, consentId, txtName.Text, txtIC.Text, txtNet.Text.Replace(",", ""),
                    string.IsNullOrEmpty(txtcoapplicantName.Text) ? DBNull.Value.ToString() : txtcoapplicantName.Text,
                    string.IsNullOrEmpty(txtcoapplicantId.Text) ? DBNull.Value.ToString() : txtcoapplicantId.Text,
                    string.IsNullOrEmpty(txtcoapplicantNetIncome.Text) ? DBNull.Value.ToString() : txtcoapplicantNetIncome.Text,
                    ddlProject.SelectedValue == "Others" ? DBNull.Value.ToString() : ddlProject.SelectedValue,
                    string.IsNullOrEmpty(txtprojectName.Text.Trim()) ? DBNull.Value.ToString() : txtprojectName.Text.Trim(), secure.RC(txtPrice.Text), secure.RC(txtAmount.Text),
                    "B", secure.RC(txtRemark), secure.RC(txtStatus), ViewState["PerRequestPrice"].ToString(),
                    ViewState["PersuccessfulCasePrice"].ToString());

                wwdb db = new wwdb();

                #region EmailNotification
                if (!login_user.isStaffAdmin())
                {
                    db.OpenTable(@"
                    select a.Email from tbl_memberinfo a
                    inner join
                    tbl_MemberRole mr WITH(NOLOCK) ON a.MemberId = mr.MemberID WHERE mr.RoleCode = 'AD'");


                    EmailController emc = new EmailController();
                    while (!db.Eof())
                    {
                        emc.sendNewRequestNotification(db.Item("Email"), requestid);
                        db.MoveNext();
                    }
                }
                #endregion

                if (!login_user.isSpeedUser())
                {
                    clearTextBox();

                    string tempSQL = string.Format(@"
                            INSERT INTO tbl_TApplicants (RequestID, ContactNumber, ApplicantType)
                            VALUES (N'{0}', N'{1}', '{2}')
                        ", requestid, txtContactNo.Text, "MA");
                    db.Execute(tempSQL);

                    string newPath = "/Upload/RequestDocument/";
                    newPath += requestid + "/";
                    if (!Directory.Exists(Server.MapPath("~" + newPath)))
                        Directory.CreateDirectory(Server.MapPath("~" + newPath));

                    for (int count1 = 0; count1 < uploadImages.Count; count1++)
                    {
                        string imgpath = uploadImages[count1];
                        FileInfo FromFile = new FileInfo(imgpath);
                        string toFilePath = newPath + FromFile.Name;
                        FromFile.MoveTo(Server.MapPath("~" + toFilePath));
                        string sql = "insert into tbl_RequestDocs(RequestID,DocumentType , DocsURL)values(" + requestid + ",'" + DocumentType.NormalFiles + "',N'" + toFilePath + "') ";
                        db.Execute(sql.ToString());
                    }
                    //end of insert

                    #region SMS Notification
                    //send sms
                    db.OpenTable("SELECT a.Mobile from tbl_memberinfo a inner join tbl_MemberRole mr WITH(NOLOCK) ON a.MemberId = mr.MemberID WHERE mr.RoleCode = 'AD'");

                    MessagingService SMS = new MessagingService();
                    //send to every admin
                    while (!db.Eof())
                    {
                        string mobileNo = db.Item("Mobile");
                        bool validFormat = false;
                        if (mobileNo.StartsWith("0") && !(mobileNo.StartsWith("6")))
                        {
                            mobileNo = "6";
                            mobileNo += db.Item("Mobile");
                            validFormat = true;
                        }
                        else if (mobileNo.StartsWith("601"))
                            validFormat = true;

                        if (validFormat)//only send sms when mobileNo is in valid format to save cost
                            SMS.sendNewRequestNotification(mobileNo, requestid);

                        db.MoveNext(); //next number
                    }
                    #endregion

                    sqlString.displayAlert2(this, Resources.resource.Action_Successful, Request.Url.ToString());
                }
                else
                {
                    try
                    {
                        bool test = false; //set testing mode here
                        string MAtype = string.Empty;
                        string CAtype = string.Empty;
                        bool haveCoApplicant = ddlHaveCoApplicant.SelectedIndex > 0;
                        bool isMain;

                        StringBuilder sql = new StringBuilder();

                        sql.Clear();
                        sql.Append(@"
                            SELECT Name FROM tbl_PageController with (nolock) 
                            WHERE Category = 'ReportSetting' AND IsTrue = 1
                        ");
                        db.OpenTable(sql.ToString());
                        if (db.RecordCount() > 0)
                        {
                            MAtype = db.Item("Name");
                            CAtype = db.Item("Name");
                        }

                        sql.Clear();
                        sql.Append("UPDATE tbl_Request SET ReportType = 'S' where RequestID = N'" + requestid + "'");
                        db.Execute(sql.ToString());

                        string mainAge = string.Empty, coAge = string.Empty;
                        string mainResultID = string.Empty, coResultID = string.Empty;
                        //bool result = false;

                        mainAge = rc.getAgeFromIC(txtIC.Text).ToString();
                        if (!string.IsNullOrEmpty(txtcoapplicantId.Text))
                            coAge = rc.getAgeFromIC(txtcoapplicantId.Text).ToString();

                        //string type = "RAMCI"; //temp set APItype here
                        string mainCCRIS = "", coCCRIS = "";
                        bool callAPI = true, mainReuse = false, coReuse = false;

                        //check if reuseReport function is activated
                        db.OpenTable(@"
                            SELECT isTrue as 'Active' 
                            FROM tbl_PageController with (nolock)
                            WHERE Category = 'Reuse Report'
                            AND Name = 'Active'
                        ");
                        if (db.RecordCount() > 0)
                            if (ConvertHelper.ConvertToBoolen(db.Item("Active"), false)) //only reuse when active, off by default
                            {
                                if (rc.checkReuseReport(secure.RC(txtIC.Text), true))
                                    if (rc.checkReuseReport(secure.RC(txtIC.Text), true, true))
                                    {
                                        mainReuse = true;
                                        MAtype = rc.MAtype; //replacing setting reportType if there's reuse
                                    }
                                if (rc.checkReuseReport(secure.RC(txtcoapplicantId.Text), false))
                                    if (rc.checkReuseReport(secure.RC(txtcoapplicantId.Text), false, true))
                                    {
                                        coReuse = true;
                                        CAtype = rc.CAtype; //replacing setting reportType if there's reuse
                                    }
                            }

                        //Main Applicant
                        if (mainReuse || test) //if reusing, retrieve from db
                        {
                            mainCCRIS = "false";
                            coCCRIS = "";
                            callAPI = false;
                        }
                        else
                            callAPI = true;

                        try
                        {
                            isMain = true;
                            if (rc.RetrieveXMLDataForMainCoApp(requestid, txtName.Text, "", txtIC.Text,
                                "I", haveCoApplicant, mainAge, txtAmount.Text, mainCCRIS, coCCRIS, isMain, callAPI, MAtype))
                            {
                                int houseLoanNumber = 0;
                                if (MAtype.Equals("CTOS"))
                                {
                                    if (rc.section_ccris != null && rc.section_ccris.accounts != null && rc.section_ccris.accounts.account != null)
                                    {
                                        rc.PassDataIntoArray(true, mainAge, coAge, rc.section_ccris);
                                        //result = true;
                                        rc.InsertCTOSReport(rc.section_ccris.accounts.account.ToList(), isMain);
                                    }
                                    if (rc.section_ccris.special_attention_accs != null &&
                                        rc.section_ccris.special_attention_accs.FirstOrDefault() != null)
                                        rc.InsertCTOSReport(rc.section_ccris.special_attention_accs.ToList(), isMain);

                                    if (rc.section_ccris != null && rc.section_ccris.accounts != null && rc.section_ccris.accounts.account != null)
                                        for (int count1 = 0; count1 < rc.section_ccris.accounts.account.Count(); count1++)
                                        {
                                            var account = rc.section_ccris.accounts.account[count1];
                                            db.OpenTable("SELECT LoanID FROM tbl_LoanTypesXML WITH(NOLOCK) WHERE LoanTypeCode = '" + account.sub_accounts[0].facility.code.ToString() + "';");
                                            if (db.RecordCount() > 0)
                                                if (db.Item("LoanID").Equals("1"))
                                                    houseLoanNumber++;
                                        }

                                    if (rc.section_ccris != null &&
                                        rc.section_ccris.accounts != null &&
                                        rc.section_ccris.accounts.account != null)
                                        rc.checkCCRIScount(rc.section_ccris.accounts.account.ToList(), isMain, !haveCoApplicant);
                                }
                                if (MAtype.Equals("RAMCI"))
                                {
                                    if (rc.detail != null && rc.detail.outstanding_credit != null &&
                                        rc.detail.outstanding_credit.FirstOrDefault() != null)
                                    {
                                        rc.PassDataIntoArray(true, mainAge, coAge, rc.detail);
                                        //result = true;
                                        rc.InsertRAMCIReport(rc.detail.outstanding_credit.ToList(), isMain);
                                    }
                                    if (rc.detail.special_attention_account != null &&
                                        rc.detail.special_attention_account.FirstOrDefault() != null)
                                        rc.InsertRAMCIReport(rc.detail.special_attention_account.ToList(), isMain, true);

                                    if (rc.detail != null && rc.detail.outstanding_credit != null &&
                                        rc.detail.outstanding_credit.FirstOrDefault() != null)
                                        for (int count1 = 0; count1 < rc.detail.outstanding_credit.Count(); count1++)
                                        {
                                            var account = rc.detail.outstanding_credit[count1];
                                            db.OpenTable("SELECT LoanID FROM tbl_LoanTypesXML WITH(NOLOCK) WHERE LoanTypeCode = '" + account.sub_account[0].item.facility + "';");
                                            if (db.RecordCount() > 0)
                                                if (db.Item("LoanID").Equals("1"))
                                                    houseLoanNumber++;
                                        }

                                    if (rc.detail != null &&
                                        rc.detail.outstanding_credit != null)
                                        rc.checkCCRIScount(rc.detail.outstanding_credit.ToList(), isMain, !haveCoApplicant);
                                }
                                rc.finishChecking();

                                rc.InsertTempData(txtName.Text, mainAge, txtIC.Text, houseLoanNumber, isMain, requestid, rc.getIntrestRate(6), !haveCoApplicant);

                                if (!string.IsNullOrEmpty(rc.mainResultID))
                                    mainResultID = rc.mainResultID;

                                sql.Clear();
                                sql.Append("select * ");
                                sql.Append("from tbl_CTOS_Report_Loan_Details ");
                                sql.Append("where tbl_CTOS_Report_Loan_Details.CTOS_Result_Id = '" + mainResultID + "' ");
                                db.OpenTable(sql.ToString());
                                if (db.RecordCount() > 0)
                                {
                                    sql.Clear();
                                    //show all ccriss
                                    sql.Append("UPDATE tbl_CTOS_Report_Loan_Details SET IsShowinReport = 1 WHERE CTOS_Result_Id = '" +
                                        mainResultID + "';");
                                    db.Execute(sql.ToString());
                                    LogUtil.logAction(sql.ToString(), "Speed-UPDATE CTOS_Report_Loan_Details show all details. Result_ID:" + mainResultID);
                                }
                            }
                            else
                            {  //sqlString.displayAlert2(this, "No Report found.");
                                sqlString.displayAlert2(this, "CCRIS Service Temp Unavailable.Please try again later.");// Promt CCRIS Down message 
                                LogUtil.logError("MainApplicant. RequestID:" + requestid, "SpeedMode No Report");
                            }
                        }
                        catch (Exception ex)
                        {
                            LogUtil.logError(ex.Message, "Exception error. MainApplicant pulling " + MAtype + " type report. RequestID:" + requestid);
                            db.Execute("UPDATE tbl_CTOS_Result SET Report_Type='E', IsActive=0 WHERE ID='" + rc.mainResultID + "'");
                        }

                        if (haveCoApplicant)
                        {
                            isMain = false;
                            //Co Applicant
                            if (coReuse || test) //if reusing, retrieve from db
                            {
                                mainCCRIS = "";
                                coCCRIS = "false";
                                callAPI = false;
                            }
                            else
                                callAPI = true;
                            try
                            {
                                if (rc.RetrieveXMLDataForMainCoApp(requestid, string.IsNullOrEmpty(txtcoapplicantName.Text) ? DBNull.Value.ToString() : txtcoapplicantName.Text,
                                    DBNull.Value.ToString(), string.IsNullOrEmpty(txtcoapplicantId.Text) ? DBNull.Value.ToString() : txtcoapplicantId.Text, "I", haveCoApplicant,
                                    coAge, secure.RC(txtAmount.Text), mainCCRIS, coCCRIS, isMain, callAPI, CAtype))
                                {
                                    int houseLoanNumber = 0;
                                    if (CAtype.Equals("CTOS"))
                                    {
                                        if (rc.section_ccris != null && rc.section_ccris.accounts != null && rc.section_ccris.accounts.account != null)
                                        {
                                            rc.PassDataIntoArray(isMain, mainAge, coAge, rc.section_ccris);
                                            //result = true;
                                            rc.InsertCTOSReport(rc.section_ccris.accounts.account.ToList(), isMain);
                                        }
                                        if (rc.section_ccris.special_attention_accs != null)
                                            rc.InsertCTOSReport(rc.section_ccris.special_attention_accs.ToList(), isMain);

                                        if (rc.section_ccris != null && rc.section_ccris.accounts != null && rc.section_ccris.accounts.account != null)
                                            for (int count1 = 0; count1 < rc.section_ccris.accounts.account.Count(); count1++)
                                            {
                                                var account = rc.section_ccris.accounts.account[count1];
                                                db.OpenTable("SELECT LoanID FROM tbl_LoanTypesXML with (nolock) where loantypecode = '" + account.sub_accounts[0].facility.code.ToString() + "';");
                                                if (db.RecordCount() > 0)
                                                    if (db.Item("LoanID").Equals("1"))
                                                        houseLoanNumber++;
                                            }

                                        if (rc.section_ccris != null &&
                                            rc.section_ccris.accounts != null &&
                                            rc.section_ccris.accounts.account != null)
                                            rc.checkCCRIScount(rc.section_ccris.accounts.account.ToList(), isMain, !haveCoApplicant);
                                    }
                                    if (CAtype.Equals("RAMCI"))
                                    {
                                        if (rc.detail != null && rc.detail.outstanding_credit != null)
                                        {
                                            rc.PassDataIntoArray(isMain, mainAge, coAge, rc.detail);
                                            //result = true;
                                            rc.InsertRAMCIReport(rc.detail.outstanding_credit.ToList(), isMain);
                                        }
                                        if (rc.section_ccris.special_attention_accs != null)
                                            rc.InsertRAMCIReport(rc.detail.special_attention_account.ToList(), isMain, true);

                                        if (rc.detail != null && rc.detail.outstanding_credit != null)
                                            for (int count1 = 0; count1 < rc.detail.outstanding_credit.Count(); count1++)
                                            {
                                                var account = rc.detail.outstanding_credit[count1];
                                                db.OpenTable("SELECT LoanID FROM tbl_LoanTypesXML with (nolock) where loantypecode = '" + account.sub_account[0].item.facility.ToString() + "';");
                                                if (db.RecordCount() > 0)
                                                    if (db.Item("LoanID").Equals("1"))
                                                        houseLoanNumber++;
                                            }

                                        if (rc.detail != null &&
                                            rc.detail.outstanding_credit != null)
                                            rc.checkCCRIScount(rc.detail.outstanding_credit.ToList(), isMain, !haveCoApplicant);
                                    }
                                    rc.finishChecking();

                                    rc.InsertTempData(string.IsNullOrEmpty(txtcoapplicantName.Text) ? DBNull.Value.ToString() : txtcoapplicantName.Text, coAge,
                                            string.IsNullOrEmpty(txtcoapplicantId.Text) ? DBNull.Value.ToString() : txtcoapplicantId.Text, houseLoanNumber, isMain, requestid,
                                            rc.getIntrestRate(6), !haveCoApplicant);

                                    if (!string.IsNullOrEmpty(rc.coResultID))
                                        coResultID = rc.coResultID;

                                    sql.Clear();
                                    sql.Append("select * ");
                                    sql.Append("from tbl_CTOS_Report_Loan_Details ");
                                    sql.Append("where tbl_CTOS_Report_Loan_Details.CTOS_Result_Id = '" + coResultID + "' ");
                                    db.OpenTable(sql.ToString());
                                    sql.Clear();
                                    if (db.RecordCount() > 0)
                                    {
                                        //show all ccriss
                                        sql.Append("UPDATE tbl_CTOS_Report_Loan_Details SET IsShowinReport = 1 WHERE CTOS_Result_Id = '" +
                                            coResultID + "';");
                                        db.Execute(sql.ToString());
                                        LogUtil.logAction(sql.ToString(), "Speed-UPDATE CTOS_Report_Loan_Details show all details. Result_ID:" + coResultID);
                                    }
                                }
                                else
                                    //sqlString.displayAlert2(this, "No Report found.");
                                    LogUtil.logError("CoApplicant. RequestID:" + requestid, "SpeedMode No Report");
                            }
                            catch (Exception ex)
                            {
                                LogUtil.logError(ex.Message, "Exception error. CoApplicant pulling " + CAtype + " type report. RequestID:" + requestid);
                                db.Execute("UPDATE tbl_CTOS_Result SET Report_Type='E', IsActive=0 WHERE ID='" + rc.coResultID + "'");
                            }
                        }
                        clearTextBox();
                        string newPath = "/Upload/RequestDocument/";
                        newPath += requestid + "/";
                        if (!Directory.Exists(Server.MapPath("~" + newPath)))
                            Directory.CreateDirectory(Server.MapPath("~" + newPath));
                        for (int count1 = 0; count1 < uploadImages.Count; count1++)
                        {
                            string imgpath = uploadImages[count1];
                            FileInfo FromFile = new FileInfo(imgpath);
                            string toFilePath = newPath + FromFile.Name;
                            FromFile.MoveTo(Server.MapPath("~" + toFilePath));
                            string sql2 = "insert into tbl_RequestDocs(RequestID,DocumentType , DocsURL)values(" + requestid + ",'" + DocumentType.NormalFiles + "',N'" + toFilePath + "') ";
                            db.Execute(sql2.ToString());
                            LogUtil.logAction(sql2.ToString(), "Insert Doc into tbl_RequestDocs - RequestID:" + requestid);
                        }

                        //if (result) //if there's something in array
                        rc.InsertLoanData(mainAge, coAge, !(ddlHaveCoApplicant.SelectedIndex > 0));
                        List<string> emptyBankList = new List<string>();
                        emptyBankList.Add("12");//Default to Classic
                        if (rc.SaveReport(requestid, emptyBankList, false, false))
                        {
                            //string reportIrl = "/Form/Loan/DsrReport.aspx?rid=" + sqlString.encryptURL(requestid) + "&BankID=" + sqlString.encryptURL("12");//temp use Classic Bank for all
                            //sqlString.OpenNewWindow_Center(reportIrl, "Report", 800, 600, this);
                            sqlString.displayAlert2(this, "Report is ready.", "/Form/Report/ClientList.aspx");
                        }
                    }
                    catch (Exception ex)
                    {
                        LogUtil.logError(ex.Message, "SpeedMode Error. RequestID:" + requestid);
                        db.Execute("UPDATE tbl_CTOS_Result SET Report_Type='E', IsActive=0 WHERE Request_Id='" + requestid + "'");
                        sqlString.displayAlert2(this, Resources.resource.unknownError);
                    }
                    finally { db.Close(); }
                }
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", "window.open('/Form/Report/ClientList.aspx','_newtab');", true);
                //sqlString.displayAlert2(this, "Report is ready.", "/Form/Report/ClientList.aspx");
            }
        }
        private void clearTextBox()
        {
            txtName.Text = string.Empty;
            txtIC.Text = string.Empty;
            txtNet.Text = string.Empty;
            txtcoapplicantName.Text = string.Empty;
            txtcoapplicantId.Text = string.Empty;
            txtcoapplicantNetIncome.Text = string.Empty;
            ddlProject.SelectedIndex = 0;
            txtPrice.Text = string.Empty;
            txtAmount.Text = string.Empty;
            txtRemark.Text = string.Empty;
            //try
            //{
            //    CheckBox chkterms = (CheckBox)this.FindControl("chkterms");
            //    chkterms.Checked = false;
            //}
            //catch (Exception e) { LogUtil.logError(e.Message, "Error clearing checkbox."); }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            FileUpload fupload = (FileUpload)divUpload.FindControl(hid_FUID.Value);
            StringBuilder errMsg = new StringBuilder();
            var strContent2 = fupload.FileBytes.Length;

            if (fupload != null && fupload.HasFile)
            {
                if (Convert.ToInt32(strContent2) > 5242889)
                {
                    errMsg.Append("Maximum allowed file size is 5 MB");
                }
                else
                {
                    string path = "/Upload/Temp/RequestDocument/";
                    string physicalPath = Server.MapPath("~" + path);
                    FileInfo Finfo = new FileInfo(fupload.PostedFile.FileName);
                    string filename = DateTime.Now.ToString("ddMMyyyyhhmmssfff") + "_" + login_user.UserId + Finfo.Extension;
                    string fullPhysical = physicalPath + filename;
                    if (!Directory.Exists(physicalPath))
                        Directory.CreateDirectory(physicalPath);

                    if (File.Exists(physicalPath))
                        File.Delete(fullPhysical);

                    fupload.SaveAs(fullPhysical);
                    uploadImages.Add(fullPhysical);
                    Session["uploadImages"] = uploadImages;
                    Image imgDisplay = (Image)divUpload.FindControl("img_" + hid_FUID.Value);
                    imgDisplay.Visible = true;
                    imgDisplay.Attributes.Add("onclick", "window.open('" + path + filename + "')");
                }
            }

        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            //wwdb db = new wwdb();
            //StringBuilder sql = new StringBuilder();

            //sql.Clear();
            //sql.Append("SELECT TOP(1) DocsURL FROM tbl_ConsentDocs WHERE isDeleted = 0 OR isDeleted IS NULL"); //ToDo: add active column and enable to set by admin
            //db.OpenTable(sql.ToString());

            //if (db.RecordCount() > 0)
            //{
            //    string filePath = db.Item("DocsURL");

            //    sqlString.OpenNewWindow_Center(filePath, "Consent Form", 600, 600, this);
            //}

            sqlString.OpenNewWindow_Center("/Upload/Consent/WhoPayConsentForm_v1.pdf", "Consent Form", 600, 600, this);
        }

        protected void btnSignConsent_Click(object sender, EventArgs e)
        {
            if (validateConsentFormRequiredField())
            {
                sqlString.OpenNewWindow_Center("/Form/Loan/ConsentForm.aspx?id=" + sqlString.encryptURL(consentId) +
                "&maid=" + sqlString.encryptURL(txtIC.Text) + "&man=" + sqlString.encryptURL(txtName.Text) +
                "&caid=" + sqlString.encryptURL(txtcoapplicantId.Text) + "&can=" + sqlString.encryptURL(txtcoapplicantName.Text)
                , "Consent Form", 800, 800, this);
            }
        }

        protected void ddlHaveCoApplicant_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlHaveCoApplicant.SelectedItem.Text == "Yes")
            {
                divcoapplicant.Visible = true;
                lblpricedetails.Text = Resources.resource.Request_PriceDetails;
                decimal PerRequestPrice = Convert.ToDecimal(ViewState["PerRequestPrice"].ToString());
                decimal PersuccessfulCasePrice = Convert.ToDecimal(ViewState["PersuccessfulCasePrice"].ToString());
                lblpricedetails.Text = lblpricedetails.Text.Replace("{0}", (PerRequestPrice * 2).ToString() + " (MYR " + PerRequestPrice.ToString() + " * 2 applicants)");
                lblpricedetails.Text = lblpricedetails.Text.Replace("{1}", (PersuccessfulCasePrice * 2).ToString() + " (MYR " + PersuccessfulCasePrice.ToString() + " * 2 applicants)");
            }
            else
            {
                divcoapplicant.Visible = false;
                lblpricedetails.Text = Resources.resource.Request_PriceDetails;
                decimal PerRequestPrice = Convert.ToDecimal(ViewState["PerRequestPrice"].ToString());
                decimal PersuccessfulCasePrice = Convert.ToDecimal(ViewState["PersuccessfulCasePrice"].ToString());
                lblpricedetails.Text = lblpricedetails.Text.Replace("{0}", PerRequestPrice.ToString());
                lblpricedetails.Text = lblpricedetails.Text.Replace("{1}", PersuccessfulCasePrice.ToString());
            }
        }

        protected void ddlProject_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlProject.SelectedItem.Text == "Others")
                divprojectName.Visible = true;
            else
                divprojectName.Visible = false;

            //function ddlProjectChanged(ddlProject)
            //{

            //    var name = ddlProject.value;
            //    var othername = document.getElementById("divprojectName");
            //    if (name == "Others")
            //    {
            //        othername.style.display = "block";
            //    }
            //    else
            //    {
            //        othername.style.display = "none";
            //    }
            //}

        }
    }
}