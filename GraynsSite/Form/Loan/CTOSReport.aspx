﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CTOSReport.aspx.cs" Inherits="HJT.Form.Loan.CTOSReport" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
</head>
<body>
    <form id="form1" runat="server">
        <div id="divReportExpiration" runat="server" visible="false" style="text-align:center; width: 100%; height: 100%; margin-top: 20%;">
            <asp:Label runat="server" Text='<%$ Resources:resource, ReportExpiredMessage %>' Font-Bold="true" Font-Size="X-Large"/>
        </div>
        <div id="divErrorReport" runat="server" visible="false" style="text-align:center; width: 100%; height: 100%; margin-top: 20%;">
            <asp:Label runat="server" Text='<%$ Resources:resource, ReportErrorMessage %>' Font-Bold="true" Font-Size="X-Large"/>
        </div>
    </form>
</body>
</html>

