﻿<%@ Page Language="C#" MasterPageFile="~/Global1.Master" AutoEventWireup="true" CodeBehind="NewRequest.aspx.cs" Inherits="HJT.Form.Loan.NewRequest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function checkReuse(message) {
            var main = document.getElementById('<%= hfMainReuseReport.ClientID %>').value;
            if (document.getElementById('<%= ddlHaveCoApplicant.ClientID %>').selectedIndex() > 0)
                var co = document.getElementById('<%= hfCoReuseReport.ClientID %>').value;
            if (confirm(message)) {
                main = "Yes";
                co = "Yes";
            } else {
                main = "No";
                co = "No";
            }
        }

        function nextPage(requestID, bank) {
            //window.open('../Form/Loan/DsrReport.aspx?rid=' + requestID + '&BankID=' + bank, 'tab');
            window.location.href = '../Form/Report/ClientList.aspx';
        }

        function confirmSubmit() {
            var x = document.getElementById("chkterms").checked;
            if (x == false) {
                document.getElementById("chkterms").focus();
                alert('You must agree to the terms first.');
                return false;
            }
            var message = ('<%= GetGlobalResourceObject("resource", "IC") %>');
            message = message + " is " + document.getElementById("<%= txtIC.ClientID %>").value;
            if (document.getElementById("<%=ddlHaveCoApplicant.ClientID%>").selectedIndex > 0)
            {
                message = message + " and " + document.getElementById("<%= txtcoapplicantId.ClientID %>").value;
            }
            message = message + ". ";
            return confirm(message + ('<%= GetGlobalResourceObject("resource", "Confirm_submit") %>'));
        }

        function FileUpload(fuid) {
            var uploadControl = "";
            if (fuid == "fu1") {
                uploadControl = document.getElementById('<%= fu1.ClientID %>');
            } else if (fuid == "fu2") {
                uploadControl = document.getElementById('<%= fu2.ClientID %>');
            } else if (fuid == "fu3") {
                uploadControl = document.getElementById('<%= fu3.ClientID %>');
            }
            var maxsize = 5242889;
            //var maxsize = 10485778;
            var filesize = uploadControl.files[0].size;
            if (filesize >= maxsize) {
                if (fuid == "fu1") {
                    document.getElementById('<%= fu1.ClientID %>').value = '';
                } else if (fuid == "fu2") {
                    document.getElementById('<%= fu2.ClientID %>').value = '';
                } else if (fuid == "fu3") {
                    document.getElementById('<%= fu3.ClientID %>').value = '';
                }
                alert("Maximum allowed file size is 5 MB");
            }
            else {

                $('#<%= hid_FUID.ClientID %>').val(fuid);
                $('#<%= btnUpload.ClientID %>').click();
            }
        }

        $('#foo').change(function () {
            $(this).removeClass("bar");
        })
    </script>
    <style>
        .bar:after {
            content: "I.C and Consent Form or Sales Form";
            background-color: white;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" width="400px">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                <span class="icon fa fa-fw fa-book" aria-hidden="true"></span>
                <asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><span runat="server" id="spHeader"><%= GetGlobalResourceObject("resource", "Request_Report")%></span></asp:Label>
            </h1>
            <asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
        </section>
        <section class="content">
            <div class="col-md-12">
                <div runat="server">
                    <div class="box">
                        <h4>
                            <asp:Label ID="panel" runat="server" Visible="false" Text="<%$ Resources:Resource, Request_Report%>"></asp:Label>
                        </h4>
                        <div id="divErrorMessage" runat="server" visible="false" class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <b><%= GetGlobalResourceObject("resource", "Alert")%></b>
                            <br />
                            <asp:Label runat="server" ID="lblErr"></asp:Label>
                        </div>
                        <asp:Panel runat="server" DefaultButton="btnSend">
                            <div class="box-body">
                                <div class="form-group">
                                    <div><%= GetGlobalResourceObject("resource", "Name")%><span style="color: Red;">&nbsp;*</span></div>
                                    <div class="controls">
                                        <asp:TextBox runat="server" ID="txtName" CssClass="form-control"> </asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div>
                                        <asp:Label runat="server" ID="lblIC">
                                        </asp:Label>
                                        <span style="color: Red;">&nbsp;*</span>
                                    </div>
                                    <div class="controls">
                                        <asp:TextBox runat="server" ID="txtIC" CssClass="form-control"> </asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div><%= GetGlobalResourceObject("resource", "Net_Income")%></div>
                                    <div class="controls">
                                        <asp:TextBox runat="server" ID="txtNet" CssClass="form-control"> </asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div><%= GetGlobalResourceObject("resource", "Contact_Number")%></div>
                                    <div class="controls">
                                        <asp:TextBox runat="server" ID="txtContactNo" CssClass="form-control"> </asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <table>
                                        <tr>
                                            <td>
                                                <label class="control-label"><%= GetGlobalResourceObject("resource", "Have_Co_Applicant")%>&nbsp;?&nbsp;<span style="color: Red;"></span></label>
                                            </td>
                                            <td>
                                                <asp:DropDownList runat="server" ID="ddlHaveCoApplicant" AutoPostBack="true" OnSelectedIndexChanged="ddlHaveCoApplicant_SelectedIndexChanged" >
                                                    <asp:ListItem Value="0" Text="<%$ Resources:Resource, No%>"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="<%$ Resources:Resource, Yes%>"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="form-group" id="divcoapplicant" runat="server" visible="false">
                                    <div class="form-group">
                                        <div><%= GetGlobalResourceObject("resource", "Co_Applicant_Name")%><span style="color: Red;">&nbsp;*</span></div>
                                        <div class="controls">
                                            <asp:TextBox runat="server" ID="txtcoapplicantName" CssClass="form-control"> </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div><%= GetGlobalResourceObject("resource", "Co_Applicant_IC")%><span style="color: Red;">&nbsp;*</span></div>
                                        <div class="controls">
                                            <asp:TextBox runat="server" ID="txtcoapplicantId" CssClass="form-control"> </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div><%= GetGlobalResourceObject("resource", "Co_Applicant_Net_Income")%></div>
                                        <div class="controls">
                                            <asp:TextBox runat="server" ID="txtcoapplicantNetIncome" CssClass="form-control"> </asp:TextBox>
                                        </div>
                                    </div>
                                    </div>
                                <div class="form-group" runat="server" id="divproject">
                                    <div><%= GetGlobalResourceObject("resource", "ProjectName")%><span style="color: Red;">&nbsp;*</span></div>
                                    <div class="controls">
                                        <asp:DropDownList runat="server" CssClass="form-control" ID="ddlProject" OnSelectedIndexChanged="ddlProject_SelectedIndexChanged"
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group" id="divprojectName" runat="server" visible="false">
                                    <div class="controls">
                                        <asp:TextBox runat="server" ID="txtprojectName" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div><%= GetGlobalResourceObject("resource", "Purchase_Price")%><span style="color: Red;">&nbsp;*</span></div>
                                    <div class="controls">
                                        <asp:TextBox runat="server" ID="txtPrice" CssClass="form-control"> </asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div><%= GetGlobalResourceObject("resource", "Loan_Amount")%></div>
                                    <div class="controls">
                                        <asp:TextBox runat="server" ID="txtAmount" CssClass="form-control"> </asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group" style="display: none;">
                                    <div><%= GetGlobalResourceObject("resource", "Report_Type")%><span style="color: Red;">&nbsp;*</span></div>
                                    <div class="controls">
                                        <asp:RadioButtonList runat="server" ID="rblReport"></asp:RadioButtonList>
                                    </div>
                                </div>
                                <div runat="server" class="form-group" visible="false">
                                    <div><%= GetGlobalResourceObject("resource", "Status")%></div>
                                    <div class="controls">
                                        <asp:TextBox runat="server" ID="txtStatus" CssClass="form-control" TextMode="MultiLine" Rows="3"> </asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div><%= GetGlobalResourceObject("resource", "Remark")%></div>
                                    <div class="controls">
                                        <asp:TextBox runat="server" ID="txtRemark" CssClass="form-control" TextMode="MultiLine" Rows="3"> </asp:TextBox>
                                    </div>
                                </div>
                                <div runat="server" id="divUploadDoc">
                                    <div class="form-group">
                                        <div><%= GetGlobalResourceObject("resource", "Upload_Document")%></div>
                                        <div class="controls">
                                            <asp:Panel runat="server" ID="divUpload">
                                                <asp:FileUpload runat="server" ID="fu1" onchange="FileUpload('fu1')" class="bar" Width="100%" />
                                                <asp:Image runat="server" ID="img_fu1" ImageUrl="/Images/Doc_Icon.png" Width="50px" Visible="false" />
                                                <br />
                                                <asp:FileUpload runat="server" ID="fu2" onchange="FileUpload('fu2')" class="bar" Width="100%"/>
                                                <asp:Image runat="server" ID="img_fu2" ImageUrl="/Images/Doc_Icon.png" Width="50px" Visible="false" />
                                                <br />
                                                <asp:FileUpload runat="server" ID="fu3" onchange="FileUpload('fu3')" class="bar" Width="100%"/>
                                                <asp:Image runat="server" ID="img_fu3" ImageUrl="/Images/Doc_Icon.png" Width="50px" Visible="false" />
                                                <br />
                                                <asp:Button runat="server" ID="btnUpload" Style="display: none;" OnClick="btnUpload_Click" />
                                                <asp:HiddenField runat="server" ID="hid_FUID" />
                                            </asp:Panel>
                                        </div>
                                    </div>
                                    <div style="font-weight:bold">
                                    <span style="color: Red;">&nbsp;*</span>
                                        <%= GetGlobalResourceObject("resource","MaximumFileSize") %>
                                    </div>
                                    <br />
                                    <br />
                                    <div class="form-group">
                                        <div class="check">
                                            <div>
                                                <span>
                                                    *
                                                </span>
                                                <asp:Label ID="lblpricedetails" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                        <br />
                                    <div class="form-group check">
                                        <span>
                                            <input id="chkterms" type="checkbox" class='icheck-me' data-skin="square" data-color="blue"/>
                                        </span>
                                        <%= GetGlobalResourceObject("resource", "Request_TermsandCondition")%>&nbsp;
                                        <asp:LinkButton ID="btnDownload" OnClick="btnDownload_Click" style="color: #ff0000 ; font-size: 10px;" runat="server">(*Click here to download Consent Form.*)</asp:LinkButton>
                                        <asp:Label ID="lblReportType" style="color: #ff0000 ;font-size:10px"  runat="server"/> <%--Aaron--%>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <asp:Button ID="btnSignConsent" Text="Sign Consent Form" CssClass="btn btn-primary" OnClick="btnSignConsent_Click" runat="server" />
                                    <asp:Button ID="btnSend" Text="<%$ Resources:Resource, send%>" runat="server" CssClass="btn btn-primary" 
                                        OnClientClick="return confirmSubmit(); this.disabled = true; this.value = 'Loading...';" OnClick="btnSend_Click" />
                                    <asp:HiddenField runat="server" ID="hfMainReuseReport" />
                                    <asp:HiddenField runat="server" ID="hfCoReuseReport" />
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </section>
    </aside>


</asp:Content>
