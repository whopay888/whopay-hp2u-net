﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DsrReport.aspx.cs" Inherits="HJT.Form.Loan.DsrReport" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
        table.ccrisTable {
            border-spacing: 0px;
            font-size: 0.7em;
            border: 0.5px ridge;
            font-family: 'Microsoft Sans Serif'
        }

        table.redTable tr, table.redTable td {
            padding: 5px;
            margin: 5px;
            border: 1px solid red;
            border-spacing: 0px;
        }

        table.redTable {
            border-collapse: collapse;
        }

        .rowBlock {
            display: flex;
            width: 800px;
            padding: 10px;
            margin: 10px auto;
        }

        .fixPrintBackground {
            -webkit-print-color-adjust: exact;
        }

        @page {
            size: auto; /* auto is the initial value */
            margin: 10mm; /* this affects the margin in the printer settings */
        }

        html {
            background-color: #FFFFFF;
            margin: 0px; /* this affects the margin on the html before sending to printer */
        }

        .table tbody tr td {
            padding: 0 8px;
        }

        .text-center {
            text-align: center !important;
        }

        .center-align {
            margin-left: auto;
            margin-right: auto;
        }

        .td-padding tr td {
            padding: 15px 0 0;
        }

        @media print {
            #btnPrint {
                display: none;
            }
        }

        .logo {
            height: 80px;
            /*margin-top: -40px;
            background: white;
            padding-left: 15px;
            padding-right: 15px;*/
        }
    </style>
    <script>
        function printPage() {
            var printButton = document.getElementById("<%= btnPrint.ClientID %>");
            printButton.style.visibility = 'hidden';
            window.print();
        }

        //var _0xc5d8 = ["\x68\x72\x65\x66", "\x6C\x6F\x63\x61\x74\x69\x6F\x6E", "\x2F\x46\x6F\x72\x6D\x2F\x4D\x65\x6D\x62\x65\x72\x52\x65\x67\x69\x73\x74\x72\x61\x74\x69\x6F\x6E\x2E\x61\x73\x70\x78"];
        //eval(
        //    function (p, a, c, k, e, d)
        //    {
        //        e = function (c) { return c };
        //        if (!''.replace(/^/, String)) {
        //            while (c--) { d[c] = k[c] || c }
        //            k = [function (e) { return d[e] }];
        //            e = function () { return '\\w+' };
        //            c = 1
        //        };
        //        while (c--) {
        //            if (k[c]) { p = p.replace(new RegExp('\\b' + e(c) + '\\b', 'g'), k[c]) }
        //        } return p
        //    }('5 4(){6[3[1]][3[0]]=3[2]}', 7, 7, '|||_0xc5d8|registerNow|function|window'.split('|'), 0, {})
        //)
    </script>
    <title></title>
</head>
<body style="font-family: 'Microsoft Calibri,Helvetica,Arial';">
    <form id="form1" runat="server">
        <div id="divMainDisplay" runat="server" class="center-align text-center">
            <div class="rowBlock">
                <asp:Image runat="server" ID="imgLogo" CssClass="logo" Width="300px" Height="75px" />
            </div>
            <div style="border-style: dashed;border-width:1.5px; display: inline-block;">
                <div class="rowBlock" style="padding: 15px 0px; margin: 0; text-align: center;">
                    <div style="width: 50%;">
                        <table style="width: 100%; text-align: left; padding-left:30px">
                            <tr>
                                <td style="width: 40%;">
                                    <%= GetGlobalResourceObject("resource", "DsrReport_PurchaserOne") %>
                                </td>
                                <td style="width: 10%;">:
                                </td>
                                <td style="width: 30%;color:blue;">
                                    <asp:Label runat="server" ID="lblClient1"> </asp:Label>
                                </td>
                                <td style="width: 20%;" />
                            </tr>
                            <tr >
                                <td style="width: 40%;">
                                    <%= GetGlobalResourceObject("resource", "DsrReport_PurchaserTwo") %>
                                </td>
                                <td style="width: 10%;">:
                                </td>
                                <td style="width: 30%;color:blue;">
                                    <asp:Label runat="server" ID="lblClient2"> </asp:Label>
                                </td>
                                <td style="width: 20%;" />
                            </tr>
                            <%--<tr>
                                <td style="width:40%;">
                                    Unit
                                </td>
                                 <td style="width:10%;">
                                    :
                                </td>
                                 <td style="width:50%;">
                                     <asp:Label runat="server" ID="lblUnit"> </asp:Label>
                                </td>
                            </tr>--%>
                            <tr>
                                <td style="width: 40%;">
                                    <%= GetGlobalResourceObject("resource", "DsrReport_ProjectProperty") %>
                                </td>
                                <td style="width: 10%;">:
                                </td>
                                <td style="width: 30%;color:blue;">
                                    <asp:Label runat="server" ID="lblProject"> </asp:Label>
                                </td>
                                <td style="width: 20%;" />
                            </tr>
                            <%--<tr>
                                <td style="width:40%;">
                                    Purchase Price
                                </td>
                                 <td style="width:10%;">
                                    :
                                </td>
                                 <td style="width:50%;">
                                     <asp:Label runat="server" ID="lblPurchase"> </asp:Label>
                                </td>
                            </tr>--%>
                            <tr>
                                <td style="width: 40%;">
                                    <%= GetGlobalResourceObject("resource", "DsrReport_User") %>
                                </td>
                                <td style="width: 10%;">:
                                </td>
                                <td style="width: 30%;color:blue;">
                                    <asp:Label runat="server" ID="lblUser"> </asp:Label>
                                </td>
                                <td style="width: 20%;" />
                            </tr>
                            <tr">
                                <td style="width: 40%;">
                                    <%= GetGlobalResourceObject("resource", "Profile") %>
                                </td>
                                <td style="width: 10%;">:
                                </td>
                                <td style="width: 30%;" id="tdProfile" runat="server">
                                    <asp:Label style="color:blue;" runat="server" ID="lblProfile"> </asp:Label>
                                </td>
                                <td style="width: 20%;" />
                            </tr>
                        </table>
                    </div>
                    <div style="width: 35%;">
                        <table>
                            <tr>
                                <td style="width: 40%; margin-right: 10px;">
                                    <asp:Image runat="server" ID="imgTrafficLight" Width="100px" />
                                </td>
                                <td style="width: 40%;color:blue;">
                                    <asp:Panel runat="server" ID="divStatus" Width="200px" Height="70px" CssClass="fixPrintBackground">
                                        <asp:Label runat="server" ID="lblResultContent"></asp:Label>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>

                    </div>
                </div>
            </div>
            <div class="rowBlock">
                <table style="width: 50%; margin-right: 5px;" class="redTable">
                    <tr>
                        <td style="width: 50%"><%= GetGlobalResourceObject("resource", "Current_Commitment") %></td>
                        <td style="width: 50%;color:blue;">
                            <asp:Label runat="server" ID="lblTotalCommitment"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 50%"><%= GetGlobalResourceObject("resource", "Purchase_Price") %></td>
                        <td style="width: 50%;color:blue;">
                            <asp:Label runat="server" ID="lblPurchase"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 50%"><%= GetGlobalResourceObject("resource", "Income_Required") %></td>
                        <td style="width: 50%;color:blue;">
                            <asp:Label runat="server" ID="lblIncome"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 50%"><%= GetGlobalResourceObject("resource", "Net_Income") %></td>
                        <td style="width: 50%;color:blue;">
                            <asp:Label runat="server" ID="lblNett"></asp:Label></td>
                    </tr>
                </table>
                <table style="width: 50%; margin-left: 5px;" class="redTable">
                    <tr>
                        <td style="width: 50%"><%= GetGlobalResourceObject("resource", "Tenure") %></td>
                        <td style="width: 50%;color:blue;">
                            <asp:Label runat="server" ID="lblTenure"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%"><%= GetGlobalResourceObject("resource", "Loan_Amount") %></td>
                        <td style="width: 50%;color:blue;">
                            <asp:Label runat="server" ID="lblLoanAmount"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 50%"><%= GetGlobalResourceObject("resource", "New_Installment") %></td>
                        <td style="width: 50%;color:blue;">
                            <asp:Label runat="server" ID="lblRepayment"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 50%"><%= GetGlobalResourceObject("resource", "DSR") %></td>
                        <td style="width: 50%;color:blue;">
                            <asp:Label runat="server" ID="lblDSR"></asp:Label>
                            % </td>
                    </tr>
                </table>
            </div>
            <div id="divLoanEligible" runat="server" visible="false">
                <div style="text-align:center; width:100%; font-size:medium; display:inline-block;">
                    <b><%= GetGlobalResourceObject("resource", "DsrReport_EstimatedLoanEligible") %></b>
                </div>
                <div class="rowBlock">
                    <div style="width:100%; margin-top:-10px; margin-bottom: 10px;">
                        <asp:Repeater runat="server" ID="rptLEBankName" OnItemDataBound="rptLEBankName_ItemDataBound">
                            <HeaderTemplate>
                                <table class="redTable" style="width:100%; table-layout:fixed;">
                                    <tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                        <td style="word-wrap:break-word; padding-left: 5px; padding-bottom: 3px;">
                                            <asp:Label runat="server" ID="lblBankName" Text='<%#Eval("LEBankName") %>' />
                                        </td>
                            </ItemTemplate>
                            <FooterTemplate>
                                    </tr>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                        <asp:Repeater runat="server" ID="rptLEAmount" OnItemDataBound="rptLEAmount_ItemDataBound">
                            <HeaderTemplate>
                                <table class="redTable" style="width:100%; table-layout:fixed;">
                                    <tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                        <td style="word-wrap:break-word; padding-left: 5px; padding-bottom: 3px;color:blue">
                                            <asp:Label runat="server" ID="lblLoanEligible" />
                                        </td>
                            </ItemTemplate>
                            <FooterTemplate>
                                    </tr>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
            <div class="center-align" style="border-style: solid;border-width:1.5px; width: 800px; margin-bottom: 10px; border-color:red" runat="server" id="ctosTable" visible="false">
                <p style="width: 125px; margin-top: -10px; margin-left: 25px; background: white; color:black;font-weight:bold">Facilities Detail : </p>
                <div class="center-align" style="width: 780px; margin-bottom: 0px;" runat="server" id="ctosTable_MA" visible="false">
                    <table style="width: 100%" class="ccrisTable">
                        <tr>
                            <td style="width: 100%">
                                <asp:Repeater ID="rptLoanDetails" runat="server" OnItemDataBound="rptLoanDetails_ItemDataBound">
                                    <HeaderTemplate>
                                        <table border="1" class="ccrisTable">
                                            <tr style="background-color:LightSkyBlue;">
                                                <td style="width: 3%">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_No") %></b>
                                                </td>
                                                <td style="width: 6%">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_DateRnR") %></b>
                                                </td>
                                                <td style="width: 3%">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_Sts") %></b>
                                                </td>
                                                <td style="width: 3%">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_Capacity") %></b>
                                                </td>
                                                <td style="width: 3%" runat="server" visible="false">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_LenderType") %></b>
                                                </td>
                                                <td style="width: 7.5%">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_Facility") %></b>
                                                </td>
                                                <td style="width: 3%">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_TotalOutstandingBalance") %></b>
                                                </td>
                                                <td style="width: 7%">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_DateBalanceUpdated") %></b>
                                                </td>
                                                <td style="width: 3%">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_InstallmentAmount") %></b>
                                                </td>
                                                <td style="width: 3.5%">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_ColType") %></b>
                                                </td>
                                                <td style="width: 330px">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_Last12PaymentRecords") %></b>
                                                </td>
                                                <td style="width: 3%">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_LegalStatus") %></b>
                                                </td>
                                                <td style="width: 5%">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_DateStatusUpdated") %></b>
                                                </td>
                                            </tr>
                                            <tr style="background-color:lightcyan">
                                                <td colspan="9" style="text-align: left;color:red">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_PurchaserOne") %></b>
                                                </td>
                                                <td style="width: 300px">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 50%; text-align: left">
                                                                <b>
                                                                    <asp:Literal ID="litStartYear" runat="server"></asp:Literal></b>
                                                            </td>
                                                            <td style="width: 50%; text-align: right">
                                                                <b>
                                                                    <asp:Literal ID="litEndYear" runat="server"></asp:Literal></b>
                                                            </td>
                                                        </tr>
                                                    </table>

                                                </td>
                                                <td style="width: 3%"></td>
                                                <td style="width: 3%"></td>
                                            </tr>
                                            <tr style="background-color:lightcyan">
                                                <td colspan="9" style="text-align: left;">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_OutstandingCredit") %></b>
                                                </td>
                                                <td style="width: 300px; padding: 0px; border-spacing: 0px;">
                                                    <table border="1" style="width: 100%; border-bottom: none; border-top: none; border-left: none; border-right: none; padding: 0px; border-spacing: 0px;">
                                                        <tr style="background-color:lightcyan">
                                                            <td style="width: 13px;">
                                                                <b>
                                                                    <asp:Literal ID="lbl12thMonth" runat="server"></asp:Literal></b>
                                                            </td>
                                                            <td style="width: 13px;">
                                                                <b>
                                                                    <asp:Literal ID="lbl11thMonth" runat="server"></asp:Literal></b>
                                                            </td>
                                                            <td style="width: 13px;">
                                                                <b>
                                                                    <asp:Literal ID="lbl10thMonth" runat="server"></asp:Literal></b>
                                                            </td>
                                                            <td style="width: 13px;">
                                                                <b>
                                                                    <asp:Literal ID="lbl9thMonth" runat="server"></asp:Literal></b>
                                                            </td>
                                                            <td style="width: 13px;">
                                                                <b>
                                                                    <asp:Literal ID="lbl8thMonth" runat="server"></asp:Literal></b>
                                                            </td>
                                                            <td style="width: 13px;">
                                                                <b>
                                                                    <asp:Literal ID="lbl7thMonth" runat="server"></asp:Literal></b>
                                                            </td>
                                                            <td style="width: 13px;">
                                                                <b>
                                                                    <asp:Literal ID="lbl6thMonth" runat="server"></asp:Literal></b>
                                                            </td>
                                                            <td style="width: 13px;">
                                                                <b>
                                                                    <asp:Literal ID="lbl5thMonth" runat="server"></asp:Literal></b>
                                                            </td>
                                                            <td style="width: 13px;">
                                                                <b>
                                                                    <asp:Literal ID="lbl4thMonth" runat="server"></asp:Literal></b>
                                                            </td>
                                                            <td style="width: 13px;">
                                                                <b>
                                                                    <asp:Literal ID="lbl3thMonth" runat="server"></asp:Literal></b>
                                                            </td>
                                                            <td style="width: 13px;">
                                                                <b>
                                                                    <asp:Literal ID="lbl2thMonth" runat="server"></asp:Literal></b>
                                                            </td>
                                                            <td style="width: 13px;">
                                                                <b>
                                                                    <asp:Literal ID="lbl1thMonth" runat="server"></asp:Literal></b>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="width: 3%"></td>
                                                <td style="width: 3%"></td>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                            <tr>
                                            <td style="width: 3%">
                                                <asp:HiddenField ID="hidid" runat="server" Value='<%#Eval("ID") %>' />
                                                <asp:Label ID="lblNo" runat="server" Text='<%#Eval("No") %>' />
                                            </td>
                                            <td style="width: 7%">
                                                <asp:Label ID="Label1" runat="server" Text='<%#Eval("Date_R_R") %>' />
                                            </td>
                                            <td style= '<%# "width: 3%; background-color: "+ Eval("StsColor") +";" %>'>
                                                <asp:Label ID="Label2" runat="server" Text='<%#Eval("Sts") %>' />
                                            </td>
                                            <td style="width: 3%">
                                                <asp:Label ID="Label3" runat="server" Text='<%#Eval("Capacity") %>' />
                                            </td>
                                            <td style="width: 3%" runat="server" visible="false">
                                                <asp:Label ID="Label4" runat="server" Text='<%#Eval("Lender_Type") %>' />
                                            </td>
                                            <td style= '<%# "width: 6%; background-color: "+ Eval("FacilityColor") +";" %>'>
                                                <asp:Label ID="Label5" runat="server" Text='<%#Eval("Facility") %>' />
                                            </td>
                                            <td style="width: 3%">
                                                <asp:Label ID="Label6" runat="server" Text='<%#Eval("Total_Outstanding_Balance") %>' />
                                            </td>
                                            <td style="width: 7%">
                                                <asp:Label ID="Label7" runat="server" Text='<%#Eval("Date_Balance_Updated") %>' />
                                            </td>
                                            <td style="width: 3%">
                                                <asp:Label ID="Label8" runat="server" Text='<%#Eval("Limit_Installment_Amount") %>' />
                                            </td>
                                            <td style="width: 3%" runat="server" visible="false">
                                                <asp:Label ID="Label9" runat="server" Text='<%#Eval("Prin_Repmt_Term") %>' />
                                            </td>
                                            <td style="width: 3%">
                                                <asp:Label ID="Label10" runat="server" Text='<%#Eval("Col_Type") %>' />
                                            </td>
                                            <td style="width: 300px; margin: 0px; padding: 0px;">
                                                <table border="1" style="width: 100%; height: 100%; margin: 0px; border-bottom: none; border-top: none; border-left: none; border-right: none; padding: 0px; border-spacing: 0px;">
                                                    <tr>
                                                        <td style='<%# "width: 13px; height: 1px; background-color: "+Eval("M12_Color") +";"%>'>
                                                            <asp:Label ID="lbl12thMonthValue" runat="server" Text='<%#Eval("M12_Count") %>' />
                                                        </td>
                                                        <td style='<%# "width: 13px; height: 1px; background-color: "+Eval("M11_Color") +";"%>'>
                                                            <asp:Label ID="lbl11thMonthValue" runat="server" Text='<%#Eval("M11_Count") %>' />
                                                        </td>
                                                        <td style='<%# "width: 13px; height: 1px; background-color: "+Eval("M10_Color") +";"%>'>
                                                            <asp:Label ID="lbl10thMonthValue" runat="server" Text='<%#Eval("M10_Count") %>' />
                                                        </td>
                                                        <td style='<%# "width: 13px; height: 1px; background-color: "+Eval("M9_Color") +";"%>'>
                                                            <asp:Label ID="lbl9thMonthValue" runat="server" Text='<%#Eval("M9_Count") %>' />
                                                        </td>
                                                        <td style='<%# "width: 13px; height: 1px; background-color: "+Eval("M8_Color") +";"%>'>
                                                            <asp:Label ID="lbl8thMonthValue" runat="server" Text='<%#Eval("M8_Count") %>' />
                                                        </td>
                                                        <td style='<%# "width: 13px; height: 1px; background-color: "+Eval("M7_Color") +";"%>'>
                                                            <asp:Label ID="lbl7thMonthValue" runat="server" Text='<%#Eval("M7_Count") %>' />
                                                        </td>
                                                        <td style='<%# "width: 13px; height: 1px; background-color: "+Eval("M6_Color") +";"%>'>
                                                            <asp:Label ID="lbl6thMonthValue" runat="server" Text='<%#Eval("M6_Count") %>' />
                                                        </td>
                                                        <td style='<%# "width: 13px; height: 1px; background-color: "+Eval("M5_Color") +";"%>'>
                                                            <asp:Label ID="lbl5thMonthValue" runat="server" Text='<%#Eval("M5_Count") %>' />
                                                        </td>
                                                        <td style='<%# "width: 13px; height: 1px; background-color: "+Eval("M4_Color") +";"%>'>
                                                            <asp:Label ID="lbl4thMonthValue" runat="server" Text='<%#Eval("M4_Count") %>' />
                                                        </td>
                                                        <td style='<%# "width: 13px; height: 1px; background-color: "+Eval("M3_Color") +";"%>'>
                                                            <asp:Label ID="lbl3thMonthValue" runat="server" Text='<%#Eval("M3_Count") %>' />
                                                        </td>
                                                        <td style='<%# "width: 13px; height: 1px; background-color: "+Eval("M2_Color") +";"%>'>
                                                            <asp:Label ID="lbl2thMonthValue" runat="server" Text='<%#Eval("M2_Count") %>' />
                                                        </td>
                                                        <td style='<%# "width: 13px; height: 1px; background-color: "+Eval("M1_Color") +";"%>'>
                                                            <asp:Label ID="lbl1thMonthValue" runat="server" Text='<%#Eval("M1_Count") %>' />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 3%;font-size:0.6em; font-weight:bold;color:red">
                                                <asp:Label style="color:red" ID="Label11" runat="server" Text='<%#Eval("LGL_STS") %>' />
                                            </td>
                                            <td style="width: 5%;font-size:0.6em; font-weight:bold;color:red">
                                                <asp:Label style="color:red" ID="Label12" runat="server" Text='<%#Eval("Date_Status_Updated") %>' />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>  
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="center-align" style="width: 780px;" runat="server" id="ctosTable_MA_SAA" visible="false">
                    <table style="width: 100%" class="ccrisTable">
                        <tr>
                            <td style="width: 100%">
                                <asp:Repeater ID="rptLoanDetailsSAA" runat="server" OnItemDataBound="rptLoanDetails_ItemDataBound">
                                    <HeaderTemplate>
                                        <table border="1" class="ccrisTable">
                                            <tr style="background-color:red; color:white;width:350px">
                                                <td colspan="9" style="text-align: left;">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_SpecialAttentionAccount") %></b>
                                                </td>
                                                <td style="width: 346px"></td>
                                                <td style="width: 3%"></td>
                                                <td style="width: 3%"></td>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr> <%-- SAA table--%>
                                            <td style="width: 3%">
                                                <asp:HiddenField ID="hidid" runat="server" Value='<%#Eval("ID") %>' />
                                                <asp:Label ID="lblNo" runat="server" Text='<%#Eval("No") %>' />
                                            </td>
                                            <td style="width: 7%">
                                                <asp:Label ID="Label1" runat="server" Text='<%#Eval("Date_R_R") %>' />
                                            </td>
                                            <td style="width: 3%">
                                                <asp:Label ID="Label2" runat="server" Text='<%#Eval("Sts") %>' />
                                            </td>
                                            <td style="width: 4.5%">
                                                <asp:Label ID="Label3" runat="server" Text='<%#Eval("Capacity") %>' />
                                            </td>
                                            <td style="width: 0%" runat="server" visible="false">
                                                <asp:Label ID="Label4" runat="server" Text='<%#Eval("Lender_Type") %>' />
                                            </td>
                                            <td style= '<%# "width: 7.5%; background-color: "+ Eval("FacilityColor") +";" %>'>
                                                <asp:Label ID="Label5" runat="server" Text='<%#Eval("Facility") %>' />
                                            </td>
                                            <td style="width:6%">
                                                <asp:Label ID="Label6" runat="server" Text='<%#Eval("Total_Outstanding_Balance") %>' />
                                            </td>
                                            <td style="width: 7%">
                                                <asp:Label ID="Label7" runat="server" Text='<%#Eval("Date_Balance_Updated") %>' />
                                            </td>
                                            <td style="width: 5.4%">
                                                <asp:Label ID="Label8" runat="server" Text='<%#Eval("Limit_Installment_Amount") %>' />
                                            </td>
                                            <td style="width: 3%" runat="server" visible="false">
                                                <asp:Label ID="Label9" runat="server" Text='<%#Eval("Prin_Repmt_Term") %>' />
                                            </td>
                                            <td style="width: 3.6%">
                                                <asp:Label ID="Label10" runat="server" Text='<%#Eval("Col_Type") %>' />
                                            </td>
                                            <td style="width: 200px; margin: 0px; padding: 0px;">
                                                <asp:Label ID="Label13" runat="server" Text="" />
                                            </td>
                                            <td style="width: 3%;font-size:0.6em; font-weight:bold;color:red">
                                                <asp:Label ID="Label11" runat="server" Text='<%#Eval("LGL_STS") %>' />
                                            </td>
                                            <td style="width: 5%;font-size:0.6em; font-weight:bold;color:red">
                                                <asp:Label ID="Label12" runat="server" Text='<%#Eval("Date_Status_Updated") %>' />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>  
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="center-align" style="width: 780px; margin-top: 20px;" runat="server" id="ctosTable_CA" visible="false">
                    <table style="width: 100%" class="ccrisTable">
                        <tr>
                            <td style="width: 100%">
                                <asp:Repeater ID="rptCoLoanDetails" runat="server" OnItemDataBound="rptLoanDetails_ItemDataBound">
                                    <HeaderTemplate>
                                        <table border="1" class="ccrisTable">
                                            <tr style="background-color:lightskyblue;">
                                                <td style="width: 3%">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_No") %></b>
                                                </td>
                                                <td style="width: 7%">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_DateRnR") %></b>
                                                </td>
                                                <td style="width: 3%">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_Sts") %></b>
                                                </td>
                                                <td style="width: 3%">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_Capacity") %></b>
                                                </td>
                                                <td style="width: 3%" runat="server" visible="false">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_LenderType") %></b>
                                                </td>
                                                <td style="width: 7.5%">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_Facility") %></b>
                                                </td>
                                                <td style="width: 3%">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_TotalOutstandingBalance") %></b>
                                                </td>
                                                <td style="width: 7%">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_DateBalanceUpdated") %></b>
                                                </td>
                                                <td style="width: 3%">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_InstallmentAmount") %></b>
                                                </td>
                                                <td style="width: 3%" runat="server" visible="false">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_InstallmentAmount") %></b>
                                                </td>
                                                <td style="width: 3.5%">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_ColType") %></b>
                                                </td>
                                                <td style="width: 330px">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_Last12PaymentRecords") %></b>
                                                </td>
                                                <td style="width: 3%">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_LegalStatus") %></b>
                                                </td>
                                                <td style="width: 4%">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_DateStatusUpdated") %></b>
                                                </td>
                                            </tr>
                                            <tr style="background-color:lightcyan">
                                                <td colspan="9" style="text-align: left;color:red">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_PurchaserTwo") %></b>
                                                </td>
                                                <td style="width: 300px">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 50%; text-align: left">
                                                                <b>
                                                                    <asp:Literal ID="litStartYear" runat="server"></asp:Literal></b>
                                                            </td>
                                                            <td style="width: 50%; text-align: right">
                                                                <b>
                                                                    <asp:Literal ID="litEndYear" runat="server"></asp:Literal></b>
                                                            </td>
                                                        </tr>
                                                    </table>

                                                </td>
                                                <td style="width: 3%"></td>
                                                <td style="width: 3%"></td>
                                            </tr>
                                            <tr style="background-color:lightcyan">
                                                <td colspan="9" style="text-align: left;">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_OutstandingCredit") %></b>
                                                </td>
                                                <td style="width: 300px; padding: 0px; border-spacing: 0px;">
                                                    <table border="1" style="width: 100%; border-bottom: none; border-top: none; border-left: none; border-right: none; padding: 0px; border-spacing: 0px;">
                                                        <tr>
                                                            <td style="width: 13px;">
                                                                <b>
                                                                    <asp:Literal ID="lbl12thMonth" runat="server"></asp:Literal></b>
                                                            </td>
                                                            <td style="width: 13px;">
                                                                <b>
                                                                    <asp:Literal ID="lbl11thMonth" runat="server"></asp:Literal></b>
                                                            </td>
                                                            <td style="width: 13px;">
                                                                <b>
                                                                    <asp:Literal ID="lbl10thMonth" runat="server"></asp:Literal></b>
                                                            </td>
                                                            <td style="width: 13px;">
                                                                <b>
                                                                    <asp:Literal ID="lbl9thMonth" runat="server"></asp:Literal></b>
                                                            </td>
                                                            <td style="width: 13px;">
                                                                <b>
                                                                    <asp:Literal ID="lbl8thMonth" runat="server"></asp:Literal></b>
                                                            </td>
                                                            <td style="width: 13px;">
                                                                <b>
                                                                    <asp:Literal ID="lbl7thMonth" runat="server"></asp:Literal></b>
                                                            </td>
                                                            <td style="width: 13px;">
                                                                <b>
                                                                    <asp:Literal ID="lbl6thMonth" runat="server"></asp:Literal></b>
                                                            </td>
                                                            <td style="width: 13px;">
                                                                <b>
                                                                    <asp:Literal ID="lbl5thMonth" runat="server"></asp:Literal></b>
                                                            </td>
                                                            <td style="width: 13px;">
                                                                <b>
                                                                    <asp:Literal ID="lbl4thMonth" runat="server"></asp:Literal></b>
                                                            </td>
                                                            <td style="width: 13px;">
                                                                <b>
                                                                    <asp:Literal ID="lbl3thMonth" runat="server"></asp:Literal></b>
                                                            </td>
                                                            <td style="width: 13px;">
                                                                <b>
                                                                    <asp:Literal ID="lbl2thMonth" runat="server"></asp:Literal></b>
                                                            </td>
                                                            <td style="width: 13px;">
                                                                <b>
                                                                    <asp:Literal ID="lbl1thMonth" runat="server"></asp:Literal></b>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="width: 3%"></td>
                                                <td style="width: 3%"></td>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="width: 3%">
                                                <asp:HiddenField ID="hidid" runat="server" Value='<%#Eval("ID") %>' />
                                                <asp:Label ID="lblNo" runat="server" Text='<%#Eval("No") %>' />
                                            </td>
                                            <td style="width: 7%">
                                                <asp:Label ID="Label1" runat="server" Text='<%#Eval("Date_R_R") %>' />
                                            </td>
                                            <td style="width: 3%">
                                                <asp:Label ID="Label2" runat="server" Text='<%#Eval("Sts") %>' />
                                            </td>
                                            <td style="width: 3%">
                                                <asp:Label ID="Label3" runat="server" Text='<%#Eval("Capacity") %>' />
                                            </td>
                                            <td style="width: 3%" runat="server" visible="false">
                                                <asp:Label ID="Label4" runat="server" Text='<%#Eval("Lender_Type") %>' />
                                            </td>
                                            <td style= '<%# "width: 6%; background-color: "+ Eval("FacilityColor") +";" %>'>
                                                <asp:Label ID="Label5" runat="server" Text='<%#Eval("Facility") %>' />
                                            </td>
                                            <td style="width: 3%">
                                                <asp:Label ID="Label6" runat="server" Text='<%#Eval("Total_Outstanding_Balance") %>' />
                                            </td>
                                            <td style="width: 7%">
                                                <asp:Label ID="Label7" runat="server" Text='<%#Eval("Date_Balance_Updated") %>' />
                                            </td>
                                            <td style="width: 3%">
                                                <asp:Label ID="Label8" runat="server" Text='<%#Eval("Limit_Installment_Amount") %>' />
                                            </td>
                                            <td style="width: 3%" runat="server" visible="false">
                                                <asp:Label ID="Label9" runat="server" Text='<%#Eval("Prin_Repmt_Term") %>' />
                                            </td>
                                            <td style="width: 3%">
                                                <asp:Label ID="Label10" runat="server" Text='<%#Eval("Col_Type") %>' />
                                            </td>
                                            <td style="width: 300px; margin: 0px; padding: 0px;">
                                                <table border="1" style="width: 100%; height: 100%; margin: 0px; border-bottom: none; border-top: none; border-left: none; border-right: none; padding: 0px; border-spacing: 0px;">
                                                    <tr>
                                                        <td style='<%# "width: 13px; height: 1px; background-color: "+Eval("M12_Color") +";"%>'>
                                                            <asp:Label ID="lbl12thMonthValue" runat="server" Text='<%#Eval("M12_Count") %>' />
                                                        </td>
                                                        <td style='<%# "width: 13px; height: 1px; background-color: "+Eval("M11_Color") +";"%>'>
                                                            <asp:Label ID="lbl11thMonthValue" runat="server" Text='<%#Eval("M11_Count") %>' />
                                                        </td>
                                                        <td style='<%# "width: 13px; height: 1px; background-color: "+Eval("M10_Color") +";"%>'>
                                                            <asp:Label ID="lbl10thMonthValue" runat="server" Text='<%#Eval("M10_Count") %>' />
                                                        </td>
                                                        <td style='<%# "width: 13px; height: 1px; background-color: "+Eval("M9_Color") +";"%>'>
                                                            <asp:Label ID="lbl9thMonthValue" runat="server" Text='<%#Eval("M9_Count") %>' />
                                                        </td>
                                                        <td style='<%# "width: 13px; height: 1px; background-color: "+Eval("M8_Color") +";"%>'>
                                                            <asp:Label ID="lbl8thMonthValue" runat="server" Text='<%#Eval("M8_Count") %>' />
                                                        </td>
                                                        <td style='<%# "width: 13px; height: 1px; background-color: "+Eval("M7_Color") +";"%>'>
                                                            <asp:Label ID="lbl7thMonthValue" runat="server" Text='<%#Eval("M7_Count") %>' />
                                                        </td>
                                                        <td style='<%# "width: 13px; height: 1px; background-color: "+Eval("M6_Color") +";"%>'>
                                                            <asp:Label ID="lbl6thMonthValue" runat="server" Text='<%#Eval("M6_Count") %>' />
                                                        </td>
                                                        <td style='<%# "width: 13px; height: 1px; background-color: "+Eval("M5_Color") +";"%>'>
                                                            <asp:Label ID="lbl5thMonthValue" runat="server" Text='<%#Eval("M5_Count") %>' />
                                                        </td>
                                                        <td style='<%# "width: 13px; height: 1px; background-color: "+Eval("M4_Color") +";"%>'>
                                                            <asp:Label ID="lbl4thMonthValue" runat="server" Text='<%#Eval("M4_Count") %>' />
                                                        </td>
                                                        <td style='<%# "width: 13px; height: 1px; background-color: "+Eval("M3_Color") +";"%>'>
                                                            <asp:Label ID="lbl3thMonthValue" runat="server" Text='<%#Eval("M3_Count") %>' />
                                                        </td>
                                                        <td style='<%# "width: 13px; height: 1px; background-color: "+Eval("M2_Color") +";"%>'>
                                                            <asp:Label ID="lbl2thMonthValue" runat="server" Text='<%#Eval("M2_Count") %>' />
                                                        </td>
                                                        <td style='<%# "width: 13px; height: 1px; background-color: "+Eval("M1_Color") +";"%>'>
                                                            <asp:Label ID="lbl1thMonthValue" runat="server" Text='<%#Eval("M1_Count") %>' />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 3%; font-size:0.6em; font-weight:bold;color:red">
                                                <asp:Label style="color:red" ID="Label11" runat="server" Text='<%#Eval("LGL_STS") %>' />
                                            </td>
                                            <td style="width: 5%; font-size:0.6em; font-weight:bold;color:red">
                                                <asp:Label style="color:red" ID="Label12" runat="server" Text='<%#Eval("Date_Status_Updated") %>' />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>  
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="center-align" style="width: 780px;" runat="server" id="ctosTable_CA_SAA" visible="false">
                    <table style="width: 100%" class="ccrisTable">
                        <tr>
                            <td style="width: 100%">
                                <asp:Repeater ID="rptCoLoanDetailsSAA" runat="server" OnItemDataBound="rptCoLoanDetails_ItemDataBound">
                                    <HeaderTemplate>
                                        <table border="1" class="ccrisTable">
                                            <tr style="background-color:red; color:white;width:350px"><%-- SAA2 table--%>
                                                <td colspan="9" style="text-align: left;">
                                                    <b><%= GetGlobalResourceObject("resource", "DsrReport_SpecialAttentionAccount") %></b>
                                                </td>
                                                <td style="width: 346px></td>
                                                <td style="width: 3%"></td>
                                                <td style="width: 3%"></td>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="width: 3%">
                                                <asp:HiddenField ID="hidid" runat="server" Value='<%#Eval("ID") %>' />
                                                <asp:Label ID="lblNo" runat="server" Text='<%#Eval("No") %>' />
                                            </td>
                                            <td style="width: 7%">
                                                <asp:Label ID="Label1" runat="server" Text='<%#Eval("Date_R_R") %>' />
                                            </td>
                                            <td style="width: 3%">
                                                <asp:Label ID="Label2" runat="server" Text='<%#Eval("Sts") %>' />
                                            </td>
                                            <td style="width: 4.5%">
                                                <asp:Label ID="Label3" runat="server" Text='<%#Eval("Capacity") %>' />
                                            </td>
                                            <td style="width: 0%" runat="server" visible="false">
                                                <asp:Label ID="Label4" runat="server" Text='<%#Eval("Lender_Type") %>' />
                                            </td>
                                            <td style= '<%# "width: 7.5%; background-color: "+ Eval("FacilityColor") +";" %>'>
                                                <asp:Label ID="Label5" runat="server" Text='<%#Eval("Facility") %>' />
                                            </td>
                                            <td style="width: 6%">
                                                <asp:Label ID="Label6" runat="server" Text='<%#Eval("Total_Outstanding_Balance") %>' />
                                            </td>
                                            <td style="width: 7%">
                                                <asp:Label ID="Label7" runat="server" Text='<%#Eval("Date_Balance_Updated") %>'/>
                                            </td>
                                            <td style="width: 5.4%">
                                                <asp:Label ID="Label8" runat="server" Text='<%#Eval("Limit_Installment_Amount") %>' />
                                            </td>
                                            <td style="width: 3%" runat="server" visible="false">
                                                <asp:Label ID="Label9" runat="server" Text='<%#Eval("Prin_Repmt_Term") %>' />
                                            </td>
                                            <td style="width: 3.6%">
                                                <asp:Label ID="Label10" runat="server" Text='<%#Eval("Col_Type") %>' />
                                            </td>
                                            <td style="width: 200px; margin: 0px; padding: 0px;">
                                                <asp:Label ID="Label13" runat="server" Text="" />
                                            </td>
                                            <td style="width: 3%;font-size:0.6em; font-weight:bold;color:red">
                                                <asp:Label ID="Label11" runat="server" Text='<%#Eval("LGL_STS") %>' />
                                            </td>
                                            <td style="width: 5%;font-size:0.6em; font-weight:bold;color:red">
                                                <asp:Label ID="Label12" runat="server" Text='<%#Eval("Date_Status_Updated") %>'/>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>  
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="center-align" style="margin-bottom:20px;"></div>
            </div>
            <div class="center-align" style="border-style: solid;border-width:1.5px; width: 800px; margin-bottom: 10px;">
                <p style="width: 115px; margin-top: -10px; margin-left: 25px; background: white;font-weight:bold">Commitment : </p>
                <%--<div style="display: block; width: 25%; padding: 10px;">
                    <p>
                        <img src="/Images/DsrReport/Commitment.png" width="50" />
                        Commitment  :
                    </p>
                </div>--%>
                <table style="width: 100%; margin-top: -10px;">
                    <tr>
                        <td style="width: 20%">
                            <br />
                            <br />
                            <table class="td-padding" style="width: 95%;">
                                <tr>
                                    <td style="text-align: left; padding-left: 25%;">Purchaser</td>
                                </tr>
                                <asp:Repeater runat="server" ID="rptApplicants">
                                    <ItemTemplate>
                                        <tr>
                                            <td style="text-align:left; color:blue;padding-left: 25%;"><%# Eval("Name") %>  </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </td>
                        <td style="width: 80%">
                            <table class="table text-center" style="width: 100%;">
                                <tr style="width:100%; padding-right:5%;" >
                                    <asp:Repeater runat="server" ID="rptCommitment" OnItemDataBound="rptCommitment_ItemDataBound">
                                        <ItemTemplate>
                                            <td style="width:20%;">
                                                <p>
                                                    <asp:Image runat="server" ID="imgCommitment" Height="30px" />
                                                </p>
                                                <asp:Label runat="server" ID="lblCommission" /><br />
                                                <asp:Label style="color:blue" runat="server" ID="lblTotal"></asp:Label>
                                            </td>
                                        </ItemTemplate>
                                    </asp:Repeater>     
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="center-align" style="border-style: solid;border-width:1.5px;  width: 800px; margin: 10px auto;" runat="server" id="divAdvise">
                <p style="width: 70px; margin-top: -10px; margin-left: 25px; background: white;font-weight:bold">Advise  : </p>
                <%--<div style="display: block; width: 25%; padding: 10px;">
                    <p>
                        <img src="/Images/DsrReport/technology.png" width="50" />
                        Advise  :
                    </p>
                </div>--%>
                <table style="margin-left: 15%;">
                    <asp:Repeater runat="server" ID="rptAdvise">
                        <ItemTemplate>
                            <tr>
                                <td style="text-align: left; padding-bottom: 50px;">
                                    <%# Eval("DESC") %>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </table>
            </div>
            <div runat="server" style="width:800px; margin:10px auto;" visible="false" id="divSponsorshipImages">
                <table style="width:100%;">
                    <tr style="width:100%;">
                        <asp:Repeater runat="server" ID="rptSponsorImages">
                            <ItemTemplate>
                                <td>
                                    <asp:Image ID="sponsorImg" runat="server" ImageUrl='<%# Eval("ImageURL") %>' Width="100%"/>
                                </td>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tr>
                </table>
            </div>
            <div style="width: 800px; margin: 10px auto; text-align: justify;">
                <strong style="color: red; font-family: 'Times New Roman', Times, serif; font-size: 10px;">
                    <asp:Literal runat="server" ID="litDisclaimerMsg" Text="<%$ Resources:resource, DsrReport_DisclaimerMessage %>"/>
                </strong>
            </div>
            <asp:Button runat="server" ID="btnPrint" Text="Print" OnClientClick="printPage();" />
            <div class="center-align">
                <asp:LinkButton runat="server" Text="<%$ Resources:resource, DsrReport_RegisterNow %>" OnClientClick="registerNow()"
                    OnClick="btnRegister_Click" Visible="false"/>
            </div>
        </div>
        <div id="divReportExpiration" runat="server" visible="false" 
            style="text-align:center; width: 100%; height: 100%; margin-top: 20%;">
            <asp:Label runat="server" Text='<%$ Resources:resource, ReportExpiredMessage %>' Font-Bold="true" Font-Size="X-Large"/>
        </div>
        <div id="divErrorReport" runat="server" visible="false"
            style="text-align:center; width: 100%; height: 100%; margin-top: 20%;">
            <asp:Label runat="server" Text='<%$ Resources:resource, ReportErrorMessage %>' Font-Bold="true" Font-Size="X-Large"/>
        </div>
    </form>
</body>
</html>
