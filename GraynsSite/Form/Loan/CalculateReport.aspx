﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Global1.Master" CodeBehind="CalculateReport.aspx.cs" Inherits="HJT.Form.Loan.CalculateReport" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        table.blackTable tr,
        table.blackTable td {
            padding: 5px;
            margin: 5px;
            border: 1.5px solid black;
            border-spacing: 0px;
        }

        table.blackTable {
            border-collapse: collapse;
        }

        @media print {
            section.content {
                margin: 0;
                padding: 0;
                position: absolute;
                top: 25px;
                width: 100%;
            }

            #mainTable tr td {
                padding: 2px 1px;
                font-size: 12px
            }

                #mainTable tr td label, #mainTable tr td p, #mainTable tr td span {
                    font-size: 11px;
                    padding: 0;
                    margin: 0;
                }

            .box {
                margin: 0;
                padding: 0;
            }

            .btn {
                display: none;
            }
        }
    </style>
    <script>
        function confirmDelete() {
            return confirm('<%= GetGlobalResourceObject("resource","Confirm_Delete")%>');
        }

        function confirmSave() {
            return confirm('<%= GetGlobalResourceObject("resource","Confirm_Save_Result")%>');
        }

        function confirmSend() {
            return confirm('<%= GetGlobalResourceObject("resource","Confirm_Send")%>');
        }

        function printPage() {
            var printButton = document.getElementById("<%= btnPrint.ClientID %>");
            var saveButton = document.getElementById("<%= btnSave.ClientID %>");

            var sendButton = document.getElementById("<%= btnSend.ClientID %>");
            var divResult = document.getElementById("divResult");
            var divAdvisor = document.getElementById("divAdvisor");
            var mainTable = document.getElementById('mainTable');

            var inputs = mainTable.getElementsByTagName('input');
            for (var z = 0; z < inputs.length; z++) {

                if (inputs[z].type == "checkbox")
                    inputs[z].style.visibility = 'hidden';
                else if (inputs[z].type == "submit")
                    inputs[z].style.visibility = 'hidden';
            }
            printButton.style.visibility = 'hidden';
            saveButton.style.visibility = 'hidden';
            sendButton.style.visibility = 'hidden';
            divAdvisor.style.visibility = 'hidden';
            divResult.style.visibility = 'hidden';
            window.print();

            for (var z = 0; z < inputs.length; z++) {

                if (inputs[z].type == "checkbox")
                    inputs[z].style.visibility = 'visible';
                else if (inputs[z].type == "submit")
                    inputs[z].style.visibility = 'visible';
            }
            printButton.style.visibility = 'visible';
            saveButton.style.visibility = 'visible';
            sendButton.style.visibility = 'visible';
            divAdvisor.style.visibility = 'visible';
            divResult.style.visibility = 'visible';




        }


    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <aside class="right-side">
        <section class="content-header">
            <h1>
                <span class="icon fa fa-fw fa-book" aria-hidden="true"></span>
                <asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><span runat="server" id="spHeader"><%= GetGlobalResourceObject("resource", "Calculation_Summary")%></span></asp:Label>
            </h1>
            <asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
        </section>
        <section class="content">
            <div class="col-md-12 no-padding">
                <div runat="server">
                    <div class="box">
                        <div class="box-body">
                            <div class="form-group">
                                <div class="row print-aline">
                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <table class="blackTable">
                                                    <tr>
                                                        <td><%= GetGlobalResourceObject("resource","fullname")%></td>
                                                        <td>
                                                            <asp:Label runat="server" ID="lblNameMain"></asp:Label></td>
                                                        <td runat="server" id="trNameCo">
                                                            <asp:Label runat="server" ID="lblNameCo"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td><%= GetGlobalResourceObject("resource","ic")%></td>
                                                        <td>
                                                            <asp:Label runat="server" ID="lblICMain"></asp:Label></td>
                                                        <td runat="server" id="trICCo">
                                                            <asp:Label runat="server" ID="lblICCo"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td><%= GetGlobalResourceObject("resource","Total_Salary")%></td>
                                                        <td>
                                                            <asp:Label runat="server" ID="lblTotalSalaryMain"></asp:Label></td>
                                                        <td runat="server" id="trSalaryCo">
                                                            <asp:Label runat="server" ID="lblTotalSalaryCo"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td><%= GetGlobalResourceObject("resource","Project_Name")%></td>
                                                        <td colspan="2">
                                                            <asp:Label runat="server" ID="lblProjectName"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td><%= GetGlobalResourceObject("resource","unit")%></td>
                                                        <td colspan="2">
                                                            <asp:Label runat="server" ID="lblUnit"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td><%= GetGlobalResourceObject("resource","Purchase_Price")%></td>
                                                        <td colspan="2">
                                                            <asp:Label runat="server" ID="lblPurchase"></asp:Label></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <h4><%= GetGlobalResourceObject("resource","Comment")%></h4>
                                        <asp:TextBox runat="server" ID="txtComment" TextMode="MultiLine" Height="200px" Width="100%"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row print-aline">
                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <table class="blackTable">
                                                    <tr>
                                                        <td>
                                                            <h4><%= GetGlobalResourceObject("resource","Bank_Legal_Action")%></h4>
                                                        </td>
                                                        <td>
                                                            <h4><%= GetGlobalResourceObject("resource","Status")%></h4>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td><%= GetGlobalResourceObject("resource","CTOS")%></td>
                                                        <td>
                                                            <asp:Label runat="server" ID="lblCTOS"></asp:Label></td>
                                                    </tr>

                                                    <tr>
                                                        <td><%= GetGlobalResourceObject("resource","Special_Attention")%></td>
                                                        <td>
                                                            <asp:Label runat="server" ID="lblSpecialAttention"></asp:Label></td>
                                                    </tr>

                                                    <tr>
                                                        <td><%= GetGlobalResourceObject("resource","Restructuring")%></td>
                                                        <td>
                                                            <asp:Label runat="server" ID="lblRestructure"></asp:Label></td>
                                                    </tr>

                                                    <tr>
                                                        <td><%= GetGlobalResourceObject("resource","Dishonour_Cheque")%></td>
                                                        <td>
                                                            <asp:Label runat="server" ID="lblDisHonour"></asp:Label></td>
                                                    </tr>

                                                    <tr>
                                                        <td><%= GetGlobalResourceObject("resource","CCRIS_Attention")%></td>
                                                        <td>
                                                            <asp:Label runat="server" ID="lblCCRIS"></asp:Label></td>
                                                    </tr>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 no-print" id="divResult">
                                        <table>
                                            <tr>
                                                <td>
                                                    <h4><%= GetGlobalResourceObject("resource","Customer_Profile")%></h4>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RadioButtonList runat="server" ID="rblResult" RepeatDirection="Vertical" CssClass="table-report"></asp:RadioButtonList>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-4 no-print" id="divAdvisor" runat="server" visible="false">
                                        <table>
                                            <tr>
                                                <td>
                                                    <h4><%= GetGlobalResourceObject("resource","Credit_Advisor")%></h4>
                                                </td>
                                            </tr>
                                            <tr>
                                                
                                                <td>
                                                    <asp:CheckBoxList runat="server" ID="chkAdvisorList" CssClass="table-report"></asp:CheckBoxList>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-8">
                                        <h4><%= GetGlobalResourceObject("resource","Message")%></h4>
                                        <asp:TextBox runat="server" ID="txtMessage" TextMode="MultiLine" Rows="3" Width="100%"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive" runat="server" id="ccrisTable_MA" visible="false">
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 100%">
                                        <asp:Repeater ID="rptLoanDetails" runat="server" OnItemDataBound="rptLoanDetails_ItemDataBound">
                                            <HeaderTemplate>
                                                <table border="1">
                                                    <tr>
                                                        <td style="width: 3%; text-align:center">
                                                            <asp:CheckBox ID="chkMAshowall" runat="server" OnCheckedChanged="chkMAshowall_CheckedChanged" AutoPostBack="true"/>
                                                        </td>
                                                        <td colspan="14">
                                                            <b><asp:Literal runat="server" Text="<%$ Resources:Resource, ShowAllReport%>" /></b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 3%">
                                                            <b>Show in Report</b>
                                                        </td>
                                                        <td style="width: 3%">
                                                            <b>No</b>
                                                        </td>
                                                        <td style="width: 6%">
                                                            <b>Date/R&R</b>
                                                        </td>
                                                        <td style="width: 3%">
                                                            <b>Sts</b>
                                                        </td>
                                                        <td style="width: 3%">
                                                            <b>Capacity</b>
                                                        </td>
                                                        <td style="width: 3%">
                                                            <b>Lender Type</b>
                                                        </td>
                                                        <td style="width: 3%">
                                                            <b>Facility
                                                            </b>
                                                        </td>
                                                        <td style="width: 3%">
                                                            <b>Total Outstanding Balance (RM)</b>
                                                        </td>
                                                        <td style="width: 6%">
                                                            <b>Date Balance Updated</b>
                                                        </td>
                                                        <td style="width: 3%">
                                                            <b>Limit/ Installment Amount (AM)</b>
                                                        </td>
                                                        <td style="width: 3%">
                                                            <b>Print. Repmt Term</b>
                                                        </td>
                                                        <td style="width: 3%">
                                                            <b>Col Type</b>
                                                        </td>
                                                        <td style="width: 25%">
                                                            <b>Conduct Of Account For Last12 Months</b>
                                                        </td>
                                                        <td style="width: 3%">
                                                            <b>LGL STS</b>
                                                        </td>
                                                        <td style="width: 3%">
                                                            <b>Date Status Updated </b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="12">
                                                            <b><asp:Literal runat="server" Text="<%$ Resources:Resource, ApplicantType_MA%>" /></b>
                                                        </td>
                                                        <td style="width: 25%">
                                                            <table style="width: 100%">
                                                                <tr>
                                                                    <td style="width: 50%; text-align: left">
                                                                        <b>
                                                                            <asp:Literal ID="litStartYear" runat="server"></asp:Literal></b>
                                                                    </td>
                                                                    <td style="width: 50%; text-align: right">
                                                                        <b>
                                                                            <asp:Literal ID="litEndYear" runat="server"></asp:Literal></b>
                                                                    </td>
                                                                </tr>
                                                            </table>

                                                        </td>
                                                        <td style="width: 3%"></td>
                                                        <td style="width: 3%"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="12">
                                                            <b>Outstanding Credit</b>
                                                        </td>
                                                        <td style="width: 25%; padding: 0px; border-spacing: 0px;">
                                                            <table border="1" style="width: 100%; border-bottom: none; border-top: none; border-left: none; border-right: none; padding: 0px; border-spacing: 0px;">
                                                                <tr>
                                                                    <td style="width: 0%;">
                                                                        <b>
                                                                            <asp:Literal ID="lbl12thMonth" runat="server"></asp:Literal></b>
                                                                    </td>
                                                                    <td style="width: 0%;">
                                                                        <b>
                                                                            <asp:Literal ID="lbl11thMonth" runat="server"></asp:Literal></b>
                                                                    </td>
                                                                    <td style="width: 0%;">
                                                                        <b>
                                                                            <asp:Literal ID="lbl10thMonth" runat="server"></asp:Literal></b>
                                                                    </td>
                                                                    <td style="width: 0%;">
                                                                        <b>
                                                                            <asp:Literal ID="lbl9thMonth" runat="server"></asp:Literal></b>
                                                                    </td>
                                                                    <td style="width: 0%;">
                                                                        <b>
                                                                            <asp:Literal ID="lbl8thMonth" runat="server"></asp:Literal></b>
                                                                    </td>
                                                                    <td style="width: 0%;">
                                                                        <b>
                                                                            <asp:Literal ID="lbl7thMonth" runat="server"></asp:Literal></b>
                                                                    </td>
                                                                    <td style="width: 0%;">
                                                                        <b>
                                                                            <asp:Literal ID="lbl6thMonth" runat="server"></asp:Literal></b>
                                                                    </td>
                                                                    <td style="width: 0%;">
                                                                        <b>
                                                                            <asp:Literal ID="lbl5thMonth" runat="server"></asp:Literal></b>
                                                                    </td>
                                                                    <td style="width: 0%;">
                                                                        <b>
                                                                            <asp:Literal ID="lbl4thMonth" runat="server"></asp:Literal></b>
                                                                    </td>
                                                                    <td style="width: 0%;">
                                                                        <b>
                                                                            <asp:Literal ID="lbl3thMonth" runat="server"></asp:Literal></b>
                                                                    </td>
                                                                    <td style="width: 0%;">
                                                                        <b>
                                                                            <asp:Literal ID="lbl2thMonth" runat="server"></asp:Literal></b>
                                                                    </td>
                                                                    <td style="width: 0%;">
                                                                        <b>
                                                                            <asp:Literal ID="lbl1thMonth" runat="server"></asp:Literal></b>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="width: 3%"></td>
                                                        <td style="width: 3%"></td>
                                                    </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="width: 3%; text-align: center">
                                                        <asp:CheckBox ID="chkshowinreport" runat="server" />
                                                        <asp:HiddenField ID="hidshowinreport" runat="server" Value='<%#Eval("IsShowinReport") %>' />
                                                    </td>
                                                    <td style="width: 3%">
                                                        <asp:HiddenField ID="hidid" runat="server" Value='<%#Eval("ID") %>' />
                                                        <asp:Label ID="lblNo" runat="server" Text='<%#Eval("No") %>' />
                                                    </td>
                                                    <td style="width: 6%">
                                                        <asp:Label ID="Label1" runat="server" Text='<%#Eval("Date_R_R") %>' />
                                                    </td>
                                                    <td style="width: 3%">
                                                        <asp:Label ID="Label2" runat="server" Text='<%#Eval("Sts") %>' />
                                                    </td>
                                                    <td style="width: 3%">
                                                        <asp:Label ID="Label3" runat="server" Text='<%#Eval("Capacity") %>' />
                                                    </td>
                                                    <td style="width: 3%">
                                                        <asp:Label ID="Label4" runat="server" Text='<%#Eval("Lender_Type") %>' />
                                                    </td>
                                                    <td style="width: 3%">
                                                        <asp:Label ID="Label5" runat="server" Text='<%#Eval("Facility") %>' />
                                                    </td>
                                                    <td style="width: 3%">
                                                        <asp:Label ID="Label6" runat="server" Text='<%#Eval("Total_Outstanding_Balance") %>' />
                                                    </td>
                                                    <td style="width: 6%">
                                                        <asp:Label ID="Label7" runat="server" Text='<%#Eval("Date_Balance_Updated") %>' />
                                                    </td>
                                                    <td style="width: 3%">
                                                        <asp:Label ID="Label8" runat="server" Text='<%#Eval("Limit_Installment_Amount") %>' />
                                                    </td>
                                                    <td style="width: 3%">
                                                        <asp:Label ID="Label9" runat="server" Text='<%#Eval("Prin_Repmt_Term") %>' />
                                                    </td>
                                                    <td style="width: 3%">
                                                        <asp:Label ID="Label10" runat="server" Text='<%#Eval("Col_Type") %>' />
                                                    </td>
                                                    <td style="width: 25%; margin: 0px; padding: 0px;">
                                                        <table border="1" style="width: 100%; height: 100%; margin: 0px; border-bottom: none; border-top: none; border-left: none; border-right: none; padding: 0px; border-spacing: 0px;">
                                                            <tr>
                                                                <td style="width: 1%; height: 1px">
                                                                    <asp:Label ID="lbl12thMonthValue" runat="server" Text='<%#Eval("M12_Count") %>' />
                                                                </td>
                                                                <td style="width: 1%; height: 1px">
                                                                    <asp:Label ID="lb11thMonthValue" runat="server" Text='<%#Eval("M11_Count") %>' />
                                                                </td>
                                                                <td style="width: 1%; height: 1px">
                                                                    <asp:Label ID="lbl10thMonthValue" runat="server" Text='<%#Eval("M10_Count") %>' />
                                                                </td>
                                                                <td style="width: 1%; height: 1px">
                                                                    <asp:Label ID="lbl9thMonthValue" runat="server" Text='<%#Eval("M9_Count") %>' />
                                                                </td>
                                                                <td style="width: 1%; height: 1px">
                                                                    <asp:Label ID="lbl8thMonthValue" runat="server" Text='<%#Eval("M8_Count") %>' />
                                                                </td>
                                                                <td style="width: 1%; height: 1px">
                                                                    <asp:Label ID="lbl7thMonthValue" runat="server" Text='<%#Eval("M7_Count") %>' />
                                                                </td>
                                                                <td style="width: 1%; height: 1px">
                                                                    <asp:Label ID="lbl6thMonthValue" runat="server" Text='<%#Eval("M6_Count") %>' />
                                                                </td>
                                                                <td style="width: 1%; height: 1px">
                                                                    <asp:Label ID="lbl5thMonthValue" runat="server" Text='<%#Eval("M5_Count") %>' />
                                                                </td>
                                                                <td style="width: 1%; height: 1px">
                                                                    <asp:Label ID="lbl4thMonthValue" runat="server" Text='<%#Eval("M4_Count") %>' />
                                                                </td>
                                                                <td style="width: 1%; height: 1px">
                                                                    <asp:Label ID="lbl3thMonthValue" runat="server" Text='<%#Eval("M3_Count") %>' />
                                                                </td>
                                                                <td style="width: 1%; height: 1px;">
                                                                    <asp:Label ID="lbl2thMonthValue" runat="server" Text='<%#Eval("M2_Count") %>' />
                                                                </td>
                                                                <td style="width: 1%; height: 1px;">
                                                                    <asp:Label ID="lbl1thMonthValue" runat="server" Text='<%#Eval("M1_Count") %>' />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td style="width: 3%">
                                                        <asp:Label ID="Label11" runat="server" Text='<%#Eval("LGL_STS") %>' />
                                                    </td>
                                                    <td style="width: 3%">
                                                        <asp:Label ID="Label12" runat="server" Text='<%#Eval("Date_Status_Updated") %>' />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>  
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </table>
                            <asp:Button runat="server" Visible="false" ID="btnViewMACTOSReport" Text="View Main Applicant's CTOS Report" CssClass="btn btn-searh" Style="margin-left: 10px;" OnClick="btnViewMACTOSReport_Click" />
                            <asp:Button runat="server" Visible="false" ID="btnViewMARAMCIReport" Text="View Main Applicant's RAMCI Report" CssClass="btn btn-searh" Style="margin-left: 10px;" OnClick="btnViewMARAMCIReport_Click" />
                        </div>

                        <div class="table-responsive" runat="server" id="ccrisTable_CA" visible="false">
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 100%">
                                        <asp:Repeater ID="rptCoLoanDetails" runat="server" OnItemDataBound="rptCoLoanDetails_ItemDataBound">
                                            <HeaderTemplate>
                                                <table border="1">
                                                    <tr>
                                                        <td style="width: 3%; text-align:center">
                                                            <asp:CheckBox ID="chkCAshowall" runat="server" OnCheckedChanged="chkCAshowall_CheckedChanged" AutoPostBack="true" />
                                                        </td>
                                                        <td colspan="14">
                                                            <b><asp:Literal runat="server" Text="<%$ Resources:Resource, ShowAllReport%>" /></b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 3%">
                                                            <b>Show in Report</b>
                                                        </td>
                                                        <td style="width: 3%">
                                                            <b>No</b>
                                                        </td>
                                                        <td style="width: 6%">
                                                            <b>Date/R&R</b>
                                                        </td>
                                                        <td style="width: 3%">
                                                            <b>Sts</b>
                                                        </td>
                                                        <td style="width: 3%">
                                                            <b>Capacity</b>
                                                        </td>
                                                        <td style="width: 3%">
                                                            <b>Lender Type</b>
                                                        </td>
                                                        <td style="width: 3%">
                                                            <b>Facility
                                                            </b>
                                                        </td>
                                                        <td style="width: 3%">
                                                            <b>Total Outstanding Balance (RM)</b>
                                                        </td>
                                                        <td style="width: 6%">
                                                            <b>Date Balance Updated</b>
                                                        </td>
                                                        <td style="width: 3%">
                                                            <b>Limit/ Installment Amount (AM)</b>
                                                        </td>
                                                        <td style="width: 3%">
                                                            <b>Print. Repmt Term</b>
                                                        </td>
                                                        <td style="width: 3%">
                                                            <b>Col Type</b>
                                                        </td>
                                                        <td style="width: 25%">
                                                            <b>Conduct Of Account For Last12 Months</b>
                                                        </td>
                                                        <td style="width: 3%">
                                                            <b>LGL STS</b>
                                                        </td>
                                                        <td style="width: 3%">
                                                            <b>Date Status Updated </b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="12"><b>
                                                            <asp:Literal runat="server" Text="<%$ Resources:Resource, ApplicantType_CA%>" /></b></td>
                                                        <td style="width: 25%">
                                                            <table style="width: 100%">
                                                                <tr>
                                                                    <td style="width: 50%; text-align: left">
                                                                        <b>
                                                                            <asp:Literal ID="litStartYear" runat="server"></asp:Literal></b>
                                                                    </td>
                                                                    <td style="width: 50%; text-align: right">
                                                                        <b>
                                                                            <asp:Literal ID="litEndYear" runat="server"></asp:Literal></b>
                                                                    </td>
                                                                </tr>
                                                            </table>

                                                        </td>
                                                        <td style="width: 3%"></td>
                                                        <td style="width: 3%"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="12">
                                                            <b>Outstanding Credit</b>
                                                        </td>
                                                        <td style="width: 25%; padding: 0px; border-spacing: 0px;">
                                                            <table border="1" style="width: 100%; border-bottom: none; border-top: none; border-left: none; border-right: none; padding: 0px; border-spacing: 0px;">
                                                                <tr>
                                                                    <td style="width: 0%;">
                                                                        <b>
                                                                            <asp:Literal ID="lbl12thMonth" runat="server"></asp:Literal></b>
                                                                    </td>
                                                                    <td style="width: 0%;">
                                                                        <b>
                                                                            <asp:Literal ID="lbl11thMonth" runat="server"></asp:Literal></b>
                                                                    </td>
                                                                    <td style="width: 0%;">
                                                                        <b>
                                                                            <asp:Literal ID="lbl10thMonth" runat="server"></asp:Literal></b>
                                                                    </td>
                                                                    <td style="width: 0%;">
                                                                        <b>
                                                                            <asp:Literal ID="lbl9thMonth" runat="server"></asp:Literal></b>
                                                                    </td>
                                                                    <td style="width: 0%;">
                                                                        <b>
                                                                            <asp:Literal ID="lbl8thMonth" runat="server"></asp:Literal></b>
                                                                    </td>
                                                                    <td style="width: 0%;">
                                                                        <b>
                                                                            <asp:Literal ID="lbl7thMonth" runat="server"></asp:Literal></b>
                                                                    </td>
                                                                    <td style="width: 0%;">
                                                                        <b>
                                                                            <asp:Literal ID="lbl6thMonth" runat="server"></asp:Literal></b>
                                                                    </td>
                                                                    <td style="width: 0%;">
                                                                        <b>
                                                                            <asp:Literal ID="lbl5thMonth" runat="server"></asp:Literal></b>
                                                                    </td>
                                                                    <td style="width: 0%;">
                                                                        <b>
                                                                            <asp:Literal ID="lbl4thMonth" runat="server"></asp:Literal></b>
                                                                    </td>
                                                                    <td style="width: 0%;">
                                                                        <b>
                                                                            <asp:Literal ID="lbl3thMonth" runat="server"></asp:Literal></b>
                                                                    </td>
                                                                    <td style="width: 0%;">
                                                                        <b>
                                                                            <asp:Literal ID="lbl2thMonth" runat="server"></asp:Literal></b>
                                                                    </td>
                                                                    <td style="width: 0%;">
                                                                        <b>
                                                                            <asp:Literal ID="lbl1thMonth" runat="server"></asp:Literal></b>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="width: 3%"></td>
                                                        <td style="width: 3%"></td>
                                                    </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="width: 3%; text-align: center">
                                                        <asp:CheckBox ID="chkshowinreport" runat="server" />
                                                        <asp:HiddenField ID="hidshowinreport" runat="server" Value='<%#Eval("IsShowinReport") %>' />
                                                    </td>
                                                    <td style="width: 3%">
                                                        <asp:HiddenField ID="hidid" runat="server" Value='<%#Eval("ID") %>' />
                                                        <asp:Label ID="lblNo" runat="server" Text='<%#Eval("No") %>' />
                                                    </td>
                                                    <td style="width: 6%">
                                                        <asp:Label ID="Label1" runat="server" Text='<%#Eval("Date_R_R") %>' />
                                                    </td>
                                                    <td style="width: 3%">
                                                        <asp:Label ID="Label2" runat="server" Text='<%#Eval("Sts") %>' />
                                                    </td>
                                                    <td style="width: 3%">
                                                        <asp:Label ID="Label3" runat="server" Text='<%#Eval("Capacity") %>' />
                                                    </td>
                                                    <td style="width: 3%">
                                                        <asp:Label ID="Label4" runat="server" Text='<%#Eval("Lender_Type") %>' />
                                                    </td>
                                                    <td style="width: 3%">
                                                        <asp:Label ID="Label5" runat="server" Text='<%#Eval("Facility") %>' />
                                                    </td>
                                                    <td style="width: 3%">
                                                        <asp:Label ID="Label6" runat="server" Text='<%#Eval("Total_Outstanding_Balance") %>' />
                                                    </td>
                                                    <td style="width: 6%">
                                                        <asp:Label ID="Label7" runat="server" Text='<%#Eval("Date_Balance_Updated") %>' />
                                                    </td>
                                                    <td style="width: 3%">
                                                        <asp:Label ID="Label8" runat="server" Text='<%#Eval("Limit_Installment_Amount") %>' />
                                                    </td>
                                                    <td style="width: 3%">
                                                        <asp:Label ID="Label9" runat="server" Text='<%#Eval("Prin_Repmt_Term") %>' />
                                                    </td>
                                                    <td style="width: 3%">
                                                        <asp:Label ID="Label10" runat="server" Text='<%#Eval("Col_Type") %>' />
                                                    </td>
                                                    <td style="width: 25%; margin: 0px; padding: 0px;">
                                                        <table border="1" style="width: 100%; height: 100%; margin: 0px; border-bottom: none; border-top: none; border-left: none; border-right: none; padding: 0px; border-spacing: 0px;">
                                                            <tr>
                                                                <td style="width: 1%; height: 1px">
                                                                    <asp:Label ID="lbl12thMonthValue" runat="server" Text='<%#Eval("M12_Count") %>' />
                                                                </td>
                                                                <td style="width: 1%; height: 1px">
                                                                    <asp:Label ID="lbl11thMonthValue" runat="server" Text='<%#Eval("M11_Count") %>' />
                                                                </td>
                                                                <td style="width: 1%; height: 1px">
                                                                    <asp:Label ID="lbl10thMonthValue" runat="server" Text='<%#Eval("M10_Count") %>' />
                                                                </td>
                                                                <td style="width: 1%; height: 1px">
                                                                    <asp:Label ID="lbl9thMonthValue" runat="server" Text='<%#Eval("M9_Count") %>' />
                                                                </td>
                                                                <td style="width: 1%; height: 1px">
                                                                    <asp:Label ID="lbl8thMonthValue" runat="server" Text='<%#Eval("M8_Count") %>' />
                                                                </td>
                                                                <td style="width: 1%; height: 1px">
                                                                    <asp:Label ID="lbl7thMonthValue" runat="server" Text='<%#Eval("M7_Count") %>' />
                                                                </td>
                                                                <td style="width: 1%; height: 1px">
                                                                    <asp:Label ID="lbl6thMonthValue" runat="server" Text='<%#Eval("M6_Count") %>' />
                                                                </td>
                                                                <td style="width: 1%; height: 1px">
                                                                    <asp:Label ID="lbl5thMonthValue" runat="server" Text='<%#Eval("M5_Count") %>' />
                                                                </td>
                                                                <td style="width: 1%; height: 1px">
                                                                    <asp:Label ID="lbl4thMonthValue" runat="server" Text='<%#Eval("M4_Count") %>' />
                                                                </td>
                                                                <td style="width: 1%; height: 1px">
                                                                    <asp:Label ID="lbl3thMonthValue" runat="server" Text='<%#Eval("M3_Count") %>' />
                                                                </td>
                                                                <td style="width: 1%; height: 1px;">
                                                                    <asp:Label ID="lbl2thMonthValue" runat="server" Text='<%#Eval("M2_Count") %>' />
                                                                </td>
                                                                <td style="width: 1%; height: 1px;">
                                                                    <asp:Label ID="lbl1thMonthValue" runat="server" Text='<%#Eval("M1_Count") %>' />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td style="width: 3%">
                                                        <asp:Label ID="Label11" runat="server" Text='<%#Eval("LGL_STS") %>' />
                                                    </td>
                                                    <td style="width: 3%">
                                                        <asp:Label ID="Label12" runat="server" Text='<%#Eval("Date_Status_Updated") %>' />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>  
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </table>
                            <asp:Button runat="server" Visible="false" ID="btnViewCACTOSReport" Text="View Co Applicant's CTOS Report" CssClass="btn btn-searh" Style="margin-left: 10px;" OnClick="btnViewCACTOSReport_Click" />
                            <asp:Button runat="server" Visible="false" ID="btnViewCARAMCIReport" Text="View Co Applicant's RAMCI Report" CssClass="btn btn-searh" Style="margin-left: 10px;" OnClick="btnViewCARAMCIReport_Click" />
                        </div>

                    </div>

                    <div class="box">
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="mainTable">
                                    <tr>
                                        <td></td>
                                        <asp:Repeater runat="server" ID="rptHeader">
                                            <ItemTemplate>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="chkSelect" />
                                                    <p><%# Eval("BankName") %> </p>
                                                    <asp:HiddenField runat="server" ID="Hid_Bank" Value='<%# Eval("BankID") %>' Visible="false" />
                                                </td>
                                            </ItemTemplate>

                                        </asp:Repeater>
                                    </tr>
                                    <asp:Repeater runat="server" ID="rptApplicant" OnItemDataBound="rptApplicant_ItemDataBound">

                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <p><%# Eval("Name") %></p>
                                                    <asp:HiddenField runat="server" ID="Hid_ApplicantType" Value='<%#Eval("ID") %>' />
                                                </td>
                                            </tr>

                                            <asp:Repeater runat="server" ID="rptApplicantDetails" OnItemDataBound="rptApplicantDetails_ItemDataBound">
                                                <ItemTemplate>
                                                    <tr>
                                                        <asp:Literal runat="server" ID="loanRowData"></asp:Literal>

                                                    </tr>

                                                </ItemTemplate>
                                            </asp:Repeater>


                                        </ItemTemplate>
                                    </asp:Repeater>



                                    <tr>
                                        <td>&nbsp; </td>
                                    </tr>
                                    <tr>
                                        <td>DSR (%)</td>
                                        <asp:Literal runat="server" ID="LiDsr"></asp:Literal>
                                    </tr>
                                    <tr>
                                        <td><%= GetGlobalResourceObject("resource","Bank_Policy")%></td>
                                        <asp:Literal runat="server" ID="LiPolicy"></asp:Literal>
                                    </tr>
                                    <tr>
                                        <td><%= GetGlobalResourceObject("resource","Income_Required")%></td>
                                        <asp:Literal runat="server" ID="LiIncome"></asp:Literal>
                                    </tr>
                                    <tr>
                                        <td><%= GetGlobalResourceObject("resource","Loan_Eligible")%></td>
                                        <asp:Literal runat="server" ID="LiLoanEligible"></asp:Literal>
                                    </tr>
                                    <tr>
                                        <td><%= GetGlobalResourceObject("resource","New_Loan_Repayment")%></td>
                                        <asp:Literal runat="server" ID="LiRepayment"></asp:Literal>
                                    </tr>


                                    <tr>
                                        <td></td>
                                        <asp:Repeater runat="server" ID="rptFooter">
                                            <ItemTemplate>
                                                <td>

                                                    <asp:Button runat="server" ID="btnView" Text='<%#Eval("BankName") %>' CommandArgument='<%#Eval("BankID") %>' CssClass="btn btn-searh" OnClick="btnView_Click" />

                                                </td>
                                            </ItemTemplate>

                                        </asp:Repeater>
                                    </tr>


                                </table>

                            </div>
                        </div>


                        <div class="box-footer">
                            <asp:Button runat="server" ID="btnSave" Text="<%$ Resources:Resource, Save%>" CssClass="btn btn-submit" OnClick="btnSave_Click" OnClientClick="return confirmSave()" />
                            <asp:Button runat="server" ID="btnSend" Text="<%$ Resources:Resource, Finalise_Send%>" CssClass="btn btn-success" OnClientClick="return confirmSend();" OnClick="btnSend_Click" />

                            <asp:Button runat="server" ID="btnPrint" OnClientClick=" printPage(); return false;" CssClass="btn btn-export" Text="<%$ Resources:Resource, Print%>" />

                        </div>

                    </div>
                </div>
            </div>

        </section>
    </aside>


</asp:Content>

