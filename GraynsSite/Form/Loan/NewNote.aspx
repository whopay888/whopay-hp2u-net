﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewNote.aspx.cs" MasterPageFile="~/Global1.Master" Inherits="HJT.Form.Loan.NewNote" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function confirmSubmit() {
            return confirm('<%= GetGlobalResourceObject("resource" , "Confirm_submit") %>');
        }
        function setTab() {
            if ($("#HidTab").val() == '' || $("#HidTab").val() === undefined) {
                $("#HidTab").val("#tab_1");
            }
            $("a[data-toggle='tab'] ");

            $("a[href='" + $("#HidTab").val() + "' ]").click();
        }

        function pageLoad() {
            $("a[data-toggle='tab']").each(
                function (index) {
                    $(this).on("click", function () {
                        $("#HidTab").val($(this).attr("href"));

                    });
                }
            )
            setTab();
        }
    </script>
    <style>
        #divLoanDetail .table-bordered{
            width: 50%;
        }

        @media only screen and (max-width: 550px) {
            #divLoanDetail .table-bordered {
                width: 100%;
            }
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                <span class="icon fa fa-fw fa-book" aria-hidden="true"></span>
                <asp:Label ID="lblPageTitle" runat="server"><i class="icon-th-list"></i><span runat="server" id="spHeader"><%= GetGlobalResourceObject("resource", "Sequence_of_Events")%></span></asp:Label>
            </h1>
            <asp:SiteMapPath ID="SiteMapPath1" CssClass="breadcrumb" runat="server"></asp:SiteMapPath>
        </section>
        <section class="content">
            <div class="col-md-12">
                <div runat="server">
                    <div class="box">
                        <div id="divManagerMsg" runat="server" visible="false">
                            <asp:Label runat="server" Font-Size="Larger" Font-Bold="true">
                                <%=GetGlobalResourceObject("resource","ManagerACLMessage") %>
                            </asp:Label>
                        </div>
                        <div id="divErrorMessage" runat="server" visible="false" class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <b><%= GetGlobalResourceObject("resource", "Alert")%></b>
                            <br />
                            <asp:Label runat="server" ID="lblErr"></asp:Label>
                        </div>
                        <div runat="server" id="divBody" class="box-body">
                            <asp:HiddenField runat="server" ID="hfRequestID"/>
                            <div class="nav-tabs-custom">
                                <table class="table table-bordered" style="width:100%">
                                    <tr runat="server" visible="false" class="form-group" style="width:100%">
                                        <td style="width:15%"><%= GetGlobalResourceObject("resource", "RequestID")%></td>
                                        <td class="controls" style="width:85%">
                                            <asp:TextBox ReadOnly="true" runat="server" ID="txtRequestID" CssClass="form-control"> </asp:TextBox>
                                        </td>
                                        <td style="width:0%"></td>
                                    </tr>
                                    <tr class="form-group" style="width:100%">
                                        <td style="width:15%"><%= GetGlobalResourceObject("resource", "Applicant_Name")%></td>
                                        <td class="controls" style="width:85%">
                                            <asp:TextBox ReadOnly="true" runat="server" ID="txtApplicantName" CssClass="form-control"> </asp:TextBox>
                                        </td>
                                        <td style="width:0%"></td>
                                    </tr>
                                    <tr class="form-group" style="width:100%">
                                        <td style="width:15%"><%= GetGlobalResourceObject("resource", "Request_Date")%></td>
                                        <td class="controls" style="width:85%">
                                            <asp:TextBox ReadOnly="true" runat="server" ID="txtRequestDate" CssClass="form-control"> </asp:TextBox>
                                        </td>
                                        <td style="width:0%"></td>
                                    </tr>
                                    <tr class="form-group" style="width:100%">
                                        <td style="width:15%"><%= GetGlobalResourceObject("resource", "ProjectName")%></td>
                                        <td class="controls" style="width:85%">
                                            <asp:TextBox ReadOnly="true" runat="server" ID="txtProjectName" CssClass="form-control"> </asp:TextBox>
                                        </td>
                                        <td style="width:0%"></td>
                                    </tr>
                                    <tr class="form-group" runat="server" visible="false" style="width:100%">
                                        <td style="width:15%"><%= GetGlobalResourceObject("resource", "ApplicationResult")%></td>
                                        <td class="controls" style="width:85%">
                                            <asp:TextBox ReadOnly="true" runat="server" ID="txtApplicationResult" CssClass="form-control"> </asp:TextBox>
                                        </td>
                                        <td style="width:0%"></td>
                                    </tr>
                                </table>
                                <br />
                                <br />
                                <ul class="nav nav-tabs">
                                    <li class=""><a href="#tab_1" id="aTab1" data-toggle="tab" aria-expanded="false"><%= GetGlobalResourceObject("resource", "New_Note") %></a></li>
                                    <li class=""><a href="#tab_2" id="aTab2" data-toggle="tab" aria-expanded="false"><%= GetGlobalResourceObject("resource", "Note_History") %></a></li>
                                    <li class=""><a href="#tab_3" id="aTab3" data-toggle="tab" aria-expanded="false"><%= GetGlobalResourceObject("resource", "Recalculate") %></a></li>
                                    <asp:HiddenField runat="server" ID="HidTab" ClientIDMode="Static" />
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane" id="tab_1">
                                        <div runat="server" id="tab_1_content" visible="false">
                                            <table class="table table-bordered" style="width:100%">
                                                <tr class="form-group" style="width:100%">                                    
                                                    <td style="width:10%">
                                                        <%= GetGlobalResourceObject("resource", "Status")%>
                                                    </td>
                                                    <td style="width:90%">
                                                        <table style="width:100%">
                                                            <%--<tr style="width:100%">
                                                                <td style="width:100%">
                                                                    <%= GetGlobalResourceObject("resource", "Document")%>
                                                                </td>
                                                                <td style="width:25%">
                                                                    <%= GetGlobalResourceObject("resource", "Bank")%>
                                                                </td>
                                                                <td style="width:25%">
                                                                    <%= GetGlobalResourceObject("resource", "Letter_Offer")%>
                                                                </td>
                                                                <td style="width:25%">
                                                                    <%= GetGlobalResourceObject("resource", "Loan_Agreement")%>
                                                                </td>
                                                            </tr>--%>
                                                            <tr style="width:100%">
                                                                <td class="controls" style="width:100%">
                                                                    <asp:DropDownList runat="server" ID="ddlDocument"/>
                                                                </td>
                                                                <%--<td class="controls" style="width:25%">
                                                                    <asp:DropDownList runat="server" ID="ddlBank"/>
                                                                </td>
                                                                <td class="controls" style="width:25%">
                                                                    <asp:DropDownList runat="server" ID="ddlLetterOffer"/>
                                                                </td>
                                                                <td class="controls" style="width:25%">
                                                                    <asp:DropDownList runat="server" ID="ddlLoanAgreement"/>
                                                                </td>--%>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr class="form-group" style="width:100%">
                                                    <td style="width:10%"><%= GetGlobalResourceObject("resource", "Remark")%></td>
                                                    <td class="controls" style="width:90%">
                                                        <asp:TextBox ReadOnly="false" runat="server" ID="txtRemark" CssClass="form-control" 
                                                            TextMode="MultiLine" Rows="5"> </asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="box-footer">
                                                <asp:Button ID="btnSave" Text="<%$ Resources:Resource, Submit%>" runat="server" CssClass="btn btn-primary" 
                                                    OnClientClick="return confirmSubmit(); " OnClick="btnSave_Click" Visible="false" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_2">
                                        <div class="box-body box table-responsive" id="tab_2_content" runat="server" visible="false">
                                            <table>
                                                <tr runat="server" class="form-group" id="divPaymentStatus" visible="false">
                                                    <td style="width:20%"><%= GetGlobalResourceObject("resource","SPA_Signed") %></td>
                                                    <td style="width:20%">
                                                        <asp:Label runat="server" ID="lblPaymentStatus" Font-Bold="true" />
                                                    </td>
                                                    <td style="width:60%">
                                                        <asp:Button runat="server" ID="btnSuccess" Text="<%$ Resources:Resource, Set_As_Success%>" OnClick="btnSuccess_Click" 
                                                            OnClientClick="return confirmSubmit(); " CssClass="btn btn-view" Visible="false" />
                                                        <asp:Button runat="server" ID="btnFail" Text="<%$ Resources:Resource, Set_As_Fail%>" OnClick="btnFailed_Click" 
                                                            OnClientClick="return confirmSubmit(); " CssClass="btn btn-view" Visible="false" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:GridView AlternatingRowStyle-CssClass="table-alternateRow" ID="gv" runat="server" AutoGenerateColumns="false" GridLines="None" AllowPaging="True" ShowFooter="true"
                                                EmptyDataText="<%$ Resources:Resource, No_Record_Found%>" CssClass="table table-bordered" Width="100%" OnPageIndexChanging="gv_PageIndexChanging"
                                                AllowSorting="true" OnSorting="gv_Sorting">
                                                <PagerSettings Mode="NumericFirstLast" />
                                                <Columns>
                                                    <asp:BoundField HeaderText="<%$ Resources:Resource, Date%>" DataField="Date" SortExpression="Date" ItemStyle-Width="20%"/>
                                                    <asp:BoundField HeaderText="<%$ Resources:Resource, Status%>" DataField="Document" ItemStyle-Width="20%"/>
                                                    <%--<asp:BoundField HeaderText="<%$ Resources:Resource, Bank%>" DataField="Bank" ItemStyle-Width="10%"/>
                                                    <asp:BoundField HeaderText="<%$ Resources:Resource, Letter_Offer%>" DataField="LetterOffer" ItemStyle-Width="10%"/>
                                                    <asp:BoundField HeaderText="<%$ Resources:Resource, Loan_Agreement%>" DataField="LoanAgreement" ItemStyle-Width="10%"/>--%>
                                                    <asp:BoundField HeaderText="<%$ Resources:Resource, Remark%>" DataField="Remark" ItemStyle-Width="45%"/>
                                                    <asp:BoundField HeaderText="<%$ Resources:Resource, UpdatedBy%>" DataField="CreatedBy" ItemStyle-Width="15%" />
                                                </Columns>
                                                <PagerTemplate>
                                                    <div>
                                                        <div class="dataTables_paginate paging_bootstrap">
                                                            <ul class="pagination">
                                                                <li><asp:PlaceHolder runat="server" ID="phPreviousPaging"></asp:PlaceHolder></li>
                                                                <li><asp:PlaceHolder runat="server" ID="phPaging"></asp:PlaceHolder></li>
                                                                <li><asp:PlaceHolder runat="server" ID="phNextPaging"></asp:PlaceHolder></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </PagerTemplate>
                                            </asp:GridView>
                                            
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_3">
                                        <div id="tab_3_content" runat="server" visible="false">
                                            <asp:Panel runat="server" DefaultButton="btnSaveUserPage">
                                                <div>
                                                    <h4><b>
                                                        <asp:Literal runat="server" Text="<%$ Resources:resource, Request_Information %>" />
                                                    </b></h4>
                                                    <table class="table table-bordered" style="width:100%">
                                                        <tr class="form-group" style="width:100%">
                                                            <td style="width:20%"><b><%= GetGlobalResourceObject("resource", "Member_Name")%></b></td>
                                                            <td class="controls" style="width:80%">
                                                                <asp:Label runat="server" ID="lblMemberName" />
                                                            </td>
                                                        </tr>
                                                        <tr class="form-group" style="width:100%">
                                                            <td style="width:20%"><b><%= GetGlobalResourceObject("resource", "Branch")%></b></td>
                                                            <td class="controls" style="width:80%">
                                                                <asp:Label runat="server" ID="lblBranchName" />
                                                            </td>
                                                        </tr>
                                                        <tr class="form-group" style="width:100%">
                                                            <td style="width:20%"><b><%= GetGlobalResourceObject("resource", "Applicant_Information")%></b></td>
                                                            <td class="controls" style="width:80%">
                                                                <table style="width:100%;">
                                                                    <tr class="form-group" style="width:100%">
                                                                        <td style="width:15%"/>
                                                                        <td style="width:45%"><b><%= GetGlobalResourceObject("resource", "Applicant_Name")%></b></td>
                                                                        <td style="width:30%"><b><%= GetGlobalResourceObject("resource", "IC")%></b></td>
                                                                        <td style="width:10%"><b><%= GetGlobalResourceObject("resource", "CCRIS")%></b></td>
                                                                    </tr>
                                                                    <tr class="form-group" style="width:100%">
                                                                        <td style="width:15%"><b><%= GetGlobalResourceObject("resource", "Applicant")%> 1</b></td>
                                                                        <td class="controls" style="width:45%">
                                                                            <asp:Label runat="server" ID="lblMainApplicantName" />
                                                                        </td>
                                                                        <td class="controls" style="width:30%">
                                                                            <asp:Label runat="server" ID="lblMainApplicantIC" />
                                                                        </td>
                                                                        <td style="width:10%">
                                                                            <asp:Button runat="server" Visible="false" Text="<%$ Resources:Resource, Main_Applicant %>" 
                                                                                CssClass="btn btn-searh" OnClick="btnMainCCRIS_Click" Font-Size="Small" ID="btnUserMainCCRIS" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="form-group" style="width:100%" runat="server" visible="false" id="trUserCoApplicant">
                                                                        <td style="width:15%"><b><%= GetGlobalResourceObject("resource", "Applicant")%> 2</b></td>
                                                                        <td class="controls" style="width:45%">
                                                                            <asp:Label runat="server" ID="lblCoApplicantName" />
                                                                        </td>
                                                                        <td class="controls" style="width:30%">
                                                                            <asp:Label runat="server" ID="lblCoApplicantIC" />
                                                                        </td>
                                                                        <td style="width:10%">
                                                                            <asp:Button runat="server" Visible="false" Text="<%$ Resources:Resource, Co_Applicant %>" 
                                                                                CssClass="btn btn-searh" OnClick="btnCoCCRIS_Click" Font-Size="Small" ID="btnUserCoCCRIS" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr class="form-group" style="width:100%">
                                                            <td style="width:20%"><b><%= GetGlobalResourceObject("resource", "ApplicationResult")%></b></td>
                                                            <td class="controls" style="width:80%">
                                                                <asp:Label runat="server" ID="lblApplicationResult" />
                                                            </td>
                                                        </tr>
                                                        <tr class="form-group" style="width:100%">
                                                            <td style="width:20%"><b><%= GetGlobalResourceObject("resource", "ProjectName")%></b></td>
                                                            <td class="controls" style="width:80%">
                                                                <asp:Label runat="server" ID="lblProjectName" />
                                                            </td>
                                                        </tr>
                                                        <tr class="form-group" style="width:100%">
                                                            <td style="width:20%"><b><%= GetGlobalResourceObject("resource", "Advise")%></b></td>
                                                            <td class="controls" style="width:80%">
                                                                <asp:Label runat="server" ID="lblAdvise" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="divLoanDetail">
                                                    <h4><b>
                                                        <asp:Literal runat="server" Text="<%$ Resources:resource, Loan_Detail %>" />
                                                    </b></h4>
                                                    <table class="table table-bordered" style="float:left;">
                                                        <tr runat="server" class="form-group">
                                                            <td style="width:50%"><b><%= GetGlobalResourceObject("resource", "Current_Commitment")%></b></td>
                                                            <td class="controls" style="width:50%">
                                                                <asp:TextBox Text="N/A" runat="server" ID="txtCurrentCommitment" CssClass="form-control"> </asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" class="form-group">
                                                            <td style="width:50%"><b><%= GetGlobalResourceObject("resource", "Purchase_Price")%></b></td>
                                                            <td class="controls" style="width:50%">
                                                                <asp:TextBox runat="server" ID="txtPurchasePrice" CssClass="form-control"> </asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" class="form-group">
                                                            <td style="width:50%"><b><%= GetGlobalResourceObject("resource", "Income_Required")%></b></td>
                                                            <td class="controls" style="width:50%">
                                                                <asp:TextBox runat="server" ID="txtIncomeRequired" CssClass="form-control"> </asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" class="form-group">
                                                            <td style="width:50%"><b><%= GetGlobalResourceObject("resource", "Net_Income")%></b></td>
                                                            <td class="controls" style="width:50%">
                                                                <asp:TextBox runat="server" ID="txtNetIncome" CssClass="form-control"> </asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table class="table table-bordered" style="float:right;">
                                                        <tr runat="server" class="form-group">
                                                            <td style="width:50%"><b><%= GetGlobalResourceObject("resource", "Tenure")%></b></td>
                                                            <td class="controls" style="width:50%">
                                                                <asp:TextBox runat="server" ID="txtTenure" CssClass="form-control"> </asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" class="form-group">
                                                            <td style="width:50%"><b><%= GetGlobalResourceObject("resource", "DSR")%></b></td>
                                                            <td class="controls" style="width:50%">
                                                                <asp:TextBox runat="server" ID="txtDSR" CssClass="form-control"> </asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" class="form-group">
                                                            <td style="width:50%"><b><%= GetGlobalResourceObject("resource", "Loan_Amount")%></b></td>
                                                            <td class="controls" style="width:50%">
                                                                <asp:TextBox runat="server" ID="txtLoanAmount" CssClass="form-control"> </asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" class="form-group">
                                                            <td style="width:50%"><b><%= GetGlobalResourceObject("resource", "New_Installment")%></b></td>
                                                            <td class="controls" style="width:50%">
                                                                <asp:TextBox runat="server" ID="txtNewInstallment" CssClass="form-control"> </asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div style="margin-top: 10px; display:inline-block; width:100%;">
                                                        <asp:GridView ID="gvLoanEligible" runat="server" OnRowDataBound="gvLoanEligible_RowDataBound" 
                                                            AutoGenerateColumns="false" CssClass="table-gvLoanEligible">
                                                            <Columns>
                                                                <asp:BoundField HeaderText="<%$ Resources:Resource, ID %>" DataField="BankID" ItemStyle-Width="10%" 
                                                                    ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" />
                                                                <asp:BoundField HeaderText="<%$ Resources:Resource, Bank_Name %>" DataField="BankName" ItemStyle-Width="20%" />
                                                                <asp:TemplateField HeaderText="<%$ Resources:Resource, Loan_Eligible %>" ItemStyle-Width="50%">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox runat="server" ID="gvtxtLoanEligible" Width="100%" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                                <div class="box-footer">
                                                    <asp:Button ID="btnRecalculateUserPage" Text="<%$ Resources:Resource, Recalculate%>" runat="server" CssClass="btn btn-primary" OnClick="btnRecalculateUserPage_Click"/>
                                                    <asp:Button ID="btnSaveUserPage" Text="<%$ Resources:Resource, Save%>" runat="server" CssClass="btn btn-primary" OnClientClick="return confirmSubmit(); " OnClick="btnSaveUserPage_Click" />
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </aside>
</asp:Content>