﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Synergy.Model;
using Synergy.Util;

namespace WMElegance
{
    public partial class Login : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlCulture.Visible = false;
                Bind_Language();
            }
        }

        protected void ddlCulture_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (KeyVal.MultiLanguage)
            {

                if (Request.Cookies[KeyVal.cookie_language] != null)
                {
                    HttpCookie MyCookie = Request.Cookies[KeyVal.cookie_language];
                    MyCookie.Value = secure.Encrypt(ddlCulture.SelectedValue, true);
                    HttpContext.Current.Response.SetCookie(MyCookie);
                    HttpContext.Current.Response.Redirect(this.Request.RawUrl, true);
                }
                else
                {
                    DateTime exdate = DateTime.Now.AddYears(10);
                    HttpCookie MyCookie = new HttpCookie(KeyVal.cookie_language);
                    MyCookie.Value = secure.Encrypt(ddlCulture.SelectedValue, true);
                    MyCookie.Expires = exdate;
                    HttpContext.Current.Response.Cookies.Add(MyCookie);
                    HttpContext.Current.Response.Redirect(this.Request.RawUrl, true);
                }
            }
        }

        protected void Bind_Language()
        {
            DataTable dtCulture = LanguageUtil.LoadLanguageResources("language.resx");
            ddlCulture.DataTextField = "Key";
            ddlCulture.DataValueField = "Value";
            foreach (DataRow dr in dtCulture.Rows)
            {
                String value = LanguageUtil.LoadSingleResourceValue("LanguageLibrary.resx", dr["Key"].ToString());
                dr["Value"] = value;
            }
            ddlCulture.DataSource = dtCulture;
            ddlCulture.DataBind();

            ListItem liCulture = ddlCulture.Items.FindByValue(Thread.CurrentThread.CurrentCulture.Name);
            if (liCulture != null)
            {
                liCulture.Selected = true;
            }
        }
    }
}