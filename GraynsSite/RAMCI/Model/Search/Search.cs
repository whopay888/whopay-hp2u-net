﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace HJT.RAMCI.Model.Search
{
    [XmlRoot(ElementName = "item")]
    public class Item
    {
        [XmlElement(ElementName = "CRefId")]
        public string CRefId { get; set; }
        [XmlElement(ElementName = "EntityKey")]
        public string EntityKey { get; set; }
        [XmlElement(ElementName = "EntityId")]
        public string EntityId { get; set; }
        [XmlElement(ElementName = "EntityId2")]
        public string EntityId2 { get; set; }
        [XmlElement(ElementName = "EntityName")]
        public string EntityName { get; set; }
        [XmlElement(ElementName = "EntityDOBDOC")]
        public string EntityDOBDOC { get; set; }
        [XmlElement(ElementName = "EntityGroupCode")]
        public string EntityGroupCode { get; set; }
        [XmlElement(ElementName = "EntityState")]
        public string EntityState { get; set; }
        [XmlElement(ElementName = "EntityNationality")]
        public string EntityNationality { get; set; }
        [XmlElement(ElementName = "CcrisNote")]
        public string CcrisNote { get; set; }
    }

    [XmlRoot(ElementName = "ccris_identity")]
    public class Ccris_identity
    {
        [XmlElement(ElementName = "item")]
        public List<Item> Item { get; set; }
    }

    [XmlRoot(ElementName = "xml")]
    public class Xml
    {
        [XmlElement(ElementName = "ccris_identity")]
        public Ccris_identity Ccris_identity { get; set; }
    }

}