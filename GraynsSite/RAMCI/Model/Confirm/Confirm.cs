﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace HJT.RAMCI.Model.Confirm
{
    [XmlRoot(ElementName = "xml")]
    public class Xml
    {
        [XmlElement(ElementName = "token1")]
        public string Token1 { get; set; }
        [XmlElement(ElementName = "token2")]
        public string Token2 { get; set; }
    }


}