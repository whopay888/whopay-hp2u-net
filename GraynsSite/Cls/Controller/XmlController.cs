﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace HJT.Cls.Controller
{
    public class XmlController
    {
        public TARGET DeserializeXml<TARGET>(string Xml_Text)
        {
            object _object = null;
            try
            {
                using (TextReader reader = new StringReader(Xml_Text))
                {
                    //XmlDocument doc = new XmlDocument();
                    //doc.LoadXml(Xml_Text);

                    //string json = JsonConvert.SerializeXmlNode(doc);
                    //_object = JsonConvert.DeserializeObject<TARGET>(json);
                    XmlSerializer _serializer = new XmlSerializer(typeof(TARGET));
                    _object = (TARGET)_serializer.Deserialize(reader);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return (TARGET)Convert.ChangeType(_object, typeof(TARGET));
        }
    }
}