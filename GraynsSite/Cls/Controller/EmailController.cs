﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;

using Synergy.Model;
using Synergy.Util;

/// <summary>
/// Summary description for EmailController
/// </summary>
/// 
namespace Synergy.Controller
{
    public class EmailController : UtilityController
    {
        public EmailController()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public bool SendForgotPassword(String MemberID, String MemberName, String MemberEmail, String Password)
        {
            EmailTemplate emailTemplate = getEmailTemplate(EmailTypes.ForgotPassword);
            string strContent = emailTemplate.Message
                                             .Replace("#TEMPPWD#", secure.Decrypt(Password, true))
                                             .Replace("#FULLNAME#", MemberName)
                                             .Replace("#MEMBERCODE#", MemberID);

            sendSmtpEmail(EmailTypes.ForgotPassword, emailTemplate.Sender, MemberEmail, emailTemplate.Subject, null, strContent);
            return !HasError;
        }

        public bool SendOTP(String MemberEmail, String PassCode)
        {
            EmailTemplate emailTemplate = getEmailTemplate(EmailTypes.OTP);
            string strContent = emailTemplate.Message
                                             .Replace("#PASSCODE#", PassCode);

            sendSmtpEmail(EmailTypes.OTP, emailTemplate.Sender, MemberEmail, emailTemplate.Subject, null, strContent);
            return !HasError;
        }

        public bool SendReportNotification(String EmailAddress, string requestID)
        {
            EmailTemplate emailTemplate = getEmailTemplate(EmailTypes.ReportReady);
            wwdb db = new wwdb();
            string sql = null;

            sql = "SELECT tbl_Request.ApplicationResult AS ApplicationResult ";
            sql += "FROM tbl_Request INNER JOIN ";
            sql += "tbl_Applicants ON tbl_Request.RequestID = tbl_Applicants.RequestID ";
            sql += "AND tbl_Request.RequestID = '" + requestID + "'";

            db.OpenTable(sql);
            string customerProfile = null;

            if (db.Item("ApplicationResult") == "A")
                customerProfile = "Green";
            if (db.Item("ApplicationResult") == "M")
                customerProfile = "Yellow";
            if (db.Item("ApplicationResult") == "D")
                customerProfile = "Red";

            sql = "SELECT Name FROM tbl_Applicants INNER JOIN tbl_Request ON ";
            sql += "tbl_Applicants.RequestID = tbl_Request.RequestID ";
            sql += "AND tbl_Request.RequestID='" + requestID + "'";

            db.OpenTable(sql);
            string applicantName = db.Item("Name");
            sendSmtpEmail(EmailTypes.ReportReady, emailTemplate.Sender, EmailAddress, emailTemplate.Subject, null, emailTemplate.Message+" Applicant Name: "+applicantName+". Customer Profile: "+customerProfile);
     
            return !HasError;
        }

        public bool sendNewRequestNotification(String EmailAddress, string requestID)
        {
            EmailTemplate emailTemplate = getEmailTemplate(EmailTypes.NewRequest);

            wwdb db = new wwdb();
            string sql = null;

            sql = "SELECT tbl_MemberInfo.Fullname as 'Name', tbl_Branch.BranchName as 'Branch' ";
            sql += "FROM tbl_Branch, tbl_MemberInfo, tbl_Login, tbl_Request ";
            sql += "WHERE tbl_Branch.BranchID = tbl_Login.BranchID AND ";
            sql += "tbl_MemberInfo.MemberId = tbl_Request.MemberID AND ";
            sql += "tbl_Request.MemberID = tbl_Request.RequestBy AND ";
            sql += "tbl_Login.login_id = tbl_MemberInfo.MemberId AND ";
            sql += "tbl_Request.RequestID = '"+requestID+"'";

            db.OpenTable(sql);
            string customerName = db.Item("Name");
            string branchName = db.Item("Branch");
            sendSmtpEmail(EmailTypes.NewRequest, emailTemplate.Sender, EmailAddress, emailTemplate.Subject+"-"+customerName+"/"+branchName, null, emailTemplate.Message+" User name: "+customerName+". Branch: "+branchName);

            return !HasError;
        }

        public bool sendNewAdviseNotification(String EmailAddress)
        {
            EmailTemplate emailTemplate = getEmailTemplate(EmailTypes.NewApplicationAdvise);


            sendSmtpEmail(EmailTypes.NewApplicationAdvise, emailTemplate.Sender, EmailAddress, emailTemplate.Subject, null, emailTemplate.Message);

            return !HasError;
        }

        public void sendSmtpEmail( string emailtype, string strSender, string strRecipient, string strSubject, string[] strAttachment, string strContent)
        {

            AddEmailHistory(emailtype, strRecipient, strSubject, strContent);
            string ServerURL, ServerPort, EmailUsername, EmailPassword, EnableSSL;
            ServerURL = ConfigurationManager.AppSettings["EmailServer"];
            ServerPort = ConfigurationManager.AppSettings["EmailPort"];
            EmailUsername = ConfigurationManager.AppSettings["EmailUsername"];
            EmailPassword = secure.Decrypt(ConfigurationManager.AppSettings["EmailPassword"], true);
            EnableSSL = ConfigurationManager.AppSettings["EmailEnableSSL"];

            SmtpClient smtpClient = new SmtpClient(ServerURL, int.Parse(ServerPort));
            
            try
            {
                MailMessage msgMail = new MailMessage();
                //msgMail.From = new MailAddress(strSender.Trim());
                msgMail.From = new MailAddress(EmailUsername);
                msgMail.Subject = strSubject;
                msgMail.BodyEncoding = System.Text.Encoding.UTF8;

                if (strAttachment != null)
                {
                    bindAttachment(strAttachment, msgMail);
                }

                msgMail.IsBodyHtml = true;
                msgMail.To.Add(strRecipient);
                msgMail.Body = strContent;

                if (EnableSSL.ToLower().Equals("true"))
                    smtpClient.EnableSsl = true;
                //smtpClient.
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new System.Net.NetworkCredential(EmailUsername, EmailPassword);

                smtpClient.Send(msgMail);

            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.ToString(), "Error Sending Email");
                errorMsg = ex.Message;
            }
            finally
            {
                smtpClient.Dispose();
            }
        }

        private void AddEmailHistory(string emailtype, string strRecipient, string strSubject,  string strContent)
        {
            wwdb db = new wwdb();
            db.Execute(string.Format(@"

insert into tbl_emailhistory
(Email_TemplateId , Email_Subject , Email_Content , Email_To , Email_SentDate )
values
(N'{0}',N'{1}',N'{2}',N'{3}',getdate());", emailtype, strSubject, strContent, strRecipient));
            db.Close();
        }

        private MailMessage bindAttachment(string[] strAttachment, MailMessage message)
        {
            for (int intIdx = 0; intIdx <= strAttachment.Length - 1; intIdx++)
            {
                try
                {
                    // Create  the file attachment for this e-mail message.
                    Attachment data = new Attachment(strAttachment[intIdx]);
                    // Add the file attachment to this e-mail message.
                    message.Attachments.Add(data);
                }
                catch (Exception ex)
                {
                    errorMsg = ex.Message;
                }
            }

            return message;
        }
      
        private EmailTemplate getEmailTemplate(string strEmailType)
        {
            db = new wwdb();
            StringBuilder sbSQL = new StringBuilder();
            sbSQL.Append("SELECT TOP(1) * FROM tbl_Emailtemplate ")
           .AppendFormat("WHERE EmailType='{0}'", strEmailType);

            //Add applicant name

            DataTable dt = db.getDataTable(sbSQL.ToString());
            db.Close();
            if (!db.HasError)
            {
                return bindEmailTemplate(dt);
            }
            else
            {
                errorMsg = db.ErrorMessage;
                return null;
            }

        }

        private EmailTemplate bindEmailTemplate(DataTable dt)
        {
            EmailTemplate obj = new EmailTemplate();
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                //if (dr["RowID"] != DBNull.Value) { obj.RowID = (int)dr["RowID"]; }
                //if (dr["Sender"] != DBNull.Value) { obj.Sender = (string)dr["Sender"]; }
                //if (dr["Subject"] != DBNull.Value) { obj.Subject = (string)dr["Subject"]; }
                //if (dr["Message"] != DBNull.Value) { obj.Message = (string)dr["Message"]; }
                //if (dr["emailType"] != DBNull.Value) { obj.emailType = (int)dr["emailType"]; }
                //if (dr["isActive"] != DBNull.Value) { obj.isActive = (bool)dr["isActive"]; }
                //if (dr["isDeleted"] != DBNull.Value) { obj.isDeleted = (bool)dr["isDeleted"]; }
                //if (dr["CreatedBy"] != DBNull.Value) { obj.CreatedBy = (string)dr["CreatedBy"]; }
                //if (dr["CreatedAt"] != DBNull.Value) { obj.CreatedAt = (DateTime)dr["CreatedAt"]; }
                //if (dr["UpdatedBy"] != DBNull.Value) { obj.UpdatedBy = (DateTime)dr["UpdatedBy"]; }
                //if (dr["UpdatedAt"] != DBNull.Value) { obj.UpdatedAt = (DateTime)dr["UpdatedAt"]; }

                if (dr["Id"] != DBNull.Value) { obj.RowID = (int)dr["Id"]; }
                if (dr["Sender"] != DBNull.Value) { obj.Sender = (string)dr["Sender"]; }
                if (dr["Subjectx"] != DBNull.Value) { obj.Subject = (string)dr["Subjectx"]; }
                if (dr["Messagex"] != DBNull.Value) { obj.Message = (string)dr["Messagex"]; }
                if (dr["EmailType"] != DBNull.Value) { obj.emailType = (int)dr["EmailType"]; }
                //if (dr["isActive"] != DBNull.Value) { obj.isActive = (bool)dr["isActive"]; }
                if (dr["isDeleted"] != DBNull.Value) { obj.isDeleted = (bool)dr["isDeleted"]; }
                if (dr["CreatedBy"] != DBNull.Value) { obj.CreatedBy = (string)dr["CreatedBy"]; }
                if (dr["CreatedAt"] != DBNull.Value) { obj.CreatedAt = (DateTime)dr["CreatedAt"]; }
                if (dr["UpdatedBy"] != DBNull.Value) { obj.UpdatedBy = (string)dr["UpdatedBy"]; }
                if (dr["UpdatedAt"] != DBNull.Value) { obj.UpdatedAt = (DateTime)dr["UpdatedAt"]; }
                return obj;
            }
            else
                return null;
        }

    }

    public class EmailTypes
    {
        public static string ForgotPassword
        {
            get { return "3"; }
        }

        public static string ReportReady
        {
            get { return "6"; }
        }


        public static string NewRequest
        {
            get { return "7"; }
        }

        public static string NewApplicationAdvise
        {
            get { return "8"; }
        }

        public static string OTP
        {
            get { return "4"; }
        }
    }

}

