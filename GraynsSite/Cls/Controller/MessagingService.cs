﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Synergy.Util;

namespace Synergy.Controller
{
    public class MessagingService: UtilityController
    {
        public MessagingService(){
            //constructor
        }

        private string getApplicantName(string requestID) {
            wwdb db = new wwdb();
            string sql = null;

            sql = "SELECT Name FROM tbl_Applicants INNER JOIN tbl_Request ON ";
            sql += "tbl_Applicants.RequestID = tbl_Request.RequestID ";
            sql += "AND tbl_Request.RequestID='"+requestID+"'";
            
            db.OpenTable(sql);
            return db.Item("Name");
        }
        private string getApplicationResult(string requestID) {
            wwdb db = new wwdb();
            string sql = null;

            sql = "SELECT tbl_Request.ApplicationResult AS ApplicationResult ";
            sql += "FROM tbl_Request INNER JOIN ";
            sql += "tbl_Applicants ON tbl_Request.RequestID = tbl_Applicants.RequestID ";
            sql += "AND tbl_Request.RequestID = '"+requestID+"'";

            db.OpenTable(sql);
            string result = null;

            if (db.Item("ApplicationResult")=="A")
                result = "Green";
            if (db.Item("ApplicationResult") == "M")
                result = "Yellow";
            if (db.Item("ApplicationResult") == "D")
                result = "Red";

            return result;
        }
        private string getMemberName(string requestID)
        {
            wwdb db = new wwdb();
            string sql = null;

            sql = "SELECT tbl_MemberInfo.Fullname as 'Name' ";
            sql += "FROM tbl_Branch, tbl_MemberInfo, tbl_Login, tbl_Request ";
            sql += "WHERE tbl_Branch.BranchID = tbl_Login.BranchID AND ";
            sql += "tbl_MemberInfo.MemberId = tbl_Request.MemberID AND ";
            sql += "tbl_Request.MemberID = tbl_Request.RequestBy AND ";
            sql += "tbl_Login.login_id = tbl_MemberInfo.MemberId AND ";
            sql += "tbl_Request.RequestID = '" + requestID + "'";

            db.OpenTable(sql);
            string fullName = db.Item("Name");

            return fullName;
        }
        private string getMemberBranchName(string requestID)
        {
            wwdb db = new wwdb();
            string sql = null;

            sql = "SELECT tbl_Branch.BranchName as 'Branch' ";
            sql += "FROM tbl_Branch, tbl_MemberInfo, tbl_Login, tbl_Request ";
            sql += "WHERE tbl_Branch.BranchID = tbl_Login.BranchID AND ";
            sql += "tbl_MemberInfo.MemberId = tbl_Request.MemberID AND ";
            sql += "tbl_Request.MemberID = tbl_Request.RequestBy AND ";
            sql += "tbl_Login.login_id = tbl_MemberInfo.MemberId AND ";
            sql += "tbl_Request.RequestID = '" + requestID + "'";

            db.OpenTable(sql);
            string branchName = db.Item("Branch");

            return branchName;
        }

        public void sendReportNotification(string mobileNo, string requestID)
        {
            HttpWebRequest myWebRequest = null/* TODO Change to default(_) if this is not a reference type */;
            HttpWebResponse myWebResponse = null/* TODO Change to default(_) if this is not a reference type */;
            try
            {
                string sURL = "http://gateway.onewaysms.com.my:10001/api.aspx";
                string apiUsername = ConfigurationManager.AppSettings["SMSUsername"];
                string apiPassword = ConfigurationManager.AppSettings["SMSPassword"];
                string apiSenderID = ConfigurationManager.AppSettings["SMSSenderID"];
                sURL = sURL + "?apiusername=" + HttpUtility.UrlEncode(apiUsername);
                sURL = sURL + "&apipassword=" + HttpUtility.UrlEncode(apiPassword);
                sURL = sURL + "&mobileno=" + HttpUtility.UrlEncode(mobileNo);
                sURL = sURL + "&senderid=" + HttpUtility.UrlEncode(apiSenderID);
                sURL = sURL + "&languagetype=" + "1";
                string applicantName = getApplicantName(requestID);
                string applicationResult = getApplicationResult(requestID);
                sURL = sURL + "&message=" + HttpUtility.UrlEncode("WhoPay: "+applicantName+"'s Report is ready. Customer Profile: "+applicationResult+". Thank you."); // message content
                myWebRequest = (HttpWebRequest)WebRequest.Create(sURL);
                myWebResponse = (HttpWebResponse)myWebRequest.GetResponse();
                if (myWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    Stream oStream = myWebResponse.GetResponseStream();
                    StreamReader oReader = new StreamReader(oStream);
                    string sResult = oReader.ReadToEnd();
                    
                    //if (long.Parse(sResult) > 0)
                    //    Response.Write("Success - MT ID :" + sResult);
                    //else if (long.Parse(sResult) == -100)
                    //    Response.Write("Failed - Invalid username or password.");
                    //else if (long.Parse(sResult) == -200)
                    //    Response.Write("Failed - Invalid sender ID.");
                    //else if (long.Parse(sResult) == -300)
                    //    Response.Write("Failed - Invalid mobile number.");
                    //else if (long.Parse(sResult) == -400)
                    //    Response.Write("Failed - Invalid language type (only 1 or 2).");
                    //else if (long.Parse(sResult) == -500)
                    //    Response.Write("Failed - Invalid character(s) or vulgar word(s) detected in message.");
                    //else if (long.Parse(sResult) == -600)
                    //    Response.Write("Failed - Insufficient credit balance.");
                    //else
                    //    Response.Write("fail - Error code :" + sResult);
                }
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.Message, "Error sending message. RequestID:" + requestID);
                //Response.Write("Some issue happen: "+ex);
            }
            finally
            {
                if (myWebResponse != null)
                    myWebResponse.Close();
            }
        }
        public void sendNewRequestNotification(string mobileNo, string requestID) {
            HttpWebRequest myWebRequest = null/* TODO Change to default(_) if this is not a reference type */;
            HttpWebResponse myWebResponse = null/* TODO Change to default(_) if this is not a reference type */;
            try
            {
                string sURL = "http://gateway.onewaysms.com.my:10001/api.aspx";
                string apiUsername = ConfigurationManager.AppSettings["SMSUsername"];
                string apiPassword = ConfigurationManager.AppSettings["SMSPassword"];
                string apiSenderID = ConfigurationManager.AppSettings["SMSSenderID"];
                sURL = sURL + "?apiusername=" + HttpUtility.UrlEncode(apiUsername);
                sURL = sURL + "&apipassword=" + HttpUtility.UrlEncode(apiPassword);
                sURL = sURL + "&mobileno=" + HttpUtility.UrlEncode(mobileNo);
                sURL = sURL + "&senderid=" + HttpUtility.UrlEncode(apiSenderID);
                sURL = sURL + "&languagetype=" + "1";

                string branchName = getMemberBranchName(requestID);
                string userName = getMemberName(requestID);
                if (branchName.Equals(""))
                    branchName = "WhoPay";
                if (userName.Equals(""))
                    userName = "Admin";
                sURL = sURL + "&message=" + HttpUtility.UrlEncode("WhoPay: "+userName+" from "+branchName+" has sent a report request."); // message content
                myWebRequest = (HttpWebRequest)WebRequest.Create(sURL);
                myWebResponse = (HttpWebResponse)myWebRequest.GetResponse();
                if (myWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    Stream oStream = myWebResponse.GetResponseStream();
                    StreamReader oReader = new StreamReader(oStream);
                    string sResult = oReader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.Message, "Error sending message. RequestID:" + requestID);
                //Response.Write("Some issue happen: "+ex);
            }
            finally
            {
                if (myWebResponse != null)
                    myWebResponse.Close();
            }
        }
        public void sendInvalidFileNotification(string mobileNo)
        {
            HttpWebRequest myWebRequest = null/* TODO Change to default(_) if this is not a reference type */;
            HttpWebResponse myWebResponse = null/* TODO Change to default(_) if this is not a reference type */;
            try
            {
                string sURL = "http://gateway.onewaysms.com.my:10001/api.aspx";
                string apiUsername = ConfigurationManager.AppSettings["SMSUsername"];
                string apiPassword = ConfigurationManager.AppSettings["SMSPassword"];
                string apiSenderID = ConfigurationManager.AppSettings["SMSSenderID"];
                sURL = sURL + "?apiusername=" + HttpUtility.UrlEncode(apiUsername);
                sURL = sURL + "&apipassword=" + HttpUtility.UrlEncode(apiPassword);
                sURL = sURL + "&mobileno=" + HttpUtility.UrlEncode(mobileNo);
                sURL = sURL + "&senderid=" + HttpUtility.UrlEncode(apiSenderID);
                sURL = sURL + "&languagetype=" + "1";

                StringBuilder content = new StringBuilder();
                content.Append("WhoPay:\n");
                content.Append("Please provide us Customer consent.\n");
                content.Append("- IC front & back \n");
                content.Append("- Consent form or Sales form. \n\n");
                content.Append("Kindly email to \n");
                content.Append("Support@whopay.com.my\n\n");
                content.Append("OR\n\n");
                content.Append("WhatsApp to 0198796433\n\n");
                sURL = sURL + "&message=" + HttpUtility.UrlEncode(content.ToString()); // message content
                myWebRequest = (HttpWebRequest)WebRequest.Create(sURL);
                myWebResponse = (HttpWebResponse)myWebRequest.GetResponse();
                if (myWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    Stream oStream = myWebResponse.GetResponseStream();
                    StreamReader oReader = new StreamReader(oStream);
                    string sResult = oReader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.Message, "Error sending message. MobileNo:" + mobileNo);
                //Response.Write("Some issue happen: "+ex);
            }
            finally
            {
                if (myWebResponse != null)
                    myWebResponse.Close();
            }
        }
        public bool sendOTP(string mobileNo, string otp)
        {
            HttpWebRequest myWebRequest = null/* TODO Change to default(_) if this is not a reference type */;
            HttpWebResponse myWebResponse = null/* TODO Change to default(_) if this is not a reference type */;

            string temp = mobileNo;
            bool validFormat=false;
            if (mobileNo.StartsWith("0") && !(mobileNo.StartsWith("6")))
            {
                mobileNo = "6";
                mobileNo += temp;
                validFormat = true;
            }else if (mobileNo.StartsWith("601"))
            {
                validFormat = true;
            }

            if (validFormat)
            {
                try
                {
                    string sURL = "http://gateway.onewaysms.com.my:10001/api.aspx";
                    string apiUsername = ConfigurationManager.AppSettings["SMSUsername"];
                    string apiPassword = ConfigurationManager.AppSettings["SMSPassword"];
                    string apiSenderID = ConfigurationManager.AppSettings["SMSSenderID"];
                    sURL = sURL + "?apiusername=" + HttpUtility.UrlEncode(apiUsername);
                    sURL = sURL + "&apipassword=" + HttpUtility.UrlEncode(apiPassword);
                    sURL = sURL + "&mobileno=" + HttpUtility.UrlEncode(mobileNo);
                    sURL = sURL + "&senderid=" + HttpUtility.UrlEncode(apiSenderID);
                    sURL = sURL + "&languagetype=" + "1";
                    sURL = sURL + "&message=" + HttpUtility.UrlEncode("WhoPay: Thanks for registering on our website. Please use this OTP to verify your account: " + otp + ". If you didn't request a OTP, please ignore this email."); // message content
                    myWebRequest = (HttpWebRequest)WebRequest.Create(sURL);
                    myWebResponse = (HttpWebResponse)myWebRequest.GetResponse();
                    if (myWebResponse.StatusCode == HttpStatusCode.OK)
                    {
                        Stream oStream = myWebResponse.GetResponseStream();
                        StreamReader oReader = new StreamReader(oStream);
                        string sResult = oReader.ReadToEnd();
                    }
                }
                catch (Exception ex)
                {
                    LogUtil.logError(ex.Message, "Error sending message. MobileNo:" + mobileNo);
                    //Response.Write("Some issue happen: "+ex);
                }
                finally
                {
                    if (myWebResponse != null)
                        myWebResponse.Close();
                }
                return !HasError;
            }
            else
            {
                return HasError;
            }
        }
    }
}