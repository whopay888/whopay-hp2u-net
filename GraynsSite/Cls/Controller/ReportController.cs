﻿using CTOS.Model;
using HJT.RAMCI.Model.Report;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Web.UI.WebControls;
using System.Xml.Serialization;
using Synergy.Controller;
using Synergy.Helper;
using Synergy.Model;
using Synergy.Util;
using HJT.Cls.Services;
using System.Threading.Tasks;

namespace HJT.Cls.Controller
{
    public class ReportController
    {
        #region Variables
        #region Private
        private const int row5 = 15;
        private const int row10 = 20;

        private string tempRequestID = string.Empty;
        private string requestID = string.Empty;
        private string autoCode = string.Empty;

        private string userID = string.Empty;
        private string purchasePrice = string.Empty;
        private string mainNetIncome = "0";
        private string mainOtherIncome = "0";
        private string coNetIncome = "0";
        private string coOtherIncome = "0";
        private string loanAmount = string.Empty;
        private string projectID = string.Empty;
        private string interest = string.Empty;

        private CtosAuthenticationService _ctosAuthService;
        #endregion
        #region Public
        public string mainResultID { get; set; } = string.Empty;
        public string coResultID { get; set; } = string.Empty;
        public string MAtype { get; set; } = string.Empty;
        public string CAtype { get; set; } = string.Empty;
        public string value1 { get; set; } = string.Empty;
        public string value2 { get; set; } = string.Empty;
        public string value3 { get; set; } = string.Empty;
        public MemberModel member = new MemberModel();
        public section_ccris section_ccris { get; set; } = new section_ccris();
        public xmlBanking_infoCcris_banking_details detail { get; set; } = new xmlBanking_infoCcris_banking_details();
        #endregion
        #region  CCRIS data
        /*
          [row,0] = original
          [row,1] = outstanding
          [row,2] = repayment
        */
        public string[,] mainHL { get; set; } = new string[row5, 3]; //Housing Loan
        //private string[,] mainHLrecalc { get; set; } = new string[10, 3]; //Housing Loan < 3years, used to record down and compare with calculated repayment
        //private string[,] mainHLcapCode { get; set; } = new string[10, 3];
        public string[,] mainHP { get; set; } = new string[row5, 3]; //Hire Purchase
        public string[,] mainCL { get; set; } = new string[row5, 3]; //Car Loan
        public string[,] mainCC { get; set; } = new string[row10, 3]; //Credit Card
        public string[,] mainPL { get; set; } = new string[row5, 3]; //Personal Loan
        public string[,] mainOD { get; set; } = new string[row5, 3]; //Overdraft
        public string[,] mainXP { get; set; } = new string[row10, 3]; //X-Product

        public string[,] coHL { get; set; } = new string[row5, 3];
        //private string[,] coHLrecalc { get; set; } = new string[10, 3];
        //private string[,] coHLcapCode { get; set; } = new string[10, 3];
        public string[,] coHP { get; set; } = new string[row5, 3];
        public string[,] coCL { get; set; } = new string[row5, 3];
        public string[,] coCC { get; set; } = new string[row10, 3];
        public string[,] coPL { get; set; } = new string[row5, 3];
        public string[,] coOD { get; set; } = new string[row5, 3];
        public string[,] coXP { get; set; } = new string[row10, 3];
        #endregion
        #endregion

        #region Construtor
        public ReportController()
        {
            wwdb db = new wwdb();
            db.OpenTable("select AutoCode from tbl_TApplicantCreditAdvisor with (nolock) where requestid=N'" + requestID + "'");
            if (db.RecordCount() > 0)
                autoCode = db.Item("AutoCode");
            _ctosAuthService = new CtosAuthenticationService();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        }
        public ReportController(string userID, string requestID)
        {
            wwdb db = new wwdb();
            db.OpenTable("select AutoCode from tbl_TApplicantCreditAdvisor with (nolock) where requestid=N'" + requestID + "'");
            if (db.RecordCount() > 0)
                autoCode = db.Item("AutoCode");
            this.userID = userID;
            this.requestID = requestID;
            _ctosAuthService = new CtosAuthenticationService();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        }
        #endregion

        #region Automation
        private void finalizeAdvise()
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            string advise = string.Empty;

            db.OpenTable(string.Format(@"
                with cte as (
                    SELECT distinct tra.Advise, tra.Category, tra.CategoryCode, tra.isMain, p.Sort
                    FROM tbl_TRequestAdvise tra WITH (NOLOCK)
                    inner join tbl_Parameter p with (nolock) on (tra.CategoryCode = p.ParameterName OR tra.Category = p.ParameterValue)
                    WHERE tra.RequestID = N'{0}'
                )

                select * from cte
                where 1 = 1
                ORDER BY cast(cte.isMain as int) desc, cast(cte.Sort as int) asc
            ", this.requestID));
            if (db.RecordCount() > 0)
            {
                while (!db.Eof())
                {
                    advise += db.Item("Advise") + Environment.NewLine;
                    db.MoveNext();
                }
                sql.Clear();
                sql.AppendFormat(@"
                    DELETE FROM tbl_TApplicantCreditAdvisor WHERE requestID = N'{1}';
                    INSERT INTO tbl_TApplicantCreditAdvisor (comment, requestID)
                    VALUES (N'{0}', N'{1}');
                ", advise
                , this.requestID);
                db.Execute(sql.ToString());

                if (db.HasError)
                    LogUtil.logError(db.ErrorMessage, sql.ToString());
                else
                    LogUtil.logAction(sql.ToString(), "Finalize Advise INTO Table tbl_TApplicantCreditAdvisor. RequestID: " + this.requestID);
            }
        }
        private void writeAdvise(string advise, string category, string categoryCode, bool isMain)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            sql.Clear();
            sql.AppendFormat(@"
                INSERT INTO tbl_TRequestAdvise (RequestID, Category, CategoryCode, Advise, CreatedAt, isMain)
                VALUES ('{0}', '{1}', '{2}', '{3}', GETDATE(), '{4}');
            ", this.requestID
            , category
            , categoryCode
            , advise
            , isMain ? "1" : "0");
            db.Execute(sql.ToString());

            if (db.HasError)
                LogUtil.logError(db.ErrorMessage, sql.ToString());
            else
                LogUtil.logAction(sql.ToString(), "INSERT INTO Table tbl_TRequestAdvise. RequestID: " + this.requestID);
        }
        private void checkExistingAuto()
        {
            wwdb db = new wwdb();

            db.OpenTable("SELECT autocode FROM tbl_TApplicantCreditAdvisor with (nolock) WHERE RequestID=N'" + requestID + "'");
            if (db.RecordCount() > 0)
            {
                this.autoCode = db.Item("autocode");
                appendAdvise();
            }
        }
        private void appendAdvise()
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            string comment = string.Empty;

            db.OpenTable("select comment from tbl_tapplicantcreditadvisor with (nolock) where requestid=N'" + requestID + "' order by id");
            if (db.RecordCount() > 0)
                while (!db.Eof())
                {
                    comment += db.Item("comment") + Environment.NewLine;
                    db.MoveNext();
                }
            sql.Append("delete from tbl_tapplicantcreditadvisor where requestid=N'" + this.requestID + "'; ");
            sql.Append("insert into tbl_tapplicantcreditadvisor (requestid, advisorid, comment) values ('" + requestID + "','0','" + secure.RC(comment) + "')");
            db.Execute(sql.ToString());
        }
        public void finishChecking()
        {
            if (!string.IsNullOrEmpty(this.autoCode) && !string.IsNullOrEmpty(this.requestID))
            {
                finalizeAdvise();
                wwdb db = new wwdb();
                StringBuilder sql = new StringBuilder();

                sql.Clear();
                sql.Append("update tbl_TApplicantCreditAdvisor set autocode=N'" + this.autoCode + "' where requestid=N'" + this.requestID + "'");
                db.Execute(sql.ToString());
                LogUtil.logAction(sql.ToString(), "UPDATE tbl_TApplicantCreditAdvisor - Finish Checking. RequestID:" + this.requestID);
            }
        }
        private void countFacilities(object main_account, bool isMain, int age, decimal loanAmount, string name, string icNo,
            bool haveCoApplicant = false, string advise = "")
        {
            if (!autoCode.Contains((isMain ? "MA" : "CA") + "-CF"))
            {
                wwdb db = new wwdb();
                StringBuilder sql = new StringBuilder();
                section_ccris section_ccris = new section_ccris();
                xmlBanking_infoCcris_banking_details detail = new xmlBanking_infoCcris_banking_details();
                string type = string.Empty;

                if (main_account is section_ccris)
                {
                    section_ccris = (section_ccris)main_account;
                    type = "CTOS";
                }
                if (main_account is xmlBanking_infoCcris_banking_details)
                {
                    detail = (xmlBanking_infoCcris_banking_details)main_account;
                    type = "RAMCI";
                }

                try
                {
                    int housingLoan = 0, carLoan = 0, pLoan = 0, creditCard = 0, termLoan = 0;

                    if (type.Equals("RAMCI") && detail != null && detail.outstanding_credit != null)
                    {
                        var accounts = detail.outstanding_credit.ToList();
                        for (int count1 = 0; count1 < accounts.Count(); count1++)
                        {
                            var account = accounts[count1];
                            for (int count2 = 0; count2 < account.sub_account.Count(); count2++)
                            {
                                var sub_account = account.sub_account[count2];
                                sql.Clear();
                                sql.Append("SELECT LoanID FROM tbl_LoanTypesXML WITH(NOLOCK) WHERE LoanTypeCode = '" +
                                    sub_account.item.facility + "';");
                                db.OpenTable(sql.ToString());

                                if (db.RecordCount() > 0)
                                {
                                    string loanid = db.Item("LoanID");
                                    switch (loanid)
                                    {
                                        case "1"://Housing Loan
                                            housingLoan++;
                                            break;
                                        case "2"://Commercial Loan
                                            termLoan++;
                                            break;
                                        case "3"://Personal Loan
                                            pLoan++;
                                            break;
                                        case "4"://OverDraft
                                            termLoan++;
                                            break;
                                        case "5"://X-Product
                                            termLoan++;
                                            break;
                                        case "6"://Hire Purchase - Car Loan
                                            carLoan++;
                                            break;
                                        case "7"://Credit Card
                                            creditCard++;
                                            break;
                                        default:
                                            //sqlString.displayAlert2(this, "Don't have any loan");
                                            break;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    if (type.Equals("CTOS") && section_ccris != null && section_ccris.accounts != null && section_ccris.accounts.account != null)
                    {
                        List<ccris_account_detail> accounts = section_ccris.accounts.account.ToList();
                        for (int count1 = 0; count1 < accounts.Count(); count1++)
                        {
                            var account = accounts[count1];
                            for (int count2 = 0; count2 < account.sub_accounts.Count(); count2++)
                            {
                                var sub_account = account.sub_accounts[count2];
                                sql.Clear();
                                sql.Append("SELECT LoanID FROM tbl_LoanTypesXML WITH(NOLOCK) WHERE LoanTypeCode = '" +
                                    sub_account.facility.code + "';");
                                db.OpenTable(sql.ToString());

                                if (db.RecordCount() > 0)
                                {
                                    string loanid = db.Item("LoanID");
                                    switch (loanid)
                                    {
                                        case "1"://Housing Loan
                                            housingLoan++;
                                            break;
                                        case "2"://Commercial Loan
                                            termLoan++;
                                            break;
                                        case "3"://Personal Loan
                                            pLoan++;
                                            break;
                                        case "4"://OverDraft
                                            termLoan++;
                                            break;
                                        case "5"://X-Product
                                            termLoan++;
                                            break;
                                        case "6"://Hire Purchase - Car Loan
                                            carLoan++;
                                            break;
                                        case "7"://Credit Card
                                            creditCard++;
                                            break;
                                        default:
                                            //sqlString.displayAlert2(this, "Don't have any loan");
                                            break;
                                    }
                                    break;
                                }
                            }
                        }
                    }

                    if (haveCoApplicant)
                    {
                        if (!isMain)
                        {
                            db.OpenTable("SELECT Description FROM tbl_CreditAdvisor with (nolock) WHERE AdvisorID = '18' and status='T'");
                            if (db.RecordCount() > 0)
                                advise += Environment.NewLine + db.Item("Description");
                        }
                        advise += Environment.NewLine + name.ToUpper().Substring(0, 2) + "-" + icNo.Substring(10, 2) + Environment.NewLine;
                    }
                    if (housingLoan > 0 || carLoan > 0 || pLoan > 0 || creditCard > 0 || termLoan > 0)
                    {
                        db.OpenTable("SELECT Description FROM tbl_CreditAdvisor with (nolock) WHERE AdvisorID='14' and status='T'");
                        advise += Environment.NewLine + db.Item("Description") + " ";
                        if (housingLoan > 0)
                            advise += housingLoan + " " + Resources.resource.Housing_Loan + ", ";
                        if (carLoan > 0)
                            advise += carLoan + " " + Resources.resource.Car_Loan + ", ";
                        if (pLoan > 0)
                            advise += pLoan + " " + Resources.resource.Personal_Loan + ", ";
                        if (creditCard > 0)
                            advise += creditCard + " " + Resources.resource.Credit_Card + ", ";
                        if (termLoan > 0)
                            advise += termLoan + " " + Resources.resource.Term_Loan + ", ";

                        advise = advise.TrimEnd(' ', ',');
                        advise += "." + Environment.NewLine;
                    }
                    else //no facilities
                    {
                        string advise1 = string.Empty, advise2 = string.Empty, advise3 = string.Empty,
                            advise4 = string.Empty, advise5 = string.Empty, advise6 = string.Empty , adviseKey = string.Empty;

                        int youngAgeSetting = 25; //default value
                        db.OpenTable(@"
                            SELECT ParameterValue FROM tbl_Parameter with (nolock) 
                            WHERE ParameterName = 'CountFacilities_YoungAge' 
                            AND Category = 'AdviceDetail'
                        ");

                        if ((section_ccris.summary is null) && (age > youngAgeSetting))
                        {
                            db.OpenTable("SELECT Description FROM tbl_CreditAdvisor with (nolock) WHERE AdvisorID='30' and status='T'");
                            if (db.RecordCount() > 0)
                            {
                                adviseKey = db.Item("Description");
                                advise += Environment.NewLine + adviseKey;
                            }
                        }

                        db.OpenTable("SELECT Description FROM tbl_CreditAdvisor with (nolock) WHERE AdvisorID='9' and status='T'");
                        if (db.RecordCount() > 0)
                            advise += Environment.NewLine + db.Item("Description");

                        /*  No bank facility. No CCRIS reference
                            To provide supporting document
                            - FD / Saving
                            {0}
                            {1}
                        */

                        db.OpenTable("SELECT Description FROM tbl_CreditAdvisor with (nolock) WHERE AdvisorID='10' and status='T'");
                        if (db.RecordCount() > 0)
                            advise1 = db.Item("Description");

                        db.OpenTable("SELECT Description FROM tbl_CreditAdvisor with (nolock) WHERE AdvisorID='11' and status='T'");
                        if (db.RecordCount() > 0)
                            advise2 = db.Item("Description");

                        db.OpenTable("SELECT Description FROM tbl_CreditAdvisor with (nolock) WHERE AdvisorID='13' and status='T'");
                        if (db.RecordCount() > 0)
                            advise3 = db.Item("Description");

                        //int youngAgeSetting = 25; //default value
                        //db.OpenTable(@"
                        //    SELECT ParameterValue FROM tbl_Parameter with (nolock) 
                        //    WHERE ParameterName = 'CountFacilities_YoungAge' 
                        //    AND Category = 'AdviceDetail'
                        //");
                        if (db.RecordCount() > 0)
                            youngAgeSetting = ConvertHelper.ConvertToInt(db.Item("ParameterValue"), 25);

                        if (age >= youngAgeSetting)
                            advise = string.Format(advise, advise1, advise3);
                        else//<=30
                            advise = string.Format(advise, advise2, advise3);

                        /*
                            No bank facility. No CCRIS reference
                            To provide supporting document
                            - FD / Saving
                            - House Title / Car Cert
                            **Advise to bring in joint applicant due to {0}
                        */

                        db.OpenTable("SELECT Description FROM tbl_CreditAdvisor with (nolock) WHERE AdvisorID='12' and status='T'");
                        if (db.RecordCount() > 0)
                            advise4 = db.Item("Description");

                        db.OpenTable("SELECT Description FROM tbl_CreditAdvisor with (nolock) WHERE AdvisorID='19' and status='T'");
                        if (db.RecordCount() > 0)
                            advise5 = db.Item("Description");

                        decimal loanAmountThreshold = 500000; //default value
                        db.OpenTable(@"
                            SELECT ParameterValue FROM tbl_Parameter with (nolock) 
                            WHERE ParameterName = 'CountFacilities_LoanAmountThreshold' 
                            AND Category = 'AdviceDetail'
                        ");
                        if (db.RecordCount() > 0)
                            loanAmountThreshold = ConvertHelper.ConvertToDecimal(db.Item("ParameterValue"), 500000);

                        bool noAppendAdvise = false;
                        if (age >= youngAgeSetting)
                        {
                            if (loanAmount > loanAmountThreshold)
                                advise = string.Format(advise, advise4);
                            else
                                noAppendAdvise = true;
                        }
                        else//<=30 
                        {
                            if (loanAmount > loanAmountThreshold)
                                advise5 += "{0}";//young age{0}
                            advise = string.Format(advise, advise5);//**Advise to bring in joint applicant due to young age{0}
                            if (loanAmount > loanAmountThreshold)
                                advise = string.Format(advise, (", " + advise4));//due to young age, big loan amount
                        }
                        db.OpenTable("SELECT Description FROM tbl_CreditAdvisor with (nolock) WHERE AdvisorID='20' and status='T'");
                        if (db.RecordCount() > 0)
                            advise6 = db.Item("Description");

                        if (noAppendAdvise)
                        {
                            advise = string.Format(advise, "");
                            advise += advise6;
                        }
                        else
                        {
                            advise += " & " + advise6;
                        }
                        advise += Environment.NewLine;
                    }

                    writeAdvise(advise, "Count Facilities", "CF", isMain);
                    LogUtil.logAction(sql.ToString(), "INSERT Advise Facilities RequestID:" + secure.RC(this.requestID));
                    autoCode += (isMain ? "MA" : "CA") + "-CF_";
                }
                catch (Exception ex) { LogUtil.logError(ex.Message, "Check Facilities"); }
            }
        }
        private void checkCreditCardUsage(object sectionDetail, bool isMain)
        {
            if (!autoCode.Contains((isMain ? "MA" : "CA") + "-CC"))
            {
                wwdb db = new wwdb();
                StringBuilder sql = new StringBuilder();
                section_ccris section_ccris = new section_ccris();
                xmlBanking_infoCcris_banking_details detail = new xmlBanking_infoCcris_banking_details();
                string type = string.Empty;

                if (sectionDetail is section_ccris)
                {
                    section_ccris = (section_ccris)sectionDetail;
                    type = "CTOS";
                    // Aaron - add in PTPTN advise                       
                    string advise = string.Empty;
                    string advise2 = string.Empty;
                    if (section_ccris != null && section_ccris.accounts != null && section_ccris.accounts.account != null)
                    {
                        for (int count1 = 0; count1 < section_ccris.accounts.account.ToList().Count(); count1++)
                        {
                            var sub_account = section_ccris.accounts.account[count1];
                            if (sub_account.org1 == "PT")
                            {
                                for (int count2 = 0; count2 < sub_account.sub_accounts.ToList().Count(); count2++)
                                {
                                    var sub_ptptn = sub_account.sub_accounts[count2];

                                    if (sub_ptptn.cr_positions.FirstOrDefault().inst_arrears.Equals("0"))
                                    { }
                                    else if (sub_ptptn.cr_positions.FirstOrDefault().inst_arrears.Equals("-"))
                                    { }
                                    else
                                    {
                                        var ptptn_mths = sub_ptptn.cr_positions.FirstOrDefault().inst_arrears;
                                        // advise2 += "** Default " + ptptn_mths + " months" + Environment.NewLine;
                                        advise2 += "** Default " + (2 > ConvertHelper.ConvertToInt(ptptn_mths, 0) ? " 1 month" + Environment.NewLine : ptptn_mths + " months" + Environment.NewLine);
                                    }
                                }

                            }
                        }
                        if (advise2 != "")
                        {
                            db.OpenTable("SELECT Description FROM tbl_CreditAdvisor with (nolock) WHERE AdvisorID='22' and status='T'");
                            if (db.RecordCount() > 0)
                                advise = string.Format(db.Item("Description"), advise2);
                            advise = advise.Replace("\r", "").Replace("\n ", "\n").Replace("\n\n", "\n");
                            writeAdvise(advise, "PTPTN", "CC", isMain);
                        }
                    }
                }
                if (sectionDetail is xmlBanking_infoCcris_banking_details)
                {
                    detail = (xmlBanking_infoCcris_banking_details)sectionDetail;
                    type = "RAMCI";
                }

                decimal[] percent = new decimal[row10];
                string[] noOfCards = new string[row10];
                decimal outstanding = 0, total_limit = 0;
                bool exceed = false;
                int numberOfCards = 1; //Number of cards
                int totalNumber = 1; // Number of facilities
                if (section_ccris == null && detail == null)
                {
                    //for (int i = 0; i < (isMain ? mainCC.Length - 1 : coCC.Length - 1); i++)
                    //{
                    //    outstanding = ConvertHelper.ConvertToDecimal(isMain ? mainCC[i, 1] : coCC[i, 1], 0);
                    //    if (outstanding > 0)
                    //    {
                    //        if ((isMain ? mainCC[i + 1, 1] : coCC[i + 1, 1]) != null)
                    //        {
                    //            while ((isMain ? mainCC[i, 0] : coCC[i, 0]) == (isMain ? mainCC[i, 0] : coCC[i, 0]))
                    //            {
                    //                i++;
                    //                outstanding += ConvertHelper.ConvertToDecimal(isMain ? mainCC[i, 1] : coCC[i, 1], 0);
                    //            }
                    //        }
                    //        total_limit = ConvertHelper.ConvertToDecimal(isMain ? mainCC[i, 0] : coCC[i, 0], 0);
                    //        if (outstanding >= ((decimal)((decimal)70.00 / (decimal)100.00) * total_limit))
                    //        {
                    //            exceed = true;
                    //            break;
                    //        }
                    //    }
                    //}
                }
                else
                {
                    decimal creditCardLimitedUsage = (decimal)0.70; //default value 70%
                    db.OpenTable(@"
                            SELECT ParameterValue FROM tbl_Parameter with (nolock) 
                            WHERE ParameterName = 'CreditCard_LimitedUsage' 
                            AND Category = 'AdviceDetail'
                        ");
                    if (db.RecordCount() > 0)
                        creditCardLimitedUsage = ConvertHelper.ConvertToDecimal(db.Item("ParameterValue"), 70) / 100;

                    if (type.Equals("CTOS") && section_ccris != null && section_ccris.accounts != null && section_ccris.accounts.account != null)
                        for (int count1 = 0; count1 < section_ccris.accounts.account.ToList().Count(); count1++)
                        {
                            var _account = section_ccris.accounts.account.ToList()[count1];
                            outstanding = 0;
                            for (int count2 = 0; count2 < _account.sub_accounts.Count(); count2++)
                            {
                                var sub_account = _account.sub_accounts[count2];
                                sql.Clear();
                                sql.Append("SELECT LoanID FROM tbl_LoanTypesXML WITH(NOLOCK) " +
                                    "WHERE LoanTypeCode = '" + sub_account.facility.code + "';");
                                db.OpenTable(sql.ToString());
                                if (db.RecordCount() > 0)
                                {
                                    if (db.Item("LoanID").Equals("7"))
                                    {
                                        total_limit = ConvertHelper.ConvertToDecimal(_account.limit, 0);
                                        outstanding += ConvertHelper.ConvertToDecimal(sub_account.cr_positions.FirstOrDefault().balance, 0);
                                        if (_account.sub_accounts.Last() == sub_account)
                                            if (outstanding >= (creditCardLimitedUsage * total_limit))
                                            {
                                                percent[numberOfCards - 1] = (outstanding / total_limit) * 100;
                                                noOfCards[numberOfCards - 1] = totalNumber.ToString();
                                                numberOfCards++;
                                                exceed = true;
                                                break;
                                            }
                                    }
                                    else
                                        break;
                                }
                            }
                            totalNumber++;
                        }
                    if (type.Equals("RAMCI") && detail != null && detail.outstanding_credit != null && detail.outstanding_credit.FirstOrDefault() != null)
                        for (int count1 = 0; count1 < detail.outstanding_credit.ToList().Count(); count1++)
                        {
                            var _account = detail.outstanding_credit.ToList()[count1];
                            outstanding = 0;
                            for (int count2 = 0; count2 < _account.sub_account.Count(); count2++)
                            {
                                var sub_account = _account.sub_account[count2];
                                sql.Clear();
                                sql.Append("SELECT LoanID FROM tbl_LoanTypesXML WITH(NOLOCK) WHERE " +
                                    "LoanTypeCode = '" + sub_account.item.facility + "';");
                                db.OpenTable(sql.ToString());
                                if (db.RecordCount() > 0)
                                {
                                    if (db.Item("LoanID").Equals("7"))
                                    {
                                        total_limit = ConvertHelper.ConvertToDecimal(_account.master.item.limit, 0);
                                        outstanding += ConvertHelper.ConvertToDecimal(sub_account.item.total_outstanding_balance, 0);
                                        if (_account.sub_account.Last() == sub_account)
                                            if (outstanding >= (creditCardLimitedUsage * total_limit))
                                            {
                                                percent[numberOfCards - 1] = (outstanding / total_limit) * 100;
                                                noOfCards[numberOfCards - 1] = totalNumber.ToString();
                                                numberOfCards++;
                                                exceed = true;
                                                break;
                                            }
                                    }
                                    else
                                        break;
                                }
                            }
                            totalNumber++;
                        }
                }
                if (exceed)
                {
                    string cardString = string.Empty;
                    for (int temp = 1; temp < numberOfCards; temp++)
                    {
                        cardString += "No. " + noOfCards[temp - 1].ToString() + " (" + Math.Round(percent[temp - 1]).ToString() + "%), ";
                    }
                    cardString = cardString.TrimEnd(' ').TrimEnd(',') + ".";
                    db.OpenTable("SELECT ApplicationResult FROM tbl_Request with (nolock) WHERE RequestID=N'" +
                            secure.RC(this.requestID) + "'");
                    if (!db.Item("ApplicationResult").Equals(ApplicationResult.Denied))
                    {
                        sql.Clear();
                        sql.Append("UPDATE tbl_Request SET ApplicationResult = '" + ApplicationResult.Modify + "'" +
                            " WHERE RequestID = N'" + secure.RC(this.requestID) + "';");
                        db.Execute(sql.ToString());
                        LogUtil.logAction(sql.ToString(), "UPDATE tbl_Request RequestID:" + this.requestID);
                    }
                    string advise = string.Empty;
                    /*
                        Credit Card high usage > 70%.
                        {0}
                        This may affect system scoring.
                        **To reduce below 70% by 30/{1} & apply after 15/{2}.
                        **Or to support with FD / Saving.
                    */
                    db.OpenTable("SELECT Description FROM tbl_CreditAdvisor with (nolock) WHERE AdvisorID='15' and status='T'");
                    if (db.RecordCount() > 0)
                        advise = string.Format(db.Item("Description")
                                    , cardString
                                    , DateTime.Now.Month
                                    , ((DateTime.Now.Month + 1 > 12) ? 1 : DateTime.Now.Month + 1).ToString());
                    advise = advise.Replace("\r", "").Replace("\n ", "\n").Replace("\n\n", "\n");
                    writeAdvise(advise, "Credit Card", "CC", isMain);
                    LogUtil.logAction(sql.ToString(), type + "-Calculating Credit Card Usage. RequestID:" + requestID);

                    autoCode += (isMain ? "MA" : "CA") + "-CC_";
                }
            }
        } //CC
        private int countCCRISnumber(object sectionDetail, int number, string loanType = "")
        {
            int count = -1;
            string type = string.Empty;

            #region Identifying object
            List<ccris_account_detail> ccris_accounts = new List<ccris_account_detail>();
            List<xmlBanking_infoCcris_banking_detailsItem> items = new List<xmlBanking_infoCcris_banking_detailsItem>();
            if (sectionDetail is section_ccris)
                if (((section_ccris)sectionDetail).accounts != null && ((section_ccris)sectionDetail).accounts.account != null)
                {
                    ccris_accounts = ((section_ccris)sectionDetail).accounts.account.ToList();
                    type = "CTOS";
                }
                else
                    return -1; //empty ccris, not counting
            if (sectionDetail is xmlBanking_infoCcris_banking_details)
                if (((xmlBanking_infoCcris_banking_details)sectionDetail) != null && ((xmlBanking_infoCcris_banking_details)sectionDetail).outstanding_credit != null)
                {
                    items = ((xmlBanking_infoCcris_banking_details)sectionDetail).outstanding_credit.ToList();
                    type = "RAMCI";
                }
                else
                    return -1; //empty ccris, not counting
            #endregion

            if (type.Equals("CTOS"))
                try
                {
                    bool cont = false;
                    for (int count1 = 0; count1 < ccris_accounts.Count(); count1++)
                    {
                        var account = ccris_accounts[count1];
                        for (int count2 = 0; count2 < account.sub_accounts.Count(); count2++)
                        {
                            var sub_account = account.sub_accounts[count2];
                            if (!string.IsNullOrEmpty(loanType)) //loanType specified, check if loanType valid and exist in the CCRIS
                            {
                                wwdb db = new wwdb();
                                db.OpenTable(string.Format(@"
                                    select lt.ShortForm as 'LoanType' from tbl_LoanTypes lt with (nolock)
                                    inner join tbl_LoanTypesXML ltx with (nolock) on lt.LoanID = ltx.LoanID
                                    where ltx.LoanTypeCode = '{0}' and lt.ShortForm = '{1}'
                                ", sub_account.facility.code
                                , loanType));
                                if (!(db.RecordCount() > 0))
                                    continue; //loop until get result
                                else if (!cont) //got result for this loanType
                                    count = 0; //ready count for counting
                            }
                            else if (!cont)
                                count = 0; //no loanType specified, ready for counting

                            for (int count3 = 0; count3 < sub_account.cr_positions.Count(); count3++)
                            {
                                var cr_position = sub_account.cr_positions[count3];
                                if (!string.IsNullOrEmpty(cr_position.inst_arrears) && !string.IsNullOrWhiteSpace(cr_position.inst_arrears))
                                    if (ConvertHelper.ConvertToInt(cr_position.inst_arrears, 0) == number)
                                        count++;
                            }
                            cont = true; //do not reset count to 0
                        }
                    }
                }
                catch (Exception ex) { LogUtil.logError(ex.Message, "CTOS-Count CCRIS"); }

            if (type.Equals("RAMCI"))
                try
                {
                    bool cont = false;
                    for (int count1 = 0; count1 < items.Count(); count1++)
                    {
                        var item = items[count1];
                        for (int count2 = 0; count2 < item.sub_account.Count(); count2++)
                        {
                            var sub_account_item = item.sub_account[count2];
                            if (!string.IsNullOrEmpty(loanType)) //loanType specified, check if loanType valid and exist in the CCRIS
                            {
                                wwdb db = new wwdb();
                                db.OpenTable(string.Format(@"
                                    select lt.ShortForm as 'LoanType' from tbl_LoanTypes lt with (nolock)
                                    inner join tbl_LoanTypesXML ltx with (nolock) on lt.LoanID = ltx.LoanID
                                    where ltx.LoanTypeCode = '{0}' and lt.ShortForm = '{1}'
                                ", sub_account_item.item.facility
                                , loanType));
                                if (!(db.RecordCount() > 0))
                                    continue; //loop until get result
                                else if (!cont) //got result for this loanType
                                    count = 0; //ready count for counting
                            }
                            else if (!cont)
                                count = 0; //no loanType specified, ready for counting

                            for (int count3 = 0; count3 < sub_account_item.item.credit_position.Count(); count3++)
                            {
                                var credit_position_item = sub_account_item.item.credit_position[count3];
                                if (!string.IsNullOrEmpty(credit_position_item) && !string.IsNullOrWhiteSpace(credit_position_item))
                                    if (ConvertHelper.ConvertToInt(credit_position_item, 0) == number)
                                        count++;
                            }
                            cont = true; //do not reset count to 0
                        }
                    }
                    //autoCode = autoCode + "CCRIS_";
                }
                catch (Exception ex) { LogUtil.logError(ex.Message, "RAMCI-Count CCRIS"); }

            /*
            if (count == -1 && !string.IsNullOrEmpty(loanType))
                return -1; //loanType not found, not counting
            else if (count == -1 && string.IsNullOrEmpty(loanType))
                return -1; //error. not counting
            else if (count == 0)
                return 0; //working well, 0 result found
            */

            return count;
        }
        public void checkCCRIScount(object accounts, bool isMain, bool isMainOnly)
        {
            string type = string.Empty;
            try
            {
                List<xmlBanking_infoCcris_banking_detailsItem> RAMCIaccounts = new List<xmlBanking_infoCcris_banking_detailsItem>();
                List<ccris_account_detail> CTOSaccounts = null;

                if (accounts is List<xmlBanking_infoCcris_banking_detailsItem>)
                {
                    RAMCIaccounts = (List<xmlBanking_infoCcris_banking_detailsItem>)accounts;
                    type = "RAMCI";
                }
                if (accounts is List<ccris_account_detail>)
                {
                    CTOSaccounts = (List<ccris_account_detail>)accounts;
                    type = "CTOS";
                }

                if (type.Equals("CTOS") && CTOSaccounts != null)
                {
                    bool skipCCRIScheck = false, allZero = true;
                    for (int count1 = 0; count1 < CTOSaccounts.Count(); count1++)
                    {
                        var account = CTOSaccounts[count1];
                        for (int count2 = 0; count2 < account.sub_accounts.Count(); count2++)
                        {
                            skipCCRIScheck = false;
                            var sub_account = account.sub_accounts[count2];
                            for (int count3 = 0; count3 < sub_account.cr_positions.Count(); count3++)
                            {
                                var cr_position = sub_account.cr_positions[count3];
                                if (!string.IsNullOrEmpty(cr_position.inst_arrears) && !string.IsNullOrWhiteSpace(cr_position.inst_arrears))
                                {
                                    if (!(ConvertHelper.ConvertToInt(cr_position.inst_arrears, 0) > 0))
                                    {
                                        if (account == CTOSaccounts.Last() && sub_account == account.sub_accounts.Last() &&
                                            cr_position == sub_account.cr_positions.Last() && allZero)
                                            CCRISClear(isMain, isMainOnly);
                                    }
                                    else
                                        allZero = false;

                                    if (!skipCCRIScheck)
                                        if (checkFirstCCRISCount(cr_position.inst_arrears, account.org1)) //only check the first non-null and non-PTPTN number
                                            skipCCRIScheck = true;

                                    if (skipCCRIScheck && !allZero)
                                        break;
                                }
                            }
                            if (skipCCRIScheck && !allZero)
                                break;
                        }
                        if (skipCCRIScheck && !allZero)
                            break;
                    }
                }
                if (type.Equals("RAMCI") && RAMCIaccounts != null)
                {
                    bool skipCCRIScheck = false, allZero = true;
                    for (int count1 = 0; count1 < RAMCIaccounts.Count(); count1++)
                    {
                        var account = RAMCIaccounts[count1];
                        for (int count2 = 0; count2 < account.sub_account.Count(); count2++)
                        {
                            skipCCRIScheck = false;
                            var sub_account_item = account.sub_account[count2];
                            for (int count3 = 0; count3 < sub_account_item.item.credit_position.Count(); count3++)
                            {
                                var credit_position_item = sub_account_item.item.credit_position[count3];
                                if (!string.IsNullOrEmpty(credit_position_item) && !string.IsNullOrWhiteSpace(credit_position_item))
                                {
                                    if (!(ConvertHelper.ConvertToInt(credit_position_item, 0) > 0))
                                    {
                                        if (account == RAMCIaccounts.Last() && sub_account_item == account.sub_account.Last() &&
                                            credit_position_item == sub_account_item.item.credit_position.Last() && allZero)
                                            CCRISClear(isMain, isMainOnly);
                                    }
                                    else
                                        allZero = false;

                                    if (!skipCCRIScheck)
                                        if (checkFirstCCRISCount(credit_position_item, sub_account_item.item.facility)) //only check the first non-null and non-PTPTN number
                                            skipCCRIScheck = true;

                                    if (skipCCRIScheck && !allZero)
                                        break;
                                }
                            }
                            if (skipCCRIScheck && !allZero)
                                break;
                        }
                        if (skipCCRIScheck && !allZero)
                            break;
                    }
                }
            }
            catch (Exception ex) { LogUtil.logError(ex.Message, type + "-Check CCRIS count"); }
        }
        private void CCRISClear(bool isMain, bool isMainOnly)
        {
            if (!autoCode.Contains((isMain ? "MA" : "CA") + "-CLEAR_"))
            {
                wwdb db = new wwdb();
                StringBuilder sql = new StringBuilder();

                if (!autoCode.Contains("SAA") && !autoCode.Contains("AKPK")
                    && !autoCode.Contains("TR") && !autoCode.Contains("LS"))
                {
                    db.OpenTable(string.Format(@"
                        SELECT ApplicationResult FROM tbl_Request with (nolock) WHERE RequestID = N'{0}'
                    ", this.requestID));
                    if (!db.Item("ApplicationResult").Equals(ApplicationResult.Denied) && (isMainOnly || !isMain))
                    {
                        sql.Clear();
                        sql.AppendFormat(@"
                            UPDATE tbl_Request SET ApplicationResult = '{0}'
                            WHERE RequestID = N'{1}';
                        ", ApplicationResult.Approved
                        , this.requestID);
                        db.Execute(sql.ToString());
                        LogUtil.logAction(sql.ToString(), "CCRIS Clear. RequestID: " + this.requestID);
                    }
                    db.OpenTable("SELECT Description FROM tbl_CreditAdvisor with (nolock) WHERE AdvisorID='21' and status='T'");
                    if (db.RecordCount() > 0)
                        writeAdvise(db.Item("Description"), "CCRIS Clear", "CLEAR", isMain);
                    LogUtil.logAction(sql.ToString(), "Checking CCRIS Clear. RequestID:" + this.requestID);

                    autoCode += (isMain ? "MA" : "CA") + "-CLEAR_";
                }
            }
        } //CLEAR
        private bool checkFirstCCRISCount(string itemCount, string orgCodeOrFacCode)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            if (ConvertHelper.ConvertToInt(itemCount, 0) > 2)//more than 2
                if (!string.IsNullOrEmpty(orgCodeOrFacCode) //org is PTPTN type
                    && orgCodeOrFacCode.Equals("PT") || orgCodeOrFacCode.Equals("NHEDFNCE"))
                {
                    //deny

                    //insert advise

                    return false;
                }
                else
                {
                    db.OpenTable("SELECT ApplicationResult FROM tbl_Request with (nolock) WHERE RequestID=N'" +
                        secure.RC(this.requestID) + "'");
                    if (!db.Item("ApplicationResult").Equals(ApplicationResult.Denied))
                    {
                        sql.Clear();
                        sql.Append("UPDATE tbl_Request SET ApplicationResult = '" + ApplicationResult.Denied + "'" +
                            " WHERE RequestID = N'" + secure.RC(this.requestID) + "';");
                        db.Execute(sql.ToString());
                        LogUtil.logAction(sql.ToString(), "UPDATE tbl_Request RequestID:" + this.requestID);
                    }
                    return true;
                }
            else
                return false;
        }
        #region CTOS Automation
        private void checkCtosAKPK(string code, bool isMain)
        {
            wwdb db = new wwdb();

            if (!autoCode.Contains((isMain ? "MA" : "CA") + "-AKPK"))
                if (code.Equals("K"))
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Clear();
                    sql.Append(" UPDATE tbl_Request SET StatusID ='" + ApplicationStatus.InProgress +
                        "' , ApplicationResult ='" + ApplicationResult.Denied + "' WHERE RequestID = '" +
                        this.requestID.ToString() + "';");
                    db.Execute(sql.ToString());
                    sql.Clear();

                    db.OpenTable("SELECT Description FROM tbl_CreditAdvisor with (nolock) WHERE AdvisorID='6' and Status='T'");
                    string advise = db.Item("Description");
                    writeAdvise(advise, "AKPK", "AKPK", isMain);
                    LogUtil.logAction(sql.ToString(), "CTOS-Update AKPK. RequestID:" + this.requestID.ToString());
                    autoCode += (isMain ? "MA" : "CA") + "-AKPK";
                }
        } //AKPK
        private void checkCtosSAA(enquiry enquiry, bool isMain)
        {
            wwdb db = new wwdb();

            if (!autoCode.Contains((isMain ? "MA" : "CA") + "-SAA"))
                try
                {
                    string special_attention_acc_summary = "";
                    string special_attention_acc_ccris = "";
                    if (enquiry.section_summary.ccris.special_attention != null)
                        special_attention_acc_summary = enquiry.section_summary.ccris.special_attention.accounts.ToString();
                    if (enquiry.section_summary.ccris.special_attention != null)
                        special_attention_acc_ccris = enquiry.section_ccris.summary.special_attention.status.ToString();
                    if ((!string.IsNullOrEmpty(special_attention_acc_summary) && !string.IsNullOrWhiteSpace(special_attention_acc_ccris))
                        || (!string.IsNullOrEmpty(special_attention_acc_ccris) && !string.IsNullOrWhiteSpace(special_attention_acc_ccris)))
                        if (special_attention_acc_summary.Equals("1") || special_attention_acc_ccris.Equals("1"))
                        {
                            db.OpenTable("SELECT ApplicationResult FROM tbl_Request with (nolock) WHERE RequestID = '" + requestID.ToString() + "';");
                            if (db.RecordCount() > 0)
                            {
                                StringBuilder sql = new StringBuilder();
                                sql.Clear();
                                sql.Append(" UPDATE tbl_Request SET StatusID ='" + ApplicationStatus.InProgress +
                                    "' , ApplicationResult ='" + ApplicationResult.Denied + "' WHERE RequestID = '" +
                                    this.requestID.ToString() + "';");
                                db.Execute(sql.ToString());
                                sql.Clear();

                                /*
                                    **Special Attention Account (SAA)**
                                    - Term loan - 29/05/2017 (Last update - 31/03/2019)
                                */
                                db.OpenTable("SELECT Description FROM tbl_CreditAdvisor with (nolock) WHERE AdvisorID = '7' and status='T'");
                                string advise = db.Item("Description");
                                var special_attention_accs = enquiry.section_ccris.special_attention_accs;
                                for (int count1 = 0; count1 < special_attention_accs.Count(); count1++)
                                {
                                    var special_attention_acc = special_attention_accs[count1];
                                    advise += Environment.NewLine + "- ";
                                    string approval_date = "";
                                    string position_date = "";
                                    string facility = "";

                                    if (special_attention_acc.approval_date != null)
                                        approval_date = special_attention_acc.approval_date;
                                    for (int count2 = 0; count2 < special_attention_acc.sub_accounts.Count(); count2++)
                                    {
                                        var sub_account = special_attention_acc.sub_accounts[count2];
                                        facility = sub_account.facility.code;
                                        db.OpenTable("SELECT LoanID FROM tbl_LoanTypesXML with (nolock) WHERE LoanTypeCode='" + facility + "'");
                                        switch (db.Item("LoanID"))
                                        {
                                            case "1":
                                                advise += Resources.resource.Housing_Loan;
                                                break;
                                            case "2":
                                                advise += Resources.resource.Commercial_Loan;
                                                break;
                                            case "3":
                                                advise += Resources.resource.Personal_Loan;
                                                break;
                                            case "4":
                                                advise += Resources.resource.OverDraft;
                                                break;
                                            case "5":
                                                advise += Resources.resource.Term_Loan;
                                                break;
                                            case "6":
                                                advise += Resources.resource.Car_Loan;
                                                break;
                                            case "7":
                                                advise += Resources.resource.Credit_Card;
                                                break;
                                            default:
                                                break;
                                        }
                                        advise += " - " + approval_date;
                                        for (int count3 = 0; count3 < sub_account.cr_positions.Count(); count3++)
                                        {
                                            var cr_position = sub_account.cr_positions[count3];
                                            if (cr_position.position_date != null)
                                            {
                                                position_date = cr_position.position_date;
                                                advise += " (Last Update - " + position_date + ")";
                                                break;//get only the first
                                            }
                                        }
                                    }
                                }
                                advise = advise.Replace("\r", "").Replace("\n ", "\n").Replace("\n\n", "\n");
                                writeAdvise(advise, "Special Attention Account", "SAA", isMain);
                                LogUtil.logAction(sql.ToString(), "CTOS-Update Special Attention Account. RequestID:" + requestID.ToString());
                                autoCode += (isMain ? "MA" : "CA") + "-SAA_";
                            }
                        }
                }
                catch (Exception ex)
                {
                    LogUtil.logError(ex.Message, "CTOS-Check SAA");
                }
        } //SAA
        private void checkCtosLegal(section_ccris section_ccris, bool isMain)
        {
            wwdb db = new wwdb();

            if (!autoCode.Contains((isMain ? "MA" : "CA") + "-LS"))
                try
                {
                    //int legal_total = ConvertHelper.ConvertToInt(report.enq_report.enquiry.FirstOrDefault().section_summary.ctos.legal.total, 0);
                    //int legal_value = ConvertHelper.ConvertToInt(report.enq_report.enquiry.FirstOrDefault().section_summary.ctos.legal.value, 0);
                    string legal_status = section_ccris.summary.legal.status;

                    if (!string.IsNullOrEmpty(legal_status) && legal_status.Equals("1")) //legal_total > 0 || legal_value > 0)
                    {
                        StringBuilder sql = new StringBuilder();
                        string advise = string.Empty;
                        for (int count = 0; count < section_ccris.accounts.account.ToList().Count(); count++)
                        {
                            //var cr_position = sub_account.cr_positions[count3];
                            if (section_ccris.accounts.account[count].legal.status != null && section_ccris.accounts.account[count].org1 != "PT")
                            {
                                advise += "No." + (count + 1) + ") " +
                                            (section_ccris.accounts.account[count].sub_accounts.First().facility.Value
                                            + " - "
                                            + section_ccris.accounts.account[count].legal.name
                                            + " ( "
                                            + section_ccris.accounts.account[count].legal.date
                                            + " ) ");
                            }
                        }

                        if (advise != "")
                        {
                            sql.Clear();
                            sql.Append(" UPDATE tbl_Request SET ApplicationResult ='" + ApplicationResult.Denied + "' WHERE RequestID = '" +
                            this.requestID + "';");
                            db.Execute(sql.ToString());
                            sql.Clear();
                            db.OpenTable("SELECT Description FROM tbl_CreditAdvisor with (nolock) WHERE AdvisorID='8' and status='T'");
                            if (db.RecordCount() > 0)
                                advise = string.Format(db.Item("Description")
                                        , advise
                                        , DateTime.Now.Month
                                        , ((DateTime.Now.Month + 1 > 12) ? 1 : DateTime.Now.Month + 1).ToString());
                            writeAdvise(advise, "Legal Status", "LS", isMain);
                            LogUtil.logAction(sql.ToString(), "CTOS-Check Legal Status. RequestID:" + this.requestID);
                            autoCode += (isMain ? "MA" : "CA") + "-LS_";
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogUtil.logError(ex.Message, "CTOS-Check Legal Status");
                }
        } //LS - Legal Remark
        private void checkCtosTR(List<tr_report> reports, bool isMain)
        {
            if (!autoCode.Contains((isMain ? "MA" : "CA") + "-TR"))
            {
                wwdb db = new wwdb();
                string advise = string.Empty;
                string companyName;
                //string[] amount = new string[10];
                //int numberOfAmount = 0;
                decimal totalAmount = 0;
                decimal CountTotalTR = 0;

                long lessThanAmount = 1000; //default value 70%
                db.OpenTable(@"
                    SELECT ParameterValue FROM tbl_Parameter with (nolock) 
                    WHERE ParameterName = 'TradeReference_LessThanAmount' 
                    AND Category = 'AdviceDetail'
                ");
                if (db.RecordCount() > 0)
                    lessThanAmount = ConvertHelper.ConvertToInt64(db.Item("ParameterValue"), 1000);

                db.OpenTable("SELECT Description FROM tbl_CreditAdvisor with (nolock) WHERE AdvisorID='17' and status='T'");
                if (db.RecordCount() > 0)
                    advise = db.Item("Description") + Environment.NewLine;
                for (int count1 = 0; count1 < reports.Count(); count1++)
                {
                    var report = reports[count1];
                    companyName = report.header.ref_com_name;
                    for (int count2 = 0; count2 < report.enquiry.Count(); count2++)
                    {
                        var enquiry = report.enquiry[count2];
                        if (enquiry.section == null)
                            break;
                        for (int count3 = 0; count3 < enquiry.section.Count(); count3++) //search for account_status section
                        {
                            var section = enquiry.section[count3];
                            if (section.id.Equals("account_status"))
                            {
                                if (section.data == null) continue;
                                for (int count4 = 0; count4 < section.data.Count(); count4++)
                                {
                                    var data = section.data[count4];
                                    if (data.name.Equals("age")) //search for section age data
                                    {
                                        for (int count5 = 0; count5 < data.item.Count(); count5++) //search for the amount
                                        {
                                            var item = data.item[count5];
                                            if (ConvertHelper.ConvertToDecimal(item.Value, 0) > 0)
                                            {
                                                //there may be multiple cells containing data
                                                totalAmount += ConvertHelper.ConvertToDecimal(item.Value, 0);
                                                //amount[numberOfAmount] = item.Value;
                                                //numberOfAmount++;
                                            }
                                        }
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                    }
                    if (totalAmount > 0)
                    {
                        /*
                             Trade Reference (CTOS)
                             - {0} - RM{1}
                             **Advise to provide settlement letter before loan submission {2}
                             - {0} - RM{1}
                             **Advise to provide settlement letter before loan submission {2}
                        */
                        /*
                        TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                        db.OpenTable("SELECT Description FROM tbl_CreditAdvisor with (nolock) WHERE AdvisorID='16' and status='T'");
                        if (db.RecordCount() > 0)
                            advise += string.Format(db.Item("Description") + Environment.NewLine
                                        , textInfo.ToTitleCase(companyName.ToLower())
                                        , totalAmount.ToString()
                                        , totalAmount < lessThanAmount ? string.Format("(Less than RM{0}, optional)", lessThanAmount.ToString()) : "");
                        totalAmount = 0;
                        */
                        TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                        CountTotalTR = Convert.ToInt64(CountTotalTR) + Convert.ToInt64(totalAmount);
                        advise += " - " + textInfo.ToTitleCase(companyName.ToLower()) + " - RM" + totalAmount.ToString() + Environment.NewLine;
                        totalAmount = 0;
                    }
                }
                if (CountTotalTR > 0)
                {
                    db.OpenTable("SELECT Description FROM tbl_CreditAdvisor with (nolock) WHERE AdvisorID='16' and status='T'");
                    if (db.RecordCount() > 0)
                        advise += string.Format(db.Item("Description") + Environment.NewLine
                                  , CountTotalTR < lessThanAmount ? string.Format("(Less than RM{0}, optional)", lessThanAmount.ToString()) : "");
                    advise = advise.Replace("\r", "").Replace("\n ", "\n").Replace("\n\n", "\n");
                    writeAdvise(advise, "Trade Reference", "TR", isMain);

                    LogUtil.logAction("Advise: " + secure.RC(advise), /*type +*/ "Checking Trade Reference. RequestID:" + requestID);
                    autoCode += (isMain ? "MA" : "CA") + "-TR_";
                }
            }
        } //TR
        
        //==============================
        private void checkCtosLegalCase(enquiry enquiry, bool isMain)
        {

          //  if (!autoCode.Contains((isMain ? "MA" : "CA") + "-SAA"))
                try
                {
                    wwdb db = new wwdb();
                    string Plantiff;
                    string legalCase = enquiry.section_d.record.FirstOrDefault().title;
                    int CaseAmount = ConvertHelper.ConvertToInt(enquiry.section_d.record.FirstOrDefault().amount, 0);

                    if (enquiry.section_d.record.FirstOrDefault().title == null)
                    {
                        legalCase = " - ";
                    }

                    if (enquiry.section_d.record.FirstOrDefault().exparte != null)
                    {
                        Plantiff = enquiry.section_d.record.FirstOrDefault().exparte;
                    }
                    else if (enquiry.section_d.record.FirstOrDefault().plaintiff != null)
                    {
                        Plantiff = enquiry.section_d.record.FirstOrDefault().plaintiff;
                    }
                    else if (enquiry.section_d.record.FirstOrDefault().petitioner != null)
                    {
                        Plantiff = enquiry.section_d.record.FirstOrDefault().petitioner;
                    }
                    else if (enquiry.section_d.record.FirstOrDefault().assignee != null)
                    {
                        Plantiff = enquiry.section_d.record.FirstOrDefault().assignee;
                    }
                    else if (enquiry.section_d.record.FirstOrDefault().chargee != null)
                    {
                        Plantiff = enquiry.section_d.record.FirstOrDefault().chargee;
                    }
                    else if (enquiry.section_d.record.FirstOrDefault().applicant != null)
                    {
                        Plantiff = enquiry.section_d.record.FirstOrDefault().applicant;
                    }
                    else
                    {
                        Plantiff = " - ";
                    }

                    db.OpenTable("SELECT ApplicationResult FROM tbl_Request with (nolock) WHERE RequestID = '" + requestID.ToString() + "';");
                    if (db.RecordCount() > 0)
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Clear();
                        sql.Append(" UPDATE tbl_Request SET StatusID ='" + ApplicationStatus.InProgress +
                            "' , ApplicationResult ='" + ApplicationResult.Denied + "' WHERE RequestID = '" +
                            this.requestID.ToString() + "';");
                        db.Execute(sql.ToString());
                        sql.Clear();

                        //db.OpenTable("SELECT Description FROM tbl_CreditAdvisor with (nolock) WHERE AdvisorID = '8' and status='T'");
                        //string advise = db.Item("Description");
                        string advise = string.Empty;
                        TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

                        advise = textInfo.ToTitleCase(Plantiff.ToLower()) + Environment.NewLine + " - " + textInfo.ToTitleCase(legalCase.ToLower());    

                        if (CaseAmount > 0)
                        {
                            advise += " (RM" + CaseAmount + ")";
                        }

                        sql.Clear();
                        db.OpenTable("SELECT Description FROM tbl_CreditAdvisor with (nolock) WHERE AdvisorID = '23' and status='T'");
                        advise = string.Format(db.Item("Description"), advise.Replace("\r", ""));
                        if (enquiry.section_d.record.FirstOrDefault().settlement.date != null)
                        {
                            advise += " (" + enquiry.section_d.record.FirstOrDefault().settlement.date + ")";
                        }
                        advise = advise.Replace("\r", "").Replace("\n ", "\n").Replace("\n\n", "\n");
                        writeAdvise(advise, "Special Attention Account", "SAA", isMain);
                        LogUtil.logAction(sql.ToString(), "CTOS-Update Legal Case. RequestID:" + requestID.ToString());
                        if (!autoCode.Contains((isMain ? "MA" : "CA") + "-SAA"))
                            autoCode += (isMain ? "MA" : "CA") + "-SAA_";
                    }

                }
                catch (Exception ex)
                {
                    LogUtil.logError(ex.Message, "CTOS-Check Legal Case");
                }
        }
        //=============================Legal Case

        //==============================
        private void checkCreditApp(section_ccris section_ccris, bool isMain)
        {
            wwdb db = new wwdb();
            string CreApp = string.Empty;
            //  if (!autoCode.Contains((isMain ? "MA" : "CA") + "-LS"))
            try
            {
                int AppCount = section_ccris.applications.Count();
                if (AppCount > 0)
                {
                    for (int count1 = 0; count1 < section_ccris.applications.Count(); count1++)
                    {
                        var cApp = section_ccris.applications[count1];
                        var AppStatus = string.Empty;
                        if (cApp.status.code.Equals("A"))
                            AppStatus = "ACCEPTED";
                        else if (cApp.status.code.Equals("T"))
                            AppStatus = "APPROVED";
                        else
                            AppStatus = "PENDING";

                        CreApp += (count1 + 1) + ") " + cApp.date + " - " 
                                + (cApp.capacity.code.Equals("Own")? "( Own )":"(Joint)")  + " - "
                                + cApp.facility.code + " - RM" + string.Format("{0:#,0}", Convert.ToDouble(cApp.amount)) 
                                + " (" + AppStatus + ")" + Environment.NewLine;
                }

                    StringBuilder sql = new StringBuilder();
                    string advise = string.Empty;
                    sql.Clear();

                    db.OpenTable("SELECT Description FROM tbl_CreditAdvisor with (nolock) WHERE AdvisorID='24' and status='T'");
                    if (db.RecordCount() > 0)
                    {
                        advise += string.Format(db.Item("Description")) + Environment.NewLine;
                        advise += CreApp;
                        advise = advise.Replace("\r", "").Replace("\n ", "\n").Replace("\n\n", "\n");
                        writeAdvise(advise, "Credit Application", "APP", isMain);
                        LogUtil.logAction(sql.ToString(), "CTOS-Check Legal Status. RequestID:" + this.requestID);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtil.logError(ex.Message, "Check Credit Applications");
            }
        }
        //==============================Check Credit Application
        #endregion
        #region RAMCI Automation
        private void checkRamciAKPK(string code, bool isMain)
        {
            wwdb db = new wwdb();

            if (!autoCode.Contains((isMain ? "MA" : "CA") + "-AKPK"))
                if (code.Equals("K"))
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Clear();
                    sql.Append(" UPDATE tbl_Request SET StatusID ='" + ApplicationStatus.InProgress +
                        "' , ApplicationResult ='" + ApplicationResult.Denied + "' WHERE RequestID = '" +
                        this.requestID.ToString() + "';");
                    db.Execute(sql.ToString());
                    sql.Clear();

                    db.OpenTable("SELECT Description FROM tbl_CreditAdvisor with (nolock) WHERE AdvisorID='6' and status='T'");
                    string advise = db.Item("Description");
                    writeAdvise(advise, "AKPK", "AKPK", isMain);
                    LogUtil.logAction(sql.ToString(), "RAMCI-Update AKPK. RequestID:" + this.requestID.ToString());
                    autoCode += (isMain ? "MA" : "CA") + "-AKPK_";
                }
        } //AKPK
        private void checkRamciSAA(xml report, bool isMain)
        {
            wwdb db = new wwdb();

            if (!autoCode.Contains((isMain ? "MA" : "CA") + "-SAA"))
                try
                {
                    string special_attention_count = string.Empty, special_attention_status = string.Empty;
                    if (report.summary.info_summary.special_attention_account_count != null)
                        special_attention_count = report.summary.info_summary.special_attention_account_count;
                    if (report.banking_info.ccris_banking_summary.summary_liabilities.special_attention_account != null)
                        special_attention_status = report.banking_info.ccris_banking_summary.summary_liabilities.special_attention_account;

                    if ((!string.IsNullOrEmpty(special_attention_count) && !string.IsNullOrWhiteSpace(special_attention_count))
                        || (!string.IsNullOrEmpty(special_attention_status) && !string.IsNullOrWhiteSpace(special_attention_status)))
                        if (ConvertHelper.ConvertToInt(special_attention_count, 0) > 0 || special_attention_status.Equals("Y"))
                        {
                            db.OpenTable("SELECT ApplicationResult FROM tbl_Request with (nolock) WHERE RequestID = '" + requestID.ToString() + "';");
                            if (db.RecordCount() > 0)
                            {
                                StringBuilder sql = new StringBuilder();
                                sql.Clear();
                                sql.AppendFormat(@" 
                                    UPDATE tbl_Request SET StatusID ='{0}', ApplicationResult ='{1}' WHERE RequestID = '{2}';
                                ", ApplicationStatus.InProgress
                                , ApplicationResult.Denied
                                , requestID);
                                db.Execute(sql.ToString());
                                sql.Clear();
                                /*
                                    **Special Attention Account (SAA)**
                                    - Term loan - 29/05/2017 (Last update - 31/03/2019)
                                */
                                db.OpenTable("SELECT Description FROM tbl_CreditAdvisor with (nolock) WHERE AdvisorID = '7' and status='T'");
                                string advise = db.Item("Description");
                                var special_attention_acc = report.banking_info.ccris_banking_details.special_attention_account;
                                for (int count1 = 0; count1 < special_attention_acc.Count(); count1++)
                                {
                                    var item = special_attention_acc[count1];
                                    string approval_date = string.Empty, position_date = string.Empty, facility = string.Empty;

                                    if (item.master != null && item.master.item != null)
                                        approval_date = item.master.item.date;
                                    for (int count2 = 0; count2 < item.sub_account.Count(); count2++)
                                    {
                                        var sub_account = item.sub_account[count2];
                                        advise += Environment.NewLine + "- ";
                                        facility = sub_account.item.facility;
                                        db.OpenTable("SELECT LoanID FROM tbl_LoanTypesXML with (nolock) WHERE LoanTypeCode='" + facility + "'");
                                        switch (db.Item("LoanID"))
                                        {
                                            case "1":
                                                advise += Resources.resource.Housing_Loan;
                                                break;
                                            case "2":
                                                advise += Resources.resource.Commercial_Loan;
                                                break;
                                            case "3":
                                                advise += Resources.resource.Personal_Loan;
                                                break;
                                            case "4":
                                                advise += Resources.resource.OverDraft;
                                                break;
                                            case "5":
                                                advise += Resources.resource.Term_Loan;
                                                break;
                                            case "6":
                                                advise += Resources.resource.Car_Loan;
                                                break;
                                            case "7":
                                                advise += Resources.resource.Credit_Card;
                                                break;
                                            default:
                                                break;
                                        }
                                        advise += " - " + approval_date;
                                        if (sub_account.item.balance_updated_date != null)
                                        {
                                            position_date = sub_account.item.balance_updated_date;
                                            advise += " (Last Update - " + position_date + ")";
                                        }
                                    }
                                }
                                advise = advise.Replace("\r", "").Replace("\n ", "\n").Replace("\n\n", "\n");
                                writeAdvise(advise, "Special Attention Account", "SAA", isMain);
                                LogUtil.logAction(sql.ToString(), "RAMCI-Update Special Attention Account. RequestID:" + requestID.ToString());
                                autoCode += (isMain ? "MA" : "CA") + "-SAA_";
                            }
                        }
                }
                catch (Exception ex)
                {
                    LogUtil.logError(ex.Message, "RAMCI-Check SAA");
                }
        } //SAA
        private void checkRamciLegal(string legal_status, bool isMain)
        {
            wwdb db = new wwdb();

            if (!autoCode.Contains((isMain ? "MA" : "CA") + "-LS"))
                try
                {
                    if (!string.IsNullOrEmpty(legal_status) && legal_status.Equals("Y"))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Clear();
                        sql.Append(" UPDATE tbl_Request SET StatusID ='" + ApplicationStatus.InProgress +
                            "' , ApplicationResult ='" + ApplicationResult.Denied + "' WHERE RequestID = '" +
                            this.requestID + "';");
                        db.Execute(sql.ToString());
                        sql.Clear();

                        db.OpenTable("SELECT Description FROM tbl_CreditAdvisor with (nolock) WHERE AdvisorID='8' and status='T'");
                        string advise = db.Item("Description");
                        writeAdvise(advise, "Legal Status", "LS", isMain);
                        LogUtil.logAction(sql.ToString(), "RAMCI-Update Special Attention Account. RequestID:" + requestID);
                        autoCode += (isMain ? "MA" : "CA") + "-LS_";
                    }
                }
                catch (Exception ex)
                {
                    LogUtil.logError(ex.Message, "RAMCI-Check Legal Status");
                }
        } //LS
        private void checkRamciTR(string tradeRefCount, List<xmlTrade_bureauTrade_bureau_entity_details> tradeRefs, bool isMain)
        {
            if (!autoCode.Contains((isMain ? "MA" : "CA") + "-TR"))
            {
                wwdb db = new wwdb();
                string advise = string.Empty;
                string companyName;
                decimal ExpTotalAmount = 0;
                decimal CountTotalTR = 0;
                long lessThanAmount = 1000; //default value 70% - Aaron 17/03

                if (ConvertHelper.ConvertToInt(tradeRefCount, 0) > 0)
                {
                    db.OpenTable("SELECT Description FROM tbl_CreditAdvisor with (nolock) WHERE AdvisorID='17' and status='T'");
                    if (db.RecordCount() > 0)
                        advise = db.Item("Description") + Environment.NewLine; ;
                    for (int count = 0; count < tradeRefs.Count; count++)
                    {
                        var tradeRef = tradeRefs[count];
                        companyName = tradeRef.creditor_name;
                        ExpTotalAmount = ConvertHelper.ConvertToDecimal(tradeRef.amount, 0);
                        /*
                             Trade Reference (CTOS)
                             - {0} - RM{1}
                             **Advise to provide settlement letter before loan submission {2}
                             - {0} - RM{1}
                             **Advise to provide settlement letter before loan submission {2}
                        */
                        TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                        CountTotalTR = Convert.ToInt64(CountTotalTR) + Convert.ToInt64(ExpTotalAmount);
                        advise += " - " + textInfo.ToTitleCase(companyName.ToLower()) + " - RM" + ExpTotalAmount.ToString() + Environment.NewLine;
                        ExpTotalAmount = 0;
                    }
                    db.OpenTable("SELECT Description FROM tbl_CreditAdvisor with (nolock) WHERE AdvisorID='16' and status='T'");
                    if (db.RecordCount() > 0)
                        advise += string.Format(db.Item("Description") + Environment.NewLine
                                  , CountTotalTR < lessThanAmount ? string.Format("(Less than RM{0}, optional)", lessThanAmount.ToString()) : "");
                    advise = advise.Replace("\r", "").Replace("\n ", "\n").Replace("\n\n", "\n");
                    writeAdvise(secure.RC(advise), "Trade Reference", "TR", isMain);
                }
                LogUtil.logAction("Advise: " + secure.RC(advise), /*type +*/ "Checking Trade Reference. RequestID:" + requestID);
                autoCode += (isMain ? "MA" : "CA") + "-TR_";
            }
        }
        #endregion
        #endregion

        #region Report
        public bool checkReuseReport(string newIC, bool isMain, bool copy = false)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            string year = System.DateTime.Now.Year.ToString();
            string month = System.DateTime.Now.Month.ToString();
            string day = System.DateTime.Now.Day.ToString();

            string date1 = year + "-" + month + "-15";
            string date2 = year + "-" + month + "-" + day;

            sql.Clear();
            sql.AppendFormat(@"SELECT cr.Result, cr.Type, cr.GeneratedDate, mi.Fullname, l.BranchID 
                                FROM tbl_CTOS_Result cr with(nolock) inner join 
                                tbl_Request r on r.RequestID = Request_Id inner join 
                                tbl_MemberInfo mi on r.MemberID = mi.MemberId inner join
                                tbl_Login l on mi.memberId = l.login_id
                                where IC_Number = '{0}' AND 
								((cr.GeneratedDate between '{1}' and DATEADD(DAY, 16, '{1}')) 
								OR (cr.GeneratedDate between '{2}' and DATEADD(DAY, 2, '{2}'))) 
                                AND cr.Report_Type='I'
                                order by cr.GeneratedDate desc 
                                ", secure.RC(newIC), date1, date2);
            db.OpenTable(sql.ToString());
            if (db.RecordCount() > 0)
            {
                string decodedString = db.Item("Result");
                if (db.Item("Type").Equals("CTOS"))
                {
                    try
                    {
                        const string serviceUnavailable = "Service Unavailable";
                        XmlSerializer serializer = new XmlSerializer(typeof(report));
                        StringReader stringReader = new StringReader(decodedString);
                        report report = (report)serializer.Deserialize(stringReader);

                        if (report != null && report.enq_report.summary.FirstOrDefault() != null)
                        {
                            if (report.enq_report.summary.FirstOrDefault().enq_status.Value.Equals(serviceUnavailable))
                                return false;
                        }
                        else
                            LogUtil.logAction("Empty string or empty report.", "ReuseReport: Deserializing decoded string.");
                    }
                    catch (Exception ex) { LogUtil.logError(ex.Message, "ReuseReport: Error reading decoded string."); }
                }
                member.Fullname = db.Item("Fullname");
                member.CreatedBy = db.Item("GeneratedDate"); //temp use member.CreatedBy to store report GeneratedDate
                if (!string.IsNullOrEmpty(db.Item("BranchID")))
                    member.BranchList.BranchID = db.Item("BranchID");
                if (copy)
                {
                    if (isMain)
                        MAtype = db.Item("Type");
                    else
                        CAtype = db.Item("Type");
                    //this.type = type;

                    //copy another row for another request
                    try
                    {
                        sql.Clear();
                        sql.AppendFormat(@"
                            UPDATE tbl_CTOS_Result SET IsActive = 0 
                            WHERE Request_Id = N'{0}' AND IC_Number = '{1}' AND IsCoApplicant = '{2}';
                            INSERT INTO tbl_CTOS_Result 
                            (Request_Id, IC_Number, Report_Type, IsCoApplicant, Result, GeneratedDate, IsActive, Type, isReuse) 
                            VALUES(N'{0}', '{3}', 'I', '{4}', '{5}', GETDATE(), '1', '{6}', 1);
                        ", secure.RC(this.requestID), secure.RC(newIC), (!isMain) ? 1 : 0
                        , secure.RC(newIC), !isMain, secure.RC(decodedString), isMain ? MAtype : CAtype);
                        db.Execute(sql.ToString());
                        LogUtil.logAction(sql.ToString(), "COPY row INTO tbl_CTOS_Result RequestID:" + this.requestID);

                        if (db.HasError)
                        {
                            LogUtil.logError(db.ErrorMessage, sql.ToString());
                            return false;
                        }
                        else
                            return true;
                    }
                    catch (Exception ex)
                    {
                        LogUtil.logError(ex.Message, sql.ToString());
                        return false;
                    }
                }
                else
                    return true;
            }//else new report will be pulled

            return false;
        }
        public bool RetrieveXMLDataForMainCoApp(string requestId, string mName, string oldIC, string newIC, string mType,
            bool haveCoApplicant, string age, string loanAmount, string hfMainCCRIS, string hfCoCCRIS, bool isMain, bool callAPI = false,
            string apiType = "")
        {
            this.requestID = requestId;
            string errorResponse = string.Empty;
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            string decodedString = string.Empty;
            string type = string.Empty;

            string entitiesNumber = "90007";
            bool CTOSHasMultipleEntities = false;
            do
            {
                CTOSHasMultipleEntities = false;
                sql.Clear();
                sql.Append(" SELECT Result, Type ");
                sql.Append(" FROM tbl_CTOS_Result WITH(NOLOCK) ");
                sql.Append(" WHERE IsActive = 1 ");
                if (tempRequestID.Trim() == string.Empty || tempRequestID == null)
                    sql.Append(" AND Request_Id = N'" + secure.RC(requestId) + "'");
                else
                    sql.Append(" AND TempId = N'" + tempRequestID + "'");
                sql.Append(" AND IsCoApplicant = N'" + (isMain == true ? 0 : 1) + "'");
                db.OpenTable(sql.ToString());

                if (db.RecordCount() > 0)
                {
                    if (hfMainCCRIS == "false" && isMain == true)
                    {
                        decodedString = db.Item("Result").ToString();
                        type = db.Item("Type").ToString();
                    }
                    else if (hfCoCCRIS == "false" && isMain == false)
                    {
                        decodedString = db.Item("Result").ToString();
                        type = db.Item("Type").ToString();
                    }
                    else
                    {
                        sql.Clear();
                        sql.Append(" UPDATE tbl_CTOS_Result SET IsActive = 0 WHERE");
                        if (tempRequestID.Trim() == string.Empty || tempRequestID == null)
                            sql.Append(" Request_Id = N'" + secure.RC(requestId) + "'");
                        else
                            sql.Append(" TempId = N'" + tempRequestID + "'");
                        sql.Append(" AND IsCoApplicant = N'" + (isMain == true ? 0 : 1) + "'");
                        db.Execute(sql.ToString());
                    }
                    if (isMain)
                        MAtype = type;
                    else
                        CAtype = type;
                }

                #region CTOS
                if (decodedString.Trim() == string.Empty && callAPI == true && apiType == "CTOS")
                {
                    type = apiType;
                    string API_URL, API_Username, API_Password, API_ContentType, API_Method, API_CompanyCode, API_AccountNo, API_Version;
                    API_URL = ConfigurationManager.AppSettings["API_URL"];
                    API_Username = ConfigurationManager.AppSettings["API_Username"];
                    API_Password = ConfigurationManager.AppSettings["API_Password"];
                    API_ContentType = ConfigurationManager.AppSettings["API_ContentType"];
                    API_Method = ConfigurationManager.AppSettings["API_Method"];
                    API_CompanyCode = ConfigurationManager.AppSettings["API_CompanyCode"];
                    API_AccountNo = ConfigurationManager.AppSettings["API_AccountNo"];
                    API_Version = ConfigurationManager.AppSettings["API_Version"];

                    //convert the xml string to byte array
                    var xmlText = @"<?xml version=""1.0"" encoding=""UTF-8""?>
            <soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
                <soapenv:Body>
                    <tns:request xmlns:tns=""http://ws.proxy.xml.ctos.com.my/"">
                        <input>{0}</input>
                    </tns:request>
                </soapenv:Body>
            </soapenv:Envelope>";
                    var payload = @"<batch output=""{0}"" no=""0009"" xmlns=""http://ws.cmctos.com.my/ctosnet/request"">
                <company_code>" + API_CompanyCode + @"</company_code>
                <account_no>" + API_AccountNo + @"</account_no>
                <user_id>" + API_Username + @"</user_id>
                <record_total>1</record_total>
                <records>
                <type>{1}</type>
                <ic_lc>{2}</ic_lc>
                <nic_br>{3}</nic_br>
                <name>{4}</name>
                <mphone_nos/>
                <ref_no></ref_no>
                <dist_code>PENANG BRANCH</dist_code>
                <purpose code=""200"">Credit evaluation/account opening on subject/directors/shareholder with consent /due diligence on AMLA compliance</purpose>
                <include_consent>0</include_consent>
                <include_ctos>1</include_ctos>
                <include_trex>1</include_trex>
                <include_ccris sum=""0"">1</include_ccris>
                <include_dcheq>0</include_dcheq>
                <include_ccris_supp>0</include_ccris_supp> 
                <include_fico>0</include_fico>
                <include_sfi>0</include_sfi>
                <include_ssm>0</include_ssm>
                <confirm_entity>{5}</confirm_entity>
                </records>
            </batch>";

                    string xmlRequestBody = string.Format(xmlText, System.Net.WebUtility.HtmlEncode(string.Format(payload, "0", mType, oldIC, newIC, mName, entitiesNumber)));
                    string _result;
                    
                    try
                    {
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12; 
                        var request = (HttpWebRequest)WebRequest.Create(API_URL);
                        request.Method = API_Method;
                        request.ContentType = API_ContentType;
                        var writer = new StreamWriter(request.GetRequestStream());
                        writer.Write(xmlRequestBody);
                        writer.Close();

                        // newly added code for CTOS new authentication
                        var posted = AuthenticatedCtosRequest(request);

                        var response = posted.Result;

                        if (response == null)
                        {
                            throw new Exception("Unable to get valid login response from CTOS");
                        }

                        var streamResponse = response.GetResponseStream();
                        var streamRead = new StreamReader(streamResponse);

                        _result = streamRead.ReadToEnd().Trim();

                        int startIndex = _result.IndexOf("<return>") + 8;
                        int endIndex = _result.IndexOf("</return>");

                        var result2 = _result.Substring(startIndex, endIndex - startIndex);
                        byte[] data = Convert.FromBase64String(result2);
                        decodedString = Encoding.UTF8.GetString(data);
                        //------------------------------To prompt message when CTOS system down

                        const string serviceUnavailable = "Service Unavailable";
                        XmlSerializer serializer = new XmlSerializer(typeof(report));
                        StringReader stringReader = new StringReader(decodedString);
                        report report = (report)serializer.Deserialize(stringReader);
                        if (report != null && report.enq_report.summary.FirstOrDefault() != null)
                        {
                            if (report.enq_report.summary.FirstOrDefault().enq_status.Value.Equals(serviceUnavailable))
                                return false;
                            else if (report.enq_report.summary.FirstOrDefault().enq_status.Value.Equals("CCRIS Services Unavailable"))
                                return false;
                        }
                        //------------------------------
                        streamRead.Close();
                        streamResponse.Close();
                        response.Close();
                    }
                    catch (Exception ex)
                    {
                        LogUtil.logError(ex.Message, "Error pulling " + apiType + " report. RequestID:" + requestId);
                        string externalip = new WebClient().DownloadString("http://icanhazip.com");
                        Console.WriteLine(externalip);
                        return false;
                    }

                    StringBuilder sqlInsert = new StringBuilder();
                    StringBuilder sqlValues = new StringBuilder();
                    sql.Clear();
                    sqlInsert.Clear();
                    sqlValues.Clear();
                    sqlInsert.Append(" INSERT INTO tbl_CTOS_Result ");
                    sqlInsert.Append(" (Request_Id, IC_Number, Report_Type, IsCoApplicant, Result, GeneratedDate, IsActive, Type, RequestPayload, IsReuse");
                    sqlValues.Append(" VALUES( N'" + secure.RC(requestId) + "', N'" + secure.RC(newIC) + "','" + secure.RC(mType) + "'," +
                        (isMain == true ? 0 : 1) + ",'" + secure.RC(decodedString) + "',GETDATE(),1,N'" + secure.RC(type) + "', N'" +
                        secure.RC(xmlRequestBody) + "', 0");
                    if (tempRequestID.Trim() != string.Empty)
                    {
                        sqlInsert.Append(",TempId");
                        sqlValues.Append(",'" + tempRequestID + "'");
                    }
                    sqlInsert.Append(")");
                    sqlValues.Append(")");

                    sql.Append(sqlInsert);
                    sql.Append(sqlValues);
                    db.Execute(sql.ToString());
                }
                #endregion

                #region RAMCI
                if (decodedString.Trim() == string.Empty && callAPI == true && apiType == "RAMCI")
                {
                    type = apiType;
                    string RAMCI_API_REPORT_URL, RAMCI_API_XML_URL, RAMCI_API_Method, RAMCI_API_ContentType, RAMCI_API_Username, RAMCI_API_Password, RAMCI_API_Authorization;
                    RAMCI_API_REPORT_URL = ConfigurationManager.AppSettings["RAMCI_API_REPORT_URL"];
                    RAMCI_API_XML_URL = ConfigurationManager.AppSettings["RAMCI_API_XML_URL"];
                    RAMCI_API_Method = ConfigurationManager.AppSettings["RAMCI_API_Method"];
                    RAMCI_API_ContentType = ConfigurationManager.AppSettings["RAMCI_API_ContentType"];
                    RAMCI_API_Username = ConfigurationManager.AppSettings["RAMCI_API_Username"];
                    RAMCI_API_Password = ConfigurationManager.AppSettings["RAMCI_API_Password"];
                    var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(RAMCI_API_Username + ":" + RAMCI_API_Password);
                    RAMCI_API_Authorization = "Basic " + System.Convert.ToBase64String(plainTextBytes);

                    string requestPayload = string.Empty;
                    try
                    {
                        #region Search

                        var searchPayload = @"<?xml version=""1.0"" encoding=""UTF-8""?>
                        <xml>
                            <request>
                                <ProductType>CCRIS_SEARCH</ProductType>
		                        <GroupCode>11</GroupCode>
		                        <EntityName>{0}</EntityName>
		                        <EntityId>{1}</EntityId>
		                        <EntityId2></EntityId2>
		                        <Country>MY</Country>
		                        <DOB>{2}</DOB>
                            </request>
                        </xml>";

                        string searchXmlRequestBody = string.Format(searchPayload, mName, newIC.Insert(6, "-").Insert(9, "-"), GetDateFromIc(newIC));
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                        var searchRequest = (HttpWebRequest)WebRequest.Create(RAMCI_API_REPORT_URL);
                        searchRequest.Method = RAMCI_API_Method;
                        searchRequest.ContentType = RAMCI_API_ContentType;
                        searchRequest.Headers["Authorization"] = RAMCI_API_Authorization;
                        var searchWriter = new StreamWriter(searchRequest.GetRequestStream());
                        searchWriter.Write(searchXmlRequestBody);
                        searchWriter.Close();

                        var searchResponse = (HttpWebResponse)searchRequest.GetResponse();
                        var searchStreamResponse = searchResponse.GetResponseStream();
                        var searchStreamRead = new StreamReader(searchStreamResponse);
                        string searchResponseXml = searchStreamRead.ReadToEnd().Trim();
                        searchStreamRead.Close();
                        searchStreamResponse.Close();
                        searchResponse.Close();
                        XmlSerializer searchSerializer = new XmlSerializer(typeof(HJT.RAMCI.Model.Search.Xml));
                        StringReader searchStringReader = new StringReader(searchResponseXml);
                        HJT.RAMCI.Model.Search.Xml searchResponseObject = (HJT.RAMCI.Model.Search.Xml)searchSerializer.Deserialize(searchStringReader);
                        errorResponse = searchResponseXml;
                        #endregion

                        #region Confirm
                        var confirmPayload = @"<?xml version=""1.0"" encoding=""UTF-8""?>
                        <xml>
                            <request>
                                <ProductType>IRISS</ProductType>
		                        <CRefId>{0}</CRefId>
		                        <EntityKey>{1}</EntityKey>
		                        <MobileNo></MobileNo>
		                        <EmailAddress>aaron@whopay.com.my</EmailAddress>
		                        <LastKnownAddress></LastKnownAddress>
		                        <ConsentGranted>Y</ConsentGranted>
		                        <EnquiryPurpose>REVIEW</EnquiryPurpose>
		                        <Ref1>Company Name</Ref1>
		                        <Ref2></Ref2>
		                        <Ref3></Ref3>
		                        <Ref4></Ref4>
                            </request>
                        </xml>";
                        string crefId = searchResponseObject.Ccris_identity.Item.FirstOrDefault().CRefId;
                        string entityKey = searchResponseObject.Ccris_identity.Item.FirstOrDefault().EntityKey;

                        string confirmXmlRequestBody = string.Format(confirmPayload, crefId, entityKey);
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12; //aaron 25/10
                        var confirmRequest = (HttpWebRequest)WebRequest.Create(RAMCI_API_REPORT_URL);
                        confirmRequest.Method = RAMCI_API_Method;
                        confirmRequest.ContentType = RAMCI_API_ContentType;
                        confirmRequest.Headers["Authorization"] = RAMCI_API_Authorization;
                        var confirmWriter = new StreamWriter(confirmRequest.GetRequestStream());
                        confirmWriter.Write(confirmXmlRequestBody);
                        confirmWriter.Close();

                        var confirmResponse = (HttpWebResponse)confirmRequest.GetResponse();
                        var confirmStreamResponse = confirmResponse.GetResponseStream();
                        var confirmStreamRead = new StreamReader(confirmStreamResponse);
                        string confirmResponseXml = confirmStreamRead.ReadToEnd().Trim();
                        confirmStreamRead.Close();
                        confirmStreamResponse.Close();
                        confirmResponse.Close();
                        XmlSerializer confirmSerializer = new XmlSerializer(typeof(HJT.RAMCI.Model.Confirm.Xml));
                        StringReader confirmStringReader = new StringReader(confirmResponseXml);
                        HJT.RAMCI.Model.Confirm.Xml confirmResponseObject = (HJT.RAMCI.Model.Confirm.Xml)confirmSerializer.Deserialize(confirmStringReader);
                        errorResponse = confirmResponseXml;
                        #endregion

                        #region Report
                        var reportPayload = @"<?xml version=""1.0"" encoding=""UTF-8""?>
                        <xml>
                            <request>
                                <token1>{0}</token1>
	                            <token2>{1}</token2>
                        </request>
                        </xml>";
                        string token1 = confirmResponseObject.Token1;
                        string token2 = confirmResponseObject.Token2;

                        int retryCounter = 0;

                        //maximum retry 20 * 10 seconds = 200s (3m20s)
                        while (retryCounter < 20)
                        {
                            string reportXmlRequestBody = string.Format(reportPayload, token1, token2);
                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12; //aaron 26/10
                            var reportRequest = (HttpWebRequest)WebRequest.Create(RAMCI_API_XML_URL);
                            reportRequest.Method = RAMCI_API_Method;
                            reportRequest.ContentType = RAMCI_API_ContentType;
                            reportRequest.Headers["Authorization"] = RAMCI_API_Authorization;
                            var reportWriter = new StreamWriter(reportRequest.GetRequestStream());
                            requestPayload = reportXmlRequestBody;
                            reportWriter.Write(reportXmlRequestBody);
                            reportWriter.Close();

                            var reportResponse = (HttpWebResponse)reportRequest.GetResponse();
                            var reportStreamResponse = reportResponse.GetResponseStream();
                            var reportStreamRead = new StreamReader(reportStreamResponse);
                            string reportResponseXml = reportStreamRead.ReadToEnd().Trim();
                            reportStreamRead.Close();
                            reportStreamResponse.Close();
                            reportResponse.Close();
                            decodedString = reportResponseXml;
                            errorResponse = reportResponseXml;

                            if (reportResponse.StatusCode == (HttpStatusCode)102 || reportResponseXml.Contains("<code>102</code>"))
                            {
                                retryCounter++;
                                Thread.Sleep(10000);
                            }
                            else
                                break;
                        }
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        string externalip = new WebClient().DownloadString("http://icanhazip.com");
                        LogUtil.logError(ex.ToString(), "public IP:" + externalip + ", response:" + errorResponse);
                        return false;
                    }

                    StringBuilder sqlInsert = new StringBuilder();
                    StringBuilder sqlValues = new StringBuilder();
                    sql.Clear();
                    sqlInsert.Clear();
                    sqlValues.Clear();
                    sqlInsert.Append(" INSERT INTO tbl_CTOS_Result ");
                    sqlInsert.Append(" (Request_Id, IC_Number, Report_Type, IsCoApplicant, Result, GeneratedDate, IsActive, Type, RequestPayload, isReuse");
                    sqlValues.Append(" VALUES( N'" + secure.RC(requestId) + "', N'" + secure.RC(newIC) + "','" + secure.RC(mType) + "'," +
                        (isMain == true ? 0 : 1) + ",'" + secure.RC(decodedString) + "',GETDATE(),1,N'" + secure.RC(type) + "',N'" +
                        secure.RC(requestPayload) + "', 0");
                    if (tempRequestID.Trim() != string.Empty)
                    {
                        sqlInsert.Append(",TempId");
                        sqlValues.Append(",'" + tempRequestID + "'");
                    }
                    sqlInsert.Append(")");
                    sqlValues.Append(")");

                    sql.Append(sqlInsert);
                    sql.Append(sqlValues);
                    db.Execute(sql.ToString());
                }

                #endregion
                if (decodedString.Trim() != string.Empty)
                {
                    if (type == "CTOS")
                    {
                        //XmlController _controller = new XmlController();
                        //HJT.Cls.Model.XmlModel.LOAN.report _Model = new HJT.Cls.Model.XmlModel.LOAN.report();
                        //_Model = _controller.DeserializeXml<HJT.Cls.Model.XmlModel.LOAN.report>(decodedString);
                        XmlSerializer serializer = new XmlSerializer(typeof(report));
                        StringReader stringReader = new StringReader(decodedString);
                        report report = (report)serializer.Deserialize(stringReader);

                        if (report.enq_report.entities != null && report.enq_report.enquiry == null)
                        {
                            decodedString = string.Empty;
                            entitiesNumber = report.enq_report.entities.FirstOrDefault().entity.FirstOrDefault().key;
                            CTOSHasMultipleEntities = true;
                        }
                        else
                        {
                            var enquiry = report.enq_report.enquiry.FirstOrDefault();

                            checkExistingAuto();

                            if (enquiry != null)
                            {
                                if (enquiry.section_ccris != null)
                                {
                                    this.section_ccris = enquiry.section_ccris;
                                    countFacilities(enquiry.section_ccris, isMain, ConvertHelper.ConvertToInt(age, 0),
                                        ConvertHelper.ConvertToDecimal(loanAmount, 0), mName, newIC, haveCoApplicant);
                                    checkCreditCardUsage(enquiry.section_ccris, isMain);
                                }

                                if (enquiry.section_ccris.special_attention_accs != null)
                                    checkCtosSAA(enquiry, isMain);

                                if (report.tr_report != null)
                                    checkCtosTR(report.tr_report.ToList(), isMain);

                                if (enquiry.section_ccris != null)
                                {
                                    checkCtosLegal(enquiry.section_ccris, isMain);
                                    checkCreditApp(enquiry.section_ccris, isMain);

                                    //if (enquiry.section_ccris.accounts != null &&
                                    //    enquiry.section_ccris.accounts.account != null)
                                    //    checkCCRIScount(enquiry.section_ccris.accounts.account.ToList(), isMain, !haveCoApplicant);
                                }
                                //--------------------Aaron add in check Legal Case - Section D
                                if (enquiry.section_d.data.Equals(true))
                                {
                                    checkCtosLegalCase(enquiry, isMain);
                                }
                                //---------------------------------------------------
                            }
                            sql.Clear();
                            sql.Append(" SELECT ID, IsCoApplicant ");
                            sql.Append(" FROM tbl_CTOS_Result WITH(NOLOCK) ");
                            sql.Append(" WHERE IsActive = 1 ");
                            if (tempRequestID.Trim() == string.Empty || tempRequestID == null)
                                sql.Append(" AND Request_Id = N'" + secure.RC(requestId) + "'");
                            else
                                sql.Append(" AND TempId = N'" + tempRequestID + "'");
                            sql.Append(" AND IC_Number ='" + secure.RC(newIC) + "'");
                            db.OpenTable(sql.ToString());
                            if (db.RecordCount() > 0)
                            {
                                while (!db.Eof())
                                {
                                    if (db.Item("IsCoApplicant").Equals("False"))
                                        this.mainResultID = db.Item("ID").ToString();
                                    else
                                        this.coResultID = db.Item("ID").ToString();
                                    db.MoveNext();
                                }
                            }
                            //finishChecking();
                            return true;
                        }
                    }

                    if (type == "RAMCI")
                    {
                        XmlSerializer reportSerializer = new XmlSerializer(typeof(xml));
                        StringReader reportStringReader = new StringReader(decodedString);
                        xml reportResponseObject = (xml)reportSerializer.Deserialize(reportStringReader);
                        xmlBanking_info enquiry = reportResponseObject.banking_info;

                        checkExistingAuto();
                        if (enquiry.ccris_banking_details != null)
                        {
                            this.detail = enquiry.ccris_banking_details;
                            countFacilities(enquiry.ccris_banking_details, isMain, ConvertHelper.ConvertToInt(age, 0),
                                ConvertHelper.ConvertToDecimal(loanAmount, 0), mName, newIC, haveCoApplicant);
                            checkCreditCardUsage(enquiry.ccris_banking_details, isMain);
                        }

                        checkRamciSAA(reportResponseObject, isMain);

                        if (reportResponseObject.trade_bureau != null && reportResponseObject.trade_bureau.trade_bureau_entity_details != null)
                            checkRamciTR(reportResponseObject.summary.info_summary.trade_bureau_count,
                                reportResponseObject.trade_bureau.trade_bureau_entity_details.ToList(), isMain);

                        if (enquiry != null)
                        {
                            if (enquiry.ccris_banking_summary.summary_liabilities != null)
                                checkRamciLegal(enquiry.ccris_banking_summary.summary_liabilities.legal_action_taken, isMain);

                            //if (detail != null && detail.outstanding_credit != null) 
                            //        checkCCRIScount(detail.outstanding_credit.ToList(), isMain, !haveCoApplicant);
                        }

                        sql.Clear();
                        sql.Append(" SELECT ID, IsCoApplicant ");
                        sql.Append(" FROM tbl_CTOS_Result WITH(NOLOCK) ");
                        sql.Append(" WHERE IsActive = 1 ");
                        if (tempRequestID.Trim() == string.Empty || tempRequestID == null)
                            sql.Append(" AND Request_Id = N'" + secure.RC(requestId) + "'");
                        else
                            sql.Append(" AND TempId = N'" + tempRequestID + "'");
                        sql.Append(" AND IC_Number ='" + secure.RC(newIC) + "'");
                        db.OpenTable(sql.ToString());
                        if (db.RecordCount() > 0)
                        {
                            while (!db.Eof())
                            {
                                if (db.Item("IsCoApplicant").Equals("False"))
                                    this.mainResultID = db.Item("ID").ToString();
                                else
                                    this.coResultID = db.Item("ID").ToString();
                                db.MoveNext();
                            }
                        }
                        //finishChecking();
                        return true;
                        //passdata
                    }
                }
            } while (CTOSHasMultipleEntities);

            return false;
            //Response.Write(decodedString);
        }
        public void InsertCTOSReport(List<ccris_account_detail> accounts, bool isMain)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            int RowCount = 0;
            int SortOrder = 0;
            string resultID;

            if (isMain)
                resultID = this.mainResultID;
            else
                resultID = this.coResultID;

            for (int count1 = 0; count1 < accounts.Count(); count1++)
            {
                var _account = accounts[count1];
                RowCount += 1;
                SortOrder += 1;
                int tempPTPTN = 0;
                int colcount = 0;

                StringBuilder sqlinsert = new StringBuilder();
                StringBuilder sqlvalues = new StringBuilder();
                sql.Clear();
                sqlinsert.Clear();
                sqlvalues.Clear();
                sqlinsert.Append(" INSERT INTO tbl_CTOS_Report_Loan_Details ");
                sqlinsert.Append(" (CTOS_Result_Id,No,Date_R_R,Capacity,Lender_Type,Limit_Installment_Amount,LGL_STS,Date_Status_Updated,SortOrder");
                sqlvalues.Append(" VALUES(N'" + secure.RC(resultID) + "','" + RowCount.ToString() + "','" +
                    _account.approval_date.ToString() + "','" + secure.RC(_account.capacity.code) + "','" +
                    secure.RC(_account.lender_type.code) + "','" + _account.limit + "','" + _account.legal.status + "','" +
                    _account.legal.date + "'," + SortOrder);
                tempPTPTN = SortOrder;

                if (_account.collaterals.Count() > 0)
                {
                    for (int count2 = 0; count2 < _account.collaterals.Count(); count2++)
                    {
                        var collaterals = _account.collaterals[count2];
                        if (colcount == 0)
                        {
                            sqlinsert.Append(",Col_Type");
                            sqlvalues.Append(",'" + collaterals.code + "'");
                            sqlinsert.Append(")");
                            sqlvalues.Append(")");
                            sql.Append(sqlinsert);
                            sql.Append(sqlvalues);
                            db.Execute(sql.ToString());
                            SortOrder += 1;
                            colcount += 1;
                        }
                        else
                        {
                            sql.Clear();
                            sql.Append(" INSERT INTO tbl_CTOS_Report_Loan_Details ");
                            sql.Append(" (CTOS_Result_Id,Col_Type,SortOrder)");
                            sql.Append(" VALUES(N'" + secure.RC(resultID) + "',N'" + secure.RC(collaterals.code) + "'," + SortOrder + ")");
                            db.Execute(sql.ToString());
                            SortOrder += 1;
                            colcount += 1;
                        }
                    }
                }
                else
                {
                    var SubCollaterals = _account.sub_accounts.First();
                    if (SubCollaterals.collaterals.Count() > 0)
                    {
                        sqlinsert.Append(",Col_Type");
                        sqlvalues.Append(",'" + SubCollaterals.collaterals.First().code + "'");
                    }
                    sqlinsert.Append(")");
                    sqlvalues.Append(")");
                    sql.Append(sqlinsert);
                    sql.Append(sqlvalues);
                    db.Execute(sql.ToString());
                    SortOrder += 1;
                    colcount += 1;
                }

                for (int count2 = 0; count2 < _account.sub_accounts.Count(); count2++)
                {
                    var subaccount = _account.sub_accounts[count2];
                    var firstcr_position = subaccount.cr_positions.FirstOrDefault();

                    //----------------------TO add in Restructure date
                    string DateRR;
                    if (firstcr_position.status.code.Equals("T"))
                        DateRR = firstcr_position.restructured_date;
                    else if (firstcr_position.status.code.Equals("C"))
                        DateRR = firstcr_position.rescheduled_date;
                    else
                        DateRR = "";

                    //--------------------/

                    sql.Clear();
                    sqlinsert.Clear();
                    sqlvalues.Clear();
                    sqlinsert.Append(" INSERT INTO tbl_CTOS_Report_Loan_Details ");
                    sqlinsert.Append("(CTOS_Result_Id,Date_R_R,Sts,Facility,Total_OutStanding_Balance,Date_Balance_Updated,Limit_Installment_Amount,Prin_Repmt_Term,SortOrder");
                    sqlvalues.Append("VALUES(N'" + secure.RC(resultID) + "','" + DateRR + "',N'" + secure.RC(firstcr_position.status.code) + "','" + secure.RC(subaccount.facility.code) + "','" + firstcr_position.balance + "','" + firstcr_position.position_date + "','" + firstcr_position.inst_amount + "','" + secure.RC(subaccount.repay_term.code) + "'," + SortOrder);

                    if (!(string.IsNullOrEmpty(firstcr_position.status.code) || string.IsNullOrWhiteSpace(firstcr_position.status.code)))
                        checkCtosAKPK(secure.RC(firstcr_position.status.code), isMain);

                    Dictionary<string, Dictionary<string, string>> last12Month = new Dictionary<string, Dictionary<string, string>>();

                    for (int i = 0; i < 12; i++)
                    {
                        DateTime date = DateTime.Now.AddMonths(-i);
                        last12Month.Add(date.ToString("yyyyMM"), new Dictionary<string, string>());
                    }

                    for (int count3 = 0; count3 < subaccount.cr_positions.Count(); count3++)
                    {
                        var positions = subaccount.cr_positions[count3];
                        DateTime date = DateTime.ParseExact(positions.position_date.ToString(), "dd-MM-yyyy", null);
                        Dictionary<string, string> val = new Dictionary<string, string>();
                        val.Add(positions.inst_arrears, positions.position_date);
                        last12Month[date.ToString("yyyyMM")] = val;
                    }

                    int mon = 1;
                    foreach (var item in last12Month)
                    {
                        if (item.Value.Count > 0)
                        {
                            var val = item.Value.FirstOrDefault();
                            switch (mon)
                            {
                                case 1:
                                    sqlinsert.Append(",M12_Count,M12_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 2:
                                    sqlinsert.Append(",M11_Count,M11_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 3:
                                    sqlinsert.Append(",M10_Count,M10_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 4:
                                    sqlinsert.Append(",M9_Count,M9_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 5:
                                    sqlinsert.Append(",M8_Count,M8_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 6:
                                    sqlinsert.Append(",M7_Count,M7_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 7:
                                    sqlinsert.Append(",M6_Count,M6_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 8:
                                    sqlinsert.Append(",M5_Count,M5_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 9:
                                    sqlinsert.Append(",M4_Count,M4_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 10:
                                    sqlinsert.Append(",M3_Count,M3_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 11:
                                    sqlinsert.Append(",M2_Count,M2_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 12:
                                    sqlinsert.Append(",M1_Count,M1_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                            }
                        }

                        mon += 1;
                    }

                    sqlinsert.Append(")");
                    sqlvalues.Append(")");
                    sql.Append(sqlinsert);
                    sql.Append(sqlvalues);

                    db.Execute(sql.ToString());

                    if (firstcr_position.status.code.Equals("K"))
                    {
                        db.OpenTable(string.Format(@"
                            SELECT ID FROM tbl_CTOS_Report_Loan_Details 
                            WHERE CTOS_Result_Id = N'{0}' AND SortOrder = '{1}';
                        ", secure.RC(resultID), SortOrder.ToString()));
                        if (db.RecordCount() > 0)
                        {
                            sql.Clear();
                            sql.Append("UPDATE tbl_CTOS_Report_Loan_Details SET Status='AKPK' WHERE ID=N'" + db.Item("ID") + "';");
                            db.Execute(sql.ToString());
                        }
                    }

                    SortOrder += 1;
                    colcount += 1;
                }

                if (!string.IsNullOrEmpty(_account.org1) && _account.org1.Equals("PT"))
                {
                    db.OpenTable("SELECT ID FROM tbl_CTOS_Report_Loan_Details WHERE CTOS_Result_Id=N'" + secure.RC(resultID) + "' AND " +
                        "SortOrder='" + (tempPTPTN + 1).ToString() + "';");
                    if (db.RecordCount() > 0)
                    {
                        sql.Clear();
                        sql.Append("UPDATE tbl_CTOS_Report_Loan_Details SET Status='PTPTN' WHERE ID=N'" + db.Item("ID") + "';");
                        db.Execute(sql.ToString());
                    }
                }
            }
        }
        public void InsertCTOSReport(List<special_attention_acc_detail> accounts, bool isMain)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            int RowCount = 0;
            int SortOrder = 0;
            string resultID;

            if (isMain)
                resultID = this.mainResultID;
            else
                resultID = this.coResultID;

            db.OpenTable("SELECT distinct sortorder FROM tbl_CTOS_Report_Loan_Details WITH(NOLOCK) WHERE sortorder = (SELECT " +
                "MAX(cast(sortorder as int)) FROM tbl_CTOS_Report_Loan_Details WITH(NOLOCK) WHERE CTOS_Result_Id = '" + resultID + "')");
            if (db.RecordCount() > 0)
                SortOrder = ConvertHelper.ConvertToInt(db.Item("sortorder"), 0);
            for (int count1 = 0; count1 < accounts.Count(); count1++)//_account = special_attention_acc
            {
                var _account = accounts[count1];
                RowCount += 1;
                SortOrder += 1;
                int colcount = 0;

                StringBuilder sqlinsert = new StringBuilder();
                StringBuilder sqlvalues = new StringBuilder();
                sql.Clear();
                sqlinsert.Clear();
                sqlvalues.Clear();
                sqlinsert.Append(" INSERT INTO tbl_CTOS_Report_Loan_Details ");
                sqlinsert.Append(" (CTOS_Result_Id,No,Date_R_R,Capacity,Lender_Type,Limit_Installment_Amount,LGL_STS,Date_Status_Updated,SortOrder,Status");
                sqlvalues.Append(" VALUES(N'" + secure.RC(resultID) + "','" + RowCount.ToString() + "','" +
                    _account.approval_date.ToString() + "','" + secure.RC(_account.capacity.code) + "','" +
                    secure.RC(_account.lender_type.code) + "','" + _account.limit + "','" + _account.legal.status + "','" +
                    _account.legal.date + "'," + SortOrder + ", 'SAA'");

                if (_account.collaterals.Count() > 0)
                {
                    for (int count2 = 0; count2 < _account.collaterals.Count(); count2++)
                    {
                        var collaterals = _account.collaterals[count2];
                        if (colcount == 0)
                        {
                            sqlinsert.Append(",Col_Type");
                            sqlvalues.Append(",'" + collaterals.code + "'");
                            sqlinsert.Append(")");
                            sqlvalues.Append(")");
                            sql.Append(sqlinsert);
                            sql.Append(sqlvalues);
                            db.Execute(sql.ToString());
                            SortOrder += 1;
                            colcount += 1;
                        }
                        else
                        {
                            sql.Clear();
                            sql.Append(" INSERT INTO tbl_CTOS_Report_Loan_Details ");
                            sql.Append(" (CTOS_Result_Id, Col_Type, SortOrder, Status)");
                            sql.Append(" VALUES(N'" + secure.RC(resultID) + "',N'" + secure.RC(collaterals.code) + "'," + SortOrder + ", 'SAA')");
                            db.Execute(sql.ToString());
                            SortOrder += 1;
                            colcount += 1;
                        }
                    }
                }
                else
                {
                    var SubCollaterals = _account.sub_accounts.First();
                    if (SubCollaterals.collaterals.Count() > 0)
                    {
                        sqlinsert.Append(",Col_Type");
                        sqlvalues.Append(",'" + SubCollaterals.collaterals.First().code + "'");
                    }
                    sqlinsert.Append(")");
                    sqlvalues.Append(")");
                    sql.Append(sqlinsert);
                    sql.Append(sqlvalues);
                    db.Execute(sql.ToString());
                    SortOrder += 1;
                    colcount += 1;
                }

                for (int count2 = 0; count2 < _account.sub_accounts.Count(); count2++)
                {
                    var subaccount = _account.sub_accounts[count2];
                    var firstcr_position = subaccount.cr_positions.FirstOrDefault();
                    sql.Clear();
                    sqlinsert.Clear();
                    sqlvalues.Clear();
                    sqlinsert.Append(" INSERT INTO tbl_CTOS_Report_Loan_Details ");
                    sqlinsert.Append("(CTOS_Result_Id, Sts, Facility, Total_OutStanding_Balance, Date_Balance_Updated, Limit_Installment_Amount, Prin_Repmt_Term, SortOrder" +
                        ", Status");
                    sqlvalues.Append("VALUES(N'" + secure.RC(resultID) + "',N'" + secure.RC(firstcr_position.status.code) + "','" + secure.RC(subaccount.facility.code)
                        + "','" + firstcr_position.balance + "','" + firstcr_position.position_date + "','" + firstcr_position.inst_amount + "','"
                        + secure.RC(subaccount.repay_term.code) + "'," + SortOrder + ", 'SAA'");

                    if (!(string.IsNullOrEmpty(firstcr_position.status.code) || string.IsNullOrWhiteSpace(firstcr_position.status.code)))
                        checkCtosAKPK(secure.RC(firstcr_position.status.code), isMain);

                    Dictionary<string, Dictionary<string, string>> last12Month = new Dictionary<string, Dictionary<string, string>>();

                    for (int i = 0; i < 12; i++)
                    {
                        DateTime date = DateTime.Now.AddMonths(-i);
                        last12Month.Add(date.ToString("yyyyMM"), new Dictionary<string, string>());
                    }

                    for (int count3 = 0; count3 < subaccount.cr_positions.Count(); count3++)
                    {
                        var positions = subaccount.cr_positions[count3];
                        DateTime date = DateTime.ParseExact(positions.position_date.ToString(), "dd-MM-yyyy", null);
                        Dictionary<string, string> val = new Dictionary<string, string>();
                        val.Add(positions.inst_arrears, positions.position_date);
                        last12Month[date.ToString("yyyyMM")] = val;
                    }

                    int mon = 1;
                    foreach (var item in last12Month)
                    {
                        if (item.Value.Count > 0)
                        {
                            var val = item.Value.FirstOrDefault();
                            switch (mon)
                            {
                                case 1:
                                    sqlinsert.Append(",M12_Count,M12_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 2:
                                    sqlinsert.Append(",M11_Count,M11_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 3:
                                    sqlinsert.Append(",M10_Count,M10_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 4:
                                    sqlinsert.Append(",M9_Count,M9_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 5:
                                    sqlinsert.Append(",M8_Count,M8_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 6:
                                    sqlinsert.Append(",M7_Count,M7_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 7:
                                    sqlinsert.Append(",M6_Count,M6_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 8:
                                    sqlinsert.Append(",M5_Count,M5_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 9:
                                    sqlinsert.Append(",M4_Count,M4_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 10:
                                    sqlinsert.Append(",M3_Count,M3_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 11:
                                    sqlinsert.Append(",M2_Count,M2_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                                case 12:
                                    sqlinsert.Append(",M1_Count,M1_Date");
                                    sqlvalues.Append(",'" + val.Key.ToString() + "','" + val.Value.ToString() + "'");
                                    break;
                            }
                        }

                        mon += 1;
                    }

                    sqlinsert.Append(")");
                    sqlvalues.Append(")");
                    sql.Append(sqlinsert);
                    sql.Append(sqlvalues);

                    db.Execute(sql.ToString());

                    if (firstcr_position.status.code.Equals("K"))
                    {
                        db.OpenTable(string.Format(@"
                            SELECT ID FROM tbl_CTOS_Report_Loan_Details 
                            WHERE CTOS_Result_Id = N'{0}' AND SortOrder = '{1}';
                        ", secure.RC(resultID), SortOrder.ToString()));
                        if (db.RecordCount() > 0)
                        {
                            sql.Clear();
                            sql.Append("UPDATE tbl_CTOS_Report_Loan_Details SET Status='AKPK' WHERE ID=N'" + db.Item("ID") + "';");
                            db.Execute(sql.ToString());
                        }
                    }

                    SortOrder += 1;
                    colcount += 1;
                }
            }
        }
        public void InsertRAMCIReport(List<xmlBanking_infoCcris_banking_detailsItem> accounts, bool isMain, bool isSAA = false)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            int RowCount = 0;
            int SortOrder = 0;
            string resultID;

            if (isMain)
                resultID = this.mainResultID;
            else
                resultID = this.coResultID;

            db.OpenTable("SELECT distinct sortorder FROM tbl_CTOS_Report_Loan_Details WITH(NOLOCK) WHERE sortorder = (SELECT " +
                "MAX(cast(sortorder as int)) FROM tbl_CTOS_Report_Loan_Details WITH(NOLOCK) WHERE CTOS_Result_Id = '" + resultID + "')");
            if (db.RecordCount() > 0)
                SortOrder = ConvertHelper.ConvertToInt(db.Item("sortorder"), 0);
            for (int count1 = 0; count1 < accounts.Count(); count1++)
            {
                var _account = accounts[count1];
                RowCount += 1;
                SortOrder += 1;
                int temp = 0;
                int colcount = 0;

                StringBuilder sqlinsert = new StringBuilder();
                StringBuilder sqlvalues = new StringBuilder();
                sql.Clear();
                sqlinsert.Clear();
                sqlvalues.Clear();
                sqlinsert.Append(" INSERT INTO tbl_CTOS_Report_Loan_Details ");
                sqlinsert.Append(" (CTOS_Result_Id, No, Date_R_R, Capacity, Lender_Type, Limit_Installment_Amount, SortOrder, Col_Type, LGL_STS" +
                    ", Date_Status_Updated " + (isSAA ? ", Status" : "") + ")");
                sqlvalues.Append(" VALUES(N'" + secure.RC(resultID) + "','" + RowCount.ToString() + "','" + _account.master.item.date.ToString() + "','" +
                    secure.RC(_account.master.item.capacity.ToString()) + "','" + secure.RC(_account.master.item.lender_type.ToString()) + "','" +
                    _account.master.item.limit.ToString() + "'," + SortOrder + ",N'" + secure.RC(_account.master.item.collateral_type_code.ToString()) + "',N'" +
                    secure.RC(_account.master.item.legal_status.ToString()) + "', N'" + secure.RC(_account.master.item.legal_status_date.ToString()) + "'" +
                    (isSAA ? ", 'SAA'" : "") + ")");
                sql.Append(sqlinsert);
                sql.Append(sqlvalues);
                db.Execute(sql.ToString());
                temp = SortOrder;
                SortOrder += 1;
                colcount += 1;

                for (int count2 = 0; count2 < _account.sub_account.Count(); count2++)
                {
                    var subaccount = _account.sub_account[count2];
                    var firstcr_position = subaccount.item;
                    sql.Clear();
                    sqlinsert.Clear();
                    sqlvalues.Clear();
                    sqlinsert.Append(" INSERT INTO tbl_CTOS_Report_Loan_Details ");
                    sqlinsert.Append("(CTOS_Result_Id, Sts, Facility, Total_OutStanding_Balance, Date_Balance_Updated, Limit_Installment_Amount, Prin_Repmt_Term" +
                        ", SortOrder" + (isSAA ? ", Status" : ""));
                    sqlvalues.Append("VALUES(N'" + secure.RC(resultID) + "',N'" + secure.RC(firstcr_position.status) + "','" + secure.RC(firstcr_position.facility) +
                        "','" + firstcr_position.total_outstanding_balance + "','" + firstcr_position.balance_updated_date + "','" +
                        firstcr_position.installment_amount + "','" + secure.RC(firstcr_position.principle_repayment_term) + "','" + SortOrder + "'" +
                        (isSAA ? ",'SAA'" : ""));

                    if (!string.IsNullOrEmpty(subaccount.item.status) && !string.IsNullOrWhiteSpace(subaccount.item.status))
                        checkRamciAKPK(secure.RC(subaccount.item.status), isMain);

                    int counter = 12;
                    for (int count3 = 0; count3 < firstcr_position.credit_position.Count(); count3++)
                    {
                        var item = firstcr_position.credit_position[count3];
                        sqlinsert.Append(",M" + counter.ToString() + "_Count, M" + counter.ToString() + "_Date");
                        if (string.IsNullOrWhiteSpace(item))
                            sqlvalues.Append(",null,null");
                        else
                            sqlvalues.Append(",'" + item.ToString() + "',null");

                        counter--;
                        if (counter < 1)
                            break;
                    }

                    sqlinsert.Append(")");
                    sqlvalues.Append(")");
                    sql.Append(sqlinsert);
                    sql.Append(sqlvalues);

                    db.Execute(sql.ToString());

                    if (firstcr_position.status.Equals("K"))
                    {
                        db.OpenTable(string.Format(@"
                            SELECT ID FROM tbl_CTOS_Report_Loan_Details 
                            WHERE CTOS_Result_Id = N'{0}' AND SortOrder = '{1}';
                        ", secure.RC(resultID), SortOrder.ToString()));
                        if (db.RecordCount() > 0)
                        {
                            sql.Clear();
                            sql.Append("UPDATE tbl_CTOS_Report_Loan_Details SET Status='AKPK' WHERE ID=N'" + db.Item("ID") + "';");
                            db.Execute(sql.ToString());
                        }
                    }

                    SortOrder += 1;
                    colcount += 1;
                }

                if (_account.sub_account != null
                    && _account.sub_account.FirstOrDefault() != null
                    && !string.IsNullOrEmpty(_account.sub_account.FirstOrDefault().item.facility)
                    && _account.sub_account.FirstOrDefault().item.facility.Equals("NHEDFNCE"))
                {
                    db.OpenTable("SELECT ID FROM tbl_CTOS_Report_Loan_Details WHERE CTOS_Result_Id=N'" + secure.RC(resultID) + "' AND " +
                        "SortOrder='" + (temp + 1).ToString() + "';");
                    if (db.RecordCount() > 0)
                    {
                        sql.Clear();
                        sql.Append("UPDATE tbl_CTOS_Report_Loan_Details SET Status='PTPTN' WHERE ID=N'" + db.Item("ID") + "';");
                        db.Execute(sql.ToString());
                    }
                }
            }
        }
        public bool SaveReport(string requestID, List<string> bankList, bool isFinal = false, bool notification = true) //Classic bank default
        {
            StringBuilder sql = new StringBuilder();
            wwdb db = new wwdb();
            this.requestID = requestID;

            sql.Clear();
            sql.Append("UPDATE tbl_Request SET StatusID ='" + ApplicationStatus.InProgress + "' ");
            db.OpenTable("SELECT ApplicationResult FROM tbl_Request WHERE RequestID=N'" + requestID + "'");
            if (string.IsNullOrEmpty(db.Item("ApplicationResult")))
                sql.Append(", ApplicationResult='M' ");
            sql.Append("WHERE RequestID = '" + requestID + "';");
            db.Execute(sql.ToString());//save
            sql.Clear();
            sql.Append("EXEC SP_ToRealData @RequestID='" + requestID + "';");
            for (int count1 = 0; count1 < bankList.Count(); count1++)
            {
                string bankID = bankList[count1];
                sql.Append("EXEC SP_AddRealDetails @RequestID='" + requestID + "' , @BankID='" + bankID + "';");
            }

            //set request advise final
            sql.Append("UPDATE tbl_ApplicantCreditAdvisor SET IsFinal='" + (isFinal ? "1" : "0") + "' WHERE RequestID=N'" + requestID + "'; ");

            sql.AppendFormat(@"update tbl_Request set ProjectID = ( 
	                    select b.ID AS ProjectID
	                    from tbl_TLoanLegalAction a with (nolock) left join 
	                    tbl_ProjectSettings b on a.ProjectID = b.ProjectID where a.RequestID = '{0}') 
	                    where tbl_Request.RequestID = '{0}'; ", requestID);

            db.Execute(sql.ToString());//send

            if (notification)
            {
                //send sms
                db.OpenTable("SELECT (SELECT TOP 1 Mobile FROM " +
                    "tbl_MemberInfo WITH(NOLOCK) WHERE MemberID = tr.MemberID) AS mobile FROM " +
                    "tbl_request tr WITH(NOLOCK) WHERE requestid = '" + requestID + "'");
                //if mobile phone found
                if (db.RecordCount() > 0)
                {
                    string mobileNo = db.Item("Mobile");
                    bool validFormat = false;
                    if (mobileNo.StartsWith("0") && !(mobileNo.StartsWith("6")))
                    {
                        mobileNo = "6";
                        mobileNo += db.Item("Mobile");
                        validFormat = true;
                    }
                    else if (mobileNo.StartsWith("601"))
                        validFormat = true;

                    if (validFormat)//only send sms when mobileNo is in valid format to save cost
                    {
                        MessagingService SMS = new MessagingService();
                        SMS.sendReportNotification(db.Item("Mobile"), requestID);
                    }
                }
            }

            if (!db.HasError)
                return true;
            else
            {
                LogUtil.logError(db.ErrorMessage, sql.ToString());
                return false;
            }
        }
        #endregion

        #region Data Passing
        public string InsertRequestRecord(string userID, string branchID, string consentID, string MainName, string MainIC, string MainIncome,
            string CoName = "", string CoIC = "", string CoIncome = "", string projectID = "", string projectName = "", string purchasePrice = "0",
            string loanAmount = "0", string reportType = "", string remark = "", string status = "", string perRequestPrice = "0.00",
            string perSuccessfulPrice = "0.00")
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            //string newPath = "/Upload/RequestDocument/";
            this.userID = userID;
            SqlParameter[] sqlParams = {
                    new SqlParameter("@UserID",  userID),
                     new SqlParameter("@Name", MainName),
                      new SqlParameter("@IC", MainIC),
                       new SqlParameter("@NetIncome", MainIncome.Replace(",","")),
                      new SqlParameter("@CoApplicantName", string.IsNullOrEmpty(CoName) ? (object) DBNull.Value : CoName.Trim()),
                      new SqlParameter("@CoApplicantIC", string.IsNullOrEmpty(CoIC) ? (object) DBNull.Value : CoIC.Trim()),
                       new SqlParameter("@CoApplicantNetIncome", string.IsNullOrEmpty(CoIncome.Trim()) ? (object) DBNull.Value : CoIncome.Trim().Replace(",","")),
                       new SqlParameter("@ProjectId", string.IsNullOrEmpty(projectID) ? (object) DBNull.Value : projectID),
                       new SqlParameter("@ProjectName", string.IsNullOrEmpty(projectName.Trim()) ? (object) DBNull.Value : projectName.Trim()),
                        new SqlParameter("@Purchase", purchasePrice == string.Empty?"0" : purchasePrice.Replace(",","")),
                         new SqlParameter("@LoanAmount", loanAmount == string.Empty?"0" : loanAmount.Replace(",","") ),
                          new SqlParameter("@ReportType", reportType) ,
                           new SqlParameter("@BranchID", branchID),
                           new SqlParameter("@PerRequestPrice",perRequestPrice),
                           new SqlParameter("@PerSuccessfulPrice",perSuccessfulPrice),
                           new SqlParameter("@ConsentFormId", consentID)
                                          };

            string requestid = db.executeScalarSP("SP_AddRequest", sqlParams).ToString();
            this.requestID = requestid;
            this.mainNetIncome = MainIncome;
            this.coNetIncome = CoIncome;
            //newPath += requestid + "/";
            //if (!Directory.Exists(Server.MapPath("~" + newPath)))
            //    Directory.CreateDirectory(Server.MapPath("~" + newPath));

            //foreach (string imgpath in uploadImages)
            //{
            //    FileInfo FromFile = new FileInfo(imgpath);
            //    string toFilePath = newPath + FromFile.Name;
            //    FromFile.MoveTo(Server.MapPath("~" + toFilePath));
            //    string sql = "insert into tbl_RequestDocs(RequestID,DocumentType , DocsURL)values(" + requestid + ",'" + DocumentType.NormalFiles + "',N'" + toFilePath + "') ";
            //    db.Execute(sql.ToString());

            //}

            if (!string.IsNullOrEmpty(remark))
            {
                sql.Clear();
                sql.Append("UPDATE tbl_Request set Remark='" + remark + "' WHERE RequestID='" + requestid + "'; ");
                db.Execute(sql.ToString());
            }
            //if (!string.IsNullOrEmpty(status))
            //{
            //    db.OpenTable("SELECT TOP (1) NoteID FROM tbl_Note with (nolock) order by cast(NoteID as int) desc");
            //    sql.Append("INSERT INTO tbl_Note (NoteID, RequestID, NoteContent, IsDeleted, CreatedBy, CreatedAt, UpdatedBy, UpdatedAt) ");
            //    sql.Append("VALUES ('" + (ConvertHelper.ConvertToInt(db.Item("NoteID"), 0) + 1) + "', '" + requestid + "', '" +
            //        status + "', '0', '" + userID + "', getdate(), '" + userID + "', getdate()); ");
            //}

            int UserFreeCount = 0;
            sql = new StringBuilder();
            sql.Clear();
            sql.Append("SELECT FreeRequestCount FROM tbl_UserCreditBalance WHERE MemberID ='" + userID + "'");
            db.OpenTable(sql.ToString());
            if (db.RecordCount() > 0)
            {
                UserFreeCount = ConvertHelper.ConvertToInt(db.Item("FreeRequestCount").ToString(), 0);
            }

            #region Billing
            SqlParameter[] billingSPParams = {
                     new SqlParameter("@RequestID", requestid),
                      new SqlParameter("@RequestType", "Request"),
                       new SqlParameter("@CreatedBy", userID)
            };

            db.executeScalarSP("SP_AddBillingTransaction", billingSPParams);

            //sql.Clear();
            //sql.Append("SELECT ISNULL(SUM(ISNULL(FreeRequestCount,0)),0) as RequestCount FROM tbl_UserTransactions WHERE RequestType = 'Request' AND MemberID ='" + userID + "'");
            //db.OpenTable(sql.ToString());
            //if (db.RecordCount() > 0)
            //{
            //    int FreeCount = UserFreeCount;
            //    int RequestCount = ConvertHelper.ConvertToInt(db.Item("RequestCount").ToString(), 0);
            //    Decimal PerRequestPrice = ConvertHelper.ConvertToDecimal(perRequestPrice, 0);
            //    if (PerRequestPrice <= 0 || FreeCount <= 0)
            //    {
            //        Decimal RequestPrice = Convert.ToDecimal(perRequestPrice);
            //        if (CoName.Trim() != string.Empty)
            //        {
            //            RequestPrice = RequestPrice * 2;
            //        }
            //        string sql3 = "INSERT INTO tbl_UserTransactions(MemberId,Amount,Status,RequestType,RequestId,CreatedAt,IsFreeRequest) Values('" + userID + "',-" + RequestPrice.ToString() + ",1,'Request'," + requestid + ",GETDATE(),0);" +
            //                      "IF (SELECT 1 FROM tbl_UserCreditBalance WHERE MemberID ='" + userID + "') > 0 " +
            //                      "UPDATE tbl_UserCreditBalance SET CreditUsed = CreditUsed - " + RequestPrice.ToString() + " WHERE MemberId ='" + userID + "'; " +
            //                      "ELSE " +
            //                      "INSERT INTO tbl_UserCreditBalance(MemberId,CreditUsed,CreatedAt) Values('" + userID + "',-" + RequestPrice.ToString() + ",GETDATE());";
            //        db.Execute(sql3.ToString());
            //    }
            //    else
            //    {

            //        int FreeRequestCount = 1;
            //        if (CoName.Trim() != string.Empty)
            //        {
            //            if (FreeCount >= 2)
            //            {
            //                FreeRequestCount += 1;
            //                PerRequestPrice = PerRequestPrice * 2;

            //                string sql4 = "UPDATE tbl_Request set FreeRequestCount=" + FreeRequestCount + " WHERE RequestID='" + requestid + "';" +
            //                     " INSERT INTO tbl_UserTransactions(MemberId,Amount,Status,RequestType,RequestId,CreatedAt,IsFreeRequest,FreeRequestCount,CreatedBy)" +
            //                     " Values('" + userID + "',-" + PerRequestPrice + ",1,'Request'," + requestid + ",GETDATE(), 1,(" + FreeRequestCount * -1 + "),'" + userID + "');" +
            //                     " IF (SELECT 1 FROM tbl_UserCreditBalance WHERE MemberID ='" + userID + "') > 0 " +
            //                     "UPDATE tbl_UserCreditBalance SET FreeRequestCount = ISNULL(FreeRequestCount,0) + (" + FreeRequestCount * -1 + ") WHERE MemberId ='" + userID + "'; " +
            //                      "ELSE " +
            //                     "INSERT INTO tbl_UserCreditBalance(MemberId,CreditUsed,CreatedAt,FreeRequestCount) Values('" + userID + "',0,GETDATE()," + FreeRequestCount * -1 + ");";
            //                db.Execute(sql4.ToString());
            //            }
            //            else
            //            {
            //                string sql4 = "UPDATE tbl_Request set FreeRequestCount=" + FreeRequestCount + " WHERE RequestID='" + requestid + "';" +
            //                     " INSERT INTO tbl_UserTransactions(MemberId,Amount,Status,RequestType,RequestId,CreatedAt,IsFreeRequest,FreeRequestCount,CreatedBy,Remark)" +
            //                     " Values('" + userID + "',-" + PerRequestPrice + ",1,'Request'," + requestid + ",GETDATE(), 1,(" + FreeRequestCount * -1 + "),'" + userID + "','TWO APPLICANTS (1 FREE REQUEST)');" +
            //                     " INSERT INTO tbl_UserTransactions(MemberId,Amount,Status,RequestType,RequestId,CreatedAt,IsFreeRequest,FreeRequestCount,CreatedBy,Remark)" +
            //                     " Values('" + userID + "',-" + PerRequestPrice + ",1,'Request'," + requestid + ",GETDATE(), 0,0,'" + userID + "','TWO APPLICANTS (NON FREE REQUEST)');" +
            //                     " IF (SELECT 1 FROM tbl_UserCreditBalance WHERE MemberID ='" + userID + "') > 0 " +
            //                     "UPDATE tbl_UserCreditBalance SET CreditUsed = CreditUsed + (-" + PerRequestPrice + "),FreeRequestCount = ISNULL(FreeRequestCount,0) + " + FreeRequestCount * -1 + " WHERE MemberId ='" + userID + "'; " +
            //                      "ELSE " +
            //                     "INSERT INTO tbl_UserCreditBalance(MemberId,CreditUsed,CreatedAt,FreeRequestCount) Values('" + userID + "',0,GETDATE()," + FreeRequestCount * -1 + ");";
            //                db.Execute(sql4.ToString());
            //            }
            //        }
            //        else
            //        {

            //            string sql4 = "UPDATE tbl_Request set FreeRequestCount=" + FreeRequestCount + " WHERE RequestID='" + requestid + "';" +
            //                 " INSERT INTO tbl_UserTransactions(MemberId,Amount,Status,RequestType,RequestId,CreatedAt,IsFreeRequest,FreeRequestCount,CreatedBy)" +
            //                 " Values('" + userID + "',-" + PerRequestPrice + ",1,'Request'," + requestid + ",GETDATE(), 1,(" + FreeRequestCount * -1 + "),'" + userID + "');" +
            //                 " IF (SELECT 1 FROM tbl_UserCreditBalance WHERE MemberID ='" + userID + "') > 0 " +
            //                 "UPDATE tbl_UserCreditBalance SET FreeRequestCount = ISNULL(FreeRequestCount,0) + " + FreeRequestCount * -1 + " WHERE MemberId ='" + userID + "'; " +
            //                  "ELSE " +
            //                 "INSERT INTO tbl_UserCreditBalance(MemberId,CreditUsed,CreatedAt,FreeRequestCount) Values('" + userID + "',0,GETDATE()," + FreeRequestCount * -1 + ");";
            //            db.Execute(sql4.ToString());
            //        }

            //    }
            //}
            #endregion

            //end of insert
            return requestid;
        }
        public void InsertTempData(string name, string age, string IC, int houseLoanNumber, bool isMain, string requestID, decimal interest, 
            bool isMainOnly = true, string grossIncome = "", string otherIncome = "", string area = "", string loanAmount = "", 
            string purchasePrice = "", string contactNo = "") //per applicant 
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            if (!string.IsNullOrEmpty(requestID))
                this.requestID = requestID;

            this.interest = interest.ToString();

            try
            {
                string CTOSitem = string.Empty;
                string specialAttention = string.Empty;
                string restructuring = string.Empty;
                string dishonourCheque = string.Empty;
                string CCRISAttention = string.Empty;
                //need revising

                //Delete Record
                if (isMain)
                {
                    sql.Clear();
                    sql.Append(" DELETE FROM tbl_TApplicants WHERE RequestID = N'" + secure.RC(requestID) + "';");
                    sql.Append(" DELETE FROM tbl_TLoanLegalAction WHERE RequestID = N'" + secure.RC(requestID) + "';");
                    sql.Append(" DELETE FROM tbl_TLoanInfo WHERE RequestID = N'" + secure.RC(requestID) + "';");
                    sql.Append(" DELETE FROM tbl_TLoanDetail WHERE RequestID = N'" + secure.RC(requestID) + "';");
                    sql.Append(" DELETE FROM tbl_RequestDocs WHERE RequestID = N'" + secure.RC(requestID) + "' AND DocumentType = N'" + DocumentType.CCRIS + "';");
                    db.Execute(sql.ToString());
                    LogUtil.logAction(sql.ToString(), "DELETE Temporary Information for RequestID :" + requestID);
                }

                //Insert Applicant into Database
                sql.Clear();
                sql.AppendFormat(@" 
                    INSERT INTO tbl_TApplicants
                    (RequestID, Name, Age, AreaID, MyCard, NetIncome, GrossIncome, OtherIncome, ApplicantType, CreateBy, CreateAt, UpdateBy, UpdateAt,
                    HouseLoanNumber, ContactNumber)
                    VALUES (N'{0}', N'{1}', N'{2}', N'{3}', N'{4}', '{5}', '{6}', '{7}', N'{8}', N'{9}', GETDATE(), N'{10}', GETDATE(),
                    N'{11}', '{12}')
                ", secure.RC(requestID), secure.RC(name), age, area, secure.RC(IC)
                , string.IsNullOrEmpty(this.mainNetIncome) ? "0" : this.mainNetIncome
                , string.IsNullOrEmpty(grossIncome) ? "0" : grossIncome, string.IsNullOrEmpty(otherIncome) ? "0" : otherIncome
                , isMain ? "MA" : "CA", secure.RC(userID), secure.RC(userID), houseLoanNumber > 0 ? houseLoanNumber.ToString() : "0"
                , secure.RC(contactNo));
                db.Execute(sql.ToString());
                LogUtil.logAction(sql.ToString(), "Insert " + (isMain ? "Main" : "Co") + "-Applicant Into tbl_TApplicants - RequestID :" + requestID);

                if (string.IsNullOrEmpty(this.loanAmount) || string.IsNullOrEmpty(this.purchasePrice) || string.IsNullOrEmpty(this.projectID))
                {
                    sql.Clear();
                    sql.Append(" SELECT ra.NetIncome, ra.PurchasePrice, ra.LoanAmount, tps.ProjectID");
                    sql.Append(" FROM tbl_Request r WITH(NOLOCK)");
                    sql.Append(" LEFT JOIN tbl_RequestApplicant ra WITH(NOLOCK) ON ra.RequestID = r.RequestID");
                    sql.Append(" LEFT JOIN dbo.tbl_ProjectSettings tps ON r.ProjectID = tps.ID");
                    sql.Append(" WHERE r.RequestID = N'" + requestID + "'");
                    sql.Append(" ORDER BY ra.ID");
                    db.OpenTable(sql.ToString());

                    if (db.RecordCount() > 0)
                    {
                        int count = 1;
                        while (!db.Eof())
                        {
                            if (string.IsNullOrEmpty(this.purchasePrice))
                                if (string.IsNullOrEmpty(purchasePrice) || ConvertHelper.ConvertToDecimal(purchasePrice, 0) <= 0)
                                    this.purchasePrice = db.Item("PurchasePrice");
                                else
                                    this.purchasePrice = purchasePrice;
                            if (string.IsNullOrEmpty(this.loanAmount))
                                if (string.IsNullOrEmpty(loanAmount))// || ConvertHelper.ConvertToDecimal(loanAmount, 0) <= 0)
                                    this.loanAmount = db.Item("LoanAmount");
                                else
                                    this.loanAmount = loanAmount;
                            if (count == 1)
                                if (string.IsNullOrEmpty(this.mainNetIncome) || this.mainNetIncome.Equals("0"))
                                    this.mainNetIncome = db.Item("NetIncome");
                            if (count > 1)
                                this.coNetIncome = db.Item("NetIncome");
                            if (string.IsNullOrEmpty(this.projectID))
                                this.projectID = db.Item("ProjectID");

                            db.MoveNext();
                            count++;
                        }
                    }
                }
                if (!string.IsNullOrEmpty(this.purchasePrice) && ConvertHelper.ConvertToDecimal(this.loanAmount, 0) <= 0)
                    this.loanAmount = Math.Round(ConvertHelper.ConvertToDecimal(this.purchasePrice, 0) * PassBankMargin("12", projectID, requestID) / 100).ToString();
                this.loanAmount = Math.Round(ConvertHelper.ConvertToDecimal(this.loanAmount.Replace(",", ""), 0)).ToString();

                //Update new calculated loanAmount
                db.Execute("UPDATE tbl_RequestApplicant SET LoanAmount = '" + this.loanAmount + "' WHERE RequestID='" + requestID + "'");
                LogUtil.logAction(sql.ToString(), "Update new loanAmount - RequestID :" + requestID);

                //Insert Loan / Bank Legal Action
                if (isMain)
                {
                    sql.Clear();
                    sql.Append(" INSERT INTO tbl_TLoanLegalAction ");
                    sql.Append(" (RequestID, Interest, PurchasePrice, LoanAmount, ProjectID, CTOS, SAttention, Restructuring, DishonourCheque, CCRIS, IsDeleted, CreateAt, CreateBy, UpdateAt, UpdateBy)");
                    sql.Append(" VALUES(N'" + secure.RC(requestID) + "', '" + this.interest +
                        "', '" + ((string.IsNullOrEmpty(this.purchasePrice) || string.IsNullOrWhiteSpace(this.purchasePrice)) ? "0" : this.purchasePrice) +
                        "', '" + ((string.IsNullOrEmpty(this.loanAmount) || string.IsNullOrWhiteSpace(this.loanAmount)) ? "0" : this.loanAmount) +
                        "', '" + ((string.IsNullOrEmpty(this.projectID) || string.IsNullOrWhiteSpace(this.projectID)) ? "Others" : projectID) + "',");
                    sql.Append(" '" + (string.IsNullOrEmpty(secure.RC(CTOSitem)) ? "0" : secure.RC(CTOSitem)) +
                        "', '" + (string.IsNullOrEmpty(secure.RC(specialAttention)) ? "0" : secure.RC(specialAttention)) +
                        "', '" + (string.IsNullOrEmpty(secure.RC(restructuring)) ? "0" : secure.RC(restructuring)) + "', ");
                    sql.Append(" '" + (string.IsNullOrEmpty(secure.RC(dishonourCheque)) ? "0" : secure.RC(dishonourCheque)) +
                        "', '" + (string.IsNullOrEmpty(secure.RC(CCRISAttention)) ? "0" : secure.RC(CCRISAttention)) +
                        "', 'False', GETDATE(),'" + secure.RC(userID) + "', GETDATE(),'" + secure.RC(userID) + "')");
                    db.Execute(sql.ToString());
                    LogUtil.logAction(sql.ToString(), "Insert Bank Legal Action Into tbl_TLoanLegalAction - RequestID :" + requestID);
                }
                //Insert CCRIS File in tbl_RequestDocs
            }
            catch (Exception ex) { LogUtil.logError(ex.ToString(), sql.ToString()); }
            finally { db.Close(); }
        }
        public void InsertLoanData(string mainAge, string coAge, bool isMainOnly) //once 
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            //Read all the bank id
            string BankID = "";
            sql.Clear();
            sql.Append(" SELECT BankID ");
            sql.Append(" FROM tbl_BankName WITH(NOLOCK) ");
            sql.Append(" WHERE Status='A' AND IsDeleted = 'False' ");
            db.OpenTable(sql.ToString());
            if (db.RecordCount() > 0)
                while (!db.Eof())
                {
                    BankID = db.Item("BankID");

                    // Main Applicant Gridview 
                    InsertDataIntoLoanDetail(mainHL, requestID, BankID, 1, row5, mainAge, true);
                    InsertDataIntoLoanDetail(mainCL, requestID, BankID, 2, row5, mainAge, true);
                    InsertDataIntoLoanDetail(mainPL, requestID, BankID, 3, row5, mainAge, true);
                    InsertDataIntoLoanDetail(mainOD, requestID, BankID, 4, row5, mainAge, true);
                    InsertDataIntoLoanDetail(mainXP, requestID, BankID, 5, row10, mainAge, true);
                    InsertDataIntoLoanDetail(mainHP, requestID, BankID, 6, row5, mainAge, true);
                    InsertDataIntoLoanDetail(mainCC, requestID, BankID, 7, row10, mainAge, true);

                    if (!isMainOnly)
                    {
                        InsertDataIntoLoanDetail(coHL, requestID, BankID, 1, row5, mainAge, false, coAge);
                        InsertDataIntoLoanDetail(coCL, requestID, BankID, 2, row5, mainAge, false, coAge);
                        InsertDataIntoLoanDetail(coPL, requestID, BankID, 3, row5, mainAge, false, coAge);
                        InsertDataIntoLoanDetail(coOD, requestID, BankID, 4, row5, mainAge, false, coAge);
                        InsertDataIntoLoanDetail(coXP, requestID, BankID, 5, row10, mainAge, false, coAge);
                        InsertDataIntoLoanDetail(coHP, requestID, BankID, 6, row5, mainAge, false, coAge);
                        InsertDataIntoLoanDetail(coCC, requestID, BankID, 7, row10, mainAge, false, coAge);
                    }

                    if (isMainOnly)
                        InsertDataIntoLoanInfo(requestID, BankID, isMainOnly, mainAge);
                    else
                        InsertDataIntoLoanInfo(requestID, BankID, isMainOnly, mainAge, coAge);

                    if (!db.Eof())
                        db.MoveNext();
                }
        }
        public bool PassDataIntoArray(bool isMain, string mainAge, string coAge, section_ccris section_ccris = null) //use controller ccris or pass in a ccris 
        {
            if (this.section_ccris == null)
            {
                if (section_ccris == null)
                    return false;
                else
                    this.section_ccris = section_ccris;
            }
            else
            {
                if (section_ccris == null)
                    section_ccris = this.section_ccris;
                //else
                //    this.section_ccris = section_ccris;
            }

            Array.Clear(isMain ? mainHL : coHL, 0, isMain ? mainHL.Length : coHL.Length);
            Array.Clear(isMain ? mainCL : coCL, 0, isMain ? mainCL.Length : coCL.Length);
            Array.Clear(isMain ? mainHP : coHP, 0, isMain ? mainHP.Length : coHP.Length);
            Array.Clear(isMain ? mainCC : coCC, 0, isMain ? mainCC.Length : coCC.Length);
            Array.Clear(isMain ? mainOD : coOD, 0, isMain ? mainOD.Length : coOD.Length);
            Array.Clear(isMain ? mainPL : coPL, 0, isMain ? mainPL.Length : coPL.Length);
            Array.Clear(isMain ? mainXP : coXP, 0, isMain ? mainXP.Length : coXP.Length);

            if (section_ccris != null && section_ccris.accounts != null && section_ccris.accounts.account != null)
            {
                wwdb db = new wwdb();
                StringBuilder sql = new StringBuilder();
                string loanid = string.Empty;
                string original = "";
                string outstanding = "";
                string repayment = "";

                for (int count1 = 0; count1 < section_ccris.accounts.account.Count(); count1++)
                {
                    var _account = section_ccris.accounts.account[count1];
                    string capacityCode = _account.capacity.code.ToString();//Own/Joint/Partner
                    string loanType = _account.sub_accounts[0].facility.code.ToString();
                    sql.Clear();
                    sql.Append("SELECT LoanID FROM tbl_LoanTypesXML WITH(NOLOCK) WHERE LoanTypeCode = '" + loanType + "';");
                    db.OpenTable(sql.ToString());
                    if (db.RecordCount() > 0)
                    {
                        loanid = db.Item("LoanID");
                        switch (loanid)
                        {
                            case "1"://Housing Loan
                                for (int i = 0; i < _account.sub_accounts.Count(); i++)
                                {
                                    original = string.IsNullOrEmpty(_account.limit.ToString()) ? "" : _account.limit.ToString();
                                    outstanding = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].balance.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].balance.ToString();
                                    repayment = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].inst_amount.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].inst_amount.ToString();
                                    if ((repayment.Equals("0") || string.IsNullOrEmpty(repayment)) && !string.IsNullOrEmpty(original))
                                        repayment = RepaymentEffective(12, ConvertHelper.ConvertToDecimal(original, 0), 1, mainAge, coAge).ToString(); //temp
                                    if ((capacityCode.Equals("Joint") || capacityCode.Equals("Partner")) && !string.IsNullOrWhiteSpace(repayment) && double.TryParse(repayment, out double parseRepayment))
                                        repayment = (parseRepayment / 2).ToString();

                                    for (int j = 0; j < row5; j++)
                                        if (!string.IsNullOrEmpty(original) && !string.IsNullOrEmpty(outstanding) && !string.IsNullOrEmpty(repayment))
                                            if (isMain)
                                            {
                                                if (string.IsNullOrEmpty(mainHL[j, 0]) && string.IsNullOrEmpty(mainHL[j, 1]) && string.IsNullOrEmpty(mainHL[j, 2]))
                                                {
                                                    mainHL[j, 0] = original;
                                                    mainHL[j, 1] = outstanding;
                                                    //DateTime approvalDate = DateTime.ParseExact(_account.approval_date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                                                    //if (DateTime.Now.Subtract(approvalDate).Days <= (3 * 365))
                                                    //{
                                                    //    mainHLrecalc[j, 2] = repayment;
                                                    //    mainHLcapCode[j, 2] = capacityCode;
                                                    //    repayment = "0";
                                                    //}
                                                    mainHL[j, 2] = repayment;
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                if (string.IsNullOrEmpty(coHL[j, 0]) && string.IsNullOrEmpty(coHL[j, 1]) && string.IsNullOrEmpty(coHL[j, 2]))
                                                {
                                                    coHL[j, 0] = original;
                                                    coHL[j, 1] = outstanding;
                                                    //DateTime approvalDate = DateTime.ParseExact(_account.approval_date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                                                    //if (DateTime.Now.Subtract(approvalDate).Days <= (3 * 365))
                                                    //{
                                                    //    coHLrecalc[j, 2] = repayment;
                                                    //    coHLcapCode[j, 2] = capacityCode;
                                                    //    repayment = "0";
                                                    //}
                                                    coHL[j, 2] = repayment;
                                                    break;
                                                }
                                            }
                                }
                                break;
                            case "2"://Commercial Loan
                                for (int i = 0; i < _account.sub_accounts.Count(); i++)
                                {
                                    original = string.IsNullOrEmpty(_account.limit.ToString()) ? "" : _account.limit.ToString();
                                    outstanding = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].balance.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].balance.ToString();
                                    repayment = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].inst_amount.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].inst_amount.ToString();
                                    if ((repayment.Equals("0") || string.IsNullOrEmpty(repayment)) && !string.IsNullOrEmpty(outstanding))
                                        repayment = RepaymentEffective(12, ConvertHelper.ConvertToDecimal(outstanding, 0), 2, mainAge, coAge).ToString();
                                    if ((capacityCode.Equals("Joint") || capacityCode.Equals("Partner")) && !string.IsNullOrWhiteSpace(repayment) && double.TryParse(repayment, out double parseRepayment))
                                        repayment = (parseRepayment / 2).ToString();

                                    for (int j = 0; j < row5; j++)
                                        if (!string.IsNullOrEmpty(original) && !string.IsNullOrEmpty(outstanding) && !string.IsNullOrEmpty(repayment))
                                            if (isMain)
                                            {
                                                if (string.IsNullOrEmpty(mainCL[j, 0]) && string.IsNullOrEmpty(mainCL[j, 1]) && string.IsNullOrEmpty(mainCL[j, 2]))
                                                {
                                                    mainCL[j, 0] = original;
                                                    mainCL[j, 1] = outstanding;
                                                    mainCL[j, 2] = repayment;
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                if (string.IsNullOrEmpty(coCL[j, 0]) && string.IsNullOrEmpty(coCL[j, 1]) && string.IsNullOrEmpty(coCL[j, 2]))
                                                {
                                                    coCL[j, 0] = original;
                                                    coCL[j, 1] = outstanding;
                                                    coCL[j, 2] = repayment;
                                                    break;
                                                }
                                            }
                                }
                                break;
                            case "3"://PersonalLoan
                                for (int i = 0; i < _account.sub_accounts.Count(); i++)
                                {
                                    original = string.IsNullOrEmpty(_account.limit.ToString()) ? "" : _account.limit.ToString();
                                    outstanding = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].balance.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].balance.ToString();
                                    repayment = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].inst_amount.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].inst_amount.ToString();
                                    if ((repayment.Equals("0") || string.IsNullOrEmpty(repayment)) && !string.IsNullOrEmpty(outstanding))
                                        repayment = RepaymentFlat(12, ConvertHelper.ConvertToDecimal(outstanding, 0), 3).ToString();
                                    if ((capacityCode.Equals("Joint") || capacityCode.Equals("Partner")) && !string.IsNullOrWhiteSpace(repayment) && double.TryParse(repayment, out double parseRepayment))
                                        repayment = (parseRepayment / 2).ToString();

                                    for (int j = 0; j < row5; j++)
                                        if (!string.IsNullOrEmpty(original) && !string.IsNullOrEmpty(outstanding) && !string.IsNullOrEmpty(repayment))
                                            if (isMain)
                                            {
                                                if (string.IsNullOrEmpty(mainPL[j, 0]) && string.IsNullOrEmpty(mainPL[j, 1]) && string.IsNullOrEmpty(mainPL[j, 2]))
                                                {
                                                    mainPL[j, 0] = original;
                                                    mainPL[j, 1] = outstanding;
                                                    mainPL[j, 2] = repayment;
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                if (string.IsNullOrEmpty(coPL[j, 0]) && string.IsNullOrEmpty(coPL[j, 1]) && string.IsNullOrEmpty(coPL[j, 2]))
                                                {
                                                    coPL[j, 0] = original;
                                                    coPL[j, 1] = outstanding;
                                                    coPL[j, 2] = repayment;
                                                    break;
                                                }
                                            }
                                }
                                break;
                            case "4"://OverDraft
                                for (int i = 0; i < _account.sub_accounts.Count(); i++)
                                {
                                    original = string.IsNullOrEmpty(_account.limit.ToString()) ? "" : _account.limit.ToString();
                                    outstanding = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].balance.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].balance.ToString();
                                    repayment = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].inst_amount.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].inst_amount.ToString();
                                    if ((repayment.Equals("0") || string.IsNullOrEmpty(repayment)) && !string.IsNullOrEmpty(original))
                                        repayment = RepaymentFlat(12, ConvertHelper.ConvertToDecimal(original, 0), 4).ToString();
                                    if ((capacityCode.Equals("Joint") || capacityCode.Equals("Partner")) && !string.IsNullOrWhiteSpace(repayment) && double.TryParse(repayment, out double parseRepayment))
                                        repayment = (parseRepayment / 2).ToString();

                                    for (int j = 0; j < row5; j++)
                                        if (!string.IsNullOrEmpty(original) && !string.IsNullOrEmpty(outstanding) && !string.IsNullOrEmpty(repayment))
                                            if (isMain)
                                            {
                                                if (string.IsNullOrEmpty(mainOD[j, 0]) && string.IsNullOrEmpty(mainOD[j, 1]) && string.IsNullOrEmpty(mainOD[j, 2]))
                                                {
                                                    mainOD[j, 0] = original;
                                                    mainOD[j, 1] = outstanding;
                                                    mainOD[j, 2] = repayment;
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                if (string.IsNullOrEmpty(coOD[j, 0]) && string.IsNullOrEmpty(coOD[j, 1]) && string.IsNullOrEmpty(coOD[j, 2]))
                                                {
                                                    coOD[j, 0] = original;
                                                    coOD[j, 1] = outstanding;
                                                    coOD[j, 2] = repayment;
                                                    break;
                                                }
                                            }
                                }
                                break;
                            case "5"://X-Product
                                for (int i = 0; i < _account.sub_accounts.Count(); i++)
                                {
                                    original = string.IsNullOrEmpty(_account.limit.ToString()) ? "" : _account.limit.ToString();
                                    outstanding = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].balance.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].balance.ToString();
                                    repayment = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].inst_amount.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].inst_amount.ToString();
                                    if ((repayment.Equals("0") || string.IsNullOrEmpty(repayment)) && !string.IsNullOrEmpty(outstanding))
                                        repayment = RepaymentEffective(12, ConvertHelper.ConvertToDecimal(outstanding, 0), 5, mainAge, coAge).ToString();
                                    if ((capacityCode.Equals("Joint") || capacityCode.Equals("Partner")) && !string.IsNullOrWhiteSpace(repayment) && double.TryParse(repayment, out double parseRepayment))
                                        repayment = (parseRepayment / 2).ToString();

                                    for (int j = 0; j < row10; j++)
                                        if (!string.IsNullOrEmpty(original) && !string.IsNullOrEmpty(outstanding) && !string.IsNullOrEmpty(repayment))
                                            if (isMain)
                                            {
                                                if (string.IsNullOrEmpty(mainXP[j, 0]) && string.IsNullOrEmpty(mainXP[j, 1]) && string.IsNullOrEmpty(mainXP[j, 2]))
                                                {
                                                    mainXP[j, 0] = original;
                                                    mainXP[j, 1] = outstanding;
                                                    mainXP[j, 2] = repayment;
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                if (string.IsNullOrEmpty(coXP[j, 0]) && string.IsNullOrEmpty(coXP[j, 1]) && string.IsNullOrEmpty(coXP[j, 2]))
                                                {
                                                    coXP[j, 0] = original;
                                                    coXP[j, 1] = outstanding;
                                                    coXP[j, 2] = repayment;
                                                    break;
                                                }
                                            }
                                }
                                break;
                            case "6"://Hire Purchase (CarLoan)
                                for (int i = 0; i < _account.sub_accounts.Count(); i++)
                                {
                                    original = string.IsNullOrEmpty(_account.limit.ToString()) ? "" : _account.limit.ToString();
                                    outstanding = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].balance.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].balance.ToString();
                                    repayment = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].inst_amount.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].inst_amount.ToString();
                                    if ((repayment.Equals("0") || string.IsNullOrEmpty(repayment)) && !string.IsNullOrEmpty(outstanding))
                                        repayment = RepaymentFlat(12, ConvertHelper.ConvertToDecimal(outstanding, 0), 6).ToString();
                                    if ((capacityCode.Equals("Joint") || capacityCode.Equals("Partner")) && !string.IsNullOrWhiteSpace(repayment) && double.TryParse(repayment, out double parseRepayment))
                                        repayment = (parseRepayment / 2).ToString();

                                    for (int j = 0; j < row5; j++)
                                        if (!string.IsNullOrEmpty(original) && !string.IsNullOrEmpty(outstanding) && !string.IsNullOrEmpty(repayment))
                                            if (isMain)
                                            {
                                                if (string.IsNullOrEmpty(mainHP[j, 0]) && string.IsNullOrEmpty(mainHP[j, 1]) && string.IsNullOrEmpty(mainHP[j, 2]))
                                                {
                                                    mainHP[j, 0] = original;
                                                    mainHP[j, 1] = outstanding;
                                                    mainHP[j, 2] = repayment;
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                if (string.IsNullOrEmpty(coHP[j, 0]) && string.IsNullOrEmpty(coHP[j, 1]) && string.IsNullOrEmpty(coHP[j, 2]))
                                                {
                                                    coHP[j, 0] = original;
                                                    coHP[j, 1] = outstanding;
                                                    coHP[j, 2] = repayment;
                                                    break;
                                                }
                                            }
                                }
                                break;
                            case "7"://Credit Card
                                for (int i = 0; i < _account.sub_accounts.Count(); i++)
                                {
                                    outstanding = string.IsNullOrEmpty(_account.sub_accounts[i].cr_positions[0].balance.ToString()) ? "" : _account.sub_accounts[i].cr_positions[0].balance.ToString();
                                    if (ConvertHelper.ConvertToDecimal(outstanding, 0) > 0)
                                    {
                                        repayment = RepaymentMin(12, ConvertHelper.ConvertToDecimal(outstanding, 0)).ToString();

                                        for (int j = 0; j < row10; j++)
                                            if (!string.IsNullOrEmpty(outstanding) && !string.IsNullOrEmpty(repayment))
                                                if (isMain)
                                                {
                                                    if (string.IsNullOrEmpty(mainCC[j, 1]) && string.IsNullOrEmpty(mainCC[j, 2]))
                                                    {
                                                        mainCC[j, 1] = outstanding;
                                                        mainCC[j, 2] = repayment;
                                                        break;
                                                    }
                                                }
                                                else
                                                {
                                                    if (string.IsNullOrEmpty(coCC[j, 1]) && string.IsNullOrEmpty(coCC[j, 2]))
                                                    {
                                                        coCC[j, 1] = outstanding;
                                                        coCC[j, 2] = repayment;
                                                        break;
                                                    }
                                                }
                                    }
                                }
                                break;
                            default:
                                //sqlString.displayAlert2(this, "Don't have any loan");
                                break;
                        }
                    }
                }
                return true;
            }
            else
                return false;
        }
        public bool PassDataIntoArray(bool isMain, string mainAge, string coAge, xmlBanking_infoCcris_banking_details detail = null) //use controller ccris or pass in a ccris 
        {
            if (this.detail == null)
            {
                if (detail == null)
                    return false;
                else
                    this.detail = detail;
            }
            else
            {
                if (detail == null)
                    detail = this.detail;
                //else
                //    this.detail = detail;
            }

            Array.Clear(isMain ? mainHL : coHL, 0, isMain ? mainHL.Length : coHL.Length);
            Array.Clear(isMain ? mainCL : coCL, 0, isMain ? mainCL.Length : coCL.Length);
            Array.Clear(isMain ? mainHP : coHP, 0, isMain ? mainHP.Length : coHP.Length);
            Array.Clear(isMain ? mainCC : coCC, 0, isMain ? mainCC.Length : coCC.Length);
            Array.Clear(isMain ? mainOD : coOD, 0, isMain ? mainOD.Length : coOD.Length);
            Array.Clear(isMain ? mainPL : coPL, 0, isMain ? mainPL.Length : coPL.Length);
            Array.Clear(isMain ? mainXP : coXP, 0, isMain ? mainXP.Length : coXP.Length);

            if (this.detail != null)
            {
                wwdb db = new wwdb();
                StringBuilder sql = new StringBuilder();
                string loanid = string.Empty;
                string original = "";
                string outstanding = "";
                string repayment = "";

                do
                {
                    for (int count1 = 0; count1 < detail.outstanding_credit.Count(); count1++)
                    {
                        var _account = detail.outstanding_credit[count1];
                        string capacityCode = _account.master.item.capacity.ToString();//Own/Joint/Partner
                        string loanType = _account.sub_account[0].item.facility.ToString();
                        sql.Clear();
                        sql.Append("SELECT LoanID FROM tbl_LoanTypesXML WITH(NOLOCK) WHERE LoanTypeCode = '" + loanType + "';");
                        db.OpenTable(sql.ToString());
                        if (db.RecordCount() > 0)
                        {
                            loanid = db.Item("LoanID");
                            switch (loanid)
                            {
                                case "1"://Housing Loan
                                    for (int i = 0; i < _account.sub_account.Count(); i++)
                                    {
                                        original = string.IsNullOrEmpty(_account.master.item.limit.ToString()) ? "" : _account.master.item.limit.ToString();
                                        outstanding = string.IsNullOrEmpty(_account.sub_account[i].item.total_outstanding_balance.ToString()) ? "" : _account.sub_account[i].item.total_outstanding_balance.ToString();
                                        repayment = string.IsNullOrEmpty(_account.sub_account[i].item.installment_amount.ToString()) ? "" : _account.sub_account[i].item.installment_amount.ToString();
                                        if ((repayment.Equals("0") || string.IsNullOrEmpty(repayment)) && !string.IsNullOrEmpty(original))
                                            repayment = RepaymentEffective(12, ConvertHelper.ConvertToDecimal(original, 0), 1, mainAge, coAge).ToString(); //temp
                                        if ((capacityCode.Equals("Joint".ToUpper()) || capacityCode.Equals("Partner".ToUpper())) && !string.IsNullOrWhiteSpace(repayment) && double.TryParse(repayment, out double parseRepayment))
                                            repayment = (parseRepayment / 2.0).ToString();

                                        for (int j = 0; j < row5; j++)
                                            if (!string.IsNullOrEmpty(original) && !string.IsNullOrEmpty(outstanding) && !string.IsNullOrEmpty(repayment))
                                                if (isMain)
                                                {
                                                    if (string.IsNullOrEmpty(mainHL[j, 0]) && string.IsNullOrEmpty(mainHL[j, 1]) && string.IsNullOrEmpty(mainHL[j, 2]))
                                                    {
                                                        mainHL[j, 0] = original;
                                                        mainHL[j, 1] = outstanding;
                                                        //DateTime approvalDate = DateTime.ParseExact(_account.master.item.date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                                                        //if (DateTime.Now.Subtract(approvalDate).Days <= (3 * 365))
                                                        //{
                                                        //    mainHLrecalc[j, 2] = repayment;
                                                        //    mainHLcapCode[j, 2] = capacityCode;
                                                        //    repayment = "0";
                                                        //}
                                                        mainHL[j, 2] = repayment;
                                                        break;
                                                    }
                                                }
                                                else
                                                {
                                                    if (string.IsNullOrEmpty(coHL[j, 0]) && string.IsNullOrEmpty(coHL[j, 1]) && string.IsNullOrEmpty(coHL[j, 2]))
                                                    {
                                                        coHL[j, 0] = original;
                                                        coHL[j, 1] = outstanding;
                                                        //DateTime approvalDate = DateTime.ParseExact(_account.master.item.date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                                                        //if (DateTime.Now.Subtract(approvalDate).Days <= (3 * 365))
                                                        //{
                                                        //    coHLrecalc[j, 2] = repayment;
                                                        //    coHLcapCode[j, 2] = capacityCode;
                                                        //    repayment = "0";
                                                        //}
                                                        coHL[j, 2] = repayment;
                                                        break;
                                                    }
                                                }
                                    }
                                    break;
                                case "2"://Commercial Loan
                                    for (int i = 0; i < _account.sub_account.Count(); i++)
                                    {
                                        original = string.IsNullOrEmpty(_account.master.item.limit.ToString()) ? "" : _account.master.item.limit.ToString();
                                        outstanding = string.IsNullOrEmpty(_account.sub_account[i].item.total_outstanding_balance.ToString()) ? "" : _account.sub_account[i].item.total_outstanding_balance.ToString();
                                        repayment = string.IsNullOrEmpty(_account.sub_account[i].item.installment_amount.ToString()) ? "" : _account.sub_account[i].item.installment_amount.ToString();
                                        if ((repayment.Equals("0") || string.IsNullOrEmpty(repayment)) && !string.IsNullOrEmpty(outstanding))
                                            repayment = RepaymentEffective(12, ConvertHelper.ConvertToDecimal(outstanding, 0), 2, mainAge, coAge).ToString();
                                        if ((capacityCode.Equals("Joint".ToUpper()) || capacityCode.Equals("Partner".ToUpper())) && !string.IsNullOrWhiteSpace(repayment) && double.TryParse(repayment, out double parseRepayment))
                                            repayment = (parseRepayment / 2).ToString();

                                        for (int j = 0; j < row5; j++)
                                            if (!string.IsNullOrEmpty(original) && !string.IsNullOrEmpty(outstanding) && !string.IsNullOrEmpty(repayment))
                                                if (isMain)
                                                {
                                                    if (string.IsNullOrEmpty(mainCL[j, 0]) && string.IsNullOrEmpty(mainCL[j, 1]) && string.IsNullOrEmpty(mainCL[j, 2]))
                                                    {
                                                        mainCL[j, 0] = original;
                                                        mainCL[j, 1] = outstanding;
                                                        mainCL[j, 2] = repayment;
                                                        break;
                                                    }
                                                }
                                                else
                                                {
                                                    if (string.IsNullOrEmpty(coCL[j, 0]) && string.IsNullOrEmpty(coCL[j, 1]) && string.IsNullOrEmpty(coCL[j, 2]))
                                                    {
                                                        coCL[j, 0] = original;
                                                        coCL[j, 1] = outstanding;
                                                        coCL[j, 2] = repayment;
                                                        break;
                                                    }
                                                }
                                    }
                                    break;
                                case "3"://Personal Loan
                                    for (int i = 0; i < _account.sub_account.Count(); i++)
                                    {
                                        original = string.IsNullOrEmpty(_account.master.item.limit.ToString()) ? "" : _account.master.item.limit.ToString();
                                        outstanding = string.IsNullOrEmpty(_account.sub_account[i].item.total_outstanding_balance.ToString()) ? "" : _account.sub_account[i].item.total_outstanding_balance.ToString();
                                        repayment = string.IsNullOrEmpty(_account.sub_account[i].item.installment_amount.ToString()) ? "" : _account.sub_account[i].item.installment_amount.ToString();
                                        if ((repayment.Equals("0") || string.IsNullOrEmpty(repayment)) && !string.IsNullOrEmpty(outstanding))
                                            repayment = RepaymentFlat(12, ConvertHelper.ConvertToDecimal(outstanding, 0), 3).ToString();
                                        if ((capacityCode.Equals("Joint".ToUpper()) || capacityCode.Equals("Partner".ToUpper())) && !string.IsNullOrWhiteSpace(repayment) && double.TryParse(repayment, out double parseRepayment))
                                            repayment = (parseRepayment / 2).ToString();

                                        for (int j = 0; j < row5; j++)
                                            if (!string.IsNullOrEmpty(original) && !string.IsNullOrEmpty(outstanding) && !string.IsNullOrEmpty(repayment))
                                                if (isMain)
                                                {
                                                    if (string.IsNullOrEmpty(mainPL[j, 0]) && string.IsNullOrEmpty(mainPL[j, 1]) && string.IsNullOrEmpty(mainPL[j, 2]))
                                                    {
                                                        mainPL[j, 0] = original;
                                                        mainPL[j, 1] = outstanding;
                                                        mainPL[j, 2] = repayment;
                                                        break;
                                                    }
                                                }
                                                else
                                                {
                                                    if (string.IsNullOrEmpty(coPL[j, 0]) && string.IsNullOrEmpty(coPL[j, 1]) && string.IsNullOrEmpty(coPL[j, 2]))
                                                    {
                                                        coPL[j, 0] = original;
                                                        coPL[j, 1] = outstanding;
                                                        coPL[j, 2] = repayment;
                                                        break;
                                                    }
                                                }
                                    }
                                    break;
                                case "4"://OverDraft
                                    for (int i = 0; i < _account.sub_account.Count(); i++)
                                    {
                                        original = string.IsNullOrEmpty(_account.master.item.limit.ToString()) ? "" : _account.master.item.limit.ToString();
                                        outstanding = string.IsNullOrEmpty(_account.sub_account[i].item.total_outstanding_balance.ToString()) ? "" : _account.sub_account[i].item.total_outstanding_balance.ToString();
                                        repayment = string.IsNullOrEmpty(_account.sub_account[i].item.installment_amount.ToString()) ? "" : _account.sub_account[i].item.installment_amount.ToString();
                                        if ((repayment.Equals("0") || string.IsNullOrEmpty(repayment)) && !string.IsNullOrEmpty(original))
                                            repayment = RepaymentFlat(12, ConvertHelper.ConvertToDecimal(original, 0), 4).ToString();
                                        if ((capacityCode.Equals("Joint".ToUpper()) || capacityCode.Equals("Partner".ToUpper())) && !string.IsNullOrWhiteSpace(repayment) && double.TryParse(repayment, out double parseRepayment))
                                            repayment = (parseRepayment / 2).ToString();

                                        for (int j = 0; j < row5; j++)
                                            if (!string.IsNullOrEmpty(original) && !string.IsNullOrEmpty(outstanding) && !string.IsNullOrEmpty(repayment))
                                                if (isMain)
                                                {
                                                    if (string.IsNullOrEmpty(mainOD[j, 0]) && string.IsNullOrEmpty(mainOD[j, 1]) && string.IsNullOrEmpty(mainOD[j, 2]))
                                                    {
                                                        mainOD[j, 0] = original;
                                                        mainOD[j, 1] = outstanding;
                                                        mainOD[j, 2] = repayment;
                                                        break;
                                                    }
                                                }
                                                else
                                                {
                                                    if (string.IsNullOrEmpty(coOD[j, 0]) && string.IsNullOrEmpty(coOD[j, 1]) && string.IsNullOrEmpty(coOD[j, 2]))
                                                    {
                                                        coOD[j, 0] = original;
                                                        coOD[j, 1] = outstanding;
                                                        coOD[j, 2] = repayment;
                                                        break;
                                                    }
                                                }
                                    }
                                    break;
                                case "5"://X-Product
                                    for (int i = 0; i < _account.sub_account.Count(); i++)
                                    {
                                        original = string.IsNullOrEmpty(_account.master.item.limit.ToString()) ? "" : _account.master.item.limit.ToString();
                                        outstanding = string.IsNullOrEmpty(_account.sub_account[i].item.total_outstanding_balance.ToString()) ? "" : _account.sub_account[i].item.total_outstanding_balance.ToString();
                                        repayment = string.IsNullOrEmpty(_account.sub_account[i].item.installment_amount.ToString()) ? "" : _account.sub_account[i].item.installment_amount.ToString();
                                        if ((repayment.Equals("0") || string.IsNullOrEmpty(repayment)) && !string.IsNullOrEmpty(outstanding))
                                            repayment = RepaymentEffective(12, ConvertHelper.ConvertToDecimal(outstanding, 0), 5, mainAge, coAge).ToString();
                                        if ((capacityCode.Equals("Joint".ToUpper()) || capacityCode.Equals("Partner".ToUpper())) && !string.IsNullOrWhiteSpace(repayment) && double.TryParse(repayment, out double parseRepayment))
                                            repayment = (parseRepayment / 2).ToString();

                                        for (int j = 0; j < row10; j++)
                                            if (!string.IsNullOrEmpty(original) && !string.IsNullOrEmpty(outstanding) && !string.IsNullOrEmpty(repayment))
                                                if (isMain)
                                                {
                                                    if (string.IsNullOrEmpty(mainXP[j, 0]) && string.IsNullOrEmpty(mainXP[j, 1]) && string.IsNullOrEmpty(mainXP[j, 2]))
                                                    {
                                                        mainXP[j, 0] = original;
                                                        mainXP[j, 1] = outstanding;
                                                        mainXP[j, 2] = repayment;
                                                        break;
                                                    }
                                                }
                                                else
                                                {
                                                    if (string.IsNullOrEmpty(coXP[j, 0]) && string.IsNullOrEmpty(coXP[j, 1]) && string.IsNullOrEmpty(coXP[j, 2]))
                                                    {
                                                        coXP[j, 0] = original;
                                                        coXP[j, 1] = outstanding;
                                                        coXP[j, 2] = repayment;
                                                        break;
                                                    }
                                                }
                                    }
                                    break;
                                case "6"://Hire Purchase (CarLoan)
                                    for (int i = 0; i < _account.sub_account.Count(); i++)
                                    {
                                        original = string.IsNullOrEmpty(_account.master.item.limit.ToString()) ? "" : _account.master.item.limit.ToString();
                                        outstanding = string.IsNullOrEmpty(_account.sub_account[i].item.total_outstanding_balance.ToString()) ? "" : _account.sub_account[i].item.total_outstanding_balance.ToString();
                                        repayment = string.IsNullOrEmpty(_account.sub_account[i].item.installment_amount.ToString()) ? "" : _account.sub_account[i].item.installment_amount.ToString();
                                        if ((repayment.Equals("0") || string.IsNullOrEmpty(repayment)) && !string.IsNullOrEmpty(outstanding))
                                            repayment = RepaymentFlat(12, ConvertHelper.ConvertToDecimal(outstanding, 0), 6).ToString();
                                        if ((capacityCode.Equals("Joint".ToUpper()) || capacityCode.Equals("Partner".ToUpper())) && !string.IsNullOrWhiteSpace(repayment) && double.TryParse(repayment, out double parseRepayment))
                                            repayment = (parseRepayment / 2).ToString();

                                        for (int j = 0; j < row5; j++)
                                            if (!string.IsNullOrEmpty(original) && !string.IsNullOrEmpty(outstanding) && !string.IsNullOrEmpty(repayment))
                                                if (isMain)
                                                {
                                                    if (string.IsNullOrEmpty(mainHP[j, 0]) && string.IsNullOrEmpty(mainHP[j, 1]) && string.IsNullOrEmpty(mainHP[j, 2]))
                                                    {
                                                        mainHP[j, 0] = original;
                                                        mainHP[j, 1] = outstanding;
                                                        mainHP[j, 2] = repayment;
                                                        break;
                                                    }
                                                }
                                                else
                                                {
                                                    if (string.IsNullOrEmpty(coHP[j, 0]) && string.IsNullOrEmpty(coHP[j, 1]) && string.IsNullOrEmpty(coHP[j, 2]))
                                                    {
                                                        coHP[j, 0] = original;
                                                        coHP[j, 1] = outstanding;
                                                        coHP[j, 2] = repayment;
                                                        break;
                                                    }
                                                }
                                    }
                                    break;
                                case "7"://Credit Card
                                    for (int i = 0; i < _account.sub_account.Count(); i++)
                                    {
                                        outstanding = string.IsNullOrEmpty(_account.sub_account[i].item.total_outstanding_balance.ToString()) ? "" : _account.sub_account[i].item.total_outstanding_balance.ToString();
                                        if (ConvertHelper.ConvertToDecimal(outstanding, 0) > 0)
                                        {
                                            repayment = RepaymentMin(12, ConvertHelper.ConvertToDecimal(outstanding, 0)).ToString();

                                            for (int j = 0; j < row10; j++)
                                                if (!string.IsNullOrEmpty(outstanding) && !string.IsNullOrEmpty(repayment))
                                                    if (isMain)
                                                    {
                                                        if (string.IsNullOrEmpty(mainCC[j, 1]) && string.IsNullOrEmpty(mainCC[j, 2]))
                                                        {
                                                            mainCC[j, 1] = outstanding;
                                                            mainCC[j, 2] = repayment;
                                                            break;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (string.IsNullOrEmpty(coCC[j, 1]) && string.IsNullOrEmpty(coCC[j, 2]))
                                                        {
                                                            coCC[j, 1] = outstanding;
                                                            coCC[j, 2] = repayment;
                                                            break;
                                                        }
                                                    }
                                        }
                                    }
                                    break;
                                default:
                                    //sqlString.displayAlert2(this, "Don't have any loan");
                                    break;
                            }
                        }
                    }
                    if (isMain)
                        isMain = false;
                    else
                        break;
                } while (true);
                return true;
            }
            else
                return false;
        }
        public void GetGridViewData(GridView gvHL, GridView gvHP, GridView gvCL, GridView gvCC, GridView gvPL, GridView gvOD, GridView gvXP, bool isMain)
        {
            for (int j = 0; j < gvHL.Rows.Count; j++)
            {
                TextBox HLtxt0 = (TextBox)(isMain ? gvHL.Rows[j].Cells[0].FindControl("txtMainOriginalVal") : gvHL.Rows[j].Cells[0].FindControl("txtCoOriginalVal"));
                TextBox HLtxt1 = (TextBox)(isMain ? gvHL.Rows[j].Cells[1].FindControl("txtMainOutstandinglVal") : gvHL.Rows[j].Cells[1].FindControl("txtCoOutstandinglVal"));
                TextBox HLtxt2 = (TextBox)(isMain ? gvHL.Rows[j].Cells[2].FindControl("txtMainRepaymentlVal") : gvHL.Rows[j].Cells[2].FindControl("txtCoRepaymentlVal"));
                if (!string.IsNullOrEmpty(HLtxt0.Text) && !string.IsNullOrEmpty(HLtxt1.Text) && !string.IsNullOrEmpty(HLtxt2.Text))
                {
                    if (isMain)
                    {
                        mainHL[j, 0] = HLtxt0.Text;
                        mainHL[j, 1] = HLtxt1.Text;
                        mainHL[j, 2] = HLtxt2.Text;
                    }
                    else
                    {
                        coHL[j, 0] = HLtxt0.Text;
                        coHL[j, 1] = HLtxt1.Text;
                        coHL[j, 2] = HLtxt2.Text;
                    }
                }
                TextBox HPtxt0 = (TextBox)(isMain ? gvHP.Rows[j].Cells[0].FindControl("txtMainOriginalVal") : gvHP.Rows[j].Cells[0].FindControl("txtCoOriginalVal"));
                TextBox HPtxt1 = (TextBox)(isMain ? gvHP.Rows[j].Cells[1].FindControl("txtMainOutstandinglVal") : gvHP.Rows[j].Cells[1].FindControl("txtCoOutstandinglVal"));
                TextBox HPtxt2 = (TextBox)(isMain ? gvHP.Rows[j].Cells[2].FindControl("txtMainRepaymentlVal") : gvHP.Rows[j].Cells[2].FindControl("txtCoRepaymentlVal"));
                if (!string.IsNullOrEmpty(HPtxt0.Text) && !string.IsNullOrEmpty(HPtxt1.Text) && !string.IsNullOrEmpty(HPtxt2.Text))
                {
                    if (isMain)
                    {
                        mainHP[j, 0] = HPtxt0.Text;
                        mainHP[j, 1] = HPtxt1.Text;
                        mainHP[j, 2] = HPtxt2.Text;
                    }
                    else
                    {
                        coHP[j, 0] = HPtxt0.Text;
                        coHP[j, 1] = HPtxt1.Text;
                        coHP[j, 2] = HPtxt2.Text;
                    }
                }
                TextBox PLtxt0 = (TextBox)(isMain ? gvPL.Rows[j].Cells[0].FindControl("txtMainOriginalVal") : gvPL.Rows[j].Cells[0].FindControl("txtCoOriginalVal"));
                TextBox PLtxt1 = (TextBox)(isMain ? gvPL.Rows[j].Cells[1].FindControl("txtMainOutstandinglVal") : gvPL.Rows[j].Cells[1].FindControl("txtCoOutstandinglVal"));
                TextBox PLtxt2 = (TextBox)(isMain ? gvPL.Rows[j].Cells[2].FindControl("txtMainRepaymentlVal") : gvPL.Rows[j].Cells[2].FindControl("txtCoRepaymentlVal"));
                if (!string.IsNullOrEmpty(PLtxt0.Text) && !string.IsNullOrEmpty(PLtxt1.Text) && !string.IsNullOrEmpty(PLtxt2.Text))
                {
                    if (isMain)
                    {
                        mainPL[j, 0] = PLtxt0.Text;
                        mainPL[j, 1] = PLtxt1.Text;
                        mainPL[j, 2] = PLtxt2.Text;
                    }
                    else
                    {
                        coPL[j, 0] = PLtxt0.Text;
                        coPL[j, 1] = PLtxt1.Text;
                        coPL[j, 2] = PLtxt2.Text;
                    }
                }
                TextBox CLtxt0 = (TextBox)(isMain ? gvCL.Rows[j].Cells[0].FindControl("txtMainOriginalVal") : gvCL.Rows[j].Cells[0].FindControl("txtCoOriginalVal"));
                TextBox CLtxt1 = (TextBox)(isMain ? gvCL.Rows[j].Cells[1].FindControl("txtMainOutstandinglVal") : gvCL.Rows[j].Cells[1].FindControl("txtCoOutstandinglVal"));
                TextBox CLtxt2 = (TextBox)(isMain ? gvCL.Rows[j].Cells[2].FindControl("txtMainRepaymentlVal") : gvCL.Rows[j].Cells[2].FindControl("txtCoRepaymentlVal"));
                if (!string.IsNullOrEmpty(CLtxt0.Text) && !string.IsNullOrEmpty(CLtxt1.Text) && !string.IsNullOrEmpty(CLtxt2.Text))
                {
                    if (isMain)
                    {
                        mainCL[j, 0] = CLtxt0.Text;
                        mainCL[j, 1] = CLtxt1.Text;
                        mainCL[j, 2] = CLtxt2.Text;
                    }
                    else
                    {
                        coCL[j, 0] = CLtxt0.Text;
                        coCL[j, 1] = CLtxt1.Text;
                        coCL[j, 2] = CLtxt2.Text;
                    }
                }
                TextBox ODtxt0 = (TextBox)(isMain ? gvOD.Rows[j].Cells[0].FindControl("txtMainOriginalVal") : gvOD.Rows[j].Cells[0].FindControl("txtCoOriginalVal"));
                TextBox ODtxt1 = (TextBox)(isMain ? gvOD.Rows[j].Cells[1].FindControl("txtMainOutstandinglVal") : gvOD.Rows[j].Cells[1].FindControl("txtCoOutstandinglVal"));
                TextBox ODtxt2 = (TextBox)(isMain ? gvOD.Rows[j].Cells[2].FindControl("txtMainRepaymentlVal") : gvOD.Rows[j].Cells[2].FindControl("txtCoRepaymentlVal"));
                if (!string.IsNullOrEmpty(ODtxt0.Text) && !string.IsNullOrEmpty(ODtxt1.Text) && !string.IsNullOrEmpty(ODtxt2.Text))
                {
                    if (isMain)
                    {
                        mainOD[j, 0] = ODtxt0.Text;
                        mainOD[j, 1] = ODtxt1.Text;
                        mainOD[j, 2] = ODtxt2.Text;
                    }
                    else
                    {
                        coOD[j, 0] = ODtxt0.Text;
                        coOD[j, 1] = ODtxt1.Text;
                        coOD[j, 2] = ODtxt2.Text;
                    }
                }
            }
            for (int j = 0; j < gvCC.Rows.Count; j++)
            {
                TextBox CCtxt1 = (TextBox)(isMain ? gvCC.Rows[j].Cells[1].FindControl("txtMainOutstandinglVal") : gvCC.Rows[j].Cells[1].FindControl("txtCoOutstandinglVal"));
                if (!string.IsNullOrEmpty(CCtxt1.Text))
                    if (ConvertHelper.ConvertToDecimal(CCtxt1.Text, 0) > 0)
                        if (isMain)
                            mainCC[j, 1] = CCtxt1.Text;
                        else
                            coCC[j, 1] = CCtxt1.Text;
                TextBox XPtxt0 = (TextBox)(isMain ? gvXP.Rows[j].Cells[0].FindControl("txtMainOriginalVal") : gvXP.Rows[j].Cells[0].FindControl("txtCoOriginalVal"));
                TextBox XPtxt1 = (TextBox)(isMain ? gvXP.Rows[j].Cells[1].FindControl("txtMainOutstandinglVal") : gvXP.Rows[j].Cells[1].FindControl("txtCoOutstandinglVal"));
                TextBox XPtxt2 = (TextBox)(isMain ? gvXP.Rows[j].Cells[2].FindControl("txtMainRepaymentlVal") : gvXP.Rows[j].Cells[2].FindControl("txtCoRepaymentlVal"));
                if (!string.IsNullOrEmpty(XPtxt0.Text) && !string.IsNullOrEmpty(XPtxt1.Text) && !string.IsNullOrEmpty(XPtxt2.Text))
                {
                    if (isMain)
                    {
                        mainXP[j, 0] = XPtxt0.Text;
                        mainXP[j, 1] = XPtxt1.Text;
                        mainXP[j, 2] = XPtxt2.Text;
                    }
                    else
                    {
                        coXP[j, 0] = XPtxt0.Text;
                        coXP[j, 1] = XPtxt1.Text;
                        coXP[j, 2] = XPtxt2.Text;
                    }
                }
            }
        }
        private void InsertDataIntoLoanDetail(string[,] array2D, string requestid, string bankid, int gridviewtype, int noofRows, string mainAge,
            bool isMain, string coAge = "0")
        {
            StringBuilder sql = new StringBuilder();
            wwdb db = new wwdb();
            try
            {
                //Main Grid view for Housing Loan
                for (int i = 0; i < noofRows; i++)
                {
                    string txtoriginal, txtoutstanding, txtrepayment; //[row,0],[row,1],[row,2]
                    if (isMain)
                    {
                        txtoriginal = array2D[i, 0];
                        txtoutstanding = array2D[i, 1];
                        txtrepayment = array2D[i, 2];
                    }
                    else
                    {
                        txtoriginal = array2D[i, 0];
                        txtoutstanding = array2D[i, 1];
                        txtrepayment = array2D[i, 2];
                    }

                    decimal original = 0;
                    decimal outstanding = 0;
                    decimal repayment = 0;
                    int rowID = i + 1;

                    if (!string.IsNullOrEmpty(txtoriginal) || !string.IsNullOrEmpty(txtoutstanding) || !string.IsNullOrEmpty(txtrepayment))
                    {
                        if (!string.IsNullOrEmpty(txtoriginal))
                        {
                            original = ConvertHelper.ConvertToDecimal(txtoriginal, 0);
                        }
                        else
                        {
                            original = 0;
                        }

                        if (!string.IsNullOrEmpty(txtoutstanding))
                        {
                            outstanding = ConvertHelper.ConvertToDecimal(txtoutstanding, 0);
                        }
                        else
                        {
                            outstanding = 0;
                        }

                        if (!string.IsNullOrEmpty(txtrepayment) && ConvertHelper.ConvertToDecimal(txtrepayment, 0) > 0)
                        {
                            repayment = ConvertHelper.ConvertToDecimal(txtrepayment, 0);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(txtoriginal) && original != 0)
                                if (gridviewtype == 4) //Overdraft
                                    repayment = RepaymentFlat(ConvertHelper.ConvertToInt(bankid.ToString(), 0), original, gridviewtype);
                                else if (gridviewtype == 1) //HousingLoan
                                {
                                    repayment = RepaymentEffective(ConvertHelper.ConvertToInt(bankid.ToString(), 0), original, gridviewtype, mainAge, coAge);
                                    //if (isMain)
                                    //{
                                    //    if (!string.IsNullOrEmpty(mainHLrecalc[i, 2]))
                                    //    {
                                    //        if (!string.IsNullOrEmpty(mainHLcapCode[i, 2]))
                                    //            if (mainHLcapCode[i, 2].Equals("Joint") || mainHLcapCode[i, 2].Equals("Partner"))
                                    //                repayment = repayment / 2;
                                    //        if (decimal.Compare(repayment, ConvertHelper.ConvertToDecimal(mainHLrecalc[i, 2], 0)) < 0)
                                    //            repayment = ConvertHelper.ConvertToDecimal(mainHLrecalc[i, 2], 0);
                                    //        //else repayment=repayment;
                                    //    }
                                    //}
                                    //else
                                    //{
                                    //    if (!string.IsNullOrEmpty(coHLrecalc[i, 2]))
                                    //    {
                                    //        if (!string.IsNullOrEmpty(coHLcapCode[i, 2]))
                                    //            if (coHLcapCode[i, 2].Equals("Joint") || coHLcapCode[i, 2].Equals("Partner"))
                                    //                repayment = repayment / 2;
                                    //        if (decimal.Compare(repayment, ConvertHelper.ConvertToDecimal(coHLrecalc[i, 2], 0)) < 0)
                                    //            repayment = ConvertHelper.ConvertToDecimal(coHLrecalc[i, 2], 0);
                                    //    }
                                    //}
                                }
                            if (!string.IsNullOrEmpty(txtoutstanding) && outstanding != 0)
                                //Hire Purchase, Personal Loan
                                if (gridviewtype == 6 || gridviewtype == 3)
                                    repayment = RepaymentFlat(ConvertHelper.ConvertToInt(bankid.ToString(), 0), outstanding, gridviewtype);
                                //Credit Card
                                else if (gridviewtype == 7)
                                    repayment = RepaymentMin(ConvertHelper.ConvertToInt(bankid.ToString(), 0), outstanding);
                                //Any other (except OD and HousingLoan)
                                else if (gridviewtype != 4 && gridviewtype != 1)
                                    repayment = RepaymentEffective(ConvertHelper.ConvertToInt(bankid.ToString(), 0), outstanding, gridviewtype, mainAge, coAge);
                        }
                    }

                    if ((!string.IsNullOrEmpty(txtoriginal) || !string.IsNullOrEmpty(txtoutstanding) || !string.IsNullOrEmpty(txtrepayment))
                        && (original > 0 || outstanding > 0 || repayment > 0))
                    {
                        sql.Clear();
                        sql.Append(" INSERT INTO tbl_TLoanDetail ");
                        sql.Append("(RequestID, BankID, LoanID, RowID, OriginalValue, OutStandingValue, Repayment, isConstant, isMain, CreatedBy, CreatedAt, UpdatedBy, UpdatedAt)");
                        sql.Append(" VALUES( '" + secure.RC(requestid) + "', '" + secure.RC(bankid) + "', '" + gridviewtype + "', '" + secure.RC(rowID.ToString()) + "', ");
                        sql.Append(" '" + original + "', '" + outstanding + "', '" + repayment + "', 'true', '" + secure.RC(isMain.ToString()) + "', ");
                        sql.Append(" '" + secure.RC(userID) + "', GETDATE(), '" + secure.RC(userID) + "', GETDATE() ); ");
                        db.Execute(sql.ToString());
                        LogUtil.logAction(sql.ToString(), "Insert Data into tbl_LoanDetails - RequestID :" + requestid.ToString());
                    }
                }
            }
            catch (Exception ex) { LogUtil.logError(ex.ToString(), sql.ToString()); }
            finally { db.Close(); }
        }
        private void InsertDataIntoLoanInfo(string requestid, string bankid, bool isMainOnly, string mainAge, string coAge = "0")
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            decimal policy = 0;
            decimal dsr = 0;
            decimal loanAmountEligible = 0;
            decimal incomeRequired = 0;
            decimal declareIncome = 0;
            decimal income = 0;
            decimal oldRepayment = 0;
            decimal newRepayment = 0;
            string projectid = this.projectID;
            int tenure = 0;
            decimal loanAmount = 0;
            decimal margin = 0;
            decimal policyIncome = 0;
            margin = PassBankMargin(bankid, projectid, requestid);

            #region Calculating Tenure
            int tenure2 = getTenureYear(6, bankid);
            tenure = 70 - YoungerAge(ConvertHelper.ConvertToInt(mainAge, 0), ConvertHelper.ConvertToInt(coAge, 0));
            if (tenure > tenure2)
                tenure = tenure2;
            else if (tenure <= 0) //shouldn't be reached
                tenure = 1;

            if (bankid.Equals("12"))
            {
                string tempSql = "UPDATE tbl_TLoanLegalAction SET Tenure = '" + tenure.ToString() + "' WHERE RequestID = N'" + requestid + "';";
                db.Execute(tempSql);
                if (db.HasError)
                    LogUtil.logError(db.ErrorMessage, tempSql);
            }
            #endregion
            newRepayment = CalculateNewLoanRepayment(bankid, projectid, requestid, isMainOnly, tenure);
            oldRepayment = CalculateTotalOldBankRepayments(bankid, requestid, isMainOnly);
            income = CalculateTotalIncome(isMainOnly);
            policyIncome = CalculateGrossIncomePolicy();

            if (this.loanAmount.ToString() != String.Empty)
            {
                if (ConvertHelper.ConvertToDecimal(this.loanAmount, 0) > 0)
                {
                    loanAmount = ConvertHelper.ConvertToDecimal(this.loanAmount, 0);
                }
                else
                {
                    if (purchasePrice != String.Empty)
                    {
                        if (ConvertHelper.ConvertToDecimal(purchasePrice, 0) > 0)
                        {
                            loanAmount = ConvertHelper.ConvertToDecimal(purchasePrice, 0) * margin / 100;
                        }
                        else
                        {
                            loanAmount = 0;
                        }
                    }
                    else
                    {
                        loanAmount = 0;
                    }
                }
            }
            else
            {
                if (purchasePrice != String.Empty)
                {
                    if (ConvertHelper.ConvertToDecimal(purchasePrice, 0) > 0)
                    {
                        loanAmount = ConvertHelper.ConvertToDecimal(purchasePrice, 0) * margin / 100;
                    }
                    else
                    {
                        loanAmount = 0;
                    }
                }
                else
                {
                    loanAmount = 0;
                }
            }

            policy = calculatorController.getBankPolicy(bankid, policyIncome, isMainOnly);
            dsr = calculatorController.calculateDSR(oldRepayment, newRepayment, income);

            loanAmountEligible = calculatorController.calculateLoanEligible(policy, oldRepayment,
                YoungerAge(ConvertHelper.ConvertToInt(mainAge, 0), ConvertHelper.ConvertToInt(coAge, 0)),
                income, tenure, bankid);

            //newLoanRepayment = calculatorController.calculateNewRepayment(loanAmount, tenure, interest);

            incomeRequired = calculatorController.calculateIncomeRequired(policy, oldRepayment, newRepayment);

            declareIncome = income;

            try
            {
                sql.Clear();
                sql.Append(" INSERT INTO tbl_TLoanInfo ");
                sql.Append(" (RequestID, BankID, PreferRatio, DSR, ActualLoanAmount, LoanAmountEligible, NewLoanRepayment, " +
                    "IncomeRequired, DeclareIncome, CreatedBy, CreatedAt, UpdatedBy, UpdatedAt) ");
                sql.Append(" VALUES( N'" + secure.RC(requestid) + "', N'" + secure.RC(bankid) + "', '" + policy + "', '" + dsr + "', '" +
                    loanAmount.ToString().Replace(",", "") + "', '" + loanAmountEligible.ToString().Replace(",", "") + "', ");
                sql.Append(" '" + newRepayment.ToString().Replace(",", "") + "', '" + incomeRequired.ToString().Replace(",", "") +
                    "', '" + declareIncome.ToString().Replace(",", "") + "',  ");
                sql.Append(" '" + secure.RC(userID) + "', GETDATE(), '" + secure.RC(userID) + "', GETDATE() ); ");
                db.Execute(sql.ToString());
                LogUtil.logAction(sql.ToString(), "Insert Data into tbl_LoanInfo - RequestID :" + requestid.ToString());
            }
            catch (Exception ex) { LogUtil.logError(ex.ToString(), sql.ToString()); }
            finally { db.Close(); }
        }
        #endregion

        #region Calculation
        public decimal PassBankMargin(string bankid, string projectid, string requestid)
        {
            decimal bankMargin = 0;
            bool capping = false;
            decimal mainHouseLoanAmount = 0;
            decimal coHouseLoanAmount = 0;
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();

            try
            {
                sql.Clear();
                sql.Append(" SELECT HouseLoanNumber ");
                sql.Append(" FROM tbl_TApplicants WITH (NOLOCK)");
                sql.Append(" WHERE ApplicantType = 'MA' AND RequestID = '" + secure.RC(requestid) + "' ");
                db.OpenTable(sql.ToString());

                if (db.RecordCount() > 0)
                {
                    mainHouseLoanAmount = ConvertHelper.ConvertToDecimal(db.Item("HouseLoanNumber"), 0);
                }

                sql.Clear();
                sql.Append(" SELECT HouseLoanNumber ");
                sql.Append(" FROM tbl_TApplicants WITH (NOLOCK)");
                sql.Append(" WHERE ApplicantType = 'CA' AND RequestID = '" + secure.RC(requestid) + "' ");
                db.OpenTable(sql.ToString());

                if (db.RecordCount() > 0)
                {
                    coHouseLoanAmount = ConvertHelper.ConvertToDecimal(db.Item("HouseLoanNumber"), 0);
                }

                sql.Clear();
                sql.Append(" SELECT ProjectID, Capping, BankMargin, BankID ");
                sql.Append(" FROM ( ");
                sql.Append(" 	SELECT p.ProjectID, p.Capping, pb.BankMargin, pb.BankID ");
                sql.Append(" 	FROM tbl_ProjectSettings p WITH(NOLOCK) ");
                sql.Append(" 	LEFT JOIN tbl_ProjectSettings_Bank pb WITH(NOLOCK) ON pb.ProjectID = p.ProjectID ");
                sql.Append(" 	INNER JOIN tbl_BankName b WITH(NOLOCK) ON b.BankID = pb.BankID AND b.Status = 'A' AND b.IsDeleted = 'False' ");
                sql.Append(" 	WHERE p.Status = 'A' AND p.isDeleted = 'false' ");
                sql.Append(" 	) AS A ");
                sql.Append(" WHERE ProjectID = '" + secure.RC(projectid) + "' AND BankID = '" + secure.RC(bankid) + "' ;");
                db.OpenTable(sql.ToString());

                if (db.RecordCount() > 0)
                {
                    capping = ConvertHelper.ConvertToBoolen(db.Item("Capping"), false);
                    bankMargin = ConvertHelper.ConvertToDecimal(db.Item("BankMargin"), 90);
                }

                //if (capping == true)
                //{
                //    if (bankMargin > 70)
                //    {
                //        bankMargin = 70;
                //    }
                //}
                //else if (capping == false)
                //{
                //    if (mainHouseLoanAmount > 1 || coHouseLoanAmount > 1)
                //    {
                //        if (bankMargin > 70)
                //        {
                //            bankMargin = 70;
                //        }
                //    }
                //}//temp ignore capping

                if (bankMargin == 0)
                    bankMargin = 90;

                if (mainHouseLoanAmount > 1 || coHouseLoanAmount > 1)
                    //if (bankMargin > 70)
                    bankMargin = 70;
            }
            catch (Exception ex) { LogUtil.logError(ex.ToString(), sql.ToString()); }
            finally { db.Close(); }


            return bankMargin;
        }
        public int YoungerAge(int mainAge, int coAge)
        {
            if (coAge <= 0)
                return mainAge;

            int Age = 0;
            Age = mainAge <= coAge ? mainAge : coAge;
            return Age;
        }
        private string getEffectiveRate(int type)
        {
            string sql = string.Empty;
            switch (type)
            {
                case 1: //House
                    sql = "SELECT Year, Interest FROM tbl_houseloan with (nolock) ";
                    break;
                case 2://Commercial
                    sql = "SELECT Year, Interest FROM tbl_commercialloan with (nolock) ";
                    break;
                case 3://Personal
                    sql = "SELECT Year, Interest FROM tbl_PersonalLoan with (nolock) ";
                    break;
                case 4: //Overdraft
                    sql = "SELECT Year, Interest FROM tbl_overdraft with (nolock) ";
                    break;
                case 5: //X-product
                    sql = "SELECT Year, Interest FROM tbl_xproduct with (nolock) ";
                    break;
                case 6: //HirePurchase
                    sql = "SELECT Year, Interest FROM tbl_hirepurchase with (nolock) ";
                    break;
            }
            return sql;
        }
        public decimal getIntrestRate(int type, string bankID = "")
        {
            wwdb db = new wwdb();
            decimal interest = 0;
            db.OpenTable(getEffectiveRate(type) + " WHERE BankID = " + (string.IsNullOrWhiteSpace(bankID) ? "12" : bankID));
            if (db.RecordCount() > 0)
                interest = ConvertHelper.ConvertToDecimal(db.Item("Interest"), (decimal)4.5);

            return interest;
        }
        public int getTenureYear(int type, string bankID = "")
        {
            wwdb db = new wwdb();
            int tenure = 0;
            db.OpenTable(getEffectiveRate(type) + " WHERE BankID = " + (string.IsNullOrWhiteSpace(bankID) ? "12" : bankID));
            if (db.RecordCount() > 0)
                tenure = ConvertHelper.ConvertToInt(db.Item("Year"), 1);

            return tenure;
        }
        public decimal RepaymentEffective(int bankID, decimal LoanAmount, int type, string MainAge, string CoAge) //Housing Loan, Commercial Loan ,X-Product
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            //int mainAge = ConvertHelper.ConvertToInt32(txtMainAge.Text.Trim(), 0);
            //int coAge = ConvertHelper.ConvertToInt32(txtCoAge.Text.Trim(),0);
            int mainAge = ConvertHelper.ConvertToInt32(MainAge.Trim(), 0);
            int coAge = ConvertHelper.ConvertToInt32(CoAge.Trim(), 0);
            int maxAge = 70; int maxYear = 0;
            double Interest = 0;
            int Year = 0;
            decimal repayment = 0;
            try
            {
                //get interest and max loan year
                sql.Append(getEffectiveRate(type));
                sql.Append(" WHERE BankID = '" + bankID + "' ");
                db.OpenTable(sql.ToString());
                if (db.RecordCount() > 0)
                {
                    Interest = Convert.ToDouble(db.Item("Interest"));
                    Year = ConvertHelper.ConvertToInt(db.Item("Year").ToString(), 0);
                }

                db.Close();


                int Age = YoungerAge(mainAge, coAge);

                //maxYear = (Age >= 70) ? (maxAge = Age) : Year;
                //if (Age < 70)
                //    maxYear = maxAge - Age;

                maxYear = maxAge - Age;

                if (maxYear > Year)
                    maxYear = Year;

                /*
                FORMULA 

                          Interest*Total House loan that u want to calculate(main and co is seperated )			
                <!-- c -->   =  -----------------------------------------
                                1 - (1 + Interest)^(-Year Left to borrow,N)

                double Powerfor_N = -(maxYear * 12);
                double interestforonemonth = Interest / 100 / 12;
                double power = (1 - Math.Pow((1 + interestforonemonth), Powerfor_N));
                repayment = (LoanAmount * Convert.ToDecimal(interestforonemonth)) / Convert.ToDecimal(power);

                *** UPDATED 20190605 ***

                                          interest * (1 + interest)^(period)
                repayment = Original *   -------------------------------------
                                             (1 + interest)^(period) - 1
				
                Note:   Interest must be interest of months (interestOfYear/12)
                        Period must be in months unit (year*12)

                double interestForMonthD = Interest / 100 / 12;
                double periodForMonthD = maxYear * 12;
                double powerD = Math.Pow(1 + interestForMonthD, periodForMonthD);

                decimal interestForMonth = Convert.ToDecimal(interestForMonthD);
                decimal power = Convert.ToDecimal(powerD);

                repayment = LoanAmount * ((interestForMonth * power) / (power - 1));
                */

                double Powerfor_N = -(maxYear * 12);
                double interestforonemonth = Interest / 100 / 12;
                double power = (1 - Math.Pow((1 + interestforonemonth), Powerfor_N));
                repayment = (LoanAmount * Convert.ToDecimal(interestforonemonth)) / Convert.ToDecimal(power);
            }
            catch (Exception ex) { LogUtil.logError(ex.ToString(), sql.ToString()); }
            finally { db.Close(); }

            return repayment;
        }
        public decimal RepaymentFlat(int bankID, decimal LoanAmount, int type) //HirePurchase, Overdraft, Personal Loan
        {
            decimal repayment = 0;
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            try
            {
                decimal Interest = 0;
                int Year = 0;
                //get interest and loan year
                sql.Append(getEffectiveRate(type));
                sql.Append(" WHERE BankId = '" + bankID + "'");
                db.OpenTable(sql.ToString());
                if (db.RecordCount() > 0)
                {
                    Interest = Convert.ToDecimal(db.Item("Interest"));
                    Year = ConvertHelper.ConvertToInt32(db.Item("Year"), 0);
                }
                db.Close();

                /*
                    FORMULA FOR HirePurchase

                                        Interest (amount that saved into database is in percentage form)   +  Total amount 
                    <!-- c -->		= --------------------------------------------------------------------------------------
                                                                How many month used to pay
                                        //$$c_hp_interest/100*$c_hp_year is to obtain one year interest .
                */
                //repayment = ((Interest / 100 * Year) + 1) * LoanAmount / (Year * 12);
                //if OD(type 4), doesn't need to add outstanding amount
                repayment = (((type == 4 ? 0 : LoanAmount) + (LoanAmount * (Interest / 100)) * Year) / (Year * 12));
            }
            catch (Exception ex) { LogUtil.logError(ex.ToString(), sql.ToString()); }
            finally { db.Close(); }
            return repayment;
        }
        public decimal RepaymentMin(int bankID, decimal LoanAmount) //Credit Card
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            decimal repayment = LoanAmount;
            try
            {
                sql.Append("SELECT Minimum from tbl_CreditCard with (nolock) WHERE BankId = '" + bankID + "'");
                db.OpenTable(sql.ToString());
                decimal minimum = Convert.ToDecimal(db.Item("Minimum"));
                repayment = LoanAmount * (minimum / 100);
                repayment = repayment <= KeyVal.MinimumRepayment ? KeyVal.MinimumRepayment : repayment;
            }
            catch (Exception ex) { LogUtil.logError(ex.ToString(), sql.ToString()); }
            finally { db.Close(); }
            return repayment;
        }
        private string GetDateFromIc(string ic)
        {
            //1955-11-22
            int icShortYear = Convert.ToInt32(ic.Substring(0, 2));
            int currentShortYear = Convert.ToInt32(DateTime.Now.Year.ToString().Substring(2, 2));
            int reduceYear = icShortYear > currentShortYear ? -1 : 0;
            string year = (Convert.ToInt32(DateTime.Now.Year.ToString().Substring(0, 2)) + reduceYear).ToString() + ic.Substring(0, 2);
            string month = ic.Substring(2, 2);
            string day = ic.Substring(4, 2);

            return year + "-" + month + "-" + day;
        }
        private decimal CalculateNewLoanRepayment(string bankid, string projectid, string requestid, bool isMainOnly, int tenureP)
        {
            decimal newLoanRepayment = 0;
            decimal margin = 0;
            //decimal policy = 0;
            decimal income = 0;
            decimal loanAmount = 0;
            decimal interest = 0;
            wwdb db = new wwdb();

            margin = PassBankMargin(bankid, projectid, requestid);
            db.OpenTable(getEffectiveRate(6) + " WHERE BankID = '" + bankid + "'");
            interest = ConvertHelper.ConvertToDecimal(db.Item("Interest"), getIntrestRate(6));
            income = CalculateTotalIncome(isMainOnly); // user nett + other to calculate 
            //policy = calculatorController.getBankPolicy(bankid, income, isMainOnly); // use gross income to check bank policy

            if (this.loanAmount != String.Empty && ConvertHelper.ConvertToDecimal(this.loanAmount, 0) != 0)
            {
                loanAmount = ConvertHelper.ConvertToDecimal(this.loanAmount, 0);
            }
            else if (this.loanAmount == String.Empty || ConvertHelper.ConvertToDecimal(this.loanAmount, 0) == 0)
            {
                if (this.purchasePrice != String.Empty && ConvertHelper.ConvertToDecimal(this.purchasePrice, 0) != 0)
                {
                    loanAmount = ConvertHelper.ConvertToDecimal(this.purchasePrice, 0) * margin / 100;
                }
                else if (this.purchasePrice == String.Empty || ConvertHelper.ConvertToDecimal(this.purchasePrice, 0) == 0)
                {
                    newLoanRepayment = 0;//calculatorController.calculateLoanEligible(policy, oldrepayment, tenure, income);
                }
            }
            if (loanAmount > 0)
            {
                newLoanRepayment = calculatorController.calculateNewRepayment(loanAmount, tenureP, interest);
            }
            else
            {
                newLoanRepayment = 0;
            }

            return newLoanRepayment;
        }
        public decimal CalculateTotalOldBankRepayments(string bankid, string requestid, bool isMainOnly = true)
        {
            decimal totalOldRepayments = 0;

            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            try
            {
                sql.Clear();
                sql.Append(" Select t.BankID, SUM(t.repayment) AS 'Repayment' ");
                sql.Append(" FROM tbl_TLoanDetail t with (nolock) ");
                sql.Append(" INNER JOIN tbl_BankName b WITH(NOLOCK) ON b.BankID = t.BankID AND b.Status = 'A' ");
                sql.Append(" WHERE t.requestid= N'" + secure.RC(requestid) + "' AND t.BankID = N'" + secure.RC(bankid) + "' ");
                if (isMainOnly == true)
                    sql.Append(" AND t.isMain = 'true' ");
                sql.Append(" GROUP BY t.requestID, t.BankID ");
                db.OpenTable(sql.ToString());

                if (db.RecordCount() > 0)
                {
                    totalOldRepayments = ConvertHelper.ConvertToDecimal(db.Item("Repayment"), 0);
                }
            }
            catch (Exception ex) { LogUtil.logError(ex.ToString(), sql.ToString()); }
            finally { db.Close(); }

            return totalOldRepayments;

        }
        private decimal CalculateTotalIncome(bool isMainOnly = true)
        {
            decimal totalIncome = 0;

            decimal mainOtherIncome = 0;
            decimal mainNetIncome = 0;

            mainNetIncome = ConvertHelper.ConvertToDecimal(this.mainNetIncome, 0);
            mainOtherIncome = ConvertHelper.ConvertToDecimal(this.mainOtherIncome, 0);

            totalIncome = mainNetIncome + mainOtherIncome;

            if (!isMainOnly)
            {
                decimal coNetIncome = ConvertHelper.ConvertToDecimal(this.coNetIncome, 0);
                decimal coOtherIncome = ConvertHelper.ConvertToDecimal(this.coOtherIncome, 0);
                totalIncome += coNetIncome + coOtherIncome;
            }

            return totalIncome;
        }
        private decimal CalculateGrossIncomePolicy()
        {
            decimal totalIncome = 0;
            decimal mainGrossIncome = 0;
            decimal mainOtherIncome = 0;
            decimal mainNetIncome = 0;
            decimal coNetIncome = 0;
            decimal coOtherIncome = 0;
            decimal coGrossIncome = 0;
            decimal grossIncome = 0;
            decimal totalSalary = 0;

            mainGrossIncome = ConvertHelper.ConvertToDecimal(grossIncome, 0);
            mainNetIncome = ConvertHelper.ConvertToDecimal(this.mainNetIncome, 0);
            mainOtherIncome = ConvertHelper.ConvertToDecimal(this.mainOtherIncome, 0);
            coNetIncome = ConvertHelper.ConvertToDecimal(this.coNetIncome, 0);
            coOtherIncome = ConvertHelper.ConvertToDecimal(this.coOtherIncome, 0);

            grossIncome = mainGrossIncome + coGrossIncome;
            totalSalary = mainOtherIncome + mainNetIncome + coOtherIncome + coNetIncome;

            if (grossIncome != 0)
            {
                totalIncome = grossIncome;
            }
            else
            {
                totalIncome = totalSalary;
            }


            return totalIncome;
        }
        #endregion

        #region Function
        public void removeInvalidChar(object object1 = null, object object2 = null, object object3 = null)
        {
            if (object1 != null)
            {
                if (object1 is TextBox)
                {
                    TextBox textBox = (TextBox)object1;
                    textBox.Text = textBox.Text.Replace("'", "").Replace(",", "").Replace("#", "").Replace("*", "")
                        .Replace("^", "").Replace("%", "").Replace("#", "").Replace("*", "").Replace("^", "").Replace("&", "").Replace("(", "")
                        .Replace(")", "").Replace("{", "").Replace("}", "").Replace("[", "").Replace("]", "").Replace("_", "").Replace("=", "")
                        .Replace("+", "").Replace(":", "").Replace(";", "").Replace("|", "").Replace("\\", "").Replace("?", "").Replace("`", "")
                        .Replace("~", "").Replace("<", "").Replace(">", "").Replace(" ", "").Replace("-", "");
                }
                else if (object1 is string)
                {
                    string value = (string)object1;
                    value = value.Replace("'", "").Replace(",", "").Replace("#", "").Replace("*", "")
                        .Replace("^", "").Replace("%", "").Replace("#", "").Replace("*", "").Replace("^", "").Replace("&", "").Replace("(", "")
                        .Replace(")", "").Replace("{", "").Replace("}", "").Replace("[", "").Replace("]", "").Replace("_", "").Replace("=", "")
                        .Replace("+", "").Replace(":", "").Replace(";", "").Replace("|", "").Replace("\\", "").Replace("?", "").Replace("`", "")
                        .Replace("~", "").Replace("<", "").Replace(">", "").Replace(" ", "").Replace("-", "");
                    this.value1 = value;
                }
            }
            if (object2 != null)
            {
                if (object2 is TextBox)
                {
                    TextBox textBox = (TextBox)object2;
                    textBox.Text = textBox.Text.Replace("'", "").Replace(",", "").Replace("#", "").Replace("*", "")
                        .Replace("^", "").Replace("%", "").Replace("#", "").Replace("*", "").Replace("^", "").Replace("&", "").Replace("(", "")
                        .Replace(")", "").Replace("{", "").Replace("}", "").Replace("[", "").Replace("]", "").Replace("_", "").Replace("=", "")
                        .Replace("+", "").Replace(":", "").Replace(";", "").Replace("|", "").Replace("\\", "").Replace("?", "").Replace("`", "")
                        .Replace("~", "").Replace("<", "").Replace(">", "");
                }
                else if (object2 is string)
                {
                    string value = (string)object2;
                    value = value.Replace("'", "").Replace(",", "").Replace("#", "").Replace("*", "")
                        .Replace("^", "").Replace("%", "").Replace("#", "").Replace("*", "").Replace("^", "").Replace("&", "").Replace("(", "")
                        .Replace(")", "").Replace("{", "").Replace("}", "").Replace("[", "").Replace("]", "").Replace("_", "").Replace("=", "")
                        .Replace("+", "").Replace(":", "").Replace(";", "").Replace("|", "").Replace("\\", "").Replace("?", "").Replace("`", "")
                        .Replace("~", "").Replace("<", "").Replace(">", "");
                    this.value2 = value;
                }
            }
            if (object3 != null)
            {
                if (object3 is TextBox)
                {
                    TextBox textBox = (TextBox)object3;
                    textBox.Text = textBox.Text.Replace("'", "").Replace(",", "").Replace("#", "").Replace("*", "")
                        .Replace("^", "").Replace("#", "").Replace("*", "").Replace("^", "").Replace("{", "").Replace("}", "").Replace("[", "")
                        .Replace("]", "").Replace("_", "").Replace(";", "").Replace("|", "").Replace("\\", "").Replace("?", "").Replace("`", "")
                        .Replace("~", "").Replace("<", "").Replace(">", "");
                }
                else if (object3 is string)
                {
                    string value = (string)object3;
                    value = value.Replace("'", "").Replace(",", "").Replace("#", "").Replace("*", "")
                        .Replace("^", "").Replace("#", "").Replace("*", "").Replace("^", "").Replace("{", "").Replace("}", "").Replace("[", "")
                        .Replace("]", "").Replace("_", "").Replace(";", "").Replace("|", "").Replace("\\", "").Replace("?", "").Replace("`", "")
                        .Replace("~", "").Replace("<", "").Replace(">", "");
                    this.value3 = value;
                }
            }
        }
        public int getAgeFromIC(string IC)
        {
            //int birthYear = ConvertHelper.ConvertToInt(txtIC.Text.Remove(1, txtIC.Text.Length-1), 0);
            //int currentYear = ConvertHelper.ConvertToInt(DateTime.Now.Year.ToString(), 2019);

            int age = (ConvertHelper.ConvertToInt(DateTime.Now.Year.ToString(), 2019) -
                ConvertHelper.ConvertToInt(GetDateFromIc(IC).Substring(0, 4), 0));

            return age;
        }
        public string removeComma(TextBox textBox)
        {
            return textBox.Text.Replace(",", "");
        }

        async private Task<HttpWebResponse> AuthenticatedCtosRequest(HttpWebRequest req)
        {
            HttpWebResponse res = null;

            try
            {
                var tokens = await _ctosAuthService.Login().ConfigureAwait(false);

                if (tokens?.AccessToken == null)
                {
                    throw new Exception("CTOS Login API failed");
                }

                res = PostCtos(tokens.AccessToken, req);

                if (res.StatusCode != HttpStatusCode.OK)
                {
                    #region refresh logic
                    // refresh login coud be reused in future
                    //else if (res.StatusCode == HttpStatusCode.BadRequest || res.StatusCode == HttpStatusCode.Unauthorized)
                    //{
                    //    var refreshedTokens = await _ctosAuthService.RefreshLogin().ConfigureAwait(false);
                    //    res = PostCtos(refreshedTokens.AccessToken, req);

                    //    if (res.StatusCode == HttpStatusCode.OK) { }
                    //    else if (res.StatusCode == HttpStatusCode.BadRequest || res.StatusCode == HttpStatusCode.Unauthorized)
                    //    {
                    //        var newTokens = await _ctosAuthService.Login().ConfigureAwait(false);
                    //        res = PostCtos(newTokens.AccessToken, req);

                    //        if (res.StatusCode != HttpStatusCode.OK)
                    //        {
                    //            using (var reader = new StreamReader(res.GetResponseStream()))
                    //            {
                    //                LogUtil.logError($"CtosAuthenticationService.cs GetValidAccessToken newTokens {res.StatusCode} {reader.ReadToEnd()}", "");
                    //            }
                    //        }
                    //    }
                    //    else
                    //    {
                    //        using (var reader = new StreamReader(res.GetResponseStream()))
                    //        {
                    //            LogUtil.logError($"CtosAuthenticationService.cs GetValidAccessToken refreshedTokens {res.StatusCode} {reader.ReadToEnd()}", "");
                    //        }
                    //    }
                    //}
                    #endregion
                    
                    using (var reader = new StreamReader(res.GetResponseStream()))
                    {
                        LogUtil.logError($"CtosAuthenticationService.cs GetValidAccessToken dbTokens {res.StatusCode} {reader.ReadToEnd()}", "");
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtil.logError($"CtosAuthenticationService.cs GetValidAccessToken {ex.Message}", "");
            }

            return res;
        }

        private HttpWebResponse PostCtos(string accessToken, HttpWebRequest req)
        {
            req.Headers.Add("Authorization", $"Bearer {accessToken}");
            var res = (HttpWebResponse)req.GetResponse();

            return res;
        }
        #endregion
    }
}