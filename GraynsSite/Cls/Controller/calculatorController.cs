﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Synergy.Helper;
using Synergy.Util;

namespace Synergy.Controller
{
    public  class calculatorController
    {
        
     
        /// <param name="totalOldRepayment">Sum Of Applicant Old Repayment</param>
        /// <param name="totalNewRepayment">Sum Of Applicant New Repayment</param>
        /// <param name="bankPolicy">Percent of allowed loan amount on total income</param>

        public static decimal calculateIncomeRequired(decimal bankPolicy, decimal totalOldRepayment, decimal totalNewRepayment)
        {
            decimal incomeRequired = 0;
            wwdb db = new wwdb();

            incomeRequired = ConvertHelper.ConvertToDecimal(db.executeScalar("select dbo.Func_CalculateIncomeRequired('" + bankPolicy + "','" + totalOldRepayment + "','" + totalNewRepayment + "')"), 0);


            return incomeRequired;
        }

        /// <param name="bankPolicy">Percent of allowed loan amount on total income</param>
        /// <param name="totalOldRepayment">Sum Of Applicant Old Repayment</param>
        /// <param name="tenure"> Tenure in Year Unit</param>
        /// <param name="salary"> Salary of Applicant</param>
        public static decimal calculateLoanEligible(decimal bankPolicy, decimal totalOldRepayment, decimal Age, decimal salary, decimal tenure, string bankID)
        {
            decimal LoanEligible = 0;
            wwdb db = new wwdb();

            LoanEligible = ConvertHelper.ConvertToDecimal(db.executeScalar("select dbo.Func_CalculateLoanEligible('" + bankPolicy + "','" + totalOldRepayment + "','" + Age + "','" + salary + "','" + tenure + "','" + bankID + "')"), 0);


            return LoanEligible;
        }


        /// <param name="totalOldRepayment">Sum Of Applicant Old Repayment</param>
        /// <param name="totalNewRepayment"> New Repayment</param>
        /// <param name="totalSalary"> Total Salary Of Both Applicant</param>
        public static decimal calculateDSR(decimal totalOldRepayment, decimal totalNewRepayment, decimal totalSalary)
        {
            decimal DSR = 0;
            wwdb db = new wwdb();

            DSR = ConvertHelper.ConvertToDecimal(db.executeScalar("select dbo.Func_CalculateLoanDSR('" + totalOldRepayment + "','" + totalNewRepayment + "','" + totalSalary + "')"), 0);

            return DSR;
        }

        /// <param name="loanAmount">Loan Amount  </param>
        /// <param name="tenure"> Max Loan Year</param>
        /// <param name="interest"> Interest in Percent </param>
        public static decimal calculateNewRepayment(decimal loanAmount, decimal tenure, decimal interest)
        {
            decimal NewRepayment = 0;
            wwdb db = new wwdb();

            NewRepayment = ConvertHelper.ConvertToDecimal(db.executeScalar("select dbo.Func_CalculateNewRepayment('" + loanAmount + "' , '" + tenure + "' , '" + interest + "')"), 0);

            return NewRepayment;
        }


        /// <param name="Bank">Bank ID  </param>
        /// <param name="income"> Applicant Income </param>
        /// <param name="OnlyMain"> For Main Applicant only  </param>
        public static decimal getBankPolicy(string Bank, decimal income, bool OnlyMain = false)
        {
            decimal bankPolicy = 0;
            wwdb db = new wwdb();

            bankPolicy = ConvertHelper.ConvertToDecimal(db.executeScalar("select dbo.Func_GetBankPolicy('" + income + "','" + Bank + "','" + OnlyMain + "')"), 0);

            return bankPolicy;
        }



    }
}