﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.IO;
using System.Data;
using System.Resources;
using System.Globalization;
using System.Xml;

namespace Synergy.Util
{
    public class LanguageUtil
    {
        public static String lang_folder = "App_GlobalResources";
        public static String default_resource_file = "resource.resx";
        public static String default_language_file = "language.resx";

        public static DataTable LoadResourceFiles()
        {
            string resourcespath = HttpContext.Current.Request.PhysicalApplicationPath + lang_folder;
            DirectoryInfo dirInfo = new DirectoryInfo(resourcespath);
            DataTable dt = new DataTable();
            dt.Columns.Add("FileName");
            foreach (FileInfo fileInfo in dirInfo.GetFiles())
            {
                dt.Rows.Add(fileInfo.Name);
            }
            return dt;
        }

        public static DataTable LoadLanguageResources(String filename)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Key");
            dt.Columns.Add("Value");
            string filePath = HttpContext.Current.Request.PhysicalApplicationPath + lang_folder + "\\" + filename;
            Stream stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
            ResXResourceReader RrX = new ResXResourceReader(stream);
            IDictionaryEnumerator RrEn = RrX.GetEnumerator();
            //SortedList list = new SortedList();
            while (RrEn.MoveNext())
            {
                dt.Rows.Add(RrEn.Key, RrEn.Value);
            }
            RrX.Close();
            stream.Dispose();
            return dt;
        }

        public static String LoadSingleResourceValue(String filename, String key)
        {
            String filePath = HttpContext.Current.Request.PhysicalApplicationPath + lang_folder + "\\" + filename;
            ResXResourceSet rset = new ResXResourceSet(filePath);

            return rset.GetString(key);
        }

    }
}