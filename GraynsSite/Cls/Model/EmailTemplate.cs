﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


/// <summary>
/// Summary description for EmailTemplate
/// </summary>
/// 
namespace Synergy.Model
{
    public class EmailTemplate
    {
        public EmailTemplate()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        ///
        /// Gets or Sets RowID
        ///
        public int RowID
        {
            get { return _RowID; }
            set { _RowID = value; }
        }
        private int _RowID;

        ///
        /// Gets or Sets Sender
        ///
        public string Sender
        {
            get { return _Sender; }
            set { _Sender = value; }
        }
        private string _Sender;
        ///
        /// Gets or Sets Subject
        ///
        public string Subject
        {
            get { return _Subject; }
            set { _Subject = value; }
        }
        private string _Subject;

        ///
        /// Gets or Sets Message
        ///
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        private string _Message;

        ///
        /// Gets or Sets emailType
        ///
        public int emailType
        {
            get { return _emailType; }
            set { _emailType = value; }
        }
        private int _emailType;

        ///
        /// Gets or Sets isActive
        ///
        public bool isActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }
        private bool _isActive;

        ///
        /// Gets or Sets isDeleted
        ///
        public bool isDeleted
        {
            get { return _isDeleted; }
            set { _isDeleted = value; }
        }
        private bool _isDeleted;

        ///
        /// Gets or Sets CreatedBy
        ///
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        private string _CreatedBy;

        ///
        /// Gets or Sets CreatedAt
        ///
        public DateTime CreatedAt
        {
            get { return _CreatedAt; }
            set { _CreatedAt = value; }
        }
        private DateTime _CreatedAt;

        ///
        /// Gets or Sets UpdatedBy
        ///
        public string UpdatedBy
        {
            get { return _UpdatedBy; }
            set { _UpdatedBy = value; }
        }
        private string _UpdatedBy;

        ///
        /// Gets or Sets UpdatedAt
        ///
        public DateTime UpdatedAt
        {
            get { return _UpdatedAt; }
            set { _UpdatedAt = value; }
        }
        private DateTime _UpdatedAt;

    }
}