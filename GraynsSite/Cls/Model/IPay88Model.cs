﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HJT.Cls.Model
{
    public class IPay88Model
    {
        public string MerchantCode { get; set; }

        public string MerchantKey { get; set; }

        public string PaymentId { get; set; }

        public string RefNo { get; set; }

        public string Amount { get; set; }

        public string Currency { get; set; }

        public string Lang { get; set; }

        public string SignatureType { get; set; }

        public string Signature { get; set; }

        public string ResponseURL { get; set; }

        public string BackendURL { get; set; }

        public string Status { get; set; }

        public string Remark { get; set; }

        public string TransId { get; set; }

        public string AuthCode { get; set; }

        public string ErrDesc { get; set; }

        public string CCName { get; set; }

        public string CCNo { get; set; }

        public string SBankname { get; set; }

        public string SCountry { get; set; }

        public string ProdDesc { get; set; }

        public string UserName { get; set; }

        public string UserEmail { get; set; }

        public string UserContact { get; set; }
    }
}