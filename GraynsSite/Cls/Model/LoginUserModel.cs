﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using Synergy.Helper;
using Synergy.Util;

namespace Synergy.Model
{
    public class LoginUserModel
    {
        error errormessage = new error();
        private string userId;
        private string displayName;
        private string name;
        private string role;
        private string rankName;
        private int rankID;
        private string loginMessage;
        private string stockist;

        public List<UserAccess> UserAccessList { get; set; }

        public List<string> UserRole { get; set; }

        public List<string> UserType { get; set; }

        public string UserId
        {
            get { return userId; }
            set { userId = value; }
        }

        public string DisplayName
        {
            get { return displayName; }
            set { displayName = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Role
        {
            get { return role; }
            set { role = value; }
        }

        public string RankName
        {
            get { return rankName; }
            set { rankName = value; }
        }

        public int RankID
        {
            get { return rankID; }
            set { rankID = value; }
        }

        public string Stockist
        {
            get { return stockist; }
            set { stockist = value; }
        }

        public string LoginMessage
        {
            get { return loginMessage; }
        }

        public string Mobile
        {
            get { return _mobile; }
            set { _mobile = value; }
        }
        private string _mobile;

        public string OutletCode
        {
            get { return _OutletCode; }
            set { _OutletCode = value; }
        }
        private string _OutletCode;

        public string CountryID
        {
            get { return _CountryID; }
            set { _CountryID = value; }
        }
        private string _CountryID;
        public string MemberStatus
        {
            get { return _MemberStatus; }
            set { _MemberStatus = value; }
        }
        private string _MemberStatus;


        public string BranchUniqueID
        {
            get { return _BranchUniqueID; }
            set { _BranchUniqueID = value; }
        }
        private string _BranchUniqueID;

        public string BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }
        private string _BranchID;
        
        public Boolean VerifyLogin(string id, string password)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            bool result = true;

            try
            {
                //edit on 11/11/2014 use user name login
                sql.Append(@"   SELECT b.memberID, b.userName,b.fullName, a.login_role, r.RoleName As 'UserType', b.Status AS 'MemberStatus' , a.branchID, br.ID as 'BranchUniqueId'
                                FROM tbl_login a WITH (NOLOCK)
                                inner JOIN tbl_memberinfo b WITH (NOLOCK) ON a.login_id = b.memberID    
                                LEFT JOIN tbl_branch br WITH(NOLOCK) ON a.branchid = br.branchid     
                                LEFT JOIN tbl_RoleControl r WITH (NOLOCK) ON r.RoleID = b.Roles
                                WHERE b.username = N'" + secure.RC(id) + "' AND (a.login_password = N'" + secure.RC(secure.Encrypt(password, true)) + "') AND a.login_status = '1';");

                db.OpenTable(sql.ToString());

                //edit on 29/7/13
                if (db.RecordCount() == 1)
                {
                    BranchUniqueID = db.Item("branchUniqueId").ToString();
                    BranchID = db.Item("branchID").ToString();
                    UserId = db.Item("memberID").ToString();
                    Name = db.Item("fullName").ToString();
                    Role = db.Item("login_role").ToString();
                    MemberStatus = db.Item("MemberStatus").ToString();

                    //UserRole
                    UserRole = new List<string>();
                    UserType = new List<string>();
                    string userRoleSQL = @"select RoleCode from tbl_MemberRole WHERE MemberId = '" + UserId + "'";
                    wwdb db3 = new wwdb();
                    db3.OpenTable(userRoleSQL);
                    if (db3.RecordCount() > 0)
                    {
                        for (int h = 0; h < db3.RecordCount(); h++)
                        {
                            UserRole.Add("'" + secure.RC(db3.Item("RoleCode").ToString()) + "'");

                            switch (secure.RC(db3.Item("RoleCode").ToString()))
                            {
                                case "AD":
                                    UserType.Add("ADMIN");
                                    break;
                                case "DP":
                                    UserType.Add("DEVELOPER");
                                    break;
                                case "RE":
                                    UserType.Add("REALESTATE");
                                    break;
                                case "ES":
                                    UserType.Add("ESTATE");
                                    break;
                                case "ID":
                                    UserType.Add("INDIVIDUAL");
                                    break;
                                case "SF":
                                    UserType.Add("STAFF");
                                    break;
                                case "CA":
                                    UserType.Add("CREDITADVISOR");
                                    break;
                                case "CS":
                                    UserType.Add("CARSALEADVISOR");
                                    break;
                                case "AM":
                                    UserType.Add("ACCOUNTMANAGER");
                                    break;
                                case "US":
                                    UserType.Add("USER");
                                    break;
                                case "TU":
                                    UserType.Add("TESTUSER");
                                    break;
                                case "SU":
                                    UserType.Add("SPEEDUSER");
                                    break;
                                default:
                                    UserType.Add("USER");
                                    break;
                            }

                            if (!db3.Eof())
                            {
                                db3.MoveNext();
                            }
                        }
                    }


                    string SQL = @"select distinct [type],linkurl,[key] from tbl_rolemenu rm inner join tbl_menu m on m.rowid = rm.menuid where rm.roleid IN (SELECT RoleCode FROM tbl_MemberRole WHERE MemberId = '" + UserId + "')";
                    wwdb db2 = new wwdb();
                    try
                    {
                        db2.OpenTable(SQL);
                        if (db2.RecordCount() > 0)
                        {
                            UserAccessList = new List<UserAccess>();
                            for (int h = 0; h < db2.RecordCount(); h++)
                            {
                                int type = Convert.ToInt32(db2.Item("type").ToString());
                                string linkurl = db2.Item("linkurl").ToString();
                                string key = db2.Item("key").ToString();

                                UserAccessList.Add(new UserAccess { Type = type, LinkUrl = linkurl, Key = key });

                                if (!db2.Eof())
                                {
                                    db2.MoveNext();
                                }

                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        loginMessage = "Error message:" + ex.ToString();
                        LogUtil.logError(ex.ToString(), SQL.ToString());
                    }
                    finally { db2.Close(); }
                }
                else
                {
                    result = false;
                    loginMessage = errormessage.AlertText("Incorrect username or password.");
                }
            }
            catch (Exception ex)
            {
                result = false;
                loginMessage = "Error message:" + ex.ToString();
                LogUtil.logError(ex.ToString(), sql.ToString());
            }
            finally
            {
                db.Close();
            }

            return result;
        }

        public bool getMemberInfo(string id)
        {
            wwdb db = new wwdb();
            StringBuilder sql = new StringBuilder();
            bool result = true;

            if (HttpContext.Current.Session[KeyVal.session_clogin] != null)
            {
                LoginUserModel cLogin = (LoginUserModel)HttpContext.Current.Session[KeyVal.session_clogin];

                try
                {
                    sql.Append(@" SELECT b.memberID, b.userName,b.fullName, b.registerCountry, d.country_name,a.BranchID ,a.login_role, b.ranking, c.rankName, isStockist, Mobile, IsNull(OutletCode, '') as OutletCode
                                  FROM tbl_login a WITH (NOLOCK) 
                                  LEFT JOIN tbl_memberinfo b WITH (NOLOCK) ON a.login_id = b.memberID 
                                  LEFT JOIN tbl_rank c WITH (NOLOCK) ON b.ranking = c.ranking
                                  LEFT JOIN tbl_country d WITH (NOLOCK) ON b.registerCountry = d.id WHERE a.login_id = N'" + secure.RC(id) + "' AND a.login_status = '1'; ");



                    db.OpenTable(sql.ToString());

                    if (db.RecordCount() == 1)
                    {
                        UserId = db.Item("memberID").ToString();
                        Name = db.Item("fullName").ToString();
                        CountryID = db.Item("registerCountry");
                        BranchID = db.Item("BranchID");
                        Role = db.Item("login_role").ToString();
                        RankName = db.Item("rankName").ToString();
                        RankID = ConvertHelper.ConvertToInt32(db.Item("ranking"), 0);
                        Stockist = db.Item("isStockist").ToString();
                        Mobile = db.Item("Mobile").ToString();
                        OutletCode = db.Item("OutletCode").ToString();
                        UserRole = new List<string>();
                        UserType = new List<string>();
                        string userRoleSQL = @"select RoleCode from tbl_MemberRole WHERE MemberId = '" + UserId + "'";
                        wwdb db3 = new wwdb();
                        db3.OpenTable(userRoleSQL);
                        if (db3.RecordCount() > 0)
                        {
                            for (int h = 0; h < db3.RecordCount(); h++)
                            {
                                UserRole.Add("'" + secure.RC(db3.Item("RoleCode").ToString()) + "'");

                                switch (secure.RC(db3.Item("RoleCode").ToString()))
                                {
                                    case "AD":
                                        UserType.Add("ADMIN");
                                        break;
                                    case "DP":
                                        UserType.Add("DEVELOPER");
                                        break;
                                    case "RE":
                                        UserType.Add("REALESTATE");
                                        break;
                                    case "ES":
                                        UserType.Add("ESTATE");
                                        break;
                                    case "ID":
                                        UserType.Add("INDIVIDUAL");
                                        break;
                                    case "SF":
                                        UserType.Add("STAFF");
                                        break;
                                    case "CA":
                                        UserType.Add("CREDITADVISOR");
                                        break;
                                    case "CS":
                                        UserType.Add("CARSALEADVISOR");
                                        break;
                                    case "AM":
                                        UserType.Add("ACCOUNTMANAGER");
                                        break;
                                    case "US":
                                        UserType.Add("USER");
                                        break;
                                    case "TU":
                                        UserType.Add("TESTUSER");
                                        break;
                                    default:
                                        UserType.Add("USER");
                                        break;
                                }

                                if (!db3.Eof())
                                {
                                    db3.MoveNext();
                                }
                            }
                        }

                        LogUtil.logLogin(cLogin.UserId + " - " + id, "ADMIN", "Success");
                    }
                    else
                    {
                        result = false;
                        loginMessage = "Incorrect username or password.";

                        LogUtil.logLogin(cLogin.UserId + " - " + id, "ADMIN", "Failed");
                    }
                }
                catch (Exception ex)
                {
                    LogUtil.logLogin(cLogin.UserId + " - " + id, "ADMIN", "Failed");
                    result = false;
                    loginMessage = "Unknown Error message: " + ex.ToString();
                    LogUtil.logError(ex.ToString(), sql.ToString());
                }
                finally
                {
                    db.Close();
                }
            }
            else
            {
                result = false;
            }

            return result;
        }

        public bool isStaffAdmin()
        {
            if (UserType.Contains(KeyVal.Admin) || UserType.Contains(KeyVal.Staff))
                return true;
            else
                return false;
        }


        public bool isBranchManager()
        {
            if (UserType.Contains(KeyVal.AccountManager))
                return true;
            else
                return false;
        }

        public bool HaveShorcutMenu()
        {
            //if (userType.ToUpper().Equals(KeyVal.Developer.ToUpper())
            //    || userType.ToUpper().Equals(KeyVal.Admin.ToUpper())
            //    || userType.ToUpper().Equals(KeyVal.AccountManager.ToUpper())
            //    )
            //    return true;
            //else
            //    return false;

            return true;
        }

        public bool isDeveloper()
        {
            if (UserType.Contains(KeyVal.Developer.ToUpper()))
                return true;
            else
                return false;
        }
        public bool isRealEstate()
        {
            if (UserType.Contains(KeyVal.RealEstate.ToUpper()))
                return true;
            else
                return false;
        }
        public bool isEstate()
        {
            if (UserType.Contains(KeyVal.Estate.ToUpper()))
                return true;
            else
                return false;
        }
        public bool isIndividual()
        {
            if (UserType.Contains(KeyVal.Individual.ToUpper()))
                return true;
            else
                return false;
        }
        public bool isCreditAdvisor()
        {
            if (UserType.Contains(KeyVal.CreditAdvisor.ToUpper()))
                return true;
            else
                return false;
        }
        public bool isCarSaleAdvisor()
        {
            if (UserType.Contains(KeyVal.CarSaleAdvisor.ToUpper()))
                return true;
            else
                return false;
        }

        public bool isAccountManager()
        {
            if (UserType.Contains(KeyVal.AccountManager.ToUpper()))
                return true;
            else
                return false;
        }
        public bool isUser()
        {
            if (UserType.Contains(KeyVal.User.ToUpper()))
                return true;
            else
                return false;
        }
        public bool isTestUser()
        {
            if (UserType.Contains(KeyVal.TestUser.ToUpper()))
                return true;
            else
                return false;
        }

        public bool isSpeedUser()
        {
            if (UserType.Contains(KeyVal.SpeedUser.ToUpper()))
                return true;
            else
                return false;
        }
    }


    public class UserAccess
    {
        public int Type { get; set; }
        public string LinkUrl { get; set; }
        public string Key { get; set; }
    }
}