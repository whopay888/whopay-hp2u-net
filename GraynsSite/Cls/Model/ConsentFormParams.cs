﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HJT.Cls.Model
{
    public class ConsentFormParams
    {
        public string MainApplicantIC { get; set; }
        public string MainApplicantName { get; set; }
        public string MainApplicantSignature { get; set; }
        public string CoApplicantIC { get; set; }
        public string CoApplicantName { get; set; }
        public string CoApplicantSignature { get; set; }
    }
}