﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace HJT.Cls.Model
{
    [Serializable()]
    public class XmlModel
    {
        public class LOAN
        {

            // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://ws.cmctos.com.my/ctosnet/response", IsNullable = false)]
            public partial class report
            {

                private reportEnq_report enq_reportField;

                private reportTrex trexField;

                private string versionField;

                /// <remarks/>
                public reportEnq_report enq_report
                {
                    get
                    {
                        return this.enq_reportField;
                    }
                    set
                    {
                        this.enq_reportField = value;
                    }
                }

                /// <remarks/>
                public reportTrex trex
                {
                    get
                    {
                        return this.trexField;
                    }
                    set
                    {
                        this.trexField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string version
                {
                    get
                    {
                        return this.versionField;
                    }
                    set
                    {
                        this.versionField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_report
            {

                private reportEnq_reportHeader headerField;

                private reportEnq_reportSummary summaryField;

                private reportEnq_reportEnquiry enquiryField;

                private string idField;

                private string report_typeField;

                private string titleField;

                /// <remarks/>
                public reportEnq_reportHeader header
                {
                    get
                    {
                        return this.headerField;
                    }
                    set
                    {
                        this.headerField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportSummary summary
                {
                    get
                    {
                        return this.summaryField;
                    }
                    set
                    {
                        this.summaryField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquiry enquiry
                {
                    get
                    {
                        return this.enquiryField;
                    }
                    set
                    {
                        this.enquiryField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string id
                {
                    get
                    {
                        return this.idField;
                    }
                    set
                    {
                        this.idField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string report_type
                {
                    get
                    {
                        return this.report_typeField;
                    }
                    set
                    {
                        this.report_typeField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string title
                {
                    get
                    {
                        return this.titleField;
                    }
                    set
                    {
                        this.titleField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportHeader
            {

                private reportEnq_reportHeaderUser userField;

                private reportEnq_reportHeaderCompany companyField;

                private string accountField;

                private object telField;

                private object faxField;

                private System.DateTime enq_dateField;

                private System.DateTime enq_timeField;

                private reportEnq_reportHeaderEnq_status enq_statusField;

                /// <remarks/>
                public reportEnq_reportHeaderUser user
                {
                    get
                    {
                        return this.userField;
                    }
                    set
                    {
                        this.userField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportHeaderCompany company
                {
                    get
                    {
                        return this.companyField;
                    }
                    set
                    {
                        this.companyField = value;
                    }
                }

                /// <remarks/>
                public string account
                {
                    get
                    {
                        return this.accountField;
                    }
                    set
                    {
                        this.accountField = value;
                    }
                }

                /// <remarks/>
                public object tel
                {
                    get
                    {
                        return this.telField;
                    }
                    set
                    {
                        this.telField = value;
                    }
                }

                /// <remarks/>
                public object fax
                {
                    get
                    {
                        return this.faxField;
                    }
                    set
                    {
                        this.faxField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
                public System.DateTime enq_date
                {
                    get
                    {
                        return this.enq_dateField;
                    }
                    set
                    {
                        this.enq_dateField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlElementAttribute(DataType = "time")]
                public System.DateTime enq_time
                {
                    get
                    {
                        return this.enq_timeField;
                    }
                    set
                    {
                        this.enq_timeField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportHeaderEnq_status enq_status
                {
                    get
                    {
                        return this.enq_statusField;
                    }
                    set
                    {
                        this.enq_statusField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportHeaderUser
            {

                private string idField;

                private string valueField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string id
                {
                    get
                    {
                        return this.idField;
                    }
                    set
                    {
                        this.idField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlTextAttribute()]
                public string Value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportHeaderCompany
            {

                private string idField;

                private string valueField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string id
                {
                    get
                    {
                        return this.idField;
                    }
                    set
                    {
                        this.idField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlTextAttribute()]
                public string Value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportHeaderEnq_status
            {

                private string codeField;

                private string valueField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string code
                {
                    get
                    {
                        return this.codeField;
                    }
                    set
                    {
                        this.codeField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlTextAttribute()]
                public string Value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportSummary
            {

                private reportEnq_reportSummaryEnq_sum enq_sumField;

                /// <remarks/>
                public reportEnq_reportSummaryEnq_sum enq_sum
                {
                    get
                    {
                        return this.enq_sumField;
                    }
                    set
                    {
                        this.enq_sumField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportSummaryEnq_sum
            {

                private string nameField;

                private object ic_lcnoField;

                private ulong nic_brnoField;

                private ushort statField;

                private ushort dd_indexField;

                private object mphone_nosField;

                private object ref_noField;

                private string dist_codeField;

                private reportEnq_reportSummaryEnq_sumPurpose purposeField;

                private string include_consentField;

                private string include_ctosField;

                private string include_trexField;

                private reportEnq_reportSummaryEnq_sumInclude_ccris include_ccrisField;

                private string include_dcheqField;

                private string include_ficoField;

                private string include_ssmField;

                private uint confirm_entityField;

                private reportEnq_reportSummaryEnq_sumEnq_status enq_statusField;

                private reportEnq_reportSummaryEnq_sumEnq_code enq_codeField;

                private string ptypeField;

                private string pcodeField;

                private string seqField;

                /// <remarks/>
                public string name
                {
                    get
                    {
                        return this.nameField;
                    }
                    set
                    {
                        this.nameField = value;
                    }
                }

                /// <remarks/>
                public object ic_lcno
                {
                    get
                    {
                        return this.ic_lcnoField;
                    }
                    set
                    {
                        this.ic_lcnoField = value;
                    }
                }

                /// <remarks/>
                public ulong nic_brno
                {
                    get
                    {
                        return this.nic_brnoField;
                    }
                    set
                    {
                        this.nic_brnoField = value;
                    }
                }

                /// <remarks/>
                public ushort stat
                {
                    get
                    {
                        return this.statField;
                    }
                    set
                    {
                        this.statField = value;
                    }
                }

                /// <remarks/>
                public ushort dd_index
                {
                    get
                    {
                        return this.dd_indexField;
                    }
                    set
                    {
                        this.dd_indexField = value;
                    }
                }

                /// <remarks/>
                public object mphone_nos
                {
                    get
                    {
                        return this.mphone_nosField;
                    }
                    set
                    {
                        this.mphone_nosField = value;
                    }
                }

                /// <remarks/>
                public object ref_no
                {
                    get
                    {
                        return this.ref_noField;
                    }
                    set
                    {
                        this.ref_noField = value;
                    }
                }

                /// <remarks/>
                public string dist_code
                {
                    get
                    {
                        return this.dist_codeField;
                    }
                    set
                    {
                        this.dist_codeField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportSummaryEnq_sumPurpose purpose
                {
                    get
                    {
                        return this.purposeField;
                    }
                    set
                    {
                        this.purposeField = value;
                    }
                }

                /// <remarks/>
                public string include_consent
                {
                    get
                    {
                        return this.include_consentField;
                    }
                    set
                    {
                        this.include_consentField = value;
                    }
                }

                /// <remarks/>
                public string include_ctos
                {
                    get
                    {
                        return this.include_ctosField;
                    }
                    set
                    {
                        this.include_ctosField = value;
                    }
                }

                /// <remarks/>
                public string include_trex
                {
                    get
                    {
                        return this.include_trexField;
                    }
                    set
                    {
                        this.include_trexField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportSummaryEnq_sumInclude_ccris include_ccris
                {
                    get
                    {
                        return this.include_ccrisField;
                    }
                    set
                    {
                        this.include_ccrisField = value;
                    }
                }

                /// <remarks/>
                public string include_dcheq
                {
                    get
                    {
                        return this.include_dcheqField;
                    }
                    set
                    {
                        this.include_dcheqField = value;
                    }
                }

                /// <remarks/>
                public string include_fico
                {
                    get
                    {
                        return this.include_ficoField;
                    }
                    set
                    {
                        this.include_ficoField = value;
                    }
                }

                /// <remarks/>
                public string include_ssm
                {
                    get
                    {
                        return this.include_ssmField;
                    }
                    set
                    {
                        this.include_ssmField = value;
                    }
                }

                /// <remarks/>
                public uint confirm_entity
                {
                    get
                    {
                        return this.confirm_entityField;
                    }
                    set
                    {
                        this.confirm_entityField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportSummaryEnq_sumEnq_status enq_status
                {
                    get
                    {
                        return this.enq_statusField;
                    }
                    set
                    {
                        this.enq_statusField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportSummaryEnq_sumEnq_code enq_code
                {
                    get
                    {
                        return this.enq_codeField;
                    }
                    set
                    {
                        this.enq_codeField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string ptype
                {
                    get
                    {
                        return this.ptypeField;
                    }
                    set
                    {
                        this.ptypeField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string pcode
                {
                    get
                    {
                        return this.pcodeField;
                    }
                    set
                    {
                        this.pcodeField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string seq
                {
                    get
                    {
                        return this.seqField;
                    }
                    set
                    {
                        this.seqField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportSummaryEnq_sumPurpose
            {

                private string codeField;

                private string valueField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string code
                {
                    get
                    {
                        return this.codeField;
                    }
                    set
                    {
                        this.codeField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlTextAttribute()]
                public string Value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportSummaryEnq_sumInclude_ccris
            {

                private string sumField;

                private string valueField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string sum
                {
                    get
                    {
                        return this.sumField;
                    }
                    set
                    {
                        this.sumField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlTextAttribute()]
                public string Value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportSummaryEnq_sumEnq_status
            {

                private string codeField;

                private string valueField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string code
                {
                    get
                    {
                        return this.codeField;
                    }
                    set
                    {
                        this.codeField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlTextAttribute()]
                public string Value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportSummaryEnq_sumEnq_code
            {

                private string codeField;

                private string valueField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string code
                {
                    get
                    {
                        return this.codeField;
                    }
                    set
                    {
                        this.codeField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlTextAttribute()]
                public string Value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquiry
            {

                private reportEnq_reportEnquirySection_summary section_summaryField;

                private reportEnq_reportEnquirySection_a section_aField;

                private reportEnq_reportEnquirySection_b section_bField;

                private reportEnq_reportEnquirySection_c section_cField;

                private reportEnq_reportEnquirySection_d section_dField;

                private reportEnq_reportEnquirySection_d2 section_d2Field;

                private reportEnq_reportEnquirySection_d4 section_d4Field;

                private reportEnq_reportEnquirySection_ccris section_ccrisField;

                private reportEnq_reportEnquirySection_dcheqs section_dcheqsField;

                private string seqField;

                /// <remarks/>
                public reportEnq_reportEnquirySection_summary section_summary
                {
                    get
                    {
                        return this.section_summaryField;
                    }
                    set
                    {
                        this.section_summaryField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_a section_a
                {
                    get
                    {
                        return this.section_aField;
                    }
                    set
                    {
                        this.section_aField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_b section_b
                {
                    get
                    {
                        return this.section_bField;
                    }
                    set
                    {
                        this.section_bField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_c section_c
                {
                    get
                    {
                        return this.section_cField;
                    }
                    set
                    {
                        this.section_cField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_d section_d
                {
                    get
                    {
                        return this.section_dField;
                    }
                    set
                    {
                        this.section_dField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_d2 section_d2
                {
                    get
                    {
                        return this.section_d2Field;
                    }
                    set
                    {
                        this.section_d2Field = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_d4 section_d4
                {
                    get
                    {
                        return this.section_d4Field;
                    }
                    set
                    {
                        this.section_d4Field = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccris section_ccris
                {
                    get
                    {
                        return this.section_ccrisField;
                    }
                    set
                    {
                        this.section_ccrisField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_dcheqs section_dcheqs
                {
                    get
                    {
                        return this.section_dcheqsField;
                    }
                    set
                    {
                        this.section_dcheqsField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string seq
                {
                    get
                    {
                        return this.seqField;
                    }
                    set
                    {
                        this.seqField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_summary
            {

                private reportEnq_reportEnquirySection_summaryCtos ctosField;

                private reportEnq_reportEnquirySection_summaryTR trField;

                private reportEnq_reportEnquirySection_summaryCcris ccrisField;

                private reportEnq_reportEnquirySection_summaryDcheqs dcheqsField;

                /// <remarks/>
                public reportEnq_reportEnquirySection_summaryCtos ctos
                {
                    get
                    {
                        return this.ctosField;
                    }
                    set
                    {
                        this.ctosField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_summaryTR tr
                {
                    get
                    {
                        return this.trField;
                    }
                    set
                    {
                        this.trField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_summaryCcris ccris
                {
                    get
                    {
                        return this.ccrisField;
                    }
                    set
                    {
                        this.ccrisField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_summaryDcheqs dcheqs
                {
                    get
                    {
                        return this.dcheqsField;
                    }
                    set
                    {
                        this.dcheqsField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_summaryCtos
            {

                private reportEnq_reportEnquirySection_summaryCtosBankruptcy bankruptcyField;

                private reportEnq_reportEnquirySection_summaryCtosLegal legalField;

                private reportEnq_reportEnquirySection_summaryCtosLegal_personal_capacity legal_personal_capacityField;

                private reportEnq_reportEnquirySection_summaryCtosLegal_non_personal_capacity legal_non_personal_capacityField;

                /// <remarks/>
                public reportEnq_reportEnquirySection_summaryCtosBankruptcy bankruptcy
                {
                    get
                    {
                        return this.bankruptcyField;
                    }
                    set
                    {
                        this.bankruptcyField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_summaryCtosLegal legal
                {
                    get
                    {
                        return this.legalField;
                    }
                    set
                    {
                        this.legalField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_summaryCtosLegal_personal_capacity legal_personal_capacity
                {
                    get
                    {
                        return this.legal_personal_capacityField;
                    }
                    set
                    {
                        this.legal_personal_capacityField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_summaryCtosLegal_non_personal_capacity legal_non_personal_capacity
                {
                    get
                    {
                        return this.legal_non_personal_capacityField;
                    }
                    set
                    {
                        this.legal_non_personal_capacityField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_summaryCtosBankruptcy
            {

                private reportEnq_reportEnquirySection_summaryCtosBankruptcySource sourceField;

                private string statusField;

                /// <remarks/>
                public reportEnq_reportEnquirySection_summaryCtosBankruptcySource source
                {
                    get
                    {
                        return this.sourceField;
                    }
                    set
                    {
                        this.sourceField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string status
                {
                    get
                    {
                        return this.statusField;
                    }
                    set
                    {
                        this.statusField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_summaryCtosBankruptcySource
            {

                private string codeField;

                private string nameField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string code
                {
                    get
                    {
                        return this.codeField;
                    }
                    set
                    {
                        this.codeField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string name
                {
                    get
                    {
                        return this.nameField;
                    }
                    set
                    {
                        this.nameField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_summaryCtosLegal
            {

                private reportEnq_reportEnquirySection_summaryCtosLegalSource sourceField;

                private string totalField;

                private uint valueField;

                /// <remarks/>
                public reportEnq_reportEnquirySection_summaryCtosLegalSource source
                {
                    get
                    {
                        return this.sourceField;
                    }
                    set
                    {
                        this.sourceField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string total
                {
                    get
                    {
                        return this.totalField;
                    }
                    set
                    {
                        this.totalField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public uint value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_summaryCtosLegalSource
            {

                private string codeField;

                private string nameField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string code
                {
                    get
                    {
                        return this.codeField;
                    }
                    set
                    {
                        this.codeField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string name
                {
                    get
                    {
                        return this.nameField;
                    }
                    set
                    {
                        this.nameField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_summaryCtosLegal_personal_capacity
            {

                private reportEnq_reportEnquirySection_summaryCtosLegal_personal_capacitySource sourceField;

                private string totalField;

                private uint valueField;

                /// <remarks/>
                public reportEnq_reportEnquirySection_summaryCtosLegal_personal_capacitySource source
                {
                    get
                    {
                        return this.sourceField;
                    }
                    set
                    {
                        this.sourceField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string total
                {
                    get
                    {
                        return this.totalField;
                    }
                    set
                    {
                        this.totalField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public uint value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_summaryCtosLegal_personal_capacitySource
            {

                private string codeField;

                private string nameField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string code
                {
                    get
                    {
                        return this.codeField;
                    }
                    set
                    {
                        this.codeField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string name
                {
                    get
                    {
                        return this.nameField;
                    }
                    set
                    {
                        this.nameField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_summaryCtosLegal_non_personal_capacity
            {

                private string totalField;

                private string valueField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string total
                {
                    get
                    {
                        return this.totalField;
                    }
                    set
                    {
                        this.totalField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_summaryTR
            {

                private reportEnq_reportEnquirySection_summaryTRTrex_ref trex_refField;

                /// <remarks/>
                public reportEnq_reportEnquirySection_summaryTRTrex_ref trex_ref
                {
                    get
                    {
                        return this.trex_refField;
                    }
                    set
                    {
                        this.trex_refField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_summaryTRTrex_ref
            {

                private string negativeField;

                private string positiveField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string negative
                {
                    get
                    {
                        return this.negativeField;
                    }
                    set
                    {
                        this.negativeField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string positive
                {
                    get
                    {
                        return this.positiveField;
                    }
                    set
                    {
                        this.positiveField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_summaryCcris
            {

                private reportEnq_reportEnquirySection_summaryCcrisApplication applicationField;

                private reportEnq_reportEnquirySection_summaryCcrisFacility facilityField;

                private reportEnq_reportEnquirySection_summaryCcrisSpecial_attention special_attentionField;

                /// <remarks/>
                public reportEnq_reportEnquirySection_summaryCcrisApplication application
                {
                    get
                    {
                        return this.applicationField;
                    }
                    set
                    {
                        this.applicationField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_summaryCcrisFacility facility
                {
                    get
                    {
                        return this.facilityField;
                    }
                    set
                    {
                        this.facilityField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_summaryCcrisSpecial_attention special_attention
                {
                    get
                    {
                        return this.special_attentionField;
                    }
                    set
                    {
                        this.special_attentionField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_summaryCcrisApplication
            {

                private string totalField;

                private string approvedField;

                private string pendingField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string total
                {
                    get
                    {
                        return this.totalField;
                    }
                    set
                    {
                        this.totalField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string approved
                {
                    get
                    {
                        return this.approvedField;
                    }
                    set
                    {
                        this.approvedField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string pending
                {
                    get
                    {
                        return this.pendingField;
                    }
                    set
                    {
                        this.pendingField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_summaryCcrisFacility
            {

                private string totalField;

                private string arrearsField;

                private decimal valueField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string total
                {
                    get
                    {
                        return this.totalField;
                    }
                    set
                    {
                        this.totalField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string arrears
                {
                    get
                    {
                        return this.arrearsField;
                    }
                    set
                    {
                        this.arrearsField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public decimal value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_summaryCcrisSpecial_attention
            {

                private string accountsField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string accounts
                {
                    get
                    {
                        return this.accountsField;
                    }
                    set
                    {
                        this.accountsField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_summaryDcheqs
            {

                private string entityField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string entity
                {
                    get
                    {
                        return this.entityField;
                    }
                    set
                    {
                        this.entityField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_a
            {

                private reportEnq_reportEnquirySection_aRecord recordField;

                private bool dataField;

                private string titleField;

                /// <remarks/>
                public reportEnq_reportEnquirySection_aRecord record
                {
                    get
                    {
                        return this.recordField;
                    }
                    set
                    {
                        this.recordField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public bool data
                {
                    get
                    {
                        return this.dataField;
                    }
                    set
                    {
                        this.dataField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string title
                {
                    get
                    {
                        return this.titleField;
                    }
                    set
                    {
                        this.titleField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_aRecord
            {

                private reportEnq_reportEnquirySection_aRecordName nameField;

                private object ic_lcnoField;

                private reportEnq_reportEnquirySection_aRecordNic_brno nic_brnoField;

                private string addrField;

                private object ccris_addressesField;

                private string sourceField;

                private string cpo_dateField;

                private object remarkField;

                private string nationalityField;

                private string birth_dateField;

                private string rpttypeField;

                private string seqField;

                /// <remarks/>
                public reportEnq_reportEnquirySection_aRecordName name
                {
                    get
                    {
                        return this.nameField;
                    }
                    set
                    {
                        this.nameField = value;
                    }
                }

                /// <remarks/>
                public object ic_lcno
                {
                    get
                    {
                        return this.ic_lcnoField;
                    }
                    set
                    {
                        this.ic_lcnoField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_aRecordNic_brno nic_brno
                {
                    get
                    {
                        return this.nic_brnoField;
                    }
                    set
                    {
                        this.nic_brnoField = value;
                    }
                }

                /// <remarks/>
                public string addr
                {
                    get
                    {
                        return this.addrField;
                    }
                    set
                    {
                        this.addrField = value;
                    }
                }

                /// <remarks/>
                public object ccris_addresses
                {
                    get
                    {
                        return this.ccris_addressesField;
                    }
                    set
                    {
                        this.ccris_addressesField = value;
                    }
                }

                /// <remarks/>
                public string source
                {
                    get
                    {
                        return this.sourceField;
                    }
                    set
                    {
                        this.sourceField = value;
                    }
                }

                /// <remarks/>
                public string cpo_date
                {
                    get
                    {
                        return this.cpo_dateField;
                    }
                    set
                    {
                        this.cpo_dateField = value;
                    }
                }

                /// <remarks/>
                public object remark
                {
                    get
                    {
                        return this.remarkField;
                    }
                    set
                    {
                        this.remarkField = value;
                    }
                }

                /// <remarks/>
                public string nationality
                {
                    get
                    {
                        return this.nationalityField;
                    }
                    set
                    {
                        this.nationalityField = value;
                    }
                }

                /// <remarks/>
                public string birth_date
                {
                    get
                    {
                        return this.birth_dateField;
                    }
                    set
                    {
                        this.birth_dateField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string rpttype
                {
                    get
                    {
                        return this.rpttypeField;
                    }
                    set
                    {
                        this.rpttypeField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string seq
                {
                    get
                    {
                        return this.seqField;
                    }
                    set
                    {
                        this.seqField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_aRecordName
            {

                private string matchField;

                private string valueField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string match
                {
                    get
                    {
                        return this.matchField;
                    }
                    set
                    {
                        this.matchField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlTextAttribute()]
                public string Value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_aRecordNic_brno
            {

                private string matchField;

                private ulong valueField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string match
                {
                    get
                    {
                        return this.matchField;
                    }
                    set
                    {
                        this.matchField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlTextAttribute()]
                public ulong Value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_b
            {

                private reportEnq_reportEnquirySection_bHistory[] historyField;

                private string titleField;

                private bool dataField;

                /// <remarks/>
                [System.Xml.Serialization.XmlElementAttribute("history")]
                public reportEnq_reportEnquirySection_bHistory[] history
                {
                    get
                    {
                        return this.historyField;
                    }
                    set
                    {
                        this.historyField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string title
                {
                    get
                    {
                        return this.titleField;
                    }
                    set
                    {
                        this.titleField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public bool data
                {
                    get
                    {
                        return this.dataField;
                    }
                    set
                    {
                        this.dataField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_bHistory
            {

                private reportEnq_reportEnquirySection_bHistoryPeriod[] periodField;

                private string seqField;

                private string rpttypeField;

                private ushort yearField;

                /// <remarks/>
                [System.Xml.Serialization.XmlElementAttribute("period")]
                public reportEnq_reportEnquirySection_bHistoryPeriod[] period
                {
                    get
                    {
                        return this.periodField;
                    }
                    set
                    {
                        this.periodField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string seq
                {
                    get
                    {
                        return this.seqField;
                    }
                    set
                    {
                        this.seqField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string rpttype
                {
                    get
                    {
                        return this.rpttypeField;
                    }
                    set
                    {
                        this.rpttypeField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public ushort year
                {
                    get
                    {
                        return this.yearField;
                    }
                    set
                    {
                        this.yearField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_bHistoryPeriod
            {

                private reportEnq_reportEnquirySection_bHistoryPeriodEntity[] entityField;

                private string monthField;

                /// <remarks/>
                [System.Xml.Serialization.XmlElementAttribute("entity")]
                public reportEnq_reportEnquirySection_bHistoryPeriodEntity[] entity
                {
                    get
                    {
                        return this.entityField;
                    }
                    set
                    {
                        this.entityField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string month
                {
                    get
                    {
                        return this.monthField;
                    }
                    set
                    {
                        this.monthField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_bHistoryPeriodEntity
            {

                private string typeField;

                private string valueField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string type
                {
                    get
                    {
                        return this.typeField;
                    }
                    set
                    {
                        this.typeField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_c
            {

                private reportEnq_reportEnquirySection_cRecord recordField;

                private string titleField;

                private bool dataField;

                /// <remarks/>
                public reportEnq_reportEnquirySection_cRecord record
                {
                    get
                    {
                        return this.recordField;
                    }
                    set
                    {
                        this.recordField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string title
                {
                    get
                    {
                        return this.titleField;
                    }
                    set
                    {
                        this.titleField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public bool data
                {
                    get
                    {
                        return this.dataField;
                    }
                    set
                    {
                        this.dataField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_cRecord
            {

                private string company_nameField;

                private uint localField;

                private string localaField;

                private string objectField;

                private string incdateField;

                private string lastdocField;

                private string appointField;

                private object resignField;

                private string nameField;

                private string ic_lcnoField;

                private ulong nic_brnoField;

                private string addrField;

                private string positionField;

                private string cpo_dateField;

                private uint paidupField;

                private string sharesField;

                private object remarkField;

                private decimal protif_after_taxField;

                private string latest_financial_yearField;

                private string statusField;

                private string rpttypeField;

                private string seqField;

                /// <remarks/>
                public string company_name
                {
                    get
                    {
                        return this.company_nameField;
                    }
                    set
                    {
                        this.company_nameField = value;
                    }
                }

                /// <remarks/>
                public uint local
                {
                    get
                    {
                        return this.localField;
                    }
                    set
                    {
                        this.localField = value;
                    }
                }

                /// <remarks/>
                public string locala
                {
                    get
                    {
                        return this.localaField;
                    }
                    set
                    {
                        this.localaField = value;
                    }
                }

                /// <remarks/>
                public string @object
                {
                    get
                    {
                        return this.objectField;
                    }
                    set
                    {
                        this.objectField = value;
                    }
                }

                /// <remarks/>
                public string incdate
                {
                    get
                    {
                        return this.incdateField;
                    }
                    set
                    {
                        this.incdateField = value;
                    }
                }

                /// <remarks/>
                public string lastdoc
                {
                    get
                    {
                        return this.lastdocField;
                    }
                    set
                    {
                        this.lastdocField = value;
                    }
                }

                /// <remarks/>
                public string appoint
                {
                    get
                    {
                        return this.appointField;
                    }
                    set
                    {
                        this.appointField = value;
                    }
                }

                /// <remarks/>
                public object resign
                {
                    get
                    {
                        return this.resignField;
                    }
                    set
                    {
                        this.resignField = value;
                    }
                }

                /// <remarks/>
                public string name
                {
                    get
                    {
                        return this.nameField;
                    }
                    set
                    {
                        this.nameField = value;
                    }
                }

                /// <remarks/>
                public string ic_lcno
                {
                    get
                    {
                        return this.ic_lcnoField;
                    }
                    set
                    {
                        this.ic_lcnoField = value;
                    }
                }

                /// <remarks/>
                public ulong nic_brno
                {
                    get
                    {
                        return this.nic_brnoField;
                    }
                    set
                    {
                        this.nic_brnoField = value;
                    }
                }

                /// <remarks/>
                public string addr
                {
                    get
                    {
                        return this.addrField;
                    }
                    set
                    {
                        this.addrField = value;
                    }
                }

                /// <remarks/>
                public string position
                {
                    get
                    {
                        return this.positionField;
                    }
                    set
                    {
                        this.positionField = value;
                    }
                }

                /// <remarks/>
                public string cpo_date
                {
                    get
                    {
                        return this.cpo_dateField;
                    }
                    set
                    {
                        this.cpo_dateField = value;
                    }
                }

                /// <remarks/>
                public uint paidup
                {
                    get
                    {
                        return this.paidupField;
                    }
                    set
                    {
                        this.paidupField = value;
                    }
                }

                /// <remarks/>
                public string shares
                {
                    get
                    {
                        return this.sharesField;
                    }
                    set
                    {
                        this.sharesField = value;
                    }
                }

                /// <remarks/>
                public object remark
                {
                    get
                    {
                        return this.remarkField;
                    }
                    set
                    {
                        this.remarkField = value;
                    }
                }

                /// <remarks/>
                public decimal protif_after_tax
                {
                    get
                    {
                        return this.protif_after_taxField;
                    }
                    set
                    {
                        this.protif_after_taxField = value;
                    }
                }

                /// <remarks/>
                public string latest_financial_year
                {
                    get
                    {
                        return this.latest_financial_yearField;
                    }
                    set
                    {
                        this.latest_financial_yearField = value;
                    }
                }

                /// <remarks/>
                public string status
                {
                    get
                    {
                        return this.statusField;
                    }
                    set
                    {
                        this.statusField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string rpttype
                {
                    get
                    {
                        return this.rpttypeField;
                    }
                    set
                    {
                        this.rpttypeField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string seq
                {
                    get
                    {
                        return this.seqField;
                    }
                    set
                    {
                        this.seqField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_d
            {

                private reportEnq_reportEnquirySection_dRecord[] recordField;

                private string titleField;

                private bool dataField;

                /// <remarks/>
                [System.Xml.Serialization.XmlElementAttribute("record")]
                public reportEnq_reportEnquirySection_dRecord[] record
                {
                    get
                    {
                        return this.recordField;
                    }
                    set
                    {
                        this.recordField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string title
                {
                    get
                    {
                        return this.titleField;
                    }
                    set
                    {
                        this.titleField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public bool data
                {
                    get
                    {
                        return this.dataField;
                    }
                    set
                    {
                        this.dataField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_dRecord
            {

                private string titleField;

                private string special_remarkField;

                private reportEnq_reportEnquirySection_dRecordName nameField;

                private object aliasField;

                private string addrField;

                private string ic_lcnoField;

                private reportEnq_reportEnquirySection_dRecordNic_brno nic_brnoField;

                private string case_noField;

                private string court_detailField;

                private string property_locationField;

                private string property_descriptionField;

                private string firmField;

                private string exparteField;

                private reportEnq_reportEnquirySection_dRecordNotice noticeField;

                private reportEnq_reportEnquirySection_dRecordPetition petitionField;

                private reportEnq_reportEnquirySection_dRecordOrder orderField;

                private object releaseField;

                private object gazette_orderField;

                private object gazette_noticeField;

                private object gazette_petitionField;

                private object gazette_dischargeField;

                private string plaintiffField;

                private reportEnq_reportEnquirySection_dRecordAuction auctionField;

                private object originating_summonsField;

                private object gazetteField;

                private string register_ownerField;

                private reportEnq_reportEnquirySection_dRecordAction actionField;

                private string hear_dateField;

                private string order_for_sale_dateField;

                private string outstanding_amountField;

                private bool outstanding_amountFieldSpecified;

                private uint reserve_priceField;

                private bool reserve_priceFieldSpecified;

                private string amountField;

                private string remarkField;

                private reportEnq_reportEnquirySection_dRecordLawyer lawyerField;

                private reportEnq_reportEnquirySection_dRecordCedcon cedconField;

                private object auctioneerField;

                private reportEnq_reportEnquirySection_dRecordSettlement settlementField;

                private reportEnq_reportEnquirySection_dRecordLatest_status latest_statusField;

                private object subject_cmtField;

                private object cra_cmtField;

                private string rpttypeField;

                private string seqField;

                private string statusField;

                /// <remarks/>
                public string title
                {
                    get
                    {
                        return this.titleField;
                    }
                    set
                    {
                        this.titleField = value;
                    }
                }

                /// <remarks/>
                public string special_remark
                {
                    get
                    {
                        return this.special_remarkField;
                    }
                    set
                    {
                        this.special_remarkField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_dRecordName name
                {
                    get
                    {
                        return this.nameField;
                    }
                    set
                    {
                        this.nameField = value;
                    }
                }

                /// <remarks/>
                public object alias
                {
                    get
                    {
                        return this.aliasField;
                    }
                    set
                    {
                        this.aliasField = value;
                    }
                }

                /// <remarks/>
                public string addr
                {
                    get
                    {
                        return this.addrField;
                    }
                    set
                    {
                        this.addrField = value;
                    }
                }

                /// <remarks/>
                public string ic_lcno
                {
                    get
                    {
                        return this.ic_lcnoField;
                    }
                    set
                    {
                        this.ic_lcnoField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_dRecordNic_brno nic_brno
                {
                    get
                    {
                        return this.nic_brnoField;
                    }
                    set
                    {
                        this.nic_brnoField = value;
                    }
                }

                /// <remarks/>
                public string case_no
                {
                    get
                    {
                        return this.case_noField;
                    }
                    set
                    {
                        this.case_noField = value;
                    }
                }

                /// <remarks/>
                public string court_detail
                {
                    get
                    {
                        return this.court_detailField;
                    }
                    set
                    {
                        this.court_detailField = value;
                    }
                }

                /// <remarks/>
                public string property_location
                {
                    get
                    {
                        return this.property_locationField;
                    }
                    set
                    {
                        this.property_locationField = value;
                    }
                }

                /// <remarks/>
                public string property_description
                {
                    get
                    {
                        return this.property_descriptionField;
                    }
                    set
                    {
                        this.property_descriptionField = value;
                    }
                }

                /// <remarks/>
                public string firm
                {
                    get
                    {
                        return this.firmField;
                    }
                    set
                    {
                        this.firmField = value;
                    }
                }

                /// <remarks/>
                public string exparte
                {
                    get
                    {
                        return this.exparteField;
                    }
                    set
                    {
                        this.exparteField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_dRecordNotice notice
                {
                    get
                    {
                        return this.noticeField;
                    }
                    set
                    {
                        this.noticeField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_dRecordPetition petition
                {
                    get
                    {
                        return this.petitionField;
                    }
                    set
                    {
                        this.petitionField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_dRecordOrder order
                {
                    get
                    {
                        return this.orderField;
                    }
                    set
                    {
                        this.orderField = value;
                    }
                }

                /// <remarks/>
                public object release
                {
                    get
                    {
                        return this.releaseField;
                    }
                    set
                    {
                        this.releaseField = value;
                    }
                }

                /// <remarks/>
                public object gazette_order
                {
                    get
                    {
                        return this.gazette_orderField;
                    }
                    set
                    {
                        this.gazette_orderField = value;
                    }
                }

                /// <remarks/>
                public object gazette_notice
                {
                    get
                    {
                        return this.gazette_noticeField;
                    }
                    set
                    {
                        this.gazette_noticeField = value;
                    }
                }

                /// <remarks/>
                public object gazette_petition
                {
                    get
                    {
                        return this.gazette_petitionField;
                    }
                    set
                    {
                        this.gazette_petitionField = value;
                    }
                }

                /// <remarks/>
                public object gazette_discharge
                {
                    get
                    {
                        return this.gazette_dischargeField;
                    }
                    set
                    {
                        this.gazette_dischargeField = value;
                    }
                }

                /// <remarks/>
                public string plaintiff
                {
                    get
                    {
                        return this.plaintiffField;
                    }
                    set
                    {
                        this.plaintiffField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_dRecordAuction auction
                {
                    get
                    {
                        return this.auctionField;
                    }
                    set
                    {
                        this.auctionField = value;
                    }
                }

                /// <remarks/>
                public object originating_summons
                {
                    get
                    {
                        return this.originating_summonsField;
                    }
                    set
                    {
                        this.originating_summonsField = value;
                    }
                }

                /// <remarks/>
                public object gazette
                {
                    get
                    {
                        return this.gazetteField;
                    }
                    set
                    {
                        this.gazetteField = value;
                    }
                }

                /// <remarks/>
                public string register_owner
                {
                    get
                    {
                        return this.register_ownerField;
                    }
                    set
                    {
                        this.register_ownerField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_dRecordAction action
                {
                    get
                    {
                        return this.actionField;
                    }
                    set
                    {
                        this.actionField = value;
                    }
                }

                /// <remarks/>
                public string hear_date
                {
                    get
                    {
                        return this.hear_dateField;
                    }
                    set
                    {
                        this.hear_dateField = value;
                    }
                }

                /// <remarks/>
                public string order_for_sale_date
                {
                    get
                    {
                        return this.order_for_sale_dateField;
                    }
                    set
                    {
                        this.order_for_sale_dateField = value;
                    }
                }

                /// <remarks/>
                public string outstanding_amount
                {
                    get
                    {
                        return this.outstanding_amountField;
                    }
                    set
                    {
                        this.outstanding_amountField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlIgnoreAttribute()]
                public bool outstanding_amountSpecified
                {
                    get
                    {
                        return this.outstanding_amountFieldSpecified;
                    }
                    set
                    {
                        this.outstanding_amountFieldSpecified = value;
                    }
                }

                /// <remarks/>
                public uint reserve_price
                {
                    get
                    {
                        return this.reserve_priceField;
                    }
                    set
                    {
                        this.reserve_priceField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlIgnoreAttribute()]
                public bool reserve_priceSpecified
                {
                    get
                    {
                        return this.reserve_priceFieldSpecified;
                    }
                    set
                    {
                        this.reserve_priceFieldSpecified = value;
                    }
                }

                /// <remarks/>
                public string amount
                {
                    get
                    {
                        return this.amountField;
                    }
                    set
                    {
                        this.amountField = value;
                    }
                }

                /// <remarks/>
                public string remark
                {
                    get
                    {
                        return this.remarkField;
                    }
                    set
                    {
                        this.remarkField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_dRecordLawyer lawyer
                {
                    get
                    {
                        return this.lawyerField;
                    }
                    set
                    {
                        this.lawyerField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_dRecordCedcon cedcon
                {
                    get
                    {
                        return this.cedconField;
                    }
                    set
                    {
                        this.cedconField = value;
                    }
                }

                /// <remarks/>
                public object auctioneer
                {
                    get
                    {
                        return this.auctioneerField;
                    }
                    set
                    {
                        this.auctioneerField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_dRecordSettlement settlement
                {
                    get
                    {
                        return this.settlementField;
                    }
                    set
                    {
                        this.settlementField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_dRecordLatest_status latest_status
                {
                    get
                    {
                        return this.latest_statusField;
                    }
                    set
                    {
                        this.latest_statusField = value;
                    }
                }

                /// <remarks/>
                public object subject_cmt
                {
                    get
                    {
                        return this.subject_cmtField;
                    }
                    set
                    {
                        this.subject_cmtField = value;
                    }
                }

                /// <remarks/>
                public object cra_cmt
                {
                    get
                    {
                        return this.cra_cmtField;
                    }
                    set
                    {
                        this.cra_cmtField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string rpttype
                {
                    get
                    {
                        return this.rpttypeField;
                    }
                    set
                    {
                        this.rpttypeField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string seq
                {
                    get
                    {
                        return this.seqField;
                    }
                    set
                    {
                        this.seqField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string status
                {
                    get
                    {
                        return this.statusField;
                    }
                    set
                    {
                        this.statusField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_dRecordName
            {

                private string matchField;

                private string valueField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string match
                {
                    get
                    {
                        return this.matchField;
                    }
                    set
                    {
                        this.matchField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlTextAttribute()]
                public string Value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_dRecordNic_brno
            {

                private string matchField;

                private ulong valueField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string match
                {
                    get
                    {
                        return this.matchField;
                    }
                    set
                    {
                        this.matchField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlTextAttribute()]
                public ulong Value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_dRecordNotice
            {

                private string dateField;

                private string source_detailField;

                /// <remarks/>
                public string date
                {
                    get
                    {
                        return this.dateField;
                    }
                    set
                    {
                        this.dateField = value;
                    }
                }

                /// <remarks/>
                public string source_detail
                {
                    get
                    {
                        return this.source_detailField;
                    }
                    set
                    {
                        this.source_detailField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_dRecordPetition
            {

                private object dateField;

                private string source_detailField;

                /// <remarks/>
                public object date
                {
                    get
                    {
                        return this.dateField;
                    }
                    set
                    {
                        this.dateField = value;
                    }
                }

                /// <remarks/>
                public string source_detail
                {
                    get
                    {
                        return this.source_detailField;
                    }
                    set
                    {
                        this.source_detailField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_dRecordOrder
            {

                private System.DateTime dateField;

                private string source_detailField;

                /// <remarks/>
                [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
                public System.DateTime date
                {
                    get
                    {
                        return this.dateField;
                    }
                    set
                    {
                        this.dateField = value;
                    }
                }

                /// <remarks/>
                public string source_detail
                {
                    get
                    {
                        return this.source_detailField;
                    }
                    set
                    {
                        this.source_detailField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_dRecordAuction
            {

                private string dateField;

                private string source_detailField;

                /// <remarks/>
                public string date
                {
                    get
                    {
                        return this.dateField;
                    }
                    set
                    {
                        this.dateField = value;
                    }
                }

                /// <remarks/>
                public string source_detail
                {
                    get
                    {
                        return this.source_detailField;
                    }
                    set
                    {
                        this.source_detailField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_dRecordAction
            {

                private string dateField;

                private string source_detailField;

                /// <remarks/>
                public string date
                {
                    get
                    {
                        return this.dateField;
                    }
                    set
                    {
                        this.dateField = value;
                    }
                }

                /// <remarks/>
                public string source_detail
                {
                    get
                    {
                        return this.source_detailField;
                    }
                    set
                    {
                        this.source_detailField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_dRecordLawyer
            {

                private string nameField;

                private string add1Field;

                private string add2Field;

                private string add3Field;

                private string add4Field;

                private string telField;

                private string refField;

                /// <remarks/>
                public string name
                {
                    get
                    {
                        return this.nameField;
                    }
                    set
                    {
                        this.nameField = value;
                    }
                }

                /// <remarks/>
                public string add1
                {
                    get
                    {
                        return this.add1Field;
                    }
                    set
                    {
                        this.add1Field = value;
                    }
                }

                /// <remarks/>
                public string add2
                {
                    get
                    {
                        return this.add2Field;
                    }
                    set
                    {
                        this.add2Field = value;
                    }
                }

                /// <remarks/>
                public string add3
                {
                    get
                    {
                        return this.add3Field;
                    }
                    set
                    {
                        this.add3Field = value;
                    }
                }

                /// <remarks/>
                public string add4
                {
                    get
                    {
                        return this.add4Field;
                    }
                    set
                    {
                        this.add4Field = value;
                    }
                }

                /// <remarks/>
                public string tel
                {
                    get
                    {
                        return this.telField;
                    }
                    set
                    {
                        this.telField = value;
                    }
                }

                /// <remarks/>
                public string @ref
                {
                    get
                    {
                        return this.refField;
                    }
                    set
                    {
                        this.refField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_dRecordCedcon
            {

                private string nameField;

                private string add1Field;

                private string add2Field;

                private string add3Field;

                private string add4Field;

                private string telField;

                private string refField;

                /// <remarks/>
                public string name
                {
                    get
                    {
                        return this.nameField;
                    }
                    set
                    {
                        this.nameField = value;
                    }
                }

                /// <remarks/>
                public string add1
                {
                    get
                    {
                        return this.add1Field;
                    }
                    set
                    {
                        this.add1Field = value;
                    }
                }

                /// <remarks/>
                public string add2
                {
                    get
                    {
                        return this.add2Field;
                    }
                    set
                    {
                        this.add2Field = value;
                    }
                }

                /// <remarks/>
                public string add3
                {
                    get
                    {
                        return this.add3Field;
                    }
                    set
                    {
                        this.add3Field = value;
                    }
                }

                /// <remarks/>
                public string add4
                {
                    get
                    {
                        return this.add4Field;
                    }
                    set
                    {
                        this.add4Field = value;
                    }
                }

                /// <remarks/>
                public string tel
                {
                    get
                    {
                        return this.telField;
                    }
                    set
                    {
                        this.telField = value;
                    }
                }

                /// <remarks/>
                public string @ref
                {
                    get
                    {
                        return this.refField;
                    }
                    set
                    {
                        this.refField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_dRecordSettlement
            {

                private string codeField;

                private string dateField;

                private string sourceField;

                private string source_dateField;

                /// <remarks/>
                public string code
                {
                    get
                    {
                        return this.codeField;
                    }
                    set
                    {
                        this.codeField = value;
                    }
                }

                /// <remarks/>
                public string date
                {
                    get
                    {
                        return this.dateField;
                    }
                    set
                    {
                        this.dateField = value;
                    }
                }

                /// <remarks/>
                public string source
                {
                    get
                    {
                        return this.sourceField;
                    }
                    set
                    {
                        this.sourceField = value;
                    }
                }

                /// <remarks/>
                public string source_date
                {
                    get
                    {
                        return this.source_dateField;
                    }
                    set
                    {
                        this.source_dateField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_dRecordLatest_status
            {

                private string codeField;

                private string dateField;

                private string sourceField;

                private string source_dateField;

                private string exp_dateField;

                private string update_dateField;

                /// <remarks/>
                public string code
                {
                    get
                    {
                        return this.codeField;
                    }
                    set
                    {
                        this.codeField = value;
                    }
                }

                /// <remarks/>
                public string date
                {
                    get
                    {
                        return this.dateField;
                    }
                    set
                    {
                        this.dateField = value;
                    }
                }

                /// <remarks/>
                public string source
                {
                    get
                    {
                        return this.sourceField;
                    }
                    set
                    {
                        this.sourceField = value;
                    }
                }

                /// <remarks/>
                public string source_date
                {
                    get
                    {
                        return this.source_dateField;
                    }
                    set
                    {
                        this.source_dateField = value;
                    }
                }

                /// <remarks/>
                public string exp_date
                {
                    get
                    {
                        return this.exp_dateField;
                    }
                    set
                    {
                        this.exp_dateField = value;
                    }
                }

                /// <remarks/>
                public string update_date
                {
                    get
                    {
                        return this.update_dateField;
                    }
                    set
                    {
                        this.update_dateField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_d2
            {

                private string titleField;

                private bool dataField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string title
                {
                    get
                    {
                        return this.titleField;
                    }
                    set
                    {
                        this.titleField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public bool data
                {
                    get
                    {
                        return this.dataField;
                    }
                    set
                    {
                        this.dataField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_d4
            {

                private string titleField;

                private bool dataField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string title
                {
                    get
                    {
                        return this.titleField;
                    }
                    set
                    {
                        this.titleField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public bool data
                {
                    get
                    {
                        return this.dataField;
                    }
                    set
                    {
                        this.dataField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccris
            {

                private reportEnq_reportEnquirySection_ccrisSummary summaryField;

                private reportEnq_reportEnquirySection_ccrisDerivatives derivativesField;

                private reportEnq_reportEnquirySection_ccrisApplications applicationsField;

                private reportEnq_reportEnquirySection_ccrisAccount[] accountsField;

                private object special_attention_accsField;

                private string titleField;

                private bool dataField;

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisSummary summary
                {
                    get
                    {
                        return this.summaryField;
                    }
                    set
                    {
                        this.summaryField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisDerivatives derivatives
                {
                    get
                    {
                        return this.derivativesField;
                    }
                    set
                    {
                        this.derivativesField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisApplications applications
                {
                    get
                    {
                        return this.applicationsField;
                    }
                    set
                    {
                        this.applicationsField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlArrayItemAttribute("account", IsNullable = false)]
                public reportEnq_reportEnquirySection_ccrisAccount[] accounts
                {
                    get
                    {
                        return this.accountsField;
                    }
                    set
                    {
                        this.accountsField = value;
                    }
                }

                /// <remarks/>
                public object special_attention_accs
                {
                    get
                    {
                        return this.special_attention_accsField;
                    }
                    set
                    {
                        this.special_attention_accsField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string title
                {
                    get
                    {
                        return this.titleField;
                    }
                    set
                    {
                        this.titleField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public bool data
                {
                    get
                    {
                        return this.dataField;
                    }
                    set
                    {
                        this.dataField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisSummary
            {

                private reportEnq_reportEnquirySection_ccrisSummaryApplication applicationField;

                private reportEnq_reportEnquirySection_ccrisSummaryLiabilities liabilitiesField;

                private reportEnq_reportEnquirySection_ccrisSummaryLegal legalField;

                private reportEnq_reportEnquirySection_ccrisSummarySpecial_attention special_attentionField;

                private reportEnq_reportEnquirySection_ccrisSummarySpecial_name special_nameField;

                private uint entity_keyField;

                private string entity_warningField;

                private string fi_codeField;

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisSummaryApplication application
                {
                    get
                    {
                        return this.applicationField;
                    }
                    set
                    {
                        this.applicationField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisSummaryLiabilities liabilities
                {
                    get
                    {
                        return this.liabilitiesField;
                    }
                    set
                    {
                        this.liabilitiesField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisSummaryLegal legal
                {
                    get
                    {
                        return this.legalField;
                    }
                    set
                    {
                        this.legalField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisSummarySpecial_attention special_attention
                {
                    get
                    {
                        return this.special_attentionField;
                    }
                    set
                    {
                        this.special_attentionField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisSummarySpecial_name special_name
                {
                    get
                    {
                        return this.special_nameField;
                    }
                    set
                    {
                        this.special_nameField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public uint entity_key
                {
                    get
                    {
                        return this.entity_keyField;
                    }
                    set
                    {
                        this.entity_keyField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string entity_warning
                {
                    get
                    {
                        return this.entity_warningField;
                    }
                    set
                    {
                        this.entity_warningField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string fi_code
                {
                    get
                    {
                        return this.fi_codeField;
                    }
                    set
                    {
                        this.fi_codeField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisSummaryApplication
            {

                private reportEnq_reportEnquirySection_ccrisSummaryApplicationApproved approvedField;

                private reportEnq_reportEnquirySection_ccrisSummaryApplicationPending pendingField;

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisSummaryApplicationApproved approved
                {
                    get
                    {
                        return this.approvedField;
                    }
                    set
                    {
                        this.approvedField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisSummaryApplicationPending pending
                {
                    get
                    {
                        return this.pendingField;
                    }
                    set
                    {
                        this.pendingField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisSummaryApplicationApproved
            {

                private string countField;

                private string valueField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string count
                {
                    get
                    {
                        return this.countField;
                    }
                    set
                    {
                        this.countField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlTextAttribute()]
                public string Value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisSummaryApplicationPending
            {

                private string countField;

                private string valueField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string count
                {
                    get
                    {
                        return this.countField;
                    }
                    set
                    {
                        this.countField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlTextAttribute()]
                public string Value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisSummaryLiabilities
            {

                private reportEnq_reportEnquirySection_ccrisSummaryLiabilitiesBorrower borrowerField;

                private reportEnq_reportEnquirySection_ccrisSummaryLiabilitiesGuarantor guarantorField;

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisSummaryLiabilitiesBorrower borrower
                {
                    get
                    {
                        return this.borrowerField;
                    }
                    set
                    {
                        this.borrowerField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisSummaryLiabilitiesGuarantor guarantor
                {
                    get
                    {
                        return this.guarantorField;
                    }
                    set
                    {
                        this.guarantorField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisSummaryLiabilitiesBorrower
            {

                private string total_limitField;

                private string fec_limitField;

                private string valueField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string total_limit
                {
                    get
                    {
                        return this.total_limitField;
                    }
                    set
                    {
                        this.total_limitField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string fec_limit
                {
                    get
                    {
                        return this.fec_limitField;
                    }
                    set
                    {
                        this.fec_limitField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlTextAttribute()]
                public string Value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisSummaryLiabilitiesGuarantor
            {

                private string total_limitField;

                private string fec_limitField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string total_limit
                {
                    get
                    {
                        return this.total_limitField;
                    }
                    set
                    {
                        this.total_limitField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string fec_limit
                {
                    get
                    {
                        return this.fec_limitField;
                    }
                    set
                    {
                        this.fec_limitField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisSummaryLegal
            {

                private string statusField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string status
                {
                    get
                    {
                        return this.statusField;
                    }
                    set
                    {
                        this.statusField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisSummarySpecial_attention
            {

                private string statusField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string status
                {
                    get
                    {
                        return this.statusField;
                    }
                    set
                    {
                        this.statusField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisSummarySpecial_name
            {

                private string statusField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string status
                {
                    get
                    {
                        return this.statusField;
                    }
                    set
                    {
                        this.statusField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisDerivatives
            {

                private reportEnq_reportEnquirySection_ccrisDerivativesApplication applicationField;

                private reportEnq_reportEnquirySection_ccrisDerivativesFacilities facilitiesField;

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisDerivativesApplication application
                {
                    get
                    {
                        return this.applicationField;
                    }
                    set
                    {
                        this.applicationField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisDerivativesFacilities facilities
                {
                    get
                    {
                        return this.facilitiesField;
                    }
                    set
                    {
                        this.facilitiesField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisDerivativesApplication
            {

                private string dateField;

                private reportEnq_reportEnquirySection_ccrisDerivativesApplicationFacility facilityField;

                /// <remarks/>
                public string date
                {
                    get
                    {
                        return this.dateField;
                    }
                    set
                    {
                        this.dateField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisDerivativesApplicationFacility facility
                {
                    get
                    {
                        return this.facilityField;
                    }
                    set
                    {
                        this.facilityField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisDerivativesApplicationFacility
            {

                private string codeField;

                private string valueField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string code
                {
                    get
                    {
                        return this.codeField;
                    }
                    set
                    {
                        this.codeField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlTextAttribute()]
                public string Value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisDerivativesFacilities
            {

                private reportEnq_reportEnquirySection_ccrisDerivativesFacilitiesSecure secureField;

                private reportEnq_reportEnquirySection_ccrisDerivativesFacilitiesUnsecure unsecureField;

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisDerivativesFacilitiesSecure secure
                {
                    get
                    {
                        return this.secureField;
                    }
                    set
                    {
                        this.secureField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisDerivativesFacilitiesUnsecure unsecure
                {
                    get
                    {
                        return this.unsecureField;
                    }
                    set
                    {
                        this.unsecureField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisDerivativesFacilitiesSecure
            {

                private reportEnq_reportEnquirySection_ccrisDerivativesFacilitiesSecureOutstanding outstandingField;

                private string totalField;

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisDerivativesFacilitiesSecureOutstanding outstanding
                {
                    get
                    {
                        return this.outstandingField;
                    }
                    set
                    {
                        this.outstandingField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string total
                {
                    get
                    {
                        return this.totalField;
                    }
                    set
                    {
                        this.totalField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisDerivativesFacilitiesSecureOutstanding
            {

                private decimal averageField;

                private string limitField;

                private ushort valueField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public decimal average
                {
                    get
                    {
                        return this.averageField;
                    }
                    set
                    {
                        this.averageField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string limit
                {
                    get
                    {
                        return this.limitField;
                    }
                    set
                    {
                        this.limitField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlTextAttribute()]
                public ushort Value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisDerivativesFacilitiesUnsecure
            {

                private reportEnq_reportEnquirySection_ccrisDerivativesFacilitiesUnsecureOutstanding outstandingField;

                private string totalField;

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisDerivativesFacilitiesUnsecureOutstanding outstanding
                {
                    get
                    {
                        return this.outstandingField;
                    }
                    set
                    {
                        this.outstandingField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string total
                {
                    get
                    {
                        return this.totalField;
                    }
                    set
                    {
                        this.totalField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisDerivativesFacilitiesUnsecureOutstanding
            {

                private decimal averageField;

                private string limitField;

                private ushort valueField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public decimal average
                {
                    get
                    {
                        return this.averageField;
                    }
                    set
                    {
                        this.averageField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string limit
                {
                    get
                    {
                        return this.limitField;
                    }
                    set
                    {
                        this.limitField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlTextAttribute()]
                public ushort Value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisApplications
            {

                private reportEnq_reportEnquirySection_ccrisApplicationsApplication applicationField;

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisApplicationsApplication application
                {
                    get
                    {
                        return this.applicationField;
                    }
                    set
                    {
                        this.applicationField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisApplicationsApplication
            {

                private ushort org1Field;

                private ushort org2Field;

                private uint adta_idField;

                private string dateField;

                private string app_typeField;

                private reportEnq_reportEnquirySection_ccrisApplicationsApplicationStatus statusField;

                private reportEnq_reportEnquirySection_ccrisApplicationsApplicationCapacity capacityField;

                private reportEnq_reportEnquirySection_ccrisApplicationsApplicationLender_type lender_typeField;

                private object my_fgnField;

                private ushort amountField;

                /// <remarks/>
                public ushort org1
                {
                    get
                    {
                        return this.org1Field;
                    }
                    set
                    {
                        this.org1Field = value;
                    }
                }

                /// <remarks/>
                public ushort org2
                {
                    get
                    {
                        return this.org2Field;
                    }
                    set
                    {
                        this.org2Field = value;
                    }
                }

                /// <remarks/>
                public uint adta_id
                {
                    get
                    {
                        return this.adta_idField;
                    }
                    set
                    {
                        this.adta_idField = value;
                    }
                }

                /// <remarks/>
                public string date
                {
                    get
                    {
                        return this.dateField;
                    }
                    set
                    {
                        this.dateField = value;
                    }
                }

                /// <remarks/>
                public string app_type
                {
                    get
                    {
                        return this.app_typeField;
                    }
                    set
                    {
                        this.app_typeField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisApplicationsApplicationStatus status
                {
                    get
                    {
                        return this.statusField;
                    }
                    set
                    {
                        this.statusField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisApplicationsApplicationCapacity capacity
                {
                    get
                    {
                        return this.capacityField;
                    }
                    set
                    {
                        this.capacityField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisApplicationsApplicationLender_type lender_type
                {
                    get
                    {
                        return this.lender_typeField;
                    }
                    set
                    {
                        this.lender_typeField = value;
                    }
                }

                /// <remarks/>
                public object my_fgn
                {
                    get
                    {
                        return this.my_fgnField;
                    }
                    set
                    {
                        this.my_fgnField = value;
                    }
                }

                /// <remarks/>
                public ushort amount
                {
                    get
                    {
                        return this.amountField;
                    }
                    set
                    {
                        this.amountField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisApplicationsApplicationStatus
            {

                private string codeField;

                private string valueField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string code
                {
                    get
                    {
                        return this.codeField;
                    }
                    set
                    {
                        this.codeField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlTextAttribute()]
                public string Value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisApplicationsApplicationCapacity
            {

                private string codeField;

                private string valueField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string code
                {
                    get
                    {
                        return this.codeField;
                    }
                    set
                    {
                        this.codeField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlTextAttribute()]
                public string Value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisApplicationsApplicationLender_type
            {

                private string codeField;

                private string valueField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string code
                {
                    get
                    {
                        return this.codeField;
                    }
                    set
                    {
                        this.codeField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlTextAttribute()]
                public string Value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisAccount
            {

                private ushort org1Field;

                private ushort org2Field;

                private uint mdta_idField;

                private object my_fgnField;

                private string approval_dateField;

                private reportEnq_reportEnquirySection_ccrisAccountCapacity capacityField;

                private reportEnq_reportEnquirySection_ccrisAccountLender_type lender_typeField;

                private ushort limitField;

                private reportEnq_reportEnquirySection_ccrisAccountCollaterals collateralsField;

                private reportEnq_reportEnquirySection_ccrisAccountLegal legalField;

                private reportEnq_reportEnquirySection_ccrisAccountSub_account[] sub_accountsField;

                /// <remarks/>
                public ushort org1
                {
                    get
                    {
                        return this.org1Field;
                    }
                    set
                    {
                        this.org1Field = value;
                    }
                }

                /// <remarks/>
                public ushort org2
                {
                    get
                    {
                        return this.org2Field;
                    }
                    set
                    {
                        this.org2Field = value;
                    }
                }

                /// <remarks/>
                public uint mdta_id
                {
                    get
                    {
                        return this.mdta_idField;
                    }
                    set
                    {
                        this.mdta_idField = value;
                    }
                }

                /// <remarks/>
                public object my_fgn
                {
                    get
                    {
                        return this.my_fgnField;
                    }
                    set
                    {
                        this.my_fgnField = value;
                    }
                }

                /// <remarks/>
                public string approval_date
                {
                    get
                    {
                        return this.approval_dateField;
                    }
                    set
                    {
                        this.approval_dateField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisAccountCapacity capacity
                {
                    get
                    {
                        return this.capacityField;
                    }
                    set
                    {
                        this.capacityField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisAccountLender_type lender_type
                {
                    get
                    {
                        return this.lender_typeField;
                    }
                    set
                    {
                        this.lender_typeField = value;
                    }
                }

                /// <remarks/>
                public ushort limit
                {
                    get
                    {
                        return this.limitField;
                    }
                    set
                    {
                        this.limitField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisAccountCollaterals collaterals
                {
                    get
                    {
                        return this.collateralsField;
                    }
                    set
                    {
                        this.collateralsField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisAccountLegal legal
                {
                    get
                    {
                        return this.legalField;
                    }
                    set
                    {
                        this.legalField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlArrayItemAttribute("sub_account", IsNullable = false)]
                public reportEnq_reportEnquirySection_ccrisAccountSub_account[] sub_accounts
                {
                    get
                    {
                        return this.sub_accountsField;
                    }
                    set
                    {
                        this.sub_accountsField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisAccountCapacity
            {

                private string codeField;

                private string valueField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string code
                {
                    get
                    {
                        return this.codeField;
                    }
                    set
                    {
                        this.codeField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlTextAttribute()]
                public string Value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisAccountLender_type
            {

                private string codeField;

                private string valueField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string code
                {
                    get
                    {
                        return this.codeField;
                    }
                    set
                    {
                        this.codeField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlTextAttribute()]
                public string Value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisAccountCollaterals
            {

                private reportEnq_reportEnquirySection_ccrisAccountCollateralsCollateral collateralField;

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisAccountCollateralsCollateral collateral
                {
                    get
                    {
                        return this.collateralField;
                    }
                    set
                    {
                        this.collateralField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisAccountCollateralsCollateral
            {

                private string nameField;

                private ushort valueField;

                private string codeField;

                /// <remarks/>
                public string name
                {
                    get
                    {
                        return this.nameField;
                    }
                    set
                    {
                        this.nameField = value;
                    }
                }

                /// <remarks/>
                public ushort value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string code
                {
                    get
                    {
                        return this.codeField;
                    }
                    set
                    {
                        this.codeField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisAccountLegal
            {

                private object dateField;

                /// <remarks/>
                public object date
                {
                    get
                    {
                        return this.dateField;
                    }
                    set
                    {
                        this.dateField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisAccountSub_account
            {

                private reportEnq_reportEnquirySection_ccrisAccountSub_accountFacility facilityField;

                private reportEnq_reportEnquirySection_ccrisAccountSub_accountRepay_term repay_termField;

                private reportEnq_reportEnquirySection_ccrisAccountSub_accountCollaterals collateralsField;

                private reportEnq_reportEnquirySection_ccrisAccountSub_accountCr_position[] cr_positionsField;

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisAccountSub_accountFacility facility
                {
                    get
                    {
                        return this.facilityField;
                    }
                    set
                    {
                        this.facilityField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisAccountSub_accountRepay_term repay_term
                {
                    get
                    {
                        return this.repay_termField;
                    }
                    set
                    {
                        this.repay_termField = value;
                    }
                }

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisAccountSub_accountCollaterals collaterals
                {
                    get
                    {
                        return this.collateralsField;
                    }
                    set
                    {
                        this.collateralsField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlArrayItemAttribute("cr_position", IsNullable = false)]
                public reportEnq_reportEnquirySection_ccrisAccountSub_accountCr_position[] cr_positions
                {
                    get
                    {
                        return this.cr_positionsField;
                    }
                    set
                    {
                        this.cr_positionsField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisAccountSub_accountFacility
            {

                private string codeField;

                private string valueField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string code
                {
                    get
                    {
                        return this.codeField;
                    }
                    set
                    {
                        this.codeField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlTextAttribute()]
                public string Value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisAccountSub_accountRepay_term
            {

                private string codeField;

                private string valueField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string code
                {
                    get
                    {
                        return this.codeField;
                    }
                    set
                    {
                        this.codeField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlTextAttribute()]
                public string Value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisAccountSub_accountCollaterals
            {

                private reportEnq_reportEnquirySection_ccrisAccountSub_accountCollateralsCollateral collateralField;

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisAccountSub_accountCollateralsCollateral collateral
                {
                    get
                    {
                        return this.collateralField;
                    }
                    set
                    {
                        this.collateralField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisAccountSub_accountCollateralsCollateral
            {

                private string nameField;

                private string valueField;

                private string codeField;

                /// <remarks/>
                public string name
                {
                    get
                    {
                        return this.nameField;
                    }
                    set
                    {
                        this.nameField = value;
                    }
                }

                /// <remarks/>
                public string value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string code
                {
                    get
                    {
                        return this.codeField;
                    }
                    set
                    {
                        this.codeField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisAccountSub_accountCr_position
            {

                private reportEnq_reportEnquirySection_ccrisAccountSub_accountCr_positionStatus statusField;

                private string balanceField;

                private string position_dateField;

                private string inst_arrearsField;

                private object mon_arrearsField;

                private ushort inst_amountField;

                private object rescheduled_dateField;

                private object restructured_dateField;

                /// <remarks/>
                public reportEnq_reportEnquirySection_ccrisAccountSub_accountCr_positionStatus status
                {
                    get
                    {
                        return this.statusField;
                    }
                    set
                    {
                        this.statusField = value;
                    }
                }

                /// <remarks/>
                public string balance
                {
                    get
                    {
                        return this.balanceField;
                    }
                    set
                    {
                        this.balanceField = value;
                    }
                }

                /// <remarks/>
                public string position_date
                {
                    get
                    {
                        return this.position_dateField;
                    }
                    set
                    {
                        this.position_dateField = value;
                    }
                }

                /// <remarks/>
                public string inst_arrears
                {
                    get
                    {
                        return this.inst_arrearsField;
                    }
                    set
                    {
                        this.inst_arrearsField = value;
                    }
                }

                /// <remarks/>
                public object mon_arrears
                {
                    get
                    {
                        return this.mon_arrearsField;
                    }
                    set
                    {
                        this.mon_arrearsField = value;
                    }
                }

                /// <remarks/>
                public ushort inst_amount
                {
                    get
                    {
                        return this.inst_amountField;
                    }
                    set
                    {
                        this.inst_amountField = value;
                    }
                }

                /// <remarks/>
                public object rescheduled_date
                {
                    get
                    {
                        return this.rescheduled_dateField;
                    }
                    set
                    {
                        this.rescheduled_dateField = value;
                    }
                }

                /// <remarks/>
                public object restructured_date
                {
                    get
                    {
                        return this.restructured_dateField;
                    }
                    set
                    {
                        this.restructured_dateField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_ccrisAccountSub_accountCr_positionStatus
            {

                private string codeField;

                private string valueField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string code
                {
                    get
                    {
                        return this.codeField;
                    }
                    set
                    {
                        this.codeField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlTextAttribute()]
                public string Value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportEnq_reportEnquirySection_dcheqs
            {

                private string titleField;

                private bool dataField;

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string title
                {
                    get
                    {
                        return this.titleField;
                    }
                    set
                    {
                        this.titleField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public bool data
                {
                    get
                    {
                        return this.dataField;
                    }
                    set
                    {
                        this.dataField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportTrex
            {

                private reportTrexSubject_request subject_requestField;

                private string statusField;

                /// <remarks/>
                public reportTrexSubject_request subject_request
                {
                    get
                    {
                        return this.subject_requestField;
                    }
                    set
                    {
                        this.subject_requestField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlAttributeAttribute()]
                public string status
                {
                    get
                    {
                        return this.statusField;
                    }
                    set
                    {
                        this.statusField = value;
                    }
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.cmctos.com.my/ctosnet/response")]
            public partial class reportTrexSubject_request
            {

                private string party_typeField;

                private string nameField;

                private object iclcField;

                private ulong nicbrField;

                private string date_requestedField;

                private string date_completedField;

                private object requester_idField;

                private object requester_nameField;

                private string requester_comp_codeField;

                private string requester_accountField;

                /// <remarks/>
                public string party_type
                {
                    get
                    {
                        return this.party_typeField;
                    }
                    set
                    {
                        this.party_typeField = value;
                    }
                }

                /// <remarks/>
                public string name
                {
                    get
                    {
                        return this.nameField;
                    }
                    set
                    {
                        this.nameField = value;
                    }
                }

                /// <remarks/>
                public object iclc
                {
                    get
                    {
                        return this.iclcField;
                    }
                    set
                    {
                        this.iclcField = value;
                    }
                }

                /// <remarks/>
                public ulong nicbr
                {
                    get
                    {
                        return this.nicbrField;
                    }
                    set
                    {
                        this.nicbrField = value;
                    }
                }

                /// <remarks/>
                public string date_requested
                {
                    get
                    {
                        return this.date_requestedField;
                    }
                    set
                    {
                        this.date_requestedField = value;
                    }
                }

                /// <remarks/>
                public string date_completed
                {
                    get
                    {
                        return this.date_completedField;
                    }
                    set
                    {
                        this.date_completedField = value;
                    }
                }

                /// <remarks/>
                public object requester_id
                {
                    get
                    {
                        return this.requester_idField;
                    }
                    set
                    {
                        this.requester_idField = value;
                    }
                }

                /// <remarks/>
                public object requester_name
                {
                    get
                    {
                        return this.requester_nameField;
                    }
                    set
                    {
                        this.requester_nameField = value;
                    }
                }

                /// <remarks/>
                public string requester_comp_code
                {
                    get
                    {
                        return this.requester_comp_codeField;
                    }
                    set
                    {
                        this.requester_comp_codeField = value;
                    }
                }

                /// <remarks/>
                public string requester_account
                {
                    get
                    {
                        return this.requester_accountField;
                    }
                    set
                    {
                        this.requester_accountField = value;
                    }
                }
            }

        }
    }
}