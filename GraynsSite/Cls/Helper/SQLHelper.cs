﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Synergy.Helper
{
    public class SQLHelper
    {
        public static string[] splitResult(string input)
        {
            string[] str1;
            if (input.IndexOf("###") >= 0)
            {
                str1 = input.Split(new string[1] { "###" }, StringSplitOptions.RemoveEmptyEntries);
            }
            else
            {
                str1 = new string[1];
                str1[0] = input;
            }

            return str1;
        }
    }
}