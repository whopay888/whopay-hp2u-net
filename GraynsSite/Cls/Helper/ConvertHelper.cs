﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Synergy.Util;

namespace Synergy.Helper
{
    public class ConvertHelper
    {
        public ConvertHelper()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        //public static string FormatDateTime(string input)
        //{
        //    if (string.IsNullOrEmpty(input))
        //        input = ProjectProperties.MIN_DATE;
        //    return Convert.ToDateTime(input).ToString(ProjectProperties.TIME_FORMAT);
        //}

        public static DateTime ConvertToDateTime(object obj, DateTime defaultValue)
        {
            if (obj == null)
                return defaultValue;
            try
            {
                return Convert.ToDateTime(obj);
            }
            catch
            {
                return defaultValue;
            }
        }

        public static decimal ConvertToDecimal(object obj, decimal defaultValue)
        {
            decimal cDecimal;
            if (obj != null)
            {
                if (!decimal.TryParse(obj.ToString(), out cDecimal))
                    cDecimal = defaultValue;
            }
            else
                cDecimal = defaultValue;
            return cDecimal;
        }

        public static int ConvertToInt(object obj, int defaultValue)
        {
            int cint;

            if (obj != null)
            {
                if (!int.TryParse(obj.ToString(), out cint))
                    cint = defaultValue;
            }
            else
                cint = defaultValue;
            return cint;
        }

        public static Int32 ConvertToInt32(object obj, int defaultValue)
        {
            if (obj.ToString() == string.Empty)
                return defaultValue;
            if (Validation.isNumeric(obj.ToString()) == false)
                return defaultValue;
            return Convert.ToInt32(obj);
        }

        public static Int64 ConvertToInt64(object obj, long defaultValue)
        {
            Int64 i;
            if (!Int64.TryParse(obj.ToString(), out i))
                i = defaultValue;


            return i;
        }

        public static bool ConvertToBoolen(object obj, bool defaultValue)
        {
            switch (obj.ToString().ToUpper())
            {
                case "TRUE":
                    return true;
                case "FALSE":
                    return false;
                case "0":
                    return false;
                case "1":
                    return true;
                default:
                    return defaultValue;
            }
        }

        public static Int16 ConvertBoolenToBit(bool input, Int16 defaultValue)
        {
            switch (input.ToString().ToUpper())
            {
                case "FALSE":
                    return 0;
                case "TRUE":
                    return 1;
                default:
                    return defaultValue;
            }
        }
    }
}