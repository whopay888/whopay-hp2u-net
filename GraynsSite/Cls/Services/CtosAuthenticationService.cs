﻿using Dapper;
using Newtonsoft.Json;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Security;
using Synergy.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace HJT.Cls.Services
{
    public class CtosAuthenticationService : IAuthenticationService
    {
        async public Task<IAuthToken> Login()
        {
            // generate jwt signed token with private key
            var signedJwtToken = GenerateSignedToken();

            // prepare login api request body
            var ctosAuthUrl = ConfigurationManager.AppSettings["API_AUTH_URL"];
            var body = new List<KeyValuePair<string, string>>();
            body.Add(new KeyValuePair<string, string>("grant_type", "password"));
            body.Add(new KeyValuePair<string, string>(
                "client_id",
                ConfigurationManager.AppSettings["API_AUTH_CLIENT_ID"]
            ));
            body.Add(new KeyValuePair<string, string>(
                "username",
                ConfigurationManager.AppSettings["API_Username"]
            ));
            body.Add(new KeyValuePair<string, string>(
                "password",
                ConfigurationManager.AppSettings["API_Password"]
            ));
            body.Add(new KeyValuePair<string, string>(
                "client_assertion_type",
                "urn:ietf:params:oauth:client-assertion-type:jwt-bearer"
            ));
            body.Add(new KeyValuePair<string, string>(
                "client_assertion",
                signedJwtToken
            ));

            using (var client = new HttpClient())
            {
                try
                {
                    var response = await client.PostAsync(ctosAuthUrl, new FormUrlEncodedContent(body)).ConfigureAwait(false);

                    if (response.IsSuccessStatusCode)
                    {
                        var read = response.Content.ReadAsStringAsync().Result;
                        var content = JsonConvert.DeserializeObject<CtosLoginResponse>(read);
                        var ctosAuth = new CtosAuthTokens
                        {
                            AccessToken = content.AccessToken,
                            RefreshToken = content.RefreshToken,
                            UpdatedAt = DateTime.Now
                        };

                        await SaveTokens(ctosAuth).ConfigureAwait(false);
                        return ctosAuth;
                    }
                    else
                    {
                        LogUtil.logError($"CtosAuthenticationService.cs Login {response.StatusCode} {response.Content}", "");
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    LogUtil.logError($"CtosAuthenticationService.cs Login {ex.Message}", "");
                    return null;
                }
            }
        }

        async public Task<IAuthToken> RefreshLogin()
        {
            var conn = new SqlConnection(DbHelper.connectionString);
            var ctosAuthUrl = ConfigurationManager.AppSettings["API_AUTH_URL"];
            var body = new List<KeyValuePair<string, string>>();
            body.Add(new KeyValuePair<string, string>("grant_type", "refresh_token"));
            body.Add(new KeyValuePair<string, string>("scope", "openid"));
            body.Add(new KeyValuePair<string, string>(
                "client_id",
                ConfigurationManager.AppSettings["API_AUTH_CLIENT_ID"]
            ));
            body.Add(new KeyValuePair<string, string>(
                "client_secret",
                ConfigurationManager.AppSettings["API_AUTH_CLIENT_SECRET"]
            ));

            try
            {
                var token = await GetTokens().ConfigureAwait(false);
                body.Add(new KeyValuePair<string, string>("refresh_token", token.RefreshToken));

                using (var client = new HttpClient())
                {
                    var response = await client.PostAsync(ctosAuthUrl, new FormUrlEncodedContent(body)).ConfigureAwait(false);

                    if (response.IsSuccessStatusCode)
                    {
                        var read = response.Content.ReadAsStringAsync().Result;
                        var content = JsonConvert.DeserializeObject<CtosLoginResponse>(read);
                        var ctosAuth = new CtosAuthTokens
                        {
                            AccessToken = content.AccessToken,
                            RefreshToken = content.RefreshToken,
                            UpdatedAt = DateTime.Now
                        };

                        await SaveTokens(ctosAuth).ConfigureAwait(false);
                        return ctosAuth;
                    }
                    else
                    {
                        LogUtil.logError($"CtosAuthenticationService.cs RefreshToken {response.StatusCode} {response.Content}", "");
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtil.logError($"CtosAuthenticationService.cs RefreshToken {ex.Message}", "");
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        async public Task<IAuthToken> GetTokens()
        {
            var conn = new SqlConnection(DbHelper.connectionString);
            conn.Open();
            var query = @"SELECT accessToken, refreshToken, updatedAt FROM tbl_CtosAuthToken";

            try
            {
                var found = await conn.QueryFirstOrDefaultAsync<CtosAuthTokens>(query).ConfigureAwait(false);
                if (found == null) throw new Exception("Token not found in database.");

                return found;
            }
            catch (Exception ex)
            {
                LogUtil.logError($"CtosAuthenticationService.cs GetTokens {ex.Message}", query);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        async public Task<bool> SaveTokens(IAuthToken auth)
        {
            var conn = new SqlConnection(DbHelper.connectionString);
            conn.Open();
            var query = @"
                            UPDATE tbl_CtosAuthToken SET 
                            accessToken = @at,
                            refreshToken = @rt,
                            updatedAt = @dt";

            try
            {
                var executed = await conn.ExecuteAsync(query, new
                {
                    at = auth.AccessToken,
                    rt = auth.RefreshToken,
                    dt = DateTime.Now,
                }).ConfigureAwait(false);

                if (executed > 0) return true;
                else return false;
            }
            catch (Exception ex)
            {
                LogUtil.logError($"CtosAuthenticationService.cs SaveTokens {ex.Message}", query);
                return false;
            }
            finally
            {
                conn.Close();
            }
        }

        private string GenerateSignedToken()
        {
            var privateKeybyteArray = Encoding.ASCII.GetBytes(ConfigurationManager.AppSettings["API_AUTH_PRIVATE_KEY"]);
            var claims = new List<Claim>();
            claims.Add(new Claim("iss", ConfigurationManager.AppSettings["API_AUTH_ISS"]));
            claims.Add(new Claim(
                "iat",
                ((DateTimeOffset)DateTime.Now).ToUnixTimeSeconds().ToString())
            );
            claims.Add(new Claim(
                "exp",
                ((DateTimeOffset)DateTime.Now.AddMinutes(5)).ToUnixTimeSeconds().ToString())
            );
            claims.Add(new Claim("sub", ConfigurationManager.AppSettings["API_AUTH_ISS"]));
            claims.Add(new Claim("aud", ConfigurationManager.AppSettings["API_AUTH_AUD"]));
            claims.Add(new Claim("jti", Guid.NewGuid().ToString()));

            RSAParameters rsaParams;
            using (var ms = new MemoryStream(privateKeybyteArray))
            {
                using (var sr = new StreamReader(ms))
                {
                    var pemReader = new PemReader(sr);
                    var keyPair = pemReader.ReadObject() as AsymmetricCipherKeyPair;
                    var privateRsaParams = keyPair.Private as RsaPrivateCrtKeyParameters;
                    rsaParams = DotNetUtilities.ToRSAParameters(privateRsaParams);

                }
            }
            string token = "";
            using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider())
            {
                rsa.ImportParameters(rsaParams);
                Dictionary<string, object> payload = claims.ToDictionary(k => k.Type, v => (object)v.Value);
                token = Jose.JWT.Encode(payload, rsa, Jose.JwsAlgorithm.RS256);
            }

            return token;
        }
    }

    public interface IAuthenticationService
    {
        Task<IAuthToken> Login();
        Task<IAuthToken> RefreshLogin();
        Task<IAuthToken> GetTokens();
        Task<bool> SaveTokens(IAuthToken auth);
    }

    public class CtosAuthTokens : IAuthToken
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public DateTime UpdatedAt { get; set; }
    }

    public interface IAuthToken
    {
        string AccessToken { get; set; }
        string RefreshToken { get; set; }
        DateTime UpdatedAt { get; set; }
    }

    public class CtosLoginResponse
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        [JsonProperty("expires_in")]
        public string ExpiresIn { get; set; }
        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }
        [JsonProperty("refresh_expires_in")]
        public string RefreshExpiresIn { get; set; }
        [JsonProperty("token_type")]
        public string TokenType { get; set; }
        [JsonProperty("not-before-policy")]
        public string NotBeforePolicy { get; set; }
        [JsonProperty("session_state")]
        public string SessionState { get; set; }
        [JsonProperty("scope")]
        public string Scope { get; set; }
    }
}