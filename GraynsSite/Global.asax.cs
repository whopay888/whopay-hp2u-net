﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Globalization;
using System.Threading;
using Synergy.Util;

namespace WMElegance
{
    public class Global : System.Web.HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup

        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            // To initialize culture
            string culture = null;
            try
            {
                if (Request.Cookies[KeyVal.cookie_language] != null)
                {
                    culture = secure.Decrypt(Request.Cookies[KeyVal.cookie_language].Value, true);// retrieve value from cookie
                    //Request.Cookies[KeyVal.cookie_language].Expires = DateTime.Now.AddDays(-1D);
                    
                }
                else
                {
                    culture = "en";                    
                }
            }
            catch
            {
                culture = "en";
            }
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
            Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);
        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs
            Exception ErrException = Server.GetLastError();
            if (ErrException.InnerException != null)
            {
                LogUtil.logError("System Error : " + ErrException.InnerException.ToString(), "");
            }

        }

        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started

        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.

        }
    }
}