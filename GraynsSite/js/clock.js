﻿function date_time(id) {
    date = new Date;
    year = date.getFullYear();
    month = date.getMonth();
    //months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'Jully', 'August', 'September', 'October', 'November', 'December');
    months = new Array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
    d = date.getDate();
    day = date.getDay();
    //days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');

    time = 'AM';

    h = date.getHours();

    if (h >= 12) {

        if (h > 12)
            h = h - 12;

        if (h < 10) {
            h = '0' + h;
        }

        time = 'PM';
    }
    else if (h == 0) {
        h = 12;
    }
    else if (h < 10) {
        h = '0' + h;
    }

    m = date.getMinutes();
    if (m < 10) {
        m = "0" + m;
    }
    s = date.getSeconds();
    if (s < 10) {
        s = "0" + s;
    }
    //result = '' + days[day] + ' ' + months[month] + ' ' + d + ' ' + year + ' ' + h + ':' + m + ':' + s;

    result = '' + d + ' ' + months[month] + ' ' + year + ' ' + h + ':' + m + ':' + s + ' ' + time;
    document.getElementById(id).innerHTML = result;
    setTimeout('date_time("' + id + '");', '1000');
    return true;
}