insert into tbl_Parameter (Category, ParameterName, ParameterValue, Remarks)
values('ClientList', 'NumberOfDataRows', '100', 'Limit number of data rows displayed in ClientList')