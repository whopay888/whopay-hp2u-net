/*
   Monday, July 22, 20192:08:04 PM
   User: dsradmin
   Server: 110.4.46.107,1533
   Database: DSR
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
EXECUTE sp_rename N'dbo.tbl_MemberInfo.UploadDoucment', N'Tmp_UploadDocument', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.tbl_MemberInfo.Tmp_UploadDocument', N'UploadDocument', 'COLUMN' 
GO
ALTER TABLE dbo.tbl_MemberInfo SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

insert into tbl_Menu (ShowText_EN, Name, ParentID, menuLevel, LinkUrl, Sort, IsDeleted, CreateAt, CreateBy, UpdateAt, UpdateBy, Type)
values('Profile Setting', 'Profile Setting', '20', '1', '/Form/User/EditProfile.aspx', '3', '0', GETDATE(), 'SYSTEM', GETDATE(), 'SYSTEM', '1')