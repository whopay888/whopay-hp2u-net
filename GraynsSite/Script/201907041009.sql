USE [HP2U]
GO
/****** Object:  UserDefinedFunction [dbo].[Func_CalculateLoanEligible]    Script Date: 7/4/2019 9:46:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[Func_CalculateLoanEligible]
(
	-- Add the parameters for the function here
	@BankPolicy Decimal(18,2), -- in %
	@TotalOldRepayment Decimal(18,2) , 
	@Age int,
	@Salary Decimal(18,2),
	@Tenure int,
	@BankID int
)
RETURNS decimal(18,2)
AS
BEGIN

declare @Interest as decimal(18,2) = 0;
select top 1 @Interest = hp.Interest from tbl_HirePurchase hp with (nolock) where hp.BankID = @BankID;
declare @LoanEligible decimal(18,2) = 0 ;
declare @TenureMonth as int = @Tenure*12;

declare @BankPolicyReal as decimal(18,10) = @BankPolicy/100;

declare @PeriodicRepayment as decimal(18,10) = (@Salary*@BankPolicyReal) - @TotalOldRepayment;

declare @InterestReal as decimal(18,10) = @Interest /100;

set @LoanEligible = (@PeriodicRepayment * @TenureMonth) / (1 + (@InterestReal * @Tenure))

return @LoanEligible;

END
