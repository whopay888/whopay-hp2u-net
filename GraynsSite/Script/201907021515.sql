/*
   Tuesday, July 2, 20193:15:03 PM
   User: dsradmin
   Server: 110.4.46.107,1533
   Database: DSR
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.tbl_CreditAdvisor ADD
	Remark nvarchar(500) NULL,
	Category nvarchar(50) NULL
GO
ALTER TABLE dbo.tbl_CreditAdvisor SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
