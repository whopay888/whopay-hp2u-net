/*
   Friday, June 21, 201912:14:36 PM
   User: dsradmin
   Server: 110.4.46.107,1533
   Database: DSR
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.tbl_BankName ADD
	IsSelectedDefault bit NULL
GO
ALTER TABLE dbo.tbl_BankName SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
