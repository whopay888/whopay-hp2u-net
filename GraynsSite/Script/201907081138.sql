/*
   Monday, July 8, 201911:37:41 AM
   User: dsradmin
   Server: 110.4.46.107,1533
   Database: HP2U
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.tbl_State
	(
	StateID bigint NOT NULL IDENTITY (1, 1),
	StateName nvarchar(200) NULL,
	isDeleted bit NOT NULL,
	CreatedBy nvarchar(200) NULL,
	CreatedAt datetime NULL,
	UpdatedBy nvarchar(200) NULL,
	UpdatedAt datetime NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.tbl_State ADD CONSTRAINT
	DF_tbl_State_isDeleted DEFAULT 0 FOR isDeleted
GO
ALTER TABLE dbo.tbl_State ADD CONSTRAINT
	PK_tbl_State PRIMARY KEY CLUSTERED 
	(
	StateID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.tbl_State SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
